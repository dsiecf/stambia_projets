<?xml version="1.0" encoding="UTF-8"?>
<proc:process xmlns:UUID="java.util.UUID" xmlns:common="http://exslt.org/common" xmlns:mdj="java:com.indy.xsl.global.Functions" xmlns:proc="http://www.example.org/proc" xmlns:saxon="http://saxon.sf.net/" id="_p0wtRslVEeeq3sp4Y851OA" description="This template is used for data integration on a target table without using a load step.&#xD;&#xA;Data is directly selected from the source and inserted into the target using a bind.&#xD;&#xA;&#xD;&#xA;Refer to the description of each Parameter for more information.&#xD;&#xA;&#xD;&#xA;&#xD;&#xA;History:&#xD;&#xA;14/11/2017   Template creation" metaInf="&lt;template-feature-support name=&quot;load&quot; enable=&quot;false&quot; evaluation=&quot;NATIVE&quot;/>" isBeginAction="true" nbCycles="-1" toleratedError="false">
  <parameter id="_p0wtSclVEeeq3sp4Y851OA" description="If true, the transactions are committed at the end of the integration.&#xD;&#xA;Please note that this option is taken into acount only if TransactionalModeOnTarget is set to true." name="commitTransaction" type="Boolean" value="true"/>
  <parameter id="_p0wtSslVEeeq3sp4Y851OA" description="If true, the operations on the target table will be executed in a dedicated transaction.&#xD;&#xA;If false, each statement on the target table uses an Autocommit transaction." name="transactionalModeOnTarget" type="Boolean" value="false"/>
  <parameter id="_p0wtS8lVEeeq3sp4Y851OA" description="Name of the transaction when Transaction Mode is set to true." name="transactionName" type="String" value="T1"/>
  <parameter id="_p0wtTclVEeeq3sp4Y851OA" description="If true, a DISTINCT statement will be added on the query inserting data in the integration table.&#xD;&#xA;This is used to avoid having duplicates in the target table." name="useDistinct" type="Boolean" value="false"/>
  <parameter id="_p0wtTslVEeeq3sp4Y851OA" description="If true, target table is truncated prior to the Integration.&#xD;&#xA;Note: If you set this option to true you probably want to load the target table with 'Append Mode' to true for better performance." name="truncateTargetTable" type="Boolean" value="false"/>
  <parameter id="_p0wtT8lVEeeq3sp4Y851OA" description="If true, all the target table rows are deleted before the integration with a DELETE statement.&#xD;&#xA;For better performance you should use truncateTargetTable when possible." name="deleteAllTargetTable" type="Boolean" value="false"/>
  <parameter id="_p0wtUMlVEeeq3sp4Y851OA" description="This option is used to choose whether to create or not the target table before the integration.&#xD;&#xA;&#xD;&#xA;- true: The target table is created. If it already exists no error is raised.&#xD;&#xA;- false: The target table is not created.&#xD;&#xA;- drop and create: The target table will be dropped, and re-created on each execution." metaInf="&lt;metaInf id=&quot;value&quot;  value=&quot;true&quot;/>&#xD;&#xA;&lt;metaInf id=&quot;value&quot;  value=&quot;false&quot;/>&#xD;&#xA;&lt;metaInf id=&quot;value&quot;  value=&quot;drop and create&quot;/>" name="createTargetTable" type="String" value="false"/>
  <parameter id="_p0wtVclVEeeq3sp4Y851OA" description="JDBC Batch size (number of lines) used when inserting the data into the target table.&#xD;&#xA;Larger values provide better performance but consume more memory in the Runtime as the data is stored in the Memory before being sent to the target table." name="batchSize" type="Integer" value="1000"/>
  <parameter id="_p0wtVslVEeeq3sp4Y851OA" description="Defines a valid SQL Expression used to order the data when loading the target table." name="orderByExpression" type="String" value=""/>
  <subProcess id="_p0wtV8lVEeeq3sp4Y851OA" name="PREPARE_INTEGRATION">
    <link id="_p0wtWMlVEeeq3sp4Y851OA" executionType="OK" generationType="OK" mandatory="1" targetId="_p01l4slVEeeq3sp4Y851OA?fileId=_p0wtRslVEeeq3sp4Y851OA$type=proc$name=INTEGRATION?"/>
    <parameter id="_p0wtWclVEeeq3sp4Y851OA" name="TPL_STEP" type="String" value="Integration.BeforeControl"/>
    <actionCode id="_p0wtWslVEeeq3sp4Y851OA" generationCondition="'${createTargetTable}$'='true' or '${createTargetTable}$' = 'drop and create'" isBeginAction="false" name="Creation of target table " nbCycles="-1" toleratedError="true" technology="com.indy.engine.actionCodes.JdbcActionCodeI">
      <link id="_p0wtW8lVEeeq3sp4Y851OA" executionType="OK" generationType="OK_KO" mandatory="1" targetId="_p0wtaclVEeeq3sp4Y851OA"/>
      <parameter id="_p0wtXMlVEeeq3sp4Y851OA" name="SQL_CONNECTION" type="String" value="%targetConnection{$REF}%"/>
      <parameter id="_p0wtXclVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE" type="String" value="DDL_DML"/>
      <parameter id="_p0wtXslVEeeq3sp4Y851OA" name="SQL_TRANSACTION_TYPE" type="String" value="AUTOCOMMIT"/>
      <code>Create table %x{md:physicalPath($REF/ref:target())}x%
(
	%x{md:patternList($REF/ref:target()/ref:columns(),'[columnName] [targetCreationType] [null]',',\n\t')}x%
)</code>
    </actionCode>
    <actionCode id="_p0wtaclVEeeq3sp4Y851OA" generationCondition="'${truncateTargetTable}$'='true'" isBeginAction="false" name="Truncation of target table" nbCycles="-1" toleratedError="false" technology="com.indy.engine.actionCodes.JdbcActionCodeI">
      <link id="_p0wtaslVEeeq3sp4Y851OA" generationCondition="" executionType="OK" generationType="OK_KO" mandatory="1" targetId="_p01lz8lVEeeq3sp4Y851OA"/>
      <parameter id="_p0wta8lVEeeq3sp4Y851OA" name="SQL_CONNECTION" type="String" value="%targetConnection{$REF}%"/>
      <parameter id="_p0wtbMlVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE" type="String" value="DDL_DML"/>
      <parameter id="_p0wtbclVEeeq3sp4Y851OA" name="SQL_TRANSACTION_TYPE" type="String" value="AUTOCOMMIT"/>
      <code>Truncate table %x{md:physicalPath($REF/ref:target())}x%</code>
    </actionCode>
    <actionCode id="_p0wtbslVEeeq3sp4Y851OA" generationCondition="" isBeginAction="false" name="Lock of CDC table" nbCycles="-1" repetitionMode="Sequential" repetitionQuery="$REF/ref:from()[tech:isCDC() and not(tech:isLoaded())]" repetitionVariableName="CDC" toleratedError="false" technology="com.indy.engine.actionCodes.JdbcActionCodeI">
      <parameter id="_p0wtcMlVEeeq3sp4Y851OA" name="SQL_CONNECTION" type="String" value="%targetConnection{$REF}%"/>
      <parameter id="_p0wtcclVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE" type="String" value="DDL_DML"/>
      <parameter id="_p0wtcslVEeeq3sp4Y851OA" name="SQL_TRANSACTION_TYPE" type="String" value="AUTOCOMMIT"/>
      <code>update 	%x{md:physicalPath($CDC,'cdcTableName')}x%
set 	CDC_CONSUMMATION = '1'
where	(1=1)
%x{$CDC/tech:cdcFilterPart()}x%</code>
    </actionCode>
    <actionCode id="_p01lz8lVEeeq3sp4Y851OA" generationCondition="'${deleteAllTargetTable}$'='true'" isBeginAction="false" name="T - Deletion of target table" nbCycles="-1" toleratedError="false" technology="com.indy.engine.actionCodes.JdbcActionCodeI">
      <link id="_p01l0MlVEeeq3sp4Y851OA" executionType="OK" generationType="OK_KO" mandatory="1" targetId="_p0wtbslVEeeq3sp4Y851OA"/>
      <parameter id="_p01l0clVEeeq3sp4Y851OA" name="SQL_CONNECTION" type="String" value="%targetConnection{$REF}%"/>
      <parameter id="_p01l0slVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE" type="String" value="DDL_DML"/>
      <parameter id="_p01l08lVEeeq3sp4Y851OA" name="SQL_TRANSACTION_TYPE" type="String" value="%x{if (md:paramValue($this,'transactionalModeOnTarget')='true') then 'NOCOMMIT' else 'AUTOCOMMIT' }x%"/>
      <parameter id="_p01l1MlVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE_STAT" type="String" value="DELETE"/>
      <parameter id="_p01l1clVEeeq3sp4Y851OA" name="SQL_TRANSACTION_NAME" type="String" value="%x{if (md:paramValue($this,'transactionalModeOnTarget')='true') then md:paramValue($this,'transactionName') else '' }x%"/>
      <code>delete from %x{md:physicalPath($REF/ref:target())}x%</code>
    </actionCode>
    <actionCode id="_p01l1slVEeeq3sp4Y851OA" generationCondition="'${createTargetTable}$' = 'drop and create'" isBeginAction="false" name="Drop of target table " nbCycles="-1" toleratedError="true" technology="com.indy.engine.actionCodes.JdbcActionCodeI">
      <link id="_p01l18lVEeeq3sp4Y851OA" executionType="OK" generationType="OK_KO" mandatory="1" targetId="_p0wtWslVEeeq3sp4Y851OA"/>
      <parameter id="_p01l2MlVEeeq3sp4Y851OA" name="SQL_CONNECTION" type="String" value="%targetConnection{$REF}%"/>
      <parameter id="_p01l2clVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE" type="String" value="DDL_DML"/>
      <parameter id="_p01l2slVEeeq3sp4Y851OA" name="SQL_TRANSACTION_TYPE" type="String" value="AUTOCOMMIT"/>
      <code>Drop table %x{md:physicalPath($REF/ref:target())}x%</code>
    </actionCode>
    <templateType>I-TP.RDBMS.PREPARING</templateType>
  </subProcess>
  <subProcess id="_p01l4slVEeeq3sp4Y851OA" name="INTEGRATION">
    <parameter id="_p01l48lVEeeq3sp4Y851OA" name="TPL_STEP" type="String" value="Integration.AfterControl"/>
    <actionCode id="_p01mAMlVEeeq3sp4Y851OA" generationCondition="'${commitTransaction}$'='true' and '${transactionalModeOnTarget}$'='true'" isBeginAction="false" name="T - Commit of transaction in target" nbCycles="-1" toleratedError="false" technology="com.indy.engine.actionCodes.JdbcActionCodeI">
      <link id="_p01mAclVEeeq3sp4Y851OA" executionType="OK" generationType="OK_KO" mandatory="1" targetId="_p01mBslVEeeq3sp4Y851OA"/>
      <parameter id="_p01mAslVEeeq3sp4Y851OA" name="SQL_CONNECTION" type="String" value="%targetConnection{$REF}%"/>
      <parameter id="_p01mA8lVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE" type="String" value="DDL_DML"/>
      <parameter id="_p01mBMlVEeeq3sp4Y851OA" name="SQL_TRANSACTION_NAME" type="String" value="%x{if (md:paramValue($this,'transactionalModeOnTarget')='true') then md:paramValue($this,'transactionName') else '' }x%"/>
      <parameter id="_p01mBclVEeeq3sp4Y851OA" name="SQL_TRANSACTION_TYPE" type="String" value="COMMIT"/>
      <code></code>
    </actionCode>
    <actionCode id="_p01mBslVEeeq3sp4Y851OA" generationCondition="" isBeginAction="false" name="Unlock of CDC table" nbCycles="-1" repetitionMode="Sequential" repetitionQuery="$REF/ref:from()[tech:isCDC() and not(tech:isLoaded())]" repetitionVariableName="CDC" toleratedError="false" technology="com.indy.engine.actionCodes.JdbcActionCodeI">
      <parameter id="_p01mCMlVEeeq3sp4Y851OA" name="SQL_CONNECTION" type="String" value="%targetConnection{$REF}%"/>
      <parameter id="_p01mCclVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE" type="String" value="DDL_DML"/>
      <parameter id="_p01mCslVEeeq3sp4Y851OA" name="SQL_TRANSACTION_TYPE" type="String" value="AUTOCOMMIT"/>
      <code>Delete 	from 	%x{md:physicalPath($CDC,'cdcTableName')}x%
Where	CDC_CONSUMMATION = '1'
%x{$CDC/tech:cdcFilterPart()}x%</code>
    </actionCode>
    <actionCode id="_p01mIMlVEeeq3sp4Y851OA" generationCondition="" isBeginAction="false" name="Select on source table (SRC)" nbCycles="-1" toleratedError="false" technology="com.indy.engine.actionCodes.JdbcActionCodeI">
      <bindLink id="_p01mIclVEeeq3sp4Y851OA" bindType="DirectBind" executionType="OK" targetId="_p01mJMlVEeeq3sp4Y851OA"/>
      <parameter id="_p01mIslVEeeq3sp4Y851OA" name="SQL_CONNECTION" type="String" value="%sourceConnection{$REF}%"/>
      <parameter id="_p01mI8lVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE" type="String" value="SELECT"/>
      <code>select		%x{if (md:paramValue($this,'useDistinct')='true') then 'DISTINCT' else ''}x%&#xD;
			%x{md:patternList($REF/ref:columns(),'[expression] [columnAliasWord] [workName]', ',\n\t\t\t')}x%&#xD;
&#xD;
from	%x{$REF/tech:fromPart()}x%&#xD;
where	(1=1)%xsl{&#xD;
%x{$REF/tech:joinPart()}x%&#xD;
%x{$REF/tech:filterPart()}x%&#xD;
%x{$REF/tech:cdcFilterPart()}x%&#xD;
%x{$REF/tech:groupByPart()}x%&#xD;
%x{$REF/tech:havingPart()}x%}xsl%&#xD;
%x{if (md:paramValue($this,'orderByExpression')!='') then concat('order by ',md:paramValue($this,'orderByExpression')) else ''}x%</code>
    </actionCode>
    <actionCode id="_p01mJMlVEeeq3sp4Y851OA" generationCondition="" isBeginAction="false" name="Load of data (TRG)" nbCycles="-1" repetitionQuery="" toleratedError="false" technology="com.indy.engine.actionCodes.JdbcActionCodeI">
      <link id="_p01mJclVEeeq3sp4Y851OA" executionType="OK" generationType="OK" mandatory="1"/>
      <link id="_p01mJslVEeeq3sp4Y851OA" executionType="OK" generationType="OK_KO" mandatory="1" targetId="_p01mAMlVEeeq3sp4Y851OA"/>
      <parameter id="_p01mJ8lVEeeq3sp4Y851OA" name="SQL_CONNECTION" type="String" value="%targetConnection{$REF}%"/>
      <parameter id="_p01mKMlVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE" type="String" value="INSERT"/>
      <parameter id="_p01mKclVEeeq3sp4Y851OA" name="SQL_TRANSACTION_TYPE" type="String" value="%x{if (md:paramValue($this,'transactionalModeOnTarget')='true') then 'NOCOMMIT' else 'AUTOCOMMIT' }x%"/>
      <parameter id="_p01mKslVEeeq3sp4Y851OA" name="SQL_TRANSACTION_NAME" type="String" value="%x{if (md:paramValue($this,'transactionalModeOnTarget')='true') then md:paramValue($this,'transactionName') else '' }x%"/>
      <parameter id="_p01mK8lVEeeq3sp4Y851OA" name="SQL_BATCH_SIZE" type="String" value="%x{md:paramValue($this,'batchSize')}x%"/>
      <parameter id="_p01mLMlVEeeq3sp4Y851OA" name="SQL_ACTION_TYPE_STAT" type="String" value="INSERT"/>
      <code>insert into	%x{md:physicalPath($REF/ref:target())}x%&#xD;
( &#xD;
	%x{md:patternList($REF/ref:columns()[tech:isIns()],'[name]', ',\n\t')}x%&#xD;
) &#xD;
values&#xD;
(&#xD;
	%x{md:patternList($REF/ref:columns()[tech:isIns()],':{{[workName]}}:', ',\n\t')}x%&#xD;
&#xD;
)&#xD;
</code>
    </actionCode>
    <templateType>I-TP.RDBMS.INTEGRATING</templateType>
  </subProcess>
  <templateType>I-TP.RDBMS</templateType>
</proc:process>