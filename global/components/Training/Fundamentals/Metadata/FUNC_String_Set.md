<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.function.folder" id="_ckwI0OYqEeeg4IHoU6igbw" name="stambiaStringLibrary" md:ref="platform:/plugin/com.indy.environment/.tech/others/udf.md#UUID_MD_UDF?fileId=UUID_MD_UDF$type=md$name=User%20Defined%20Functions?">
  <attribute defType="com.stambia.function.folder.prefix" id="_TAxUUOYuEeeg4IHoU6igbw" value="s"/>
  <node defType="com.stambia.function.function" id="_WYMjIOYuEeeg4IHoU6igbw" name="removeSpace">
    <attribute defType="com.stambia.function.function.description" id="_PjoXsOYxEee4xLxZ5pzwCQ" value="Remove space before and after"/>
    <node defType="com.stambia.function.parameter" id="_4mtI8OYwEee4xLxZ5pzwCQ" name="string"/>
    <node defType="com.stambia.function.implementation" id="_9N9n8eYwEee4xLxZ5pzwCQ" name="Direct_Trim">
      <attribute defType="com.stambia.function.implementation.productCode" id="_B-gl0OYxEee4xLxZ5pzwCQ">
        <values>HYPERSONIC_SQL</values>
        <values>ORACLE</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_EP9rEOYxEee4xLxZ5pzwCQ" value="TRIM($string)"/>
      <attribute defType="com.stambia.function.implementation.description" id="_J0VSkOYxEee4xLxZ5pzwCQ" value=""/>
    </node>
    <node defType="com.stambia.function.implementation" id="_KGzioeYxEee4xLxZ5pzwCQ" name="No_Direct_Trim">
      <attribute defType="com.stambia.function.implementation.productCode" id="_MJAD4OYxEee4xLxZ5pzwCQ">
        <values>MICROSOFT_SQL_SERVER</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_N-UmwOYxEee4xLxZ5pzwCQ" value="RTRIM(LTRIM($string))"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_B23EIPICEeeYj5neBLIRPw" name="rightPad">
    <attribute defType="com.stambia.function.function.description" id="_B23EIfICEeeYj5neBLIRPw" value="Right padding of a string"/>
    <node defType="com.stambia.function.parameter" id="_B23EIvICEeeYj5neBLIRPw" name="stringToPad"/>
    <node defType="com.stambia.function.parameter" id="_B23EI_ICEeeYj5neBLIRPw" name="intLengthToPad"/>
    <node defType="com.stambia.function.parameter" id="_B23EJPICEeeYj5neBLIRPw" name="charToUseInPad"/>
    <node defType="com.stambia.function.implementation" id="_B23EJfICEeeYj5neBLIRPw" name="rpad">
      <attribute defType="com.stambia.function.implementation.productCode" id="_B23EJvICEeeYj5neBLIRPw">
        <values>HYPERSONIC_SQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_B23EJ_ICEeeYj5neBLIRPw" value="rpad($stringToPad,$intLengthToPad,$charToUseInPad)"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_GOMqoOY3Eee4xLxZ5pzwCQ" name="lowerCaseString">
    <attribute defType="com.stambia.function.function.description" id="_Iz4TcOY3Eee4xLxZ5pzwCQ" value="lower of a string"/>
    <node defType="com.stambia.function.parameter" id="_G1etEOY3Eee4xLxZ5pzwCQ" name="string"/>
    <node defType="com.stambia.function.implementation" id="_Kq9J0eY3Eee4xLxZ5pzwCQ" name="Direct_Lower">
      <attribute defType="com.stambia.function.implementation.productCode" id="_O_VJEOY3Eee4xLxZ5pzwCQ">
        <values>HYPERSONIC_SQL</values>
        <values>IBM_DB2_400</values>
        <values>MICROSOFT_SQL_SERVER</values>
        <values>MYSQL</values>
        <values>ORACLE</values>
        <values>POSTGRESSQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_QzX5kOY3Eee4xLxZ5pzwCQ" value="LOWER($string)"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_T7zDEOY3Eee4xLxZ5pzwCQ" name="upperCaseString">
    <attribute defType="com.stambia.function.function.description" id="_T7zDEeY3Eee4xLxZ5pzwCQ" value="uppercase of a string"/>
    <node defType="com.stambia.function.parameter" id="_T7zDEuY3Eee4xLxZ5pzwCQ" name="string"/>
    <node defType="com.stambia.function.implementation" id="_T7zDE-Y3Eee4xLxZ5pzwCQ" name="Direct_Upper">
      <attribute defType="com.stambia.function.implementation.productCode" id="_T7zDFOY3Eee4xLxZ5pzwCQ">
        <values>HYPERSONIC_SQL</values>
        <values>IBM_DB2_400</values>
        <values>MICROSOFT_SQL_SERVER</values>
        <values>MYSQL</values>
        <values>ORACLE</values>
        <values>POSTGRESSQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_T7zDFeY3Eee4xLxZ5pzwCQ" value="UPPER($string)"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_yOcvYPICEeeYj5neBLIRPw" name="numberToString">
    <attribute defType="com.stambia.function.function.description" id="_yOcvYfICEeeYj5neBLIRPw" value="Conversion from number to string (varchar)"/>
    <node defType="com.stambia.function.parameter" id="_yOcvYvICEeeYj5neBLIRPw" name="number"/>
    <node defType="com.stambia.function.implementation" id="_yOcvY_ICEeeYj5neBLIRPw" name="WithConvert">
      <attribute defType="com.stambia.function.implementation.productCode" id="_yOcvZPICEeeYj5neBLIRPw">
        <values>HYPERSONIC_SQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_yOcvZfICEeeYj5neBLIRPw" value="convert($number,VARCHAR) "/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_aBDzEOlWEee4xLxZ5pzwCQ" name="concat2strings">
    <attribute defType="com.stambia.function.function.description" id="_uPHn0OlWEee4xLxZ5pzwCQ" value="Concatenation of 2 strings"/>
    <node defType="com.stambia.function.parameter" id="_ag1fUOlWEee4xLxZ5pzwCQ" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_wMzcgOlWEee4xLxZ5pzwCQ" name="string2"/>
    <node defType="com.stambia.function.implementation" id="_xeLk0elWEee4xLxZ5pzwCQ" name="Concatenation_With_Plus_Operator">
      <attribute defType="com.stambia.function.implementation.productCode" id="_5bq_0OlWEee4xLxZ5pzwCQ">
        <values>HYPERSONIC_SQL</values>
        <values>MICROSOFT_SQL_SERVER</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_85PS4OlWEee4xLxZ5pzwCQ" value="$string1 + $string2"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_EWVqOfH1EeeYj5neBLIRPw" name="concat4Strings">
    <node defType="com.stambia.function.parameter" id="_EWVqOvH1EeeYj5neBLIRPw" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_EWVqO_H1EeeYj5neBLIRPw" name="string2"/>
    <node defType="com.stambia.function.parameter" id="_EWVqPPH1EeeYj5neBLIRPw" name="string3"/>
    <node defType="com.stambia.function.implementation" id="_EWVqPfH1EeeYj5neBLIRPw" name="Concatenation_With_Plus_Operator">
      <attribute defType="com.stambia.function.implementation.productCode" id="_EWVqPvH1EeeYj5neBLIRPw">
        <values>HYPERSONIC_SQL</values>
        <values>MICROSOFT_SQL_SERVER</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_EWVqP_H1EeeYj5neBLIRPw" value="$string1 + $string2 + $string3 + $string4"/>
    </node>
    <node defType="com.stambia.function.parameter" id="_EWVqQPH1EeeYj5neBLIRPw" name="string4"/>
  </node>
  <node defType="com.stambia.function.function" id="_BhJSIOlXEee4xLxZ5pzwCQ" name="concat3Strings">
    <node defType="com.stambia.function.parameter" id="_CaZOUOlXEee4xLxZ5pzwCQ" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_Iq3G0OlXEee4xLxZ5pzwCQ" name="string2"/>
    <node defType="com.stambia.function.parameter" id="_JxND0OlXEee4xLxZ5pzwCQ" name="string3"/>
    <node defType="com.stambia.function.implementation" id="_LM8pEelXEee4xLxZ5pzwCQ" name="Concatenation_With_Plus_Operator">
      <attribute defType="com.stambia.function.implementation.productCode" id="_OtA2gOlXEee4xLxZ5pzwCQ">
        <values>HYPERSONIC_SQL</values>
        <values>MICROSOFT_SQL_SERVER</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_RTAoYOlXEee4xLxZ5pzwCQ" value="$string1 + $string2 + $string3"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_Ic_L8PH0EeeYj5neBLIRPw" name="subString">
    <attribute defType="com.stambia.function.function.description" id="_MKh0sPH0EeeYj5neBLIRPw" value="Substring of a string"/>
    <node defType="com.stambia.function.implementation" id="_Ix4d4fH0EeeYj5neBLIRPw" name="substring">
      <attribute defType="com.stambia.function.implementation.productCode" id="_RkTVwPH0EeeYj5neBLIRPw">
        <values>MICROSOFT_SQL_SERVER</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_TWE1YPH0EeeYj5neBLIRPw" value="substring($stringToCut,$intBeginPosition,$intLengthToCut)"/>
    </node>
    <node defType="com.stambia.function.parameter" id="_WY34APH0EeeYj5neBLIRPw" name="stringToCut"/>
    <node defType="com.stambia.function.parameter" id="_auiQsPH0EeeYj5neBLIRPw" name="intBeginPosition"/>
    <node defType="com.stambia.function.parameter" id="_dlJRcPH0EeeYj5neBLIRPw" name="intLengthToCut"/>
    <node defType="com.stambia.function.implementation" id="_tDhrA_H0EeeYj5neBLIRPw" name="substr">
      <attribute defType="com.stambia.function.implementation.productCode" id="_tDhrBPH0EeeYj5neBLIRPw">
        <values>H2</values>
        <values>HYPERSONIC_SQL</values>
        <values>MYSQL</values>
        <values>ORACLE</values>
        <values>POSTGRESSQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_tDhrBfH0EeeYj5neBLIRPw" value="substr($stringToCut,$intBeginPosition,$intLengthToCut)"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_6nSK9_H0EeeYj5neBLIRPw" name="concat5Strings">
    <node defType="com.stambia.function.parameter" id="_6nSK-PH0EeeYj5neBLIRPw" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_6nSK-fH0EeeYj5neBLIRPw" name="string2"/>
    <node defType="com.stambia.function.parameter" id="_6nSK-vH0EeeYj5neBLIRPw" name="string3"/>
    <node defType="com.stambia.function.implementation" id="_6nSK-_H0EeeYj5neBLIRPw" name="Concatenation_With_Plus_Operator">
      <attribute defType="com.stambia.function.implementation.productCode" id="_6nSK_PH0EeeYj5neBLIRPw">
        <values>HYPERSONIC_SQL</values>
        <values>MICROSOFT_SQL_SERVER</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_6nSK_fH0EeeYj5neBLIRPw" value="$string1 + $string2 + $string3 + $string4 + $string5"/>
    </node>
    <node defType="com.stambia.function.parameter" id="_6yTvMPH0EeeYj5neBLIRPw" name="string4"/>
    <node defType="com.stambia.function.parameter" id="_-V9cIPH0EeeYj5neBLIRPw" name="string5"/>
  </node>
  <node defType="com.stambia.function.function" id="_xxL3ifH1EeeYj5neBLIRPw" name="concat6Strings">
    <node defType="com.stambia.function.parameter" id="_xxL3ivH1EeeYj5neBLIRPw" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_xxL3i_H1EeeYj5neBLIRPw" name="string2"/>
    <node defType="com.stambia.function.parameter" id="_xxL3jPH1EeeYj5neBLIRPw" name="string3"/>
    <node defType="com.stambia.function.implementation" id="_xxL3jfH1EeeYj5neBLIRPw" name="Concatenation_With_Plus_Operator">
      <attribute defType="com.stambia.function.implementation.productCode" id="_xxL3jvH1EeeYj5neBLIRPw">
        <values>HYPERSONIC_SQL</values>
        <values>MICROSOFT_SQL_SERVER</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_xxL3j_H1EeeYj5neBLIRPw" value="$string1 + $string2 + $string3 + $string4 + $string5 + $string6"/>
    </node>
    <node defType="com.stambia.function.parameter" id="_xxL3kPH1EeeYj5neBLIRPw" name="string4"/>
    <node defType="com.stambia.function.parameter" id="_xxL3kfH1EeeYj5neBLIRPw" name="string5"/>
    <node defType="com.stambia.function.parameter" id="_ze5WgPH1EeeYj5neBLIRPw" name="string6"/>
  </node>
  <node defType="com.stambia.function.function" id="_lrCHkPIBEeeYj5neBLIRPw" name="leftPad">
    <attribute defType="com.stambia.function.function.description" id="_uKwF4PIBEeeYj5neBLIRPw" value="Left padding of a string"/>
    <node defType="com.stambia.function.parameter" id="_mCT_IPIBEeeYj5neBLIRPw" name="stringToPad"/>
    <node defType="com.stambia.function.parameter" id="_xXbdQPIBEeeYj5neBLIRPw" name="intLengthToPad"/>
    <node defType="com.stambia.function.parameter" id="_zvA_sPIBEeeYj5neBLIRPw" name="charToUseInPad"/>
    <node defType="com.stambia.function.implementation" id="_23o9gfIBEeeYj5neBLIRPw" name="lpad">
      <attribute defType="com.stambia.function.implementation.productCode" id="_679HgPIBEeeYj5neBLIRPw">
        <values>HYPERSONIC_SQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="__paVIPIBEeeYj5neBLIRPw" value="lpad($stringToPad,$intLengthToPad,$charToUseInPad)"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_NCSPsP0DEeelPr9Q-FM8CQ" name="concat">
    <attribute defType="com.stambia.function.function.description" id="_UoOD4AJ1EeiL96vAZVh-Mw" value="Concat 2 strings"/>
    <node defType="com.stambia.function.parameter" id="_Nb1_MP0DEeelPr9Q-FM8CQ" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_QLAUIP0DEeelPr9Q-FM8CQ" name="string2"/>
    <node defType="com.stambia.function.implementation" id="_R984cf0DEeelPr9Q-FM8CQ" name="concatenation with plus">
      <attribute defType="com.stambia.function.implementation.productCode" id="_WA4iYP0DEeelPr9Q-FM8CQ">
        <values>HYPERSONIC_SQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_YSxFcP0DEeelPr9Q-FM8CQ" value="$string1 + $string2"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_ZcE24P0DEeelPr9Q-FM8CQ" name="concat">
    <attribute defType="com.stambia.function.function.description" id="_V2QkUAJ1EeiL96vAZVh-Mw" value="Concat 3 strings"/>
    <node defType="com.stambia.function.parameter" id="_ZxaNsP0DEeelPr9Q-FM8CQ" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_cO3uoP0DEeelPr9Q-FM8CQ" name="string2"/>
    <node defType="com.stambia.function.parameter" id="_lHQroP0DEeelPr9Q-FM8CQ" name="string3"/>
    <node defType="com.stambia.function.implementation" id="_mdYKAf0DEeelPr9Q-FM8CQ" name="Concatenation with plus">
      <attribute defType="com.stambia.function.implementation.productCode" id="_pmLG8P0DEeelPr9Q-FM8CQ">
        <values>HYPERSONIC_SQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_sMxVwP0DEeelPr9Q-FM8CQ" value="$string1 + $string2 + $string3"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_aeMYwAJ1EeiL96vAZVh-Mw" name="concat">
    <attribute defType="com.stambia.function.function.description" id="_aeMYwQJ1EeiL96vAZVh-Mw" value="Concat 4 strings"/>
    <node defType="com.stambia.function.parameter" id="_aeMYwgJ1EeiL96vAZVh-Mw" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_aeMYwwJ1EeiL96vAZVh-Mw" name="string2"/>
    <node defType="com.stambia.function.parameter" id="_aeMYxAJ1EeiL96vAZVh-Mw" name="string3"/>
    <node defType="com.stambia.function.implementation" id="_aeMYxQJ1EeiL96vAZVh-Mw" name="Concatenation with plus">
      <attribute defType="com.stambia.function.implementation.productCode" id="_aeMYxgJ1EeiL96vAZVh-Mw">
        <values>HYPERSONIC_SQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_aeMYxwJ1EeiL96vAZVh-Mw" value="$string1 + $string2 + $string3 + $string4"/>
    </node>
    <node defType="com.stambia.function.parameter" id="_bPPbMAJ1EeiL96vAZVh-Mw" name="string4"/>
  </node>
  <node defType="com.stambia.function.function" id="_iXU4OQJ1EeiL96vAZVh-Mw" name="concat">
    <attribute defType="com.stambia.function.function.description" id="_iXU4OgJ1EeiL96vAZVh-Mw" value="Concat 5 strings"/>
    <node defType="com.stambia.function.parameter" id="_iXU4OwJ1EeiL96vAZVh-Mw" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_iXU4PAJ1EeiL96vAZVh-Mw" name="string2"/>
    <node defType="com.stambia.function.parameter" id="_iXU4PQJ1EeiL96vAZVh-Mw" name="string3"/>
    <node defType="com.stambia.function.implementation" id="_iXU4PgJ1EeiL96vAZVh-Mw" name="Concatenation with plus">
      <attribute defType="com.stambia.function.implementation.productCode" id="_iXU4PwJ1EeiL96vAZVh-Mw">
        <values>HYPERSONIC_SQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_iXU4QAJ1EeiL96vAZVh-Mw" value="$string1 + $string2 + $string3 + $string4 + $string5"/>
    </node>
    <node defType="com.stambia.function.parameter" id="_iXU4QQJ1EeiL96vAZVh-Mw" name="string4"/>
    <node defType="com.stambia.function.parameter" id="_i5nF4AJ1EeiL96vAZVh-Mw" name="string5"/>
  </node>
  <node defType="com.stambia.function.function" id="_oXGCQAJ1EeiL96vAZVh-Mw" name="concat">
    <attribute defType="com.stambia.function.function.description" id="_oXGCQQJ1EeiL96vAZVh-Mw" value="Concat 6 strings"/>
    <node defType="com.stambia.function.parameter" id="_oXGCQgJ1EeiL96vAZVh-Mw" name="string1"/>
    <node defType="com.stambia.function.parameter" id="_oXGCQwJ1EeiL96vAZVh-Mw" name="string2"/>
    <node defType="com.stambia.function.parameter" id="_oXGCRAJ1EeiL96vAZVh-Mw" name="string3"/>
    <node defType="com.stambia.function.implementation" id="_oXGCRQJ1EeiL96vAZVh-Mw" name="Concatenation with plus">
      <attribute defType="com.stambia.function.implementation.productCode" id="_oXGCRgJ1EeiL96vAZVh-Mw">
        <values>HYPERSONIC_SQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_oXGCRwJ1EeiL96vAZVh-Mw" value="$string1 + $string2 + $string3 + $string4 + $string5 + $string6"/>
    </node>
    <node defType="com.stambia.function.parameter" id="_oXGCSAJ1EeiL96vAZVh-Mw" name="string4"/>
    <node defType="com.stambia.function.parameter" id="_oXGCSQJ1EeiL96vAZVh-Mw" name="string5"/>
    <node defType="com.stambia.function.parameter" id="_onz-0AJ1EeiL96vAZVh-Mw" name="string6"/>
  </node>
  <node defType="com.stambia.function.function" id="_uTak1QJ3EeiL96vAZVh-Mw" name="databaseFileName">
    <attribute defType="com.stambia.function.function.description" id="_uTak1gJ3EeiL96vAZVh-Mw" value="Returns the file name (without directory information) of the database."/>
    <node defType="com.stambia.function.implementation" id="_uTak1wJ3EeiL96vAZVh-Mw" name="HSQL">
      <attribute defType="com.stambia.function.implementation.productCode" id="_uTak2AJ3EeiL96vAZVh-Mw">
        <values>HYPERSONIC_SQL</values>
        <values>MYSQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_uTak2QJ3EeiL96vAZVh-Mw" value="DATABASE()"/>
    </node>
  </node>
  <node defType="com.stambia.function.function" id="_dNCAxwJ5EeiL96vAZVh-Mw" name="toChar">
    <attribute defType="com.stambia.function.function.description" id="_dNCAyAJ5EeiL96vAZVh-Mw" value="This function formats a datetime or numeric value to the format given in the second argument.&#xD;&#xA; The format string can contain pattern elements from the list given below,&#xD;&#xA; plus punctuation and space characters."/>
    <node defType="com.stambia.function.implementation" id="_dNCAyQJ5EeiL96vAZVh-Mw" name="HSQL">
      <attribute defType="com.stambia.function.implementation.productCode" id="_dNCAygJ5EeiL96vAZVh-Mw">
        <values>HYPERSONIC_SQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_dNCAywJ5EeiL96vAZVh-Mw" value="TO_CHAR($datetime_value_expr,$char_value_expr)"/>
    </node>
    <node defType="com.stambia.function.parameter" id="_dNCAzAJ5EeiL96vAZVh-Mw" name="datetime_value_expr"/>
    <node defType="com.stambia.function.parameter" id="_dNCAzQJ5EeiL96vAZVh-Mw" name="char_value_expr"/>
  </node>
  <node defType="com.stambia.function.function" id="_ie3t1QJ5EeiL96vAZVh-Mw" name="user">
    <attribute defType="com.stambia.function.function.description" id="_ie3t1gJ5EeiL96vAZVh-Mw" value="Equivalent to the SQL function CURRENT_USER. "/>
    <node defType="com.stambia.function.implementation" id="_ie3t1wJ5EeiL96vAZVh-Mw" name="HSQL">
      <attribute defType="com.stambia.function.implementation.productCode" id="_ie3t2AJ5EeiL96vAZVh-Mw">
        <values>HYPERSONIC_SQL</values>
        <values>MYSQL</values>
      </attribute>
      <attribute defType="com.stambia.function.implementation.expression" id="_ie3t2QJ5EeiL96vAZVh-Mw" value="USER()"/>
    </node>
  </node>
</md:node>