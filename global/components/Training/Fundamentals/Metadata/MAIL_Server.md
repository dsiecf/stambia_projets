<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.mail.server" id="_zqCjnuf3EeSyXoFUa3ys7A" name="MailServer" md:ref="platform:/plugin/com.indy.environment/.tech/web/mail.tech#UUID_STAMBIA_TECH_MAIL?fileId=UUID_STAMBIA_TECH_MAIL$type=tech$name=Mail?">
  <node defType="com.stambia.mail.outgoingServer" id="_zqCjn-f3EeSyXoFUa3ys7A" name="SMTP Mail Server">
    <attribute defType="com.stambia.mail.outgoingServer.host" id="_zqCjoOf3EeSyXoFUa3ys7A" value="smtp.gmail.com"/>
    <attribute defType="com.stambia.mail.outgoingServer.port" id="_zqCjoef3EeSyXoFUa3ys7A" value="465"/>
    <attribute defType="com.stambia.mail.outgoingServer.user" id="_zqCjouf3EeSyXoFUa3ys7A" value="jm.souchard@gmail.com"/>
    <attribute defType="com.stambia.mail.outgoingServer.password" id="_zqCjo-f3EeSyXoFUa3ys7A" value="96ACFACAC2328BA67DAAAB702179D0CB"/>
    <attribute defType="com.stambia.mail.outgoingServer.secureProtocol" id="_zqCjpOf3EeSyXoFUa3ys7A" value="SSL"/>
  </node>
  <node defType="com.stambia.mail.mailingList" id="_zqCjpef3EeSyXoFUa3ys7A" name="dwh_mailing_list">
    <attribute defType="com.stambia.mail.mailingList.toTextList" id="_zqIqMOf3EeSyXoFUa3ys7A" value="jm.souchard@gmail.com"/>
    <attribute defType="com.stambia.mail.mailingList.toRef" id="_zqIqMef3EeSyXoFUa3ys7A"/>
    <attribute defType="com.stambia.mail.mailingList.toText" id="_zqIqMuf3EeSyXoFUa3ys7A"/>
  </node>
  <node defType="com.stambia.mail.mailingList" id="_zqIqM-f3EeSyXoFUa3ys7A" name="gen_mailing_list">
    <attribute defType="com.stambia.mail.mailingList.toTextList" id="_zqIqNOf3EeSyXoFUa3ys7A" value="jm.souchard@gmail.com"/>
  </node>
  <node defType="com.stambia.mail.message" id="_zqIqNef3EeSyXoFUa3ys7A" name="Information_message">
    <attribute defType="com.stambia.mail.message.toRef" id="__ok3gOzsEeSNRrizoJ3Gug">
      <refs>#_zqCjpef3EeSyXoFUa3ys7A?fileId=_zqCjnuf3EeSyXoFUa3ys7A$type=md$name=dwh_mailing_list?</refs>
    </attribute>
    <attribute defType="com.stambia.mail.message.subject" id="_TmeT4OzuEeSNRrizoJ3Gug" value=""/>
    <attribute defType="com.stambia.mail.message.outgoingServer" id="_6_0cEO2OEeSNRrizoJ3Gug" ref="#_zqCjn-f3EeSyXoFUa3ys7A?fileId=_zqCjnuf3EeSyXoFUa3ys7A$type=md$name=SMTP%20Mail%20Server?"/>
    <attribute defType="com.stambia.mail.message.senderText" id="_xccIUGw1EeWqu86E0Wk1nA" value="jm.souchard@gmail.com"/>
  </node>
  <node defType="com.stambia.mail.incomingAccount" id="_rv3qcdVZEeW4fKRPl2tywA" name="Pop Incoming Account">
    <attribute defType="com.stambia.mail.incomingAccount.protocol" id="_5jNegNVZEeW4fKRPl2tywA" value="pop3"/>
    <attribute defType="com.stambia.mail.incomingAccount.host" id="_6Utz8NVZEeW4fKRPl2tywA" value="pop.gmail.com"/>
    <attribute defType="com.stambia.mail.incomingAccount.port" id="_uj_bENVdEeW4fKRPl2tywA" value="995"/>
    <attribute defType="com.stambia.mail.incomingAccount.user" id="_xJZ-INVdEeW4fKRPl2tywA" value="jm.souchard@gmail.com"/>
    <attribute defType="com.stambia.mail.incomingAccount.password" id="_0FwagNVdEeW4fKRPl2tywA" value="96ACFACAC2328BA67DAAAB702179D0CB"/>
    <attribute defType="com.stambia.mail.incomingAccount.secureProtocol" id="_0IBO4NVeEeW4fKRPl2tywA" value="SSL"/>
    <attribute defType="com.stambia.mail.incomingAccount.address" id="_7P_hsNVfEeW4fKRPl2tywA" value="jm.souchard@gmail.com"/>
    <attribute defType="com.stambia.mail.incomingAccount.displayName" id="_8L7lgNVfEeW4fKRPl2tywA" value="mail perso"/>
    <attribute defType="com.stambia.mail.incomingAccount.useSecureAuthentification" id="_YYm2oNVgEeW4fKRPl2tywA" value="true"/>
  </node>
  <node defType="com.stambia.mail.incomingAccount" id="_ofQZUZxOEea_J8zHTqyYhw" name="Imap Incoming Account">
    <attribute defType="com.stambia.mail.incomingAccount.address" id="_0Gb30JxOEea_J8zHTqyYhw" value="jm.souchard@gmail.com"/>
    <attribute defType="com.stambia.mail.incomingAccount.displayName" id="_03p5YJxOEea_J8zHTqyYhw" value="Mail perso"/>
    <attribute defType="com.stambia.mail.incomingAccount.protocol" id="_1Q3qoJxOEea_J8zHTqyYhw" value="imap"/>
    <attribute defType="com.stambia.mail.incomingAccount.host" id="_3am-UJxOEea_J8zHTqyYhw" value="imap.gmail.com"/>
    <attribute defType="com.stambia.mail.incomingAccount.port" id="_3-9okJxOEea_J8zHTqyYhw" value="993"/>
    <attribute defType="com.stambia.mail.incomingAccount.user" id="_8RAskJxOEea_J8zHTqyYhw" value="jm.souchard@gmail.com"/>
    <attribute defType="com.stambia.mail.incomingAccount.password" id="_A2Uq0JxPEea_J8zHTqyYhw" value="96ACFACAC2328BA67DAAAB702179D0CB"/>
  </node>
</md:node>