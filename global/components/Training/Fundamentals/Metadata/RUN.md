<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.runtime" id="_EVptcOZfEee4xLxZ5pzwCQ" md:ref="platform:/plugin/com.indy.environment/.tech/others/runtime.tech#UUID_TECH_RUNTIME?fileId=UUID_TECH_RUNTIME$type=tech$name=Stambia%20Runtime?">
  <node defType="com.stambia.runtime.engine" id="_vfF1APVWEeesOOULJSn_7g" name="Localhost">
    <attribute defType="com.stambia.runtime.engine.host" id="_z0dSwPVWEeesOOULJSn_7g" value="localhost"/>
    <attribute defType="com.stambia.runtime.engine.port" id="_0SM-sPVWEeesOOULJSn_7g" value="42000"/>
  </node>
</md:node>