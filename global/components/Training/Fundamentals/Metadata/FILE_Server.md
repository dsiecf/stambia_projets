<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.file.server" id="_YVen0OYxEee4xLxZ5pzwCQ" md:ref="platform:/plugin/com.indy.environment/.tech/file/standard.file.md#UUID_TECH_FILE_MD?fileId=UUID_TECH_FILE_MD$type=md$name=File?">
  <node defType="com.stambia.file.directory" id="_Ya23EOYxEee4xLxZ5pzwCQ" name="Reference_Files_Folder">
    <attribute defType="com.stambia.file.directory.path" id="_YbEScOYxEee4xLxZ5pzwCQ" value="C:\app_dev\Used_Files\In_Files\Reference_Files\Default"/>
    <node defType="com.stambia.file.file" id="_YbGHoOYxEee4xLxZ5pzwCQ" name="DiscountRanges">
      <attribute defType="com.stambia.file.file.type" id="_Ybvn4OYxEee4xLxZ5pzwCQ" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_YbyEIOYxEee4xLxZ5pzwCQ"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_YbyrMOYxEee4xLxZ5pzwCQ" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_YbzSQOYxEee4xLxZ5pzwCQ" value="2C"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_YbzSQeYxEee4xLxZ5pzwCQ"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_Ybz5UOYxEee4xLxZ5pzwCQ" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_Yb0gYOYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_Yb1HcOYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_Yb1ugOYxEee4xLxZ5pzwCQ" value="1"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_dehlQOYxEee4xLxZ5pzwCQ" value="DiscountRanges.txt"/>
      <node defType="com.stambia.file.field" id="_fB_jJOYxEee4xLxZ5pzwCQ" name="max" position="2">
        <attribute defType="com.stambia.file.field.size" id="_fB_jJeYxEee4xLxZ5pzwCQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_fB_jJuYxEee4xLxZ5pzwCQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_fB_jJ-YxEee4xLxZ5pzwCQ" value="MAX"/>
      </node>
      <node defType="com.stambia.file.field" id="_fB_jKOYxEee4xLxZ5pzwCQ" name="range" position="3">
        <attribute defType="com.stambia.file.field.size" id="_fB_jKeYxEee4xLxZ5pzwCQ" value="62"/>
        <attribute defType="com.stambia.file.field.type" id="_fB_jKuYxEee4xLxZ5pzwCQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_fB_jK-YxEee4xLxZ5pzwCQ" value="RANGE"/>
      </node>
      <node defType="com.stambia.file.field" id="_fB_jIOYxEee4xLxZ5pzwCQ" name="min" position="1">
        <attribute defType="com.stambia.file.field.size" id="_fB_jIeYxEee4xLxZ5pzwCQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_fB_jIuYxEee4xLxZ5pzwCQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_fB_jI-YxEee4xLxZ5pzwCQ" value="MIN"/>
      </node>
    </node>
    <node defType="com.stambia.file.file" id="_j6aqoOYxEee4xLxZ5pzwCQ" name="US_States">
      <attribute defType="com.stambia.file.file.type" id="_j7dzgOYxEee4xLxZ5pzwCQ" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_j7gPwOYxEee4xLxZ5pzwCQ"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_j7g20OYxEee4xLxZ5pzwCQ" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_j7hd4OYxEee4xLxZ5pzwCQ" value="2C"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_j7hd4eYxEee4xLxZ5pzwCQ"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_j7iE8OYxEee4xLxZ5pzwCQ" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_j7iE8eYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_j7isAOYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_j7isAeYxEee4xLxZ5pzwCQ" value="1"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_ks8TwOYxEee4xLxZ5pzwCQ" value="REF_US_STATES.csv"/>
      <node defType="com.stambia.file.field" id="_nY9OEOYxEee4xLxZ5pzwCQ" name="STATE_UPPER_CASE" position="1">
        <attribute defType="com.stambia.file.field.size" id="_nY91IOYxEee4xLxZ5pzwCQ" value="64"/>
        <attribute defType="com.stambia.file.field.type" id="_nY91IeYxEee4xLxZ5pzwCQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_nY91IuYxEee4xLxZ5pzwCQ" value="STATE_UPPER_CASE"/>
      </node>
      <node defType="com.stambia.file.field" id="_nY91I-YxEee4xLxZ5pzwCQ" name="STATE" position="2">
        <attribute defType="com.stambia.file.field.size" id="_nY91JOYxEee4xLxZ5pzwCQ" value="64"/>
        <attribute defType="com.stambia.file.field.type" id="_nY91JeYxEee4xLxZ5pzwCQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_nY91JuYxEee4xLxZ5pzwCQ" value="STATE"/>
      </node>
      <node defType="com.stambia.file.field" id="_nY91J-YxEee4xLxZ5pzwCQ" name="STATE_CODE" position="3">
        <attribute defType="com.stambia.file.field.size" id="_nY91KOYxEee4xLxZ5pzwCQ" value="52"/>
        <attribute defType="com.stambia.file.field.type" id="_nY91KeYxEee4xLxZ5pzwCQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_nY91KuYxEee4xLxZ5pzwCQ" value="STATE_CODE"/>
      </node>
    </node>
    <node defType="com.stambia.file.file" id="_ost8AOYxEee4xLxZ5pzwCQ" name="US_Cities">
      <attribute defType="com.stambia.file.file.type" id="_otcUwOYxEee4xLxZ5pzwCQ" value="POSITIONAL"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_otexAOYxEee4xLxZ5pzwCQ"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_otfYEOYxEee4xLxZ5pzwCQ" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_otfYEeYxEee4xLxZ5pzwCQ" value="3B"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_otf_IOYxEee4xLxZ5pzwCQ"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_otf_IeYxEee4xLxZ5pzwCQ" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_otgmMOYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_otgmMeYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_otgmMuYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_pV5b4OYxEee4xLxZ5pzwCQ" value="ref_us_cities.txt"/>
      <attribute defType="com.stambia.file.file.nameHelper" id="_4d2ScOYxEee4xLxZ5pzwCQ" value="ZIP_CODE;CITY;STATE_CODE"/>
      <attribute defType="com.stambia.file.file.positionHelper" id="_4d2SceYxEee4xLxZ5pzwCQ" value="1;6;78"/>
      <attribute defType="com.stambia.file.file.sizeHelper" id="_4d25gOYxEee4xLxZ5pzwCQ" value="5;72;10"/>
      <attribute defType="com.stambia.file.file.decimalHelper" id="_4d25geYxEee4xLxZ5pzwCQ"/>
      <attribute defType="com.stambia.file.file.typeHelper" id="_4d3gkOYxEee4xLxZ5pzwCQ" value="String;String;String"/>
      <attribute defType="com.stambia.file.file.formatHelper" id="_4d3gkeYxEee4xLxZ5pzwCQ"/>
      <node defType="com.stambia.file.field" id="_4nwcZ-YxEee4xLxZ5pzwCQ" name="STATE_CODE" position="78">
        <attribute defType="com.stambia.file.field.size" id="_4nwcaOYxEee4xLxZ5pzwCQ" value="10"/>
        <attribute defType="com.stambia.file.field.type" id="_4nwcaeYxEee4xLxZ5pzwCQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_4nwcauYxEee4xLxZ5pzwCQ" value="STATE_CODE"/>
      </node>
      <node defType="com.stambia.file.field" id="_4nwcY-YxEee4xLxZ5pzwCQ" name="CITY" position="6">
        <attribute defType="com.stambia.file.field.size" id="_4nwcZOYxEee4xLxZ5pzwCQ" value="72"/>
        <attribute defType="com.stambia.file.field.type" id="_4nwcZeYxEee4xLxZ5pzwCQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_4nwcZuYxEee4xLxZ5pzwCQ" value="CITY"/>
      </node>
      <node defType="com.stambia.file.field" id="_4nv1UOYxEee4xLxZ5pzwCQ" name="ZIP_CODE" position="1">
        <attribute defType="com.stambia.file.field.size" id="_4nwcYOYxEee4xLxZ5pzwCQ" value="5"/>
        <attribute defType="com.stambia.file.field.type" id="_4nwcYeYxEee4xLxZ5pzwCQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_4nwcYuYxEee4xLxZ5pzwCQ" value="ZIP_CODE"/>
      </node>
    </node>
    <node defType="com.stambia.file.file" id="_6c-RkOYxEee4xLxZ5pzwCQ" name="Time">
      <attribute defType="com.stambia.file.file.type" id="_6dvtoOYxEee4xLxZ5pzwCQ" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_6dyJ4OYxEee4xLxZ5pzwCQ"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_6dyw8OYxEee4xLxZ5pzwCQ" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_6dzYAOYxEee4xLxZ5pzwCQ" value="3B"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_6dzYAeYxEee4xLxZ5pzwCQ"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_6dz_EOYxEee4xLxZ5pzwCQ" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_6dz_EeYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_6d0mIOYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_6d0mIeYxEee4xLxZ5pzwCQ" value="0"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_63H28OYxEee4xLxZ5pzwCQ" value="Time.csv"/>
      <node defType="com.stambia.file.field" id="_7u3XQOYxEee4xLxZ5pzwCQ" name="DAY_DATE" position="1">
        <attribute defType="com.stambia.file.field.size" id="_7u3XQeYxEee4xLxZ5pzwCQ" value="66"/>
        <attribute defType="com.stambia.file.field.type" id="_7u3XQuYxEee4xLxZ5pzwCQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_7u3XQ-YxEee4xLxZ5pzwCQ" value="DAY_DATE"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.file.directory" id="_Wa6pIPIlEeeYj5neBLIRPw" name="Statistic_Report_Folder">
    <attribute defType="com.stambia.file.directory.path" id="_WbJ5sPIlEeeYj5neBLIRPw" value="C:\app_dev\Used_Files\Out_Files\Statistic_Report"/>
  </node>
</md:node>