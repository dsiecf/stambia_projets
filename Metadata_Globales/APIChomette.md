<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.wsdl.wsdl" id="_4baAADCxEeq3399qC-CuVg" name="Mywebservices" md:ref="platform:/plugin/com.indy.environment/technology/web/wsdl.tech#UUID_TECH_WSDL1?fileId=UUID_TECH_WSDL1$type=tech$name=wsdl?">
  <attribute defType="com.stambia.wsdl.wsdl.xsdReverseVersion" id="_4bjJ8DCxEeq3399qC-CuVg" value="1"/>
  <attribute defType="com.stambia.wsdl.wsdl.url" id="_4jgzIDCxEeq3399qC-CuVg"/>
  <node defType="com.stambia.wsdl.service" id="_F1BsgDCyEeq3399qC-CuVg" name="openChomettehttpApi">
    <node defType="com.stambia.wsdl.port" id="_J5H0EDCyEeq3399qC-CuVg" name="Company">
      <attribute defType="com.stambia.wsdl.port.address" id="_ZhBWADCyEeq3399qC-CuVg" value="https://preprod.chomette.com/rest/V1/kdcApi/company/import"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_ZyECsDCyEeq3399qC-CuVg" value="HTTP"/>
      <attribute defType="com.stambia.wsdl.port.verb" id="_aweLsDCyEeq3399qC-CuVg" value="POST"/>
      <node defType="com.stambia.wsdl.operation" id="_2k4GwDCyEeq3399qC-CuVg" name="postCompany">
        <attribute defType="com.stambia.wsdl.operation.httpHeaderProperties" id="_l4_PoDFfEeqF3cPVFXUsDw" value="Content-Type: application-json"/>
        <attribute defType="com.stambia.wsdl.operation.address" id="_8ZkuYDPVEeqF3cPVFXUsDw" value="/"/>
        <node defType="com.stambia.wsdl.input" id="_wBZuQDFYEeqF3cPVFXUsDw">
          <node defType="com.stambia.wsdl.part" id="_pjmgMDFfEeqF3cPVFXUsDw" name="Authorization">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_t9ss8DFfEeqF3cPVFXUsDw" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_veRH0DFfEeqF3cPVFXUsDw" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_LrrngDFaEeqF3cPVFXUsDw" name="content">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_V2OI4DFaEeqF3cPVFXUsDw" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_WosusDFaEeqF3cPVFXUsDw" value="application/json"/>
            <node defType="com.stambia.json.rootObject" id="_wQFpZTLrEeqF3cPVFXUsDw" name="wsSocietes">
              <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_2aD4UDLrEeqF3cPVFXUsDw" value="C:\app_dev\Used_Files\In_Files\Json\wsSocietes.json"/>
              <node defType="com.stambia.json.object" id="_4ktyATLrEeqF3cPVFXUsDw" name="information" position="1">
                <node defType="com.stambia.json.array" id="_4ktyAjLrEeqF3cPVFXUsDw" name="data" position="1">
                  <node defType="com.stambia.json.object" id="_4ktyAzLrEeqF3cPVFXUsDw" name="item" position="1">
                    <node defType="com.stambia.json.object" id="_4ktyBDLrEeqF3cPVFXUsDw" name="identification" position="1">
                      <node defType="com.stambia.json.value" id="_4ktyBTLrEeqF3cPVFXUsDw" name="website_id" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyBjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyBzLrEeqF3cPVFXUsDw" name="store_id" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyCDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyCTLrEeqF3cPVFXUsDw" name="id_client_chomette" position="3">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyCjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyCzLrEeqF3cPVFXUsDw" name="id_societe_magento" position="4">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyDDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyDTLrEeqF3cPVFXUsDw" name="email_admin" position="5">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyDjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyDzLrEeqF3cPVFXUsDw" name="email_societe" position="6">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyEDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyETLrEeqF3cPVFXUsDw" name="email_bis" position="7">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyEjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyEzLrEeqF3cPVFXUsDw" name="commercial" position="8">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyFDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyFTLrEeqF3cPVFXUsDw" name="tarif" position="9">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyFjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyFzLrEeqF3cPVFXUsDw" name="raison_sociale" position="10">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyGDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyGTLrEeqF3cPVFXUsDw" name="enseigne" position="11">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyGjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyGzLrEeqF3cPVFXUsDw" name="nom" position="12">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyHDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyHTLrEeqF3cPVFXUsDw" name="prenom" position="13">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyHjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyHzLrEeqF3cPVFXUsDw" name="rue1" position="14">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyIDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyITLrEeqF3cPVFXUsDw" name="rue2" position="15">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyIjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyIzLrEeqF3cPVFXUsDw" name="ville" position="16">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyJDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyJTLrEeqF3cPVFXUsDw" name="pays" position="17">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyJjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyJzLrEeqF3cPVFXUsDw" name="code_postal" position="18">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyKDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyKTLrEeqF3cPVFXUsDw" name="tel" position="19">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyKjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_lPXJ45FxEeuZUfdHLPu9yA" name="status" position="20">
                        <attribute defType="com.stambia.json.value.type" id="_lPXJ5JFxEeuZUfdHLPu9yA" value="number"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_4ktyKzLrEeqF3cPVFXUsDw" name="livraison" position="2">
                      <node defType="com.stambia.json.value" id="_4ktyLDLrEeqF3cPVFXUsDw" name="rue1" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyLTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyLjLrEeqF3cPVFXUsDw" name="rue2" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyLzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyMDLrEeqF3cPVFXUsDw" name="ville" position="3">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyMTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyMjLrEeqF3cPVFXUsDw" name="pays" position="4">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyMzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyNDLrEeqF3cPVFXUsDw" name="code_postal" position="5">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyNTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyNjLrEeqF3cPVFXUsDw" name="tel" position="6">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyNzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_4ktyODLrEeqF3cPVFXUsDw" name="facturation" position="3">
                      <node defType="com.stambia.json.value" id="_4ktyOTLrEeqF3cPVFXUsDw" name="rue1" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyOjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyOzLrEeqF3cPVFXUsDw" name="rue2" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyPDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyPTLrEeqF3cPVFXUsDw" name="ville" position="3">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyPjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyPzLrEeqF3cPVFXUsDw" name="pays" position="4">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyQDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyQTLrEeqF3cPVFXUsDw" name="code_postal" position="5">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyQjLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyQzLrEeqF3cPVFXUsDw" name="tel" position="6">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyRDLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_4ktyRTLrEeqF3cPVFXUsDw" name="activite" position="4">
                      <node defType="com.stambia.json.value" id="_4ktyRjLrEeqF3cPVFXUsDw" name="siret" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyRzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktySDLrEeqF3cPVFXUsDw" name="code_client" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_4ktySTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktySjLrEeqF3cPVFXUsDw" name="mercuriale" position="3">
                        <attribute defType="com.stambia.json.value.type" id="_4ktySzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyTDLrEeqF3cPVFXUsDw" name="centralisateur_gescom" position="4">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyTTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyTjLrEeqF3cPVFXUsDw" name="chaine_gescom" position="5">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyTzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyUDLrEeqF3cPVFXUsDw" name="groupe_gescom" position="6">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyUTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyUjLrEeqF3cPVFXUsDw" name="fax" position="7">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyUzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyVDLrEeqF3cPVFXUsDw" name="tva_intracomm" position="8">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyVTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyVjLrEeqF3cPVFXUsDw" name="type_etab_1" position="9">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyVzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyWDLrEeqF3cPVFXUsDw" name="type_etab_2" position="10">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyWTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyWjLrEeqF3cPVFXUsDw" name="type_etab_3" position="11">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyWzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyXDLrEeqF3cPVFXUsDw" name="specialites_culinaires" position="12">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyXTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyXjLrEeqF3cPVFXUsDw" name="niveau_standing" position="13">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyXzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyYDLrEeqF3cPVFXUsDw" name="repas_servis" position="14">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyYTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyYjLrEeqF3cPVFXUsDw" name="prestations" position="15">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyYzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyZDLrEeqF3cPVFXUsDw" name="langue_principale" position="16">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyZTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyZjLrEeqF3cPVFXUsDw" name="nb_couverts_jour" position="17">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyZzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyaDLrEeqF3cPVFXUsDw" name="nb_repas_jour" position="18">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyaTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyajLrEeqF3cPVFXUsDw" name="nb_couchages" position="19">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyazLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktybDLrEeqF3cPVFXUsDw" name="nb_lits" position="20">
                        <attribute defType="com.stambia.json.value.type" id="_4ktybTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktybjLrEeqF3cPVFXUsDw" name="nb_eleves" position="21">
                        <attribute defType="com.stambia.json.value.type" id="_4ktybzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktycDLrEeqF3cPVFXUsDw" name="type_compte" position="22">
                        <attribute defType="com.stambia.json.value.type" id="_4ktycTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktycjLrEeqF3cPVFXUsDw" name="num_adherent" position="23">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyczLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktydDLrEeqF3cPVFXUsDw" name="env_tourist" position="24">
                        <attribute defType="com.stambia.json.value.type" id="_4ktydTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktydjLrEeqF3cPVFXUsDw" name="annees_existence" position="25">
                        <attribute defType="com.stambia.json.value.type" id="_4ktydzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyeDLrEeqF3cPVFXUsDw" name="scoring_ca" position="26">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyeTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyejLrEeqF3cPVFXUsDw" name="statut_client" position="27">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyezLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyfDLrEeqF3cPVFXUsDw" name="niveau_fidelite" position="28">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyfTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyfjLrEeqF3cPVFXUsDw" name="comportement" position="29">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyfzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktygDLrEeqF3cPVFXUsDw" name="demat_factures" position="30">
                        <attribute defType="com.stambia.json.value.type" id="_4ktygTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktygjLrEeqF3cPVFXUsDw" name="email_fact_demat" position="31">
                        <attribute defType="com.stambia.json.value.type" id="_4ktygzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyhDLrEeqF3cPVFXUsDw" name="jours_d_ouverture" position="32">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyhTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyhjLrEeqF3cPVFXUsDw" name="couleur" position="33">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyhzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyiDLrEeqF3cPVFXUsDw" name="mode_paiement" position="34">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyiTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyijLrEeqF3cPVFXUsDw" name="frais_de_port" position="35">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyizLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyjDLrEeqF3cPVFXUsDw" name="free_shipping" position="36">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyjTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktyjjLrEeqF3cPVFXUsDw" name="minimum_commande" position="37">
                        <attribute defType="com.stambia.json.value.type" id="_4ktyjzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktykDLrEeqF3cPVFXUsDw" name="type_livraison" position="38">
                        <attribute defType="com.stambia.json.value.type" id="_4ktykTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktykjLrEeqF3cPVFXUsDw" name="instruction_livraison" position="39">
                        <attribute defType="com.stambia.json.value.type" id="_4ktykzLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_4ktylDLrEeqF3cPVFXUsDw" name="reference_commande_obligatoire" position="40">
                        <attribute defType="com.stambia.json.value.type" id="_4ktylTLrEeqF3cPVFXUsDw" value="string"/>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="_YuCCkTMSEeqF3cPVFXUsDw">
          <node defType="com.stambia.wsdl.part" id="_aqgWUDMSEeqF3cPVFXUsDw" name="result">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_CTxmkDMTEeqF3cPVFXUsDw" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_Dl_cYDMTEeqF3cPVFXUsDw" value="application/json"/>
            <node defType="com.stambia.json.rootArray" id="_MbG-gDYqEeqF3cPVFXUsDw" name="wsretsoc">
              <attribute defType="com.stambia.json.rootArray.encoding" id="_MbuCgDYqEeqF3cPVFXUsDw" value="UTF-8"/>
              <attribute defType="com.stambia.json.rootArray.reverseURLPath" id="_MbxF0DYqEeqF3cPVFXUsDw"/>
              <attribute defType="com.stambia.json.rootArray.reverseFilePath" id="_MbxF0TYqEeqF3cPVFXUsDw" value="C:\app_dev\Used_Files\In_Files\Json\wsretsoc.json"/>
              <node defType="com.stambia.json.object" id="_Tt2zYTYqEeqF3cPVFXUsDw" name="item" position="1">
                <node defType="com.stambia.json.value" id="_Tt2zYjYqEeqF3cPVFXUsDw" name="id_societe_chomette" position="1">
                  <attribute defType="com.stambia.json.value.type" id="_Tt2zYzYqEeqF3cPVFXUsDw" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_Tt2zZDYqEeqF3cPVFXUsDw" name="id_societe_magento" position="2">
                  <attribute defType="com.stambia.json.value.type" id="_Tt2zZTYqEeqF3cPVFXUsDw" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_9kWRRja7EeqF3cPVFXUsDw" name="statut" position="3">
                  <attribute defType="com.stambia.json.value.type" id="_9kWRRza7EeqF3cPVFXUsDw" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_9kWRSDa7EeqF3cPVFXUsDw" name="Message" position="4">
                  <attribute defType="com.stambia.json.value.type" id="_9kWRSTa7EeqF3cPVFXUsDw" value="string"/>
                </node>
              </node>
            </node>
          </node>
          <node defType="com.stambia.wsdl.part" id="_QI58YDYeEeqF3cPVFXUsDw" name="response-code">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_pKuloDYfEeqF3cPVFXUsDw" value="http:responseCode"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_q8AWADYfEeqF3cPVFXUsDw" value="integer"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_XRSgoDaoEeqF3cPVFXUsDw" name="response-msg">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_Zivl4DaoEeqF3cPVFXUsDw" value="http:responseMessage"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_aiCfsDaoEeqF3cPVFXUsDw" value="string"/>
          </node>
        </node>
      </node>
    </node>
    <node defType="com.stambia.wsdl.port" id="_nybfoDIyEeqF3cPVFXUsDw" name="Customer">
      <attribute defType="com.stambia.wsdl.port.address" id="_qWO5IDIyEeqF3cPVFXUsDw" value="https://preprod.chomette.com/rest/V1/kdcApi/customer/import"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_qrYpwDIyEeqF3cPVFXUsDw" value="HTTP"/>
      <attribute defType="com.stambia.wsdl.port.verb" id="_rud3YDIyEeqF3cPVFXUsDw" value="POST"/>
      <node defType="com.stambia.wsdl.operation" id="_tYRGwDIyEeqF3cPVFXUsDw" name="postCustomer">
        <attribute defType="com.stambia.wsdl.operation.httpHeaderProperties" id="_xvlFQDIyEeqF3cPVFXUsDw" value="Content-Type: application-json"/>
        <attribute defType="com.stambia.wsdl.operation.address" id="_-nA2UDPVEeqF3cPVFXUsDw" value="/"/>
        <node defType="com.stambia.wsdl.input" id="_z6VsoDIyEeqF3cPVFXUsDw">
          <node defType="com.stambia.wsdl.part" id="_1hWtwDIyEeqF3cPVFXUsDw" name="Authorization">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_4x4DUDIyEeqF3cPVFXUsDw" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_51s30DIyEeqF3cPVFXUsDw" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_2T3IwDIyEeqF3cPVFXUsDw" name="content">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_8szn0DIyEeqF3cPVFXUsDw" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_90SNQDIyEeqF3cPVFXUsDw" value="application/json"/>
            <node defType="com.stambia.json.rootObject" id="_8dhPlTLrEeqF3cPVFXUsDw" name="wsClients">
              <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_-KHUQDLrEeqF3cPVFXUsDw" value="C:\app_dev\Used_Files\In_Files\Json\wsClients.json"/>
              <node defType="com.stambia.json.object" id="_ASL0ATLsEeqF3cPVFXUsDw" name="information" position="1">
                <node defType="com.stambia.json.array" id="_ASL0AjLsEeqF3cPVFXUsDw" name="data" position="1">
                  <node defType="com.stambia.json.object" id="_ASL0AzLsEeqF3cPVFXUsDw" name="item" position="1">
                    <node defType="com.stambia.json.object" id="_ASL0BDLsEeqF3cPVFXUsDw" name="identification" position="1">
                      <node defType="com.stambia.json.value" id="_ASL0BTLsEeqF3cPVFXUsDw" name="website_id" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0BjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0BzLsEeqF3cPVFXUsDw" name="store_id" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0CDLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0CTLsEeqF3cPVFXUsDw" name="id_client_magento" position="3">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0CjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0CzLsEeqF3cPVFXUsDw" name="id_societe_magento" position="4">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0DDLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0DTLsEeqF3cPVFXUsDw" name="id_client_chomette" position="5">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0DjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0DzLsEeqF3cPVFXUsDw" name="id_societe_chomette" position="6">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0EDLsEeqF3cPVFXUsDw" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0ETLsEeqF3cPVFXUsDw" name="type" position="7">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0EjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0EzLsEeqF3cPVFXUsDw" name="action" position="8">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0FDLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0FTLsEeqF3cPVFXUsDw" name="max_order" position="9">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0FjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0FzLsEeqF3cPVFXUsDw" name="autorisation_order" position="10">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0GDLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0GTLsEeqF3cPVFXUsDw" name="civilite" position="11">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0GjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0GzLsEeqF3cPVFXUsDw" name="nom" position="12">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0HDLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0HTLsEeqF3cPVFXUsDw" name="prenom" position="13">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0HjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0HzLsEeqF3cPVFXUsDw" name="email" position="14">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0IDLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0ITLsEeqF3cPVFXUsDw" name="password" position="15">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0IjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0IzLsEeqF3cPVFXUsDw" name="telephone" position="16">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0JDLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0JTLsEeqF3cPVFXUsDw" name="mobile" position="17">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0JjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0JzLsEeqF3cPVFXUsDw" name="intitule_poste" position="18">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0KDLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0KTLsEeqF3cPVFXUsDw" name="is_shipping_change" position="19">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0KjLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_ASL0KzLsEeqF3cPVFXUsDw" name="is_billing_change" position="20">
                        <attribute defType="com.stambia.json.value.type" id="_ASL0LDLsEeqF3cPVFXUsDw" value="string"/>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>