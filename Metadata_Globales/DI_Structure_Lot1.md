<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.file.server" id="_SJ3B0BBeEeqPGbHwfkinWw" md:ref="platform:/plugin/com.indy.environment/technology/file/standard.file.md#UUID_TECH_FILE_MD?fileId=UUID_TECH_FILE_MD$type=md$name=File?">
  <node defType="com.stambia.file.directory" id="_STHEcBBeEeqPGbHwfkinWw" name="Export.csv">
    <attribute defType="com.stambia.file.directory.path" id="_ST7j0BBeEeqPGbHwfkinWw" value="\\servernt46\Stambia\DI\Export"/>
    <node defType="com.stambia.file.file" id="_xctkQRBfEeqPGbHwfkinWw" name="DI_Export">
      <attribute defType="com.stambia.file.file.type" id="_zEWQcBBfEeqPGbHwfkinWw" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_zEZ60RBfEeqPGbHwfkinWw" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_zEZ60hBfEeqPGbHwfkinWw" value="3B"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_zEah4BBfEeqPGbHwfkinWw"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_zEah4RBfEeqPGbHwfkinWw" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_zEbI8BBfEeqPGbHwfkinWw" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_zEbI8RBfEeqPGbHwfkinWw" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_zEbwABBfEeqPGbHwfkinWw" value="1"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_z0FEQBBfEeqPGbHwfkinWw" value="DI_Structure_lot1.csv"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_j-fHwBLUEeqPGbHwfkinWw" value="UTF-8"/>
      <node defType="com.stambia.file.field" id="_3GFofBBfEeqPGbHwfkinWw" name="CA Facture 3DM" position="61">
        <attribute defType="com.stambia.file.field.size" id="_3GFofRBfEeqPGbHwfkinWw" value="64"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFofhBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFofxBfEeqPGbHwfkinWw" value="F61"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFomBBfEeqPGbHwfkinWw" name="CA 24DM" position="68">
        <attribute defType="com.stambia.file.field.size" id="_3GFomRBfEeqPGbHwfkinWw" value="58"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFomhBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFomxBfEeqPGbHwfkinWw" value="F68"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoJBBfEeqPGbHwfkinWw" name="Nb Couverts/j" position="39">
        <attribute defType="com.stambia.file.field.size" id="_3GFoJRBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoJhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoJxBfEeqPGbHwfkinWw" value="F39"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnrBBfEeqPGbHwfkinWw" name="Segment" position="9">
        <attribute defType="com.stambia.file.field.size" id="_3GFnrRBfEeqPGbHwfkinWw" value="35"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnrhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnrxBfEeqPGbHwfkinWw" value="F9"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn7BBfEeqPGbHwfkinWw" name="Nom Responsable" position="25">
        <attribute defType="com.stambia.file.field.size" id="_3GFn7RBfEeqPGbHwfkinWw" value="40"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn7hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn7xBfEeqPGbHwfkinWw" value="F25"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnuBBfEeqPGbHwfkinWw" name="Enseigne" position="12">
        <attribute defType="com.stambia.file.field.size" id="_3GFnuRBfEeqPGbHwfkinWw" value="40"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnuhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnuxBfEeqPGbHwfkinWw" value="F12"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoVBBfEeqPGbHwfkinWw" name="CptChomette" position="51">
        <attribute defType="com.stambia.file.field.size" id="_3GFoVRBfEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoVhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoVxBfEeqPGbHwfkinWw" value="F51"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoeBBfEeqPGbHwfkinWw" name="CA Facture Cmd HT" position="60">
        <attribute defType="com.stambia.file.field.size" id="_3GFoeRBfEeqPGbHwfkinWw" value="81"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoehBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoexBfEeqPGbHwfkinWw" value="F60"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoHBBfEeqPGbHwfkinWw" name="TypeEtab2" position="37">
        <attribute defType="com.stambia.file.field.size" id="_3GFoHRBfEeqPGbHwfkinWw" value="100"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoHhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoHxBfEeqPGbHwfkinWw" value="F37"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoZBBfEeqPGbHwfkinWw" name="MailUserAdmin" position="55">
        <attribute defType="com.stambia.file.field.size" id="_3GFoZRBfEeqPGbHwfkinWw" value="250"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoZhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoZxBfEeqPGbHwfkinWw" value="F55"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFooBBfEeqPGbHwfkinWw" name="Eligible Promo" position="70">
        <attribute defType="com.stambia.file.field.size" id="_3GFooRBfEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoohBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFooxBfEeqPGbHwfkinWw" value="F70"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn9BBfEeqPGbHwfkinWw" name="Optin" position="27">
        <attribute defType="com.stambia.file.field.size" id="_3GFn9RBfEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn9hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn9xBfEeqPGbHwfkinWw" value="F27"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFntBBfEeqPGbHwfkinWw" name="Multicompte" position="11">
        <attribute defType="com.stambia.file.field.size" id="_3GFntRBfEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnthBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFntxBfEeqPGbHwfkinWw" value="F11"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFocBBfEeqPGbHwfkinWw" name="Prestations" position="58">
        <attribute defType="com.stambia.file.field.size" id="_3GFocRBfEeqPGbHwfkinWw" value="100"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFochBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFocxBfEeqPGbHwfkinWw" value="F58"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoWBBfEeqPGbHwfkinWw" name="Nom Admin" position="52">
        <attribute defType="com.stambia.file.field.size" id="_3GFoWRBfEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoWhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoWxBfEeqPGbHwfkinWw" value="F52"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoDBBfEeqPGbHwfkinWw" name="Date Cmd S.Client" position="33">
        <attribute defType="com.stambia.file.field.size" id="_3GFoDRBfEeqPGbHwfkinWw" value="90"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoDhBfEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoDxBfEeqPGbHwfkinWw" value="F33"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFodBBfEeqPGbHwfkinWw" name="Date dernière Cmd" position="59">
        <attribute defType="com.stambia.file.field.size" id="_3GFodRBfEeqPGbHwfkinWw" value="72"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFodhBfEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFodxBfEeqPGbHwfkinWw" value="F59"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnyBBfEeqPGbHwfkinWw" name="Civilite Client" position="16">
        <attribute defType="com.stambia.file.field.size" id="_3GFnyRBfEeqPGbHwfkinWw" value="5"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnyhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnyxBfEeqPGbHwfkinWw" value="F16"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFopBBfEeqPGbHwfkinWw" name="Mercuriale" position="71">
        <attribute defType="com.stambia.file.field.size" id="_3GFopRBfEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFophBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFopxBfEeqPGbHwfkinWw" value="F71"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnmBBfEeqPGbHwfkinWw" name="BU" position="4">
        <attribute defType="com.stambia.file.field.size" id="_3GFnmRBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnmhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnmxBfEeqPGbHwfkinWw" value="F4"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnzBBfEeqPGbHwfkinWw" name="Nom Client" position="17">
        <attribute defType="com.stambia.file.field.size" id="_3GFnzRBfEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnzhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnzxBfEeqPGbHwfkinWw" value="F17"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn5BBfEeqPGbHwfkinWw" name="Tel Fixe" position="23">
        <attribute defType="com.stambia.file.field.size" id="_3GFn5RBfEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn5hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn5xBfEeqPGbHwfkinWw" value="F23"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnnBBfEeqPGbHwfkinWw" name="DIC" position="5">
        <attribute defType="com.stambia.file.field.size" id="_3GFnnRBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnnhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnnxBfEeqPGbHwfkinWw" value="F5"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnxBBfEeqPGbHwfkinWw" name="Tva intracommunautaire" position="15">
        <attribute defType="com.stambia.file.field.size" id="_3GFnxRBfEeqPGbHwfkinWw" value="15"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnxhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnxxBfEeqPGbHwfkinWw" value="F15"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn6BBfEeqPGbHwfkinWw" name="Tel Portable" position="24">
        <attribute defType="com.stambia.file.field.size" id="_3GFn6RBfEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn6hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn6xBfEeqPGbHwfkinWw" value="F24"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoPBBfEeqPGbHwfkinWw" name="NbRepasJour" position="45">
        <attribute defType="com.stambia.file.field.size" id="_3GFoPRBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoPhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoPxBfEeqPGbHwfkinWw" value="F45"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnpBBfEeqPGbHwfkinWw" name="Nom Ccial" position="7">
        <attribute defType="com.stambia.file.field.size" id="_3GFnpRBfEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnphBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnpxBfEeqPGbHwfkinWw" value="F7"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnqBBfEeqPGbHwfkinWw" name="Prenom Ccial" position="8">
        <attribute defType="com.stambia.file.field.size" id="_3GFnqRBfEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnqhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnqxBfEeqPGbHwfkinWw" value="F8"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn1BBfEeqPGbHwfkinWw" name="Type Contact Web" position="19">
        <attribute defType="com.stambia.file.field.size" id="_3GFn1RBfEeqPGbHwfkinWw" value="15"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn1hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn1xBfEeqPGbHwfkinWw" value="F19"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn-BBfEeqPGbHwfkinWw" name="Nb Cmd par jour SALES" position="28">
        <attribute defType="com.stambia.file.field.size" id="_3GFn-RBfEeqPGbHwfkinWw" value="110"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn-hBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn-xBfEeqPGbHwfkinWw" value="F28"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoKBBfEeqPGbHwfkinWw" name="DateCreation Societe" position="40">
        <attribute defType="com.stambia.file.field.size" id="_3GFoKRBfEeqPGbHwfkinWw" value="70"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoKhBfEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoKxBfEeqPGbHwfkinWw" value="F40"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnvBBfEeqPGbHwfkinWw" name="Raison Sociale" position="13">
        <attribute defType="com.stambia.file.field.size" id="_3GFnvRBfEeqPGbHwfkinWw" value="35"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnvhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnvxBfEeqPGbHwfkinWw" value="F13"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnlBBfEeqPGbHwfkinWw" name="Email principal (oriente acheteur)" position="3">
        <attribute defType="com.stambia.file.field.size" id="_3GFnlRBfEeqPGbHwfkinWw" value="250"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnlhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnlxBfEeqPGbHwfkinWw" value="F3"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFonBBfEeqPGbHwfkinWw" name="CA 36DM" position="69">
        <attribute defType="com.stambia.file.field.size" id="_3GFonRBfEeqPGbHwfkinWw" value="63"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFonhBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFonxBfEeqPGbHwfkinWw" value="F69"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoBBBfEeqPGbHwfkinWw" name="Date Cmd WEB" position="31">
        <attribute defType="com.stambia.file.field.size" id="_3GFoBRBfEeqPGbHwfkinWw" value="79"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoBhBfEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoBxBfEeqPGbHwfkinWw" value="F31"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoOBBfEeqPGbHwfkinWw" name="NivStanding" position="44">
        <attribute defType="com.stambia.file.field.size" id="_3GFoORBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoOhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoOxBfEeqPGbHwfkinWw" value="F44"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFobBBfEeqPGbHwfkinWw" name="DateMaj_Web" position="57">
        <attribute defType="com.stambia.file.field.size" id="_3GFobRBfEeqPGbHwfkinWw" value="61"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFobhBfEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFobxBfEeqPGbHwfkinWw" value="F57"/>
        <attribute defType="com.stambia.file.field.physicalSize" id="_Mx7-EBBiEeqPGbHwfkinWw" value=""/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoLBBfEeqPGbHwfkinWw" name="Secteur" position="41">
        <attribute defType="com.stambia.file.field.size" id="_3GFoLRBfEeqPGbHwfkinWw" value="40"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoLhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoLxBfEeqPGbHwfkinWw" value="F41"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFokBBfEeqPGbHwfkinWw" name="CA 15DM" position="66">
        <attribute defType="com.stambia.file.field.size" id="_3GFokRBfEeqPGbHwfkinWw" value="56"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFokhBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFokxBfEeqPGbHwfkinWw" value="F66"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnoBBfEeqPGbHwfkinWw" name="Civilite CCial" position="6">
        <attribute defType="com.stambia.file.field.size" id="_3GFnoRBfEeqPGbHwfkinWw" value="5"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnohBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnoxBfEeqPGbHwfkinWw" value="F6"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFogBBfEeqPGbHwfkinWw" name="CA Facture 6DM" position="62">
        <attribute defType="com.stambia.file.field.size" id="_3GFogRBfEeqPGbHwfkinWw" value="64"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoghBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFogxBfEeqPGbHwfkinWw" value="F62"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnwBBfEeqPGbHwfkinWw" name="Siret" position="14">
        <attribute defType="com.stambia.file.field.size" id="_3GFnwRBfEeqPGbHwfkinWw" value="14"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnwhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnwxBfEeqPGbHwfkinWw" value="F14"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoaBBfEeqPGbHwfkinWw" name="Num Tel" position="56">
        <attribute defType="com.stambia.file.field.size" id="_3GFoaRBfEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoahBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoaxBfEeqPGbHwfkinWw" value="F56"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFAgBBfEeqPGbHwfkinWw" name="CodClient" position="1">
        <attribute defType="com.stambia.file.field.size" id="_3GFAgRBfEeqPGbHwfkinWw" value="9"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFAghBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFAgxBfEeqPGbHwfkinWw" value="F1"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoFBBfEeqPGbHwfkinWw" name="Date Cmd EI" position="35">
        <attribute defType="com.stambia.file.field.size" id="_3GFoFRBfEeqPGbHwfkinWw" value="72"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoFhBfEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoFxBfEeqPGbHwfkinWw" value="F35"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoUBBfEeqPGbHwfkinWw" name="TicketMoyen" position="50">
        <attribute defType="com.stambia.file.field.size" id="_3GFoURBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoUhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoUxBfEeqPGbHwfkinWw" value="F50"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFohBBfEeqPGbHwfkinWw" name="CA Facture 9DM" position="63">
        <attribute defType="com.stambia.file.field.size" id="_3GFohRBfEeqPGbHwfkinWw" value="64"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFohhBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFohxBfEeqPGbHwfkinWw" value="F63"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFojBBfEeqPGbHwfkinWw" name="CA 12DM" position="65">
        <attribute defType="com.stambia.file.field.size" id="_3GFojRBfEeqPGbHwfkinWw" value="57"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFojhBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFojxBfEeqPGbHwfkinWw" value="F65"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoNBBfEeqPGbHwfkinWw" name="Specialite culinaire" position="43">
        <attribute defType="com.stambia.file.field.size" id="_3GFoNRBfEeqPGbHwfkinWw" value="100"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoNhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoNxBfEeqPGbHwfkinWw" value="F43"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoiBBfEeqPGbHwfkinWw" name="CA Facture 12DM" position="64">
        <attribute defType="com.stambia.file.field.size" id="_3GFoiRBfEeqPGbHwfkinWw" value="57"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoihBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoixBfEeqPGbHwfkinWw" value="F64"/>
        <attribute defType="com.stambia.file.field.physicalSize" id="_KsZN4BBiEeqPGbHwfkinWw" value=""/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn_BBfEeqPGbHwfkinWw" name="Date Cmd SALES" position="29">
        <attribute defType="com.stambia.file.field.size" id="_3GFn_RBfEeqPGbHwfkinWw" value="81"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn_hBfEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn_xBfEeqPGbHwfkinWw" value="F29"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoqBBfEeqPGbHwfkinWw" name="Mini Mercuriale" position="72">
        <attribute defType="com.stambia.file.field.size" id="_3GFoqRBfEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoqhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoqxBfEeqPGbHwfkinWw" value="F72"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnsBBfEeqPGbHwfkinWw" name="Actif" position="10">
        <attribute defType="com.stambia.file.field.size" id="_3GFnsRBfEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnshBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnsxBfEeqPGbHwfkinWw" value="F10"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoRBBfEeqPGbHwfkinWw" name="NbChambres" position="47">
        <attribute defType="com.stambia.file.field.size" id="_3GFoRRBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoRhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoRxBfEeqPGbHwfkinWw" value="F47"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoABBfEeqPGbHwfkinWw" name="Nb Cmd par jour WEB" position="30">
        <attribute defType="com.stambia.file.field.size" id="_3GFoARBfEeqPGbHwfkinWw" value="108"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoAhBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoAxBfEeqPGbHwfkinWw" value="F30"/>
        <attribute defType="com.stambia.file.field.decimal" id="_ovcx0BBiEeqPGbHwfkinWw" value=""/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoTBBfEeqPGbHwfkinWw" name="TranchesCA" position="49">
        <attribute defType="com.stambia.file.field.size" id="_3GFoTRBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoThBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoTxBfEeqPGbHwfkinWw" value="F49"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoMBBfEeqPGbHwfkinWw" name="MailPrincipal" position="42">
        <attribute defType="com.stambia.file.field.size" id="_3GFoMRBfEeqPGbHwfkinWw" value="250"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoMhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoMxBfEeqPGbHwfkinWw" value="F42"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFnkBBfEeqPGbHwfkinWw" name="Filiale" position="2">
        <attribute defType="com.stambia.file.field.size" id="_3GFnkRBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFnkhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFnkxBfEeqPGbHwfkinWw" value="F2"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn3BBfEeqPGbHwfkinWw" name="CP" position="21">
        <attribute defType="com.stambia.file.field.size" id="_3GFn3RBfEeqPGbHwfkinWw" value="9"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn3hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn3xBfEeqPGbHwfkinWw" value="F21"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoGBBfEeqPGbHwfkinWw" name="TypeEtab1" position="36">
        <attribute defType="com.stambia.file.field.size" id="_3GFoGRBfEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoGhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoGxBfEeqPGbHwfkinWw" value="F36"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoCBBfEeqPGbHwfkinWw" name="Nb Cmd par jour S.Client" position="32">
        <attribute defType="com.stambia.file.field.size" id="_3GFoCRBfEeqPGbHwfkinWw" value="119"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoChBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoCxBfEeqPGbHwfkinWw" value="F32"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoEBBfEeqPGbHwfkinWw" name="Nb Commande EI" position="34">
        <attribute defType="com.stambia.file.field.size" id="_3GFoERBfEeqPGbHwfkinWw" value="76"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoEhBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoExBfEeqPGbHwfkinWw" value="F34"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoSBBfEeqPGbHwfkinWw" name="NbEmployes" position="48">
        <attribute defType="com.stambia.file.field.size" id="_3GFoSRBfEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoShBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoSxBfEeqPGbHwfkinWw" value="F48"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn8BBfEeqPGbHwfkinWw" name="Intitulé fonction" position="26">
        <attribute defType="com.stambia.file.field.size" id="_3GFn8RBfEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn8hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn8xBfEeqPGbHwfkinWw" value="F26"/>
        <attribute defType="com.stambia.file.field.physicalSize" id="_qPKREBBiEeqPGbHwfkinWw" value=""/>
        <attribute defType="com.stambia.file.field.tag" id="_rHnjEBBiEeqPGbHwfkinWw">
          <values></values>
        </attribute>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn0BBfEeqPGbHwfkinWw" name="Prenom Client" position="18">
        <attribute defType="com.stambia.file.field.size" id="_3GFn0RBfEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn0hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn0xBfEeqPGbHwfkinWw" value="F18"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoXBBfEeqPGbHwfkinWw" name="PrenomAdmin" position="53">
        <attribute defType="com.stambia.file.field.size" id="_3GFoXRBfEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoXhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoXxBfEeqPGbHwfkinWw" value="F53"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoIBBfEeqPGbHwfkinWw" name="TypeEtab3" position="38">
        <attribute defType="com.stambia.file.field.size" id="_3GFoIRBfEeqPGbHwfkinWw" value="100"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoIhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoIxBfEeqPGbHwfkinWw" value="F38"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn2BBfEeqPGbHwfkinWw" name="Adresse" position="20">
        <attribute defType="com.stambia.file.field.size" id="_3GFn2RBfEeqPGbHwfkinWw" value="70"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn2hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn2xBfEeqPGbHwfkinWw" value="F20"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFn4BBfEeqPGbHwfkinWw" name="Ville" position="22">
        <attribute defType="com.stambia.file.field.size" id="_3GFn4RBfEeqPGbHwfkinWw" value="40"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFn4hBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFn4xBfEeqPGbHwfkinWw" value="F22"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoYBBfEeqPGbHwfkinWw" name="Intitule Poste" position="54">
        <attribute defType="com.stambia.file.field.size" id="_3GFoYRBfEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoYhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoYxBfEeqPGbHwfkinWw" value="F54"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFolBBfEeqPGbHwfkinWw" name="CA 18DM" position="67">
        <attribute defType="com.stambia.file.field.size" id="_3GFolRBfEeqPGbHwfkinWw" value="57"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFolhBfEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFolxBfEeqPGbHwfkinWw" value="F67"/>
      </node>
      <node defType="com.stambia.file.field" id="_3GFoQBBfEeqPGbHwfkinWw" name="AnneeExistence" position="46">
        <attribute defType="com.stambia.file.field.size" id="_3GFoQRBfEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_3GFoQhBfEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_3GFoQxBfEeqPGbHwfkinWw" value="F46"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.file.directory" id="_C6IfABTiEeqPGbHwfkinWw" name="Extract_DI">
    <attribute defType="com.stambia.file.directory.path" id="_6RQvQBTiEeqPGbHwfkinWw" value="\\servernt46\Stambia\DI\Export"/>
    <node defType="com.stambia.file.file" id="_8LuQ0BTiEeqPGbHwfkinWw" name="DI">
      <attribute defType="com.stambia.file.file.type" id="_8MshMBTiEeqPGbHwfkinWw" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_8MtvUBTiEeqPGbHwfkinWw" value="UTF-8"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_8MtvURTiEeqPGbHwfkinWw" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_8MtvUhTiEeqPGbHwfkinWw" value="3B"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_8MuWYBTiEeqPGbHwfkinWw" value="22"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_8MuWYRTiEeqPGbHwfkinWw" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_8MuWYhTiEeqPGbHwfkinWw" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_8MuWYxTiEeqPGbHwfkinWw" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_8MuWZBTiEeqPGbHwfkinWw" value="1"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_HnoVYEjWEeqGlq8GK04HnQ" value="${~/NomFichierExport}$"/>
      <node defType="com.stambia.file.field" id="_-Rz0BxTiEeqPGbHwfkinWw" name="Mercuriale" position="74">
        <attribute defType="com.stambia.file.field.size" id="_-Rz0CBTiEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rz0CRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rz0ChTiEeqPGbHwfkinWw" value="F71"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMpBTiEeqPGbHwfkinWw" name="Multicompte" position="11">
        <attribute defType="com.stambia.file.field.size" id="_-RzMpRTiEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMphTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMpxTiEeqPGbHwfkinWw" value="F11"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMuBTiEeqPGbHwfkinWw" name="Civilite Client" position="16">
        <attribute defType="com.stambia.file.field.size" id="_-RzMuRTiEeqPGbHwfkinWw" value="5"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMuhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMuxTiEeqPGbHwfkinWw" value="F16"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM_BTiEeqPGbHwfkinWw" name="Date Cmd S.Client" position="36">
        <attribute defType="com.stambia.file.field.size" id="_-RzM_RTiEeqPGbHwfkinWw" value="90"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM_hTiEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM_xTiEeqPGbHwfkinWw" value="F33"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMsBTiEeqPGbHwfkinWw" name="Siret" position="14">
        <attribute defType="com.stambia.file.field.size" id="_-RzMsRTiEeqPGbHwfkinWw" value="14"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMshTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMsxTiEeqPGbHwfkinWw" value="F14"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM3BTiEeqPGbHwfkinWw" name="Nom Responsable" position="25">
        <attribute defType="com.stambia.file.field.size" id="_-RzM3RTiEeqPGbHwfkinWw" value="40"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM3hTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM3xTiEeqPGbHwfkinWw" value="F25"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM0BTiEeqPGbHwfkinWw" name="Ville" position="22">
        <attribute defType="com.stambia.file.field.size" id="_-RzM0RTiEeqPGbHwfkinWw" value="40"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM0hTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM0xTiEeqPGbHwfkinWw" value="F22"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMkBTiEeqPGbHwfkinWw" name="Civilite CCial" position="6">
        <attribute defType="com.stambia.file.field.size" id="_-RzMkRTiEeqPGbHwfkinWw" value="5"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMkhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMkxTiEeqPGbHwfkinWw" value="F6"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMqBTiEeqPGbHwfkinWw" name="Enseigne" position="12">
        <attribute defType="com.stambia.file.field.size" id="_-RzMqRTiEeqPGbHwfkinWw" value="40"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMqhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMqxTiEeqPGbHwfkinWw" value="F12"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzvxTiEeqPGbHwfkinWw" name="PrenomAdmin" position="56">
        <attribute defType="com.stambia.file.field.size" id="_-RzzwBTiEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzwRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzwhTiEeqPGbHwfkinWw" value="F53"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzrxTiEeqPGbHwfkinWw" name="TranchesCA" position="52">
        <attribute defType="com.stambia.file.field.size" id="_-RzzsBTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzsRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzshTiEeqPGbHwfkinWw" value="F49"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rzz6xTiEeqPGbHwfkinWw" name="CA Facture 12DM" position="67">
        <attribute defType="com.stambia.file.field.size" id="_-Rzz7BTiEeqPGbHwfkinWw" value="57"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rzz7RTiEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rzz7hTiEeqPGbHwfkinWw" value="F64"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzNEBTiEeqPGbHwfkinWw" name="TypeEtab3" position="41">
        <attribute defType="com.stambia.file.field.size" id="_-RzNERTiEeqPGbHwfkinWw" value="100"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzNEhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzNExTiEeqPGbHwfkinWw" value="F38"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzkxTiEeqPGbHwfkinWw" name="MailPrincipal" position="45">
        <attribute defType="com.stambia.file.field.size" id="_-RzzlBTiEeqPGbHwfkinWw" value="250"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzlRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzlhTiEeqPGbHwfkinWw" value="F42"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzNHBTiEeqPGbHwfkinWw" name="Secteur" position="44">
        <attribute defType="com.stambia.file.field.size" id="_-RzzkBTiEeqPGbHwfkinWw" value="40"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzkRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzkhTiEeqPGbHwfkinWw" value="F41"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMoBTiEeqPGbHwfkinWw" name="Actif" position="10">
        <attribute defType="com.stambia.file.field.size" id="_-RzMoRTiEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMohTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMoxTiEeqPGbHwfkinWw" value="F10"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM9BTiEeqPGbHwfkinWw" name="Date Cmd WEB" position="34">
        <attribute defType="com.stambia.file.field.size" id="_-RzM9RTiEeqPGbHwfkinWw" value="79"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM9hTiEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM9xTiEeqPGbHwfkinWw" value="F31"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rzz0xTiEeqPGbHwfkinWw" name="Prestations" position="61">
        <attribute defType="com.stambia.file.field.size" id="_-Rzz1BTiEeqPGbHwfkinWw" value="100"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rzz1RTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rzz1hTiEeqPGbHwfkinWw" value="F58"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rzz4xTiEeqPGbHwfkinWw" name="CA Facture 6DM" position="65">
        <attribute defType="com.stambia.file.field.size" id="_-Rzz5BTiEeqPGbHwfkinWw" value="64"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rzz5RTiEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rzz5hTiEeqPGbHwfkinWw" value="F62"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzsxTiEeqPGbHwfkinWw" name="TicketMoyen" position="53">
        <attribute defType="com.stambia.file.field.size" id="_-RzztBTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzztRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzthTiEeqPGbHwfkinWw" value="F50"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMgBTiEeqPGbHwfkinWw" name="Filiale" position="2">
        <attribute defType="com.stambia.file.field.size" id="_-RzMgRTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMghTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMgxTiEeqPGbHwfkinWw" value="F2"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM6BTiEeqPGbHwfkinWw" name="Nb Cmd par jour SALES" position="31">
        <attribute defType="com.stambia.file.field.size" id="_-RzM6RTiEeqPGbHwfkinWw" value="110"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM6hTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM6xTiEeqPGbHwfkinWw" value="F28"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM4BTiEeqPGbHwfkinWw" name="Intitulé fonction" position="26">
        <attribute defType="com.stambia.file.field.size" id="_-RzM4RTiEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM4hTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM4xTiEeqPGbHwfkinWw" value="F26"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzNABTiEeqPGbHwfkinWw" name="Nb Commande EI" position="37">
        <attribute defType="com.stambia.file.field.size" id="_-RzNARTiEeqPGbHwfkinWw" value="76"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzNAhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzNAxTiEeqPGbHwfkinWw" value="F34"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzmxTiEeqPGbHwfkinWw" name="NivStanding" position="47">
        <attribute defType="com.stambia.file.field.size" id="_-RzznBTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzznRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzznhTiEeqPGbHwfkinWw" value="F44"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzzxTiEeqPGbHwfkinWw" name="DateMaj_Web" position="60">
        <attribute defType="com.stambia.file.field.size" id="_-Rzz0BTiEeqPGbHwfkinWw" value="61"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rzz0RTiEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rzz0hTiEeqPGbHwfkinWw" value="F57"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMrBTiEeqPGbHwfkinWw" name="Raison Sociale" position="13">
        <attribute defType="com.stambia.file.field.size" id="_-RzMrRTiEeqPGbHwfkinWw" value="40"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMrhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMrxTiEeqPGbHwfkinWw" value="F13"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMwBTiEeqPGbHwfkinWw" name="Prenom Client" position="18">
        <attribute defType="com.stambia.file.field.size" id="_-RzMwRTiEeqPGbHwfkinWw" value="63"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMwhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMwxTiEeqPGbHwfkinWw" value="F18"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzuxTiEeqPGbHwfkinWw" name="Nom Admin" position="55">
        <attribute defType="com.stambia.file.field.size" id="_-RzzvBTiEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzvRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzvhTiEeqPGbHwfkinWw" value="F52"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzpxTiEeqPGbHwfkinWw" name="NbChambres" position="50">
        <attribute defType="com.stambia.file.field.size" id="_-RzzqBTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzqRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzqhTiEeqPGbHwfkinWw" value="F47"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM7BTiEeqPGbHwfkinWw" name="Date Cmd SALES" position="32">
        <attribute defType="com.stambia.file.field.size" id="_-RzM7RTiEeqPGbHwfkinWw" value="81"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM7hTiEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM7xTiEeqPGbHwfkinWw" value="F29"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rzz3xTiEeqPGbHwfkinWw" name="CA Facture 3DM" position="64">
        <attribute defType="com.stambia.file.field.size" id="_-Rzz4BTiEeqPGbHwfkinWw" value="64"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rzz4RTiEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rzz4hTiEeqPGbHwfkinWw" value="F61"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzNCBTiEeqPGbHwfkinWw" name="TypeEtab1" position="39">
        <attribute defType="com.stambia.file.field.size" id="_-RzNCRTiEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzNChTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzNCxTiEeqPGbHwfkinWw" value="F36"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzNDBTiEeqPGbHwfkinWw" name="TypeEtab2" position="40">
        <attribute defType="com.stambia.file.field.size" id="_-RzNDRTiEeqPGbHwfkinWw" value="100"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzNDhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzNDxTiEeqPGbHwfkinWw" value="F37"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzyxTiEeqPGbHwfkinWw" name="Num Tel" position="59">
        <attribute defType="com.stambia.file.field.size" id="_-RzzzBTiEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzzRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzzhTiEeqPGbHwfkinWw" value="F56"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMzBTiEeqPGbHwfkinWw" name="CP" position="21">
        <attribute defType="com.stambia.file.field.size" id="_-RzMzRTiEeqPGbHwfkinWw" value="9"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMzhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMzxTiEeqPGbHwfkinWw" value="F21"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rzz1xTiEeqPGbHwfkinWw" name="Date dernière Cmd" position="62">
        <attribute defType="com.stambia.file.field.size" id="_-Rzz2BTiEeqPGbHwfkinWw" value="72"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rzz2RTiEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rzz2hTiEeqPGbHwfkinWw" value="F59"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzxxTiEeqPGbHwfkinWw" name="MailUserAdmin" position="58">
        <attribute defType="com.stambia.file.field.size" id="_-RzzyBTiEeqPGbHwfkinWw" value="250"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzyRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzyhTiEeqPGbHwfkinWw" value="F55"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM2BTiEeqPGbHwfkinWw" name="Tel Portable" position="24">
        <attribute defType="com.stambia.file.field.size" id="_-RzM2RTiEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM2hTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM2xTiEeqPGbHwfkinWw" value="F24"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM8BTiEeqPGbHwfkinWw" name="Nb Cmd par jour WEB" position="33">
        <attribute defType="com.stambia.file.field.size" id="_-RzM8RTiEeqPGbHwfkinWw" value="108"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM8hTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM8xTiEeqPGbHwfkinWw" value="F30"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzNGBTiEeqPGbHwfkinWw" name="DateCreation Societe" position="43">
        <attribute defType="com.stambia.file.field.size" id="_-RzNGRTiEeqPGbHwfkinWw" value="70"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzNGhTiEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzNGxTiEeqPGbHwfkinWw" value="F40"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMlBTiEeqPGbHwfkinWw" name="Nom Ccial" position="7">
        <attribute defType="com.stambia.file.field.size" id="_-RzMlRTiEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMlhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMlxTiEeqPGbHwfkinWw" value="F7"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzlxTiEeqPGbHwfkinWw" name="Specialite culinaire" position="46">
        <attribute defType="com.stambia.file.field.size" id="_-RzzmBTiEeqPGbHwfkinWw" value="100"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzmRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzmhTiEeqPGbHwfkinWw" value="F43"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMiBTiEeqPGbHwfkinWw" name="BU" position="4">
        <attribute defType="com.stambia.file.field.size" id="_-RzMiRTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMihTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMixTiEeqPGbHwfkinWw" value="F4"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM-BTiEeqPGbHwfkinWw" name="Nb Cmd par jour S.Client" position="35">
        <attribute defType="com.stambia.file.field.size" id="_-RzM-RTiEeqPGbHwfkinWw" value="119"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM-hTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM-xTiEeqPGbHwfkinWw" value="F32"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzNFBTiEeqPGbHwfkinWw" name="Nb Couverts/j" position="42">
        <attribute defType="com.stambia.file.field.size" id="_-RzNFRTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzNFhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzNFxTiEeqPGbHwfkinWw" value="F39"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzznxTiEeqPGbHwfkinWw" name="NbRepasJour" position="48">
        <attribute defType="com.stambia.file.field.size" id="_-RzzoBTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzoRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzohTiEeqPGbHwfkinWw" value="F45"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMhBTiEeqPGbHwfkinWw" name="Email principal (oriente acheteur)" position="3">
        <attribute defType="com.stambia.file.field.size" id="_-RzMhRTiEeqPGbHwfkinWw" value="250"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMhhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMhxTiEeqPGbHwfkinWw" value="F3"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzwxTiEeqPGbHwfkinWw" name="Intitule Poste" position="57">
        <attribute defType="com.stambia.file.field.size" id="_-RzzxBTiEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzxRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzxhTiEeqPGbHwfkinWw" value="F54"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMnBTiEeqPGbHwfkinWw" name="Segment" position="9">
        <attribute defType="com.stambia.file.field.size" id="_-RzMnRTiEeqPGbHwfkinWw" value="35"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMnhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMnxTiEeqPGbHwfkinWw" value="F9"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rz0AxTiEeqPGbHwfkinWw" name="Eligible Promo" position="73">
        <attribute defType="com.stambia.file.field.size" id="_-Rz0BBTiEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rz0BRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rz0BhTiEeqPGbHwfkinWw" value="F70"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMjBTiEeqPGbHwfkinWw" name="DIC" position="5">
        <attribute defType="com.stambia.file.field.size" id="_-RzMjRTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMjhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMjxTiEeqPGbHwfkinWw" value="F5"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzztxTiEeqPGbHwfkinWw" name="CptChomette" position="54">
        <attribute defType="com.stambia.file.field.size" id="_-RzzuBTiEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzuRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzuhTiEeqPGbHwfkinWw" value="F51"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMxBTiEeqPGbHwfkinWw" name="Type Contact Web" position="19">
        <attribute defType="com.stambia.file.field.size" id="_-RzMxRTiEeqPGbHwfkinWw" value="15"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMxhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMxxTiEeqPGbHwfkinWw" value="F19"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM1BTiEeqPGbHwfkinWw" name="Tel Fixe" position="23">
        <attribute defType="com.stambia.file.field.size" id="_-RzM1RTiEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM1hTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM1xTiEeqPGbHwfkinWw" value="F23"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMmBTiEeqPGbHwfkinWw" name="Prenom Ccial" position="8">
        <attribute defType="com.stambia.file.field.size" id="_-RzMmRTiEeqPGbHwfkinWw" value="20"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMmhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMmxTiEeqPGbHwfkinWw" value="F8"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rz0CxTiEeqPGbHwfkinWw" name="Mini Mercuriale" position="75">
        <attribute defType="com.stambia.file.field.size" id="_-Rz0DBTiEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rz0DRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rz0DhTiEeqPGbHwfkinWw" value="F72"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzNBBTiEeqPGbHwfkinWw" name="Date Cmd EI" position="38">
        <attribute defType="com.stambia.file.field.size" id="_-RzNBRTiEeqPGbHwfkinWw" value="72"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzNBhTiEeqPGbHwfkinWw" value="Date"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzNBxTiEeqPGbHwfkinWw" value="F35"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMtBTiEeqPGbHwfkinWw" name="Tva intracommunautaire" position="15">
        <attribute defType="com.stambia.file.field.size" id="_-RzMtRTiEeqPGbHwfkinWw" value="15"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMthTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMtxTiEeqPGbHwfkinWw" value="F15"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzqxTiEeqPGbHwfkinWw" name="NbEmployes" position="51">
        <attribute defType="com.stambia.file.field.size" id="_-RzzrBTiEeqPGbHwfkinWw" value="30"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzrRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzrhTiEeqPGbHwfkinWw" value="F48"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzM5BTiEeqPGbHwfkinWw" name="Optin" position="30">
        <attribute defType="com.stambia.file.field.size" id="_-RzM5RTiEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzM5hTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzM5xTiEeqPGbHwfkinWw" value="F27"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rx-YBTiEeqPGbHwfkinWw" name="CodClient" position="1">
        <attribute defType="com.stambia.file.field.size" id="_-Rx-YRTiEeqPGbHwfkinWw" value="9"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rx-YhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rx-YxTiEeqPGbHwfkinWw" value="F1"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMyBTiEeqPGbHwfkinWw" name="Adresse" position="20">
        <attribute defType="com.stambia.file.field.size" id="_-RzMyRTiEeqPGbHwfkinWw" value="70"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMyhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMyxTiEeqPGbHwfkinWw" value="F20"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rzz2xTiEeqPGbHwfkinWw" name="CA Facture Cmd HT" position="63">
        <attribute defType="com.stambia.file.field.size" id="_-Rzz3BTiEeqPGbHwfkinWw" value="81"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rzz3RTiEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rzz3hTiEeqPGbHwfkinWw" value="F60"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzMvBTiEeqPGbHwfkinWw" name="Nom Client" position="17">
        <attribute defType="com.stambia.file.field.size" id="_-RzMvRTiEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzMvhTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzMvxTiEeqPGbHwfkinWw" value="F17"/>
      </node>
      <node defType="com.stambia.file.field" id="_-RzzoxTiEeqPGbHwfkinWw" name="AnneeExistence" position="49">
        <attribute defType="com.stambia.file.field.size" id="_-RzzpBTiEeqPGbHwfkinWw" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_-RzzpRTiEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-RzzphTiEeqPGbHwfkinWw" value="F46"/>
      </node>
      <node defType="com.stambia.file.field" id="_-Rzz5xTiEeqPGbHwfkinWw" name="CA Facture 9DM" position="66">
        <attribute defType="com.stambia.file.field.size" id="_-Rzz6BTiEeqPGbHwfkinWw" value="64"/>
        <attribute defType="com.stambia.file.field.type" id="_-Rzz6RTiEeqPGbHwfkinWw" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-Rzz6hTiEeqPGbHwfkinWw" value="F63"/>
      </node>
      <node defType="com.stambia.file.field" id="_VxmQEBTpEeqPGbHwfkinWw" name="Groupe" position="27">
        <attribute defType="com.stambia.file.field.physicalName" id="_VyC8ABTpEeqPGbHwfkinWw" value="C73"/>
        <attribute defType="com.stambia.file.field.type" id="_VyDjEBTpEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.size" id="_VyDjERTpEeqPGbHwfkinWw" value="50"/>
      </node>
      <node defType="com.stambia.file.field" id="_V-p0oBTpEeqPGbHwfkinWw" name="Centralisateur" position="28">
        <attribute defType="com.stambia.file.field.physicalName" id="_V-2o8BTpEeqPGbHwfkinWw" value="C74"/>
        <attribute defType="com.stambia.file.field.type" id="_V-2o8RTpEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.size" id="_V-2o8hTpEeqPGbHwfkinWw" value="50"/>
      </node>
      <node defType="com.stambia.file.field" id="_Wq0XEBTpEeqPGbHwfkinWw" name="Chaine" position="29">
        <attribute defType="com.stambia.file.field.physicalName" id="_WrBLYBTpEeqPGbHwfkinWw" value="C75"/>
        <attribute defType="com.stambia.file.field.type" id="_WrBycBTpEeqPGbHwfkinWw" value="String"/>
        <attribute defType="com.stambia.file.field.size" id="_WrBycRTpEeqPGbHwfkinWw" value="50"/>
      </node>
      <node defType="com.stambia.file.field" id="_yRg18EjhEeqGlq8GK04HnQ" name="COLONNE TARIFAIRE" position="76">
        <attribute defType="com.stambia.file.field.physicalName" id="_yR7FoEjhEeqGlq8GK04HnQ" value="C76"/>
        <attribute defType="com.stambia.file.field.type" id="_yR7FoUjhEeqGlq8GK04HnQ" value="String"/>
        <attribute defType="com.stambia.file.field.size" id="_yR7FokjhEeqGlq8GK04HnQ" value="2"/>
      </node>
    </node>
  </node>
</md:node>