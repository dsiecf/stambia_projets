<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_Jcn8MAcEEeu5RMooklzXfQ" name="MSSQLServer_Divalto" md:ref="platform:/plugin/com.indy.environment/technology/rdbms/mssql/mssql.rdbms.md#UUID_MD_RDBMS_MSSQL?fileId=UUID_MD_RDBMS_MSSQL$type=md$name=Microsoft%20SQL%20Server?">
  <attribute defType="com.stambia.rdbms.server.url" id="_S7wwIAcEEeu5RMooklzXfQ" value="jdbc:sqlserver://172.16.128.191"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_S7wwIQcEEeu5RMooklzXfQ" value="com.microsoft.sqlserver.jdbc.SQLServerDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_S7wwIgcEEeu5RMooklzXfQ" value="dsi_etudes"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_S7xXMAcEEeu5RMooklzXfQ" value="25FC3DFA90B1557E3938DF9446D31D379F56FC671615A6C08640143EF56D2DF2"/>
  <node defType="com.stambia.rdbms.schema" id="_Uk1mwAcEEeu5RMooklzXfQ" name="ERPDIVALTOTEST.dbo">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_UlyB8AcEEeu5RMooklzXfQ" value="ERPDIVALTOTEST"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_UlyB8QcEEeu5RMooklzXfQ" value="dbo"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_UlyB8gcEEeu5RMooklzXfQ" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_UlyB8wcEEeu5RMooklzXfQ" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_UlypAAcEEeu5RMooklzXfQ" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_Yo6gMAcEEeu5RMooklzXfQ" name="ECF_COBAL_CLIENT">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Yo6gMQcEEeu5RMooklzXfQ" value="ECF_COBAL_CLIENT"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Yo6gMgcEEeu5RMooklzXfQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_YpGtcAcEEeu5RMooklzXfQ" name="ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_YpGtcQcEEeu5RMooklzXfQ" value="ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YpGtcgcEEeu5RMooklzXfQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YpGtcwcEEeu5RMooklzXfQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YpGtdAcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YpGtdQcEEeu5RMooklzXfQ" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YpGtdgcEEeu5RMooklzXfQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YpKX0AcEEeu5RMooklzXfQ" name="CODE_CLIENT" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_YpKX0QcEEeu5RMooklzXfQ" value="CODE_CLIENT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YpKX0gcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YpKX0wcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YpKX1AcEEeu5RMooklzXfQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YpKX1QcEEeu5RMooklzXfQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YpNbIAcEEeu5RMooklzXfQ" name="RS_CLIENT" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_YpNbIQcEEeu5RMooklzXfQ" value="RS_CLIENT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YpNbIgcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YpNbIwcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YpNbJAcEEeu5RMooklzXfQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YpNbJQcEEeu5RMooklzXfQ" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YpP3YAcEEeu5RMooklzXfQ" name="NOM_CONTACT" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_YpP3YQcEEeu5RMooklzXfQ" value="NOM_CONTACT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YpP3YgcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YpP3YwcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YpP3ZAcEEeu5RMooklzXfQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YpP3ZQcEEeu5RMooklzXfQ" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YpSToAcEEeu5RMooklzXfQ" name="EMAIL_CONTACT" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_YpSToQcEEeu5RMooklzXfQ" value="EMAIL_CONTACT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YpSTogcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YpSTowcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YpSTpAcEEeu5RMooklzXfQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YpSTpQcEEeu5RMooklzXfQ" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YpVW8AcEEeu5RMooklzXfQ" name="TOKEN" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_YpVW8QcEEeu5RMooklzXfQ" value="TOKEN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YpVW8gcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YpVW8wcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YpVW9AcEEeu5RMooklzXfQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YpVW9QcEEeu5RMooklzXfQ" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YpZBUAcEEeu5RMooklzXfQ" name="TOP_ETL" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_YpZBUQcEEeu5RMooklzXfQ" value="TOP_ETL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YpZBUgcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YpZBUwcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YpZBVAcEEeu5RMooklzXfQ" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YpZBVQcEEeu5RMooklzXfQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YpbdkAcEEeu5RMooklzXfQ" name="TOP_MAJ" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_YpbdkQcEEeu5RMooklzXfQ" value="TOP_MAJ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YpbdkgcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YpbdkwcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YpbdlAcEEeu5RMooklzXfQ" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YpbdlQcEEeu5RMooklzXfQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ypd50AcEEeu5RMooklzXfQ" name="DATE_CREATION" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ypd50QcEEeu5RMooklzXfQ" value="DATE_CREATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ypd50gcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ypd50wcEEeu5RMooklzXfQ" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ypd51AcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ypd51QcEEeu5RMooklzXfQ" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ypd51gcEEeu5RMooklzXfQ" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ypg9IAcEEeu5RMooklzXfQ" name="DATE_MODIFICATION" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ypg9IQcEEeu5RMooklzXfQ" value="DATE_MODIFICATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ypg9IgcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ypg9IwcEEeu5RMooklzXfQ" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ypg9JAcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ypg9JQcEEeu5RMooklzXfQ" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ypg9JgcEEeu5RMooklzXfQ" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YpkngAcEEeu5RMooklzXfQ" name="DATE_SUPPRESSION" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_YpkngQcEEeu5RMooklzXfQ" value="DATE_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YpknggcEEeu5RMooklzXfQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YpkngwcEEeu5RMooklzXfQ" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YpknhAcEEeu5RMooklzXfQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YpknhQcEEeu5RMooklzXfQ" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YpknhgcEEeu5RMooklzXfQ" value="23"/>
      </node>
    </node>
  </node>
</md:node>