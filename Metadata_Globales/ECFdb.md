<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_sSHtYF-UEema_r8Hg2-1dA" name="ECFdb" md:ref="platform:/plugin/com.indy.environment/technology/rdbms/mssql/mssql.rdbms.md#UUID_MD_RDBMS_MSSQL?fileId=UUID_MD_RDBMS_MSSQL$type=md$name=Microsoft%20SQL%20Server?">
  <attribute defType="com.stambia.rdbms.server.url" id="_D2WMsF-VEema_r8Hg2-1dA" value="jdbc:sqlserver://172.16.128.35;instance=SERVERNT52;databaseName=ECFDBTest"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_D2WMsV-VEema_r8Hg2-1dA" value="com.microsoft.sqlserver.jdbc.SQLServerDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_D2WMsl-VEema_r8Hg2-1dA" value="etude"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_D2WzwF-VEema_r8Hg2-1dA" value="356C965F9E8F8893F3B9C9982987DD1F"/>
  <node defType="com.stambia.rdbms.schema" id="_sX7acF-UEema_r8Hg2-1dA" name="ECFDBTest.dbo">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_sYh3YF-UEema_r8Hg2-1dA" value="ECFDBTest"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_sYh3YV-UEema_r8Hg2-1dA" value="dbo"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_sYh3Yl-UEema_r8Hg2-1dA" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_sYiecF-UEema_r8Hg2-1dA" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_sYiecV-UEema_r8Hg2-1dA" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_DmSXcV-VEema_r8Hg2-1dA" name="Client_HoraireLiv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DmSXcl-VEema_r8Hg2-1dA" value="Client_HoraireLiv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DmSXc1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DmWB0F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmWB0V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmWB0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmWB01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmWB1F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmWB1V-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmWB1l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmXP8F-VEema_r8Hg2-1dA" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmXP8V-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmXP8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmXP81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmXP9F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmXP9V-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmXP9l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmX3AF-VEema_r8Hg2-1dA" name="H_DebutLivLundi" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmX3AV-VEema_r8Hg2-1dA" value="H_DebutLivLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmX3Al-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmX3A1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmX3BF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmX3BV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmX3Bl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmZFIF-VEema_r8Hg2-1dA" name="H_DebutLivMardi" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmZFIV-VEema_r8Hg2-1dA" value="H_DebutLivMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmZFIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmZFI1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmZFJF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmZFJV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmZFJl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmZsMF-VEema_r8Hg2-1dA" name="H_DebutLivMercredi" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmZsMV-VEema_r8Hg2-1dA" value="H_DebutLivMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmZsMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmZsM1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmZsNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmZsNV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmZsNl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dma6UF-VEema_r8Hg2-1dA" name="H_DebutLivJeudi" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dma6UV-VEema_r8Hg2-1dA" value="H_DebutLivJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dma6Ul-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dma6U1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dma6VF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dma6VV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dma6Vl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmbhYF-VEema_r8Hg2-1dA" name="H_DebutLivVendredi" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmbhYV-VEema_r8Hg2-1dA" value="H_DebutLivVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmbhYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmbhY1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmbhZF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmbhZV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmbhZl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmcvgF-VEema_r8Hg2-1dA" name="H_DebutLivSamedi" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmcvgV-VEema_r8Hg2-1dA" value="H_DebutLivSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dmcvgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dmcvg1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmcvhF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmcvhV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dmcvhl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmdWkF-VEema_r8Hg2-1dA" name="H_DebutLivDimanche" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmdWkV-VEema_r8Hg2-1dA" value="H_DebutLivDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmdWkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmdWk1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmdWlF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmdWlV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmdWll-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dmd9oF-VEema_r8Hg2-1dA" name="H_FinLivLundi" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dmd9oV-VEema_r8Hg2-1dA" value="H_FinLivLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dmd9ol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dmd9o1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dmd9pF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dmd9pV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dmd9pl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmfLwF-VEema_r8Hg2-1dA" name="H_FinLivMardi" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmfLwV-VEema_r8Hg2-1dA" value="H_FinLivMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmfLwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmfLw1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmfLxF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmfLxV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmfLxl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dmfy0F-VEema_r8Hg2-1dA" name="H_FinLivMercredi" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dmfy0V-VEema_r8Hg2-1dA" value="H_FinLivMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dmfy0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dmfy01-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dmfy1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dmfy1V-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dmfy1l-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmhA8F-VEema_r8Hg2-1dA" name="H_FinLivJeudi" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmhA8V-VEema_r8Hg2-1dA" value="H_FinLivJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmhA8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmhA81-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmhA9F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmhA9V-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmhA9l-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmiPEF-VEema_r8Hg2-1dA" name="H_FinLivVendredi" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmiPEV-VEema_r8Hg2-1dA" value="H_FinLivVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmiPEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmiPE1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmiPFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmiPFV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmiPFl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dmi2IF-VEema_r8Hg2-1dA" name="H_FinLivSamedi" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dmi2IV-VEema_r8Hg2-1dA" value="H_FinLivSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dmi2Il-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dmi2I1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dmi2JF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dmi2JV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dmi2Jl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmjdMF-VEema_r8Hg2-1dA" name="H_FinLivDimanche" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmjdMV-VEema_r8Hg2-1dA" value="H_FinLivDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmjdMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmjdM1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmjdNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmjdNV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmjdNl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dml5cF-VEema_r8Hg2-1dA" name="PK_Horaire_Livraison">
        <node defType="com.stambia.rdbms.colref" id="_Dml5cV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dml5cl-VEema_r8Hg2-1dA" ref="#_DmXP8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzvkoF-VEema_r8Hg2-1dA" name="FK_Client_HoraireLiv_Client">
        <node defType="com.stambia.rdbms.relation" id="_DzvkoV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzvkol-VEema_r8Hg2-1dA" ref="#_DmXP8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzvko1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DexaMV-VEema_r8Hg2-1dA" name="Client_JourLiv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DexaMl-VEema_r8Hg2-1dA" value="Client_JourLiv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DexaM1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_De1EkF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_De1EkV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_De1Ekl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_De1Ek1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_De1ElF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_De1ElV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_De1Ell-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_De2SsF-VEema_r8Hg2-1dA" name="IdJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_De2SsV-VEema_r8Hg2-1dA" value="IdJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_De2Ssl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_De2Ss1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_De2StF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_De2StV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_De2Stl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_De4u8F-VEema_r8Hg2-1dA" name="PK_Client_JourLiv">
        <node defType="com.stambia.rdbms.colref" id="_De4u8V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_De4u8l-VEema_r8Hg2-1dA" ref="#_De1EkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_De4u81-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_De4u9F-VEema_r8Hg2-1dA" ref="#_De2SsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdJour?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzXKIF-VEema_r8Hg2-1dA" name="FK_Client_JourLiv_Client">
        <node defType="com.stambia.rdbms.relation" id="_DzXKIV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzXKIl-VEema_r8Hg2-1dA" ref="#_De1EkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzXKI1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzXKJF-VEema_r8Hg2-1dA" name="FK_Client_JourLiv_ref_Jours1">
        <node defType="com.stambia.rdbms.relation" id="_DzXKJV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzXKJl-VEema_r8Hg2-1dA" ref="#_De2SsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzXKJ1-VEema_r8Hg2-1dA" ref="#_DnkJ0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DngfcV-VEema_r8Hg2-1dA" name="ref_Jours">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dngfcl-VEema_r8Hg2-1dA" value="ref_Jours"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dngfc1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DnkJ0F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnkJ0V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnkJ0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DnkJ01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnkJ1F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnkJ1V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnkJ1l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DnlX8F-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnlX8V-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnlX8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnlX81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnlX9F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnlX9V-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DnppYF-VEema_r8Hg2-1dA" name="PK_ref_Jours">
        <node defType="com.stambia.rdbms.colref" id="_DnppYV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DnppYl-VEema_r8Hg2-1dA" ref="#_DnkJ0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DqCPAV-VEema_r8Hg2-1dA" name="ref_NbChambres">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DqCPAl-VEema_r8Hg2-1dA" value="ref_NbChambres"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DqCPA1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DqFSUF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqFSUV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqFSUl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DqFSU1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqFSVF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqFSVV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqFSVl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DqGgcF-VEema_r8Hg2-1dA" name="NbChambres" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqGgcV-VEema_r8Hg2-1dA" value="NbChambres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqGgcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqGgc1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqGgdF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqGgdV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DqHHgF-VEema_r8Hg2-1dA" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqHHgV-VEema_r8Hg2-1dA" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqHHgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DqHHg1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqHHhF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqHHhV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqHHhl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DqIVoF-VEema_r8Hg2-1dA" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqIVoV-VEema_r8Hg2-1dA" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqIVol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DqIVo1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqIVpF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqIVpV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqIVpl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DqMnEF-VEema_r8Hg2-1dA" name="PK_ref_NbChambres">
        <node defType="com.stambia.rdbms.colref" id="_DqMnEV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DqMnEl-VEema_r8Hg2-1dA" ref="#_DqFSUF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DoJYoV-VEema_r8Hg2-1dA" name="ref_StatutClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DoJYol-VEema_r8Hg2-1dA" value="ref_StatutClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DoJYo1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DoMb8F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoMb8V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoMb8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DoMb81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoMb9F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoMb9V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoMb9l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DoNDAF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoNDAV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoNDAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoNDA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoNDBF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoNDBV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DoORIF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoORIV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoO4MF-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoO4MV-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoO4Ml-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoO4M1-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DoQtYF-VEema_r8Hg2-1dA" name="PK_ref_StatutClient">
        <node defType="com.stambia.rdbms.colref" id="_DoQtYV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DoQtYl-VEema_r8Hg2-1dA" ref="#_DoMb8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dl-1cV-VEema_r8Hg2-1dA" name="ref_NbLits">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dl_cgF-VEema_r8Hg2-1dA" value="ref_NbLits"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dl_cgV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DmCf0F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmCf0V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmCf0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmCf01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmCf1F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmCf1V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmCf1l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmDt8F-VEema_r8Hg2-1dA" name="Nblits" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmDt8V-VEema_r8Hg2-1dA" value="Nblits"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmDt8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmDt81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmDt9F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmDt9V-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmE8EF-VEema_r8Hg2-1dA" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmE8EV-VEema_r8Hg2-1dA" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmE8El-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmE8E1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmE8FF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmE8FV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmE8Fl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmFjIF-VEema_r8Hg2-1dA" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmFjIV-VEema_r8Hg2-1dA" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmFjIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmFjI1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmFjJF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmFjJV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmFjJl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DmHYUF-VEema_r8Hg2-1dA" name="PK_ref_NbLits">
        <node defType="com.stambia.rdbms.colref" id="_DmHYUV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DmHYUl-VEema_r8Hg2-1dA" ref="#_DmCf0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DsMyMV-VEema_r8Hg2-1dA" name="ref_FormeJuridique">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DsMyMl-VEema_r8Hg2-1dA" value="ref_FormeJuridique"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DsMyM1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DsP1gF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsP1gV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsP1gl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DsP1g1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsP1hF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsP1hV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsP1hl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DsQckF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsQckV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsQckl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsQck1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsQclF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsQclV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DsRqsF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsRqsV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsRqsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsRqs1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsRqtF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsRqtV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DsWjMF-VEema_r8Hg2-1dA" name="PK_FormeJuridique">
        <node defType="com.stambia.rdbms.colref" id="_DsWjMV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DsWjMl-VEema_r8Hg2-1dA" ref="#_DsP1gF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DsaNkF-VEema_r8Hg2-1dA" name="ref_TypeEtabClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DsaNkV-VEema_r8Hg2-1dA" value="ref_TypeEtabClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DsaNkl-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DsdQ4F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsdQ4V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsdQ4l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DsdQ41-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsdQ5F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsdQ5V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsdQ5l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DsefAF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsefAV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsefAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsefA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsefBF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsefBV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DsgUMF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsgUMV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsgUMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsgUM1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsgUNF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsgUNV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DsiwcF-VEema_r8Hg2-1dA" name="PK_ref_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_DsiwcV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dsiwcl-VEema_r8Hg2-1dA" ref="#_DsdQ4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DxHucV-VEema_r8Hg2-1dA" name="ref_ComportementMultiCanal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DxHucl-VEema_r8Hg2-1dA" value="ref_ComportementMultiCanal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DxHuc1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DxKKsF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxKKsV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxKKsl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DxKKs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxKKtF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxKKtV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxKKtl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxLY0F-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxLY0V-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxLY0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxLY01-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxLY1F-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxLY1V-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DxNOAF-VEema_r8Hg2-1dA" name="PK_ref_ComportementMultiCanal">
        <node defType="com.stambia.rdbms.colref" id="_DxNOAV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DxNOAl-VEema_r8Hg2-1dA" ref="#_DxKKsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Df40gV-VEema_r8Hg2-1dA" name="ref_ColonneTarifaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Df40gl-VEema_r8Hg2-1dA" value="ref_ColonneTarifaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Df40g1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Df8e4F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Df8e4V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Df8e4l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Df8e41-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Df8e5F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Df8e5V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Df8e5l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Df-UEF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Df-UEV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Df-UEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Df-7IF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Df-7IV-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Df-7Il-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DgAJQF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DgAJQV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DgAJQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DgAJQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DgAJRF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DgAJRV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DgGP4F-VEema_r8Hg2-1dA" name="PK_ref_ColonneTarifaire">
        <node defType="com.stambia.rdbms.colref" id="_DgGP4V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DgGP4l-VEema_r8Hg2-1dA" ref="#_Df8e4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dpk8AF-VEema_r8Hg2-1dA" name="ref_TypeTel">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dpk8AV-VEema_r8Hg2-1dA" value="ref_TypeTel"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dpk8Al-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DpnYQF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpnYQV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpnYQl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DpnYQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpnYRF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpnYRV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpnYRl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DpomYF-VEema_r8Hg2-1dA" name="Type" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpomYV-VEema_r8Hg2-1dA" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpomYl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpomY1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpomZF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpomZV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DpqbkF-VEema_r8Hg2-1dA" name="PK_ref_TypeTel">
        <node defType="com.stambia.rdbms.colref" id="_DpqbkV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dpqbkl-VEema_r8Hg2-1dA" ref="#_DpnYQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Do94AV-VEema_r8Hg2-1dA" name="Client_Telephone">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Do94Al-VEema_r8Hg2-1dA" value="Client_Telephone"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Do94A1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DpCJcF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpCJcV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpCJcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DpCJc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpCJdF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpCJdV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpCJdl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DpDXkF-VEema_r8Hg2-1dA" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpDXkV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpDXkl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DpDXk1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpDXlF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpDXlV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpDXll-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DpElsF-VEema_r8Hg2-1dA" name="Numero" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpElsV-VEema_r8Hg2-1dA" value="Numero"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpElsl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpEls1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpEltF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpEltV-VEema_r8Hg2-1dA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DpFMwF-VEema_r8Hg2-1dA" name="IdType" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpFMwV-VEema_r8Hg2-1dA" value="IdType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpFMwl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DpFMw1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpFMxF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpFMxV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpFMxl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DpHpAF-VEema_r8Hg2-1dA" name="PK_Telephone">
        <node defType="com.stambia.rdbms.colref" id="_DpHpAV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DpHpAl-VEema_r8Hg2-1dA" ref="#_DpCJcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dz4HgF-VEema_r8Hg2-1dA" name="FK_Client_Telephone_Client">
        <node defType="com.stambia.rdbms.relation" id="_Dz4HgV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dz4Hgl-VEema_r8Hg2-1dA" ref="#_DpDXkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dz4Hg1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dz4HhF-VEema_r8Hg2-1dA" name="FK_Telephone_ref_TypeTel">
        <node defType="com.stambia.rdbms.relation" id="_Dz4HhV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dz4Hhl-VEema_r8Hg2-1dA" ref="#_DpFMwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdType?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dz4Hh1-VEema_r8Hg2-1dA" ref="#_DpnYQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DkN64V-VEema_r8Hg2-1dA" name="Client_HoraireOuv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DkN64l-VEema_r8Hg2-1dA" value="Client_HoraireOuv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DkN641-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DkXE0F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkXE0V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkXE0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkXE01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkXE1F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkXE1V-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkXE1l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkY6AF-VEema_r8Hg2-1dA" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkY6AV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkY6Al-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkY6A1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkY6BF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkY6BV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkY6Bl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkaIIF-VEema_r8Hg2-1dA" name="H_OuvertureLundi" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkaIIV-VEema_r8Hg2-1dA" value="H_OuvertureLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkavMF-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkavMV-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkavMl-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkavM1-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkavNF-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkbWQF-VEema_r8Hg2-1dA" name="H_FermeLundi" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkbWQV-VEema_r8Hg2-1dA" value="H_FermeLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkbWQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkbWQ1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkbWRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkbWRV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkbWRl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkckYF-VEema_r8Hg2-1dA" name="H_OuvertureLundi2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkckYV-VEema_r8Hg2-1dA" value="H_OuvertureLundi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkckYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkckY1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkckZF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkckZV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkckZl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkdLcF-VEema_r8Hg2-1dA" name="H_FermeLundi2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkdLcV-VEema_r8Hg2-1dA" value="H_FermeLundi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkdLcl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkdLc1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkdLdF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkdLdV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkdLdl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkdygF-VEema_r8Hg2-1dA" name="H_OuvertureMardi" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkdygV-VEema_r8Hg2-1dA" value="H_OuvertureMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkdygl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkdyg1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkdyhF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkdyhV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkdyhl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkfAoF-VEema_r8Hg2-1dA" name="H_FermeMardi" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkfAoV-VEema_r8Hg2-1dA" value="H_FermeMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkfAol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkfAo1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkfApF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkfApV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkfApl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkfnsF-VEema_r8Hg2-1dA" name="H_OuvertureMardi2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkfnsV-VEema_r8Hg2-1dA" value="H_OuvertureMardi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkfnsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkfns1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkfntF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkfntV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkfntl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dkg10F-VEema_r8Hg2-1dA" name="H_FermeMardi2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dkg10V-VEema_r8Hg2-1dA" value="H_FermeMardi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkg10l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkg101-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dkg11F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dkg11V-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkg11l-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dkhc4F-VEema_r8Hg2-1dA" name="H_OuvertureMercredi" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dkhc4V-VEema_r8Hg2-1dA" value="H_OuvertureMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkhc4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkhc41-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dkhc5F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dkhc5V-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkhc5l-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkirAF-VEema_r8Hg2-1dA" name="H_FermeMercredi" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkirAV-VEema_r8Hg2-1dA" value="H_FermeMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkirAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkirA1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkirBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkirBV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkirBl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DklHQF-VEema_r8Hg2-1dA" name="H_OuvertureMercredi2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_DklHQV-VEema_r8Hg2-1dA" value="H_OuvertureMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DklHQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DklHQ1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DklHRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DklHRV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DklHRl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkmVYF-VEema_r8Hg2-1dA" name="H_FermeMercredi2" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkmVYV-VEema_r8Hg2-1dA" value="H_FermeMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkmVYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkmVY1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkmVZF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkmVZV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkmVZl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dkm8cF-VEema_r8Hg2-1dA" name="H_OuvertureJeudi" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dkm8cV-VEema_r8Hg2-1dA" value="H_OuvertureJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkm8cl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkm8c1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dkm8dF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dkm8dV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkm8dl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkoKkF-VEema_r8Hg2-1dA" name="H_FermeJeudi" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkoKkV-VEema_r8Hg2-1dA" value="H_FermeJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkoKkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkoKk1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkoKlF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkoKlV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkoKll-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkoxoF-VEema_r8Hg2-1dA" name="H_OuvertureJeudi2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkoxoV-VEema_r8Hg2-1dA" value="H_OuvertureJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkoxol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkoxo1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkoxpF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkoxpV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkoxpl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkpYsF-VEema_r8Hg2-1dA" name="H_FermeJeudi2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkpYsV-VEema_r8Hg2-1dA" value="H_FermeJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkpYsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkpYs1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkpYtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkpYtV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkpYtl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dkqm0F-VEema_r8Hg2-1dA" name="H_OuvertureVendredi" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dkqm0V-VEema_r8Hg2-1dA" value="H_OuvertureVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkqm0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkqm01-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dkqm1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dkqm1V-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkqm1l-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkrN4F-VEema_r8Hg2-1dA" name="H_FermeVendredi" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkrN4V-VEema_r8Hg2-1dA" value="H_FermeVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkrN4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkrN41-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkrN5F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkrN5V-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkrN5l-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkscAF-VEema_r8Hg2-1dA" name="H_OuvertureVendredi2" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkscAV-VEema_r8Hg2-1dA" value="H_OuvertureVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkscAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkscA1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkscBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkscBV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkscBl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DktDEF-VEema_r8Hg2-1dA" name="H_FermeVendredi2" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_DktDEV-VEema_r8Hg2-1dA" value="H_FermeVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DktDEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DktDE1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DktDFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DktDFV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DktDFl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkuRMF-VEema_r8Hg2-1dA" name="H_OuvertureSamedi" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkuRMV-VEema_r8Hg2-1dA" value="H_OuvertureSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkuRMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkuRM1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkuRNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkuRNV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkuRNl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkvfUF-VEema_r8Hg2-1dA" name="H_FermeSamedi" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkvfUV-VEema_r8Hg2-1dA" value="H_FermeSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkvfUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkvfU1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkvfVF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkvfVV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkvfVl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkwtcF-VEema_r8Hg2-1dA" name="H_OuvertureSamedi2" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkwtcV-VEema_r8Hg2-1dA" value="H_OuvertureSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkwtcl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkwtc1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkwtdF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkwtdV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkwtdl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dkx7kF-VEema_r8Hg2-1dA" name="H_FermeSamedi2" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dkx7kV-VEema_r8Hg2-1dA" value="H_FermeSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkx7kl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkx7k1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dkx7lF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dkx7lV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkx7ll-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkyioF-VEema_r8Hg2-1dA" name="H_OuvertureDimanche" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkyioV-VEema_r8Hg2-1dA" value="H_OuvertureDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkyiol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkyio1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkyipF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkyipV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkyipl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkzwwF-VEema_r8Hg2-1dA" name="H_FermeDimanche" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkzwwV-VEema_r8Hg2-1dA" value="H_FermeDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dkzwwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dkzww1-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkzwxF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkzwxV-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dkzwxl-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dk0X0F-VEema_r8Hg2-1dA" name="H_OuvertureDimanche2" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dk0X0V-VEema_r8Hg2-1dA" value="H_OuvertureDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dk0X0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dk0X01-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dk0X1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dk0X1V-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dk0X1l-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dk1l8F-VEema_r8Hg2-1dA" name="H_FermeDimanche2" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dk1l8V-VEema_r8Hg2-1dA" value="H_FermeDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dk1l8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dk1l81-VEema_r8Hg2-1dA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dk1l9F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dk1l9V-VEema_r8Hg2-1dA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dk1l9l-VEema_r8Hg2-1dA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dk3bIF-VEema_r8Hg2-1dA" name="PK_Horaire_Ouverture">
        <node defType="com.stambia.rdbms.colref" id="_Dk3bIV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dk3bIl-VEema_r8Hg2-1dA" ref="#_DkY6AF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DznBwF-VEema_r8Hg2-1dA" name="FK_Client_HoraireOuv_Client">
        <node defType="com.stambia.rdbms.relation" id="_DznBwV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DznBwl-VEema_r8Hg2-1dA" ref="#_DkY6AF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DznBw1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DnAwMV-VEema_r8Hg2-1dA" name="Wrk_CustomerTable">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DnAwMl-VEema_r8Hg2-1dA" value="Wrk_CustomerTable"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DnAwM1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DnEakF-VEema_r8Hg2-1dA" name="CodeCli" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnEakV-VEema_r8Hg2-1dA" value="CodeCli"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnEakl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DnEak1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnEalF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnEalV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnEall-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DnFosF-VEema_r8Hg2-1dA" name="DateCreation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnFosV-VEema_r8Hg2-1dA" value="DateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnFosl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DnFos1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnFotF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnFotV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnFotl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DnG20F-VEema_r8Hg2-1dA" name="TimeCreation" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnG20V-VEema_r8Hg2-1dA" value="TimeCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnG20l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DnG201-VEema_r8Hg2-1dA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnG21F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnG21V-VEema_r8Hg2-1dA" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnG21l-VEema_r8Hg2-1dA" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DnIE8F-VEema_r8Hg2-1dA" name="Statut" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnIE8V-VEema_r8Hg2-1dA" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnIE8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnIE81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnIE9F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnIE9V-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DnJ6IF-VEema_r8Hg2-1dA" name="DateTraitement" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnJ6IV-VEema_r8Hg2-1dA" value="DateTraitement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnJ6Il-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DnJ6I1-VEema_r8Hg2-1dA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnJ6JF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnJ6JV-VEema_r8Hg2-1dA" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnJ6Jl-VEema_r8Hg2-1dA" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DnLvUF-VEema_r8Hg2-1dA" name="ProgrammeTRT" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnLvUV-VEema_r8Hg2-1dA" value="ProgrammeTRT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnLvUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnLvU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnLvVF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnLvVV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DnM9cF-VEema_r8Hg2-1dA" name="Action" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnM9cV-VEema_r8Hg2-1dA" value="Action"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnM9cl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnM9c1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnM9dF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnM9dV-VEema_r8Hg2-1dA" value="15"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DnTrIV-VEema_r8Hg2-1dA" name="ref_Canal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DnTrIl-VEema_r8Hg2-1dA" value="ref_Canal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DnTrI1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DnYjoF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnYjoV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnYjol-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DnYjo1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnYjpF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnYjpV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnYjpl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DnZxwF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnZxwV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnZxwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnZxw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnZxxF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnZxxV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dna_4F-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dna_4V-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dna_4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dna_41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dna_5F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dna_5V-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DneDMF-VEema_r8Hg2-1dA" name="PK_ref_Canal">
        <node defType="com.stambia.rdbms.colref" id="_DneDMV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DneDMl-VEema_r8Hg2-1dA" ref="#_DnYjoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DlnCAV-VEema_r8Hg2-1dA" name="ref_EnvTouristique">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DlnpEF-VEema_r8Hg2-1dA" value="ref_EnvTouristique"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DlnpEV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dlr6gF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dlr6gV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dlr6gl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dlr6g1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dlr6hF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dlr6hV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dlr6hl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DltvsF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DltvsV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dltvsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dltvs1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DltvtF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DltvtV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dlvk4F-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dlvk4V-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dlvk4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dlvk41-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dlvk5F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dlvk5V-VEema_r8Hg2-1dA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dlvk5l-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DlyoMF-VEema_r8Hg2-1dA" name="PK_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.colref" id="_DlyoMV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DlyoMl-VEema_r8Hg2-1dA" ref="#_Dlr6gF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DoTJoV-VEema_r8Hg2-1dA" name="Client_Organisation">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DoTJol-VEema_r8Hg2-1dA" value="Client_Organisation"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DoTJo1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DoXbEF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoXbEV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoXbEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DoXbE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoXbFF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoXbFV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoXbFl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DoZQQF-VEema_r8Hg2-1dA" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoZQQV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoZQQl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DoZQQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoZQRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoZQRV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoZQRl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DoZ3UF-VEema_r8Hg2-1dA" name="Secteur" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoZ3UV-VEema_r8Hg2-1dA" value="Secteur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoZ3Ul-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoZ3U1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoZ3VF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoZ3VV-VEema_r8Hg2-1dA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DobFcF-VEema_r8Hg2-1dA" name="Dic" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DobFcV-VEema_r8Hg2-1dA" value="Dic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DobFcl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DobFc1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DobFdF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DobFdV-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DobsgF-VEema_r8Hg2-1dA" name="Dec" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DobsgV-VEema_r8Hg2-1dA" value="Dec"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dobsgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dobsg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DobshF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DobshV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Doc6oF-VEema_r8Hg2-1dA" name="MagasinFavori" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_Doc6oV-VEema_r8Hg2-1dA" value="MagasinFavori"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Doc6ol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Doc6o1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Doc6pF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Doc6pV-VEema_r8Hg2-1dA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DodhsF-VEema_r8Hg2-1dA" name="CentraleAchat" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DodhsV-VEema_r8Hg2-1dA" value="CentraleAchat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dodhsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dodhs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DodhtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DodhtV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dodhtl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Doev0F-VEema_r8Hg2-1dA" name="AppelOffres" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_Doev0V-VEema_r8Hg2-1dA" value="AppelOffres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Doev0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Doev01-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Doev1F-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Doev1V-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DofW4F-VEema_r8Hg2-1dA" name="MarchePublic" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_DofW4V-VEema_r8Hg2-1dA" value="MarchePublic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DofW4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DofW41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DofW5F-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DofW5V-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DohMEF-VEema_r8Hg2-1dA" name="DateFinContrat" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_DohMEV-VEema_r8Hg2-1dA" value="DateFinContrat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DohMEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DohME1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DohMFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DohMFV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DohMFl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DohzIF-VEema_r8Hg2-1dA" name="TaciteReconduction" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_DohzIV-VEema_r8Hg2-1dA" value="TaciteReconduction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DohzIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DohzI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DohzJF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DohzJV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DojoUF-VEema_r8Hg2-1dA" name="MercuFerme" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_DojoUV-VEema_r8Hg2-1dA" value="MercuFerme"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DojoUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DojoU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DojoVF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DojoVV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DokPYF-VEema_r8Hg2-1dA" name="Mercuriale" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_DokPYV-VEema_r8Hg2-1dA" value="Mercuriale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DokPYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DokPY1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DokPZF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DokPZV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DoldgF-VEema_r8Hg2-1dA" name="FctParBudget" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoldgV-VEema_r8Hg2-1dA" value="FctParBudget"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Doldgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Doldg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoldhF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoldhV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DomEkF-VEema_r8Hg2-1dA" name="VisiteCcial" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_DomEkV-VEema_r8Hg2-1dA" value="VisiteCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DomEkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DomEk1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DomElF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DomElV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DonSsF-VEema_r8Hg2-1dA" name="AppelOutcall" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_DonSsV-VEema_r8Hg2-1dA" value="AppelOutcall"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DonSsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DonSs1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DonStF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DonStV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Don5wF-VEema_r8Hg2-1dA" name="PassageMag" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_Don5wV-VEema_r8Hg2-1dA" value="PassageMag"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Don5wl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Don5w1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Don5xF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Don5xV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DopH4F-VEema_r8Hg2-1dA" name="PrefOutcall" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_DopH4V-VEema_r8Hg2-1dA" value="PrefOutcall"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DopH4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DopH41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DopH5F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DopH5V-VEema_r8Hg2-1dA" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dopu8F-VEema_r8Hg2-1dA" name="PrefVisites" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dopu8V-VEema_r8Hg2-1dA" value="PrefVisites"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dopu8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dopu81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dopu9F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dopu9V-VEema_r8Hg2-1dA" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Doq9EF-VEema_r8Hg2-1dA" name="AdherentCtraleAch" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_Doq9EV-VEema_r8Hg2-1dA" value="AdherentCtraleAch"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Doq9El-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Doq9E1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Doq9FF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Doq9FV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DorkIF-VEema_r8Hg2-1dA" name="NumAdherent" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_DorkIV-VEema_r8Hg2-1dA" value="NumAdherent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DorkIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DorkI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DorkJF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DorkJV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DosyQF-VEema_r8Hg2-1dA" name="HoraireLivSouhait" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_DosyQV-VEema_r8Hg2-1dA" value="HoraireLivSouhait"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DosyQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DosyQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DosyRF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DosyRV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DotZUF-VEema_r8Hg2-1dA" name="NumCtraleAch" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_DotZUV-VEema_r8Hg2-1dA" value="NumCtraleAch"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DotZUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DotZU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DotZVF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DotZVV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DouncF-VEema_r8Hg2-1dA" name="TypeOp" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_DouncV-VEema_r8Hg2-1dA" value="TypeOp"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Douncl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dounc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoundF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoundV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Doundl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dov1kF-VEema_r8Hg2-1dA" name="MercuContrat" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dov1kV-VEema_r8Hg2-1dA" value="MercuContrat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dov1kl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dov1k1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dov1lF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dov1lV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DowcoF-VEema_r8Hg2-1dA" name="RespMercu" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_DowcoV-VEema_r8Hg2-1dA" value="RespMercu"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dowcol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dowco1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DowcpF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DowcpV-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DoxqwF-VEema_r8Hg2-1dA" name="FacturesDemat" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoxqwV-VEema_r8Hg2-1dA" value="FacturesDemat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Doxqwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Doxqw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoxqxF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoxqxV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Doy44F-VEema_r8Hg2-1dA" name="MailFacture" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_Doy44V-VEema_r8Hg2-1dA" value="MailFacture"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Doy44l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Doy441-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Doy45F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Doy45V-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dozf8F-VEema_r8Hg2-1dA" name="multicompte" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dozf8V-VEema_r8Hg2-1dA" value="multicompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dozf8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dozf81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dozf9F-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dozf9V-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Do0uEF-VEema_r8Hg2-1dA" name="Chaine" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_Do0uEV-VEema_r8Hg2-1dA" value="Chaine"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Do0uEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Do0uE1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Do0uFF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Do0uFV-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Do18MF-VEema_r8Hg2-1dA" name="Centralisateur" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_Do18MV-VEema_r8Hg2-1dA" value="Centralisateur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Do18Ml-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Do18M1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Do18NF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Do18NV-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Do2jQF-VEema_r8Hg2-1dA" name="HabitudeCmd" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_Do2jQV-VEema_r8Hg2-1dA" value="HabitudeCmd"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Do2jQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Do2jQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Do2jRF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Do3KUF-VEema_r8Hg2-1dA" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Do6NoF-VEema_r8Hg2-1dA" name="PK_Client_Organisation">
        <node defType="com.stambia.rdbms.colref" id="_Do6NoV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Do6Nol-VEema_r8Hg2-1dA" ref="#_DoXbEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dz25YF-VEema_r8Hg2-1dA" name="FK_Client_Organisation_Client">
        <node defType="com.stambia.rdbms.relation" id="_Dz25YV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dz25Yl-VEema_r8Hg2-1dA" ref="#_DoZQQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dz25Y1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dz25ZF-VEema_r8Hg2-1dA" name="FK_Client_Organisation_ref_TypeOperation">
        <node defType="com.stambia.rdbms.relation" id="_Dz25ZV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dz25Zl-VEema_r8Hg2-1dA" ref="#_DouncF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeOp?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dz25Z1-VEema_r8Hg2-1dA" ref="#_DyDikF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DvUXoV-VEema_r8Hg2-1dA" name="ref_AnneesExistence">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DvUXol-VEema_r8Hg2-1dA" value="ref_AnneesExistence"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DvUXo1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DvXa8F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvXa8V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvXa8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DvXa81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvXa9F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvXa9V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvXa9l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DvYpEF-VEema_r8Hg2-1dA" name="libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvYpEV-VEema_r8Hg2-1dA" value="libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvYpEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvYpE1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvYpFF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvYpFV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DvZ3MF-VEema_r8Hg2-1dA" name="AnneeMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvZ3MV-VEema_r8Hg2-1dA" value="AnneeMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvZ3Ml-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DvZ3M1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvZ3NF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvZ3NV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvZ3Nl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DvbFUF-VEema_r8Hg2-1dA" name="AnneeMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvbFUV-VEema_r8Hg2-1dA" value="AnneeMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvbFUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DvbFU1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvbFVF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvbFVV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvbFVl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DvevsF-VEema_r8Hg2-1dA" name="PK_ref_AnneesExistence">
        <node defType="com.stambia.rdbms.colref" id="_DvevsV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dvevsl-VEema_r8Hg2-1dA" ref="#_DvXa8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DgWukV-VEema_r8Hg2-1dA" name="ref_StatutCOLL">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DgXVoF-VEema_r8Hg2-1dA" value="ref_StatutCOLL"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DgXVoV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DgaY8F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DgaY8V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DgaY8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DgaY81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DgaY9F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DgaY9V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DgaY9l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DgeqYF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DgeqYV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DgeqYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DgeqY1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DgeqZF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DgeqZV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dgf4gF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dgf4gV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dgf4gl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dgf4g1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dgf4hF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dgf4hV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dgji4F-VEema_r8Hg2-1dA" name="PK_StatutCOLL">
        <node defType="com.stambia.rdbms.colref" id="_Dgji4V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dgji4l-VEema_r8Hg2-1dA" ref="#_DgaY8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DryigV-VEema_r8Hg2-1dA" name="Client_UserWeb_News">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dryigl-VEema_r8Hg2-1dA" value="Client_UserWeb_News"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dryig1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dr4pIF-VEema_r8Hg2-1dA" name="IdUserWeb" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dr4pIV-VEema_r8Hg2-1dA" value="IdUserWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dr4pIl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dr4pI1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dr4pJF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dr4pJV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dr4pJl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dr53QF-VEema_r8Hg2-1dA" name="IdNews" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dr53QV-VEema_r8Hg2-1dA" value="IdNews"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dr53Ql-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dr53Q1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dr53RF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dr53RV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dr53Rl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dr9hoF-VEema_r8Hg2-1dA" name="PK_Client_Contact_News">
        <node defType="com.stambia.rdbms.colref" id="_Dr9hoV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dr9hol-VEema_r8Hg2-1dA" ref="#_Dr4pIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUserWeb?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_Dr9ho1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dr9hpF-VEema_r8Hg2-1dA" ref="#_Dr53QF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdNews?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0GxAF-VEema_r8Hg2-1dA" name="FK_Client_UserWeb_News_Client_UserWeb">
        <node defType="com.stambia.rdbms.relation" id="_D0GxAV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0GxAl-VEema_r8Hg2-1dA" ref="#_Dr4pIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUserWeb?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0GxA1-VEema_r8Hg2-1dA" ref="#_DwifoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0GxBF-VEema_r8Hg2-1dA" name="FK_Client_UserWeb_News_ref_NewsletterType">
        <node defType="com.stambia.rdbms.relation" id="_D0GxBV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0GxBl-VEema_r8Hg2-1dA" ref="#_Dr53QF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdNews?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0GxB1-VEema_r8Hg2-1dA" ref="#_DwZVsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Duvv4V-VEema_r8Hg2-1dA" name="Client_SpeCulinaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DuwW8F-VEema_r8Hg2-1dA" value="Client_SpeCulinaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DuwW8V-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DuyzMF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuyzMV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuyzMl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DuyzM1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuyzNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuyzNV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuyzNl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DuzaQF-VEema_r8Hg2-1dA" name="IdSpeCulinaire" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuzaQV-VEema_r8Hg2-1dA" value="IdSpeCulinaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuzaQl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DuzaQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuzaRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuzaRV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuzaRl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Du12gF-VEema_r8Hg2-1dA" name="PK_Client_SpeCulinaire">
        <node defType="com.stambia.rdbms.colref" id="_Du12gV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Du12gl-VEema_r8Hg2-1dA" ref="#_DuyzMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_Du12g1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Du12hF-VEema_r8Hg2-1dA" ref="#_DuzaQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdSpeCulinaire?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0ZE4F-VEema_r8Hg2-1dA" name="FK_Client_SpeCulinaire_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0ZE4V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0ZE4l-VEema_r8Hg2-1dA" ref="#_DuyzMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0ZE41-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0ZE5F-VEema_r8Hg2-1dA" name="FK_Client_SpeCulinaire_ref_SpecialiteCulinaire1">
        <node defType="com.stambia.rdbms.relation" id="_D0ZE5V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0ZE5l-VEema_r8Hg2-1dA" ref="#_DuzaQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdSpeCulinaire?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0ZE51-VEema_r8Hg2-1dA" ref="#_DuIr4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DyAfQV-VEema_r8Hg2-1dA" name="ref_TypeOperation">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DyAfQl-VEema_r8Hg2-1dA" value="ref_TypeOperation"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DyAfQ1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DyDikF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyDikV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyDikl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DyDik1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyDilF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyDilV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyDill-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DyEwsF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyEwsV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyEwsl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyEws1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyEwtF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyEwtV-VEema_r8Hg2-1dA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DyJCIF-VEema_r8Hg2-1dA" name="PK_ref_TypeOperation">
        <node defType="com.stambia.rdbms.colref" id="_DyJCIV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DyJCIl-VEema_r8Hg2-1dA" ref="#_DyDikF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DysbwV-VEema_r8Hg2-1dA" name="ref_PerimetreProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dysbwl-VEema_r8Hg2-1dA" value="ref_PerimetreProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dysbw1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DyvfEF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyvfEV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyvfEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DyvfE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyvfFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyvfFV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyvfFl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DyxUQF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyxUQV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyxUQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyxUQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyxURF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyxURV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dyx7UF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dyx7UV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dyx7Ul-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dyx7U1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dyx7VF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dyx7VV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dy0-oF-VEema_r8Hg2-1dA" name="PK_ref_PerimetreDernierProjet">
        <node defType="com.stambia.rdbms.colref" id="_Dy0-oV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dy0-ol-VEema_r8Hg2-1dA" ref="#_DyvfEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dg6IMV-VEema_r8Hg2-1dA" name="ref_Commune">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dg6IMl-VEema_r8Hg2-1dA" value="ref_Commune"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dg6IM1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DhGVcF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhGVcV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhGVcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DhGVc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhGVdF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhGVdV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhGVdl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhIxsF-VEema_r8Hg2-1dA" name="IdDepartement" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhIxsV-VEema_r8Hg2-1dA" value="IdDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhIxsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DhIxs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhIxtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhIxtV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhIxtl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhR7oF-VEema_r8Hg2-1dA" name="NumINSEECommune" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhR7oV-VEema_r8Hg2-1dA" value="NumINSEECommune"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhR7ol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhR7o1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhR7pF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhR7pV-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhTw0F-VEema_r8Hg2-1dA" name="PrtculariteCom" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhTw0V-VEema_r8Hg2-1dA" value="PrtculariteCom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhTw0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhTw01-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhTw1F-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhTw1V-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhVmAF-VEema_r8Hg2-1dA" name="ComOuLieudit" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhVmAV-VEema_r8Hg2-1dA" value="ComOuLieudit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhVmAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhVmA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhVmBF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhVmBV-VEema_r8Hg2-1dA" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhXbMF-VEema_r8Hg2-1dA" name="CodePostal" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhXbMV-VEema_r8Hg2-1dA" value="CodePostal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhXbMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhXbM1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhXbNF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhXbNV-VEema_r8Hg2-1dA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhYpUF-VEema_r8Hg2-1dA" name="PrtculariteBDist" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhYpUV-VEema_r8Hg2-1dA" value="PrtculariteBDist"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhYpUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhYpU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhYpVF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhYpVV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhaegF-VEema_r8Hg2-1dA" name="LigneAcheminement" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhaegV-VEema_r8Hg2-1dA" value="LigneAcheminement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dhaegl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dhaeg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhaehF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhaehV-VEema_r8Hg2-1dA" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhcTsF-VEema_r8Hg2-1dA" name="CodeComptaPTT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhcTsV-VEema_r8Hg2-1dA" value="CodeComptaPTT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhcTsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhcTs1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhcTtF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhcTtV-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DheI4F-VEema_r8Hg2-1dA" name="Com" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_DheI4V-VEema_r8Hg2-1dA" value="Com"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DheI4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DheI41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DheI5F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DheI5V-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dhf-EF-VEema_r8Hg2-1dA" name="INSEEComRatache" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dhf-EV-VEema_r8Hg2-1dA" value="INSEEComRatache"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dhf-El-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dhf-E1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dhf-FF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dhf-FV-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DhjocF-VEema_r8Hg2-1dA" name="PK_ref_Commune">
        <node defType="com.stambia.rdbms.colref" id="_DhjocV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dhjocl-VEema_r8Hg2-1dA" ref="#_DhGVcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DpSoIF-VEema_r8Hg2-1dA" name="ref_NiveauStanding">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DpSoIV-VEema_r8Hg2-1dA" value="ref_NiveauStanding"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DpSoIl-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DpVEYF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpVEYV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpVEYl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DpVEY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpVEZF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpVEZV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpVEZl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DpWSgF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpWSgV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpWSgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpWSg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpWShF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpWShV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DpYuwF-VEema_r8Hg2-1dA" name="PK_NiveauStanding">
        <node defType="com.stambia.rdbms.colref" id="_DpYuwV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DpYuwl-VEema_r8Hg2-1dA" ref="#_DpVEYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DlPOkV-VEema_r8Hg2-1dA" name="Client_Adresse">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DlPOkl-VEema_r8Hg2-1dA" value="Client_Adresse"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DlPOk1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DlTgAF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlTgAV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlTgAl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DlTgA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlTgBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlTgBV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlTgBl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlUHEF-VEema_r8Hg2-1dA" name="Id" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlUHEV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlUHEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DlUHE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlUHFF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlUHFV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlUHFl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlVVMF-VEema_r8Hg2-1dA" name="Rue" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlVVMV-VEema_r8Hg2-1dA" value="Rue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlVVMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlVVM1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlVVNF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlVVNV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlWjUF-VEema_r8Hg2-1dA" name="Complement" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlWjUV-VEema_r8Hg2-1dA" value="Complement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlWjUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlWjU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlWjVF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlWjVV-VEema_r8Hg2-1dA" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlYYgF-VEema_r8Hg2-1dA" name="CodePostale" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlYYgV-VEema_r8Hg2-1dA" value="CodePostale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlYYgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlYYg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlYYhF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlYYhV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlZmoF-VEema_r8Hg2-1dA" name="BureauDistributeur" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlZmoV-VEema_r8Hg2-1dA" value="BureauDistributeur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlZmol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlZmo1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlZmpF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlZmpV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlaNsF-VEema_r8Hg2-1dA" name="CodeDepartement" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlaNsV-VEema_r8Hg2-1dA" value="CodeDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlaNsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlaNs1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlaNtF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlaNtV-VEema_r8Hg2-1dA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dlbb0F-VEema_r8Hg2-1dA" name="CodeCommune" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dlbb0V-VEema_r8Hg2-1dA" value="CodeCommune"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dlbb0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dlbb01-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dlbb1F-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dlbb1V-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dlcp8F-VEema_r8Hg2-1dA" name="Ville" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dlcp8V-VEema_r8Hg2-1dA" value="Ville"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dlcp8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dlcp81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dlcp9F-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dlcp9V-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DldRAF-VEema_r8Hg2-1dA" name="IdPays" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_DldRAV-VEema_r8Hg2-1dA" value="IdPays"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DldRAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DldRA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DldRBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DldRBV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DldRBl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlefIF-VEema_r8Hg2-1dA" name="TypeAdresse" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlefIV-VEema_r8Hg2-1dA" value="TypeAdresse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlefIl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DlefI1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlefJF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlefJV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlefJl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlfGMF-VEema_r8Hg2-1dA" name="CNT4" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlfGMV-VEema_r8Hg2-1dA" value="CNT4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlfGMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlfGM1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlfGNF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlfGNV-VEema_r8Hg2-1dA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlftQF-VEema_r8Hg2-1dA" name="IRIS" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlftQV-VEema_r8Hg2-1dA" value="IRIS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlftQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlftQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlftRF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlftRV-VEema_r8Hg2-1dA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dlg7YF-VEema_r8Hg2-1dA" name="TypeZone" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dlg7YV-VEema_r8Hg2-1dA" value="TypeZone"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dlg7Yl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dlg7Y1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dlg7ZF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dlg7ZV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dlg7Zl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlhicF-VEema_r8Hg2-1dA" name="EnvTouristique" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlhicV-VEema_r8Hg2-1dA" value="EnvTouristique"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dlhicl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dlhic1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlhidF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlhidV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dlhidl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dlj-sF-VEema_r8Hg2-1dA" name="PK_Adresse">
        <node defType="com.stambia.rdbms.colref" id="_Dlj-sV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dlj-sl-VEema_r8Hg2-1dA" ref="#_DlUHEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzqFEF-VEema_r8Hg2-1dA" name="FK_Client_Adresse_Client">
        <node defType="com.stambia.rdbms.relation" id="_DzqFEV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzqFEl-VEema_r8Hg2-1dA" ref="#_DlTgAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzqFE1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzqsIF-VEema_r8Hg2-1dA" name="FK_Client_Adresse_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.relation" id="_DzqsIV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzqsIl-VEema_r8Hg2-1dA" ref="#_DlhicF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=EnvTouristique?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzqsI1-VEema_r8Hg2-1dA" ref="#_Dlr6gF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzqsJF-VEema_r8Hg2-1dA" name="FK_Adresse_ref_Pays">
        <node defType="com.stambia.rdbms.relation" id="_DzqsJV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzqsJl-VEema_r8Hg2-1dA" ref="#_DldRAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdPays?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzqsJ1-VEema_r8Hg2-1dA" ref="#_DkAfgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzqsKF-VEema_r8Hg2-1dA" name="FK_Adresse_ref_TypeAdresse">
        <node defType="com.stambia.rdbms.relation" id="_DzqsKV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzqsKl-VEema_r8Hg2-1dA" ref="#_DlefIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeAdresse?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzqsK1-VEema_r8Hg2-1dA" ref="#_Dm5bcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzqsLF-VEema_r8Hg2-1dA" name="FK_Client_Adresse_ref_TypeZone">
        <node defType="com.stambia.rdbms.relation" id="_DzqsLV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzqsLl-VEema_r8Hg2-1dA" ref="#_Dlg7YF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeZone?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzqsL1-VEema_r8Hg2-1dA" ref="#_DvMb0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DmJ0kV-VEema_r8Hg2-1dA" name="Client_ActiviteSegment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DmKboF-VEema_r8Hg2-1dA" value="Client_ActiviteSegment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DmKboV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DmNe8F-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmNe8V-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmNe8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmNe81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmNe9F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmNe9V-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmNe9l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DmOGAF-VEema_r8Hg2-1dA" name="IdActivite" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DmOGAV-VEema_r8Hg2-1dA" value="IdActivite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DmOGAl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DmOGA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DmOGBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DmOGBV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DmOGBl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DmQiQF-VEema_r8Hg2-1dA" name="PK_Client_ActiviteSegment">
        <node defType="com.stambia.rdbms.colref" id="_DmQiQV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DmQiQl-VEema_r8Hg2-1dA" ref="#_DmNe8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_DmQiQ1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DmQiRF-VEema_r8Hg2-1dA" ref="#_DmOGAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdActivite?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzuWgF-VEema_r8Hg2-1dA" name="FK_Client_ActiviteSegment_Client">
        <node defType="com.stambia.rdbms.relation" id="_DzuWgV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzuWgl-VEema_r8Hg2-1dA" ref="#_DmNe8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzuWg1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzu9kF-VEema_r8Hg2-1dA" name="FK_Client_ActiviteSegment_ref_ActiviteSegment1">
        <node defType="com.stambia.rdbms.relation" id="_Dzu9kV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzu9kl-VEema_r8Hg2-1dA" ref="#_DmOGAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdActivite?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzu9k1-VEema_r8Hg2-1dA" ref="#_Du6vAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DgJ6QV-VEema_r8Hg2-1dA" name="ref_Societe">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DgJ6Ql-VEema_r8Hg2-1dA" value="ref_Societe"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DgJ6Q1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DgM9kF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DgM9kV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DgM9kl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DgM9k1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DgM9lF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DgM9lV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DgM9ll-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DgPZ0F-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DgPZ0V-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DgPZ0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DgPZ01-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DgPZ1F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DgPZ1V-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DgRPAF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DgRPAV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DgRPAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DgRPA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DgRPBF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DgRPBV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DgUSUF-VEema_r8Hg2-1dA" name="PK_ref_Societe">
        <node defType="com.stambia.rdbms.colref" id="_DgUSUV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DgUSUl-VEema_r8Hg2-1dA" ref="#_DgM9kF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DslMsV-VEema_r8Hg2-1dA" name="Client_PerimetreDernierProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DslMsl-VEema_r8Hg2-1dA" value="Client_PerimetreDernierProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DslMs1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DsoQAF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsoQAV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsoQAl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DsoQA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsoQBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsoQBV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsoQBl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DsqFMF-VEema_r8Hg2-1dA" name="IdUnivers" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsqFMV-VEema_r8Hg2-1dA" value="IdUnivers"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsqFMl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DsqFM1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsqFNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsqFNV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsqFNl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DsshcF-VEema_r8Hg2-1dA" name="PK_Client_PerimetreDernierProjet">
        <node defType="com.stambia.rdbms.colref" id="_DsshcV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dsshcl-VEema_r8Hg2-1dA" ref="#_DsoQAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_Dsshc1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DsshdF-VEema_r8Hg2-1dA" ref="#_DsqFMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUnivers?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0LCcF-VEema_r8Hg2-1dA" name="FK_Client_PerimetreDernierProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0LCcV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0LCcl-VEema_r8Hg2-1dA" ref="#_DsoQAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0LCc1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0LCdF-VEema_r8Hg2-1dA" name="FK_Client_PerimetreDernierProjet_ref_UniversConso">
        <node defType="com.stambia.rdbms.relation" id="_D0LCdV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0LCdl-VEema_r8Hg2-1dA" ref="#_DsqFMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUnivers?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0LCd1-VEema_r8Hg2-1dA" ref="#_DfXQEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DxPDMV-VEema_r8Hg2-1dA" name="ref_Concurrent">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DxPqQF-VEema_r8Hg2-1dA" value="ref_Concurrent"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DxPqQV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DxSGgF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxSGgV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxSGgl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DxSGg1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxSGhF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxSGhV-VEema_r8Hg2-1dA" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxSGhl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxStkF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxStkV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxStkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxTUoF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxTUoV-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxTUol-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxT7sF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxT7sV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxT7sl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxT7s1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxT7tF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxT7tV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DxVw4F-VEema_r8Hg2-1dA" name="PK_ref_Concurrent">
        <node defType="com.stambia.rdbms.colref" id="_DxVw4V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DxVw4l-VEema_r8Hg2-1dA" ref="#_DxSGgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dx5xkF-VEema_r8Hg2-1dA" name="Mercuriales">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dx5xkV-VEema_r8Hg2-1dA" value="Mercuriales"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dx5xkl-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dx6_sF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dx6_sV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dx6_sl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dx6_s1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dx6_tF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dx6_tV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dx6_tl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dx7mwF-VEema_r8Hg2-1dA" name="NomMercu" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dx7mwV-VEema_r8Hg2-1dA" value="NomMercu"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dx7mwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dx7mw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dx7mxF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dx7mxV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dx-DAF-VEema_r8Hg2-1dA" name="PK_Mercuriales">
        <node defType="com.stambia.rdbms.colref" id="_Dx-DAV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dx-DAl-VEema_r8Hg2-1dA" ref="#_Dx6_sF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dh7b4V-VEema_r8Hg2-1dA" name="Client">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dh7b4l-VEema_r8Hg2-1dA" value="Client"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dh7b41-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DjPDcF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjPDcV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjPDcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjPDc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjPDdF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjPDdV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjPDdl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjQ4oF-VEema_r8Hg2-1dA" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjQ4oV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjQ4ol-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjQ4o1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjQ4pF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjQ4pV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjQ4pl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjSGwF-VEema_r8Hg2-1dA" name="IdSociete" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjSGwV-VEema_r8Hg2-1dA" value="IdSociete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjSGwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjSGw1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjSGxF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjSGxV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjSGxl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjTU4F-VEema_r8Hg2-1dA" name="CodeSocMagento" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjTU4V-VEema_r8Hg2-1dA" value="CodeSocMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjTU4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjTU41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjTU5F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjTU5V-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjUjAF-VEema_r8Hg2-1dA" name="RaisonSociale" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjUjAV-VEema_r8Hg2-1dA" value="RaisonSociale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjUjAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjUjA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjUjBF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjUjBV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjVxIF-VEema_r8Hg2-1dA" name="Enseigne" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjVxIV-VEema_r8Hg2-1dA" value="Enseigne"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjVxIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjVxI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjVxJF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjVxJV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjWYMF-VEema_r8Hg2-1dA" name="Siren" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjWYMV-VEema_r8Hg2-1dA" value="Siren"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjWYMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjWYM1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjWYNF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjWYNV-VEema_r8Hg2-1dA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjXmUF-VEema_r8Hg2-1dA" name="Siret" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjXmUV-VEema_r8Hg2-1dA" value="Siret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjXmUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjXmU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjXmVF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjXmVV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjYNYF-VEema_r8Hg2-1dA" name="Nic" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjYNYV-VEema_r8Hg2-1dA" value="Nic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjYNYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjYNY1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjYNZF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjYNZV-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjZbgF-VEema_r8Hg2-1dA" name="Tva" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjZbgV-VEema_r8Hg2-1dA" value="Tva"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjZbgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjZbg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjZbhF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjZbhV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjaCkF-VEema_r8Hg2-1dA" name="Naf" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjaCkV-VEema_r8Hg2-1dA" value="Naf"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjaCkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjaCk1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjaClF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjaClV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjbQsF-VEema_r8Hg2-1dA" name="TypeEtab1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjbQsV-VEema_r8Hg2-1dA" value="TypeEtab1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjbQsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjbQs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjbQtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjbQtV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjbQtl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Djb3wF-VEema_r8Hg2-1dA" name="TypeEtab2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_Djb3wV-VEema_r8Hg2-1dA" value="TypeEtab2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Djb3wl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Djb3w1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Djb3xF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Djb3xV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Djb3xl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjdF4F-VEema_r8Hg2-1dA" name="TypeEtab3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjdF4V-VEema_r8Hg2-1dA" value="TypeEtab3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjdF4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjdF41-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjdF5F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjdF5V-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjdF5l-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Djds8F-VEema_r8Hg2-1dA" name="DateCreation" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_Djds8V-VEema_r8Hg2-1dA" value="DateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Djds8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Djds81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Djds9F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Djds9V-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Djds9l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dje7EF-VEema_r8Hg2-1dA" name="SiteWeb" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dje7EV-VEema_r8Hg2-1dA" value="SiteWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dje7El-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dje7E1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dje7FF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dje7FV-VEema_r8Hg2-1dA" value="350"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjfiIF-VEema_r8Hg2-1dA" name="Langue" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjfiIV-VEema_r8Hg2-1dA" value="Langue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjfiIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjfiI1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjfiJF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjfiJV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjfiJl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjgwQF-VEema_r8Hg2-1dA" name="TypeCompte" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjgwQV-VEema_r8Hg2-1dA" value="TypeCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjgwQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjgwQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjgwRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjgwRV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjgwRl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjhXUF-VEema_r8Hg2-1dA" name="Groupe" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjhXUV-VEema_r8Hg2-1dA" value="Groupe"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjhXUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjhXU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjhXVF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjhXVV-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjilcF-VEema_r8Hg2-1dA" name="International" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjilcV-VEema_r8Hg2-1dA" value="International"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Djilcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Djilc1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjildF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjildV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjjMgF-VEema_r8Hg2-1dA" name="Directif" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjjMgV-VEema_r8Hg2-1dA" value="Directif"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjjMgl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjjMg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjjMhF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjjMhV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjkaoF-VEema_r8Hg2-1dA" name="DateCrtCompte" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjkaoV-VEema_r8Hg2-1dA" value="DateCrtCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Djkaol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Djkao1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjkapF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjkapV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Djkapl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjlBsF-VEema_r8Hg2-1dA" name="Statut" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjlBsV-VEema_r8Hg2-1dA" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjlBsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjlBs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjlBtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjlBtV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjlBtl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjmP0F-VEema_r8Hg2-1dA" name="CanalAcquisition" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjmP0V-VEema_r8Hg2-1dA" value="CanalAcquisition"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjmP0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjmP01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjmP1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjmP1V-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjmP1l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Djm24F-VEema_r8Hg2-1dA" name="DateMaj_CHD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_Djm24V-VEema_r8Hg2-1dA" value="DateMaj_CHD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Djm24l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Djm241-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Djm25F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Djm25V-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Djm25l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjoFAF-VEema_r8Hg2-1dA" name="DateMaj_Orion" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjoFAV-VEema_r8Hg2-1dA" value="DateMaj_Orion"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjoFAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjoFA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjoFBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjoFBV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjoFBl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjosEF-VEema_r8Hg2-1dA" name="DateMaj_Ccial" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjosEV-VEema_r8Hg2-1dA" value="DateMaj_Ccial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjosEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjosE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjosFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjosFV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjosFl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Djp6MF-VEema_r8Hg2-1dA" name="CompteChomette" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_Djp6MV-VEema_r8Hg2-1dA" value="CompteChomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Djp6Ml-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Djp6M1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Djp6NF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Djp6NV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjrIUF-VEema_r8Hg2-1dA" name="DateSuppression" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjrIUV-VEema_r8Hg2-1dA" value="DateSuppression"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjrIUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjrIU1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjrIVF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjrIVV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjrIVl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjsWcF-VEema_r8Hg2-1dA" name="TopSuppression" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjsWcV-VEema_r8Hg2-1dA" value="TopSuppression"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjsWcl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjsWc1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjsWdF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjsWdV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjtkkF-VEema_r8Hg2-1dA" name="CHDID" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjtkkV-VEema_r8Hg2-1dA" value="CHDID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Djtkkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Djtkk1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjtklF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjtklV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjuLoF-VEema_r8Hg2-1dA" name="Latitude" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjuLoV-VEema_r8Hg2-1dA" value="Latitude"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjuLol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjuLo1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjuLpF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjuLpV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjvZwF-VEema_r8Hg2-1dA" name="Longitude" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjvZwV-VEema_r8Hg2-1dA" value="Longitude"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjvZwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjvZw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjvZxF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjvZxV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Djwn4F-VEema_r8Hg2-1dA" name="Propriete" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_Djwn4V-VEema_r8Hg2-1dA" value="Propriete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Djwn4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Djwn41-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Djwn5F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Djwn5V-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Djwn5l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjxO8F-VEema_r8Hg2-1dA" name="Nature" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjxO8V-VEema_r8Hg2-1dA" value="Nature"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjxO8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjxO81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjxO9F-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjxO9V-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjydEF-VEema_r8Hg2-1dA" name="DateMaj_Web" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjydEV-VEema_r8Hg2-1dA" value="DateMaj_Web"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjydEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DjydE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjydFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjydFV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjydFl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DjzrMF-VEema_r8Hg2-1dA" name="MailPrincipal" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_DjzrMV-VEema_r8Hg2-1dA" value="MailPrincipal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DjzrMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DjzrM1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DjzrNF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DjzrNV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dj2ugF-VEema_r8Hg2-1dA" name="PK_Client">
        <node defType="com.stambia.rdbms.colref" id="_Dj2ugV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dj2ugl-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzj-cF-VEema_r8Hg2-1dA" name="FK_Client_ref_Canal">
        <node defType="com.stambia.rdbms.relation" id="_Dzj-cV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzj-cl-VEema_r8Hg2-1dA" ref="#_DjmP0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=CanalAcquisition?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzj-c1-VEema_r8Hg2-1dA" ref="#_DnYjoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzj-dF-VEema_r8Hg2-1dA" name="FK_Client_ref_Langue">
        <node defType="com.stambia.rdbms.relation" id="_Dzj-dV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzj-dl-VEema_r8Hg2-1dA" ref="#_DjfiIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Langue?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzj-d1-VEema_r8Hg2-1dA" ref="#_DvnSkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzj-eF-VEema_r8Hg2-1dA" name="FK_Client_ref_Propriété">
        <node defType="com.stambia.rdbms.relation" id="_Dzj-eV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzj-el-VEema_r8Hg2-1dA" ref="#_Djwn4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Propriete?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzj-e1-VEema_r8Hg2-1dA" ref="#_Dv5mcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzj-fF-VEema_r8Hg2-1dA" name="FK_Client_ref_Societe">
        <node defType="com.stambia.rdbms.relation" id="_Dzj-fV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzj-fl-VEema_r8Hg2-1dA" ref="#_DjSGwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdSociete?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzj-f1-VEema_r8Hg2-1dA" ref="#_DgM9kF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzj-gF-VEema_r8Hg2-1dA" name="FK_Client_ref_StatutClient">
        <node defType="com.stambia.rdbms.relation" id="_Dzj-gV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzj-gl-VEema_r8Hg2-1dA" ref="#_DjlBsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Statut?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzj-g1-VEema_r8Hg2-1dA" ref="#_DoMb8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzklgF-VEema_r8Hg2-1dA" name="FK_Client_ref_TypeCompte">
        <node defType="com.stambia.rdbms.relation" id="_DzklgV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzklgl-VEema_r8Hg2-1dA" ref="#_DjgwQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeCompte?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzklg1-VEema_r8Hg2-1dA" ref="#_DxC18F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzklhF-VEema_r8Hg2-1dA" name="FK_Client_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_DzklhV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzklhl-VEema_r8Hg2-1dA" ref="#_DjbQsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtab1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzklh1-VEema_r8Hg2-1dA" ref="#_Duq3YF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzkliF-VEema_r8Hg2-1dA" name="FK_Client_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.relation" id="_DzkliV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzklil-VEema_r8Hg2-1dA" ref="#_Djb3wF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtab2?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzkli1-VEema_r8Hg2-1dA" ref="#_DuiUgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzkljF-VEema_r8Hg2-1dA" name="FK_Client_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.relation" id="_DzkljV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzkljl-VEema_r8Hg2-1dA" ref="#_DjdF4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtab3?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzklj1-VEema_r8Hg2-1dA" ref="#_DuROwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DwFzsV-VEema_r8Hg2-1dA" name="Client_MoisOuvert">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DwFzsl-VEema_r8Hg2-1dA" value="Client_MoisOuvert"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DwFzs1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DwI3AF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwI3AV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwI3Al-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwI3A1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwI3BF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwI3BV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwI3Bl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwJeEF-VEema_r8Hg2-1dA" name="IdMois" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwJeEV-VEema_r8Hg2-1dA" value="IdMois"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwJeEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwJeE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwJeFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwJeFV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwJeFl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DwL6UF-VEema_r8Hg2-1dA" name="PK_Client_MoisOuvert">
        <node defType="com.stambia.rdbms.colref" id="_DwL6UV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DwL6Ul-VEema_r8Hg2-1dA" ref="#_DwI3AF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_DwL6U1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DwL6VF-VEema_r8Hg2-1dA" ref="#_DwJeEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdMois?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0hnwF-VEema_r8Hg2-1dA" name="FK_Client_MoisOuvert_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0hnwV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0hnwl-VEema_r8Hg2-1dA" ref="#_DwI3AF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0hnw1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0hnxF-VEema_r8Hg2-1dA" name="FK_Client_MoisOuvert_ref_Mois1">
        <node defType="com.stambia.rdbms.relation" id="_D0hnxV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0hnxl-VEema_r8Hg2-1dA" ref="#_DwJeEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdMois?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0hnx1-VEema_r8Hg2-1dA" ref="#_DgqQkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DtfysV-VEema_r8Hg2-1dA" name="Client_Concurrent">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dtfysl-VEema_r8Hg2-1dA" value="Client_Concurrent"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dtfys1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DtjdEF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtjdEV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtjdEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DtjdE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtjdFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtjdFV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtjdFl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DtkrMF-VEema_r8Hg2-1dA" name="IdConcurrent" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtkrMV-VEema_r8Hg2-1dA" value="IdConcurrent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtkrMl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DtkrM1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtkrNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtkrNV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtkrNl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DtoVkF-VEema_r8Hg2-1dA" name="PK_Client_Concurrent">
        <node defType="com.stambia.rdbms.colref" id="_DtoVkV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DtoVkl-VEema_r8Hg2-1dA" ref="#_DtjdEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_DtoVk1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DtoVlF-VEema_r8Hg2-1dA" ref="#_DtkrMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdConcurrent?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0RJEF-VEema_r8Hg2-1dA" name="FK_Client_Concurrent_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0RJEV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0RJEl-VEema_r8Hg2-1dA" ref="#_DtjdEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0RJE1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0RJFF-VEema_r8Hg2-1dA" name="FK_Client_Concurrent_ref_Concurrent1">
        <node defType="com.stambia.rdbms.relation" id="_D0RJFV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0RJFl-VEema_r8Hg2-1dA" ref="#_DtkrMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdConcurrent?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0RJF1-VEema_r8Hg2-1dA" ref="#_DxSGgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dun0EV-VEema_r8Hg2-1dA" name="ref_TypeEtabNiv1">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dun0El-VEema_r8Hg2-1dA" value="ref_TypeEtabNiv1"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dun0E1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Duq3YF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Duq3YV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Duq3Yl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Duq3Y1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Duq3ZF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Duq3ZV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Duq3Zl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DurecF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DurecV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Durecl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Durec1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuredF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuredV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dut6sF-VEema_r8Hg2-1dA" name="PK_TypeEtablissement">
        <node defType="com.stambia.rdbms.colref" id="_Dut6sV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dut6sl-VEema_r8Hg2-1dA" ref="#_Duq3YF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DvA1oF-VEema_r8Hg2-1dA" name="ref_NiveauUser">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DvA1oV-VEema_r8Hg2-1dA" value="ref_NiveauUser"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DvA1ol-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DvDR4F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvDR4V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvDR4l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DvDR41-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvDR5F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvDR5V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvDR5l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DvD48F-VEema_r8Hg2-1dA" name="LibNiveau" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvD48V-VEema_r8Hg2-1dA" value="LibNiveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvD48l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvD481-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvD49F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvD49V-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DvGVMF-VEema_r8Hg2-1dA" name="PK_ref_NiveauUser">
        <node defType="com.stambia.rdbms.colref" id="_DvGVMV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DvGVMl-VEema_r8Hg2-1dA" ref="#_DvDR4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DwNvgV-VEema_r8Hg2-1dA" name="ref_Segment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DwNvgl-VEema_r8Hg2-1dA" value="ref_Segment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DwNvg1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DwQLwF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwQLwV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwQy0F-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwQy0V-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwQy0l-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwQy01-VEema_r8Hg2-1dA" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwQy1F-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwRZ4F-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwRZ4V-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwRZ4l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwRZ41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwRZ5F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwRZ5V-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DwT2IF-VEema_r8Hg2-1dA" name="PK_ref_Segment">
        <node defType="com.stambia.rdbms.colref" id="_DwT2IV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DwT2Il-VEema_r8Hg2-1dA" ref="#_DwQLwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DtE78V-VEema_r8Hg2-1dA" name="ref_RepasServis">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DtE78l-VEema_r8Hg2-1dA" value="ref_RepasServis"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DtE781-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DtJ0cF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtJ0cV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtJ0cl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DtJ0c1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtJ0dF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtJ0dV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtJ0dl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DtKbgF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtKbgV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtKbgl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtKbg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtKbhF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtKbhV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DtLpoF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtLpoV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtLpol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtLpo1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtLppF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtLppV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DtOs8F-VEema_r8Hg2-1dA" name="PK_ref_RepasServi">
        <node defType="com.stambia.rdbms.colref" id="_DtOs8V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DtOs8l-VEema_r8Hg2-1dA" ref="#_DtJ0cF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DgmmMF-VEema_r8Hg2-1dA" name="ref_Mois">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DgmmMV-VEema_r8Hg2-1dA" value="ref_Mois"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DgmmMl-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DgqQkF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DgqQkV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DgqQkl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DgqQk1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DgqQlF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DgqQlV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DgqQll-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DgresF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DgresV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dgresl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dgres1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DgretF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DgretV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DgxlUF-VEema_r8Hg2-1dA" name="PK_ref_Mois">
        <node defType="com.stambia.rdbms.colref" id="_DgxlUV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DgxlUl-VEema_r8Hg2-1dA" ref="#_DgqQkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dtr_8V-VEema_r8Hg2-1dA" name="ref_ClientType">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dtr_8l-VEema_r8Hg2-1dA" value="ref_ClientType"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dtr_81-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DtwRYF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtwRYV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtwRYl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DtwRY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtwRZF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtwRZV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtwRZl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DtxfgF-VEema_r8Hg2-1dA" name="LibTypeClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtxfgV-VEema_r8Hg2-1dA" value="LibTypeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dtxfgl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dtxfg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtyGkF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtyGkV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dt0i0F-VEema_r8Hg2-1dA" name="PK_ref_ClientType">
        <node defType="com.stambia.rdbms.colref" id="_Dt0i0V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dt0i0l-VEema_r8Hg2-1dA" ref="#_DtwRYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DsDBMV-VEema_r8Hg2-1dA" name="Client_ActualiteProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DsDBMl-VEema_r8Hg2-1dA" value="Client_ActualiteProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DsDBM1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DsHSoF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsHSoV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsHSol-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DsHSo1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsHSpF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsHSpV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsHSpl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DsIgwF-VEema_r8Hg2-1dA" name="IdProjet" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsIgwV-VEema_r8Hg2-1dA" value="IdProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsIgwl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DsIgw1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsIgxF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsIgxV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsIgxl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DsK9AF-VEema_r8Hg2-1dA" name="PK_Client_ActualiteProjet">
        <node defType="com.stambia.rdbms.colref" id="_DsK9AV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DsK9Al-VEema_r8Hg2-1dA" ref="#_DsHSoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_DsK9A1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DsK9BF-VEema_r8Hg2-1dA" ref="#_DsIgwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdProjet?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0H_IF-VEema_r8Hg2-1dA" name="FK_Client_ActualiteProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0H_IV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0H_Il-VEema_r8Hg2-1dA" ref="#_DsHSoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0H_I1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0H_JF-VEema_r8Hg2-1dA" name="FK_Client_ActualiteProjet_ref_ActualiteProjet1">
        <node defType="com.stambia.rdbms.relation" id="_D0H_JV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0H_Jl-VEema_r8Hg2-1dA" ref="#_DsIgwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdProjet?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0H_J1-VEema_r8Hg2-1dA" ref="#_DpvUEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dj6_8V-VEema_r8Hg2-1dA" name="ref_Pays">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dj6_8l-VEema_r8Hg2-1dA" value="ref_Pays"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dj7nAF-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DkAfgF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkAfgV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkAfgl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkAfg1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkAfhF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkAfhV-VEema_r8Hg2-1dA" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkAfhl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkCUsF-VEema_r8Hg2-1dA" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkCUsV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkCUsl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DkCUs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkCUtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkCUtV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkCUtl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkEJ4F-VEema_r8Hg2-1dA" name="Alpha2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkEJ4V-VEema_r8Hg2-1dA" value="Alpha2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkEJ4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkEJ41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkEJ5F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkEJ5V-VEema_r8Hg2-1dA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkFYAF-VEema_r8Hg2-1dA" name="Alpha3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkFYAV-VEema_r8Hg2-1dA" value="Alpha3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkFYAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkFYA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkFYBF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkFYBV-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkGmIF-VEema_r8Hg2-1dA" name="Nom_en_gb" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkGmIV-VEema_r8Hg2-1dA" value="Nom_en_gb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkGmIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkGmI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkGmJF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkGmJV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DkH0QF-VEema_r8Hg2-1dA" name="Nom_fr_fr" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DkH0QV-VEema_r8Hg2-1dA" value="Nom_fr_fr"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DkH0Ql-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DkH0Q1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DkH0RF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DkH0RV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DkK3kF-VEema_r8Hg2-1dA" name="PK_ref_Pays">
        <node defType="com.stambia.rdbms.colref" id="_DkK3kV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DkK3kl-VEema_r8Hg2-1dA" ref="#_DkAfgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DsuWoV-VEema_r8Hg2-1dA" name="ref_CHD_TypeEtab">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DsuWol-VEema_r8Hg2-1dA" value="ref_CHD_TypeEtab"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DsuWo1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DsxZ8F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsxZ8V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsxZ8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DsxZ81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsxZ9F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsxZ9V-VEema_r8Hg2-1dA" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsxZ9l-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DsyoEF-VEema_r8Hg2-1dA" name="GFCCode" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DsyoEV-VEema_r8Hg2-1dA" value="GFCCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DsyoEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DsyoE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DsyoFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DsyoFV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DsyoFl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DszPIF-VEema_r8Hg2-1dA" name="TypeEtabN1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DszPIV-VEema_r8Hg2-1dA" value="TypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DszPIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DszPI1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DszPJF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DszPJV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DszPJl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ds0dQF-VEema_r8Hg2-1dA" name="TypeEtabN2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ds0dQV-VEema_r8Hg2-1dA" value="TypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ds0dQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ds0dQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ds0dRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ds0dRV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ds0dRl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ds1EUF-VEema_r8Hg2-1dA" name="TypeEtabN3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ds1EUV-VEema_r8Hg2-1dA" value="TypeEtabN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ds1EUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ds1EU1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ds1EVF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ds1EVV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ds1EVl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Ds3gkF-VEema_r8Hg2-1dA" name="PK_ref_CHD_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_Ds3gkV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Ds3gkl-VEema_r8Hg2-1dA" ref="#_DsxZ8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0M3oF-VEema_r8Hg2-1dA" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_D0M3oV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0M3ol-VEema_r8Hg2-1dA" ref="#_DszPIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtabN1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0M3o1-VEema_r8Hg2-1dA" ref="#_Duq3YF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0M3pF-VEema_r8Hg2-1dA" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.relation" id="_D0M3pV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0M3pl-VEema_r8Hg2-1dA" ref="#_Ds0dQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtabN2?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0M3p1-VEema_r8Hg2-1dA" ref="#_DuiUgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0M3qF-VEema_r8Hg2-1dA" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.relation" id="_D0M3qV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0M3ql-VEema_r8Hg2-1dA" ref="#_Ds1EUF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtabN3?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0M3q1-VEema_r8Hg2-1dA" ref="#_DuROwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DqqhIF-VEema_r8Hg2-1dA" name="Wrk_ChometteUsers">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DqqhIV-VEema_r8Hg2-1dA" value="Wrk_ChometteUsers"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DqqhIl-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DqtkcF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqtkcV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dqtkcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dqtkc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqtkdF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqtkdV-VEema_r8Hg2-1dA" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dqtkdl-VEema_r8Hg2-1dA" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DqvZoF-VEema_r8Hg2-1dA" name="CodeMagento" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqvZoV-VEema_r8Hg2-1dA" value="CodeMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqvZol-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DqvZo1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqvZpF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqvZpV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqvZpl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DqwnwF-VEema_r8Hg2-1dA" name="CodeMagentoUser" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqwnwV-VEema_r8Hg2-1dA" value="CodeMagentoUser"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dqwnwl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dqwnw1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqwnxF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqwnxV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dqwnxl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DqxO0F-VEema_r8Hg2-1dA" name="CodeClient" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqxO0V-VEema_r8Hg2-1dA" value="CodeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqxO0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DqxO01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqxO1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqxO1V-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqxO1l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DqzEAF-VEema_r8Hg2-1dA" name="FileName" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqzEAV-VEema_r8Hg2-1dA" value="FileName"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqzEAl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqzEA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqzEBF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqzEBV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DqzrEF-VEema_r8Hg2-1dA" name="dateCreation" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dq0SIF-VEema_r8Hg2-1dA" value="dateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dq0SIV-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dq0SIl-VEema_r8Hg2-1dA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dq0SI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dq0SJF-VEema_r8Hg2-1dA" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dq0SJV-VEema_r8Hg2-1dA" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dq5KoF-VEema_r8Hg2-1dA" name="Statut" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dq5KoV-VEema_r8Hg2-1dA" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dq5Kol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dq5Ko1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dq5KpF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dq5KpV-VEema_r8Hg2-1dA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dq6YwF-VEema_r8Hg2-1dA" name="Mode" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dq6YwV-VEema_r8Hg2-1dA" value="Mode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dq6Ywl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dq6Yw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dq6YxF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dq6YxV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dq81AF-VEema_r8Hg2-1dA" name="PK_Wrk_ChometteUsers">
        <node defType="com.stambia.rdbms.colref" id="_Dq81AV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dq81Al-VEema_r8Hg2-1dA" ref="#_DqtkcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dt2_EV-VEema_r8Hg2-1dA" name="wrk_Log_WS_Integration_Commande">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dt2_El-VEema_r8Hg2-1dA" value="wrk_Log_WS_Integration_Commande"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dt2_E1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dt6CYF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dt6CYV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dt6CYl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dt6CY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dt6CZF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dt6CZV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dt6CZl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dt7QgF-VEema_r8Hg2-1dA" name="SessionId" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dt7QgV-VEema_r8Hg2-1dA" value="SessionId"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dt7Qgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dt7Qg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dt7QhF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dt7QhV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dt73kF-VEema_r8Hg2-1dA" name="InputXML" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dt73kV-VEema_r8Hg2-1dA" value="InputXML"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dt73kl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dt73k1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dt73lF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dt73lV-VEema_r8Hg2-1dA" value="max"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dt9swF-VEema_r8Hg2-1dA" name="OutputResponse" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dt9swV-VEema_r8Hg2-1dA" value="OutputResponse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dt9swl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dt9sw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dt9sxF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dt9sxV-VEema_r8Hg2-1dA" value="500"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DuAwEF-VEema_r8Hg2-1dA" name="RequestDate" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuAwEV-VEema_r8Hg2-1dA" value="RequestDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuAwEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DuAwE1-VEema_r8Hg2-1dA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuAwFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuAwFV-VEema_r8Hg2-1dA" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuAwFl-VEema_r8Hg2-1dA" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DuDMUF-VEema_r8Hg2-1dA" name="PK_wrk_Log_WS_Integration_Commande">
        <node defType="com.stambia.rdbms.colref" id="_DuDMUV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DuDMUl-VEema_r8Hg2-1dA" ref="#_Dt6CYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DlBzMV-VEema_r8Hg2-1dA" name="Client_UserWebTel">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DlCaQF-VEema_r8Hg2-1dA" value="Client_UserWebTel"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DlCaQV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DlGEoF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlGEoV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlGEol-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DlGEo1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlGEpF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlGEpV-VEema_r8Hg2-1dA" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlGEpl-VEema_r8Hg2-1dA" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlH50F-VEema_r8Hg2-1dA" name="IdUser" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlH50V-VEema_r8Hg2-1dA" value="IdUser"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlH50l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DlH501-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlH51F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlH51V-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlH51l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlJH8F-VEema_r8Hg2-1dA" name="Numero" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlJH8V-VEema_r8Hg2-1dA" value="Numero"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlJH8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlJH81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlJH9F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlJH9V-VEema_r8Hg2-1dA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DlJvAF-VEema_r8Hg2-1dA" name="Idtype_Tel" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DlJvAV-VEema_r8Hg2-1dA" value="Idtype_Tel"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DlJvAl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DlJvA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DlJvBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DlJvBV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DlJvBl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DlMyUF-VEema_r8Hg2-1dA" name="PK_Client_UserWebTel">
        <node defType="com.stambia.rdbms.colref" id="_DlMyUV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DlMyUl-VEema_r8Hg2-1dA" ref="#_DlGEoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzo28F-VEema_r8Hg2-1dA" name="FK_Client_UserWebTel_Client_UserWeb">
        <node defType="com.stambia.rdbms.relation" id="_Dzo28V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzo28l-VEema_r8Hg2-1dA" ref="#_DlH50F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUser?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzo281-VEema_r8Hg2-1dA" ref="#_DwifoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzo29F-VEema_r8Hg2-1dA" name="FK_Client_UserWebTel_ref_TypeTel">
        <node defType="com.stambia.rdbms.relation" id="_Dzo29V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzo29l-VEema_r8Hg2-1dA" ref="#_DlJvAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Idtype_Tel?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzo291-VEema_r8Hg2-1dA" ref="#_DpnYQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DyV2cV-VEema_r8Hg2-1dA" name="ref_NbEmployes">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DyV2cl-VEema_r8Hg2-1dA" value="ref_NbEmployes"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DyWdgF-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DyZg0F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyZg0V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyZg0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DyZg01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyZg1F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyZg1V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyZg1l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dyau8F-VEema_r8Hg2-1dA" name="NbEmployes" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dyau8V-VEema_r8Hg2-1dA" value="NbEmployes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dyau8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dyau81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dyau9F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dyau9V-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DybWAF-VEema_r8Hg2-1dA" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DybWAV-VEema_r8Hg2-1dA" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DybWAl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DybWA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DybWBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DybWBV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DybWBl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DyckIF-VEema_r8Hg2-1dA" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyckIV-VEema_r8Hg2-1dA" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyckIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DyckI1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyckJF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyckJV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyckJl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DyfncF-VEema_r8Hg2-1dA" name="PK_ref_NbEmployes">
        <node defType="com.stambia.rdbms.colref" id="_DyfncV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dyfncl-VEema_r8Hg2-1dA" ref="#_DyZg0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DvIxcV-VEema_r8Hg2-1dA" name="ref_TypeZone">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DvIxcl-VEema_r8Hg2-1dA" value="ref_TypeZone"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DvIxc1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DvMb0F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvMb0V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvMb0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DvMb01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvMb1F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvMb1V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvMb1l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DvNp8F-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvNp8V-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvNp8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvNp81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvNp9F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvNp9V-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DvO4EF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvO4EV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvO4El-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DvO4E1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvO4FF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvO4FV-VEema_r8Hg2-1dA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvO4Fl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DvRUUF-VEema_r8Hg2-1dA" name="PK_TypeZone">
        <node defType="com.stambia.rdbms.colref" id="_DvRUUV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DvRUUl-VEema_r8Hg2-1dA" ref="#_DvMb0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DxuycV-VEema_r8Hg2-1dA" name="ref_TypeComSouhaite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dxuycl-VEema_r8Hg2-1dA" value="ref_TypeComSouhaite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dxuyc1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dxyc0F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dxyc0V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxyc0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dxyc01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dxyc1F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dxyc1V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dxyc1l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dxzq8F-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dxzq8V-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxzq8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dxzq81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dxzq9F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dxzq9V-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dx1gIF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dx1gIV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dx1gIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dx1gI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dx1gJF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dx1gJV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dx4jcF-VEema_r8Hg2-1dA" name="PK_ref_TypeComSouhaite">
        <node defType="com.stambia.rdbms.colref" id="_Dx4jcV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dx4jcl-VEema_r8Hg2-1dA" ref="#_Dxyc0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DyiDsV-VEema_r8Hg2-1dA" name="ref_TrancheCA">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DyiDsl-VEema_r8Hg2-1dA" value="ref_TrancheCA"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DyiDs1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DyluEF-VEema_r8Hg2-1dA" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyluEV-VEema_r8Hg2-1dA" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyluEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DyluE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyluFF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyluFV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyluFl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DymVIF-VEema_r8Hg2-1dA" name="TrancheCA" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DymVIV-VEema_r8Hg2-1dA" value="TrancheCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DymVIl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DymVI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DymVJF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DymVJV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DynjQF-VEema_r8Hg2-1dA" name="TrancheCAMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DynjQV-VEema_r8Hg2-1dA" value="TrancheCAMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DynjQl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DynjQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DynjRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DynjRV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DynjRl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DyoKUF-VEema_r8Hg2-1dA" name="TrancheCAMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyoKUV-VEema_r8Hg2-1dA" value="TrancheCAMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyoKUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DyoKU1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyoKVF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyoKVV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyoKVl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DyqmkF-VEema_r8Hg2-1dA" name="PK_ref_TrancheCA">
        <node defType="com.stambia.rdbms.colref" id="_DyqmkV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dyqmkl-VEema_r8Hg2-1dA" ref="#_DyluEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dnuh4V-VEema_r8Hg2-1dA" name="ref_TicketRepasMoyen">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dnuh4l-VEema_r8Hg2-1dA" value="ref_TicketRepasMoyen"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dnuh41-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DnzaYF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DnzaYV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DnzaYl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DnzaY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DnzaZF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DnzaZV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DnzaZl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dn1PkF-VEema_r8Hg2-1dA" name="TicketMoy" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dn1PkV-VEema_r8Hg2-1dA" value="TicketMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dn1Pkl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dn1Pk1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dn1PlF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dn1PlV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dn2dsF-VEema_r8Hg2-1dA" name="MntMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dn2dsV-VEema_r8Hg2-1dA" value="MntMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dn2dsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dn2ds1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dn2dtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dn2dtV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dn2dtl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dn3r0F-VEema_r8Hg2-1dA" name="MntMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dn3r0V-VEema_r8Hg2-1dA" value="MntMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dn3r0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dn3r01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dn3r1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dn3r1V-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dn3r1l-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dn6vIF-VEema_r8Hg2-1dA" name="PK_ref_TicketRepasMoyen">
        <node defType="com.stambia.rdbms.colref" id="_Dn6vIV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dn6vIl-VEema_r8Hg2-1dA" ref="#_DnzaYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DfgaAV-VEema_r8Hg2-1dA" name="ref_NiveauFidelite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DfgaAl-VEema_r8Hg2-1dA" value="ref_NiveauFidelite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DfgaA1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DfkEYF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfkEYV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfkEYl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DfkEY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfkEZF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfkEZV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfkEZl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DflSgF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DflSgV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DflSgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DflSg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DflShF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DflShV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfmgoF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfmgoV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dfmgol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dfmgo1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfmgpF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfmgpV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dfo84F-VEema_r8Hg2-1dA" name="PK_ref_NiveauFidelite">
        <node defType="com.stambia.rdbms.colref" id="_Dfo84V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dfo84l-VEema_r8Hg2-1dA" ref="#_DfkEYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DfSXkV-VEema_r8Hg2-1dA" name="ref_UniversConso">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DfSXkl-VEema_r8Hg2-1dA" value="ref_UniversConso"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DfSXk1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DfXQEF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfXQEV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfXQEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DfXQE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfXQFF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfXQFV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfXQFl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfYeMF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfYeMV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfYeMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfYeM1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfYeNF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfYeNV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DfcvoF-VEema_r8Hg2-1dA" name="PK_ref_UniversConso">
        <node defType="com.stambia.rdbms.colref" id="_DfcvoV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dfcvol-VEema_r8Hg2-1dA" ref="#_DfXQEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DqPDUV-VEema_r8Hg2-1dA" name="Client_JourOuvert">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DqPDUl-VEema_r8Hg2-1dA" value="Client_JourOuvert"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DqPDU1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DqStsF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqStsV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqStsl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DqSts1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqSttF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqSttV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqSttl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DqUi4F-VEema_r8Hg2-1dA" name="IdJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqUi4V-VEema_r8Hg2-1dA" value="IdJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqUi4l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DqUi41-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqUi5F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqUi5V-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqUi5l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DqY0UF-VEema_r8Hg2-1dA" name="PK_Client_JourOuvert">
        <node defType="com.stambia.rdbms.colref" id="_DqY0UV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DqY0Ul-VEema_r8Hg2-1dA" ref="#_DqStsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_DqY0U1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DqY0VF-VEema_r8Hg2-1dA" ref="#_DqUi4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdJour?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dz_cQF-VEema_r8Hg2-1dA" name="FK_Client_JourOuvert_Client">
        <node defType="com.stambia.rdbms.relation" id="_Dz_cQV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dz_cQl-VEema_r8Hg2-1dA" ref="#_DqStsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dz_cQ1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dz_cRF-VEema_r8Hg2-1dA" name="FK_Client_JourOuvert_ref_Jours1">
        <node defType="com.stambia.rdbms.relation" id="_Dz_cRV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dz_cRl-VEema_r8Hg2-1dA" ref="#_DqUi4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dz_cR1-VEema_r8Hg2-1dA" ref="#_DnkJ0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dl1EcV-VEema_r8Hg2-1dA" name="ref_ScoringFinance">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dl1Ecl-VEema_r8Hg2-1dA" value="ref_ScoringFinance"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dl1Ec1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dl4HwF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dl4HwV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dl4Hwl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dl4Hw1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dl4HxF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dl4HxV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dl4Hxl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dl5V4F-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dl5V4V-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dl5V4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dl5V41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dl5V5F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dl5V5V-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dl6kAF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dl6kAV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dl6kAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dl6kA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dl6kBF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dl6kBV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dl9AQF-VEema_r8Hg2-1dA" name="PK_ref_ScoringFinance">
        <node defType="com.stambia.rdbms.colref" id="_Dl9AQV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dl9AQl-VEema_r8Hg2-1dA" ref="#_Dl4HwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dhn54V-VEema_r8Hg2-1dA" name="Wrk_ChometteCustomers">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dhn54l-VEema_r8Hg2-1dA" value="Wrk_ChometteCustomers"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dhn541-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DhsyYF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhsyYV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhsyYl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DhsyY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhsyZF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhsyZV-VEema_r8Hg2-1dA" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhsyZl-VEema_r8Hg2-1dA" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhuAgF-VEema_r8Hg2-1dA" name="CodeMagento" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhuAgV-VEema_r8Hg2-1dA" value="CodeMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhuAgl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DhuAg1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhuAhF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhuAhV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhuAhl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhvOoF-VEema_r8Hg2-1dA" name="CodeClient" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhvOoV-VEema_r8Hg2-1dA" value="CodeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DhvOol-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DhvOo1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DhvOpF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhvOpV-VEema_r8Hg2-1dA" value="bigint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhvOpl-VEema_r8Hg2-1dA" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DhwcwF-VEema_r8Hg2-1dA" name="FileName" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DhwcwV-VEema_r8Hg2-1dA" value="FileName"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dhwcwl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dhwcw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DhwcxF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DhwcxV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dhxq4F-VEema_r8Hg2-1dA" name="TypeFile" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dhxq4V-VEema_r8Hg2-1dA" value="TypeFile"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dhxq4l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dhxq41-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dhxq5F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dhxq5V-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dhxq5l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dhy5AF-VEema_r8Hg2-1dA" name="dateCreation" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dhy5AV-VEema_r8Hg2-1dA" value="dateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dhy5Al-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dhy5A1-VEema_r8Hg2-1dA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dhy5BF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dhy5BV-VEema_r8Hg2-1dA" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dhy5Bl-VEema_r8Hg2-1dA" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dh0HIF-VEema_r8Hg2-1dA" name="Statut" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dh0HIV-VEema_r8Hg2-1dA" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dh0HIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dh0HI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dh0HJF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dh0HJV-VEema_r8Hg2-1dA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dh1VQF-VEema_r8Hg2-1dA" name="NewCode" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dh1VQV-VEema_r8Hg2-1dA" value="NewCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dh1VQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dh1VQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dh1VRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dh1VRV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dh1VRl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dh2jYF-VEema_r8Hg2-1dA" name="Mode" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dh2jYV-VEema_r8Hg2-1dA" value="Mode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dh2jYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dh2jY1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dh2jZF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dh2jZV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dh5msF-VEema_r8Hg2-1dA" name="PK_Wrk_ChometteCustomers">
        <node defType="com.stambia.rdbms.colref" id="_Dh5msV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dh5msl-VEema_r8Hg2-1dA" ref="#_DhsyYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DtRwQV-VEema_r8Hg2-1dA" name="Infos_CHD">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DtRwQl-VEema_r8Hg2-1dA" value="Infos_CHD"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DtRwQ1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DtX24F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtX24V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtX24l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DtX241-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtX25F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtX25V-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtX25l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DtZFAF-VEema_r8Hg2-1dA" name="ParamRequest" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtZFAV-VEema_r8Hg2-1dA" value="ParamRequest"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtZFAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtZFA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtZFBF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtZFBV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DtZsEF-VEema_r8Hg2-1dA" name="TextResponse" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtZsEV-VEema_r8Hg2-1dA" value="TextResponse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtZsEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtZsE1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtZsFF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtZsFV-VEema_r8Hg2-1dA" value="max"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dta6MF-VEema_r8Hg2-1dA" name="CreateDate" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dta6MV-VEema_r8Hg2-1dA" value="CreateDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dta6Ml-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dta6M1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dta6NF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dta6NV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dta6Nl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DtdWcF-VEema_r8Hg2-1dA" name="PK_Infos_CHD">
        <node defType="com.stambia.rdbms.colref" id="_DtdWcV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DtdWcl-VEema_r8Hg2-1dA" ref="#_DtX24F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DviaEV-VEema_r8Hg2-1dA" name="ref_Langue">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DvjBIF-VEema_r8Hg2-1dA" value="ref_Langue"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DvjBIV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DvnSkF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvnSkV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvnSkl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DvnSk1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvnSlF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvnSlV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvnSll-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dvn5oF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dvn5oV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dvn5ol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dvn5o1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dvn5pF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dvn5pV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DvpHwF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvpHwV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvpHwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvpHw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvpHxF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvpHxV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dvq88F-VEema_r8Hg2-1dA" name="PK_Langue">
        <node defType="com.stambia.rdbms.colref" id="_Dvq88V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dvq88l-VEema_r8Hg2-1dA" ref="#_DvnSkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dp2o0V-VEema_r8Hg2-1dA" name="ref_NiveauConfiance">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dp3P4F-VEema_r8Hg2-1dA" value="ref_NiveauConfiance"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dp3P4V-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dp66QF-VEema_r8Hg2-1dA" name="IdNivConfiance" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dp66QV-VEema_r8Hg2-1dA" value="IdNivConfiance"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dp66Ql-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dp66Q1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dp66RF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dp66RV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dp66Rl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dp9WgF-VEema_r8Hg2-1dA" name="Niveau" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dp9WgV-VEema_r8Hg2-1dA" value="Niveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dp9Wgl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dp9Wg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dp9WhF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dp9WhV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dp_ywF-VEema_r8Hg2-1dA" name="PK_ref_NiveauConfiance">
        <node defType="com.stambia.rdbms.colref" id="_Dp_ywV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dp_ywl-VEema_r8Hg2-1dA" ref="#_Dp66QF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdNivConfiance?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DuOLcV-VEema_r8Hg2-1dA" name="ref_TypeEtabNiv3">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DuOLcl-VEema_r8Hg2-1dA" value="ref_TypeEtabNiv3"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DuOLc1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DuROwF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuROwV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuROwl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DuROw1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuROxF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuROxV-VEema_r8Hg2-1dA" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuROxl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DuR10F-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuR10V-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuR10l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuR101-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuR11F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuR11V-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DuTD8F-VEema_r8Hg2-1dA" name="IdTypeEtabN2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuTD8V-VEema_r8Hg2-1dA" value="IdTypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuTD8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DuTD81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuTD9F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuTD9V-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuTD9l-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DuVgMF-VEema_r8Hg2-1dA" name="PK_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.colref" id="_DuVgMV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DuVgMl-VEema_r8Hg2-1dA" ref="#_DuROwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DuXVYV-VEema_r8Hg2-1dA" name="Client_PerimetreProchainProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DuX8cF-VEema_r8Hg2-1dA" value="Client_PerimetreProchainProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DuX8cV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DuaYsF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuaYsV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuaYsl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DuaYs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuaYtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuaYtV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuaYtl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dubm0F-VEema_r8Hg2-1dA" name="IdUnivers" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dubm0V-VEema_r8Hg2-1dA" value="IdUnivers"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dubm0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dubm01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dubm1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dubm1V-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dubm1l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DudcAF-VEema_r8Hg2-1dA" name="PK_Client_PerimetreProchainProjet">
        <node defType="com.stambia.rdbms.colref" id="_DudcAV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DudcAl-VEema_r8Hg2-1dA" ref="#_DuaYsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_DudcA1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DudcBF-VEema_r8Hg2-1dA" ref="#_Dubm0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUnivers?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0WBkF-VEema_r8Hg2-1dA" name="FK_Client_PerimetreProchainProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0WBkV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0WBkl-VEema_r8Hg2-1dA" ref="#_DuaYsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0WBk1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0WBlF-VEema_r8Hg2-1dA" name="FK_Client_PerimetreProchainProjet_ref_UniversConso">
        <node defType="com.stambia.rdbms.relation" id="_D0WBlV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0WBll-VEema_r8Hg2-1dA" ref="#_Dubm0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUnivers?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0WBl1-VEema_r8Hg2-1dA" ref="#_DfXQEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DufRMV-VEema_r8Hg2-1dA" name="ref_TypeEtabNiv2">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DufRMl-VEema_r8Hg2-1dA" value="ref_TypeEtabNiv2"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DufRM1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DuiUgF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuiUgV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuiUgl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DuiUg1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuiUhF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuiUhV-VEema_r8Hg2-1dA" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuiUhl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dui7kF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dui7kV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dui7kl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dui7k1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dui7lF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dui7lV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DukJsF-VEema_r8Hg2-1dA" name="IdTypeEtabN1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DukJsV-VEema_r8Hg2-1dA" value="IdTypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DukJsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DukJs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DukJtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DukJtV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DukJtl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dul-4F-VEema_r8Hg2-1dA" name="PK_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.colref" id="_Dul-4V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dul-4l-VEema_r8Hg2-1dA" ref="#_DuiUgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0XPsF-VEema_r8Hg2-1dA" name="FK_ref_TypeEtabNiv2_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_D0XPsV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0XPsl-VEema_r8Hg2-1dA" ref="#_DukJsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdTypeEtabN1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0XPs1-VEema_r8Hg2-1dA" ref="#_Duq3YF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dk53YV-VEema_r8Hg2-1dA" name="Client_RepasServis">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dk53Yl-VEema_r8Hg2-1dA" value="Client_RepasServis"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dk53Y1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dk8ToF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dk8ToV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dk8Tol-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dk8To1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dk8TpF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dk8TpV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dk8Tpl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dk86sF-VEema_r8Hg2-1dA" name="IdRepas" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dk86sV-VEema_r8Hg2-1dA" value="IdRepas"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dk86sl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dk86s1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dk86tF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dk86tV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dk86tl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dk_W8F-VEema_r8Hg2-1dA" name="PK_Client_RepasServis">
        <node defType="com.stambia.rdbms.colref" id="_Dk_W8V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dk_W8l-VEema_r8Hg2-1dA" ref="#_Dk8ToF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_Dk_W81-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dk_W9F-VEema_r8Hg2-1dA" ref="#_Dk86sF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdRepas?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzno0F-VEema_r8Hg2-1dA" name="FK_Client_RepasServis_Client">
        <node defType="com.stambia.rdbms.relation" id="_Dzno0V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzno0l-VEema_r8Hg2-1dA" ref="#_Dk8ToF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzno01-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Dzno1F-VEema_r8Hg2-1dA" name="FK_Client_RepasServis_ref_RepasServis1">
        <node defType="com.stambia.rdbms.relation" id="_Dzno1V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Dzno1l-VEema_r8Hg2-1dA" ref="#_Dk86sF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdRepas?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Dzno11-VEema_r8Hg2-1dA" ref="#_DtJ0cF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DeaN0F-VEema_r8Hg2-1dA" name="Client_PrestationOfferte">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DeaN0V-VEema_r8Hg2-1dA" value="Client_PrestationOfferte"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DeaN0l-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Ded4MF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ded4MV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ded4Ml-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ded4M1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ded4NF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ded4NV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ded4Nl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DefGUF-VEema_r8Hg2-1dA" name="IdPrestation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DefGUV-VEema_r8Hg2-1dA" value="IdPrestation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DefGUl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DefGU1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DefGVF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DefGVV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DefGVl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DeiJoF-VEema_r8Hg2-1dA" name="PK_Client_PrestationOfferte">
        <node defType="com.stambia.rdbms.colref" id="_DeiJoV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DeiwsF-VEema_r8Hg2-1dA" ref="#_Ded4MF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_DeiwsV-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Deiwsl-VEema_r8Hg2-1dA" ref="#_DefGUF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdPrestation?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzUG0F-VEema_r8Hg2-1dA" name="FK_Client_PrestationOfferte_Client">
        <node defType="com.stambia.rdbms.relation" id="_DzUG0V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzUG0l-VEema_r8Hg2-1dA" ref="#_Ded4MF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzUG01-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzUG1F-VEema_r8Hg2-1dA" name="FK_Client_PrestationOfferte_ref_PrestationsOffertes1">
        <node defType="com.stambia.rdbms.relation" id="_DzUG1V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzUG1l-VEema_r8Hg2-1dA" ref="#_DefGUF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdPrestation?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzUG11-VEema_r8Hg2-1dA" ref="#_DpdnQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DmzU0V-VEema_r8Hg2-1dA" name="ref_TypeAdresse">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DmzU0l-VEema_r8Hg2-1dA" value="ref_TypeAdresse"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DmzU01-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dm5bcF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dm5bcV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dm5bcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dm5bc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dm5bdF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dm5bdV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dm5bdl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dm6pkF-VEema_r8Hg2-1dA" name="Type" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dm6pkV-VEema_r8Hg2-1dA" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dm6pkl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dm6pk1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dm6plF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dm6plV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dm9s4F-VEema_r8Hg2-1dA" name="PK_ref_TypeAdresse">
        <node defType="com.stambia.rdbms.colref" id="_Dm9s4V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dm9s4l-VEema_r8Hg2-1dA" ref="#_Dm5bcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_De7LMV-VEema_r8Hg2-1dA" name="Client_Activite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_De7LMl-VEema_r8Hg2-1dA" value="Client_Activite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_De7LM1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_De-1kF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_De-1kV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_De-1kl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_De-1k1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_De-1lF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_De-1lV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_De-1ll-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfADsF-VEema_r8Hg2-1dA" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfADsV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfAqwF-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DfAqwV-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfAqwl-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfAqw1-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfAqxF-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfBR0F-VEema_r8Hg2-1dA" name="ActifMoisClosN" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfBR0V-VEema_r8Hg2-1dA" value="ActifMoisClosN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfBR0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfBR01-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfBR1F-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfBR1V-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfDHAF-VEema_r8Hg2-1dA" name="ActifMoisClosN_1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfDHAV-VEema_r8Hg2-1dA" value="ActifMoisClosN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfDHAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfDHA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfDHBF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfDHBV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfEVIF-VEema_r8Hg2-1dA" name="Actif12DM" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfEVIV-VEema_r8Hg2-1dA" value="Actif12DM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfEVIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfEVI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfEVJF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfEVJV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfFjQF-VEema_r8Hg2-1dA" name="Actif13_24DM" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfFjQV-VEema_r8Hg2-1dA" value="Actif13_24DM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfFjQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfFjQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfFjRF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfFjRV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfGKUF-VEema_r8Hg2-1dA" name="ActifN" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfGKUV-VEema_r8Hg2-1dA" value="ActifN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfGKUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfGKU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfGKVF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfGKVV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfHYcF-VEema_r8Hg2-1dA" name="ActifPeriodeN_1" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfHYcV-VEema_r8Hg2-1dA" value="ActifPeriodeN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfHYcl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfHYc1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfHYdF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfHYdV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfImkF-VEema_r8Hg2-1dA" name="ActifN_1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfImkV-VEema_r8Hg2-1dA" value="ActifN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfImkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfImk1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfImlF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfImlV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfJ0sF-VEema_r8Hg2-1dA" name="ActifN_2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfJ0sV-VEema_r8Hg2-1dA" value="ActifN_2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfJ0sl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfJ0s1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfJ0tF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfJ0tV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfKbwF-VEema_r8Hg2-1dA" name="ActifN_3" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfKbwV-VEema_r8Hg2-1dA" value="ActifN_3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfKbwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfKbw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfKbxF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfKbxV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DfOGIF-VEema_r8Hg2-1dA" name="PK_Client_Activite">
        <node defType="com.stambia.rdbms.colref" id="_DfOGIV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DfOGIl-VEema_r8Hg2-1dA" ref="#_De-1kF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzYYQF-VEema_r8Hg2-1dA" name="FK_Client_Activite_Client">
        <node defType="com.stambia.rdbms.relation" id="_DzYYQV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzYYQl-VEema_r8Hg2-1dA" ref="#_DfADsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzYYQ1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dy3a4V-VEema_r8Hg2-1dA" name="ref_NbEleves">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dy3a4l-VEema_r8Hg2-1dA" value="ref_NbEleves"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dy3a41-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dy6eMF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dy6eMV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dy6eMl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dy6eM1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dy6eNF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dy6eNV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dy6eNl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dy7FQF-VEema_r8Hg2-1dA" name="NbEleves" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dy7FQV-VEema_r8Hg2-1dA" value="NbEleves"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dy7FQl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dy7FQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dy7FRF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dy7FRV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dy8TYF-VEema_r8Hg2-1dA" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dy8TYV-VEema_r8Hg2-1dA" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dy8TYl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dy8TY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dy8TZF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dy8TZV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dy8TZl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dy86cF-VEema_r8Hg2-1dA" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dy86cV-VEema_r8Hg2-1dA" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dy86cl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dy86c1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dy86dF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dy86dV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dy86dl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dy_WsF-VEema_r8Hg2-1dA" name="PK_ref_NbEleves">
        <node defType="com.stambia.rdbms.colref" id="_Dy_WsV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dy_Wsl-VEema_r8Hg2-1dA" ref="#_Dy6eMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dn9ycV-VEema_r8Hg2-1dA" name="ref_NbRepasJour">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dn9ycl-VEema_r8Hg2-1dA" value="ref_NbRepasJour"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dn9yc1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DoA1wF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoA1wV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoA1wl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DoA1w1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoA1xF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoA1xV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoA1xl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DoCD4F-VEema_r8Hg2-1dA" name="NbRepasJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoCD4V-VEema_r8Hg2-1dA" value="NbRepasJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoCD4l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoCD41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoCD5F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoCD5V-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DoDSAF-VEema_r8Hg2-1dA" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoDSAV-VEema_r8Hg2-1dA" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoDSAl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DoDSA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoDSBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoDSBV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoDSBl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DoEgIF-VEema_r8Hg2-1dA" name="nbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DoEgIV-VEema_r8Hg2-1dA" value="nbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DoEgIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DoEgI1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DoEgJF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DoEgJV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DoEgJl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DoG8YF-VEema_r8Hg2-1dA" name="PK_ref_NbRepasJour">
        <node defType="com.stambia.rdbms.colref" id="_DoG8YV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DoG8Yl-VEema_r8Hg2-1dA" ref="#_DoA1wF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DpKFQV-VEema_r8Hg2-1dA" name="ref_TypeClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DpKFQl-VEema_r8Hg2-1dA" value="ref_TypeClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DpKFQ1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DpNIkF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpNIkV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpNIkl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DpNIk1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpNIlF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpNIlV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpNIll-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DpOWsF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpOWsV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpOWsl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpOWs1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpOWtF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpOWtV-VEema_r8Hg2-1dA" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DpQL4F-VEema_r8Hg2-1dA" name="PK_ref_TypeClient">
        <node defType="com.stambia.rdbms.colref" id="_DpQL4V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DpQL4l-VEema_r8Hg2-1dA" ref="#_DpNIkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dpaj8V-VEema_r8Hg2-1dA" name="ref_PrestationsOffertes">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dpaj8l-VEema_r8Hg2-1dA" value="ref_PrestationsOffertes"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dpaj81-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DpdnQF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpdnQV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpdnQl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DpdnQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpdnRF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpdnRV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpdnRl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dpe1YF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dpe1YV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dpe1Yl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dpe1Y1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dpe1ZF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dpe1ZV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DpgDgF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpgDgV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpgDgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpgDg1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpgDhF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpgDhV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DpifwF-VEema_r8Hg2-1dA" name="PK_ref_PrestationsOffertes">
        <node defType="com.stambia.rdbms.colref" id="_DpifwV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dpifwl-VEema_r8Hg2-1dA" ref="#_DpdnQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DxYNIV-VEema_r8Hg2-1dA" name="Client_BI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DxYNIl-VEema_r8Hg2-1dA" value="Client_BI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DxYNI1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DxbQcF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxbQcV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxbQcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DxbQc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxbQdF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxbQdV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxbQdl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxcekF-VEema_r8Hg2-1dA" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxcekV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxcekl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dxcek1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxcelF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxcelV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dxcell-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxdssF-VEema_r8Hg2-1dA" name="NbLigneConsomme" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxdssV-VEema_r8Hg2-1dA" value="NbLigneConsomme"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxdssl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dxdss1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxdstF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxdstV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dxdstl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxeTwF-VEema_r8Hg2-1dA" name="FrequenceMoyCmd" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxeTwV-VEema_r8Hg2-1dA" value="FrequenceMoyCmd"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxeTwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxeTw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxeTxF-VEema_r8Hg2-1dA" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxeTxV-VEema_r8Hg2-1dA" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dxfh4F-VEema_r8Hg2-1dA" name="ProfilSaisonnier" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dxfh4V-VEema_r8Hg2-1dA" value="ProfilSaisonnier"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxfh4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dxfh41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dxfh5F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dxfh5V-VEema_r8Hg2-1dA" value="150"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxgI8F-VEema_r8Hg2-1dA" name="PanierMoyen" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxgI8V-VEema_r8Hg2-1dA" value="PanierMoyen"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxgI8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxgI81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxgI9F-VEema_r8Hg2-1dA" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxgI9V-VEema_r8Hg2-1dA" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxhXEF-VEema_r8Hg2-1dA" name="NbLignesMoy" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxhXEV-VEema_r8Hg2-1dA" value="NbLignesMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxhXEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxhXE1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxhXFF-VEema_r8Hg2-1dA" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxhXFV-VEema_r8Hg2-1dA" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dxh-IF-VEema_r8Hg2-1dA" name="FreqVisiteWeb" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dxh-IV-VEema_r8Hg2-1dA" value="FreqVisiteWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxh-Il-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dxh-I1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dxh-JF-VEema_r8Hg2-1dA" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dxh-JV-VEema_r8Hg2-1dA" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxjMQF-VEema_r8Hg2-1dA" name="FreqVisiteCcial" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxjMQV-VEema_r8Hg2-1dA" value="FreqVisiteCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxjMQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxjMQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxjMRF-VEema_r8Hg2-1dA" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxjMRV-VEema_r8Hg2-1dA" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxjzUF-VEema_r8Hg2-1dA" name="FreqAppel" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxjzUV-VEema_r8Hg2-1dA" value="FreqAppel"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxjzUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxjzU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxjzVF-VEema_r8Hg2-1dA" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxjzVV-VEema_r8Hg2-1dA" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxlBcF-VEema_r8Hg2-1dA" name="FreqEmailing" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxlBcV-VEema_r8Hg2-1dA" value="FreqEmailing"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxlBcl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxlBc1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxlBdF-VEema_r8Hg2-1dA" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxlBdV-VEema_r8Hg2-1dA" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxlogF-VEema_r8Hg2-1dA" name="SecteurVacant" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxlogV-VEema_r8Hg2-1dA" value="SecteurVacant"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxlogl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dxlog1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxlohF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxlohV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dxm2oF-VEema_r8Hg2-1dA" name="ClientPrete" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dxm2oV-VEema_r8Hg2-1dA" value="ClientPrete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxm2ol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dxm2o1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dxm2pF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dxm2pV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxndsF-VEema_r8Hg2-1dA" name="CcialConge" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxndsV-VEema_r8Hg2-1dA" value="CcialConge"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxndsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dxnds1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxndtF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxndtV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dxor0F-VEema_r8Hg2-1dA" name="CcialEnCharge" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dxor0V-VEema_r8Hg2-1dA" value="CcialEnCharge"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dxor0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dxor01-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dxor1F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dxor1V-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxpS4F-VEema_r8Hg2-1dA" name="CcialEnPret" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxpS4V-VEema_r8Hg2-1dA" value="CcialEnPret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxpS4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxpS41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxpS5F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxpS5V-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DxrvIF-VEema_r8Hg2-1dA" name="PK_Client_BI">
        <node defType="com.stambia.rdbms.colref" id="_DxrvIV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DxrvIl-VEema_r8Hg2-1dA" ref="#_DxbQcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0o8gF-VEema_r8Hg2-1dA" name="FK_Client_BI_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0o8gV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0o8gl-VEema_r8Hg2-1dA" ref="#_DxcekF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0o8g1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dv2jIV-VEema_r8Hg2-1dA" name="ref_Propriété">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dv2jIl-VEema_r8Hg2-1dA" value="ref_Propriété"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dv2jI1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dv5mcF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dv5mcV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dv5mcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dv5mc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dv5mdF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dv5mdV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dv5mdl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dv6NgF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dv6NgV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dv6Ngl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dv6Ng1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dv6NhF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dv6NhV-VEema_r8Hg2-1dA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dv8CsF-VEema_r8Hg2-1dA" name="PK_ref_Propriété">
        <node defType="com.stambia.rdbms.colref" id="_Dv8CsV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dv8Csl-VEema_r8Hg2-1dA" ref="#_Dv5mcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DwW5cF-VEema_r8Hg2-1dA" name="ref_NewsletterType">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DwW5cV-VEema_r8Hg2-1dA" value="ref_NewsletterType"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DwW5cl-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DwZVsF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwZVsV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwZVsl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwZVs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwZVtF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwZVtV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwZVtl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwZ8wF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwZ8wV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwZ8wl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwZ8w1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwZ8xF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwZ8xV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwbK4F-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwbK4V-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwbK4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwbK41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwbK5F-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwbK5V-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DwdAEF-VEema_r8Hg2-1dA" name="PK_ref_NewsletterType">
        <node defType="com.stambia.rdbms.colref" id="_DwdAEV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DwdAEl-VEema_r8Hg2-1dA" ref="#_DwZVsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DbakMF-VEema_r8Hg2-1dA" name="ref_TypeMenu">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DbakMV-VEema_r8Hg2-1dA" value="ref_TypeMenu"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DbakMl-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DdiEEF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DdiEEV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DdiEEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DdiEE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DdiEFF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DdiEFV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DdiEFl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DdlucF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DdlucV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ddlucl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ddluc1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DdludF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DdludV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DdnjoF-VEema_r8Hg2-1dA" name="IdSpecCulinaire" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DdnjoV-VEema_r8Hg2-1dA" value="IdSpecCulinaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ddnjol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ddnjo1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DdnjpF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DdnjpV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ddnjpl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DeV8YF-VEema_r8Hg2-1dA" name="PK_ref_TypeMenu">
        <node defType="com.stambia.rdbms.colref" id="_DeV8YV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DeV8Yl-VEema_r8Hg2-1dA" ref="#_DdiEEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dw320V-VEema_r8Hg2-1dA" name="Client_TypeEtabClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dw4d4F-VEema_r8Hg2-1dA" value="Client_TypeEtabClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dw4d4V-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dw66IF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dw66IV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dw66Il-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dw66I1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dw66JF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dw66JV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dw66Jl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dw7hMF-VEema_r8Hg2-1dA" name="IdClientType" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dw7hMV-VEema_r8Hg2-1dA" value="IdClientType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dw7hMl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dw7hM1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dw7hNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dw7hNV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dw7hNl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dw99cF-VEema_r8Hg2-1dA" name="PK_Client_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_Dw99cV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dw99cl-VEema_r8Hg2-1dA" ref="#_Dw66IF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_Dw99c1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dw99dF-VEema_r8Hg2-1dA" ref="#_Dw7hMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClientType?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0lSIF-VEema_r8Hg2-1dA" name="FK_Client_TypeEtabClient_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0lSIV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0lSIl-VEema_r8Hg2-1dA" ref="#_Dw66IF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0lSI1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0lSJF-VEema_r8Hg2-1dA" name="FK_Client_TypeEtabClient_ref_TypeEtabClient1">
        <node defType="com.stambia.rdbms.relation" id="_D0lSJV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0lSJl-VEema_r8Hg2-1dA" ref="#_Dw7hMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClientType?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0lSJ1-VEema_r8Hg2-1dA" ref="#_DsdQ4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DyLeYV-VEema_r8Hg2-1dA" name="ref_Genre">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DyLeYl-VEema_r8Hg2-1dA" value="ref_Genre"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DyLeY1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DyOhsF-VEema_r8Hg2-1dA" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyOhsV-VEema_r8Hg2-1dA" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyOhsl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DyOhs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyOhtF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyOhtV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyOhtl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DyPv0F-VEema_r8Hg2-1dA" name="Genre" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyPv0V-VEema_r8Hg2-1dA" value="Genre"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyPv0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyPv01-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyPv1F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyPv1V-VEema_r8Hg2-1dA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DyQ98F-VEema_r8Hg2-1dA" name="Civilite" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyQ98V-VEema_r8Hg2-1dA" value="Civilite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyQ98l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyQ981-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyQ99F-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyQ99V-VEema_r8Hg2-1dA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DyRlAF-VEema_r8Hg2-1dA" name="Abreviation" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DyRlAV-VEema_r8Hg2-1dA" value="Abreviation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DyRlAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DyRlA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DyRlBF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DyRlBV-VEema_r8Hg2-1dA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DyUBQF-VEema_r8Hg2-1dA" name="PK_ref_Genre">
        <node defType="com.stambia.rdbms.colref" id="_DyUBQV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DyUBQl-VEema_r8Hg2-1dA" ref="#_DyOhsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dv-e8V-VEema_r8Hg2-1dA" name="Client_Distinction">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dv-e8l-VEema_r8Hg2-1dA" value="Client_Distinction"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dv-e81-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DwA7MF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwA7MV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwA7Ml-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwA7M1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwA7NF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwA7NV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwA7Nl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwBiQF-VEema_r8Hg2-1dA" name="IdDistinction" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwBiQV-VEema_r8Hg2-1dA" value="IdDistinction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwBiQl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwBiQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwBiRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwBiRV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwBiRl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DwD-gF-VEema_r8Hg2-1dA" name="PK_Client_Distinction">
        <node defType="com.stambia.rdbms.colref" id="_DwD-gV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DwD-gl-VEema_r8Hg2-1dA" ref="#_DwA7MF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_DwD-g1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DwD-hF-VEema_r8Hg2-1dA" ref="#_DwBiQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdDistinction?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0gZoF-VEema_r8Hg2-1dA" name="FK_Client_Distinction_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0gZoV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0gZol-VEema_r8Hg2-1dA" ref="#_DwA7MF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0gZo1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0gZpF-VEema_r8Hg2-1dA" name="FK_Client_Distinction_ref_Distinctions1">
        <node defType="com.stambia.rdbms.relation" id="_D0gZpV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0gZpl-VEema_r8Hg2-1dA" ref="#_DwBiQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdDistinction?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0gZp1-VEema_r8Hg2-1dA" ref="#_DzEPMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DuFokV-VEema_r8Hg2-1dA" name="ref_SpecialiteCulinaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DuFokl-VEema_r8Hg2-1dA" value="ref_SpecialiteCulinaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DuFok1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DuIr4F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuIr4V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuIr4l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DuIr41-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuIr5F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuIr5V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuIr5l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DuJ6AF-VEema_r8Hg2-1dA" name="LibSpecialite" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DuJ6AV-VEema_r8Hg2-1dA" value="LibSpecialite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DuJ6Al-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DuJ6A1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DuJ6BF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DuJ6BV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DuMWQF-VEema_r8Hg2-1dA" name="PK_ref_SpecialiteCulinaire">
        <node defType="com.stambia.rdbms.colref" id="_DuMWQV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DuMWQl-VEema_r8Hg2-1dA" ref="#_DuIr4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dwe1QV-VEema_r8Hg2-1dA" name="Client_UserWeb">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dwe1Ql-VEema_r8Hg2-1dA" value="Client_UserWeb"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dwe1Q1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DwifoF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwifoV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dwifol-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dwifo1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwifpF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwifpV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dwifpl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwkU0F-VEema_r8Hg2-1dA" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwkU0V-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwkU0l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwkU01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwkU1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwkU1V-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwkU1l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dwli8F-VEema_r8Hg2-1dA" name="IdGenre" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dwli8V-VEema_r8Hg2-1dA" value="IdGenre"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dwli8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dwli81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dwli9F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dwli9V-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dwli9l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwmxEF-VEema_r8Hg2-1dA" name="Nom" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwmxEV-VEema_r8Hg2-1dA" value="Nom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwmxEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwmxE1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwmxFF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwmxFV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dwn_MF-VEema_r8Hg2-1dA" name="Prenom" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dwn_MV-VEema_r8Hg2-1dA" value="Prenom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dwn_Ml-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dwn_M1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dwn_NF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dwn_NV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwomQF-VEema_r8Hg2-1dA" name="IntitulePoste" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwomQV-VEema_r8Hg2-1dA" value="IntitulePoste"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwomQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwomQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwomRF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwomRV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwpNUF-VEema_r8Hg2-1dA" name="Mail" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwpNUV-VEema_r8Hg2-1dA" value="Mail"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwpNUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwpNU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwpNVF-VEema_r8Hg2-1dA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwpNVV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwqbcF-VEema_r8Hg2-1dA" name="IdWeb" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwqbcV-VEema_r8Hg2-1dA" value="IdWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dwqbcl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dwqbc1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwqbdF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwqbdV-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwrCgF-VEema_r8Hg2-1dA" name="IdTypeComSouhait" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwrCgV-VEema_r8Hg2-1dA" value="IdTypeComSouhait"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwrCgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwrCg1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwrChF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwrChV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwrChl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwsQoF-VEema_r8Hg2-1dA" name="OptinEmail" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwsQoV-VEema_r8Hg2-1dA" value="OptinEmail"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwsQol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwsQo1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwsQpF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwsQpV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dws3sF-VEema_r8Hg2-1dA" name="DateCrt" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dws3sV-VEema_r8Hg2-1dA" value="DateCrt"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dws3sl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dws3s1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dws3tF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dws3tV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dws3tl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwuF0F-VEema_r8Hg2-1dA" name="DateSup" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwuF0V-VEema_r8Hg2-1dA" value="DateSup"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwuF0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwuF01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwuF1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwuF1V-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwuF1l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwvT8F-VEema_r8Hg2-1dA" name="TopSup" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwvT8V-VEema_r8Hg2-1dA" value="TopSup"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwvT8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwvT81-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwvT9F-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwvT9V-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwwiEF-VEema_r8Hg2-1dA" name="IdNiveau" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwwiEV-VEema_r8Hg2-1dA" value="IdNiveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwwiEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DwwiE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwwiFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwwiFV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwwiFl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwxJIF-VEema_r8Hg2-1dA" name="IdUserMagento" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwxJIV-VEema_r8Hg2-1dA" value="IdUserMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwxJIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwxJI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwxJJF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwxJJV-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DwyXQF-VEema_r8Hg2-1dA" name="OptinSMS" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_DwyXQV-VEema_r8Hg2-1dA" value="OptinSMS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DwyXQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DwyXQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DwyXRF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DwyXRV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dw1akF-VEema_r8Hg2-1dA" name="PK_Contact">
        <node defType="com.stambia.rdbms.colref" id="_Dw1akV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dw1akl-VEema_r8Hg2-1dA" ref="#_DwifoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0krEF-VEema_r8Hg2-1dA" name="FK_Client_UserWeb_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0krEV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0krEl-VEema_r8Hg2-1dA" ref="#_DwkU0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0krE1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0krFF-VEema_r8Hg2-1dA" name="FK_Client_UserWeb_ref_Genre">
        <node defType="com.stambia.rdbms.relation" id="_D0krFV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0krFl-VEema_r8Hg2-1dA" ref="#_Dwli8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdGenre?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0krF1-VEema_r8Hg2-1dA" ref="#_DyOhsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0krGF-VEema_r8Hg2-1dA" name="FK_Client_UserWeb_ref_NiveauUser">
        <node defType="com.stambia.rdbms.relation" id="_D0krGV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0krGl-VEema_r8Hg2-1dA" ref="#_DwwiEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdNiveau?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0krG1-VEema_r8Hg2-1dA" ref="#_DvDR4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0krHF-VEema_r8Hg2-1dA" name="FK_Client_UserWeb_ref_TypeComSouhaite">
        <node defType="com.stambia.rdbms.relation" id="_D0krHV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0krHl-VEema_r8Hg2-1dA" ref="#_DwrCgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdTypeComSouhait?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0krH1-VEema_r8Hg2-1dA" ref="#_Dxyc0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DvtZMV-VEema_r8Hg2-1dA" name="ref_NbCouvertJour">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DvtZMl-VEema_r8Hg2-1dA" value="ref_NbCouvertJour"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DvtZM1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Dvv1cF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dvv1cV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dvv1cl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dvv1c1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dvv1dF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dvv1dV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dvv1dl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DvxDkF-VEema_r8Hg2-1dA" name="NbCouvertsJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvxDkV-VEema_r8Hg2-1dA" value="NbCouvertsJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DvxDkl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvxDk1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvxDlF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DvxDlV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DvxqoF-VEema_r8Hg2-1dA" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DvxqoV-VEema_r8Hg2-1dA" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dvxqol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dvxqo1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DvxqpF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DvxqpV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dvxqpl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dvy4wF-VEema_r8Hg2-1dA" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dvy4wV-VEema_r8Hg2-1dA" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dvy4wl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Dvy4w1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dvy4xF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Dvy4xV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Dvy4xl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dv0t8F-VEema_r8Hg2-1dA" name="PK_ref_NbCouvertJour">
        <node defType="com.stambia.rdbms.colref" id="_Dv0t8V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dv0t8l-VEema_r8Hg2-1dA" ref="#_Dvv1cF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DzBL4V-VEema_r8Hg2-1dA" name="ref_Distinctions">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DzBL4l-VEema_r8Hg2-1dA" value="ref_Distinctions"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DzBL41-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DzEPMF-VEema_r8Hg2-1dA" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DzEPMV-VEema_r8Hg2-1dA" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DzEPMl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DzEPM1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DzEPNF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DzEPNV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DzEPNl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DzFdUF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DzFdUV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DzFdUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DzFdU1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DzFdVF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DzFdVV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DzGEYF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DzGEYV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DzGEYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DzGEY1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DzGEZF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DzGEZV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DzIgoF-VEema_r8Hg2-1dA" name="PK_ref_Distinctions">
        <node defType="com.stambia.rdbms.colref" id="_DzIgoV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DzIgol-VEema_r8Hg2-1dA" ref="#_DzEPMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dw_yoV-VEema_r8Hg2-1dA" name="ref_TypeCompte">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dw_yol-VEema_r8Hg2-1dA" value="ref_TypeCompte"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dw_yo1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DxC18F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxC18V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxC18l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DxC181-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxC19F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxC19V-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxC19l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DxDdAF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DxDdAV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DxDdAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DxDdA1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DxDdBF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DxDdBV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DxF5QF-VEema_r8Hg2-1dA" name="PK_ref_TypeCompte">
        <node defType="com.stambia.rdbms.colref" id="_DxF5QV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DxF5Ql-VEema_r8Hg2-1dA" ref="#_DxC18F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DfrZIV-VEema_r8Hg2-1dA" name="ref_Departement">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DfsAMF-VEema_r8Hg2-1dA" value="ref_Departement"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DfsAMV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DfuccF-VEema_r8Hg2-1dA" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfuccV-VEema_r8Hg2-1dA" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfvDgF-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DfvDgV-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfvDgl-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfvDg1-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfvDhF-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfwRoF-VEema_r8Hg2-1dA" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfwRoV-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DfwRol-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DfwRo1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfwRpF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfwRpV-VEema_r8Hg2-1dA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DfxfwF-VEema_r8Hg2-1dA" name="Libelle" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DfxfwV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dfxfwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Dfxfw1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DfxfxF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DfxfxV-VEema_r8Hg2-1dA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Df0jEF-VEema_r8Hg2-1dA" name="PK_ref_Departement">
        <node defType="com.stambia.rdbms.colref" id="_Df0jEV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Df0jEl-VEema_r8Hg2-1dA" ref="#_DfuccF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Du3rsV-VEema_r8Hg2-1dA" name="ref_ActiviteSegment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Du4SwF-VEema_r8Hg2-1dA" value="ref_ActiviteSegment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Du4SwV-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Du6vAF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Du6vAV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Du6vAl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Du6vA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Du6vBF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Du6vBV-VEema_r8Hg2-1dA" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Du6vBl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Du79IF-VEema_r8Hg2-1dA" name="IdSegment" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Du79IV-VEema_r8Hg2-1dA" value="IdSegment"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Du79Il-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Du79I1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Du79JF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Du79JV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Du79Jl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Du8kMF-VEema_r8Hg2-1dA" name="Libelle" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Du8kMV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Du8kMl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Du8kM1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Du8kNF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Du8kNV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Du-ZYF-VEema_r8Hg2-1dA" name="PK_ref_ActiviteSegment">
        <node defType="com.stambia.rdbms.colref" id="_Du-ZYV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Du-ZYl-VEema_r8Hg2-1dA" ref="#_Du6vAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0aTAF-VEema_r8Hg2-1dA" name="FK_ref_ActiviteSegment_ref_Segment">
        <node defType="com.stambia.rdbms.relation" id="_D0aTAV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0aTAl-VEema_r8Hg2-1dA" ref="#_Du79IF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdSegment?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0aTA1-VEema_r8Hg2-1dA" ref="#_DwQLwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DelM8V-VEema_r8Hg2-1dA" name="Client_ActiviteParCanal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DelM8l-VEema_r8Hg2-1dA" value="Client_ActiviteParCanal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DelM81-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Deo3UF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Deo3UV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Deo3Ul-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Deo3U1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Deo3VF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Deo3VV-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Deo3Vl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DeqFcF-VEema_r8Hg2-1dA" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DeqFcV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DeqFcl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DeqFc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DeqFdF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DeqFdV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DeqFdl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DerTkF-VEema_r8Hg2-1dA" name="IdCanal" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DerTkV-VEema_r8Hg2-1dA" value="IdCanal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DerTkl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DerTk1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DerTlF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DerTlV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DerTll-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DeshsF-VEema_r8Hg2-1dA" name="Actif12dm" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DeshsV-VEema_r8Hg2-1dA" value="Actif12dm"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Deshsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Deshs1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DeshtF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DeshtV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Deu98F-VEema_r8Hg2-1dA" name="PK_ActiviteParCanal">
        <node defType="com.stambia.rdbms.colref" id="_Deu98V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Deu98l-VEema_r8Hg2-1dA" ref="#_Deo3UF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzV8AF-VEema_r8Hg2-1dA" name="FK_ActiviteParCanal_Client">
        <node defType="com.stambia.rdbms.relation" id="_DzV8AV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzV8Al-VEema_r8Hg2-1dA" ref="#_DeqFcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzV8A1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_DzV8BF-VEema_r8Hg2-1dA" name="FK_ActiviteParCanal_ref_Canal1">
        <node defType="com.stambia.rdbms.relation" id="_DzV8BV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_DzV8Bl-VEema_r8Hg2-1dA" ref="#_DerTkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdCanal?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_DzV8B1-VEema_r8Hg2-1dA" ref="#_DnYjoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DrBGcV-VEema_r8Hg2-1dA" name="Client_InfosCciales">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DrBGcl-VEema_r8Hg2-1dA" value="Client_InfosCciales"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DrBGc1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DrF-8F-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrF-8V-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrF-8l-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrF-81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrF-9F-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrF-9V-VEema_r8Hg2-1dA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrF-9l-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrHNEF-VEema_r8Hg2-1dA" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrHNEV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrHNEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrHNE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrHNFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrHNFV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrHNFl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrIbMF-VEema_r8Hg2-1dA" name="DateDebAffluence" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrIbMV-VEema_r8Hg2-1dA" value="DateDebAffluence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrIbMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrIbM1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrIbNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrIbNV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrIbNl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrJCQF-VEema_r8Hg2-1dA" name="DateFinAffluence" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrJCQV-VEema_r8Hg2-1dA" value="DateFinAffluence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrJCQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrJCQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrJCRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrJCRV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrJCRl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrJpUF-VEema_r8Hg2-1dA" name="StatutCOLL" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrJpUV-VEema_r8Hg2-1dA" value="StatutCOLL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrJpUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrJpU1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrJpVF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrJpVV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrJpVl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrK3cF-VEema_r8Hg2-1dA" name="NiveauStanding" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrK3cV-VEema_r8Hg2-1dA" value="NiveauStanding"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrK3cl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrK3c1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrK3dF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrK3dV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrK3dl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrMFkF-VEema_r8Hg2-1dA" name="OuvertTouteAnnee" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrMFkV-VEema_r8Hg2-1dA" value="OuvertTouteAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrMFkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrMFk1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrMFlF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrMFlV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrNTsF-VEema_r8Hg2-1dA" name="DigitalFriendly" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrNTsV-VEema_r8Hg2-1dA" value="DigitalFriendly"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrNTsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrNTs1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrNTtF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrNTtV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrN6wF-VEema_r8Hg2-1dA" name="NbLits" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrN6wV-VEema_r8Hg2-1dA" value="NbLits"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrN6wl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrN6w1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrN6xF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrN6xV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrN6xl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrOh0F-VEema_r8Hg2-1dA" name="NbRepasJour" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrOh0V-VEema_r8Hg2-1dA" value="NbRepasJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrOh0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrOh01-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrOh1F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrPI4F-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrPI4V-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrPv8F-VEema_r8Hg2-1dA" name="NbCouvJour" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrPv8V-VEema_r8Hg2-1dA" value="NbCouvJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrPv8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrPv81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrPv9F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrPv9V-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrPv9l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrQXAF-VEema_r8Hg2-1dA" name="NbChambres" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrQXAV-VEema_r8Hg2-1dA" value="NbChambres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrQXAl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrQXA1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrQXBF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrQXBV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrQXBl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrQ-EF-VEema_r8Hg2-1dA" name="NbEleves" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrQ-EV-VEema_r8Hg2-1dA" value="NbEleves"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrQ-El-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrQ-E1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrQ-FF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrQ-FV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrQ-Fl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrSMMF-VEema_r8Hg2-1dA" name="NbEmployes" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrSMMV-VEema_r8Hg2-1dA" value="NbEmployes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrSMMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrSMM1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrSMNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrSMNV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrSMNl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrTaUF-VEema_r8Hg2-1dA" name="TrancheCA" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrTaUV-VEema_r8Hg2-1dA" value="TrancheCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrTaUl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrTaU1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrTaVF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrTaVV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrTaVl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrUBYF-VEema_r8Hg2-1dA" name="TicketMoy" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrUBYV-VEema_r8Hg2-1dA" value="TicketMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrUBYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrUBY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrUBZF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrUBZV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrUBZl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrVPgF-VEema_r8Hg2-1dA" name="PrxMoyNuit" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrVPgV-VEema_r8Hg2-1dA" value="PrxMoyNuit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrVPgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrVPg1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrVPhF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrVPhV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrVPhl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrV2kF-VEema_r8Hg2-1dA" name="PotentielCArecur" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrV2kV-VEema_r8Hg2-1dA" value="PotentielCArecur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrV2kl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrV2k1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrV2lF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrV2lV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrV2ll-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrXEsF-VEema_r8Hg2-1dA" name="PotentielCAInvest" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrXEsV-VEema_r8Hg2-1dA" value="PotentielCAInvest"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrXEsl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrXEs1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrXEtF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrXEtV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrXEtl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrXrwF-VEema_r8Hg2-1dA" name="ScoringCA" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrXrwV-VEema_r8Hg2-1dA" value="ScoringCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrXrwl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrXrw1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrXrxF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrXrxV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrXrxl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrY54F-VEema_r8Hg2-1dA" name="FamilleFermes" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrY54V-VEema_r8Hg2-1dA" value="FamilleFermes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrY54l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrY541-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrY55F-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrY55V-VEema_r8Hg2-1dA" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrZg8F-VEema_r8Hg2-1dA" name="CondConcession" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrZg8V-VEema_r8Hg2-1dA" value="CondConcession"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrZg8l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrZg81-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrZg9F-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrZg9V-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrZg9l-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DravEF-VEema_r8Hg2-1dA" name="ScoringRelation" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_DravEV-VEema_r8Hg2-1dA" value="ScoringRelation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DravEl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DravE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DravFF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DravFV-VEema_r8Hg2-1dA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DravFl-VEema_r8Hg2-1dA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Drb9MF-VEema_r8Hg2-1dA" name="Fidelite" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_Drb9MV-VEema_r8Hg2-1dA" value="Fidelite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Drb9Ml-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Drb9M1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Drb9NF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Drb9NV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Drb9Nl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrckQF-VEema_r8Hg2-1dA" name="Comportement" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrckQV-VEema_r8Hg2-1dA" value="Comportement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrckQl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrckQ1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrckRF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrckRV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrckRl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrdyYF-VEema_r8Hg2-1dA" name="EcheanceProjet" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrdyYV-VEema_r8Hg2-1dA" value="EcheanceProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrdyYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrdyY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrdyZF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrdyZV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrdyZl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrfAgF-VEema_r8Hg2-1dA" name="DernierProjet" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrfAgV-VEema_r8Hg2-1dA" value="DernierProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrfAgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrfAg1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrfAhF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrfAhV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrfAhl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrfnkF-VEema_r8Hg2-1dA" name="CmdFinAnnee" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrfnkV-VEema_r8Hg2-1dA" value="CmdFinAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Drfnkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Drfnk1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrfnlF-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrfnlV-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Drg1sF-VEema_r8Hg2-1dA" name="PrchCmdFinAnnee" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_Drg1sV-VEema_r8Hg2-1dA" value="PrchCmdFinAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Drg1sl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Drg1s1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Drg1tF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Drg1tV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Drg1tl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DriD0F-VEema_r8Hg2-1dA" name="CompteChomette" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_DriD0V-VEema_r8Hg2-1dA" value="CompteChomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DriD0l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DriD01-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DriD1F-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DriD1V-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Driq4F-VEema_r8Hg2-1dA" name="InvestCetteAnnee" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_Driq4V-VEema_r8Hg2-1dA" value="InvestCetteAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Driq4l-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Driq41-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Driq5F-VEema_r8Hg2-1dA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Driq5V-VEema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrlHIF-VEema_r8Hg2-1dA" name="CcurentRegion" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrlHIV-VEema_r8Hg2-1dA" value="CcurentRegion"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrlHIl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrlHI1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrlHJF-VEema_r8Hg2-1dA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrlHJV-VEema_r8Hg2-1dA" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrluMF-VEema_r8Hg2-1dA" name="DteCreation" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrluMV-VEema_r8Hg2-1dA" value="DteCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrluMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrluM1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrluNF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrluNV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrluNl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Drm8UF-VEema_r8Hg2-1dA" name="DteMAJCcial" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_Drm8UV-VEema_r8Hg2-1dA" value="DteMAJCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Drm8Ul-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Drm8U1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Drm8VF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Drm8VV-VEema_r8Hg2-1dA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Drm8Vl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DroKcF-VEema_r8Hg2-1dA" name="AnneeExistence" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_DroKcV-VEema_r8Hg2-1dA" value="AnneeExistence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DroKcl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DroKc1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DroKdF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DroKdV-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DroKdl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DrpYkF-VEema_r8Hg2-1dA" name="Remise" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_DrpYkV-VEema_r8Hg2-1dA" value="Remise"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DrpYkl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DrpYk1-VEema_r8Hg2-1dA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DrpYlF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DrpYlV-VEema_r8Hg2-1dA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DrpYll-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Drp_oF-VEema_r8Hg2-1dA" name="ColTarifaire" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_Drp_oV-VEema_r8Hg2-1dA" value="ColTarifaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Drp_ol-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Drp_o1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Drp_pF-VEema_r8Hg2-1dA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Drp_pV-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DrtqAF-VEema_r8Hg2-1dA" name="PK_Client_InfosCciales">
        <node defType="com.stambia.rdbms.colref" id="_DrtqAV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DrtqAl-VEema_r8Hg2-1dA" ref="#_DrF-8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0EUwF-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0EUwV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0EUwl-VEema_r8Hg2-1dA" ref="#_DrHNEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0EUw1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0EUxF-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_AnneesExistence">
        <node defType="com.stambia.rdbms.relation" id="_D0EUxV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0EUxl-VEema_r8Hg2-1dA" ref="#_DroKcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=AnneeExistence?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0EUx1-VEema_r8Hg2-1dA" ref="#_DvXa8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0EUyF-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_ComportementMultiCanal">
        <node defType="com.stambia.rdbms.relation" id="_D0EUyV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0EUyl-VEema_r8Hg2-1dA" ref="#_DrckQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Comportement?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0EUy1-VEema_r8Hg2-1dA" ref="#_DxKKsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0EUzF-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_NbChambres">
        <node defType="com.stambia.rdbms.relation" id="_D0EUzV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0EUzl-VEema_r8Hg2-1dA" ref="#_DrQXAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbChambres?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0EUz1-VEema_r8Hg2-1dA" ref="#_DqFSUF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0EU0F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_NbCouvertJour">
        <node defType="com.stambia.rdbms.relation" id="_D0EU0V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0EU0l-VEema_r8Hg2-1dA" ref="#_DrPv8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbCouvJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0EU01-VEema_r8Hg2-1dA" ref="#_Dvv1cF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0E70F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_NbEleves">
        <node defType="com.stambia.rdbms.relation" id="_D0E70V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0E70l-VEema_r8Hg2-1dA" ref="#_DrQ-EF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbEleves?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0E701-VEema_r8Hg2-1dA" ref="#_Dy6eMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0E71F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_NbEmployes">
        <node defType="com.stambia.rdbms.relation" id="_D0E71V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0E71l-VEema_r8Hg2-1dA" ref="#_DrSMMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbEmployes?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0E711-VEema_r8Hg2-1dA" ref="#_DyZg0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0E72F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_NbLits">
        <node defType="com.stambia.rdbms.relation" id="_D0E72V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0E72l-VEema_r8Hg2-1dA" ref="#_DrN6wF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbLits?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0E721-VEema_r8Hg2-1dA" ref="#_DmCf0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0E73F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_NbRepasJour">
        <node defType="com.stambia.rdbms.relation" id="_D0E73V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0E73l-VEema_r8Hg2-1dA" ref="#_DrOh0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbRepasJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0E731-VEema_r8Hg2-1dA" ref="#_DoA1wF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0E74F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_NiveauFidelite">
        <node defType="com.stambia.rdbms.relation" id="_D0E74V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0E74l-VEema_r8Hg2-1dA" ref="#_Drb9MF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Fidelite?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0E741-VEema_r8Hg2-1dA" ref="#_DfkEYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0E75F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_NiveauStanding">
        <node defType="com.stambia.rdbms.relation" id="_D0E75V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0E75l-VEema_r8Hg2-1dA" ref="#_DrK3cF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NiveauStanding?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0E751-VEema_r8Hg2-1dA" ref="#_DpVEYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0Fi4F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_ScoringCA">
        <node defType="com.stambia.rdbms.relation" id="_D0Fi4V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0Fi4l-VEema_r8Hg2-1dA" ref="#_DrXrwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=ScoringCA?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0Fi41-VEema_r8Hg2-1dA" ref="#_Ds9AIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0Fi5F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_StatutCOLL">
        <node defType="com.stambia.rdbms.relation" id="_D0Fi5V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0Fi5l-VEema_r8Hg2-1dA" ref="#_DrJpUF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=StatutCOLL?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0Fi51-VEema_r8Hg2-1dA" ref="#_DgaY8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0Fi6F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_TicketRepasMoyen">
        <node defType="com.stambia.rdbms.relation" id="_D0Fi6V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0Fi6l-VEema_r8Hg2-1dA" ref="#_DrUBYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TicketMoy?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0Fi61-VEema_r8Hg2-1dA" ref="#_DnzaYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0Fi7F-VEema_r8Hg2-1dA" name="FK_Client_InfosCciales_ref_TrancheCA">
        <node defType="com.stambia.rdbms.relation" id="_D0Fi7V-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0Fi7l-VEema_r8Hg2-1dA" ref="#_DrTaUF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TrancheCA?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0Fi71-VEema_r8Hg2-1dA" ref="#_DyluEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zR3izX1ZEem-bPviwv5ZLw" name="RepasServis" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_zR3izn1ZEem-bPviwv5ZLw" value="RepasServis"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zR3iz31ZEem-bPviwv5ZLw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zR3i0H1ZEem-bPviwv5ZLw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zR3i0X1ZEem-bPviwv5ZLw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zR3i0n1ZEem-bPviwv5ZLw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zR3i031ZEem-bPviwv5ZLw" name="Prestations" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_zR3i1H1ZEem-bPviwv5ZLw" value="Prestations"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zR3i1X1ZEem-bPviwv5ZLw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zR3i1n1ZEem-bPviwv5ZLw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zR3i131ZEem-bPviwv5ZLw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zR3i2H1ZEem-bPviwv5ZLw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_DqdFwV-VEema_r8Hg2-1dA" name="Client_UniversConso">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_DqdFwl-VEema_r8Hg2-1dA" value="Client_UniversConso"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_DqdFw1-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DqgwIF-VEema_r8Hg2-1dA" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DqgwIV-VEema_r8Hg2-1dA" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DqgwIl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DqgwI1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqgwJF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqgwJV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqgwJl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Dqh-QF-VEema_r8Hg2-1dA" name="IdUniversProduit" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Dqh-QV-VEema_r8Hg2-1dA" value="IdUniversProduit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Dqh-Ql-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DqilUF-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DqilUV-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DqilUl-VEema_r8Hg2-1dA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DqilU1-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Dqm2wF-VEema_r8Hg2-1dA" name="PK_Client_UniversConso">
        <node defType="com.stambia.rdbms.colref" id="_Dqm2wV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dqm2wl-VEema_r8Hg2-1dA" ref="#_DqgwIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_Dqm2w1-VEema_r8Hg2-1dA" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dqm2xF-VEema_r8Hg2-1dA" ref="#_Dqh-QF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUniversProduit?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0AqYF-VEema_r8Hg2-1dA" name="FK_Client_UniversConso_Client">
        <node defType="com.stambia.rdbms.relation" id="_D0AqYV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0AqYl-VEema_r8Hg2-1dA" ref="#_DqgwIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0AqY1-VEema_r8Hg2-1dA" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_D0AqZF-VEema_r8Hg2-1dA" name="FK_Client_UniversConso_ref_UniversConso1">
        <node defType="com.stambia.rdbms.relation" id="_D0AqZV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_D0AqZl-VEema_r8Hg2-1dA" ref="#_Dqh-QF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUniversProduit?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_D0AqZ1-VEema_r8Hg2-1dA" ref="#_DfXQEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Dps30V-VEema_r8Hg2-1dA" name="ref_ActualiteProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Dps30l-VEema_r8Hg2-1dA" value="ref_ActualiteProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Dps301-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_DpvUEF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpvUEV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpvUEl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DpvUE1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpvUFF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpvUFV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpvUFl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DpwiMF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpwiMV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpwiMl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpwiM1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpwiNF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpwiNV-VEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DpxJQF-VEema_r8Hg2-1dA" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_DpxwUF-VEema_r8Hg2-1dA" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DpxwUV-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DpxwUl-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DpxwU1-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DpxwVF-VEema_r8Hg2-1dA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DpzlgF-VEema_r8Hg2-1dA" name="PK_ref_ActualiteProjet">
        <node defType="com.stambia.rdbms.colref" id="_DpzlgV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Dpzlgl-VEema_r8Hg2-1dA" ref="#_DpvUEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Ds580V-VEema_r8Hg2-1dA" name="ref_ScoringCA">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Ds580l-VEema_r8Hg2-1dA" value="ref_ScoringCA"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Ds5801-VEema_r8Hg2-1dA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Ds9AIF-VEema_r8Hg2-1dA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ds9AIV-VEema_r8Hg2-1dA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ds9AIl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ds9AI1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ds9AJF-VEema_r8Hg2-1dA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ds9AJV-VEema_r8Hg2-1dA" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ds9AJl-VEema_r8Hg2-1dA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ds-OQF-VEema_r8Hg2-1dA" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ds-OQV-VEema_r8Hg2-1dA" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ds-OQl-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ds-OQ1-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ds-ORF-VEema_r8Hg2-1dA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ds-ORV-VEema_r8Hg2-1dA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ds_cYF-VEema_r8Hg2-1dA" name="ValMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ds_cYV-VEema_r8Hg2-1dA" value="ValMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ds_cYl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ds_cY1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ds_cZF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ds_cZV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ds_cZl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_DtAqgF-VEema_r8Hg2-1dA" name="ValMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_DtAqgV-VEema_r8Hg2-1dA" value="ValMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_DtAqgl-VEema_r8Hg2-1dA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_DtAqg1-VEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_DtAqhF-VEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_DtAqhV-VEema_r8Hg2-1dA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_DtAqhl-VEema_r8Hg2-1dA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_DtCfsF-VEema_r8Hg2-1dA" name="PK_ref_ScoringCA">
        <node defType="com.stambia.rdbms.colref" id="_DtCfsV-VEema_r8Hg2-1dA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_DtCfsl-VEema_r8Hg2-1dA" ref="#_Ds9AIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_rQ32sNVoEeq6RL_1vDZCgg" name="ECFDBTest.dbo">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_rSoxQNVoEeq6RL_1vDZCgg" value="ECFDBTest"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_rSoxQdVoEeq6RL_1vDZCgg" value="dbo"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_rSoxQtVoEeq6RL_1vDZCgg" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_rSoxQ9VoEeq6RL_1vDZCgg" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_rSoxRNVoEeq6RL_1vDZCgg" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_vK3nEdVoEeq6RL_1vDZCgg" name="ref_ActiviteSegment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vK3nEtVoEeq6RL_1vDZCgg" value="ref_ActiviteSegment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vK3nE9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vK6qYNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vK6qYdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vK6qYtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vK7RcNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vK7RcdVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vK7RctVoEeq6RL_1vDZCgg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vK7Rc9VoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vK74gNVoEeq6RL_1vDZCgg" name="IdSegment" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vK74gdVoEeq6RL_1vDZCgg" value="IdSegment"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vK74gtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vK74g9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vK74hNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vK74hdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vK74htVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vK9GoNVoEeq6RL_1vDZCgg" name="LibActivite" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vK9GodVoEeq6RL_1vDZCgg" value="LibActivite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vK9GotVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vK9Go9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vK9GpNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vK9GpdVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vK-70NVoEeq6RL_1vDZCgg" name="PK_ref_ActiviteSegment">
        <node defType="com.stambia.rdbms.colref" id="_vK-70dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vK-70tVoEeq6RL_1vDZCgg" ref="#_vK6qYNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQtJUNVoEeq6RL_1vDZCgg" name="FK_ref_ActiviteSegment_ref_Segment">
        <node defType="com.stambia.rdbms.relation" id="_vQtJUdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQtJUtVoEeq6RL_1vDZCgg" ref="#_vK74gNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdSegment?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQtJU9VoEeq6RL_1vDZCgg" ref="#_DwQLwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vLxmAdVoEeq6RL_1vDZCgg" name="ref_Propriété">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vLxmAtVoEeq6RL_1vDZCgg" value="ref_Propriété"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vLxmA9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vL0CQNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vL0CQdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vL0CQtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vL0CQ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vL0CRNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vL0CRdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vL0CRtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vL1QYNVoEeq6RL_1vDZCgg" name="LibPropriete" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vL1QYdVoEeq6RL_1vDZCgg" value="LibPropriete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vL1QYtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vL1QY9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vL1QZNVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vL1QZdVoEeq6RL_1vDZCgg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vL3FkNVoEeq6RL_1vDZCgg" name="PK_ref_Propriété">
        <node defType="com.stambia.rdbms.colref" id="_vL3FkdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vL3FktVoEeq6RL_1vDZCgg" ref="#_vL0CQNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u99zitVoEeq6RL_1vDZCgg" name="Wrk_ChometteCustomers">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u99zi9VoEeq6RL_1vDZCgg" value="Wrk_ChometteCustomers"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u99zjNVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u99zjdVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u99zjtVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u99zj9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u99zkNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u99zkdVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u99zktVoEeq6RL_1vDZCgg" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u99zk9VoEeq6RL_1vDZCgg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u99zlNVoEeq6RL_1vDZCgg" name="CodeMagento" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u99zldVoEeq6RL_1vDZCgg" value="CodeMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u99zltVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u99zl9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u99zmNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u99zmdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u99zmtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u99zm9VoEeq6RL_1vDZCgg" name="CodeClient" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u99znNVoEeq6RL_1vDZCgg" value="CodeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u99zndVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u99zntVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u99zn9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u99zoNVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u99zodVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-G9cNVoEeq6RL_1vDZCgg" name="FileName" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-G9cdVoEeq6RL_1vDZCgg" value="FileName"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-G9ctVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-G9c9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-G9dNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-G9ddVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-G9dtVoEeq6RL_1vDZCgg" name="TypeFile" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-G9d9VoEeq6RL_1vDZCgg" value="TypeFile"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-G9eNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-G9edVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-G9etVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-G9e9VoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-G9fNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-G9fdVoEeq6RL_1vDZCgg" name="dateCreation" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-G9ftVoEeq6RL_1vDZCgg" value="dateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-G9f9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-G9gNVoEeq6RL_1vDZCgg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-G9gdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-G9gtVoEeq6RL_1vDZCgg" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-G9g9VoEeq6RL_1vDZCgg" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-G9hNVoEeq6RL_1vDZCgg" name="Statut" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-G9hdVoEeq6RL_1vDZCgg" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-G9htVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-G9h9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-G9iNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-G9idVoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-LO4NVoEeq6RL_1vDZCgg" name="NewCode" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-LO4dVoEeq6RL_1vDZCgg" value="NewCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-LO4tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-LO49VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-LO5NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-LO5dVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-LO5tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-LO59VoEeq6RL_1vDZCgg" name="Mode" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-LO6NVoEeq6RL_1vDZCgg" value="Mode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-LO6dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-LO6tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-LO69VoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-LO7NVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u-LO7dVoEeq6RL_1vDZCgg" name="PK_Wrk_ChometteCustomers">
        <node defType="com.stambia.rdbms.colref" id="_u-LO7tVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u-LO79VoEeq6RL_1vDZCgg" ref="#_u99zjdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vE3swdVoEeq6RL_1vDZCgg" name="ref_TypeClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vE3swtVoEeq6RL_1vDZCgg" value="ref_TypeClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vE3sw9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vE3sxNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vE3sxdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vE3sxtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vE3sx9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vE3syNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vE3sydVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vE3sytVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vE3sy9VoEeq6RL_1vDZCgg" name="LibTypeClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vE3szNVoEeq6RL_1vDZCgg" value="LibTypeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vE3szdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vE3sztVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vE3sz9VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vE3s0NVoEeq6RL_1vDZCgg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vE3s0dVoEeq6RL_1vDZCgg" name="PK_ref_TypeClient">
        <node defType="com.stambia.rdbms.colref" id="_vE3s0tVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vE3s09VoEeq6RL_1vDZCgg" ref="#_vE3sxNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vOj0QdVoEeq6RL_1vDZCgg" name="ref_TrancheCA">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vOkbUNVoEeq6RL_1vDZCgg" value="ref_TrancheCA"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vOkbUdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vOm3kNVoEeq6RL_1vDZCgg" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOm3kdVoEeq6RL_1vDZCgg" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOm3ktVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOm3k9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOm3lNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOm3ldVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOm3ltVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOoFsNVoEeq6RL_1vDZCgg" name="TrancheCA" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOoFsdVoEeq6RL_1vDZCgg" value="TrancheCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOoFstVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOoFs9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOoFtNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOoFtdVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOoswNVoEeq6RL_1vDZCgg" name="TrancheCAMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOoswdVoEeq6RL_1vDZCgg" value="TrancheCAMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOoswtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOosw9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOosxNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOosxdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOosxtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOp64NVoEeq6RL_1vDZCgg" name="TrancheCAMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOp64dVoEeq6RL_1vDZCgg" value="TrancheCAMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOp64tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOp649VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOp65NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOp65dVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOp65tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vOrwENVoEeq6RL_1vDZCgg" name="PK_ref_TrancheCA">
        <node defType="com.stambia.rdbms.colref" id="_vOrwEdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vOrwEtVoEeq6RL_1vDZCgg" ref="#_vOm3kNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vPQX0dVoEeq6RL_1vDZCgg" name="ref_Distinctions">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vPQX0tVoEeq6RL_1vDZCgg" value="ref_Distinctions"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vPQX09VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vPTbINVoEeq6RL_1vDZCgg" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vPTbIdVoEeq6RL_1vDZCgg" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vPTbItVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vPTbI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vPTbJNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vPTbJdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vPTbJtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vPUpQNVoEeq6RL_1vDZCgg" name="LibDistinction" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vPUpQdVoEeq6RL_1vDZCgg" value="LibDistinction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vPUpQtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vPUpQ9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vPUpRNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vPUpRdVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vPVQUNVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vPVQUdVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vPVQUtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vPVQU9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vPVQVNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vPVQVdVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vPXskNVoEeq6RL_1vDZCgg" name="PK_ref_Distinctions">
        <node defType="com.stambia.rdbms.colref" id="_vPXskdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vPXsktVoEeq6RL_1vDZCgg" ref="#_vPTbINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u-LO8dVoEeq6RL_1vDZCgg" name="CltAEnrichir">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u-LO8tVoEeq6RL_1vDZCgg" value="CltAEnrichir"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u-LO89VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u-LO9NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-LO9dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-LO9tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-LO99VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-LO-NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-LO-dVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-LO-tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-LO-9VoEeq6RL_1vDZCgg" name="ClientCode" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-LO_NVoEeq6RL_1vDZCgg" value="ClientCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-LO_dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-LO_tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-LO_9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-LPANVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-LPAdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-LPAtVoEeq6RL_1vDZCgg" name="Siret" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-LPA9VoEeq6RL_1vDZCgg" value="Siret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-LPBNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-LPBdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-LPBtVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-LPB9VoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-UY0NVoEeq6RL_1vDZCgg" name="Traite" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-UY0dVoEeq6RL_1vDZCgg" value="Traite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-UY0tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-UY09VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-UY1NVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-UY1dVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-UY1tVoEeq6RL_1vDZCgg" name="CreateDate" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-UY19VoEeq6RL_1vDZCgg" value="CreateDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-UY2NVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-UY2dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-UY2tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-UY29VoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-UY3NVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u-UY3dVoEeq6RL_1vDZCgg" name="PK_CltAEnrichir">
        <node defType="com.stambia.rdbms.colref" id="_u-UY3tVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u-UY39VoEeq6RL_1vDZCgg" ref="#_u-LO9NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u7nDFtVoEeq6RL_1vDZCgg" name="Client_ActiviteParCanal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u7nDF9VoEeq6RL_1vDZCgg" value="Client_ActiviteParCanal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u7nDGNVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u7nDGdVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u7nDGtVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u7nDG9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u7nDHNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u7nDHdVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u7nDHtVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u7nDH9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u7nDINVoEeq6RL_1vDZCgg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u7nDIdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u7nDItVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u7nDI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u7nDJNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u7nDJdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u7nDJtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u7wNANVoEeq6RL_1vDZCgg" name="IdCanal" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u7wNAdVoEeq6RL_1vDZCgg" value="IdCanal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u7wNAtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u7wNA9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u7wNBNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u7wNBdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u7wNBtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u7wNB9VoEeq6RL_1vDZCgg" name="Actif12dm" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_u7wNCNVoEeq6RL_1vDZCgg" value="Actif12dm"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u7wNCdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u7wNCtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u7wNC9VoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u7wNDNVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u75-ANVoEeq6RL_1vDZCgg" name="PK_ActiviteParCanal">
        <node defType="com.stambia.rdbms.colref" id="_u75-AdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u75-AtVoEeq6RL_1vDZCgg" ref="#_u7nDGdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPbW8NVoEeq6RL_1vDZCgg" name="FK_ActiviteParCanal_Client">
        <node defType="com.stambia.rdbms.relation" id="_vPbW8dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPbW8tVoEeq6RL_1vDZCgg" ref="#_u7nDINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPbW89VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPbW9NVoEeq6RL_1vDZCgg" name="FK_ActiviteParCanal_ref_Canal1">
        <node defType="com.stambia.rdbms.relation" id="_vPbW9dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPbW9tVoEeq6RL_1vDZCgg" ref="#_u7wNANVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdCanal?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPbW99VoEeq6RL_1vDZCgg" ref="#_DnYjoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vIoycdVoEeq6RL_1vDZCgg" name="ref_ScoringCA">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vIoyctVoEeq6RL_1vDZCgg" value="ref_ScoringCA"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vIoyc9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vIsc0NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIsc0dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIsc0tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIsc09VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIsc1NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIsc1dVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIsc1tVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIsc19VoEeq6RL_1vDZCgg" name="LibScoring" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIsc2NVoEeq6RL_1vDZCgg" value="LibScoring"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIsc2dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIsc2tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIsc29VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIsc3NVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIsc3dVoEeq6RL_1vDZCgg" name="ValMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIsc3tVoEeq6RL_1vDZCgg" value="ValMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIsc39VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIsc4NVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIsc4dVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIsc4tVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIsc49VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIsc5NVoEeq6RL_1vDZCgg" name="ValMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIsc5dVoEeq6RL_1vDZCgg" value="ValMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIsc5tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIsc59VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIsc6NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIsc6dVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIsc6tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vIsc69VoEeq6RL_1vDZCgg" name="PK_ref_ScoringCA">
        <node defType="com.stambia.rdbms.colref" id="_vIsc7NVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vIsc7dVoEeq6RL_1vDZCgg" ref="#_vIsc0NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vByjp9VoEeq6RL_1vDZCgg" name="ref_NbLits">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vByjqNVoEeq6RL_1vDZCgg" value="ref_NbLits"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vByjqdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vB7tgNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vB7tgdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vB7tgtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vB7tg9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vB7thNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vB7thdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vB7thtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vB7th9VoEeq6RL_1vDZCgg" name="Nblits" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vB7tiNVoEeq6RL_1vDZCgg" value="Nblits"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vB7tidVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vB7titVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vB7ti9VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vB7tjNVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vB_-8NVoEeq6RL_1vDZCgg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vB_-8dVoEeq6RL_1vDZCgg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vB_-8tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vB_-89VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vB_-9NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vB_-9dVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vB_-9tVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vB_-99VoEeq6RL_1vDZCgg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vB_--NVoEeq6RL_1vDZCgg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vB_--dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vB_--tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vB_--9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vB_-_NVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vB_-_dVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vB_-_tVoEeq6RL_1vDZCgg" name="PK_ref_NbLits">
        <node defType="com.stambia.rdbms.colref" id="_vB_-_9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vB__ANVoEeq6RL_1vDZCgg" ref="#_vB7tgNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vF-gAdVoEeq6RL_1vDZCgg" name="sysdiagrams">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vF-gAtVoEeq6RL_1vDZCgg" value="sysdiagrams"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vF-gA9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vF-gBNVoEeq6RL_1vDZCgg" name="name" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vF-gBdVoEeq6RL_1vDZCgg" value="name"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vF-gBtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vF-gB9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vF-gCNVoEeq6RL_1vDZCgg" value="sysname"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vF-gCdVoEeq6RL_1vDZCgg" value="128"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vF-gCtVoEeq6RL_1vDZCgg" name="principal_id" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vF-gC9VoEeq6RL_1vDZCgg" value="principal_id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vF-gDNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vF-gDdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vF-gDtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vF-gD9VoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vF-gENVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vF-gEdVoEeq6RL_1vDZCgg" name="diagram_id" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vF-gEtVoEeq6RL_1vDZCgg" value="diagram_id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vF-gE9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vF-gFNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vF-gFdVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vF-gFtVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vF-gF9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vF-gGNVoEeq6RL_1vDZCgg" name="version" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vF-gGdVoEeq6RL_1vDZCgg" value="version"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vF-gGtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vF-gG9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vF-gHNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vF-gHdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vF-gHtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vF-gH9VoEeq6RL_1vDZCgg" name="definition" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vF-gINVoEeq6RL_1vDZCgg" value="definition"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vF-gIdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vF-gItVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vF-gI9VoEeq6RL_1vDZCgg" value="varbinary"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vF-gJNVoEeq6RL_1vDZCgg" value="max"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vF-gJdVoEeq6RL_1vDZCgg" name="PK__sysdiagr__C2B05B61D789EBAD">
        <node defType="com.stambia.rdbms.colref" id="_vF-gJtVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vF-gJ9VoEeq6RL_1vDZCgg" ref="#_vF-gEdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=diagram_id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vJJI0NVoEeq6RL_1vDZCgg" name="CltMaj_DI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vJJI0dVoEeq6RL_1vDZCgg" value="CltMaj_DI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vJJI0tVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vJJI09VoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJJI1NVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJJI1dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vJJI1tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJJI19VoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJJI2NVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJJI2dVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS5wNVoEeq6RL_1vDZCgg" name="CltCode" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS5wdVoEeq6RL_1vDZCgg" value="CltCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS5wtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vJS5w9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS5xNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS5xdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJS5xtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS5x9VoEeq6RL_1vDZCgg" name="Traite" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS5yNVoEeq6RL_1vDZCgg" value="Traite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS5ydVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS5ytVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS5y9VoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJS5zNVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS5zdVoEeq6RL_1vDZCgg" name="CreateDate" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS5ztVoEeq6RL_1vDZCgg" value="CreateDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS5z9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vJS50NVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS50dVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS50tVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJS509VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vJS51NVoEeq6RL_1vDZCgg" name="PK_CltMaj_DI">
        <node defType="com.stambia.rdbms.colref" id="_vJS51dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vJS51tVoEeq6RL_1vDZCgg" ref="#_vJJI09VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vLl_0dVoEeq6RL_1vDZCgg" name="ref_NbCouvertJour">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vLmm4NVoEeq6RL_1vDZCgg" value="ref_NbCouvertJour"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vLmm4dVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vLpDINVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLpDIdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLpDItVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLpDI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLpDJNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLpDJdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLpDJtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLq4UNVoEeq6RL_1vDZCgg" name="NbCouvertsJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLq4UdVoEeq6RL_1vDZCgg" value="NbCouvertsJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLq4UtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLq4U9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLq4VNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLq4VdVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLsGcNVoEeq6RL_1vDZCgg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLsGcdVoEeq6RL_1vDZCgg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLsGctVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLsGc9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLsGdNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLsGddVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLsGdtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLstgNVoEeq6RL_1vDZCgg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLstgdVoEeq6RL_1vDZCgg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLstgtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLstg9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLsthNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLsthdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLsthtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vLvJwNVoEeq6RL_1vDZCgg" name="PK_ref_NbCouvertJour">
        <node defType="com.stambia.rdbms.colref" id="_vLvJwdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vLvJwtVoEeq6RL_1vDZCgg" ref="#_vLpDINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vOHvYdVoEeq6RL_1vDZCgg" name="ref_TypeOperation">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vOHvYtVoEeq6RL_1vDZCgg" value="ref_TypeOperation"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vOHvY9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vOKysNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOKysdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOKystVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOKys9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOKytNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOKytdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOKyttVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOLZwNVoEeq6RL_1vDZCgg" name="LibTypeOperation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOLZwdVoEeq6RL_1vDZCgg" value="LibTypeOperation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOLZwtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOLZw9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOLZxNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOLZxdVoEeq6RL_1vDZCgg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vONO8NVoEeq6RL_1vDZCgg" name="PK_ref_TypeOperation">
        <node defType="com.stambia.rdbms.colref" id="_vONO8dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vONO8tVoEeq6RL_1vDZCgg" ref="#_vOKysNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vKnvcdVoEeq6RL_1vDZCgg" name="ref_TypeEtabNiv1">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vKnvctVoEeq6RL_1vDZCgg" value="ref_TypeEtabNiv1"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vKnvc9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vKqywNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKqywdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKqywtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vKqyw9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKqyxNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKqyxdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKqyxtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vKrZ0NVoEeq6RL_1vDZCgg" name="LibTypeEtabN1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKrZ0dVoEeq6RL_1vDZCgg" value="LibTypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKrZ0tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKrZ09VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKrZ1NVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKrZ1dVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vKt2ENVoEeq6RL_1vDZCgg" name="PK_TypeEtablissement">
        <node defType="com.stambia.rdbms.colref" id="_vKt2EdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vKt2EtVoEeq6RL_1vDZCgg" ref="#_vKqywNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vN0NYdVoEeq6RL_1vDZCgg" name="ref_TypeComSouhaite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vN0NYtVoEeq6RL_1vDZCgg" value="ref_TypeComSouhaite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vN0NY9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vN4e0NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vN4e0dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vN4e0tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vN4e09VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vN4e1NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vN4e1dVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vN4e1tVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vN5s8NVoEeq6RL_1vDZCgg" name="LibTypeCom" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vN5s8dVoEeq6RL_1vDZCgg" value="LibTypeCom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vN5s8tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vN5s89VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vN5s9NVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vN5s9dVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vN7iINVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vN7iIdVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vN7iItVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vN7iI9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vN7iJNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vN7iJdVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vN9-YNVoEeq6RL_1vDZCgg" name="PK_ref_TypeComSouhaite">
        <node defType="com.stambia.rdbms.colref" id="_vN9-YdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vN9-YtVoEeq6RL_1vDZCgg" ref="#_vN4e0NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vJpfEdVoEeq6RL_1vDZCgg" name="ref_SpecialiteCulinaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vJpfEtVoEeq6RL_1vDZCgg" value="ref_SpecialiteCulinaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vJpfE9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vJpfFNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJpfFdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJpfFtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vJpfF9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJpfGNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJpfGdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJpfGtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJpfG9VoEeq6RL_1vDZCgg" name="LibSpecialite" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJpfHNVoEeq6RL_1vDZCgg" value="LibSpecialite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJpfHdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJpfHtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJpfH9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJpfINVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vJpfIdVoEeq6RL_1vDZCgg" name="PK_ref_SpecialiteCulinaire">
        <node defType="com.stambia.rdbms.colref" id="_vJpfItVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vJpfI9VoEeq6RL_1vDZCgg" ref="#_vJpfFNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vNFNkdVoEeq6RL_1vDZCgg" name="ref_ComportementMultiCanal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vNFNktVoEeq6RL_1vDZCgg" value="ref_ComportementMultiCanal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vNFNk9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vNHp0NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNHp0dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNHp0tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vNHp09VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNHp1NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNHp1dVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNHp1tVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNI38NVoEeq6RL_1vDZCgg" name="LibCompMultiCanal" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNI38dVoEeq6RL_1vDZCgg" value="LibCompMultiCanal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNI38tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNI389VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNI39NVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNI39dVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vNL7QNVoEeq6RL_1vDZCgg" name="PK_ref_ComportementMultiCanal">
        <node defType="com.stambia.rdbms.colref" id="_vNL7QdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vNL7QtVoEeq6RL_1vDZCgg" ref="#_vNHp0NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vFwdlNVoEeq6RL_1vDZCgg" name="ref_NbChambres">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vFwdldVoEeq6RL_1vDZCgg" value="ref_NbChambres"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vFwdltVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vF0vANVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vF0vAdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vF0vAtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vF0vA9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vF0vBNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vF0vBdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vF0vBtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vF0vB9VoEeq6RL_1vDZCgg" name="NbChambres" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vF0vCNVoEeq6RL_1vDZCgg" value="NbChambres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vF0vCdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vF0vCtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vF0vC9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vF0vDNVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vF0vDdVoEeq6RL_1vDZCgg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vF0vDtVoEeq6RL_1vDZCgg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vF0vD9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vF0vENVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vF0vEdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vF0vEtVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vF0vE9VoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vF0vFNVoEeq6RL_1vDZCgg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vF0vFdVoEeq6RL_1vDZCgg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vF0vFtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vF0vF9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vF0vGNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vF0vGdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vF0vGtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vF0vG9VoEeq6RL_1vDZCgg" name="PK_ref_NbChambres">
        <node defType="com.stambia.rdbms.colref" id="_vF0vHNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vF0vHdVoEeq6RL_1vDZCgg" ref="#_vF0vANVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u8aUYNVoEeq6RL_1vDZCgg" name="ref_UniversConso">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u8aUYdVoEeq6RL_1vDZCgg" value="ref_UniversConso"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u8aUYtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u8aUY9VoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8aUZNVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8kFUNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u8kFUdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8kFUtVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8kFU9VoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8kFVNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8kFVdVoEeq6RL_1vDZCgg" name="LibUniversConso" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8kFVtVoEeq6RL_1vDZCgg" value="LibUniversConso"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8kFV9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8kFWNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8kFWdVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8kFWtVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u8kFW9VoEeq6RL_1vDZCgg" name="PK_ref_UniversConso">
        <node defType="com.stambia.rdbms.colref" id="_u8kFXNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u8kFXdVoEeq6RL_1vDZCgg" ref="#_u8aUY9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vKvrQdVoEeq6RL_1vDZCgg" name="Client_SpeCulinaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vKvrQtVoEeq6RL_1vDZCgg" value="Client_SpeCulinaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vKvrQ9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vKyHgNVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKyHgdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKyHgtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vKyHg9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKyHhNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKyHhdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKyHhtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vKzVoNVoEeq6RL_1vDZCgg" name="IdSpeCulinaire" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKzVodVoEeq6RL_1vDZCgg" value="IdSpeCulinaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKzVotVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vKzVo9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKzVpNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKzVpdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKzVptVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vK1K0NVoEeq6RL_1vDZCgg" name="PK_Client_SpeCulinaire">
        <node defType="com.stambia.rdbms.colref" id="_vK1K0dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vK1K0tVoEeq6RL_1vDZCgg" ref="#_vKyHgNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vK1x4NVoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vK1x4dVoEeq6RL_1vDZCgg" ref="#_vKzVoNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdSpeCulinaire?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQr7MNVoEeq6RL_1vDZCgg" name="FK_Client_SpeCulinaire_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQr7MdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQr7MtVoEeq6RL_1vDZCgg" ref="#_vKyHgNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQr7M9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQsiQNVoEeq6RL_1vDZCgg" name="FK_Client_SpeCulinaire_ref_SpecialiteCulinaire1">
        <node defType="com.stambia.rdbms.relation" id="_vQsiQdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQsiQtVoEeq6RL_1vDZCgg" ref="#_vKzVoNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdSpeCulinaire?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQsiQ9VoEeq6RL_1vDZCgg" ref="#_DuIr4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u-UY4dVoEeq6RL_1vDZCgg" name="Client">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u-UY4tVoEeq6RL_1vDZCgg" value="Client"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u-UY49VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u-UY5NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-UY5dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-UY5tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-UY59VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-UY6NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-UY6dVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-UY6tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-UY69VoEeq6RL_1vDZCgg" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-UY7NVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-UY7dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-UY7tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-UY79VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-UY8NVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-UY8dVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eJ0NVoEeq6RL_1vDZCgg" name="IdSociete" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eJ0dVoEeq6RL_1vDZCgg" value="IdSociete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eJ0tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-eJ09VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eJ1NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eJ1dVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eJ1tVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eJ19VoEeq6RL_1vDZCgg" name="CodeSocMagento" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eJ2NVoEeq6RL_1vDZCgg" value="CodeSocMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eJ2dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eJ2tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eJ29VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eJ3NVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eJ3dVoEeq6RL_1vDZCgg" name="RaisonSociale" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eJ3tVoEeq6RL_1vDZCgg" value="RaisonSociale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eJ39VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eJ4NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eJ4dVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eJ4tVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eJ49VoEeq6RL_1vDZCgg" name="Enseigne" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eJ5NVoEeq6RL_1vDZCgg" value="Enseigne"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eJ5dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eJ5tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eJ59VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eJ6NVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eJ6dVoEeq6RL_1vDZCgg" name="Siren" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eJ6tVoEeq6RL_1vDZCgg" value="Siren"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eJ69VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eJ7NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eJ7dVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eJ7tVoEeq6RL_1vDZCgg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eJ79VoEeq6RL_1vDZCgg" name="Siret" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eJ8NVoEeq6RL_1vDZCgg" value="Siret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eJ8dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eJ8tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eJ89VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eJ9NVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eJ9dVoEeq6RL_1vDZCgg" name="Nic" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eJ9tVoEeq6RL_1vDZCgg" value="Nic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eJ99VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eJ-NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eJ-dVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eJ-tVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eJ-9VoEeq6RL_1vDZCgg" name="Tva" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eJ_NVoEeq6RL_1vDZCgg" value="Tva"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eJ_dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eJ_tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eJ_9VoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eKANVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eKAdVoEeq6RL_1vDZCgg" name="Naf" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eKAtVoEeq6RL_1vDZCgg" value="Naf"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eKA9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eKBNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eKBdVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eKBtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-eKB9VoEeq6RL_1vDZCgg" name="TypeEtab1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-eKCNVoEeq6RL_1vDZCgg" value="TypeEtab1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-eKCdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-eKCtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-eKC9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-eKDNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-eKDdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n60NVoEeq6RL_1vDZCgg" name="TypeEtab2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n60dVoEeq6RL_1vDZCgg" value="TypeEtab2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n60tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-n609VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n61NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n61dVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n61tVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n619VoEeq6RL_1vDZCgg" name="TypeEtab3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n62NVoEeq6RL_1vDZCgg" value="TypeEtab3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n62dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-n62tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n629VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n63NVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n63dVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n63tVoEeq6RL_1vDZCgg" name="DateCreation" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n639VoEeq6RL_1vDZCgg" value="DateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n64NVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-n64dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n64tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n649VoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n65NVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n65dVoEeq6RL_1vDZCgg" name="SiteWeb" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n65tVoEeq6RL_1vDZCgg" value="SiteWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n659VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n66NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n66dVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n66tVoEeq6RL_1vDZCgg" value="350"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n669VoEeq6RL_1vDZCgg" name="Langue" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n67NVoEeq6RL_1vDZCgg" value="Langue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n67dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-n67tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n679VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n68NVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n68dVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n68tVoEeq6RL_1vDZCgg" name="TypeCompte" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n689VoEeq6RL_1vDZCgg" value="TypeCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n69NVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-n69dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n69tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n699VoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n6-NVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n6-dVoEeq6RL_1vDZCgg" name="Groupe" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n6-tVoEeq6RL_1vDZCgg" value="Groupe"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n6-9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n6_NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n6_dVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n6_tVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n6_9VoEeq6RL_1vDZCgg" name="International" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n7ANVoEeq6RL_1vDZCgg" value="International"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n7AdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n7AtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n7A9VoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n7BNVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n7BdVoEeq6RL_1vDZCgg" name="Directif" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n7BtVoEeq6RL_1vDZCgg" value="Directif"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n7B9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n7CNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n7CdVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n7CtVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-n7C9VoEeq6RL_1vDZCgg" name="DateCrtCompte" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-n7DNVoEeq6RL_1vDZCgg" value="DateCrtCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-n7DdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-n7DtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-n7D9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-n7ENVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-n7EdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-xEwNVoEeq6RL_1vDZCgg" name="Statut" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-xEwdVoEeq6RL_1vDZCgg" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-xEwtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-xEw9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-xExNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-xExdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-xExtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-xEx9VoEeq6RL_1vDZCgg" name="CanalAcquisition" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-xEyNVoEeq6RL_1vDZCgg" value="CanalAcquisition"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-xEydVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-xEytVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-xEy9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-xEzNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-xEzdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-xEztVoEeq6RL_1vDZCgg" name="DateMaj_CHD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-xEz9VoEeq6RL_1vDZCgg" value="DateMaj_CHD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-xE0NVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-xE0dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-xE0tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-xE09VoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-xE1NVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-61wNVoEeq6RL_1vDZCgg" name="DateMaj_Orion" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-61wdVoEeq6RL_1vDZCgg" value="DateMaj_Orion"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-61wtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-61w9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-61xNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-61xdVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-61xtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u-61x9VoEeq6RL_1vDZCgg" name="DateMaj_Ccial" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_u-61yNVoEeq6RL_1vDZCgg" value="DateMaj_Ccial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u-61ydVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u-61ytVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u-61y9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u-61zNVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u-61zdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_SCINVoEeq6RL_1vDZCgg" name="CompteChomette" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_SCIdVoEeq6RL_1vDZCgg" value="CompteChomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_SCItVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_SCI9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_SCJNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_SCJdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_bMENVoEeq6RL_1vDZCgg" name="DateSuppression" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_bMEdVoEeq6RL_1vDZCgg" value="DateSuppression"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_bMEtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u_bME9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_bMFNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_bMFdVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_bMFtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_bMF9VoEeq6RL_1vDZCgg" name="TopSuppression" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_bMGNVoEeq6RL_1vDZCgg" value="TopSuppression"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_bMGdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_bMGtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_bMG9VoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_bMHNVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_bMHdVoEeq6RL_1vDZCgg" name="CHDID" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_bMHtVoEeq6RL_1vDZCgg" value="CHDID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_bMH9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_bMINVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_bMIdVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_bMItVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_bMI9VoEeq6RL_1vDZCgg" name="Latitude" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_bMJNVoEeq6RL_1vDZCgg" value="Latitude"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_bMJdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_bMJtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_bMJ9VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_bMKNVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_bMKdVoEeq6RL_1vDZCgg" name="Longitude" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_bMKtVoEeq6RL_1vDZCgg" value="Longitude"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_bMK9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_bMLNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_bMLdVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_bMLtVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_bML9VoEeq6RL_1vDZCgg" name="Propriete" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_bMMNVoEeq6RL_1vDZCgg" value="Propriete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_bMMdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u_bMMtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_bMM9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_bMNNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_bMNdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_k9ENVoEeq6RL_1vDZCgg" name="Nature" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_k9EdVoEeq6RL_1vDZCgg" value="Nature"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_k9EtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_k9E9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_k9FNVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_k9FdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_k9FtVoEeq6RL_1vDZCgg" name="DateMaj_Web" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_k9F9VoEeq6RL_1vDZCgg" value="DateMaj_Web"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_k9GNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u_k9GdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_k9GtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_k9G9VoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_k9HNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_k9HdVoEeq6RL_1vDZCgg" name="MailPrincipal" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_k9HtVoEeq6RL_1vDZCgg" value="MailPrincipal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_k9H9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_k9INVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_k9IdVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_k9ItVoEeq6RL_1vDZCgg" value="150"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u_k9I9VoEeq6RL_1vDZCgg" name="PK_Client">
        <node defType="com.stambia.rdbms.colref" id="_u_k9JNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u_k9JdVoEeq6RL_1vDZCgg" ref="#_u-UY5NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPpZYNVoEeq6RL_1vDZCgg" name="FK_Client_ref_Canal">
        <node defType="com.stambia.rdbms.relation" id="_vPpZYdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPpZYtVoEeq6RL_1vDZCgg" ref="#_u-xEx9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=CanalAcquisition?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPqAcNVoEeq6RL_1vDZCgg" ref="#_DnYjoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPqAcdVoEeq6RL_1vDZCgg" name="FK_Client_ref_Langue">
        <node defType="com.stambia.rdbms.relation" id="_vPqActVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPqAc9VoEeq6RL_1vDZCgg" ref="#_u-n669VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Langue?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPqAdNVoEeq6RL_1vDZCgg" ref="#_DvnSkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPqAddVoEeq6RL_1vDZCgg" name="FK_Client_ref_Propriété">
        <node defType="com.stambia.rdbms.relation" id="_vPqAdtVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPqAd9VoEeq6RL_1vDZCgg" ref="#_u_bML9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Propriete?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPqAeNVoEeq6RL_1vDZCgg" ref="#_Dv5mcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPqAedVoEeq6RL_1vDZCgg" name="FK_Client_ref_Societe">
        <node defType="com.stambia.rdbms.relation" id="_vPqAetVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPqAe9VoEeq6RL_1vDZCgg" ref="#_u-eJ0NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdSociete?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPqAfNVoEeq6RL_1vDZCgg" ref="#_DgM9kF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPqAfdVoEeq6RL_1vDZCgg" name="FK_Client_ref_StatutClient">
        <node defType="com.stambia.rdbms.relation" id="_vPqAftVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPqAf9VoEeq6RL_1vDZCgg" ref="#_u-xEwNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Statut?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPqAgNVoEeq6RL_1vDZCgg" ref="#_DoMb8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPqAgdVoEeq6RL_1vDZCgg" name="FK_Client_ref_TypeCompte">
        <node defType="com.stambia.rdbms.relation" id="_vPqAgtVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPqAg9VoEeq6RL_1vDZCgg" ref="#_u-n68tVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeCompte?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPqAhNVoEeq6RL_1vDZCgg" ref="#_DxC18F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPqAhdVoEeq6RL_1vDZCgg" name="FK_Client_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_vPqAhtVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPqAh9VoEeq6RL_1vDZCgg" ref="#_u-eKB9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtab1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPqAiNVoEeq6RL_1vDZCgg" ref="#_Duq3YF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPqngNVoEeq6RL_1vDZCgg" name="FK_Client_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.relation" id="_vPqngdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPqngtVoEeq6RL_1vDZCgg" ref="#_u-n60NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtab2?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPqng9VoEeq6RL_1vDZCgg" ref="#_DuiUgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPqnhNVoEeq6RL_1vDZCgg" name="FK_Client_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.relation" id="_vPqnhdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPqnhtVoEeq6RL_1vDZCgg" ref="#_u-n619VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtab3?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPqnh9VoEeq6RL_1vDZCgg" ref="#_DuROwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vFnTpNVoEeq6RL_1vDZCgg" name="ref_NiveauConfiance">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vFnTpdVoEeq6RL_1vDZCgg" value="ref_NiveauConfiance"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vFnTptVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vFnTp9VoEeq6RL_1vDZCgg" name="IdNivConfiance" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFnTqNVoEeq6RL_1vDZCgg" value="IdNivConfiance"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFnTqdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vFnTqtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFnTq9VoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFnTrNVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFnTrdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vFnTrtVoEeq6RL_1vDZCgg" name="Niveau" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFnTr9VoEeq6RL_1vDZCgg" value="Niveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFnTsNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFnTsdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFnTstVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFnTs9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vFwdkNVoEeq6RL_1vDZCgg" name="PK_ref_NiveauConfiance">
        <node defType="com.stambia.rdbms.colref" id="_vFwdkdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vFwdktVoEeq6RL_1vDZCgg" ref="#_vFnTp9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdNivConfiance?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vMR8UdVoEeq6RL_1vDZCgg" name="ref_NewsletterType">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vMR8UtVoEeq6RL_1vDZCgg" value="ref_NewsletterType"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vMR8U9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vMU_oNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMU_odVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMU_otVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMU_o9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMU_pNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMU_pdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMU_ptVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMVmsNVoEeq6RL_1vDZCgg" name="LibType" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMVmsdVoEeq6RL_1vDZCgg" value="LibType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMVmstVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMVms9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMVmtNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMVmtdVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMW00NVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMW00dVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMW00tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMW009VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMW01NVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMW01dVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vMZRENVoEeq6RL_1vDZCgg" name="PK_ref_NewsletterType">
        <node defType="com.stambia.rdbms.colref" id="_vMZREdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vMZREtVoEeq6RL_1vDZCgg" ref="#_vMU_oNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vFKntNVoEeq6RL_1vDZCgg" name="ref_PrestationsOffertes">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vFKntdVoEeq6RL_1vDZCgg" value="ref_PrestationsOffertes"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vFKnttVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vFKnt9VoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFKnuNVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFKnudVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vFKnutVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFKnu9VoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFKnvNVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFKnvdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vFKnvtVoEeq6RL_1vDZCgg" name="LibPrestation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFKnv9VoEeq6RL_1vDZCgg" value="LibPrestation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFKnwNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFKnwdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFKnwtVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFKnw9VoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vFKnxNVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFKnxdVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFKnxtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFKnx9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFKnyNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFKnydVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vFUYsNVoEeq6RL_1vDZCgg" name="PK_ref_PrestationsOffertes">
        <node defType="com.stambia.rdbms.colref" id="_vFUYsdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vFUYstVoEeq6RL_1vDZCgg" ref="#_vFKnt9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vDsoEdVoEeq6RL_1vDZCgg" name="ref_NbRepasJour">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vDsoEtVoEeq6RL_1vDZCgg" value="ref_NbRepasJour"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vDsoE9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vDsoFNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDsoFdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDsoFtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vDsoF9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDsoGNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDsoGdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDsoGtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vDsoG9VoEeq6RL_1vDZCgg" name="NbRepasJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDsoHNVoEeq6RL_1vDZCgg" value="NbRepasJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDsoHdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDsoHtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDsoH9VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDsoINVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vDsoIdVoEeq6RL_1vDZCgg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDsoItVoEeq6RL_1vDZCgg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDsoI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vDsoJNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDsoJdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDsoJtVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDsoJ9VoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vDsoKNVoEeq6RL_1vDZCgg" name="nbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDsoKdVoEeq6RL_1vDZCgg" value="nbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDsoKtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vDsoK9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDsoLNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDsoLdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDsoLtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vD2ZENVoEeq6RL_1vDZCgg" name="PK_ref_NbRepasJour">
        <node defType="com.stambia.rdbms.colref" id="_vD2ZEdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vD2ZEtVoEeq6RL_1vDZCgg" ref="#_vDsoFNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u9AxV9VoEeq6RL_1vDZCgg" name="wrk_wsSociete_Out">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u9AxWNVoEeq6RL_1vDZCgg" value="wrk_wsSociete_Out"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u9AxWdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u9J7MNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9J7MdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9J7MtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9J7M9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9J7NNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9J7NdVoEeq6RL_1vDZCgg" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9J7NtVoEeq6RL_1vDZCgg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9J7N9VoEeq6RL_1vDZCgg" name="id_societe_chomette" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9J7ONVoEeq6RL_1vDZCgg" value="id_societe_chomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9J7OdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9J7OtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9J7O9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9J7PNVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9J7PdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9J7PtVoEeq6RL_1vDZCgg" name="id_societe_magento" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9J7P9VoEeq6RL_1vDZCgg" value="id_societe_magento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9J7QNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9J7QdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9J7QtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9J7Q9VoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9J7RNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9J7RdVoEeq6RL_1vDZCgg" name="statut" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9J7RtVoEeq6RL_1vDZCgg" value="statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9J7R9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9J7SNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9J7SdVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9J7StVoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9OMoNVoEeq6RL_1vDZCgg" name="message" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9OModVoEeq6RL_1vDZCgg" value="message"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9OMotVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9OMo9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9OMpNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9OMpdVoEeq6RL_1vDZCgg" value="500"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9OMptVoEeq6RL_1vDZCgg" name="datecreation" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9OMp9VoEeq6RL_1vDZCgg" value="datecreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9OMqNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9OMqdVoEeq6RL_1vDZCgg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9OMqtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9OMq9VoEeq6RL_1vDZCgg" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9OMrNVoEeq6RL_1vDZCgg" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9OMrdVoEeq6RL_1vDZCgg" name="response_code" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9OMrtVoEeq6RL_1vDZCgg" value="response_code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9OMr9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9OMsNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9OMsdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9OMstVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9OMs9VoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9OMtNVoEeq6RL_1vDZCgg" name="response_msg" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9OMtdVoEeq6RL_1vDZCgg" value="response_msg"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9OMttVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9OMt9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9OMuNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9OMudVoEeq6RL_1vDZCgg" value="500"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u9OMutVoEeq6RL_1vDZCgg" name="PK_wrk_wsSociete_Out">
        <node defType="com.stambia.rdbms.colref" id="_u9OMu9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u9OMvNVoEeq6RL_1vDZCgg" ref="#_u9J7MNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vOPrMdVoEeq6RL_1vDZCgg" name="ref_Genre">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vOPrMtVoEeq6RL_1vDZCgg" value="ref_Genre"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vOPrM9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vOSugNVoEeq6RL_1vDZCgg" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOSugdVoEeq6RL_1vDZCgg" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOSugtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOSug9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOSuhNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOSuhdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOSuhtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOT8oNVoEeq6RL_1vDZCgg" name="Genre" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOT8odVoEeq6RL_1vDZCgg" value="Genre"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOT8otVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOT8o9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOT8pNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOT8pdVoEeq6RL_1vDZCgg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOUjsNVoEeq6RL_1vDZCgg" name="Civilite" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOUjsdVoEeq6RL_1vDZCgg" value="Civilite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOUjstVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOUjs9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOUjtNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOUjtdVoEeq6RL_1vDZCgg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOVx0NVoEeq6RL_1vDZCgg" name="Abreviation" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOVx0dVoEeq6RL_1vDZCgg" value="Abreviation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOVx0tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOVx09VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOVx1NVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOVx1dVoEeq6RL_1vDZCgg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vOXnANVoEeq6RL_1vDZCgg" name="PK_ref_Genre">
        <node defType="com.stambia.rdbms.colref" id="_vOXnAdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vOXnAtVoEeq6RL_1vDZCgg" ref="#_vOSugNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vLIFwdVoEeq6RL_1vDZCgg" name="ref_TypeZone">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vLIFwtVoEeq6RL_1vDZCgg" value="ref_TypeZone"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vLIFw9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vLLJENVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLLJEdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLLJEtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLLJE9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLLJFNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLLJFdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLLJFtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLLwINVoEeq6RL_1vDZCgg" name="LibTypeZone" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLLwIdVoEeq6RL_1vDZCgg" value="LibTypeZone"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLLwItVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLLwI9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLLwJNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLLwJdVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLM-QNVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLM-QdVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLM-QtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLM-Q9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLM-RNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLM-RdVoEeq6RL_1vDZCgg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLM-RtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vLPagNVoEeq6RL_1vDZCgg" name="PK_TypeZone">
        <node defType="com.stambia.rdbms.colref" id="_vLPagdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vLPagtVoEeq6RL_1vDZCgg" ref="#_vLLJENVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vCl00dVoEeq6RL_1vDZCgg" name="ref_TypeAdresse">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vCl00tVoEeq6RL_1vDZCgg" value="ref_TypeAdresse"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vCl009VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vCl01NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCl01dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCl01tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCl019VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCl02NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCl02dVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCl02tVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCl029VoEeq6RL_1vDZCgg" name="Type" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCl03NVoEeq6RL_1vDZCgg" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCl03dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCl03tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCl039VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCl04NVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vCl04dVoEeq6RL_1vDZCgg" name="PK_ref_TypeAdresse">
        <node defType="com.stambia.rdbms.colref" id="_vCl04tVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vCl049VoEeq6RL_1vDZCgg" ref="#_vCl01NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vICVkNVoEeq6RL_1vDZCgg" name="ref_TypeEtabClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vICVkdVoEeq6RL_1vDZCgg" value="ref_TypeEtabClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vICVktVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vIMGgNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIMGgdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIMGgtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIMGg9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIMGhNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIMGhdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIMGhtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIMGh9VoEeq6RL_1vDZCgg" name="LibTypeEtabClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIMGiNVoEeq6RL_1vDZCgg" value="LibTypeEtabClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIMGidVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIMGitVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIMGi9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIMGjNVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIMGjdVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIMGjtVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIMGj9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIMGkNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIMGkdVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIMGktVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vIMGk9VoEeq6RL_1vDZCgg" name="PK_ref_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_vIMGlNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vIMGldVoEeq6RL_1vDZCgg" ref="#_vIMGgNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vCJI7dVoEeq6RL_1vDZCgg" name="Client_HoraireLiv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vCJI7tVoEeq6RL_1vDZCgg" value="Client_HoraireLiv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vCJI79VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vCJI8NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCJI8dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCJI8tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCJI89VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCJI9NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCJI9dVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCJI9tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCS54NVoEeq6RL_1vDZCgg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCS54dVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCS54tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCS549VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCS55NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCS55dVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCS55tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCS559VoEeq6RL_1vDZCgg" name="H_DebutLivLundi" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCS56NVoEeq6RL_1vDZCgg" value="H_DebutLivLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCS56dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCS56tVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCS569VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCS57NVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCS57dVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCS57tVoEeq6RL_1vDZCgg" name="H_DebutLivMardi" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCS579VoEeq6RL_1vDZCgg" value="H_DebutLivMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCS58NVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCS58dVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCS58tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCS589VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCS59NVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCS59dVoEeq6RL_1vDZCgg" name="H_DebutLivMercredi" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCS59tVoEeq6RL_1vDZCgg" value="H_DebutLivMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCS599VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCS5-NVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCS5-dVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCS5-tVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCS5-9VoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCS5_NVoEeq6RL_1vDZCgg" name="H_DebutLivJeudi" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCS5_dVoEeq6RL_1vDZCgg" value="H_DebutLivJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCS5_tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCS5_9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCS6ANVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCS6AdVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCS6AtVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCS6A9VoEeq6RL_1vDZCgg" name="H_DebutLivVendredi" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCS6BNVoEeq6RL_1vDZCgg" value="H_DebutLivVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCS6BdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCS6BtVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCS6B9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCS6CNVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCS6CdVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCS6CtVoEeq6RL_1vDZCgg" name="H_DebutLivSamedi" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCS6C9VoEeq6RL_1vDZCgg" value="H_DebutLivSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCS6DNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCS6DdVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCS6DtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCS6D9VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCS6ENVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCS6EdVoEeq6RL_1vDZCgg" name="H_DebutLivDimanche" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCS6EtVoEeq6RL_1vDZCgg" value="H_DebutLivDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCS6E9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCS6FNVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCS6FdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCS6FtVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCS6F9VoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCS6GNVoEeq6RL_1vDZCgg" name="H_FinLivLundi" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCS6GdVoEeq6RL_1vDZCgg" value="H_FinLivLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCS6GtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCS6G9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCS6HNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCS6HdVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCS6HtVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCcq4NVoEeq6RL_1vDZCgg" name="H_FinLivMardi" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCcq4dVoEeq6RL_1vDZCgg" value="H_FinLivMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCcq4tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCcq49VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCcq5NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCcq5dVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCcq5tVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCcq59VoEeq6RL_1vDZCgg" name="H_FinLivMercredi" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCcq6NVoEeq6RL_1vDZCgg" value="H_FinLivMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCcq6dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCcq6tVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCcq69VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCcq7NVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCcq7dVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCcq7tVoEeq6RL_1vDZCgg" name="H_FinLivJeudi" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCcq79VoEeq6RL_1vDZCgg" value="H_FinLivJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCcq8NVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCcq8dVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCcq8tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCcq89VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCcq9NVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCcq9dVoEeq6RL_1vDZCgg" name="H_FinLivVendredi" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCcq9tVoEeq6RL_1vDZCgg" value="H_FinLivVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCcq99VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCcq-NVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCcq-dVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCcq-tVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCcq-9VoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCcq_NVoEeq6RL_1vDZCgg" name="H_FinLivSamedi" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCcq_dVoEeq6RL_1vDZCgg" value="H_FinLivSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCcq_tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCcq_9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCcrANVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCcrAdVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCcrAtVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCcrA9VoEeq6RL_1vDZCgg" name="H_FinLivDimanche" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCcrBNVoEeq6RL_1vDZCgg" value="H_FinLivDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCcrBdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCcrBtVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCcrB9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCcrCNVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCcrCdVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vCcrCtVoEeq6RL_1vDZCgg" name="PK_Horaire_Livraison">
        <node defType="com.stambia.rdbms.colref" id="_vCcrC9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vCcrDNVoEeq6RL_1vDZCgg" ref="#_vCS54NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vP9icNVoEeq6RL_1vDZCgg" name="FK_Client_HoraireLiv_Client">
        <node defType="com.stambia.rdbms.relation" id="_vP9icdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vP9ictVoEeq6RL_1vDZCgg" ref="#_vCS54NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vP9ic9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vFA2sdVoEeq6RL_1vDZCgg" name="ref_NiveauStanding">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vFA2stVoEeq6RL_1vDZCgg" value="ref_NiveauStanding"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vFA2s9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vFA2tNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFA2tdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFA2ttVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vFA2t9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFA2uNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFA2udVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFA2utVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vFA2u9VoEeq6RL_1vDZCgg" name="LibNivStanding" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFA2vNVoEeq6RL_1vDZCgg" value="LibNivStanding"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFA2vdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFA2vtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFA2v9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFA2wNVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vFKnsNVoEeq6RL_1vDZCgg" name="PK_NiveauStanding">
        <node defType="com.stambia.rdbms.colref" id="_vFKnsdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vFKnstVoEeq6RL_1vDZCgg" ref="#_vFA2tNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vJcDxNVoEeq6RL_1vDZCgg" name="ref_ClientType">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vJcDxdVoEeq6RL_1vDZCgg" value="ref_ClientType"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vJcDxtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vJcDx9VoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJcDyNVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJcDydVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vJcDytVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJcDy9VoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJcDzNVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJcDzdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJl0sNVoEeq6RL_1vDZCgg" name="LibTypeClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJl0sdVoEeq6RL_1vDZCgg" value="LibTypeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJl0stVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJl0s9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJl0tNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJl0tdVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vJl0ttVoEeq6RL_1vDZCgg" name="PK_ref_ClientType">
        <node defType="com.stambia.rdbms.colref" id="_vJl0t9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vJl0uNVoEeq6RL_1vDZCgg" ref="#_vJcDx9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vMCEsdVoEeq6RL_1vDZCgg" name="Client_MoisOuvert">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vMCEstVoEeq6RL_1vDZCgg" value="Client_MoisOuvert"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vMCEs9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vMFIANVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMFIAdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMFIAtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMFIA9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMFIBNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMFIBdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMFIBtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMGWINVoEeq6RL_1vDZCgg" name="IdMois" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMGWIdVoEeq6RL_1vDZCgg" value="IdMois"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMGWItVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMGWI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMGWJNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMGWJdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMGWJtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vMILUNVoEeq6RL_1vDZCgg" name="PK_Client_MoisOuvert">
        <node defType="com.stambia.rdbms.colref" id="_vMILUdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vMILUtVoEeq6RL_1vDZCgg" ref="#_vMFIANVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vMIyYNVoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vMIyYdVoEeq6RL_1vDZCgg" ref="#_vMGWINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdMois?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQz3ANVoEeq6RL_1vDZCgg" name="FK_Client_MoisOuvert_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQz3AdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQz3AtVoEeq6RL_1vDZCgg" ref="#_vMFIANVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQz3A9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQz3BNVoEeq6RL_1vDZCgg" name="FK_Client_MoisOuvert_ref_Mois1">
        <node defType="com.stambia.rdbms.relation" id="_vQz3BdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQz3BtVoEeq6RL_1vDZCgg" ref="#_vMGWINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdMois?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQz3B9VoEeq6RL_1vDZCgg" ref="#_DgqQkF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vJS52NVoEeq6RL_1vDZCgg" name="Client_Concurrent">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vJS52dVoEeq6RL_1vDZCgg" value="Client_Concurrent"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vJS52tVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vJcDsNVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJcDsdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJcDstVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vJcDs9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJcDtNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJcDtdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJcDttVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJcDt9VoEeq6RL_1vDZCgg" name="IdConcurrent" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJcDuNVoEeq6RL_1vDZCgg" value="IdConcurrent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJcDudVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vJcDutVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJcDu9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJcDvNVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJcDvdVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vJcDvtVoEeq6RL_1vDZCgg" name="PK_Client_Concurrent">
        <node defType="com.stambia.rdbms.colref" id="_vJcDv9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vJcDwNVoEeq6RL_1vDZCgg" ref="#_vJcDsNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vJcDwdVoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vJcDwtVoEeq6RL_1vDZCgg" ref="#_vJcDt9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdConcurrent?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQl0kNVoEeq6RL_1vDZCgg" name="FK_Client_Concurrent_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQl0kdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQl0ktVoEeq6RL_1vDZCgg" ref="#_vJcDsNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQl0k9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQl0lNVoEeq6RL_1vDZCgg" name="FK_Client_Concurrent_ref_Concurrent1">
        <node defType="com.stambia.rdbms.relation" id="_vQl0ldVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQl0ltVoEeq6RL_1vDZCgg" ref="#_vJcDt9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdConcurrent?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQl0l9VoEeq6RL_1vDZCgg" ref="#_DxSGgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vLAxAdVoEeq6RL_1vDZCgg" name="ref_NiveauUser">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vLAxAtVoEeq6RL_1vDZCgg" value="ref_NiveauUser"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vLAxA9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vLDNQNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLDNQdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLDNQtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLDNQ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLDNRNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLDNRdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLDNRtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLEbYNVoEeq6RL_1vDZCgg" name="LibNiveau" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLEbYdVoEeq6RL_1vDZCgg" value="LibNiveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLEbYtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLEbY9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLEbZNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLEbZdVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vLGQkNVoEeq6RL_1vDZCgg" name="PK_ref_NiveauUser">
        <node defType="com.stambia.rdbms.colref" id="_vLGQkdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vLGQktVoEeq6RL_1vDZCgg" ref="#_vLDNQNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u83AV9VoEeq6RL_1vDZCgg" name="ref_ColonneTarifaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u83AWNVoEeq6RL_1vDZCgg" value="ref_ColonneTarifaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u83AWdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u9AxQNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9AxQdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9AxQtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9AxQ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9AxRNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9AxRdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9AxRtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9AxR9VoEeq6RL_1vDZCgg" name="LibColonneTarif" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9AxSNVoEeq6RL_1vDZCgg" value="LibColonneTarif"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9AxSdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9AxStVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9AxS9VoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9AxTNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9AxTdVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9AxTtVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9AxT9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9AxUNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9AxUdVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9AxUtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u9AxU9VoEeq6RL_1vDZCgg" name="PK_ref_ColonneTarifaire">
        <node defType="com.stambia.rdbms.colref" id="_u9AxVNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u9AxVdVoEeq6RL_1vDZCgg" ref="#_u9AxQNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vKM4sdVoEeq6RL_1vDZCgg" name="Client_PerimetreProchainProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vKM4stVoEeq6RL_1vDZCgg" value="Client_PerimetreProchainProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vKM4s9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vKPU8NVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKPU8dVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKPU8tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vKPU89VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKPU9NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKPU9dVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKPU9tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vKRKINVoEeq6RL_1vDZCgg" name="IdUnivers" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKRKIdVoEeq6RL_1vDZCgg" value="IdUnivers"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKRKItVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vKRKI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKRKJNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKRKJdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKRKJtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vKTmYNVoEeq6RL_1vDZCgg" name="PK_Client_PerimetreProchainProjet">
        <node defType="com.stambia.rdbms.colref" id="_vKTmYdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vKTmYtVoEeq6RL_1vDZCgg" ref="#_vKPU8NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vKTmY9VoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vKTmZNVoEeq6RL_1vDZCgg" ref="#_vKRKINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUnivers?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQpe8NVoEeq6RL_1vDZCgg" name="FK_Client_PerimetreProchainProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQpe8dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQpe8tVoEeq6RL_1vDZCgg" ref="#_vKPU8NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQpe89VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQpe9NVoEeq6RL_1vDZCgg" name="FK_Client_PerimetreProchainProjet_ref_UniversConso">
        <node defType="com.stambia.rdbms.relation" id="_vQpe9dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQpe9tVoEeq6RL_1vDZCgg" ref="#_vKRKINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUnivers?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQpe99VoEeq6RL_1vDZCgg" ref="#_DfXQEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vIsc79VoEeq6RL_1vDZCgg" name="ref_RepasServis">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vIsc8NVoEeq6RL_1vDZCgg" value="ref_RepasServis"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vIsc8dVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vI2N0NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vI2N0dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vI2N0tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vI2N09VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vI2N1NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vI2N1dVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vI2N1tVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vI2N19VoEeq6RL_1vDZCgg" name="LibRepas" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vI2N2NVoEeq6RL_1vDZCgg" value="LibRepas"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vI2N2dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vI2N2tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vI2N29VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vI2N3NVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vI2N3dVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vI2N3tVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vI2N39VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vI2N4NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vI2N4dVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vI2N4tVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vI2N49VoEeq6RL_1vDZCgg" name="PK_ref_RepasServi">
        <node defType="com.stambia.rdbms.colref" id="_vI2N5NVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vI2N5dVoEeq6RL_1vDZCgg" ref="#_vI2N0NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vFdipNVoEeq6RL_1vDZCgg" name="ref_ActualiteProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vFdipdVoEeq6RL_1vDZCgg" value="ref_ActualiteProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vFdiptVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vFdip9VoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFdiqNVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFdiqdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vFdiqtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFdiq9VoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFdirNVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFdirdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vFdirtVoEeq6RL_1vDZCgg" name="LibActualiteProjet" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFdir9VoEeq6RL_1vDZCgg" value="LibActualiteProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFdisNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFdisdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFdistVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFdis9VoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vFditNVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFditdVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFdittVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFdit9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFdiuNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFdiudVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vFnToNVoEeq6RL_1vDZCgg" name="PK_ref_ActualiteProjet">
        <node defType="com.stambia.rdbms.colref" id="_vFnTodVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vFnTotVoEeq6RL_1vDZCgg" ref="#_vFdip9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vHh_OtVoEeq6RL_1vDZCgg" name="Client_UserWeb_News">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vHh_O9VoEeq6RL_1vDZCgg" value="Client_UserWeb_News"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vHh_PNVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vHh_PdVoEeq6RL_1vDZCgg" name="IdUserWeb" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHh_PtVoEeq6RL_1vDZCgg" value="IdUserWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHh_P9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHh_QNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHh_QdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHh_QtVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHh_Q9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHh_RNVoEeq6RL_1vDZCgg" name="IdNews" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHh_RdVoEeq6RL_1vDZCgg" value="IdNews"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHh_RtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHh_R9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHh_SNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHh_SdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHh_StVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vHrJINVoEeq6RL_1vDZCgg" name="PK_Client_Contact_News">
        <node defType="com.stambia.rdbms.colref" id="_vHrJIdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vHrJItVoEeq6RL_1vDZCgg" ref="#_vHh_PdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUserWeb?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vHrJI9VoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vHrJJNVoEeq6RL_1vDZCgg" ref="#_vHh_RNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdNews?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQZnUNVoEeq6RL_1vDZCgg" name="FK_Client_UserWeb_News_Client_UserWeb">
        <node defType="com.stambia.rdbms.relation" id="_vQZnUdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQZnUtVoEeq6RL_1vDZCgg" ref="#_vHh_PdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUserWeb?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQZnU9VoEeq6RL_1vDZCgg" ref="#_DwifoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQZnVNVoEeq6RL_1vDZCgg" name="FK_Client_UserWeb_News_ref_NewsletterType">
        <node defType="com.stambia.rdbms.relation" id="_vQZnVdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQZnVtVoEeq6RL_1vDZCgg" ref="#_vHh_RNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdNews?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQZnV9VoEeq6RL_1vDZCgg" ref="#_DwZVsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vN_zkdVoEeq6RL_1vDZCgg" name="wrktrg_importcustomer">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vN_zktVoEeq6RL_1vDZCgg" value="wrktrg_importcustomer"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vN_zk9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vODd8NVoEeq6RL_1vDZCgg" name="idtask" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vODd8dVoEeq6RL_1vDZCgg" value="idtask"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vODd8tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vODd89VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vODd9NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vODd9dVoEeq6RL_1vDZCgg" value="bigint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vODd9tVoEeq6RL_1vDZCgg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOEFANVoEeq6RL_1vDZCgg" name="action" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOEFAdVoEeq6RL_1vDZCgg" value="action"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOEFAtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOEFA9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOEFBNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOEFBdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOEFBtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vNbL0dVoEeq6RL_1vDZCgg" name="Client_BI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vNby4NVoEeq6RL_1vDZCgg" value="Client_BI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vNby4dVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vNePINVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNePIdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNePItVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vNePI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNePJNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNePJdVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNePJtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNfdQNVoEeq6RL_1vDZCgg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNfdQdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNfdQtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vNfdQ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNfdRNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNfdRdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNfdRtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNgEUNVoEeq6RL_1vDZCgg" name="NbLigneConsomme" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNgEUdVoEeq6RL_1vDZCgg" value="NbLigneConsomme"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNgEUtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vNgEU9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNgEVNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNgEVdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNgEVtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNhScNVoEeq6RL_1vDZCgg" name="FrequenceMoyCmd" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNhScdVoEeq6RL_1vDZCgg" value="FrequenceMoyCmd"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNhSctVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNhSc9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNhSdNVoEeq6RL_1vDZCgg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNhSddVoEeq6RL_1vDZCgg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNh5gNVoEeq6RL_1vDZCgg" name="ProfilSaisonnier" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNh5gdVoEeq6RL_1vDZCgg" value="ProfilSaisonnier"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNh5gtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNh5g9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNh5hNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNh5hdVoEeq6RL_1vDZCgg" value="150"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNjHoNVoEeq6RL_1vDZCgg" name="PanierMoyen" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNjHodVoEeq6RL_1vDZCgg" value="PanierMoyen"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNjHotVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNjHo9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNjHpNVoEeq6RL_1vDZCgg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNjHpdVoEeq6RL_1vDZCgg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNjusNVoEeq6RL_1vDZCgg" name="NbLignesMoy" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNjusdVoEeq6RL_1vDZCgg" value="NbLignesMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNjustVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNjus9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNjutNVoEeq6RL_1vDZCgg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNjutdVoEeq6RL_1vDZCgg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNk80NVoEeq6RL_1vDZCgg" name="FreqVisiteWeb" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNk80dVoEeq6RL_1vDZCgg" value="FreqVisiteWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNk80tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNk809VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNk81NVoEeq6RL_1vDZCgg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNk81dVoEeq6RL_1vDZCgg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNlj4NVoEeq6RL_1vDZCgg" name="FreqVisiteCcial" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNlj4dVoEeq6RL_1vDZCgg" value="FreqVisiteCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNlj4tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNlj49VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNlj5NVoEeq6RL_1vDZCgg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNlj5dVoEeq6RL_1vDZCgg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNmyANVoEeq6RL_1vDZCgg" name="FreqAppel" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNmyAdVoEeq6RL_1vDZCgg" value="FreqAppel"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNmyAtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNmyA9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNmyBNVoEeq6RL_1vDZCgg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNmyBdVoEeq6RL_1vDZCgg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNoAINVoEeq6RL_1vDZCgg" name="FreqEmailing" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNoAIdVoEeq6RL_1vDZCgg" value="FreqEmailing"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNoAItVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNoAI9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNoAJNVoEeq6RL_1vDZCgg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNoAJdVoEeq6RL_1vDZCgg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNpOQNVoEeq6RL_1vDZCgg" name="SecteurVacant" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNpOQdVoEeq6RL_1vDZCgg" value="SecteurVacant"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNpOQtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNpOQ9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNpORNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNpORdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNp1UNVoEeq6RL_1vDZCgg" name="ClientPrete" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNp1UdVoEeq6RL_1vDZCgg" value="ClientPrete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNp1UtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNp1U9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNp1VNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNp1VdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNrDcNVoEeq6RL_1vDZCgg" name="CcialConge" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNrDcdVoEeq6RL_1vDZCgg" value="CcialConge"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNrDctVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNrDc9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNrDdNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNrDddVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNrqgNVoEeq6RL_1vDZCgg" name="CcialEnCharge" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNrqgdVoEeq6RL_1vDZCgg" value="CcialEnCharge"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNrqgtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNrqg9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNrqhNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNrqhdVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNsRkNVoEeq6RL_1vDZCgg" name="CcialEnPret" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNsRkdVoEeq6RL_1vDZCgg" value="CcialEnPret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNsRktVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNsRk9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNsRlNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNsRldVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vNvU4NVoEeq6RL_1vDZCgg" name="PK_Client_BI">
        <node defType="com.stambia.rdbms.colref" id="_vNvU4dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vNvU4tVoEeq6RL_1vDZCgg" ref="#_vNePINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQ9oANVoEeq6RL_1vDZCgg" name="FK_Client_BI_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQ9oAdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQ9oAtVoEeq6RL_1vDZCgg" ref="#_vNfdQNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQ9oA9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vGHp8dVoEeq6RL_1vDZCgg" name="Client_JourOuvert">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vGHp8tVoEeq6RL_1vDZCgg" value="Client_JourOuvert"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vGHp89VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vGHp9NVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGHp9dVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGHp9tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGHp99VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGHp-NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGHp-dVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGHp-tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGHp-9VoEeq6RL_1vDZCgg" name="IdJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGHp_NVoEeq6RL_1vDZCgg" value="IdJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGHp_dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGHp_tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGHp_9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGHqANVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGHqAdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vGHqAtVoEeq6RL_1vDZCgg" name="PK_Client_JourOuvert">
        <node defType="com.stambia.rdbms.colref" id="_vGHqA9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vGHqBNVoEeq6RL_1vDZCgg" ref="#_vGHp9NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vGHqBdVoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vGHqBtVoEeq6RL_1vDZCgg" ref="#_vGHp-9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdJour?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQQdYNVoEeq6RL_1vDZCgg" name="FK_Client_JourOuvert_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQQdYdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQQdYtVoEeq6RL_1vDZCgg" ref="#_vGHp9NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQQdY9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQQdZNVoEeq6RL_1vDZCgg" name="FK_Client_JourOuvert_ref_Jours1">
        <node defType="com.stambia.rdbms.relation" id="_vQQdZdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQQdZtVoEeq6RL_1vDZCgg" ref="#_vGHp-9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQQdZ9VoEeq6RL_1vDZCgg" ref="#_DnkJ0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vB__AtVoEeq6RL_1vDZCgg" name="Client_ActiviteSegment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vB__A9VoEeq6RL_1vDZCgg" value="Client_ActiviteSegment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vB__BNVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vB__BdVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vB__BtVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vB__B9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vB__CNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vB__CdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vB__CtVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vB__C9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCJI4NVoEeq6RL_1vDZCgg" name="IdActivite" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCJI4dVoEeq6RL_1vDZCgg" value="IdActivite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCJI4tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCJI49VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCJI5NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCJI5dVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCJI5tVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vCJI59VoEeq6RL_1vDZCgg" name="PK_Client_ActiviteSegment">
        <node defType="com.stambia.rdbms.colref" id="_vCJI6NVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vCJI6dVoEeq6RL_1vDZCgg" ref="#_vB__BdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vCJI6tVoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vCJI69VoEeq6RL_1vDZCgg" ref="#_vCJI4NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdActivite?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vP7GMNVoEeq6RL_1vDZCgg" name="FK_Client_ActiviteSegment_Client">
        <node defType="com.stambia.rdbms.relation" id="_vP7GMdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vP7GMtVoEeq6RL_1vDZCgg" ref="#_vB__BdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vP7GM9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vP7GNNVoEeq6RL_1vDZCgg" name="FK_Client_ActiviteSegment_ref_ActiviteSegment1">
        <node defType="com.stambia.rdbms.relation" id="_vP7GNdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vP7GNtVoEeq6RL_1vDZCgg" ref="#_vCJI4NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdActivite?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vP7GN9VoEeq6RL_1vDZCgg" ref="#_Du6vAF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vCvl0dVoEeq6RL_1vDZCgg" name="Wrk_CustomerTable">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vCvl0tVoEeq6RL_1vDZCgg" value="Wrk_CustomerTable"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vCvl09VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vCvl1NVoEeq6RL_1vDZCgg" name="CodeCli" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCvl1dVoEeq6RL_1vDZCgg" value="CodeCli"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCvl1tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCvl19VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCvl2NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCvl2dVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCvl2tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCvl29VoEeq6RL_1vDZCgg" name="DateCreation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCvl3NVoEeq6RL_1vDZCgg" value="DateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCvl3dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCvl3tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCvl39VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCvl4NVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCvl4dVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCvl4tVoEeq6RL_1vDZCgg" name="TimeCreation" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCvl49VoEeq6RL_1vDZCgg" value="TimeCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCvl5NVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vCvl5dVoEeq6RL_1vDZCgg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCvl5tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCvl59VoEeq6RL_1vDZCgg" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCvl6NVoEeq6RL_1vDZCgg" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vCvl6dVoEeq6RL_1vDZCgg" name="Statut" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vCvl6tVoEeq6RL_1vDZCgg" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vCvl69VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vCvl7NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vCvl7dVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vCvl7tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vC4vwNVoEeq6RL_1vDZCgg" name="DateTraitement" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vC4vwdVoEeq6RL_1vDZCgg" value="DateTraitement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vC4vwtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vC4vw9VoEeq6RL_1vDZCgg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vC4vxNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vC4vxdVoEeq6RL_1vDZCgg" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vC4vxtVoEeq6RL_1vDZCgg" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vC4vx9VoEeq6RL_1vDZCgg" name="ProgrammeTRT" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vC4vyNVoEeq6RL_1vDZCgg" value="ProgrammeTRT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vC4vydVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vC4vytVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vC4vy9VoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vC4vzNVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vC4vzdVoEeq6RL_1vDZCgg" name="Action" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vC4vztVoEeq6RL_1vDZCgg" value="Action"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vC4vz9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vC4v0NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vC4v0dVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vC4v0tVoEeq6RL_1vDZCgg" value="15"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u9OMvtVoEeq6RL_1vDZCgg" name="ref_Societe">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u9OMv9VoEeq6RL_1vDZCgg" value="ref_Societe"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u9OMwNVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u9XWkNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9XWkdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9XWktVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9XWk9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9XWlNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9XWldVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9XWltVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9XWl9VoEeq6RL_1vDZCgg" name="LibSociete" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9XWmNVoEeq6RL_1vDZCgg" value="LibSociete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9XWmdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9XWmtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9XWm9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9XWnNVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9XWndVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9XWntVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9XWn9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9XWoNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9XWodVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9XWotVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u9XWo9VoEeq6RL_1vDZCgg" name="PK_ref_Societe">
        <node defType="com.stambia.rdbms.colref" id="_u9XWpNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u9XWpdVoEeq6RL_1vDZCgg" ref="#_u9XWkNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vO_SENVoEeq6RL_1vDZCgg" name="ref_NbEleves">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vO_SEdVoEeq6RL_1vDZCgg" value="ref_NbEleves"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vO_SEtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vPEKkNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vPEKkdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vPEKktVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vPEKk9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vPEKlNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vPEKldVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vPEKltVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vPHN4NVoEeq6RL_1vDZCgg" name="NbEleves" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vPHN4dVoEeq6RL_1vDZCgg" value="NbEleves"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vPHN4tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vPHN49VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vPHN5NVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vPHN5dVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vPKRMNVoEeq6RL_1vDZCgg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vPKRMdVoEeq6RL_1vDZCgg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vPKRMtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vPKRM9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vPKRNNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vPKRNdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vPKRNtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vPMGYNVoEeq6RL_1vDZCgg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vPMGYdVoEeq6RL_1vDZCgg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vPMGYtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vPMGY9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vPMGZNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vPMGZdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vPMGZtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vPN7kNVoEeq6RL_1vDZCgg" name="PK_ref_NbEleves">
        <node defType="com.stambia.rdbms.colref" id="_vPN7kdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vPN7ktVoEeq6RL_1vDZCgg" ref="#_vPEKkNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vArwZNVoEeq6RL_1vDZCgg" name="Client_UserWebTel">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vArwZdVoEeq6RL_1vDZCgg" value="Client_UserWebTel"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vArwZtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vA06QNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vA06QdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vA06QtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vA06Q9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vA06RNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vA06RdVoEeq6RL_1vDZCgg" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vA06RtVoEeq6RL_1vDZCgg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vA06R9VoEeq6RL_1vDZCgg" name="IdUser" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vA06SNVoEeq6RL_1vDZCgg" value="IdUser"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vA06SdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vA06StVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vA06S9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vA06TNVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vA06TdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vA06TtVoEeq6RL_1vDZCgg" name="Numero" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vA06T9VoEeq6RL_1vDZCgg" value="Numero"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vA06UNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vA06UdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vA06UtVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vA06U9VoEeq6RL_1vDZCgg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vA06VNVoEeq6RL_1vDZCgg" name="Idtype_Tel" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vA06VdVoEeq6RL_1vDZCgg" value="Idtype_Tel"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vA06VtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vA06V9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vA06WNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vA06WdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vA06WtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vA06W9VoEeq6RL_1vDZCgg" name="PK_Client_UserTel">
        <node defType="com.stambia.rdbms.colref" id="_vA06XNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vA06XdVoEeq6RL_1vDZCgg" ref="#_vA06QNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPuR4NVoEeq6RL_1vDZCgg" name="FK_Client_UserWebTel_Client_UserWeb">
        <node defType="com.stambia.rdbms.relation" id="_vPuR4dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPuR4tVoEeq6RL_1vDZCgg" ref="#_vA06R9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUser?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPuR49VoEeq6RL_1vDZCgg" ref="#_DwifoF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPu48NVoEeq6RL_1vDZCgg" name="FK_Client_UserWebTel_ref_TypeTel">
        <node defType="com.stambia.rdbms.relation" id="_vPu48dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPu48tVoEeq6RL_1vDZCgg" ref="#_vA06VNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Idtype_Tel?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPu489VoEeq6RL_1vDZCgg" ref="#_DpnYQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vDP8JNVoEeq6RL_1vDZCgg" name="ref_TicketRepasMoyen">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vDP8JdVoEeq6RL_1vDZCgg" value="ref_TicketRepasMoyen"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vDP8JtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vDZtINVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDZtIdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDZtItVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vDZtI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDZtJNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDZtJdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDZtJtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vDZtJ9VoEeq6RL_1vDZCgg" name="TicketMoy" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDZtKNVoEeq6RL_1vDZCgg" value="TicketMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDZtKdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDZtKtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDZtK9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDZtLNVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vDjeINVoEeq6RL_1vDZCgg" name="MntMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDjeIdVoEeq6RL_1vDZCgg" value="MntMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDjeItVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vDjeI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDjeJNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDjeJdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDjeJtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vDjeJ9VoEeq6RL_1vDZCgg" name="MntMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDjeKNVoEeq6RL_1vDZCgg" value="MntMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDjeKdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vDjeKtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDjeK9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDjeLNVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDjeLdVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vDjeLtVoEeq6RL_1vDZCgg" name="PK_ref_TicketRepasMoyen">
        <node defType="com.stambia.rdbms.colref" id="_vDjeL9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vDjeMNVoEeq6RL_1vDZCgg" ref="#_vDZtINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u9qRkdVoEeq6RL_1vDZCgg" name="ref_Commune">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u9qRktVoEeq6RL_1vDZCgg" value="ref_Commune"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u9qRk9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u9qRlNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9qRldVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9qRltVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9qRl9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9qRmNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9qRmdVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9qRmtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u90CgNVoEeq6RL_1vDZCgg" name="IdDepartement" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u90CgdVoEeq6RL_1vDZCgg" value="IdDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u90CgtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u90Cg9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u90ChNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u90ChdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u90ChtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u90Ch9VoEeq6RL_1vDZCgg" name="NumINSEECommune" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u90CiNVoEeq6RL_1vDZCgg" value="NumINSEECommune"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u90CidVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u90CitVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u90Ci9VoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u90CjNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u90CjdVoEeq6RL_1vDZCgg" name="PrtculariteCom" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_u90CjtVoEeq6RL_1vDZCgg" value="PrtculariteCom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u90Cj9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u90CkNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u90CkdVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u90CktVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u90Ck9VoEeq6RL_1vDZCgg" name="ComOuLieudit" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_u90ClNVoEeq6RL_1vDZCgg" value="ComOuLieudit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u90CldVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u90CltVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u90Cl9VoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u90CmNVoEeq6RL_1vDZCgg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u90CmdVoEeq6RL_1vDZCgg" name="CodePostal" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_u90CmtVoEeq6RL_1vDZCgg" value="CodePostal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u90Cm9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u90CnNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u90CndVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u90CntVoEeq6RL_1vDZCgg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u90Cn9VoEeq6RL_1vDZCgg" name="PrtculariteBDist" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_u90CoNVoEeq6RL_1vDZCgg" value="PrtculariteBDist"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u90CodVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u90CotVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u90Co9VoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u90CpNVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u90CpdVoEeq6RL_1vDZCgg" name="LigneAcheminement" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_u90CptVoEeq6RL_1vDZCgg" value="LigneAcheminement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u90Cp9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u90CqNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u90CqdVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u90CqtVoEeq6RL_1vDZCgg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u90Cq9VoEeq6RL_1vDZCgg" name="CodeComptaPTT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_u90CrNVoEeq6RL_1vDZCgg" value="CodeComptaPTT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u90CrdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u90CrtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u90Cr9VoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u90CsNVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u90CsdVoEeq6RL_1vDZCgg" name="Com" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_u90CstVoEeq6RL_1vDZCgg" value="Com"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u90Cs9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u90CtNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u90CtdVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u90CttVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u99zgNVoEeq6RL_1vDZCgg" name="INSEEComRatache" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_u99zgdVoEeq6RL_1vDZCgg" value="INSEEComRatache"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u99zgtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u99zg9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u99zhNVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u99zhdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u99zhtVoEeq6RL_1vDZCgg" name="PK_ref_Commune">
        <node defType="com.stambia.rdbms.colref" id="_u99zh9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u99ziNVoEeq6RL_1vDZCgg" ref="#_u9qRlNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vNQMsdVoEeq6RL_1vDZCgg" name="ref_Concurrent">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vNQMstVoEeq6RL_1vDZCgg" value="ref_Concurrent"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vNQMs9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vNUeINVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNUeIdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNUeItVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vNUeI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNUeJNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNUeJdVoEeq6RL_1vDZCgg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNUeJtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNVsQNVoEeq6RL_1vDZCgg" name="LibConcurrent" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNVsQdVoEeq6RL_1vDZCgg" value="LibConcurrent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNVsQtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNVsQ9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNVsRNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNVsRdVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNWTUNVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNWTUdVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNWTUtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNWTU9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNWTVNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNWTVdVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vNZWoNVoEeq6RL_1vDZCgg" name="PK_ref_Concurrent">
        <node defType="com.stambia.rdbms.colref" id="_vNZWodVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vNZWotVoEeq6RL_1vDZCgg" ref="#_vNUeINVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vMbGQdVoEeq6RL_1vDZCgg" name="Client_UserWeb">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vMbGQtVoEeq6RL_1vDZCgg" value="Client_UserWeb"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vMbGQ9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vMfXsNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMfXsdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMfXstVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMfXs9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMfXtNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMfXtdVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMfXttVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMf-wNVoEeq6RL_1vDZCgg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMf-wdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMf-wtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMf-w9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMf-xNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMf-xdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMf-xtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMhM4NVoEeq6RL_1vDZCgg" name="IdGenre" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMhM4dVoEeq6RL_1vDZCgg" value="IdGenre"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMhM4tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMhM49VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMhM5NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMhM5dVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMhM5tVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMhz8NVoEeq6RL_1vDZCgg" name="Nom" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMhz8dVoEeq6RL_1vDZCgg" value="Nom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMhz8tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMhz89VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMhz9NVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMhz9dVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMjCENVoEeq6RL_1vDZCgg" name="Prenom" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMjCEdVoEeq6RL_1vDZCgg" value="Prenom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMjCEtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMjCE9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMjCFNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMjCFdVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMkQMNVoEeq6RL_1vDZCgg" name="IntitulePoste" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMkQMdVoEeq6RL_1vDZCgg" value="IntitulePoste"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMkQMtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMkQM9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMkQNNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMkQNdVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMleUNVoEeq6RL_1vDZCgg" name="Mail" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMleUdVoEeq6RL_1vDZCgg" value="Mail"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMleUtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMleU9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMleVNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMleVdVoEeq6RL_1vDZCgg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMmscNVoEeq6RL_1vDZCgg" name="IdWeb" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMmscdVoEeq6RL_1vDZCgg" value="IdWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMmsctVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMmsc9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMmsdNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMmsddVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMn6kNVoEeq6RL_1vDZCgg" name="IdTypeComSouhait" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMn6kdVoEeq6RL_1vDZCgg" value="IdTypeComSouhait"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMn6ktVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMn6k9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMn6lNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMn6ldVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMn6ltVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMpIsNVoEeq6RL_1vDZCgg" name="OptinEmail" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMpIsdVoEeq6RL_1vDZCgg" value="OptinEmail"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMpIstVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMpIs9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMpItNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMpItdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMqW0NVoEeq6RL_1vDZCgg" name="DateCrt" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMqW0dVoEeq6RL_1vDZCgg" value="DateCrt"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMqW0tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMqW09VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMqW1NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMqW1dVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMqW1tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMrk8NVoEeq6RL_1vDZCgg" name="DateSup" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMrk8dVoEeq6RL_1vDZCgg" value="DateSup"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMrk8tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMrk89VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMrk9NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMrk9dVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMrk9tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMszENVoEeq6RL_1vDZCgg" name="TopSup" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMszEdVoEeq6RL_1vDZCgg" value="TopSup"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMszEtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMszE9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMszFNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMszFdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMuBMNVoEeq6RL_1vDZCgg" name="IdNiveau" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMuBMdVoEeq6RL_1vDZCgg" value="IdNiveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMuBMtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMuBM9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMuBNNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMuBNdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMuBNtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMvPUNVoEeq6RL_1vDZCgg" name="IdUserMagento" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMvPUdVoEeq6RL_1vDZCgg" value="IdUserMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMvPUtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMvPU9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMvPVNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMvPVdVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMwdcNVoEeq6RL_1vDZCgg" name="OptinSMS" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMwdcdVoEeq6RL_1vDZCgg" value="OptinSMS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMwdctVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMwdc9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMwddNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMwdddVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vMy5sNVoEeq6RL_1vDZCgg" name="PK_Contact">
        <node defType="com.stambia.rdbms.colref" id="_vMy5sdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vMy5stVoEeq6RL_1vDZCgg" ref="#_vMfXsNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQ3hYNVoEeq6RL_1vDZCgg" name="FK_Client_UserWeb_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQ3hYdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQ3hYtVoEeq6RL_1vDZCgg" ref="#_vMf-wNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQ3hY9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQ3hZNVoEeq6RL_1vDZCgg" name="FK_Client_UserWeb_ref_Genre">
        <node defType="com.stambia.rdbms.relation" id="_vQ3hZdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQ3hZtVoEeq6RL_1vDZCgg" ref="#_vMhM4NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdGenre?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQ3hZ9VoEeq6RL_1vDZCgg" ref="#_DyOhsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQ4IcNVoEeq6RL_1vDZCgg" name="FK_Client_UserWeb_ref_NiveauUser">
        <node defType="com.stambia.rdbms.relation" id="_vQ4IcdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQ4IctVoEeq6RL_1vDZCgg" ref="#_vMuBMNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdNiveau?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQ4Ic9VoEeq6RL_1vDZCgg" ref="#_DvDR4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQ4IdNVoEeq6RL_1vDZCgg" name="FK_Client_UserWeb_ref_TypeComSouhaite">
        <node defType="com.stambia.rdbms.relation" id="_vQ4IddVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQ4IdtVoEeq6RL_1vDZCgg" ref="#_vMn6kNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdTypeComSouhait?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQ4Id9VoEeq6RL_1vDZCgg" ref="#_Dxyc0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vD6Dh9VoEeq6RL_1vDZCgg" name="Client_Organisation">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vD6DiNVoEeq6RL_1vDZCgg" value="Client_Organisation"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vD6DidVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vED0cNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vED0cdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vED0ctVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vED0c9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vED0dNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vED0ddVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vED0dtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vED0d9VoEeq6RL_1vDZCgg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vED0eNVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vED0edVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vED0etVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vED0e9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vED0fNVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vED0fdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vED0ftVoEeq6RL_1vDZCgg" name="Secteur" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vED0f9VoEeq6RL_1vDZCgg" value="Secteur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vED0gNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vED0gdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vED0gtVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vED0g9VoEeq6RL_1vDZCgg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vED0hNVoEeq6RL_1vDZCgg" name="Dic" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vED0hdVoEeq6RL_1vDZCgg" value="Dic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vED0htVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vED0h9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vED0iNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vED0idVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vED0itVoEeq6RL_1vDZCgg" name="Dec" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vED0i9VoEeq6RL_1vDZCgg" value="Dec"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vED0jNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vED0jdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vED0jtVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vED0j9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vENlcNVoEeq6RL_1vDZCgg" name="MagasinFavori" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vENlcdVoEeq6RL_1vDZCgg" value="MagasinFavori"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vENlctVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vENlc9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vENldNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vENlddVoEeq6RL_1vDZCgg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vENldtVoEeq6RL_1vDZCgg" name="CentraleAchat" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vENld9VoEeq6RL_1vDZCgg" value="CentraleAchat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vENleNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vENledVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vENletVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vENle9VoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vENlfNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vENlfdVoEeq6RL_1vDZCgg" name="AppelOffres" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vENlftVoEeq6RL_1vDZCgg" value="AppelOffres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vENlf9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vENlgNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vENlgdVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vENlgtVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vENlg9VoEeq6RL_1vDZCgg" name="MarchePublic" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_vENlhNVoEeq6RL_1vDZCgg" value="MarchePublic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vENlhdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vENlhtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vENlh9VoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vENliNVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vENlidVoEeq6RL_1vDZCgg" name="DateFinContrat" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_vENlitVoEeq6RL_1vDZCgg" value="DateFinContrat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vENli9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vENljNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vENljdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vENljtVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vENlj9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vENlkNVoEeq6RL_1vDZCgg" name="TaciteReconduction" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_vENlkdVoEeq6RL_1vDZCgg" value="TaciteReconduction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vENlktVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vENlk9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vENllNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vENlldVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vENlltVoEeq6RL_1vDZCgg" name="MercuFerme" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_vENll9VoEeq6RL_1vDZCgg" value="MercuFerme"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vENlmNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vENlmdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vENlmtVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vENlm9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vENlnNVoEeq6RL_1vDZCgg" name="Mercuriale" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_vENlndVoEeq6RL_1vDZCgg" value="Mercuriale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vENlntVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vENln9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vENloNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vENlodVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWvYNVoEeq6RL_1vDZCgg" name="FctParBudget" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvYdVoEeq6RL_1vDZCgg" value="FctParBudget"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvYtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWvY9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWvZNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWvZdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWvZtVoEeq6RL_1vDZCgg" name="VisiteCcial" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvZ9VoEeq6RL_1vDZCgg" value="VisiteCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvaNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWvadVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWvatVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWva9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWvbNVoEeq6RL_1vDZCgg" name="AppelOutcall" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvbdVoEeq6RL_1vDZCgg" value="AppelOutcall"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvbtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWvb9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWvcNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWvcdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWvctVoEeq6RL_1vDZCgg" name="PassageMag" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvc9VoEeq6RL_1vDZCgg" value="PassageMag"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvdNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWvddVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWvdtVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWvd9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWveNVoEeq6RL_1vDZCgg" name="PrefOutcall" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvedVoEeq6RL_1vDZCgg" value="PrefOutcall"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvetVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWve9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWvfNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWvfdVoEeq6RL_1vDZCgg" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWvftVoEeq6RL_1vDZCgg" name="PrefVisites" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvf9VoEeq6RL_1vDZCgg" value="PrefVisites"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvgNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWvgdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWvgtVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWvg9VoEeq6RL_1vDZCgg" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWvhNVoEeq6RL_1vDZCgg" name="AdherentCtraleAch" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvhdVoEeq6RL_1vDZCgg" value="AdherentCtraleAch"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvhtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWvh9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWviNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWvidVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWvitVoEeq6RL_1vDZCgg" name="NumAdherent" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvi9VoEeq6RL_1vDZCgg" value="NumAdherent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvjNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWvjdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWvjtVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWvj9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWvkNVoEeq6RL_1vDZCgg" name="HoraireLivSouhait" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvkdVoEeq6RL_1vDZCgg" value="HoraireLivSouhait"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvktVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWvk9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWvlNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWvldVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEWvltVoEeq6RL_1vDZCgg" name="NumCtraleAch" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEWvl9VoEeq6RL_1vDZCgg" value="NumCtraleAch"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEWvmNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEWvmdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEWvmtVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEWvm9VoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEggYNVoEeq6RL_1vDZCgg" name="TypeOp" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEggYdVoEeq6RL_1vDZCgg" value="TypeOp"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEggYtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vEggY9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEggZNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEggZdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEggZtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEggZ9VoEeq6RL_1vDZCgg" name="MercuContrat" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEggaNVoEeq6RL_1vDZCgg" value="MercuContrat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEggadVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEggatVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEgga9VoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEggbNVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEggbdVoEeq6RL_1vDZCgg" name="RespMercu" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEggbtVoEeq6RL_1vDZCgg" value="RespMercu"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEggb9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEggcNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEggcdVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEggctVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEggc9VoEeq6RL_1vDZCgg" name="FacturesDemat" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEggdNVoEeq6RL_1vDZCgg" value="FacturesDemat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEggddVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEggdtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEggd9VoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEggeNVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEggedVoEeq6RL_1vDZCgg" name="MailFacture" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEggetVoEeq6RL_1vDZCgg" value="MailFacture"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEgge9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEggfNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEggfdVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEggftVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEggf9VoEeq6RL_1vDZCgg" name="multicompte" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEgggNVoEeq6RL_1vDZCgg" value="multicompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEgggdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEgggtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEggg9VoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEgghNVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEgghdVoEeq6RL_1vDZCgg" name="Chaine" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEgghtVoEeq6RL_1vDZCgg" value="Chaine"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEggh9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEggiNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEggidVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEggitVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEggi9VoEeq6RL_1vDZCgg" name="Centralisateur" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEggjNVoEeq6RL_1vDZCgg" value="Centralisateur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEggjdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEggjtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEggj9VoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEggkNVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEggkdVoEeq6RL_1vDZCgg" name="HabitudeCmd" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEggktVoEeq6RL_1vDZCgg" value="HabitudeCmd"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEggk9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEgglNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEggldVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEggltVoEeq6RL_1vDZCgg" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vEpqUNVoEeq6RL_1vDZCgg" name="PK_Client_Organisation">
        <node defType="com.stambia.rdbms.colref" id="_vEpqUdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vEpqUtVoEeq6RL_1vDZCgg" ref="#_vED0cNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQGsYNVoEeq6RL_1vDZCgg" name="FK_Client_Organisation_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQGsYdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQGsYtVoEeq6RL_1vDZCgg" ref="#_vED0d9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQHTcNVoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQHTcdVoEeq6RL_1vDZCgg" name="FK_Client_Organisation_ref_TypeOperation">
        <node defType="com.stambia.rdbms.relation" id="_vQHTctVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQHTc9VoEeq6RL_1vDZCgg" ref="#_vEggYNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeOp?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQHTdNVoEeq6RL_1vDZCgg" ref="#_DyDikF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vEpqVNVoEeq6RL_1vDZCgg" name="Client_Telephone">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vEpqVdVoEeq6RL_1vDZCgg" value="Client_Telephone"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vEpqVtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vEpqV9VoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEpqWNVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEpqWdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vEpqWtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEpqW9VoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEpqXNVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEpqXdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEpqXtVoEeq6RL_1vDZCgg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEpqX9VoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEpqYNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vEpqYdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEpqYtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEpqY9VoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEpqZNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEpqZdVoEeq6RL_1vDZCgg" name="Numero" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEpqZtVoEeq6RL_1vDZCgg" value="Numero"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEpqZ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEpqaNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEpqadVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEpqatVoEeq6RL_1vDZCgg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vEpqa9VoEeq6RL_1vDZCgg" name="IdType" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vEpqbNVoEeq6RL_1vDZCgg" value="IdType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vEpqbdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vEpqbtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vEpqb9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vEpqcNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vEpqcdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vEzbUNVoEeq6RL_1vDZCgg" name="PK_Telephone">
        <node defType="com.stambia.rdbms.colref" id="_vEzbUdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vEzbUtVoEeq6RL_1vDZCgg" ref="#_vEpqV9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQH6gNVoEeq6RL_1vDZCgg" name="FK_Client_Telephone_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQH6gdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQH6gtVoEeq6RL_1vDZCgg" ref="#_vEpqXtVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQH6g9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQH6hNVoEeq6RL_1vDZCgg" name="FK_Telephone_ref_TypeTel">
        <node defType="com.stambia.rdbms.relation" id="_vQH6hdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQH6htVoEeq6RL_1vDZCgg" ref="#_vEpqa9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdType?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQH6h9VoEeq6RL_1vDZCgg" ref="#_DpnYQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u_uuEdVoEeq6RL_1vDZCgg" name="ref_Pays">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u_uuEtVoEeq6RL_1vDZCgg" value="ref_Pays"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u_uuE9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u_34ANVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_34AdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_34AtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u_34A9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_34BNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_34BdVoEeq6RL_1vDZCgg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_34BtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_34B9VoEeq6RL_1vDZCgg" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_34CNVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_34CdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u_34CtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_34C9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_34DNVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_34DdVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_34DtVoEeq6RL_1vDZCgg" name="Alpha2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_34D9VoEeq6RL_1vDZCgg" value="Alpha2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_34ENVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_34EdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_34EtVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_34E9VoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_34FNVoEeq6RL_1vDZCgg" name="Alpha3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_34FdVoEeq6RL_1vDZCgg" value="Alpha3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_34FtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_34F9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_34GNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_34GdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_34GtVoEeq6RL_1vDZCgg" name="Nom_en_gb" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_34G9VoEeq6RL_1vDZCgg" value="Nom_en_gb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_34HNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_34HdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_34HtVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_34H9VoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u_34INVoEeq6RL_1vDZCgg" name="Nom_fr_fr" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_u_34IdVoEeq6RL_1vDZCgg" value="Nom_fr_fr"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u_34ItVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u_34I9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u_34JNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u_34JdVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u_34JtVoEeq6RL_1vDZCgg" name="PK_ref_Pays">
        <node defType="com.stambia.rdbms.colref" id="_u_34J9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u_34KNVoEeq6RL_1vDZCgg" ref="#_u_34ANVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u9hHp9VoEeq6RL_1vDZCgg" name="ref_Mois">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u9hHqNVoEeq6RL_1vDZCgg" value="ref_Mois"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u9hHqdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u9qRgNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9qRgdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9qRgtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9qRg9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9qRhNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9qRhdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9qRhtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9qRh9VoEeq6RL_1vDZCgg" name="LibMois" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9qRiNVoEeq6RL_1vDZCgg" value="LibMois"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9qRidVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9qRitVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9qRi9VoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9qRjNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u9qRjdVoEeq6RL_1vDZCgg" name="PK_ref_Mois">
        <node defType="com.stambia.rdbms.colref" id="_u9qRjtVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u9qRj9VoEeq6RL_1vDZCgg" ref="#_u9qRgNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u8kFX9VoEeq6RL_1vDZCgg" name="ref_NiveauFidelite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u8kFYNVoEeq6RL_1vDZCgg" value="ref_NiveauFidelite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u8kFYdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u8kFYtVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8kFY9VoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8kFZNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u8kFZdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8kFZtVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8kFZ9VoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8kFaNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8t2UNVoEeq6RL_1vDZCgg" name="LibNivFidelite" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8t2UdVoEeq6RL_1vDZCgg" value="LibNivFidelite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8t2UtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8t2U9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8t2VNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8t2VdVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8t2VtVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8t2V9VoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8t2WNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8t2WdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8t2WtVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8t2W9VoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u8t2XNVoEeq6RL_1vDZCgg" name="PK_ref_NiveauFidelite">
        <node defType="com.stambia.rdbms.colref" id="_u8t2XdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u8t2XtVoEeq6RL_1vDZCgg" ref="#_u8kFYtVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vLR2wdVoEeq6RL_1vDZCgg" name="ref_AnneesExistence">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vLR2wtVoEeq6RL_1vDZCgg" value="ref_AnneesExistence"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vLR2w9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vLU6ENVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLU6EdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLU6EtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLU6E9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLU6FNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLU6FdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLU6FtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLWIMNVoEeq6RL_1vDZCgg" name="libAnneeExistence" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLWIMdVoEeq6RL_1vDZCgg" value="libAnneeExistence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLWIMtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLWIM9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLWINNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLWINdVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLWvQNVoEeq6RL_1vDZCgg" name="AnneeMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLWvQdVoEeq6RL_1vDZCgg" value="AnneeMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLWvQtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLWvQ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLWvRNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLWvRdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLWvRtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLX9YNVoEeq6RL_1vDZCgg" name="AnneeMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLX9YdVoEeq6RL_1vDZCgg" value="AnneeMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLX9YtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLX9Y9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLX9ZNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLX9ZdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLX9ZtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vLZykNVoEeq6RL_1vDZCgg" name="PK_ref_AnneesExistence">
        <node defType="com.stambia.rdbms.colref" id="_vLZykdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vLZyktVoEeq6RL_1vDZCgg" ref="#_vLU6ENVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vFUYtNVoEeq6RL_1vDZCgg" name="ref_TypeTel">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vFUYtdVoEeq6RL_1vDZCgg" value="ref_TypeTel"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vFUYttVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vFUYt9VoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFUYuNVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFUYudVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vFUYutVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFUYu9VoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFUYvNVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFUYvdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vFUYvtVoEeq6RL_1vDZCgg" name="Type" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vFUYv9VoEeq6RL_1vDZCgg" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vFUYwNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vFUYwdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vFUYwtVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vFUYw9VoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vFdioNVoEeq6RL_1vDZCgg" name="PK_ref_TypeTel">
        <node defType="com.stambia.rdbms.colref" id="_vFdiodVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vFdiotVoEeq6RL_1vDZCgg" ref="#_vFUYt9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vI2N59VoEeq6RL_1vDZCgg" name="Infos_CHD">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vI2N6NVoEeq6RL_1vDZCgg" value="Infos_CHD"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vI2N6dVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vI_XwNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vI_XwdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vI_XwtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vI_Xw9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vI_XxNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vI_XxdVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vI_XxtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vI_Xx9VoEeq6RL_1vDZCgg" name="Token" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vI_XyNVoEeq6RL_1vDZCgg" value="Token"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vI_XydVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vI_XytVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vI_Xy9VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vI_XzNVoEeq6RL_1vDZCgg" value="500"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vI_XzdVoEeq6RL_1vDZCgg" name="ParamRequest" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vI_XztVoEeq6RL_1vDZCgg" value="ParamRequest"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vI_Xz9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vI_X0NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vI_X0dVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vI_X0tVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vI_X09VoEeq6RL_1vDZCgg" name="TextResponse" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vI_X1NVoEeq6RL_1vDZCgg" value="TextResponse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vI_X1dVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vI_X1tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vI_X19VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vI_X2NVoEeq6RL_1vDZCgg" value="max"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vI_X2dVoEeq6RL_1vDZCgg" name="FileName" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vI_X2tVoEeq6RL_1vDZCgg" value="FileName"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vI_X29VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vI_X3NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vI_X3dVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vI_X3tVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vI_X39VoEeq6RL_1vDZCgg" name="CreateDate" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vI_X4NVoEeq6RL_1vDZCgg" value="CreateDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vI_X4dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vI_X4tVoEeq6RL_1vDZCgg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vI_X49VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vI_X5NVoEeq6RL_1vDZCgg" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vI_X5dVoEeq6RL_1vDZCgg" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJJIwNVoEeq6RL_1vDZCgg" name="Traite" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJJIwdVoEeq6RL_1vDZCgg" value="Traite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJJIwtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJJIw9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJJIxNVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJJIxdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJJIxtVoEeq6RL_1vDZCgg" name="Status" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJJIx9VoEeq6RL_1vDZCgg" value="Status"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJJIyNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJJIydVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJJIytVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJJIy9VoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vJJIzNVoEeq6RL_1vDZCgg" name="PK_Infos_CHD">
        <node defType="com.stambia.rdbms.colref" id="_vJJIzdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vJJIztVoEeq6RL_1vDZCgg" ref="#_vI_XwNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vD2ZFNVoEeq6RL_1vDZCgg" name="ref_StatutClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vD2ZFdVoEeq6RL_1vDZCgg" value="ref_StatutClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vD2ZFtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vD6DcNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vD6DcdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vD6DctVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vD6Dc9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vD6DdNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vD6DddVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vD6DdtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vD6Dd9VoEeq6RL_1vDZCgg" name="LibStatut" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vD6DeNVoEeq6RL_1vDZCgg" value="LibStatut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vD6DedVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vD6DetVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vD6De9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vD6DfNVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vD6DfdVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vD6DftVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vD6Df9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vD6DgNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vD6DgdVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vD6DgtVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vD6Dg9VoEeq6RL_1vDZCgg" name="PK_ref_StatutClient">
        <node defType="com.stambia.rdbms.colref" id="_vD6DhNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vD6DhdVoEeq6RL_1vDZCgg" ref="#_vD6DcNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vGRa8dVoEeq6RL_1vDZCgg" name="Client_UniversConso">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vGRa8tVoEeq6RL_1vDZCgg" value="Client_UniversConso"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vGRa89VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vGRa9NVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGRa9dVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGRa9tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGRa99VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGRa-NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGRa-dVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGRa-tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGRa-9VoEeq6RL_1vDZCgg" name="IdUniversProduit" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGRa_NVoEeq6RL_1vDZCgg" value="IdUniversProduit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGRa_dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGRa_tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGRa_9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGRbANVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGRbAdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vGRbAtVoEeq6RL_1vDZCgg" name="PK_Client_UniversConso">
        <node defType="com.stambia.rdbms.colref" id="_vGRbA9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vGRbBNVoEeq6RL_1vDZCgg" ref="#_vGRa9NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vGRbBdVoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vGRbBtVoEeq6RL_1vDZCgg" ref="#_vGRa-9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUniversProduit?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQSSkNVoEeq6RL_1vDZCgg" name="FK_Client_UniversConso_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQSSkdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQSSktVoEeq6RL_1vDZCgg" ref="#_vGRa9NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQSSk9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQSSlNVoEeq6RL_1vDZCgg" name="FK_Client_UniversConso_ref_UniversConso1">
        <node defType="com.stambia.rdbms.relation" id="_vQSSldVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQSSltVoEeq6RL_1vDZCgg" ref="#_vGRa-9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUniversProduit?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQSSl9VoEeq6RL_1vDZCgg" ref="#_DfXQEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vAh_YtVoEeq6RL_1vDZCgg" name="Client_RepasServis">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vAh_Y9VoEeq6RL_1vDZCgg" value="Client_RepasServis"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vAh_ZNVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vArwUNVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vArwUdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vArwUtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vArwU9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vArwVNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vArwVdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vArwVtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vArwV9VoEeq6RL_1vDZCgg" name="IdRepas" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vArwWNVoEeq6RL_1vDZCgg" value="IdRepas"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vArwWdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vArwWtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vArwW9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vArwXNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vArwXdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vArwXtVoEeq6RL_1vDZCgg" name="PK_Client_RepasServis">
        <node defType="com.stambia.rdbms.colref" id="_vArwX9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vArwYNVoEeq6RL_1vDZCgg" ref="#_vArwUNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vArwYdVoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vArwYtVoEeq6RL_1vDZCgg" ref="#_vArwV9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdRepas?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPtq0NVoEeq6RL_1vDZCgg" name="FK_Client_RepasServis_Client">
        <node defType="com.stambia.rdbms.relation" id="_vPtq0dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPtq0tVoEeq6RL_1vDZCgg" ref="#_vArwUNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPtq09VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPtq1NVoEeq6RL_1vDZCgg" name="FK_Client_RepasServis_ref_RepasServis1">
        <node defType="com.stambia.rdbms.relation" id="_vPtq1dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPtq1tVoEeq6RL_1vDZCgg" ref="#_vArwV9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdRepas?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPtq19VoEeq6RL_1vDZCgg" ref="#_DtJ0cF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u75-BNVoEeq6RL_1vDZCgg" name="Client_JourLiv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u75-BdVoEeq6RL_1vDZCgg" value="Client_JourLiv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u75-BtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u75-B9VoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u75-CNVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u75-CdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u75-CtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u75-C9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u75-DNVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u75-DdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u75-DtVoEeq6RL_1vDZCgg" name="IdJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u75-D9VoEeq6RL_1vDZCgg" value="IdJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u75-ENVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u75-EdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u75-EtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u75-E9VoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u75-FNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u8DvANVoEeq6RL_1vDZCgg" name="PK_Client_JourLiv">
        <node defType="com.stambia.rdbms.colref" id="_u8DvAdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u8DvAtVoEeq6RL_1vDZCgg" ref="#_u75-B9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_u8DvA9VoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u8DvBNVoEeq6RL_1vDZCgg" ref="#_u75-DtVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdJour?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPb-ANVoEeq6RL_1vDZCgg" name="FK_Client_JourLiv_Client">
        <node defType="com.stambia.rdbms.relation" id="_vPb-AdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPb-AtVoEeq6RL_1vDZCgg" ref="#_u75-B9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPb-A9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPb-BNVoEeq6RL_1vDZCgg" name="FK_Client_JourLiv_ref_Jours1">
        <node defType="com.stambia.rdbms.relation" id="_vPb-BdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPb-BtVoEeq6RL_1vDZCgg" ref="#_u75-DtVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPb-B9VoEeq6RL_1vDZCgg" ref="#_DnkJ0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vBV3vtVoEeq6RL_1vDZCgg" name="ref_EnvTouristique">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vBV3v9VoEeq6RL_1vDZCgg" value="ref_EnvTouristique"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vBV3wNVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vBfBkNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBfBkdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBfBktVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vBfBk9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBfBlNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBfBldVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBfBltVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBoykNVoEeq6RL_1vDZCgg" name="LibEnvTouristique" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBoykdVoEeq6RL_1vDZCgg" value="LibEnvTouristique"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBoyktVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBoyk9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBoylNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBoyldVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBoyltVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBoyl9VoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBoymNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vBoymdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBoymtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBoym9VoEeq6RL_1vDZCgg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBoynNVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vBoyndVoEeq6RL_1vDZCgg" name="PK_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.colref" id="_vBoyntVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vBoyn9VoEeq6RL_1vDZCgg" ref="#_vBfBkNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vBoyodVoEeq6RL_1vDZCgg" name="ref_ScoringFinance">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vBoyotVoEeq6RL_1vDZCgg" value="ref_ScoringFinance"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vBoyo9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vByjkNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vByjkdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vByjktVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vByjk9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vByjlNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vByjldVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vByjltVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vByjl9VoEeq6RL_1vDZCgg" name="LibScoringFinance" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vByjmNVoEeq6RL_1vDZCgg" value="LibScoringFinance"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vByjmdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vByjmtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vByjm9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vByjnNVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vByjndVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vByjntVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vByjn9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vByjoNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vByjodVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vByjotVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vByjo9VoEeq6RL_1vDZCgg" name="PK_ref_ScoringFinance">
        <node defType="com.stambia.rdbms.colref" id="_vByjpNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vByjpdVoEeq6RL_1vDZCgg" ref="#_vByjkNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vA06X9VoEeq6RL_1vDZCgg" name="Client_Adresse">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vA06YNVoEeq6RL_1vDZCgg" value="Client_Adresse"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vA06YdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vA-rQNVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vA-rQdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vA-rQtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vA-rQ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vA-rRNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vA-rRdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vA-rRtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBCVoNVoEeq6RL_1vDZCgg" name="Id" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBCVodVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBCVotVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vBCVo9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBCVpNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBCVpdVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBCVptVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBCVp9VoEeq6RL_1vDZCgg" name="Rue" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBCVqNVoEeq6RL_1vDZCgg" value="Rue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBCVqdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBCVqtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBCVq9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBCVrNVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBCVrdVoEeq6RL_1vDZCgg" name="Complement" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBCVrtVoEeq6RL_1vDZCgg" value="Complement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBCVr9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBCVsNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBCVsdVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBCVstVoEeq6RL_1vDZCgg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBCVs9VoEeq6RL_1vDZCgg" name="CodePostale" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBCVtNVoEeq6RL_1vDZCgg" value="CodePostale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBCVtdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBCVttVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBCVt9VoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBCVuNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBCVudVoEeq6RL_1vDZCgg" name="BureauDistributeur" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBCVutVoEeq6RL_1vDZCgg" value="BureauDistributeur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBCVu9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBCVvNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBCVvdVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBCVvtVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBMGoNVoEeq6RL_1vDZCgg" name="CodeDepartement" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBMGodVoEeq6RL_1vDZCgg" value="CodeDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBMGotVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBMGo9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBMGpNVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBMGpdVoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBMGptVoEeq6RL_1vDZCgg" name="CodeCommune" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBMGp9VoEeq6RL_1vDZCgg" value="CodeCommune"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBMGqNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBMGqdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBMGqtVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBMGq9VoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBMGrNVoEeq6RL_1vDZCgg" name="Ville" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBMGrdVoEeq6RL_1vDZCgg" value="Ville"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBMGrtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBMGr9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBMGsNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBMGsdVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBMGstVoEeq6RL_1vDZCgg" name="IdPays" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBMGs9VoEeq6RL_1vDZCgg" value="IdPays"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBMGtNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vBMGtdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBMGttVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBMGt9VoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBMGuNVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBMGudVoEeq6RL_1vDZCgg" name="TypeAdresse" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBMGutVoEeq6RL_1vDZCgg" value="TypeAdresse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBMGu9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vBMGvNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBMGvdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBMGvtVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBMGv9VoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBV3oNVoEeq6RL_1vDZCgg" name="CNT4" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBV3odVoEeq6RL_1vDZCgg" value="CNT4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBV3otVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBV3o9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBV3pNVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBV3pdVoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBV3ptVoEeq6RL_1vDZCgg" name="IRIS" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBV3p9VoEeq6RL_1vDZCgg" value="IRIS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBV3qNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBV3qdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBV3qtVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBV3q9VoEeq6RL_1vDZCgg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBV3rNVoEeq6RL_1vDZCgg" name="TypeZone" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBV3rdVoEeq6RL_1vDZCgg" value="TypeZone"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBV3rtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vBV3r9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBV3sNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBV3sdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBV3stVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vBV3s9VoEeq6RL_1vDZCgg" name="EnvTouristique" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_vBV3tNVoEeq6RL_1vDZCgg" value="EnvTouristique"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vBV3tdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vBV3ttVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vBV3t9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vBV3uNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vBV3udVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vBV3utVoEeq6RL_1vDZCgg" name="PK_Adresse">
        <node defType="com.stambia.rdbms.colref" id="_vBV3u9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vBV3vNVoEeq6RL_1vDZCgg" ref="#_vBCVoNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPwuINVoEeq6RL_1vDZCgg" name="FK_Client_Adresse_Client">
        <node defType="com.stambia.rdbms.relation" id="_vPwuIdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPwuItVoEeq6RL_1vDZCgg" ref="#_vA-rQNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPxVMNVoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPxVMdVoEeq6RL_1vDZCgg" name="FK_Client_Adresse_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.relation" id="_vPxVMtVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPxVM9VoEeq6RL_1vDZCgg" ref="#_vBV3s9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=EnvTouristique?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPxVNNVoEeq6RL_1vDZCgg" ref="#_Dlr6gF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPxVNdVoEeq6RL_1vDZCgg" name="FK_Adresse_ref_Pays">
        <node defType="com.stambia.rdbms.relation" id="_vPxVNtVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPxVN9VoEeq6RL_1vDZCgg" ref="#_vBMGstVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdPays?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPxVONVoEeq6RL_1vDZCgg" ref="#_DkAfgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPxVOdVoEeq6RL_1vDZCgg" name="FK_Adresse_ref_TypeAdresse">
        <node defType="com.stambia.rdbms.relation" id="_vPxVOtVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPxVO9VoEeq6RL_1vDZCgg" ref="#_vBMGudVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeAdresse?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPxVPNVoEeq6RL_1vDZCgg" ref="#_Dm5bcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPxVPdVoEeq6RL_1vDZCgg" name="FK_Client_Adresse_ref_TypeZone">
        <node defType="com.stambia.rdbms.relation" id="_vPxVPtVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPxVP9VoEeq6RL_1vDZCgg" ref="#_vBV3rNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeZone?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPxVQNVoEeq6RL_1vDZCgg" ref="#_DvMb0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vM8qsdVoEeq6RL_1vDZCgg" name="ref_TypeCompte">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vM9RwNVoEeq6RL_1vDZCgg" value="ref_TypeCompte"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vM9RwdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vM_uANVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vM_uAdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vM_uAtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vM_uA9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vM_uBNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vM_uBdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vM_uBtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vNA8INVoEeq6RL_1vDZCgg" name="LibTypeCompte" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vNA8IdVoEeq6RL_1vDZCgg" value="LibTypeCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vNA8ItVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vNA8I9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vNA8JNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vNA8JdVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vNCxUNVoEeq6RL_1vDZCgg" name="PK_ref_TypeCompte">
        <node defType="com.stambia.rdbms.colref" id="_vNCxUdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vNCxUtVoEeq6RL_1vDZCgg" ref="#_vM_uANVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vOuMUNVoEeq6RL_1vDZCgg" name="ref_PerimetreProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vOuMUdVoEeq6RL_1vDZCgg" value="ref_PerimetreProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vOuMUtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vOwokNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOwokdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOwoktVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOwok9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOwolNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOwoldVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOwoltVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOydwNVoEeq6RL_1vDZCgg" name="LibPerimetreProj" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOydwdVoEeq6RL_1vDZCgg" value="LibPerimetreProj"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOydwtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOydw9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOydxNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOydxdVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOzr4NVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOzr4dVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOzr4tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOzr49VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOzr5NVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOzr5dVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vO7nsNVoEeq6RL_1vDZCgg" name="PK_ref_PerimetreDernierProjet">
        <node defType="com.stambia.rdbms.colref" id="_vO8OwNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vO8OwdVoEeq6RL_1vDZCgg" ref="#_vOwokNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vC9BMdVoEeq6RL_1vDZCgg" name="ref_Canal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vC9BMtVoEeq6RL_1vDZCgg" value="ref_Canal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vC9BM9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vC9BNNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vC9BNdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vC9BNtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vC9BN9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vC9BONVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vC9BOdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vC9BOtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vC9BO9VoEeq6RL_1vDZCgg" name="LibCanal" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vC9BPNVoEeq6RL_1vDZCgg" value="LibCanal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vC9BPdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vC9BPtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vC9BP9VoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vC9BQNVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vC9BQdVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vC9BQtVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vC9BQ9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vC9BRNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vC9BRdVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vC9BRtVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vDGyMNVoEeq6RL_1vDZCgg" name="PK_ref_Canal">
        <node defType="com.stambia.rdbms.colref" id="_vDGyMdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vDGyMtVoEeq6RL_1vDZCgg" ref="#_vC9BNNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vLbnwdVoEeq6RL_1vDZCgg" name="ref_Langue">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vLbnwtVoEeq6RL_1vDZCgg" value="ref_Langue"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vLbnw9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vLerENVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLerEdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLerEtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vLerE9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLerFNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLerFdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLerFtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLf5MNVoEeq6RL_1vDZCgg" name="LibLangue" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLf5MdVoEeq6RL_1vDZCgg" value="LibLangue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLf5MtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLf5M9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLf5NNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLf5NdVoEeq6RL_1vDZCgg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vLggQNVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vLggQdVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vLggQtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vLggQ9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vLggRNVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vLggRdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vLi8gNVoEeq6RL_1vDZCgg" name="PK_Langue">
        <node defType="com.stambia.rdbms.colref" id="_vLi8gdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vLi8gtVoEeq6RL_1vDZCgg" ref="#_vLerENVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vM1V8dVoEeq6RL_1vDZCgg" name="Client_TypeEtabClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vM1V8tVoEeq6RL_1vDZCgg" value="Client_TypeEtabClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vM1V89VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vM3yMNVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vM3yMdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vM3yMtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vM3yM9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vM3yNNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vM3yNdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vM3yNtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vM5AUNVoEeq6RL_1vDZCgg" name="IdClientType" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vM5AUdVoEeq6RL_1vDZCgg" value="IdClientType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vM5AUtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vM5AU9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vM5AVNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vM5AVdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vM5AVtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vM61gNVoEeq6RL_1vDZCgg" name="PK_Client_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_vM61gdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vM61gtVoEeq6RL_1vDZCgg" ref="#_vM3yMNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vM61g9VoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vM61hNVoEeq6RL_1vDZCgg" ref="#_vM5AUNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClientType?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQ5WkNVoEeq6RL_1vDZCgg" name="FK_Client_TypeEtabClient_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQ5WkdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQ5WktVoEeq6RL_1vDZCgg" ref="#_vM3yMNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQ5Wk9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQ5WlNVoEeq6RL_1vDZCgg" name="FK_Client_TypeEtabClient_ref_TypeEtabClient1">
        <node defType="com.stambia.rdbms.relation" id="_vQ5WldVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQ5WltVoEeq6RL_1vDZCgg" ref="#_vM5AUNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClientType?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQ5Wl9VoEeq6RL_1vDZCgg" ref="#_DsdQ4F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vGkWBNVoEeq6RL_1vDZCgg" name="Client_InfosCciales">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vGkWBdVoEeq6RL_1vDZCgg" value="Client_InfosCciales"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vGkWBtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vGyYUNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGyYUdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGyYUtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGyYU9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGyYVNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGyYVdVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGyYVtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGyYV9VoEeq6RL_1vDZCgg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGyYWNVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGyYWdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGyYWtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGyYW9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGyYXNVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGyYXdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGyYXtVoEeq6RL_1vDZCgg" name="DateDebAffluence" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGyYX9VoEeq6RL_1vDZCgg" value="DateDebAffluence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGyYYNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGyYYdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGyYYtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGyYY9VoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGyYZNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGyYZdVoEeq6RL_1vDZCgg" name="DateFinAffluence" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGyYZtVoEeq6RL_1vDZCgg" value="DateFinAffluence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGyYZ9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGyYaNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGyYadVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGyYatVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGyYa9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGyYbNVoEeq6RL_1vDZCgg" name="StatutCOLL" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGyYbdVoEeq6RL_1vDZCgg" value="StatutCOLL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGyYbtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGyYb9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGyYcNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGyYcdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGyYctVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGyYc9VoEeq6RL_1vDZCgg" name="NiveauStanding" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGyYdNVoEeq6RL_1vDZCgg" value="NiveauStanding"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGyYddVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGyYdtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGyYd9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGyYeNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGyYedVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vG7iQNVoEeq6RL_1vDZCgg" name="OuvertTouteAnnee" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vG7iQdVoEeq6RL_1vDZCgg" value="OuvertTouteAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vG7iQtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vG7iQ9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vG7iRNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vG7iRdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vG7iRtVoEeq6RL_1vDZCgg" name="DigitalFriendly" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vG7iR9VoEeq6RL_1vDZCgg" value="DigitalFriendly"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vG7iSNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vG7iSdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vG7iStVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vG7iS9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vG7iTNVoEeq6RL_1vDZCgg" name="NbLits" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_vG7iTdVoEeq6RL_1vDZCgg" value="NbLits"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vG7iTtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vG7iT9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vG7iUNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vG7iUdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vG7iUtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vG7iU9VoEeq6RL_1vDZCgg" name="NbRepasJour" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_vG7iVNVoEeq6RL_1vDZCgg" value="NbRepasJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vG7iVdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vG7iVtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vG7iV9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vG7iWNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vG7iWdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vG7iWtVoEeq6RL_1vDZCgg" name="NbCouvJour" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_vG7iW9VoEeq6RL_1vDZCgg" value="NbCouvJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vG7iXNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vG7iXdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vG7iXtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vG7iX9VoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vG7iYNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vG7iYdVoEeq6RL_1vDZCgg" name="NbChambres" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_vG7iYtVoEeq6RL_1vDZCgg" value="NbChambres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vG7iY9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vG7iZNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vG7iZdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vG7iZtVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vG7iZ9VoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vG7iaNVoEeq6RL_1vDZCgg" name="NbEleves" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_vG7iadVoEeq6RL_1vDZCgg" value="NbEleves"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vG7iatVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vG7ia9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vG7ibNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vG7ibdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vG7ibtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHFTQNVoEeq6RL_1vDZCgg" name="NbEmployes" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHFTQdVoEeq6RL_1vDZCgg" value="NbEmployes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHFTQtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHFTQ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHFTRNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHFTRdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHFTRtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHFTR9VoEeq6RL_1vDZCgg" name="TrancheCA" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHFTSNVoEeq6RL_1vDZCgg" value="TrancheCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHFTSdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHFTStVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHFTS9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHFTTNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHFTTdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHFTTtVoEeq6RL_1vDZCgg" name="TicketMoy" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHFTT9VoEeq6RL_1vDZCgg" value="TicketMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHFTUNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHFTUdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHFTUtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHFTU9VoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHFTVNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHFTVdVoEeq6RL_1vDZCgg" name="PrxMoyNuit" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHFTVtVoEeq6RL_1vDZCgg" value="PrxMoyNuit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHFTV9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHFTWNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHFTWdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHFTWtVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHFTW9VoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHFTXNVoEeq6RL_1vDZCgg" name="PotentielCArecur" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHFTXdVoEeq6RL_1vDZCgg" value="PotentielCArecur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHFTXtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHFTX9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHFTYNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHFTYdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHFTYtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHFTY9VoEeq6RL_1vDZCgg" name="PotentielCAInvest" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHFTZNVoEeq6RL_1vDZCgg" value="PotentielCAInvest"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHFTZdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHFTZtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHFTZ9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHFTaNVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHFTadVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHFTatVoEeq6RL_1vDZCgg" name="ScoringCA" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHFTa9VoEeq6RL_1vDZCgg" value="ScoringCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHFTbNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHFTbdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHFTbtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHFTb9VoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHFTcNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHFTcdVoEeq6RL_1vDZCgg" name="FamilleFermes" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHFTctVoEeq6RL_1vDZCgg" value="FamilleFermes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHFTc9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHFTdNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHFTddVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHFTdtVoEeq6RL_1vDZCgg" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHOdMNVoEeq6RL_1vDZCgg" name="CondConcession" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHOdMdVoEeq6RL_1vDZCgg" value="CondConcession"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHOdMtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHOdM9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHOdNNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHOdNdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHOdNtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHOdN9VoEeq6RL_1vDZCgg" name="ScoringRelation" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHOdONVoEeq6RL_1vDZCgg" value="ScoringRelation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHOdOdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHOdOtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHOdO9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHOdPNVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHOdPdVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHOdPtVoEeq6RL_1vDZCgg" name="Fidelite" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHOdP9VoEeq6RL_1vDZCgg" value="Fidelite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHOdQNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHOdQdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHOdQtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHOdQ9VoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHOdRNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHOdRdVoEeq6RL_1vDZCgg" name="Comportement" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHOdRtVoEeq6RL_1vDZCgg" value="Comportement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHOdR9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHOdSNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHOdSdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHOdStVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHOdS9VoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHOdTNVoEeq6RL_1vDZCgg" name="EcheanceProjet" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHOdTdVoEeq6RL_1vDZCgg" value="EcheanceProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHOdTtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHOdT9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHOdUNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHOdUdVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHOdUtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHOdU9VoEeq6RL_1vDZCgg" name="DernierProjet" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHOdVNVoEeq6RL_1vDZCgg" value="DernierProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHOdVdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHOdVtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHOdV9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHOdWNVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHOdWdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHOdWtVoEeq6RL_1vDZCgg" name="CmdFinAnnee" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHOdW9VoEeq6RL_1vDZCgg" value="CmdFinAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHOdXNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHOdXdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHOdXtVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHOdX9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHOdYNVoEeq6RL_1vDZCgg" name="PrchCmdFinAnnee" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHOdYdVoEeq6RL_1vDZCgg" value="PrchCmdFinAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHOdYtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHOdY9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHOdZNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHOdZdVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHOdZtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHYOMNVoEeq6RL_1vDZCgg" name="CompteChomette" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHYOMdVoEeq6RL_1vDZCgg" value="CompteChomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHYOMtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHYOM9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHYONNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHYONdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHYONtVoEeq6RL_1vDZCgg" name="InvestCetteAnnee" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHYON9VoEeq6RL_1vDZCgg" value="InvestCetteAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHYOONVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHYOOdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHYOOtVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHYOO9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHYOPNVoEeq6RL_1vDZCgg" name="CcurentRegion" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHYOPdVoEeq6RL_1vDZCgg" value="CcurentRegion"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHYOPtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHYOP9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHYOQNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHYOQdVoEeq6RL_1vDZCgg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHYOQtVoEeq6RL_1vDZCgg" name="DteCreation" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHYOQ9VoEeq6RL_1vDZCgg" value="DteCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHYORNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHYORdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHYORtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHYOR9VoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHYOSNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHYOSdVoEeq6RL_1vDZCgg" name="DteMAJCcial" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHYOStVoEeq6RL_1vDZCgg" value="DteMAJCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHYOS9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHYOTNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHYOTdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHYOTtVoEeq6RL_1vDZCgg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHYOT9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHYOUNVoEeq6RL_1vDZCgg" name="AnneeExistence" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHYOUdVoEeq6RL_1vDZCgg" value="AnneeExistence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHYOUtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHYOU9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHYOVNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHYOVdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHYOVtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHYOV9VoEeq6RL_1vDZCgg" name="Remise" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHYOWNVoEeq6RL_1vDZCgg" value="Remise"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHYOWdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHYOWtVoEeq6RL_1vDZCgg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHYOW9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHYOXNVoEeq6RL_1vDZCgg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHYOXdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHYOXtVoEeq6RL_1vDZCgg" name="ColTarifaire" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHYOX9VoEeq6RL_1vDZCgg" value="ColTarifaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHYOYNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHYOYdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHYOYtVoEeq6RL_1vDZCgg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHYOY9VoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHYOZNVoEeq6RL_1vDZCgg" name="RepasServis" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHYOZdVoEeq6RL_1vDZCgg" value="RepasServis"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHYOZtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHYOZ9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHYOaNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHYOadVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHh_MNVoEeq6RL_1vDZCgg" name="Prestations" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHh_MdVoEeq6RL_1vDZCgg" value="Prestations"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHh_MtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHh_M9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHh_NNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHh_NdVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vHh_NtVoEeq6RL_1vDZCgg" name="PK_Client_InfosCciales">
        <node defType="com.stambia.rdbms.colref" id="_vHh_N9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vHh_ONVoEeq6RL_1vDZCgg" ref="#_vGyYUNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXLENVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQXLEdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXLEtVoEeq6RL_1vDZCgg" ref="#_vGyYV9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXLE9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXLFNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_AnneesExistence">
        <node defType="com.stambia.rdbms.relation" id="_vQXLFdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXLFtVoEeq6RL_1vDZCgg" ref="#_vHYOUNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=AnneeExistence?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXLF9VoEeq6RL_1vDZCgg" ref="#_DvXa8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXLGNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_ComportementMultiCanal">
        <node defType="com.stambia.rdbms.relation" id="_vQXLGdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXLGtVoEeq6RL_1vDZCgg" ref="#_vHOdRdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Comportement?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXLG9VoEeq6RL_1vDZCgg" ref="#_DxKKsF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXLHNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_NbChambres">
        <node defType="com.stambia.rdbms.relation" id="_vQXLHdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXLHtVoEeq6RL_1vDZCgg" ref="#_vG7iYdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbChambres?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXLH9VoEeq6RL_1vDZCgg" ref="#_DqFSUF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXyINVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_NbCouvertJour">
        <node defType="com.stambia.rdbms.relation" id="_vQXyIdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXyItVoEeq6RL_1vDZCgg" ref="#_vG7iWtVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbCouvJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXyI9VoEeq6RL_1vDZCgg" ref="#_Dvv1cF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXyJNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_NbEleves">
        <node defType="com.stambia.rdbms.relation" id="_vQXyJdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXyJtVoEeq6RL_1vDZCgg" ref="#_vG7iaNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbEleves?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXyJ9VoEeq6RL_1vDZCgg" ref="#_Dy6eMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXyKNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_NbEmployes">
        <node defType="com.stambia.rdbms.relation" id="_vQXyKdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXyKtVoEeq6RL_1vDZCgg" ref="#_vHFTQNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbEmployes?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXyK9VoEeq6RL_1vDZCgg" ref="#_DyZg0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXyLNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_NbLits">
        <node defType="com.stambia.rdbms.relation" id="_vQXyLdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXyLtVoEeq6RL_1vDZCgg" ref="#_vG7iTNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbLits?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXyL9VoEeq6RL_1vDZCgg" ref="#_DmCf0F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXyMNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_NbRepasJour">
        <node defType="com.stambia.rdbms.relation" id="_vQXyMdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXyMtVoEeq6RL_1vDZCgg" ref="#_vG7iU9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NbRepasJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXyM9VoEeq6RL_1vDZCgg" ref="#_DoA1wF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXyNNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_NiveauFidelite">
        <node defType="com.stambia.rdbms.relation" id="_vQXyNdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXyNtVoEeq6RL_1vDZCgg" ref="#_vHOdPtVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Fidelite?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXyN9VoEeq6RL_1vDZCgg" ref="#_DfkEYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQXyONVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_NiveauStanding">
        <node defType="com.stambia.rdbms.relation" id="_vQXyOdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQXyOtVoEeq6RL_1vDZCgg" ref="#_vGyYc9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=NiveauStanding?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQXyO9VoEeq6RL_1vDZCgg" ref="#_DpVEYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQYZMNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_ScoringCA">
        <node defType="com.stambia.rdbms.relation" id="_vQYZMdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQYZMtVoEeq6RL_1vDZCgg" ref="#_vHFTatVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=ScoringCA?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQYZM9VoEeq6RL_1vDZCgg" ref="#_Ds9AIF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQYZNNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_StatutCOLL">
        <node defType="com.stambia.rdbms.relation" id="_vQYZNdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQYZNtVoEeq6RL_1vDZCgg" ref="#_vGyYbNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=StatutCOLL?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQYZN9VoEeq6RL_1vDZCgg" ref="#_DgaY8F-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQYZONVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_TicketRepasMoyen">
        <node defType="com.stambia.rdbms.relation" id="_vQYZOdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQYZOtVoEeq6RL_1vDZCgg" ref="#_vHFTTtVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TicketMoy?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQYZO9VoEeq6RL_1vDZCgg" ref="#_DnzaYF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQYZPNVoEeq6RL_1vDZCgg" name="FK_Client_InfosCciales_ref_TrancheCA">
        <node defType="com.stambia.rdbms.relation" id="_vQYZPdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQYZPtVoEeq6RL_1vDZCgg" ref="#_vHFTR9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TrancheCA?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQYZP9VoEeq6RL_1vDZCgg" ref="#_DyluEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u7dSG9VoEeq6RL_1vDZCgg" name="Client_PrestationOfferte">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u7dSHNVoEeq6RL_1vDZCgg" value="Client_PrestationOfferte"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u7dSHdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u7dSHtVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u7dSH9VoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u7dSINVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u7dSIdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u7dSItVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u7dSI9VoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u7dSJNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u7dSJdVoEeq6RL_1vDZCgg" name="IdPrestation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u7dSJtVoEeq6RL_1vDZCgg" value="IdPrestation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u7dSJ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u7dSKNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u7dSKdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u7dSKtVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u7dSK9VoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u7nDENVoEeq6RL_1vDZCgg" name="PK_Client_PrestationOfferte">
        <node defType="com.stambia.rdbms.colref" id="_u7nDEdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u7nDEtVoEeq6RL_1vDZCgg" ref="#_u7dSHtVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_u7nDE9VoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u7nDFNVoEeq6RL_1vDZCgg" ref="#_u7dSJdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdPrestation?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPZhwNVoEeq6RL_1vDZCgg" name="FK_Client_PrestationOfferte_Client">
        <node defType="com.stambia.rdbms.relation" id="_vPaI0NVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPaI0dVoEeq6RL_1vDZCgg" ref="#_u7dSHtVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPaI0tVoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPaI09VoEeq6RL_1vDZCgg" name="FK_Client_PrestationOfferte_ref_PrestationsOffertes1">
        <node defType="com.stambia.rdbms.relation" id="_vPaI1NVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPaI1dVoEeq6RL_1vDZCgg" ref="#_u7dSJdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdPrestation?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPaI1tVoEeq6RL_1vDZCgg" ref="#_DpdnQF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vIMGl9VoEeq6RL_1vDZCgg" name="Client_PerimetreDernierProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vIMGmNVoEeq6RL_1vDZCgg" value="Client_PerimetreDernierProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vIMGmdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vIV3gNVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIV3gdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIV3gtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIV3g9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIV3hNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIV3hdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIV3htVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIV3h9VoEeq6RL_1vDZCgg" name="IdUnivers" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIV3iNVoEeq6RL_1vDZCgg" value="IdUnivers"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIV3idVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIV3itVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIV3i9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIV3jNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIV3jdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vIV3jtVoEeq6RL_1vDZCgg" name="PK_Client_PerimetreDernierProjet">
        <node defType="com.stambia.rdbms.colref" id="_vIV3j9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vIV3kNVoEeq6RL_1vDZCgg" ref="#_vIV3gNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vIV3kdVoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vIV3ktVoEeq6RL_1vDZCgg" ref="#_vIV3h9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUnivers?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQfG4NVoEeq6RL_1vDZCgg" name="FK_Client_PerimetreDernierProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQfG4dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQfG4tVoEeq6RL_1vDZCgg" ref="#_vIV3gNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQfG49VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQfG5NVoEeq6RL_1vDZCgg" name="FK_Client_PerimetreDernierProjet_ref_UniversConso">
        <node defType="com.stambia.rdbms.relation" id="_vQfG5dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQfG5tVoEeq6RL_1vDZCgg" ref="#_vIV3h9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdUnivers?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQfG59VoEeq6RL_1vDZCgg" ref="#_DfXQEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u7ThEdVoEeq6RL_1vDZCgg" name="ref_TypeMenu">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u7ThEtVoEeq6RL_1vDZCgg" value="ref_TypeMenu"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u7ThE9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u7ThFNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u7ThFdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u7ThFtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u7ThF9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u7ThGNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u7ThGdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u7ThGtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u7ThG9VoEeq6RL_1vDZCgg" name="LibTypeMenu" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u7ThHNVoEeq6RL_1vDZCgg" value="LibTypeMenu"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u7ThHdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u7ThHtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u7ThH9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u7ThINVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u7dSENVoEeq6RL_1vDZCgg" name="IdSpecCulinaire" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u7dSEdVoEeq6RL_1vDZCgg" value="IdSpecCulinaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u7dSEtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u7dSE9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u7dSFNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u7dSFdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u7dSFtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u7dSF9VoEeq6RL_1vDZCgg" name="PK_ref_TypeMenu">
        <node defType="com.stambia.rdbms.colref" id="_u7dSGNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u7dSGdVoEeq6RL_1vDZCgg" ref="#_u7ThFNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vIV3lNVoEeq6RL_1vDZCgg" name="ref_CHD_TypeEtab">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vIV3ldVoEeq6RL_1vDZCgg" value="ref_CHD_TypeEtab"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vIV3ltVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vIfBcNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIfBcdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIfBctVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIfBc9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIfBdNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIfBddVoEeq6RL_1vDZCgg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIfBdtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIfBd9VoEeq6RL_1vDZCgg" name="GFCCode" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIfBeNVoEeq6RL_1vDZCgg" value="GFCCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIfBedVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIfBetVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIfBe9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIfBfNVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIfBfdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIfBftVoEeq6RL_1vDZCgg" name="TypeEtabN1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIfBf9VoEeq6RL_1vDZCgg" value="TypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIfBgNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIfBgdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIfBgtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIfBg9VoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIfBhNVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIfBhdVoEeq6RL_1vDZCgg" name="TypeEtabN2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIfBhtVoEeq6RL_1vDZCgg" value="TypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIfBh9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIfBiNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIfBidVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIfBitVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIfBi9VoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIfBjNVoEeq6RL_1vDZCgg" name="TypeEtabN3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIfBjdVoEeq6RL_1vDZCgg" value="TypeEtabN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIfBjtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vIfBj9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIfBkNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIfBkdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIfBktVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIfBk9VoEeq6RL_1vDZCgg" name="CodeNature" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIfBlNVoEeq6RL_1vDZCgg" value="CodeNature"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIfBldVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIfBltVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIfBl9VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIfBmNVoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vIfBmdVoEeq6RL_1vDZCgg" name="CodeTypo" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vIfBmtVoEeq6RL_1vDZCgg" value="CodeTypo"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vIfBm9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vIfBnNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vIfBndVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vIfBntVoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vIfBn9VoEeq6RL_1vDZCgg" name="PK_ref_CHD_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_vIfBoNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vIfBodVoEeq6RL_1vDZCgg" ref="#_vIfBcNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQgVANVoEeq6RL_1vDZCgg" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_vQgVAdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQgVAtVoEeq6RL_1vDZCgg" ref="#_vIfBftVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtabN1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQgVA9VoEeq6RL_1vDZCgg" ref="#_Duq3YF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQgVBNVoEeq6RL_1vDZCgg" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.relation" id="_vQgVBdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQgVBtVoEeq6RL_1vDZCgg" ref="#_vIfBhdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtabN2?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQgVB9VoEeq6RL_1vDZCgg" ref="#_DuiUgF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQgVCNVoEeq6RL_1vDZCgg" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.relation" id="_vQgVCdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQgVCtVoEeq6RL_1vDZCgg" ref="#_vIfBjNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=TypeEtabN3?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQgVC9VoEeq6RL_1vDZCgg" ref="#_DuROwF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vJpfJdVoEeq6RL_1vDZCgg" name="ref_TypeEtabNiv3">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vJpfJtVoEeq6RL_1vDZCgg" value="ref_TypeEtabNiv3"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vJpfJ9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vJ8aANVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJ8aAdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJ8aAtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vJ8aA9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJ8aBNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJ8aBdVoEeq6RL_1vDZCgg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vJ8aBtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vKGLANVoEeq6RL_1vDZCgg" name="LibTypeEtabN3" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKGLAdVoEeq6RL_1vDZCgg" value="LibTypeEtabN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKGLAtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKGLA9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKGLBNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKGLBdVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vKHZINVoEeq6RL_1vDZCgg" name="IdTypeEtabN2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKHZIdVoEeq6RL_1vDZCgg" value="IdTypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKHZItVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vKHZI9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKHZJNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKHZJdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKHZJtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vKKccNVoEeq6RL_1vDZCgg" name="PK_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.colref" id="_vKKccdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vKKcctVoEeq6RL_1vDZCgg" ref="#_vJ8aANVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vGbL8dVoEeq6RL_1vDZCgg" name="Wrk_ChometteUsers">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vGbL8tVoEeq6RL_1vDZCgg" value="Wrk_ChometteUsers"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vGbL89VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vGbL9NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGbL9dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGbL9tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGbL99VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGbL-NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGbL-dVoEeq6RL_1vDZCgg" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGbL-tVoEeq6RL_1vDZCgg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGbL-9VoEeq6RL_1vDZCgg" name="CodeMagento" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGbL_NVoEeq6RL_1vDZCgg" value="CodeMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGbL_dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGbL_tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGbL_9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGbMANVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGbMAdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGbMAtVoEeq6RL_1vDZCgg" name="CodeMagentoUser" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGbMA9VoEeq6RL_1vDZCgg" value="CodeMagentoUser"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGbMBNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGbMBdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGbMBtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGbMB9VoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGbMCNVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGkV4NVoEeq6RL_1vDZCgg" name="CodeClient" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGkV4dVoEeq6RL_1vDZCgg" value="CodeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGkV4tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGkV49VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGkV5NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGkV5dVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGkV5tVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGkV59VoEeq6RL_1vDZCgg" name="FileName" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGkV6NVoEeq6RL_1vDZCgg" value="FileName"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGkV6dVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGkV6tVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGkV69VoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGkV7NVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGkV7dVoEeq6RL_1vDZCgg" name="dateCreation" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGkV7tVoEeq6RL_1vDZCgg" value="dateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGkV79VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vGkV8NVoEeq6RL_1vDZCgg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGkV8dVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGkV8tVoEeq6RL_1vDZCgg" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGkV89VoEeq6RL_1vDZCgg" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGkV9NVoEeq6RL_1vDZCgg" name="Statut" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGkV9dVoEeq6RL_1vDZCgg" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGkV9tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGkV99VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGkV-NVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGkV-dVoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vGkV-tVoEeq6RL_1vDZCgg" name="Mode" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vGkV-9VoEeq6RL_1vDZCgg" value="Mode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vGkV_NVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vGkV_dVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vGkV_tVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vGkV_9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vGkWANVoEeq6RL_1vDZCgg" name="PK_Wrk_ChometteUsers">
        <node defType="com.stambia.rdbms.colref" id="_vGkWAdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vGkWAtVoEeq6RL_1vDZCgg" ref="#_vGbL9NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vABpAdVoEeq6RL_1vDZCgg" name="Client_HoraireOuv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vABpAtVoEeq6RL_1vDZCgg" value="Client_HoraireOuv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vABpA9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vAFTYNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAFTYdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAFTYtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAFTY9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAFTZNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAFTZdVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAFTZtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAFTZ9VoEeq6RL_1vDZCgg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAFTaNVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAFTadVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAFTatVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAFTa9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAFTbNVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAFTbdVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAFTbtVoEeq6RL_1vDZCgg" name="H_OuvertureLundi" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAFTb9VoEeq6RL_1vDZCgg" value="H_OuvertureLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAFTcNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAFTcdVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAFTctVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAFTc9VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAFTdNVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAFTddVoEeq6RL_1vDZCgg" name="H_FermeLundi" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAFTdtVoEeq6RL_1vDZCgg" value="H_FermeLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAFTd9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAFTeNVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAFTedVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAFTetVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAFTe9VoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAFTfNVoEeq6RL_1vDZCgg" name="H_OuvertureLundi2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAFTfdVoEeq6RL_1vDZCgg" value="H_OuvertureLundi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAFTftVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAFTf9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAFTgNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAFTgdVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAFTgtVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAFTg9VoEeq6RL_1vDZCgg" name="H_FermeLundi2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAFThNVoEeq6RL_1vDZCgg" value="H_FermeLundi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAFThdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAFThtVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAFTh9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAFTiNVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAFTidVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAFTitVoEeq6RL_1vDZCgg" name="H_OuvertureMardi" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAFTi9VoEeq6RL_1vDZCgg" value="H_OuvertureMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAFTjNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAFTjdVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAFTjtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAFTj9VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAFTkNVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAFTkdVoEeq6RL_1vDZCgg" name="H_FermeMardi" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAFTktVoEeq6RL_1vDZCgg" value="H_FermeMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAFTk9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAFTlNVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAFTldVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAFTltVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAFTl9VoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAPEYNVoEeq6RL_1vDZCgg" name="H_OuvertureMardi2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAPEYdVoEeq6RL_1vDZCgg" value="H_OuvertureMardi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAPEYtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAPEY9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAPEZNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAPEZdVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAPEZtVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAPEZ9VoEeq6RL_1vDZCgg" name="H_FermeMardi2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAPEaNVoEeq6RL_1vDZCgg" value="H_FermeMardi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAPEadVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAPEatVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAPEa9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAPEbNVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAPEbdVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAPEbtVoEeq6RL_1vDZCgg" name="H_OuvertureMercredi" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAPEb9VoEeq6RL_1vDZCgg" value="H_OuvertureMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAPEcNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAPEcdVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAPEctVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAPEc9VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAPEdNVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAPEddVoEeq6RL_1vDZCgg" name="H_FermeMercredi" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAPEdtVoEeq6RL_1vDZCgg" value="H_FermeMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAPEd9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAPEeNVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAPEedVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAPEetVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAPEe9VoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAPEfNVoEeq6RL_1vDZCgg" name="H_OuvertureMercredi2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAPEfdVoEeq6RL_1vDZCgg" value="H_OuvertureMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAPEftVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAPEf9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAPEgNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAPEgdVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAPEgtVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAPEg9VoEeq6RL_1vDZCgg" name="H_FermeMercredi2" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAPEhNVoEeq6RL_1vDZCgg" value="H_FermeMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAPEhdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAPEhtVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAPEh9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAPEiNVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAPEidVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAPEitVoEeq6RL_1vDZCgg" name="H_OuvertureJeudi" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAPEi9VoEeq6RL_1vDZCgg" value="H_OuvertureJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAPEjNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAPEjdVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAPEjtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAPEj9VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAPEkNVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAPEkdVoEeq6RL_1vDZCgg" name="H_FermeJeudi" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAPEktVoEeq6RL_1vDZCgg" value="H_FermeJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAPEk9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAPElNVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAPEldVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAPEltVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAPEl9VoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAPEmNVoEeq6RL_1vDZCgg" name="H_OuvertureJeudi2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAPEmdVoEeq6RL_1vDZCgg" value="H_OuvertureJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAPEmtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAPEm9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAPEnNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAPEndVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAPEntVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1YNVoEeq6RL_1vDZCgg" name="H_FermeJeudi2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1YdVoEeq6RL_1vDZCgg" value="H_FermeJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1YtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1Y9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1ZNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1ZdVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1ZtVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1Z9VoEeq6RL_1vDZCgg" name="H_OuvertureVendredi" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1aNVoEeq6RL_1vDZCgg" value="H_OuvertureVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1adVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1atVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1a9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1bNVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1bdVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1btVoEeq6RL_1vDZCgg" name="H_FermeVendredi" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1b9VoEeq6RL_1vDZCgg" value="H_FermeVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1cNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1cdVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1ctVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1c9VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1dNVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1ddVoEeq6RL_1vDZCgg" name="H_OuvertureVendredi2" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1dtVoEeq6RL_1vDZCgg" value="H_OuvertureVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1d9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1eNVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1edVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1etVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1e9VoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1fNVoEeq6RL_1vDZCgg" name="H_FermeVendredi2" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1fdVoEeq6RL_1vDZCgg" value="H_FermeVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1ftVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1f9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1gNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1gdVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1gtVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1g9VoEeq6RL_1vDZCgg" name="H_OuvertureSamedi" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1hNVoEeq6RL_1vDZCgg" value="H_OuvertureSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1hdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1htVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1h9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1iNVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1idVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1itVoEeq6RL_1vDZCgg" name="H_FermeSamedi" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1i9VoEeq6RL_1vDZCgg" value="H_FermeSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1jNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1jdVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1jtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1j9VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1kNVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1kdVoEeq6RL_1vDZCgg" name="H_OuvertureSamedi2" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1ktVoEeq6RL_1vDZCgg" value="H_OuvertureSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1k9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1lNVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1ldVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1ltVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1l9VoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1mNVoEeq6RL_1vDZCgg" name="H_FermeSamedi2" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1mdVoEeq6RL_1vDZCgg" value="H_FermeSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1mtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1m9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1nNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1ndVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1ntVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1n9VoEeq6RL_1vDZCgg" name="H_OuvertureDimanche" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1oNVoEeq6RL_1vDZCgg" value="H_OuvertureDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1odVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1otVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1o9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1pNVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1pdVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAY1ptVoEeq6RL_1vDZCgg" name="H_FermeDimanche" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAY1p9VoEeq6RL_1vDZCgg" value="H_FermeDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAY1qNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAY1qdVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAY1qtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAY1q9VoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAY1rNVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAh_UNVoEeq6RL_1vDZCgg" name="H_OuvertureDimanche2" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAh_UdVoEeq6RL_1vDZCgg" value="H_OuvertureDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAh_UtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAh_U9VoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAh_VNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAh_VdVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAh_VtVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vAh_V9VoEeq6RL_1vDZCgg" name="H_FermeDimanche2" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_vAh_WNVoEeq6RL_1vDZCgg" value="H_FermeDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vAh_WdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vAh_WtVoEeq6RL_1vDZCgg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vAh_W9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vAh_XNVoEeq6RL_1vDZCgg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vAh_XdVoEeq6RL_1vDZCgg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vAh_XtVoEeq6RL_1vDZCgg" name="PK_Horaire_Ouverture">
        <node defType="com.stambia.rdbms.colref" id="_vAh_X9VoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vAh_YNVoEeq6RL_1vDZCgg" ref="#_vAFTZ9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPscsNVoEeq6RL_1vDZCgg" name="FK_Client_HoraireOuv_Client">
        <node defType="com.stambia.rdbms.relation" id="_vPscsdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPscstVoEeq6RL_1vDZCgg" ref="#_vAFTZ9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPscs9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u8t2YNVoEeq6RL_1vDZCgg" name="ref_Departement">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u8t2YdVoEeq6RL_1vDZCgg" value="ref_Departement"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u8t2YtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u83AQNVoEeq6RL_1vDZCgg" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u83AQdVoEeq6RL_1vDZCgg" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u83AQtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u83AQ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u83ARNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u83ARdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u83ARtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u83AR9VoEeq6RL_1vDZCgg" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u83ASNVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u83ASdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u83AStVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u83AS9VoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u83ATNVoEeq6RL_1vDZCgg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u83ATdVoEeq6RL_1vDZCgg" name="LibDepartement" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u83ATtVoEeq6RL_1vDZCgg" value="LibDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u83AT9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u83AUNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u83AUdVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u83AUtVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u83AU9VoEeq6RL_1vDZCgg" name="PK_ref_Departement">
        <node defType="com.stambia.rdbms.colref" id="_u83AVNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u83AVdVoEeq6RL_1vDZCgg" ref="#_u83AQNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vHvakdVoEeq6RL_1vDZCgg" name="Client_ActualiteProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vHvaktVoEeq6RL_1vDZCgg" value="Client_ActualiteProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vHvak9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vHvalNVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHvaldVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHvaltVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHval9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHvamNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHvamdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHvamtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vHvam9VoEeq6RL_1vDZCgg" name="IdProjet" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vHvanNVoEeq6RL_1vDZCgg" value="IdProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vHvandVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vHvantVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vHvan9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vHvaoNVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vHvaodVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vH4kgNVoEeq6RL_1vDZCgg" name="PK_Client_ActualiteProjet">
        <node defType="com.stambia.rdbms.colref" id="_vH4kgdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vH4kgtVoEeq6RL_1vDZCgg" ref="#_vHvalNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vH4kg9VoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vH4khNVoEeq6RL_1vDZCgg" ref="#_vHvam9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdProjet?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQbcgNVoEeq6RL_1vDZCgg" name="FK_Client_ActualiteProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQbcgdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQbcgtVoEeq6RL_1vDZCgg" ref="#_vHvalNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQbcg9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQbchNVoEeq6RL_1vDZCgg" name="FK_Client_ActualiteProjet_ref_ActualiteProjet1">
        <node defType="com.stambia.rdbms.relation" id="_vQbchdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQbchtVoEeq6RL_1vDZCgg" ref="#_vHvam9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdProjet?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQbch9VoEeq6RL_1vDZCgg" ref="#_DpvUEF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vMKnkdVoEeq6RL_1vDZCgg" name="ref_Segment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vMKnktVoEeq6RL_1vDZCgg" value="ref_Segment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vMKnk9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vMND0NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMND0dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMND0tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vMND09VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMND1NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMND1dVoEeq6RL_1vDZCgg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMND1tVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vMOR8NVoEeq6RL_1vDZCgg" name="LibSegment" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vMOR8dVoEeq6RL_1vDZCgg" value="LibSegment"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vMOR8tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vMOR89VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vMOR9NVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vMOR9dVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vMQHINVoEeq6RL_1vDZCgg" name="PK_ref_Segment">
        <node defType="com.stambia.rdbms.colref" id="_vMQHIdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vMQHItVoEeq6RL_1vDZCgg" ref="#_vMND0NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vH4khtVoEeq6RL_1vDZCgg" name="ref_FormeJuridique">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vH4kh9VoEeq6RL_1vDZCgg" value="ref_FormeJuridique"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vH4kiNVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vH4kidVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vH4kitVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vH4ki9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vH4kjNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vH4kjdVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vH4kjtVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vH4kj9VoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vICVgNVoEeq6RL_1vDZCgg" name="LibFormeJuridique" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vICVgdVoEeq6RL_1vDZCgg" value="LibFormeJuridique"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vICVgtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vICVg9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vICVhNVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vICVhdVoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vICVhtVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vICVh9VoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vICViNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vICVidVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vICVitVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vICVi9VoEeq6RL_1vDZCgg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vICVjNVoEeq6RL_1vDZCgg" name="PK_FormeJuridique">
        <node defType="com.stambia.rdbms.colref" id="_vICVjdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vICVjtVoEeq6RL_1vDZCgg" ref="#_vH4kidVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vL6I4NVoEeq6RL_1vDZCgg" name="Client_Distinction">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vL6I4dVoEeq6RL_1vDZCgg" value="Client_Distinction"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vL6I4tVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vL9MMNVoEeq6RL_1vDZCgg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vL9MMdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vL9MMtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vL9MM9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vL9MNNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vL9MNdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vL9MNtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vL9zQNVoEeq6RL_1vDZCgg" name="IdDistinction" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vL9zQdVoEeq6RL_1vDZCgg" value="IdDistinction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vL9zQtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vL9zQ9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vL9zRNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vL9zRdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vL9zRtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vMAPgNVoEeq6RL_1vDZCgg" name="PK_Client_Distinction">
        <node defType="com.stambia.rdbms.colref" id="_vMAPgdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vMAPgtVoEeq6RL_1vDZCgg" ref="#_vL9MMNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_vMAPg9VoEeq6RL_1vDZCgg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vMAPhNVoEeq6RL_1vDZCgg" ref="#_vL9zQNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdDistinction?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQyo4NVoEeq6RL_1vDZCgg" name="FK_Client_Distinction_Client">
        <node defType="com.stambia.rdbms.relation" id="_vQyo4dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQyo4tVoEeq6RL_1vDZCgg" ref="#_vL9MMNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQyo49VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQyo5NVoEeq6RL_1vDZCgg" name="FK_Client_Distinction_ref_Distinctions1">
        <node defType="com.stambia.rdbms.relation" id="_vQyo5dVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQyo5tVoEeq6RL_1vDZCgg" ref="#_vL9zQNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdDistinction?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQyo59VoEeq6RL_1vDZCgg" ref="#_DzEPMF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u9XWp9VoEeq6RL_1vDZCgg" name="ref_StatutCOLL">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u9XWqNVoEeq6RL_1vDZCgg" value="ref_StatutCOLL"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u9XWqdVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u9hHkNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9hHkdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9hHktVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u9hHk9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9hHlNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9hHldVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9hHltVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9hHl9VoEeq6RL_1vDZCgg" name="LibStatutCOLL" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9hHmNVoEeq6RL_1vDZCgg" value="LibStatutCOLL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9hHmdVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9hHmtVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9hHm9VoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9hHnNVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u9hHndVoEeq6RL_1vDZCgg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u9hHntVoEeq6RL_1vDZCgg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u9hHn9VoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u9hHoNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u9hHodVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u9hHotVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u9hHo9VoEeq6RL_1vDZCgg" name="PK_StatutCOLL">
        <node defType="com.stambia.rdbms.colref" id="_u9hHpNVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u9hHpdVoEeq6RL_1vDZCgg" ref="#_u9hHkNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vOZcMdVoEeq6RL_1vDZCgg" name="ref_NbEmployes">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vOZcMtVoEeq6RL_1vDZCgg" value="ref_NbEmployes"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vOZcM9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vOcfgNVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOcfgdVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOcfgtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOcfg9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOcfhNVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOcfhdVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOcfhtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOdGkNVoEeq6RL_1vDZCgg" name="NbEmployes" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOdGkdVoEeq6RL_1vDZCgg" value="NbEmployes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOdGktVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOdGk9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOdGlNVoEeq6RL_1vDZCgg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOdGldVoEeq6RL_1vDZCgg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOeUsNVoEeq6RL_1vDZCgg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOeUsdVoEeq6RL_1vDZCgg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOeUstVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOeUs9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOeUtNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOeUtdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOeUttVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vOe7wNVoEeq6RL_1vDZCgg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vOe7wdVoEeq6RL_1vDZCgg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vOe7wtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vOe7w9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vOe7xNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vOe7xdVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vOe7xtVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vOh_ENVoEeq6RL_1vDZCgg" name="PK_ref_NbEmployes">
        <node defType="com.stambia.rdbms.colref" id="_vOh_EdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vOh_EtVoEeq6RL_1vDZCgg" ref="#_vOcfgNVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_u8DvBtVoEeq6RL_1vDZCgg" name="Client_Activite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_u8DvB9VoEeq6RL_1vDZCgg" value="Client_Activite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_u8DvCNVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_u8DvCdVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8DvCtVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8DvC9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u8DvDNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8DvDdVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8DvDtVoEeq6RL_1vDZCgg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8DvD9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8DvENVoEeq6RL_1vDZCgg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8DvEdVoEeq6RL_1vDZCgg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8DvEtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_u8DvE9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8DvFNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8DvFdVoEeq6RL_1vDZCgg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8DvFtVoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8M48NVoEeq6RL_1vDZCgg" name="ActifMoisClosN" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8M48dVoEeq6RL_1vDZCgg" value="ActifMoisClosN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8M48tVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8M489VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8M49NVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8M49dVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8RKYNVoEeq6RL_1vDZCgg" name="ActifMoisClosN_1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8RKYdVoEeq6RL_1vDZCgg" value="ActifMoisClosN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8RKYtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8RKY9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8RKZNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8RKZdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8RKZtVoEeq6RL_1vDZCgg" name="Actif12DM" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8RKZ9VoEeq6RL_1vDZCgg" value="Actif12DM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8RKaNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8RKadVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8RKatVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8RKa9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8RKbNVoEeq6RL_1vDZCgg" name="Actif13_24DM" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8RKbdVoEeq6RL_1vDZCgg" value="Actif13_24DM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8RKbtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8RKb9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8RKcNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8RKcdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8RKctVoEeq6RL_1vDZCgg" name="ActifN" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8RKc9VoEeq6RL_1vDZCgg" value="ActifN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8RKdNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8RKddVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8RKdtVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8RKd9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8RKeNVoEeq6RL_1vDZCgg" name="ActifPeriodeN_1" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8RKedVoEeq6RL_1vDZCgg" value="ActifPeriodeN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8RKetVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8RKe9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8RKfNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8RKfdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8RKftVoEeq6RL_1vDZCgg" name="ActifN_1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8RKf9VoEeq6RL_1vDZCgg" value="ActifN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8RKgNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8RKgdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8RKgtVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8RKg9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8aUUNVoEeq6RL_1vDZCgg" name="ActifN_2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8aUUdVoEeq6RL_1vDZCgg" value="ActifN_2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8aUUtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8aUU9VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8aUVNVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8aUVdVoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_u8aUVtVoEeq6RL_1vDZCgg" name="ActifN_3" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_u8aUV9VoEeq6RL_1vDZCgg" value="ActifN_3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_u8aUWNVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_u8aUWdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_u8aUWtVoEeq6RL_1vDZCgg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_u8aUW9VoEeq6RL_1vDZCgg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_u8aUXNVoEeq6RL_1vDZCgg" name="PK_Client_Activite">
        <node defType="com.stambia.rdbms.colref" id="_u8aUXdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_u8aUXtVoEeq6RL_1vDZCgg" ref="#_u8DvCdVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vPdzMNVoEeq6RL_1vDZCgg" name="FK_Client_Activite_Client">
        <node defType="com.stambia.rdbms.relation" id="_vPdzMdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vPdzMtVoEeq6RL_1vDZCgg" ref="#_u8DvENVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vPdzM9VoEeq6RL_1vDZCgg" ref="#_DjPDcF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vDGyNNVoEeq6RL_1vDZCgg" name="ref_Jours">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vDGyNdVoEeq6RL_1vDZCgg" value="ref_Jours"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vDGyNtVoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vDGyN9VoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDGyONVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDGyOdVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vDGyOtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDGyO9VoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDGyPNVoEeq6RL_1vDZCgg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDGyPdVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vDGyPtVoEeq6RL_1vDZCgg" name="LibJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vDGyP9VoEeq6RL_1vDZCgg" value="LibJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vDGyQNVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vDGyQdVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vDGyQtVoEeq6RL_1vDZCgg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vDGyQ9VoEeq6RL_1vDZCgg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vDP8INVoEeq6RL_1vDZCgg" name="PK_ref_Jours">
        <node defType="com.stambia.rdbms.colref" id="_vDP8IdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vDP8ItVoEeq6RL_1vDZCgg" ref="#_vDGyN9VoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_vKWCodVoEeq6RL_1vDZCgg" name="ref_TypeEtabNiv2">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vKWCotVoEeq6RL_1vDZCgg" value="ref_TypeEtabNiv2"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vKWCo9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vKZF8NVoEeq6RL_1vDZCgg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKZF8dVoEeq6RL_1vDZCgg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKZF8tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vKZF89VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKZF9NVoEeq6RL_1vDZCgg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKZF9dVoEeq6RL_1vDZCgg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKZF9tVoEeq6RL_1vDZCgg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vKi28NVoEeq6RL_1vDZCgg" name="LibTypeEtabN2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKi28dVoEeq6RL_1vDZCgg" value="LibTypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKi28tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKi289VoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKi29NVoEeq6RL_1vDZCgg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKi29dVoEeq6RL_1vDZCgg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vKjeANVoEeq6RL_1vDZCgg" name="IdTypeEtabN1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vKjeAdVoEeq6RL_1vDZCgg" value="IdTypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vKjeAtVoEeq6RL_1vDZCgg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_vKjeA9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vKjeBNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vKjeBdVoEeq6RL_1vDZCgg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_vKjeBtVoEeq6RL_1vDZCgg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_vKl6QNVoEeq6RL_1vDZCgg" name="PK_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.colref" id="_vKl6QdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_vKl6QtVoEeq6RL_1vDZCgg" ref="#_vKZF8NVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_vQqtENVoEeq6RL_1vDZCgg" name="FK_ref_TypeEtabNiv2_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_vQqtEdVoEeq6RL_1vDZCgg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_vQqtEtVoEeq6RL_1vDZCgg" ref="#_vKjeANVoEeq6RL_1vDZCgg?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=IdTypeEtabN1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_vQqtE9VoEeq6RL_1vDZCgg" ref="#_Duq3YF-VEema_r8Hg2-1dA?fileId=_sSHtYF-UEema_r8Hg2-1dA$type=md$name=Id?"/>
        </node>
      </node>
    </node>
  </node>
</md:node>