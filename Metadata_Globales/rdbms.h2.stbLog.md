<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_a-e-IEK8EeGyR_2FYG6Ysw" name="stbLog.rdbms" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/h2/h2.rdbms.md#UUID_MD_RDBMS_H2?fileId=UUID_MD_RDBMS_H2$type=md$name=H2%20Database?">
  <attribute defType="com.stambia.rdbms.server.url" id="_7a17wEK8EeGyR_2FYG6Ysw" value="jdbc:h2:tcp://localhost:42100/sessions/internalDb/sessionLogs"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_7a17wUK8EeGyR_2FYG6Ysw" value="org.h2.Driver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_7a2i0EK8EeGyR_2FYG6Ysw" value="sa"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_7a2i0UK8EeGyR_2FYG6Ysw" value="3951C0D79B227B95C1DC348DD0BCE8F1"/>
  <node defType="com.stambia.rdbms.schema" id="_B_AdIJnBEeGqjtSaWHl9Rw" name="INFORMATION_SCHEMA">
    <attribute defType="com.stambia.rdbms.schema.name" id="_B_2xsJnBEeGqjtSaWHl9Rw" value="INFORMATION_SCHEMA"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_B_3_0JnBEeGqjtSaWHl9Rw" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_B_3_0ZnBEeGqjtSaWHl9Rw" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_B_3_0pnBEeGqjtSaWHl9Rw" value="I_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.dataStoreFilter" id="_FQmJsJnBEeGqjtSaWHl9Rw" value="%"/>
    <node defType="com.stambia.rdbms.datastore" id="_E6uAUZnBEeGqjtSaWHl9Rw" name="CATALOGS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_E6unYJnBEeGqjtSaWHl9Rw" value="CATALOGS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_E6unYZnBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_E6unYpnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_E6zf4JnBEeGqjtSaWHl9Rw" name="CATALOG_NAME" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E6zf4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E6zf4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E6zf45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E6zf5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E6zf5ZnBEeGqjtSaWHl9Rw" value="CATALOG_NAME"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_E7VEUZnBEeGqjtSaWHl9Rw" name="COLLATIONS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_E7VEUpnBEeGqjtSaWHl9Rw" value="COLLATIONS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_E7VEU5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_E7VEVJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_E7WScJnBEeGqjtSaWHl9Rw" name="NAME" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E7WScZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E7WScpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E7WSc5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E7WSdJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E7WSdZnBEeGqjtSaWHl9Rw" value="NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E7bK8JnBEeGqjtSaWHl9Rw" name="KEY" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E7bK8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E7bK8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E7bK85nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E7bK9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E7bK9ZnBEeGqjtSaWHl9Rw" value="KEY"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_E7ifsJnBEeGqjtSaWHl9Rw" name="COLUMNS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_E7ifsZnBEeGqjtSaWHl9Rw" value="COLUMNS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_E7ifspnBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_E7ifs5nBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_E7kU4JnBEeGqjtSaWHl9Rw" name="TABLE_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E7kU4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E7kU4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E7kU45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E7kU5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E7kU5ZnBEeGqjtSaWHl9Rw" value="TABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E7pNYJnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E7pNYZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E7pNYpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E7pNY5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E7pNZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E7pNZZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E7uF4JnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E7uF4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E7uF4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E7uF45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E7uF5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E7uF5ZnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E7yXUJnBEeGqjtSaWHl9Rw" name="COLUMN_NAME" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E7yXUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E7yXUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E7yXU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E7yXVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E7yXVZnBEeGqjtSaWHl9Rw" value="COLUMN_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E73P0JnBEeGqjtSaWHl9Rw" name="ORDINAL_POSITION" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E73P0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E73P0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E73P05nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E73P1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E73P1ZnBEeGqjtSaWHl9Rw" value="ORDINAL_POSITION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E78IUJnBEeGqjtSaWHl9Rw" name="COLUMN_DEFAULT" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E78IUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E78IUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E78IU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E78IVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E78IVZnBEeGqjtSaWHl9Rw" value="COLUMN_DEFAULT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8AZwJnBEeGqjtSaWHl9Rw" name="IS_NULLABLE" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8BA0JnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8BA0ZnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8BA0pnBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8BA05nBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8BA1JnBEeGqjtSaWHl9Rw" value="IS_NULLABLE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8FSQJnBEeGqjtSaWHl9Rw" name="DATA_TYPE" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8FSQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8FSQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8FSQ5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8FSRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8FSRZnBEeGqjtSaWHl9Rw" value="DATA_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8KKwJnBEeGqjtSaWHl9Rw" name="CHARACTER_MAXIMUM_LENGTH" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8KKwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8KKwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8KKw5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8KKxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8KKxZnBEeGqjtSaWHl9Rw" value="CHARACTER_MAXIMUM_LENGTH"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8PDQJnBEeGqjtSaWHl9Rw" name="CHARACTER_OCTET_LENGTH" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8PDQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8PDQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8PDQ5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8PDRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8PDRZnBEeGqjtSaWHl9Rw" value="CHARACTER_OCTET_LENGTH"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8TUsJnBEeGqjtSaWHl9Rw" name="NUMERIC_PRECISION" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8TUsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8TUspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8TUs5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8TUtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8TUtZnBEeGqjtSaWHl9Rw" value="NUMERIC_PRECISION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8gJAJnBEeGqjtSaWHl9Rw" name="NUMERIC_PRECISION_RADIX" position="12">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8gJAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8gJApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8gJA5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8gJBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8gJBZnBEeGqjtSaWHl9Rw" value="NUMERIC_PRECISION_RADIX"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8kacJnBEeGqjtSaWHl9Rw" name="NUMERIC_SCALE" position="13">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8kacZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8kacpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8kac5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8kadJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8kadZnBEeGqjtSaWHl9Rw" value="NUMERIC_SCALE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8pS8JnBEeGqjtSaWHl9Rw" name="CHARACTER_SET_NAME" position="14">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8pS8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8pS8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8pS85nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8pS9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8pS9ZnBEeGqjtSaWHl9Rw" value="CHARACTER_SET_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8uLcJnBEeGqjtSaWHl9Rw" name="COLLATION_NAME" position="15">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8uLcZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8uLcpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8uLc5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8uLdJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8uLdZnBEeGqjtSaWHl9Rw" value="COLLATION_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E8yc4JnBEeGqjtSaWHl9Rw" name="TYPE_NAME" position="16">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E8yc4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E8yc4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E8yc45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E8yc5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E8yc5ZnBEeGqjtSaWHl9Rw" value="TYPE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E83VYJnBEeGqjtSaWHl9Rw" name="NULLABLE" position="17">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E83VYZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E83VYpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E83VY5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E83VZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E83VZZnBEeGqjtSaWHl9Rw" value="NULLABLE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E87m0JnBEeGqjtSaWHl9Rw" name="IS_COMPUTED" position="18">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E88N4JnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E88N4ZnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E88N4pnBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E88N45nBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E88N5JnBEeGqjtSaWHl9Rw" value="IS_COMPUTED"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E9AfUJnBEeGqjtSaWHl9Rw" name="SELECTIVITY" position="19">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E9AfUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E9AfUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E9AfU5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E9AfVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E9AfVZnBEeGqjtSaWHl9Rw" value="SELECTIVITY"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E9FX0JnBEeGqjtSaWHl9Rw" name="CHECK_CONSTRAINT" position="20">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E9FX0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E9FX0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E9FX05nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E9FX1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E9FX1ZnBEeGqjtSaWHl9Rw" value="CHECK_CONSTRAINT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E9JpQJnBEeGqjtSaWHl9Rw" name="SEQUENCE_NAME" position="21">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E9JpQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E9JpQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E9JpQ5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E9JpRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E9JpRZnBEeGqjtSaWHl9Rw" value="SEQUENCE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E9OhwJnBEeGqjtSaWHl9Rw" name="REMARKS" position="22">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E9OhwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E9OhwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E9Ohw5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E9OhxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E9OhxZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E9TaQJnBEeGqjtSaWHl9Rw" name="SOURCE_DATA_TYPE" position="23">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E9TaQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E9TaQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E9TaQ5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E9TaRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E9TaRZnBEeGqjtSaWHl9Rw" value="SOURCE_DATA_TYPE"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_E9iDwZnBEeGqjtSaWHl9Rw" name="COLUMN_PRIVILEGES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_E9iDwpnBEeGqjtSaWHl9Rw" value="COLUMN_PRIVILEGES"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_E9iDw5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_E9iDxJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_E9j48JnBEeGqjtSaWHl9Rw" name="GRANTOR" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E9j48ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E9j48pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E9j485nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E9j49JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E9j49ZnBEeGqjtSaWHl9Rw" value="GRANTOR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E9oKYJnBEeGqjtSaWHl9Rw" name="GRANTEE" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E9oKYZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E9oKYpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E9oKY5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E9oKZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E9oKZZnBEeGqjtSaWHl9Rw" value="GRANTEE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E9tC4JnBEeGqjtSaWHl9Rw" name="TABLE_CATALOG" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E9tC4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E9tC4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E9tC45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E9tC5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E9tC5ZnBEeGqjtSaWHl9Rw" value="TABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E9x7YJnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E9x7YZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E9x7YpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E9x7Y5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E9x7ZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E9x7ZZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E92M0JnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E92M0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E92M0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E92M05nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E92z4JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E92z4ZnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E97FUJnBEeGqjtSaWHl9Rw" name="COLUMN_NAME" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E97FUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E97FUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E97FU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E97FVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E97FVZnBEeGqjtSaWHl9Rw" value="COLUMN_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E-GrgJnBEeGqjtSaWHl9Rw" name="PRIVILEGE_TYPE" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-GrgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-GrgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-Grg5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-GrhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-GrhZnBEeGqjtSaWHl9Rw" value="PRIVILEGE_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E-LkAJnBEeGqjtSaWHl9Rw" name="IS_GRANTABLE" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-LkAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-LkApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-LkA5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-LkBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-LkBZnBEeGqjtSaWHl9Rw" value="IS_GRANTABLE"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_E-UG4ZnBEeGqjtSaWHl9Rw" name="CONSTANTS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_E-UG4pnBEeGqjtSaWHl9Rw" value="CONSTANTS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_E-UG45nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_E-UG5JnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_E-VVAJnBEeGqjtSaWHl9Rw" name="CONSTANT_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-VVAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-VVApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-VVA5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-VVBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-VVBZnBEeGqjtSaWHl9Rw" value="CONSTANT_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E-aNgJnBEeGqjtSaWHl9Rw" name="CONSTANT_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-aNgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-aNgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-aNg5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-aNhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-aNhZnBEeGqjtSaWHl9Rw" value="CONSTANT_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E-ee8JnBEeGqjtSaWHl9Rw" name="CONSTANT_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-ee8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-fGAJnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-fGAZnBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-fGApnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-fGA5nBEeGqjtSaWHl9Rw" value="CONSTANT_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E-jXcJnBEeGqjtSaWHl9Rw" name="DATA_TYPE" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-jXcZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-jXcpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-jXc5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-jXdJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-jXdZnBEeGqjtSaWHl9Rw" value="DATA_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E-oP8JnBEeGqjtSaWHl9Rw" name="REMARKS" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-oP8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-oP8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-oP85nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-oP9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-oP9ZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E-shYJnBEeGqjtSaWHl9Rw" name="SQL" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-shYZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-shYpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-shY5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-shZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-shZZnBEeGqjtSaWHl9Rw" value="SQL"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E-xZ4JnBEeGqjtSaWHl9Rw" name="ID" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-xZ4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-xZ4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-xZ45nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-xZ5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-xZ5ZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_E-58wZnBEeGqjtSaWHl9Rw" name="CONSTRAINTS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_E-58wpnBEeGqjtSaWHl9Rw" value="CONSTRAINTS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_E-58w5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_E-58xJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_E-7x8JnBEeGqjtSaWHl9Rw" name="CONSTRAINT_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E-7x8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E-7x8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E-7x85nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E-7x9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E-7x9ZnBEeGqjtSaWHl9Rw" value="CONSTRAINT_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_ADYJnBEeGqjtSaWHl9Rw" name="CONSTRAINT_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_ADYZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_ADYpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_ADY5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_ADZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_ADZZnBEeGqjtSaWHl9Rw" value="CONSTRAINT_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_E74JnBEeGqjtSaWHl9Rw" name="CONSTRAINT_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_E74ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_E74pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_E745nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_E75JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_E75ZnBEeGqjtSaWHl9Rw" value="CONSTRAINT_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_JNUJnBEeGqjtSaWHl9Rw" name="CONSTRAINT_TYPE" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_JNUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_JNUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_JNU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_JNVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_JNVZnBEeGqjtSaWHl9Rw" value="CONSTRAINT_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_OF0JnBEeGqjtSaWHl9Rw" name="TABLE_CATALOG" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_OF0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_OF0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_OF05nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_OF1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_OF1ZnBEeGqjtSaWHl9Rw" value="TABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_SXQJnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_SXQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_SXQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_SXQ5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_SXRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_SXRZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_XPwJnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_XPwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_XPwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_XPw5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_XPxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_XPxZnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_cIQJnBEeGqjtSaWHl9Rw" name="UNIQUE_INDEX_NAME" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_cIQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_cIQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_cIQ5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_cIRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_cIRZnBEeGqjtSaWHl9Rw" value="UNIQUE_INDEX_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_oVgJnBEeGqjtSaWHl9Rw" name="CHECK_EXPRESSION" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_oVgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_oVgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_oVg5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_oVhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_oVhZnBEeGqjtSaWHl9Rw" value="CHECK_EXPRESSION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_tOAJnBEeGqjtSaWHl9Rw" name="COLUMN_LIST" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_tOAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_tOApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_tOA5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_tOBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_tOBZnBEeGqjtSaWHl9Rw" value="COLUMN_LIST"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_xfcJnBEeGqjtSaWHl9Rw" name="REMARKS" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_xfcZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_xfcpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_xfc5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_xfdJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_xfdZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_2X8JnBEeGqjtSaWHl9Rw" name="SQL" position="12">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_2X8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_2X8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_2X85nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_2X9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_2X9ZnBEeGqjtSaWHl9Rw" value="SQL"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_E_7QcJnBEeGqjtSaWHl9Rw" name="ID" position="13">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_E_7QcZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_E_7QcpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_E_7Qc5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_E_7QdJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_E_7QdZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FAFogZnBEeGqjtSaWHl9Rw" name="CROSS_REFERENCES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FAFogpnBEeGqjtSaWHl9Rw" value="CROSS_REFERENCES"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FAFog5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FAFohJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FAHdsJnBEeGqjtSaWHl9Rw" name="PKTABLE_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FAHdsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FAHdspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FAHds5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FAHdtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FAHdtZnBEeGqjtSaWHl9Rw" value="PKTABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FALvIJnBEeGqjtSaWHl9Rw" name="PKTABLE_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FALvIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FALvIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FALvI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FALvJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FALvJZnBEeGqjtSaWHl9Rw" value="PKTABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FAQnoJnBEeGqjtSaWHl9Rw" name="PKTABLE_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FAQnoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FAQnopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FAQno5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FAQnpJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FAQnpZnBEeGqjtSaWHl9Rw" value="PKTABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FAU5EJnBEeGqjtSaWHl9Rw" name="PKCOLUMN_NAME" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FAU5EZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FAU5EpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FAU5E5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FAU5FJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FAU5FZnBEeGqjtSaWHl9Rw" value="PKCOLUMN_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FAZxkJnBEeGqjtSaWHl9Rw" name="FKTABLE_CATALOG" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FAZxkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FAZxkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FAZxk5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FAZxlJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FAZxlZnBEeGqjtSaWHl9Rw" value="FKTABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FAeDAJnBEeGqjtSaWHl9Rw" name="FKTABLE_SCHEMA" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FAeDAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FAeDApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FAeDA5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FAeDBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FAeDBZnBEeGqjtSaWHl9Rw" value="FKTABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FAi7gJnBEeGqjtSaWHl9Rw" name="FKTABLE_NAME" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FAi7gZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FAi7gpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FAi7g5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FAi7hJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FAi7hZnBEeGqjtSaWHl9Rw" value="FKTABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FAn0AJnBEeGqjtSaWHl9Rw" name="FKCOLUMN_NAME" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FAn0AZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FAn0ApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FAn0A5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FAn0BJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FAn0BZnBEeGqjtSaWHl9Rw" value="FKCOLUMN_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FAsFcJnBEeGqjtSaWHl9Rw" name="ORDINAL_POSITION" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FAsFcZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FAsFcpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FAsFc5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FAsFdJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FAsFdZnBEeGqjtSaWHl9Rw" value="ORDINAL_POSITION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FAw98JnBEeGqjtSaWHl9Rw" name="UPDATE_RULE" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FAw98ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FAw98pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FAw985nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FAw99JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FAw99ZnBEeGqjtSaWHl9Rw" value="UPDATE_RULE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FA1PYJnBEeGqjtSaWHl9Rw" name="DELETE_RULE" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FA1PYZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FA1PYpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FA1PY5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FA1PZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FA1PZZnBEeGqjtSaWHl9Rw" value="DELETE_RULE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FA6H4JnBEeGqjtSaWHl9Rw" name="FK_NAME" position="12">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FA6H4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FA6H4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FA6H45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FA6H5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FA6H5ZnBEeGqjtSaWHl9Rw" value="FK_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FA-ZUJnBEeGqjtSaWHl9Rw" name="PK_NAME" position="13">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FA-ZUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FA-ZUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FA-ZU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FA-ZVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FA-ZVZnBEeGqjtSaWHl9Rw" value="PK_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FBKmkJnBEeGqjtSaWHl9Rw" name="DEFERRABILITY" position="14">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FBKmkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FBKmkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FBKmk5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FBKmlJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FBKmlZnBEeGqjtSaWHl9Rw" value="DEFERRABILITY"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FBU-oZnBEeGqjtSaWHl9Rw" name="DOMAINS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FBU-opnBEeGqjtSaWHl9Rw" value="DOMAINS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FBU-o5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FBU-pJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FBWz0JnBEeGqjtSaWHl9Rw" name="DOMAIN_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FBWz0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FBWz0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FBWz05nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FBWz1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FBWz1ZnBEeGqjtSaWHl9Rw" value="DOMAIN_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FBbFQJnBEeGqjtSaWHl9Rw" name="DOMAIN_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FBbFQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FBbFQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FBbFQ5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FBbFRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FBbFRZnBEeGqjtSaWHl9Rw" value="DOMAIN_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FBfWsJnBEeGqjtSaWHl9Rw" name="DOMAIN_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FBfWsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FBfWspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FBfWs5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FBfWtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FBfWtZnBEeGqjtSaWHl9Rw" value="DOMAIN_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FBkPMJnBEeGqjtSaWHl9Rw" name="COLUMN_DEFAULT" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FBkPMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FBkPMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FBkPM5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FBkPNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FBkPNZnBEeGqjtSaWHl9Rw" value="COLUMN_DEFAULT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FBpHsJnBEeGqjtSaWHl9Rw" name="IS_NULLABLE" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FBpHsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FBpHspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FBpHs5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FBpHtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FBpHtZnBEeGqjtSaWHl9Rw" value="IS_NULLABLE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FBtZIJnBEeGqjtSaWHl9Rw" name="DATA_TYPE" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FBtZIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FBtZIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FBtZI5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FBtZJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FBtZJZnBEeGqjtSaWHl9Rw" value="DATA_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FByRoJnBEeGqjtSaWHl9Rw" name="PRECISION" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FByRoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FByRopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FByRo5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FByRpJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FByRpZnBEeGqjtSaWHl9Rw" value="PRECISION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FB2jEJnBEeGqjtSaWHl9Rw" name="SCALE" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FB2jEZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FB2jEpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FB2jE5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FB2jFJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FB2jFZnBEeGqjtSaWHl9Rw" value="SCALE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FB7bkJnBEeGqjtSaWHl9Rw" name="TYPE_NAME" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FB7bkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FB7bkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FB7bk5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FB7blJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FB7blZnBEeGqjtSaWHl9Rw" value="TYPE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FB_tAJnBEeGqjtSaWHl9Rw" name="SELECTIVITY" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FB_tAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FB_tApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FB_tA5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FB_tBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FB_tBZnBEeGqjtSaWHl9Rw" value="SELECTIVITY"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FCElgJnBEeGqjtSaWHl9Rw" name="CHECK_CONSTRAINT" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FCElgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FCElgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FCElg5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FCElhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FCElhZnBEeGqjtSaWHl9Rw" value="CHECK_CONSTRAINT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FCI28JnBEeGqjtSaWHl9Rw" name="REMARKS" position="12">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FCI28ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FCJeAJnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FCJeAZnBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FCJeApnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FCJeA5nBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FCNvcJnBEeGqjtSaWHl9Rw" name="SQL" position="13">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FCNvcZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FCNvcpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FCNvc5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FCNvdJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FCNvdZnBEeGqjtSaWHl9Rw" value="SQL"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FCSn8JnBEeGqjtSaWHl9Rw" name="ID" position="14">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FCSn8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FCSn8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FCSn85nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FCSn9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FCSn9ZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FCdAAZnBEeGqjtSaWHl9Rw" name="FUNCTION_ALIASES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FCdAApnBEeGqjtSaWHl9Rw" value="FUNCTION_ALIASES"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FCdAA5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FCdABJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FCeOIJnBEeGqjtSaWHl9Rw" name="ALIAS_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FCeOIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FCeOIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FCeOI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FCeOJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FCeOJZnBEeGqjtSaWHl9Rw" value="ALIAS_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FCifkJnBEeGqjtSaWHl9Rw" name="ALIAS_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FCifkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FCifkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FCifk5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FCiflJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FCiflZnBEeGqjtSaWHl9Rw" value="ALIAS_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FCvT4JnBEeGqjtSaWHl9Rw" name="ALIAS_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FCvT4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FCvT4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FCvT45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FCvT5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FCvT5ZnBEeGqjtSaWHl9Rw" value="ALIAS_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FCzlUJnBEeGqjtSaWHl9Rw" name="JAVA_CLASS" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FCzlUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FCzlUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FCzlU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FCzlVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FCzlVZnBEeGqjtSaWHl9Rw" value="JAVA_CLASS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FC4d0JnBEeGqjtSaWHl9Rw" name="JAVA_METHOD" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FC4d0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FC4d0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FC4d05nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FC4d1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FC4d1ZnBEeGqjtSaWHl9Rw" value="JAVA_METHOD"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FC8vQJnBEeGqjtSaWHl9Rw" name="DATA_TYPE" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FC8vQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FC8vQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FC8vQ5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FC8vRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FC8vRZnBEeGqjtSaWHl9Rw" value="DATA_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FDBnwJnBEeGqjtSaWHl9Rw" name="COLUMN_COUNT" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDBnwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDBnwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDBnw5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDBnxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDBnxZnBEeGqjtSaWHl9Rw" value="COLUMN_COUNT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FDF5MJnBEeGqjtSaWHl9Rw" name="RETURNS_RESULT" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDF5MZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDF5MpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDF5M5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDF5NJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDF5NZnBEeGqjtSaWHl9Rw" value="RETURNS_RESULT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FDKxsJnBEeGqjtSaWHl9Rw" name="REMARKS" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDKxsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDKxspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDKxs5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDKxtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDKxtZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FDPDIJnBEeGqjtSaWHl9Rw" name="ID" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDPDIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDPDIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDPDI5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDPDJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDPDJZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FDTUkJnBEeGqjtSaWHl9Rw" name="SOURCE" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDTUkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDTUkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDTUk5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDTUlJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDTUlZnBEeGqjtSaWHl9Rw" value="SOURCE"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FDcegZnBEeGqjtSaWHl9Rw" name="FUNCTION_COLUMNS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FDdFkJnBEeGqjtSaWHl9Rw" value="FUNCTION_COLUMNS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FDdFkZnBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FDdFkpnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FDeTsJnBEeGqjtSaWHl9Rw" name="ALIAS_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDeTsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDeTspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDeTs5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDeTtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDeTtZnBEeGqjtSaWHl9Rw" value="ALIAS_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FDjMMJnBEeGqjtSaWHl9Rw" name="ALIAS_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDjMMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDjMMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDjMM5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDjMNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDjMNZnBEeGqjtSaWHl9Rw" value="ALIAS_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FDndoJnBEeGqjtSaWHl9Rw" name="ALIAS_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDndoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDndopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDndo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDndpJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDndpZnBEeGqjtSaWHl9Rw" value="ALIAS_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FDsWIJnBEeGqjtSaWHl9Rw" name="JAVA_CLASS" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDsWIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDsWIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDsWI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDsWJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDsWJZnBEeGqjtSaWHl9Rw" value="JAVA_CLASS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FDxOoJnBEeGqjtSaWHl9Rw" name="JAVA_METHOD" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FDxOoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FDxOopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FDxOo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FDxOpJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FDxOpZnBEeGqjtSaWHl9Rw" value="JAVA_METHOD"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FD2HIJnBEeGqjtSaWHl9Rw" name="COLUMN_COUNT" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FD2HIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FD2HIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FD2HI5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FD2HJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FD2HJZnBEeGqjtSaWHl9Rw" value="COLUMN_COUNT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FD6YkJnBEeGqjtSaWHl9Rw" name="POS" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FD6YkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FD6YkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FD6Yk5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FD6YlJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FD6YlZnBEeGqjtSaWHl9Rw" value="POS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FD_REJnBEeGqjtSaWHl9Rw" name="COLUMN_NAME" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FD_REZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FD_REpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FD_RE5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FD_RFJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FD_RFZnBEeGqjtSaWHl9Rw" value="COLUMN_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FEEJkJnBEeGqjtSaWHl9Rw" name="DATA_TYPE" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FEEJkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FEEJkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FEEJk5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FEEJlJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FEEJlZnBEeGqjtSaWHl9Rw" value="DATA_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FEQW0JnBEeGqjtSaWHl9Rw" name="TYPE_NAME" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FEQW0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FEQW0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FEQW05nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FEQW1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FEQW1ZnBEeGqjtSaWHl9Rw" value="TYPE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FEVPUJnBEeGqjtSaWHl9Rw" name="PRECISION" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FEVPUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FEVPUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FEVPU5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FEVPVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FEVPVZnBEeGqjtSaWHl9Rw" value="PRECISION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FEZgwJnBEeGqjtSaWHl9Rw" name="SCALE" position="12">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FEZgwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FEZgwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FEZgw5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FEZgxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FEZgxZnBEeGqjtSaWHl9Rw" value="SCALE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FEeZQJnBEeGqjtSaWHl9Rw" name="RADIX" position="13">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FEeZQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FEeZQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FEeZQ5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FEeZRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FEeZRZnBEeGqjtSaWHl9Rw" value="RADIX"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FEiqsJnBEeGqjtSaWHl9Rw" name="NULLABLE" position="14">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FEiqsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FEiqspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FEiqs5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FEiqtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FEiqtZnBEeGqjtSaWHl9Rw" value="NULLABLE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FEnjMJnBEeGqjtSaWHl9Rw" name="COLUMN_TYPE" position="15">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FEnjMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FEnjMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FEnjM5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FEnjNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FEnjNZnBEeGqjtSaWHl9Rw" value="COLUMN_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FEsbsJnBEeGqjtSaWHl9Rw" name="REMARKS" position="16">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FEsbsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FEsbspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FEsbs5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FEsbtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FEsbtZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FEwtIJnBEeGqjtSaWHl9Rw" name="COLUMN_DEFAULT" position="17">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FEwtIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FEwtIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FEwtI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FEwtJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FEwtJZnBEeGqjtSaWHl9Rw" value="COLUMN_DEFAULT"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FE8TUZnBEeGqjtSaWHl9Rw" name="HELP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FE8TUpnBEeGqjtSaWHl9Rw" value="HELP"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FE8TU5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FE8TVJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FE86YJnBEeGqjtSaWHl9Rw" name="ID" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FE86YZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FE86YpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FE86Y5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FE9hcJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FE9hcZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FFBy4JnBEeGqjtSaWHl9Rw" name="SECTION" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FFBy4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FFBy4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FFBy45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FFBy5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FFBy5ZnBEeGqjtSaWHl9Rw" value="SECTION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FFGEUJnBEeGqjtSaWHl9Rw" name="TOPIC" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FFGEUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FFGEUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FFGEU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FFGEVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FFGEVZnBEeGqjtSaWHl9Rw" value="TOPIC"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FFK80JnBEeGqjtSaWHl9Rw" name="SYNTAX" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FFK80ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FFK80pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FFK805nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FFK81JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FFK81ZnBEeGqjtSaWHl9Rw" value="SYNTAX"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FFPOQJnBEeGqjtSaWHl9Rw" name="TEXT" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FFPOQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FFPOQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FFPOQ5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FFPORJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FFPORZnBEeGqjtSaWHl9Rw" value="TEXT"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FFWjAZnBEeGqjtSaWHl9Rw" name="INDEXES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FFWjApnBEeGqjtSaWHl9Rw" value="INDEXES"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FFWjA5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FFWjBJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FFYYMJnBEeGqjtSaWHl9Rw" name="TABLE_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FFYYMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FFYYMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FFYYM5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FFYYNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FFYYNZnBEeGqjtSaWHl9Rw" value="TABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FFcpoJnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FFcpoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FFcpopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FFcpo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FFcppJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FFcppZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FFhiIJnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FFhiIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FFhiIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FFhiI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FFhiJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FFhiJZnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FFlzkJnBEeGqjtSaWHl9Rw" name="NON_UNIQUE" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FFlzkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FFlzkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FFlzk5nBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FFlzlJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FFlzlZnBEeGqjtSaWHl9Rw" value="NON_UNIQUE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FFyn4JnBEeGqjtSaWHl9Rw" name="INDEX_NAME" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FFyn4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FFyn4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FFyn45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FFyn5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FFyn5ZnBEeGqjtSaWHl9Rw" value="INDEX_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FF3gYJnBEeGqjtSaWHl9Rw" name="ORDINAL_POSITION" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FF3gYZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FF3gYpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FF3gY5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FF3gZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FF3gZZnBEeGqjtSaWHl9Rw" value="ORDINAL_POSITION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FF7x0JnBEeGqjtSaWHl9Rw" name="COLUMN_NAME" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FF7x0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FF8Y4JnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FF8Y4ZnBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FF8Y4pnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FF8Y45nBEeGqjtSaWHl9Rw" value="COLUMN_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGAqUJnBEeGqjtSaWHl9Rw" name="CARDINALITY" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGAqUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGAqUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGAqU5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGAqVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGAqVZnBEeGqjtSaWHl9Rw" value="CARDINALITY"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGFi0JnBEeGqjtSaWHl9Rw" name="PRIMARY_KEY" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGFi0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGFi0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGFi05nBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGFi1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGFi1ZnBEeGqjtSaWHl9Rw" value="PRIMARY_KEY"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGJ0QJnBEeGqjtSaWHl9Rw" name="INDEX_TYPE_NAME" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGJ0QZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGJ0QpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGJ0Q5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGJ0RJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGJ0RZnBEeGqjtSaWHl9Rw" value="INDEX_TYPE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGOswJnBEeGqjtSaWHl9Rw" name="IS_GENERATED" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGOswZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGOswpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGOsw5nBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGOsxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGOsxZnBEeGqjtSaWHl9Rw" value="IS_GENERATED"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGS-MJnBEeGqjtSaWHl9Rw" name="INDEX_TYPE" position="12">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGS-MZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGS-MpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGS-M5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGS-NJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGS-NZnBEeGqjtSaWHl9Rw" value="INDEX_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGX2sJnBEeGqjtSaWHl9Rw" name="ASC_OR_DESC" position="13">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGX2sZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGX2spnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGX2s5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGX2tJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGX2tZnBEeGqjtSaWHl9Rw" value="ASC_OR_DESC"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGcIIJnBEeGqjtSaWHl9Rw" name="PAGES" position="14">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGcIIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGcIIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGcII5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGcIJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGcIJZnBEeGqjtSaWHl9Rw" value="PAGES"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGhAoJnBEeGqjtSaWHl9Rw" name="FILTER_CONDITION" position="15">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGhAoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGhAopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGhAo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGhApJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGhApZnBEeGqjtSaWHl9Rw" value="FILTER_CONDITION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGlSEJnBEeGqjtSaWHl9Rw" name="REMARKS" position="16">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGlSEZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGlSEpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGlSE5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGl5IJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGl5IZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGqKkJnBEeGqjtSaWHl9Rw" name="SQL" position="17">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGqKkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGqKkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGqKk5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGqKlJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGqKlZnBEeGqjtSaWHl9Rw" value="SQL"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGvDEJnBEeGqjtSaWHl9Rw" name="ID" position="18">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGvDEZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGvDEpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGvDE5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGvDFJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGvDFZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FGzUgJnBEeGqjtSaWHl9Rw" name="SORT_TYPE" position="19">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FGzUgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FGzUgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FGzUg5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FGzUhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FGzUhZnBEeGqjtSaWHl9Rw" value="SORT_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FG4NAJnBEeGqjtSaWHl9Rw" name="CONSTRAINT_NAME" position="20">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FG4NAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FG4NApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FG4NA5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FG4NBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FG4NBZnBEeGqjtSaWHl9Rw" value="CONSTRAINT_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FG8ecJnBEeGqjtSaWHl9Rw" name="INDEX_CLASS" position="21">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FG8ecZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FG8ecpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FG8ec5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FG8edJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FG8edZnBEeGqjtSaWHl9Rw" value="INDEX_CLASS"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FHJSwZnBEeGqjtSaWHl9Rw" name="IN_DOUBT">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FHJSwpnBEeGqjtSaWHl9Rw" value="IN_DOUBT"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FHJSw5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FHJSxJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FHKg4JnBEeGqjtSaWHl9Rw" name="TRANSACTION" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FHKg4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FHKg4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FHKg45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FHKg5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FHKg5ZnBEeGqjtSaWHl9Rw" value="TRANSACTION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FHXVMJnBEeGqjtSaWHl9Rw" name="STATE" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FHXVMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FHXVMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FHXVM5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FHXVNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FHXVNZnBEeGqjtSaWHl9Rw" value="STATE"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FHeC4ZnBEeGqjtSaWHl9Rw" name="LOCKS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FHeC4pnBEeGqjtSaWHl9Rw" value="LOCKS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FHeC45nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FHeC5JnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FHep8JnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FHep8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FHep8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FHep85nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FHep9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FHep9ZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FHjicJnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FHjicZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FHjicpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FHjic5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FHjidJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FHjidZnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FHnz4JnBEeGqjtSaWHl9Rw" name="SESSION_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FHnz4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FHnz4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FHnz45nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FHnz5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FHnz5ZnBEeGqjtSaWHl9Rw" value="SESSION_ID"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FHssYJnBEeGqjtSaWHl9Rw" name="LOCK_TYPE" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FHssYZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FHssYpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FHssY5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FHssZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FHssZZnBEeGqjtSaWHl9Rw" value="LOCK_TYPE"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FH0BIZnBEeGqjtSaWHl9Rw" name="RIGHTS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FH0BIpnBEeGqjtSaWHl9Rw" value="RIGHTS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FH0BI5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FH0BJJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FH1PQJnBEeGqjtSaWHl9Rw" name="GRANTEE" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FH1PQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FH1PQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FH1PQ5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FH1PRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FH1PRZnBEeGqjtSaWHl9Rw" value="GRANTEE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FH5gsJnBEeGqjtSaWHl9Rw" name="GRANTEETYPE" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FH5gsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FH5gspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FH5gs5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FH5gtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FH5gtZnBEeGqjtSaWHl9Rw" value="GRANTEETYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FH9yIJnBEeGqjtSaWHl9Rw" name="GRANTEDROLE" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FH9yIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FH9yIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FH9yI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FH9yJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FH9yJZnBEeGqjtSaWHl9Rw" value="GRANTEDROLE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FICqoJnBEeGqjtSaWHl9Rw" name="RIGHTS" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FICqoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FICqopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FICqo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FICqpJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FICqpZnBEeGqjtSaWHl9Rw" value="RIGHTS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FIHjIJnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FIHjIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FIHjIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FIHjI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FIHjJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FIHjJZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FIMboJnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FIMboZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FIMbopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FIMbo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FIMbpJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FIMbpZnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FIQtEJnBEeGqjtSaWHl9Rw" name="ID" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FIQtEZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FIQtEpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FIQtE5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FIQtFJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FIQtFZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FIZP8ZnBEeGqjtSaWHl9Rw" name="ROLES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FIZP8pnBEeGqjtSaWHl9Rw" value="ROLES"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FIZP85nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FIZP9JnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FIZ3AJnBEeGqjtSaWHl9Rw" name="NAME" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FIZ3AZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FIaeEJnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FIaeEZnBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FIaeEpnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FIaeE5nBEeGqjtSaWHl9Rw" value="NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FIevgJnBEeGqjtSaWHl9Rw" name="REMARKS" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FIevgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FIevgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FIevg5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FIevhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FIevhZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FIjA8JnBEeGqjtSaWHl9Rw" name="ID" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FIjA8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FIjA8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FIjA85nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FIjA9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FIjA9ZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FIqVsZnBEeGqjtSaWHl9Rw" name="SCHEMATA">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FIqVspnBEeGqjtSaWHl9Rw" value="SCHEMATA"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FIqVs5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FIqVtJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FIrj0JnBEeGqjtSaWHl9Rw" name="CATALOG_NAME" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FIrj0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FIrj0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FIrj05nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FIrj1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FIrj1ZnBEeGqjtSaWHl9Rw" value="CATALOG_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FIv1QJnBEeGqjtSaWHl9Rw" name="SCHEMA_NAME" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FIv1QZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FIv1QpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FIv1Q5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FIv1RJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FIv1RZnBEeGqjtSaWHl9Rw" value="SCHEMA_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FI9QoJnBEeGqjtSaWHl9Rw" name="SCHEMA_OWNER" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FI9QoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FI9QopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FI9Qo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FI9QpJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FI9QpZnBEeGqjtSaWHl9Rw" value="SCHEMA_OWNER"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJCJIJnBEeGqjtSaWHl9Rw" name="DEFAULT_CHARACTER_SET_NAME" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJCJIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJCJIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJCJI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJCJJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJCJJZnBEeGqjtSaWHl9Rw" value="DEFAULT_CHARACTER_SET_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJGakJnBEeGqjtSaWHl9Rw" name="DEFAULT_COLLATION_NAME" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJGakZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJGakpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJGak5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJGalJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJGalZnBEeGqjtSaWHl9Rw" value="DEFAULT_COLLATION_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJLTEJnBEeGqjtSaWHl9Rw" name="IS_DEFAULT" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJLTEZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJLTEpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJLTE5nBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJLTFJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJLTFZnBEeGqjtSaWHl9Rw" value="IS_DEFAULT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJPkgJnBEeGqjtSaWHl9Rw" name="REMARKS" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJPkgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJPkgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJPkg5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJPkhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJPkhZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJUdAJnBEeGqjtSaWHl9Rw" name="ID" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJUdAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJUdApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJUdA5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJUdBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJUdBZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FJcY0ZnBEeGqjtSaWHl9Rw" name="SEQUENCES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FJcY0pnBEeGqjtSaWHl9Rw" value="SEQUENCES"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FJcY05nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FJcY1JnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FJdm8JnBEeGqjtSaWHl9Rw" name="SEQUENCE_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJdm8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJdm8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJdm85nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJdm9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJdm9ZnBEeGqjtSaWHl9Rw" value="SEQUENCE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJh4YJnBEeGqjtSaWHl9Rw" name="SEQUENCE_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJh4YZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJh4YpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJh4Y5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJh4ZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJh4ZZnBEeGqjtSaWHl9Rw" value="SEQUENCE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJmw4JnBEeGqjtSaWHl9Rw" name="SEQUENCE_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJmw4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJmw4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJmw45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJmw5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJmw5ZnBEeGqjtSaWHl9Rw" value="SEQUENCE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJrCUJnBEeGqjtSaWHl9Rw" name="CURRENT_VALUE" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJrCUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJrCUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJrCU5nBEeGqjtSaWHl9Rw" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJrCVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJrCVZnBEeGqjtSaWHl9Rw" value="CURRENT_VALUE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJv60JnBEeGqjtSaWHl9Rw" name="INCREMENT" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJv60ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJv60pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJv605nBEeGqjtSaWHl9Rw" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJv61JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJv61ZnBEeGqjtSaWHl9Rw" value="INCREMENT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJ0zUJnBEeGqjtSaWHl9Rw" name="IS_GENERATED" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJ0zUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJ0zUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJ0zU5nBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJ0zVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJ0zVZnBEeGqjtSaWHl9Rw" value="IS_GENERATED"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJ5EwJnBEeGqjtSaWHl9Rw" name="REMARKS" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJ5EwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJ5EwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJ5Ew5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJ5ExJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJ5ExZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FJ99QJnBEeGqjtSaWHl9Rw" name="CACHE" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FJ99QZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FJ99QpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FJ99Q5nBEeGqjtSaWHl9Rw" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FJ99RJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FJ99RZnBEeGqjtSaWHl9Rw" value="CACHE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FKCOsJnBEeGqjtSaWHl9Rw" name="ID" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FKCOsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FKCOspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FKC1wJnBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FKC1wZnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FKC1wpnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FKKxkZnBEeGqjtSaWHl9Rw" name="SESSIONS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FKKxkpnBEeGqjtSaWHl9Rw" value="SESSIONS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FKKxk5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FKKxlJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FKL_sJnBEeGqjtSaWHl9Rw" name="ID" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FKL_sZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FKL_spnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FKL_s5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FKL_tJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FKL_tZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FKQRIJnBEeGqjtSaWHl9Rw" name="USER_NAME" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FKQRIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FKQRIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FKQRI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FKQRJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FKQRJZnBEeGqjtSaWHl9Rw" value="USER_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FKVJoJnBEeGqjtSaWHl9Rw" name="SESSION_START" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FKVJoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FKVJopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FKVJo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FKVJpJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FKVJpZnBEeGqjtSaWHl9Rw" value="SESSION_START"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FKfhsJnBEeGqjtSaWHl9Rw" name="STATEMENT" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FKfhsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FKfhspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FKfhs5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FKfhtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FKfhtZnBEeGqjtSaWHl9Rw" value="STATEMENT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FKkaMJnBEeGqjtSaWHl9Rw" name="STATEMENT_START" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FKkaMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FKkaMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FKkaM5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FKkaNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FKkaNZnBEeGqjtSaWHl9Rw" value="STATEMENT_START"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FKru8ZnBEeGqjtSaWHl9Rw" name="SESSION_STATE">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FKru8pnBEeGqjtSaWHl9Rw" value="SESSION_STATE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FKru85nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FKru9JnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FKs9EJnBEeGqjtSaWHl9Rw" name="KEY" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FKs9EZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FKs9EpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FKs9E5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FKs9FJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FKs9FZnBEeGqjtSaWHl9Rw" value="KEY"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FKxOgJnBEeGqjtSaWHl9Rw" name="SQL" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FKxOgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FKxOgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FKxOg5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FKxOhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FKxOhZnBEeGqjtSaWHl9Rw" value="SQL"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FK38MZnBEeGqjtSaWHl9Rw" name="SETTINGS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FK38MpnBEeGqjtSaWHl9Rw" value="SETTINGS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FK38M5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FK38NJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FK5KUJnBEeGqjtSaWHl9Rw" name="NAME" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FK5KUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FK5KUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FK5KU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FK5KVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FK5KVZnBEeGqjtSaWHl9Rw" value="NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FK9bwJnBEeGqjtSaWHl9Rw" name="VALUE" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FK9bwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FK9bwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FK9bw5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FK9bxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FK9bxZnBEeGqjtSaWHl9Rw" value="VALUE"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FLEJcJnBEeGqjtSaWHl9Rw" name="TABLES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FLEJcZnBEeGqjtSaWHl9Rw" value="TABLES"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FLEJcpnBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FLEJc5nBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FLEwgJnBEeGqjtSaWHl9Rw" name="TABLE_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLEwgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLEwgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLEwg5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLEwhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLEwhZnBEeGqjtSaWHl9Rw" value="TABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLJpAJnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLJpAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLJpApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLJpA5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLJpBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLJpBZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLOhgJnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLOhgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLOhgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLOhg5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLOhhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLOhhZnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLSy8JnBEeGqjtSaWHl9Rw" name="TABLE_TYPE" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLSy8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLSy8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLSy85nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLSy9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLSy9ZnBEeGqjtSaWHl9Rw" value="TABLE_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLXrcJnBEeGqjtSaWHl9Rw" name="STORAGE_TYPE" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLXrcZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLXrcpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLXrc5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLXrdJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLXrdZnBEeGqjtSaWHl9Rw" value="STORAGE_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLb84JnBEeGqjtSaWHl9Rw" name="SQL" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLb84ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLb84pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLb845nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLb85JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLb85ZnBEeGqjtSaWHl9Rw" value="SQL"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLg1YJnBEeGqjtSaWHl9Rw" name="REMARKS" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLg1YZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLg1YpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLg1Y5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLg1ZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLg1ZZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLlG0JnBEeGqjtSaWHl9Rw" name="LAST_MODIFICATION" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLlG0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLlG0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLlG05nBEeGqjtSaWHl9Rw" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLlG1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLlG1ZnBEeGqjtSaWHl9Rw" value="LAST_MODIFICATION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLp_UJnBEeGqjtSaWHl9Rw" name="ID" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLp_UZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLp_UpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLp_U5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLp_VJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLp_VZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLuQwJnBEeGqjtSaWHl9Rw" name="TYPE_NAME" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLuQwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLuQwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLuQw5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLuQxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLuQxZnBEeGqjtSaWHl9Rw" value="TYPE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FLzJQJnBEeGqjtSaWHl9Rw" name="TABLE_CLASS" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FLzJQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FLzJQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FLzJQ5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FLzJRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FLzJRZnBEeGqjtSaWHl9Rw" value="TABLE_CLASS"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FMBywZnBEeGqjtSaWHl9Rw" name="TABLE_PRIVILEGES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FMBywpnBEeGqjtSaWHl9Rw" value="TABLE_PRIVILEGES"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FMByw5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FMByxJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FMDA4JnBEeGqjtSaWHl9Rw" name="GRANTOR" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMDA4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMDA4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMDA45nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMDA5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMDA5ZnBEeGqjtSaWHl9Rw" value="GRANTOR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FMHSUJnBEeGqjtSaWHl9Rw" name="GRANTEE" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMHSUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMHSUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMHSU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMHSVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMHSVZnBEeGqjtSaWHl9Rw" value="GRANTEE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FMMK0JnBEeGqjtSaWHl9Rw" name="TABLE_CATALOG" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMMK0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMMK0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMMK05nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMMK1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMMK1ZnBEeGqjtSaWHl9Rw" value="TABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FMQcQJnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMQcQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMQcQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMQcQ5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMQcRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMQcRZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FMVUwJnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMVUwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMVUwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMVUw5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMVUxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMVUxZnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FMZmMJnBEeGqjtSaWHl9Rw" name="PRIVILEGE_TYPE" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMZmMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMZmMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMZmM5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMZmNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMZmNZnBEeGqjtSaWHl9Rw" value="PRIVILEGE_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FMeesJnBEeGqjtSaWHl9Rw" name="IS_GRANTABLE" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMeesZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMeespnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMees5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMeetJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMeetZnBEeGqjtSaWHl9Rw" value="IS_GRANTABLE"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FMmagZnBEeGqjtSaWHl9Rw" name="TABLE_TYPES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FMmagpnBEeGqjtSaWHl9Rw" value="TABLE_TYPES"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FMmag5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FMmahJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FMnBkJnBEeGqjtSaWHl9Rw" name="TYPE" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMnBkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMnBkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMnBk5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMnBlJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMnBlZnBEeGqjtSaWHl9Rw" value="TYPE"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FMtIMZnBEeGqjtSaWHl9Rw" name="TRIGGERS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FMtIMpnBEeGqjtSaWHl9Rw" value="TRIGGERS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FMtIM5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FMtINJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FMuWUJnBEeGqjtSaWHl9Rw" name="TRIGGER_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMuWUZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMuWUpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMuWU5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMuWVJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMuWVZnBEeGqjtSaWHl9Rw" value="TRIGGER_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FMynwJnBEeGqjtSaWHl9Rw" name="TRIGGER_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FMynwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FMynwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FMynw5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FMynxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FMynxZnBEeGqjtSaWHl9Rw" value="TRIGGER_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FM3gQJnBEeGqjtSaWHl9Rw" name="TRIGGER_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FM3gQZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FM3gQpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FM3gQ5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FM3gRJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FM3gRZnBEeGqjtSaWHl9Rw" value="TRIGGER_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FM8YwJnBEeGqjtSaWHl9Rw" name="TRIGGER_TYPE" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FM8YwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FM8YwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FM8Yw5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FM8YxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FM8YxZnBEeGqjtSaWHl9Rw" value="TRIGGER_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNAqMJnBEeGqjtSaWHl9Rw" name="TABLE_CATALOG" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNAqMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNAqMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNAqM5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNAqNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNAqNZnBEeGqjtSaWHl9Rw" value="TABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNFisJnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNFisZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNFispnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNFis5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNFitJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNFitZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNJ0IJnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNJ0IZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNJ0IpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNJ0I5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNJ0JJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNJ0JZnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNOsoJnBEeGqjtSaWHl9Rw" name="BEFORE" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNOsoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNOsopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNOso5nBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNOspJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNOspZnBEeGqjtSaWHl9Rw" value="BEFORE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNS-EJnBEeGqjtSaWHl9Rw" name="JAVA_CLASS" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNS-EZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNS-EpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNS-E5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNS-FJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNS-FZnBEeGqjtSaWHl9Rw" value="JAVA_CLASS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNd9MJnBEeGqjtSaWHl9Rw" name="QUEUE_SIZE" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNd9MZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNd9MpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNd9M5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNd9NJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNd9NZnBEeGqjtSaWHl9Rw" value="QUEUE_SIZE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNi1sJnBEeGqjtSaWHl9Rw" name="NO_WAIT" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNi1sZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNi1spnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNi1s5nBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNi1tJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNi1tZnBEeGqjtSaWHl9Rw" value="NO_WAIT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNnHIJnBEeGqjtSaWHl9Rw" name="REMARKS" position="12">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNnHIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNnHIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNnHI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNnHJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNnHJZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNr_oJnBEeGqjtSaWHl9Rw" name="SQL" position="13">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNr_oZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNr_opnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNr_o5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNr_pJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNr_pZnBEeGqjtSaWHl9Rw" value="SQL"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FNw4IJnBEeGqjtSaWHl9Rw" name="ID" position="14">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FNw4IZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FNw4IpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FNw4I5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FNw4JJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FNw4JZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FN6pIZnBEeGqjtSaWHl9Rw" name="TYPE_INFO">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FN6pIpnBEeGqjtSaWHl9Rw" value="TYPE_INFO"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FN6pI5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FN6pJJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FN7QMJnBEeGqjtSaWHl9Rw" name="TYPE_NAME" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FN7QMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FN7QMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FN7QM5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FN7QNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FN7QNZnBEeGqjtSaWHl9Rw" value="TYPE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOAIsJnBEeGqjtSaWHl9Rw" name="DATA_TYPE" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOAIsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOAIspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOAIs5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOAItJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOAItZnBEeGqjtSaWHl9Rw" value="DATA_TYPE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOEaIJnBEeGqjtSaWHl9Rw" name="PRECISION" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOEaIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOEaIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOEaI5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOEaJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOEaJZnBEeGqjtSaWHl9Rw" value="PRECISION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOJSoJnBEeGqjtSaWHl9Rw" name="PREFIX" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOJSoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOJSopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOJSo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOJSpJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOJSpZnBEeGqjtSaWHl9Rw" value="PREFIX"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FONkEJnBEeGqjtSaWHl9Rw" name="SUFFIX" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FONkEZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FONkEpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FONkE5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FONkFJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FONkFZnBEeGqjtSaWHl9Rw" value="SUFFIX"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOSckJnBEeGqjtSaWHl9Rw" name="PARAMS" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOSckZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOSckpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOSck5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOSclJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOSclZnBEeGqjtSaWHl9Rw" value="PARAMS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOXVEJnBEeGqjtSaWHl9Rw" name="AUTO_INCREMENT" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOXVEZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOXVEpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOXVE5nBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOXVFJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOXVFZnBEeGqjtSaWHl9Rw" value="AUTO_INCREMENT"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FObmgJnBEeGqjtSaWHl9Rw" name="MINIMUM_SCALE" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FObmgZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FObmgpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FObmg5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FObmhJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FObmhZnBEeGqjtSaWHl9Rw" value="MINIMUM_SCALE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOgfAJnBEeGqjtSaWHl9Rw" name="MAXIMUM_SCALE" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOgfAZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOgfApnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOgfA5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOgfBJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOgfBZnBEeGqjtSaWHl9Rw" value="MAXIMUM_SCALE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOkwcJnBEeGqjtSaWHl9Rw" name="RADIX" position="10">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOkwcZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOkwcpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOkwc5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOkwdJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOkwdZnBEeGqjtSaWHl9Rw" value="RADIX"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOpo8JnBEeGqjtSaWHl9Rw" name="POS" position="11">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOpo8ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOpo8pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOpo85nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOpo9JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOpo9ZnBEeGqjtSaWHl9Rw" value="POS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOt6YJnBEeGqjtSaWHl9Rw" name="CASE_SENSITIVE" position="12">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOt6YZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOt6YpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOt6Y5nBEeGqjtSaWHl9Rw" value="BOOLEAN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOt6ZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOt6ZZnBEeGqjtSaWHl9Rw" value="CASE_SENSITIVE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FOyy4JnBEeGqjtSaWHl9Rw" name="NULLABLE" position="13">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FOyy4ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FOyy4pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FOyy45nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FOyy5JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FOyy5ZnBEeGqjtSaWHl9Rw" value="NULLABLE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FO-ZEJnBEeGqjtSaWHl9Rw" name="SEARCHABLE" position="14">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FO-ZEZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FO-ZEpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FO-ZE5nBEeGqjtSaWHl9Rw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FO-ZFJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FO-ZFZnBEeGqjtSaWHl9Rw" value="SEARCHABLE"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FPIKEZnBEeGqjtSaWHl9Rw" name="USERS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FPIKEpnBEeGqjtSaWHl9Rw" value="USERS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FPIKE5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FPIKFJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FPIxIJnBEeGqjtSaWHl9Rw" name="NAME" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FPIxIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FPIxIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FPIxI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FPIxJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FPIxJZnBEeGqjtSaWHl9Rw" value="NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FPNpoJnBEeGqjtSaWHl9Rw" name="ADMIN" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FPNpoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FPNpopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FPNpo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FPNppJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FPNppZnBEeGqjtSaWHl9Rw" value="ADMIN"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FPR7EJnBEeGqjtSaWHl9Rw" name="REMARKS" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FPR7EZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FPR7EpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FPR7E5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FPR7FJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FPR7FZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FPWzkJnBEeGqjtSaWHl9Rw" name="ID" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FPWzkZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FPWzkpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FPWzk5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FPWzlJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FPWzlZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_FPdhQZnBEeGqjtSaWHl9Rw" name="VIEWS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_FPdhQpnBEeGqjtSaWHl9Rw" value="VIEWS"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_FPdhQ5nBEeGqjtSaWHl9Rw" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_FPdhRJnBEeGqjtSaWHl9Rw" value="SYSTEM TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FPevYJnBEeGqjtSaWHl9Rw" name="TABLE_CATALOG" position="1">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FPevYZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FPevYpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FPevY5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FPevZJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FPevZZnBEeGqjtSaWHl9Rw" value="TABLE_CATALOG"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FPjA0JnBEeGqjtSaWHl9Rw" name="TABLE_SCHEMA" position="2">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FPjA0ZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FPjA0pnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FPjA05nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FPjA1JnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FPjA1ZnBEeGqjtSaWHl9Rw" value="TABLE_SCHEMA"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FPnSQJnBEeGqjtSaWHl9Rw" name="TABLE_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FPn5UJnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FPn5UZnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FPn5UpnBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FPn5U5nBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FPn5VJnBEeGqjtSaWHl9Rw" value="TABLE_NAME"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FPsKwJnBEeGqjtSaWHl9Rw" name="VIEW_DEFINITION" position="4">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FPsKwZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FPsKwpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FPsKw5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FPsKxJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FPsKxZnBEeGqjtSaWHl9Rw" value="VIEW_DEFINITION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FPwcMJnBEeGqjtSaWHl9Rw" name="CHECK_OPTION" position="5">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FPwcMZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FPwcMpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FPwcM5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FPwcNJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FPwcNZnBEeGqjtSaWHl9Rw" value="CHECK_OPTION"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FP1UsJnBEeGqjtSaWHl9Rw" name="IS_UPDATABLE" position="6">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FP1UsZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FP1UspnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FP1Us5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FP1UtJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FP1UtZnBEeGqjtSaWHl9Rw" value="IS_UPDATABLE"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FP5mIJnBEeGqjtSaWHl9Rw" name="STATUS" position="7">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FP5mIZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FP5mIpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FP5mI5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FP5mJJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FP5mJZnBEeGqjtSaWHl9Rw" value="STATUS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FP-eoJnBEeGqjtSaWHl9Rw" name="REMARKS" position="8">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FP-eoZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FP-eopnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FP-eo5nBEeGqjtSaWHl9Rw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FP-epJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FP-epZnBEeGqjtSaWHl9Rw" value="REMARKS"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FQCwEJnBEeGqjtSaWHl9Rw" name="ID" position="9">
        <attribute defType="com.stambia.rdbms.column.remarks" id="_FQCwEZnBEeGqjtSaWHl9Rw" value=""/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FQCwEpnBEeGqjtSaWHl9Rw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FQCwE5nBEeGqjtSaWHl9Rw" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FQCwFJnBEeGqjtSaWHl9Rw"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_FQCwFZnBEeGqjtSaWHl9Rw" value="ID"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rpll0DScEeawXPCWZJT9_Q" name="IND_SESSION_FILE_OP_LST">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rpmM4DScEeawXPCWZJT9_Q" value="IND_SESSION_FILE_OP_LST"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rpmz8DScEeawXPCWZJT9_Q" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_rpmz8TScEeawXPCWZJT9_Q" value=""/>
      <node defType="com.stambia.rdbms.column" id="_rpp3QDScEeawXPCWZJT9_Q" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpp3QTScEeawXPCWZJT9_Q" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpp3QjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpp3QzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rpp3RDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpp3RTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpp3RjScEeawXPCWZJT9_Q" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpwk8DScEeawXPCWZJT9_Q" name="SESS_NAME" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpwk8TScEeawXPCWZJT9_Q" value="SESS_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpwk8jScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpwk8zScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rpwk9DScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpwk9TScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpwk9jScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rp2rkDScEeawXPCWZJT9_Q" name="ACT_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rp2rkTScEeawXPCWZJT9_Q" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rp2rkjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rp2rkzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rp2rlDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rp2rlTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rp2rljScEeawXPCWZJT9_Q" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rp7kEDScEeawXPCWZJT9_Q" name="ACT_NAME" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rp7kETScEeawXPCWZJT9_Q" value="ACT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rp7kEjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rp7kEzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rp7kFDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rp7kFTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rp7kFjScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqAckDScEeawXPCWZJT9_Q" name="ACT_ITER" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqAckTScEeawXPCWZJT9_Q" value="ACT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqAckjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqAckzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqAclDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqAclTScEeawXPCWZJT9_Q" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqAcljScEeawXPCWZJT9_Q" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqFVEDScEeawXPCWZJT9_Q" name="FILE_ID" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqFVETScEeawXPCWZJT9_Q" value="FILE_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqFVEjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqFVEzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqFVFDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqFVFTScEeawXPCWZJT9_Q" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqFVFjScEeawXPCWZJT9_Q" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqJmgTScEeawXPCWZJT9_Q" name="FILE_OPERATION" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqJmgjScEeawXPCWZJT9_Q" value="FILE_OPERATION"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqJmgzScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqJmhDScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqJmhTScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqJmhjScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqJmhzScEeawXPCWZJT9_Q" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqPtIDScEeawXPCWZJT9_Q" name="FILE_NAME" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqPtITScEeawXPCWZJT9_Q" value="FILE_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqPtIjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqPtIzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqPtJDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqPtJTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqPtJjScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqWa0DScEeawXPCWZJT9_Q" name="FILE_DIR" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqWa0TScEeawXPCWZJT9_Q" value="FILE_DIR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqWa0jScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqWa0zScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqWa1DScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqWa1TScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqWa1jScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqb6YDScEeawXPCWZJT9_Q" name="FILE_FROM_DIR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqb6YTScEeawXPCWZJT9_Q" value="FILE_FROM_DIR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqb6YjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqb6YzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqb6ZDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqb6ZTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqb6ZjScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqgL0DScEeawXPCWZJT9_Q" name="FILE_FROM_FILE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqgL0TScEeawXPCWZJT9_Q" value="FILE_FROM_FILE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqgL0jScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqgL0zScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqgL1DScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqgL1TScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqgL1jScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqkdQDScEeawXPCWZJT9_Q" name="FILE_TO_DIR" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqkdQTScEeawXPCWZJT9_Q" value="FILE_TO_DIR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqkdQjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqkdQzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqkdRDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqkdRTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqkdRjScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqousDScEeawXPCWZJT9_Q" name="FILE_TO_FILE" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqousTScEeawXPCWZJT9_Q" value="FILE_TO_FILE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqousjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqouszScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqoutDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqoutTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqoutjScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqsZEDScEeawXPCWZJT9_Q" name="FILE_OPERATION_DATE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqsZETScEeawXPCWZJT9_Q" value="FILE_OPERATION_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqsZEjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqsZEzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqsZFDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqsZFTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqsZFjScEeawXPCWZJT9_Q" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqwqgDScEeawXPCWZJT9_Q" name="STATUS" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqwqgTScEeawXPCWZJT9_Q" value="STATUS"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqwqgjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqwqgzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqwqhDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqwqhTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqwqhjScEeawXPCWZJT9_Q" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rq0U4DScEeawXPCWZJT9_Q" name="STATUS_COMMENT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_rq0U4TScEeawXPCWZJT9_Q" value="STATUS_COMMENT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rq0U4jScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rq0U4zScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rq0U5DScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rq0U5TScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rq0U5jScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rq4mUDScEeawXPCWZJT9_Q" name="STATUS_DATE" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_rq4mUTScEeawXPCWZJT9_Q" value="STATUS_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rq4mUjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rq4mUzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rq4mVDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rq4mVTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rq4mVjScEeawXPCWZJT9_Q" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rq83wDScEeawXPCWZJT9_Q" name="USER_COMMENT" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_rq83wTScEeawXPCWZJT9_Q" value="USER_COMMENT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rq83wjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rq83wzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rq83xDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rq83xTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rq83xjScEeawXPCWZJT9_Q" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rrBJMDScEeawXPCWZJT9_Q" name="USER_FLAG" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_rrBJMTScEeawXPCWZJT9_Q" value="USER_FLAG"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rrBJMjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rrBJMzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rrBJNDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rrBJNTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rrBJNjScEeawXPCWZJT9_Q" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rrEzkDScEeawXPCWZJT9_Q" name="USER_DATE" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_rrEzkTScEeawXPCWZJT9_Q" value="USER_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rrEzkjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rrEzkzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rrEzlDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rrEzlTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rrEzljScEeawXPCWZJT9_Q" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rrJsEDScEeawXPCWZJT9_Q" name="FILE_IS_HIDDEN" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_rrJsETScEeawXPCWZJT9_Q" value="FILE_IS_HIDDEN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rrJsEjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rrJsEzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rrJsFDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rrJsFTScEeawXPCWZJT9_Q" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rrJsFjScEeawXPCWZJT9_Q" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rrN9gDScEeawXPCWZJT9_Q" name="FILE_LAST_MODIFIED" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_rrN9gTScEeawXPCWZJT9_Q" value="FILE_LAST_MODIFIED"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rrN9gjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rrN9gzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rrN9hDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rrN9hTScEeawXPCWZJT9_Q" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rrN9hjScEeawXPCWZJT9_Q" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rrS2ADScEeawXPCWZJT9_Q" name="FILE_LAST_MODIFIED_DATE" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_rrS2ATScEeawXPCWZJT9_Q" value="FILE_LAST_MODIFIED_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rrS2AjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rrS2AzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rrS2BDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rrS2BTScEeawXPCWZJT9_Q" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rrS2BjScEeawXPCWZJT9_Q" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rrax0DScEeawXPCWZJT9_Q" name="FILE_CAN_READ" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_rrax0TScEeawXPCWZJT9_Q" value="FILE_CAN_READ"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rrax0jScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rrax0zScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rrax1DScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rrax1TScEeawXPCWZJT9_Q" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rrax1jScEeawXPCWZJT9_Q" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rrecMDScEeawXPCWZJT9_Q" name="FILE_CAN_WRITE" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_rrecMTScEeawXPCWZJT9_Q" value="FILE_CAN_WRITE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rrecMjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rrecMzScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rrecNDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rrecNTScEeawXPCWZJT9_Q" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rrecNjScEeawXPCWZJT9_Q" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rritoDScEeawXPCWZJT9_Q" name="FILE_CAN_EXECUTE" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_rritoTScEeawXPCWZJT9_Q" value="FILE_CAN_EXECUTE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rritojScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rritozScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rritpDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rritpTScEeawXPCWZJT9_Q" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rritpjScEeawXPCWZJT9_Q" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rrmYADScEeawXPCWZJT9_Q" name="FILE_IS_DIRECTORY" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_rrm_EDScEeawXPCWZJT9_Q" value="FILE_IS_DIRECTORY"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rrm_ETScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rrm_EjScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rrm_EzScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rrm_FDScEeawXPCWZJT9_Q" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rrm_FTScEeawXPCWZJT9_Q" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rrqpcDScEeawXPCWZJT9_Q" name="FILE_LENGTH" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_rrqpcTScEeawXPCWZJT9_Q" value="FILE_LENGTH"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rrqpcjScEeawXPCWZJT9_Q" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rrqpczScEeawXPCWZJT9_Q" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rrqpdDScEeawXPCWZJT9_Q" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rrqpdTScEeawXPCWZJT9_Q" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rrqpdjScEeawXPCWZJT9_Q" value="20"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_O2SnUM7JEeehY6P6Iph6GA" name="LOGS">
    <attribute defType="com.stambia.rdbms.schema.name" id="_O3I74M7JEeehY6P6Iph6GA" value="LOGS"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_O3Ji8M7JEeehY6P6Iph6GA" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_O3KKAM7JEeehY6P6Iph6GA" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_O3KKAc7JEeehY6P6Iph6GA" value="I_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.dataStoreFilter" id="_7mQZQF9QEeiUmaJUhdo6Tw" value="%"/>
    <attribute defType="com.stambia.rdbms.schema.disableHierarchy" id="_DJqFQG_kEei3ydrH_V9X7Q" value="true"/>
    <node defType="com.stambia.rdbms.datastore" id="_7e07kV9QEeiUmaJUhdo6Tw" name="STB_LOG_ACTION_S_ACT">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7e1ioF9QEeiUmaJUhdo6Tw" value="STB_LOG_ACTION_S_ACT"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7e1ioV9QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7e1iol9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7e2JsF9QEeiUmaJUhdo6Tw" name="DLV_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7e2JsV9QEeiUmaJUhdo6Tw" value="DLV_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7e2Jsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7e2Js19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7e2JtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7e2JtV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7e2Jtl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7e50EF9QEeiUmaJUhdo6Tw" name="ACT_ID" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7e50EV9QEeiUmaJUhdo6Tw" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7e50El9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7e50E19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7e50FF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7e50FV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7e50Fl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7e9ecF9QEeiUmaJUhdo6Tw" name="ACT_PARENT_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7e9ecV9QEeiUmaJUhdo6Tw" value="ACT_PARENT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7e9ecl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7e9ec19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7e9edF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7e9edV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7e9edl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fAhwF9QEeiUmaJUhdo6Tw" name="ACT_TECH" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fAhwV9QEeiUmaJUhdo6Tw" value="ACT_TECH"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fAhwl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fAhw19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fAhxF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fAhxV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fAhxl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fEMIF9QEeiUmaJUhdo6Tw" name="ACT_PARENT_PATH" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fEMIV9QEeiUmaJUhdo6Tw" value="ACT_PARENT_PATH"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fEMIl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fEMI19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fEMJF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fEMJV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fEMJl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fH2gF9QEeiUmaJUhdo6Tw" name="ACT_SHORT_NAME" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fH2gV9QEeiUmaJUhdo6Tw" value="ACT_SHORT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fH2gl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fH2g19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fH2hF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fH2hV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fH2hl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fMH8F9QEeiUmaJUhdo6Tw" name="ACT_TYPE" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fMH8V9QEeiUmaJUhdo6Tw" value="ACT_TYPE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fMH8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fMH819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fMH9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fMH9V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fMH9l9QEeiUmaJUhdo6Tw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fRAcF9QEeiUmaJUhdo6Tw" name="ACT_ERROR_ACCEPTED" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fRAcV9QEeiUmaJUhdo6Tw" value="ACT_ERROR_ACCEPTED"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fRAcl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fRAc19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fRAdF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fRAdV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fRAdl9QEeiUmaJUhdo6Tw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fVR4F9QEeiUmaJUhdo6Tw" name="ACT_IS_BEGIN" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fVR4V9QEeiUmaJUhdo6Tw" value="ACT_IS_BEGIN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fVR4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fVR419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fVR5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fVR5V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fVR5l9QEeiUmaJUhdo6Tw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fY8QF9QEeiUmaJUhdo6Tw" name="ACT_NB_CYCLE" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fY8QV9QEeiUmaJUhdo6Tw" value="ACT_NB_CYCLE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fY8Ql9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fY8Q19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fY8RF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fY8RV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fY8Rl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fb_kF9QEeiUmaJUhdo6Tw" name="ACT_RESTART_MODE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fb_kV9QEeiUmaJUhdo6Tw" value="ACT_RESTART_MODE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fb_kl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fcmoF9QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fcmoV9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fcmol9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fcmo19QEeiUmaJUhdo6Tw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ffp8F9QEeiUmaJUhdo6Tw" name="ACT_HAS_DYN_NAME" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ffp8V9QEeiUmaJUhdo6Tw" value="ACT_HAS_DYN_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ffp8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ffp819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ffp9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ffp9V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ffp9l9QEeiUmaJUhdo6Tw" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7jkRoV9QEeiUmaJUhdo6Tw" name="STB_LOG_DELIVERY_DLV">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7jkRol9QEeiUmaJUhdo6Tw" value="STB_LOG_DELIVERY_DLV"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7jkRo19QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7jkRpF9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7jk4sF9QEeiUmaJUhdo6Tw" name="DLV_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jk4sV9QEeiUmaJUhdo6Tw" value="DLV_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jk4sl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jk4s19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jk4tF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jk4tV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jk4tl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jojEF9QEeiUmaJUhdo6Tw" name="DLV_CLO" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jojEV9QEeiUmaJUhdo6Tw" value="DLV_CLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jojEl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jojE19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jojFF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jojFV9QEeiUmaJUhdo6Tw" value="CLOB"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jsNcF9QEeiUmaJUhdo6Tw" name="DLV_BLO" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jsNcV9QEeiUmaJUhdo6Tw" value="DLV_BLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7js0gF9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7js0gV9QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7js0gl9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7js0g19QEeiUmaJUhdo6Tw" value="BLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7js0hF9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jv30F9QEeiUmaJUhdo6Tw" name="DLV_FORMAT" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jv30V9QEeiUmaJUhdo6Tw" value="DLV_FORMAT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jv30l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jv3019QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jv31F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jv31V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jv31l9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jziMF9QEeiUmaJUhdo6Tw" name="DLV_NAME" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jziMV9QEeiUmaJUhdo6Tw" value="DLV_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jziMl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jziM19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jziNF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jziNV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jziNl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7j3MkF9QEeiUmaJUhdo6Tw" name="PROC_ID" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7j3MkV9QEeiUmaJUhdo6Tw" value="PROC_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7j3Mkl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7j3Mk19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7j3MlF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7j3MlV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7j3Mll9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7j6P4F9QEeiUmaJUhdo6Tw" name="DLV_CONF" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7j6P4V9QEeiUmaJUhdo6Tw" value="DLV_CONF"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7j628F9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7j628V9QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7j628l9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7j62819QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7j629F9QEeiUmaJUhdo6Tw" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7j-hUF9QEeiUmaJUhdo6Tw" name="DLV_TSTAMP" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7j-hUV9QEeiUmaJUhdo6Tw" value="DLV_TSTAMP"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7j-hUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7j-hU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7j-hVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7j-hVV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7j-hVl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kCLsF9QEeiUmaJUhdo6Tw" name="DLV_VERSION" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kCLsV9QEeiUmaJUhdo6Tw" value="DLV_VERSION"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kCLsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kCLs19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kCLtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kCLtV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kCLtl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kF2EF9QEeiUmaJUhdo6Tw" name="DLV_USER" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kF2EV9QEeiUmaJUhdo6Tw" value="DLV_USER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kF2El9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kF2E19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kF2FF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kF2FV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kF2Fl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kJgcF9QEeiUmaJUhdo6Tw" name="DLV_COMMENT" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kJgcV9QEeiUmaJUhdo6Tw" value="DLV_COMMENT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kJgcl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kJgc19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kJgdF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kJgdV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kJgdl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kMjwF9QEeiUmaJUhdo6Tw" name="PCK_ID" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kMjwV9QEeiUmaJUhdo6Tw" value="PCK_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kMjwl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kMjw19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kMjxF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kMjxV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kMjxl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7flJgV9QEeiUmaJUhdo6Tw" name="STB_LOG_LINK_LNK">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7flJgl9QEeiUmaJUhdo6Tw" value="STB_LOG_LINK_LNK"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7flJg19QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7flJhF9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7flwkF9QEeiUmaJUhdo6Tw" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7flwkV9QEeiUmaJUhdo6Tw" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7flwkl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7flwk19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7flwlF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7flwlV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7flwll9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fpa8F9QEeiUmaJUhdo6Tw" name="SESS_ITER" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fpa8V9QEeiUmaJUhdo6Tw" value="SESS_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fpa8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fpa819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fpa9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fpa9V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fpa9l9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ftFUF9QEeiUmaJUhdo6Tw" name="LNK_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ftFUV9QEeiUmaJUhdo6Tw" value="LNK_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ftFUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ftFU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ftFVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ftFVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ftFVl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fwIoF9QEeiUmaJUhdo6Tw" name="LNK_ITER" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fwIoV9QEeiUmaJUhdo6Tw" value="LNK_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fwIol9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fwIo19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fwIpF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fwIpV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fwIpl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7fzL8F9QEeiUmaJUhdo6Tw" name="LNK_DATE" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7fzL8V9QEeiUmaJUhdo6Tw" value="LNK_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7fzL8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7fzL819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7fzL9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7fzL9V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7fzL9l9QEeiUmaJUhdo6Tw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7f22UF9QEeiUmaJUhdo6Tw" name="LNK_TYPE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7f22UV9QEeiUmaJUhdo6Tw" value="LNK_TYPE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7f22Ul9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7f22U19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7f22VF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7f22VV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7f22Vl9QEeiUmaJUhdo6Tw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7f55oF9QEeiUmaJUhdo6Tw" name="LNK_MANDATORY" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7f55oV9QEeiUmaJUhdo6Tw" value="LNK_MANDATORY"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7f55ol9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7f55o19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7f55pF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7f55pV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7f55pl9QEeiUmaJUhdo6Tw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7f9kAF9QEeiUmaJUhdo6Tw" name="LNK_STATUS" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7f9kAV9QEeiUmaJUhdo6Tw" value="LNK_STATUS"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7f9kAl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7f9kA19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7f9kBF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7f9kBV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7f9kBl9QEeiUmaJUhdo6Tw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gBOYF9QEeiUmaJUhdo6Tw" name="LNK_EXE_VAR" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gBOYV9QEeiUmaJUhdo6Tw" value="LNK_EXE_VAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gBOYl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gBOY19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gBOZF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gBOZV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gBOZl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gE4wF9QEeiUmaJUhdo6Tw" name="LNK_EXE_CLO" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gE4wV9QEeiUmaJUhdo6Tw" value="LNK_EXE_CLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gE4wl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gE4w19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gE4xF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gE4xV9QEeiUmaJUhdo6Tw" value="CLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gE4xl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gIjIF9QEeiUmaJUhdo6Tw" name="LNK_EXE_BLO" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gIjIV9QEeiUmaJUhdo6Tw" value="LNK_EXE_BLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gIjIl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gIjI19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gIjJF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gIjJV9QEeiUmaJUhdo6Tw" value="BLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gIjJl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gK_YF9QEeiUmaJUhdo6Tw" name="LNK_EXE_FORMAT" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gK_YV9QEeiUmaJUhdo6Tw" value="LNK_EXE_FORMAT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gK_Yl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gK_Y19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gK_ZF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gK_ZV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gK_Zl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gOpwF9QEeiUmaJUhdo6Tw" name="LNK_SRC_VAR" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gOpwV9QEeiUmaJUhdo6Tw" value="LNK_SRC_VAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gOpwl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gOpw19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gOpxF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gOpxV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gOpxl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gSUIF9QEeiUmaJUhdo6Tw" name="LNK_SRC_CLO" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gSUIV9QEeiUmaJUhdo6Tw" value="LNK_SRC_CLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gSUIl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gSUI19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gSUJF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gSUJV9QEeiUmaJUhdo6Tw" value="CLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gSUJl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gVXcF9QEeiUmaJUhdo6Tw" name="LNK_SRC_BLO" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gVXcV9QEeiUmaJUhdo6Tw" value="LNK_SRC_BLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gVXcl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gVXc19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gVXdF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gVXdV9QEeiUmaJUhdo6Tw" value="BLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gVXdl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gYawF9QEeiUmaJUhdo6Tw" name="LNK_SRC_FORMAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gYawV9QEeiUmaJUhdo6Tw" value="LNK_SRC_FORMAT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gYawl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gYaw19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gYaxF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gYaxV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gYaxl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7i_C0F9QEeiUmaJUhdo6Tw" name="STB_LOG_ACTION_STAT_AST">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7i_C0V9QEeiUmaJUhdo6Tw" value="STB_LOG_ACTION_STAT_AST"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7i_C0l9QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7i_C019QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7i_p4F9QEeiUmaJUhdo6Tw" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7i_p4V9QEeiUmaJUhdo6Tw" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7i_p4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7i_p419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7i_p5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7i_p5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7i_p5l9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jDUQF9QEeiUmaJUhdo6Tw" name="SESS_ITER" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jDUQV9QEeiUmaJUhdo6Tw" value="SESS_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jD7UF9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jD7UV9QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jD7Ul9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jD7U19QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jD7VF9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jHlsF9QEeiUmaJUhdo6Tw" name="ACT_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jHlsV9QEeiUmaJUhdo6Tw" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jHlsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jHls19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jHltF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jHltV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jHltl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jMeMF9QEeiUmaJUhdo6Tw" name="ACT_ITER" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jMeMV9QEeiUmaJUhdo6Tw" value="ACT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jMeMl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jMeM19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jMeNF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jMeNV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jMeNl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jR9wF9QEeiUmaJUhdo6Tw" name="ACP_SHORT_NAME" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jR9wV9QEeiUmaJUhdo6Tw" value="ACP_SHORT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jR9wl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jR9w19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jR9xF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jR9xV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jR9xl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jXdUF9QEeiUmaJUhdo6Tw" name="AST_STAT_SUM" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jXdUV9QEeiUmaJUhdo6Tw" value="AST_STAT_SUM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jXdUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jXdU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jXdVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jXdVV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jXdVl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jbHsF9QEeiUmaJUhdo6Tw" name="AST_STAT_REJ" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jbHsV9QEeiUmaJUhdo6Tw" value="AST_STAT_REJ"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jbHsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jbHs19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jbHtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jbHtV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jbHtl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jeyEF9QEeiUmaJUhdo6Tw" name="AST_NUM" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jeyEV9QEeiUmaJUhdo6Tw" value="AST_NUM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jeyEl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jeyE19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jeyFF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jeyFV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jeyFl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7bQDIF9QEeiUmaJUhdo6Tw" name="STB_LOG_SESSION_CHILDS_CSES">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7bQqMF9QEeiUmaJUhdo6Tw" value="STB_LOG_SESSION_CHILDS_CSES"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7bQqMV9QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7bQqMl9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7bRRQF9QEeiUmaJUhdo6Tw" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7bRRQV9QEeiUmaJUhdo6Tw" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7bRRQl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7bRRQ19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7bRRRF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7bRRRV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7bRRRl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7bXX4F9QEeiUmaJUhdo6Tw" name="SESS_ITER" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7bXX4V9QEeiUmaJUhdo6Tw" value="SESS_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7bXX4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7bXX419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7bXX5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7bXX5V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7bXX5l9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7bbpUF9QEeiUmaJUhdo6Tw" name="CSES_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7bbpUV9QEeiUmaJUhdo6Tw" value="CSES_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7bbpUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7bbpU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7bbpVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7bbpVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7bbpVl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7bhI4F9QEeiUmaJUhdo6Tw" name="ACT_ID" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7bhI4V9QEeiUmaJUhdo6Tw" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7bhI4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7bhI419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7bhI5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7bhI5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7bhI5l9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7bkzQF9QEeiUmaJUhdo6Tw" name="ACT_ITER" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7bkzQV9QEeiUmaJUhdo6Tw" value="ACT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7bkzQl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7bkzQ19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7bkzRF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7bkzRV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7bkzRl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7bodoF9QEeiUmaJUhdo6Tw" name="CSES_ITER" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7bodoV9QEeiUmaJUhdo6Tw" value="CSES_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7bodol9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7bodo19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7bodpF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7bodpV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7bodpl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7hnJ0V9QEeiUmaJUhdo6Tw" name="STB_LOG_SESSION_SESS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7hnJ0l9QEeiUmaJUhdo6Tw" value="STB_LOG_SESSION_SESS"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7hnJ019QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7hnJ1F9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7hnw4F9QEeiUmaJUhdo6Tw" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hnw4V9QEeiUmaJUhdo6Tw" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hnw4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hnw419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hnw5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hnw5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hnw5l9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hrbQF9QEeiUmaJUhdo6Tw" name="SESS_NAME" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hrbQV9QEeiUmaJUhdo6Tw" value="SESS_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hrbQl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hrbQ19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hrbRF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hrbRV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hrbRl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7huekF9QEeiUmaJUhdo6Tw" name="SESS_ITER" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7huekV9QEeiUmaJUhdo6Tw" value="SESS_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7huekl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7huek19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7huelF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7huelV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7huell9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hxh4F9QEeiUmaJUhdo6Tw" name="SESS_BEGIN_DATE" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hxh4V9QEeiUmaJUhdo6Tw" value="SESS_BEGIN_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hxh4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hxh419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hxh5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hxh5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hxh5l9QEeiUmaJUhdo6Tw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7h1MQF9QEeiUmaJUhdo6Tw" name="SESS_END_DATE" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7h1MQV9QEeiUmaJUhdo6Tw" value="SESS_END_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7h1MQl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7h1MQ19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7h1MRF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7h1MRV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7h1MRl9QEeiUmaJUhdo6Tw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7h42oF9QEeiUmaJUhdo6Tw" name="SESS_RET_CODE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7h42oV9QEeiUmaJUhdo6Tw" value="SESS_RET_CODE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7h42ol9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7h42o19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7h42pF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7h42pV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7h42pl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7h758F9QEeiUmaJUhdo6Tw" name="SESS_RET_MSG" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7h758V9QEeiUmaJUhdo6Tw" value="SESS_RET_MSG"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7h758l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7h75819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7h759F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7h759V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7h759l9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7h_kUF9QEeiUmaJUhdo6Tw" name="SESS_ENGINE_HOST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7h_kUV9QEeiUmaJUhdo6Tw" value="SESS_ENGINE_HOST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7h_kUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7h_kU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7h_kVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7h_kVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7h_kVl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iCnoF9QEeiUmaJUhdo6Tw" name="SESS_ENGINE_PORT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iCnoV9QEeiUmaJUhdo6Tw" value="SESS_ENGINE_PORT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iCnol9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iCno19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iCnpF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iCnpV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iCnpl9QEeiUmaJUhdo6Tw" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iGSAF9QEeiUmaJUhdo6Tw" name="DLV_ID" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iGSAV9QEeiUmaJUhdo6Tw" value="DLV_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iGSAl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iGSA19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iGSBF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iGSBV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iGSBl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iJVUF9QEeiUmaJUhdo6Tw" name="SESS_LAUNCH_MODE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iJVUV9QEeiUmaJUhdo6Tw" value="SESS_LAUNCH_MODE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iJVUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iJVU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iJVVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iJVVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iJVVl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iM_sF9QEeiUmaJUhdo6Tw" name="SESS_EXECUTION_MODE" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iM_sV9QEeiUmaJUhdo6Tw" value="SESS_EXECUTION_MODE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iM_sl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iM_s19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iM_tF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iM_tV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iM_tl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iPb8F9QEeiUmaJUhdo6Tw" name="SESS_GUEST_HOST" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iPb8V9QEeiUmaJUhdo6Tw" value="SESS_GUEST_HOST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iPb8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iPb819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iPb9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iPb9V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iPb9l9QEeiUmaJUhdo6Tw" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iTGUF9QEeiUmaJUhdo6Tw" name="SESS_CONF" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iTGUV9QEeiUmaJUhdo6Tw" value="SESS_CONF"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iTGUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iTGU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iTGVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iTGVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iTGVl9QEeiUmaJUhdo6Tw" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iWwsF9QEeiUmaJUhdo6Tw" name="SESS_PARENT_ID" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iWwsV9QEeiUmaJUhdo6Tw" value="SESS_PARENT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iWwsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iWws19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iWwtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iWwtV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iWwtl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iZ0AF9QEeiUmaJUhdo6Tw" name="V_VERSION" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iZ0AV9QEeiUmaJUhdo6Tw" value="V_VERSION"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iZ0Al9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iZ0A19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iZ0BF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iZ0BV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iZ0Bl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ideYF9QEeiUmaJUhdo6Tw" name="SESS_PARENT_ITER" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ideYV9QEeiUmaJUhdo6Tw" value="SESS_PARENT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ideYl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ideY19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ideZF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ideZV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ideZl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ihIwF9QEeiUmaJUhdo6Tw" name="SESS_ACT_ROOT_ID" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ihIwV9QEeiUmaJUhdo6Tw" value="SESS_ACT_ROOT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ihIwl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ihIw19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ihIxF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ihIxV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ihIxl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ikzIF9QEeiUmaJUhdo6Tw" name="SESS_BEGIN_TSTAMP" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ikzIV9QEeiUmaJUhdo6Tw" value="SESS_BEGIN_TSTAMP"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ikzIl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ikzI19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ikzJF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ikzJV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ikzJl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7in2cF9QEeiUmaJUhdo6Tw" name="SESS_DURATION" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_7in2cV9QEeiUmaJUhdo6Tw" value="SESS_DURATION"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7in2cl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7in2c19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7in2dF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7in2dV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7in2dl9QEeiUmaJUhdo6Tw" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7irg0F9QEeiUmaJUhdo6Tw" name="SESS_TSTAMP_OFFSET" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_7irg0V9QEeiUmaJUhdo6Tw" value="SESS_TSTAMP_OFFSET"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7irg0l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7irg019QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7irg1F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7irg1V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7irg1l9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iukIF9QEeiUmaJUhdo6Tw" name="SESS_LAST_TSTAMP" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iukIV9QEeiUmaJUhdo6Tw" value="SESS_LAST_TSTAMP"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iukIl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iukI19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iukJF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iukJV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iukJl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7iy1kF9QEeiUmaJUhdo6Tw" name="SESS_INACT_TIMEOUT" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_7iy1kV9QEeiUmaJUhdo6Tw" value="SESS_INACT_TIMEOUT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7iy1kl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7iy1k19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7iy1lF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7iy1lV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7iy1ll9QEeiUmaJUhdo6Tw" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7i3uEF9QEeiUmaJUhdo6Tw" name="SESS_LAUNCH_USER" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_7i3uEV9QEeiUmaJUhdo6Tw" value="SESS_LAUNCH_USER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7i3uEl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7i3uE19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7i3uFF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7i3uFV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7i3uFl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7de3wV9QEeiUmaJUhdo6Tw" name="STB_LOG_ACTION_PROP_ACP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7de3wl9QEeiUmaJUhdo6Tw" value="STB_LOG_ACTION_PROP_ACP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7de3w19QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7de3xF9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7dfe0F9QEeiUmaJUhdo6Tw" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dfe0V9QEeiUmaJUhdo6Tw" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dfe0l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dfe019QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dfe1F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dfe1V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dfe1l9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7djJMF9QEeiUmaJUhdo6Tw" name="SESS_ITER" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7djJMV9QEeiUmaJUhdo6Tw" value="SESS_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7djJMl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7djJM19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7djJNF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7djJNV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7djJNl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dmzkF9QEeiUmaJUhdo6Tw" name="ACT_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dmzkV9QEeiUmaJUhdo6Tw" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dmzkl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dmzk19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dmzlF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dmzlV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dmzll9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dqd8F9QEeiUmaJUhdo6Tw" name="ACT_ITER" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dqd8V9QEeiUmaJUhdo6Tw" value="ACT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dqd8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dqd819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dqd9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dqd9V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dqd9l9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dthQF9QEeiUmaJUhdo6Tw" name="ACP_NAME" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dthQV9QEeiUmaJUhdo6Tw" value="ACP_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dthQl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dthQ19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dthRF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dthRV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dthRl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dxLoF9QEeiUmaJUhdo6Tw" name="ACP_SHORT_NAME" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dxLoV9QEeiUmaJUhdo6Tw" value="ACP_SHORT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dxLol9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dxLo19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dxLpF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dxLpV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dxLpl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dzn4F9QEeiUmaJUhdo6Tw" name="ACP_TYPE" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dzn4V9QEeiUmaJUhdo6Tw" value="ACP_TYPE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dzn4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dzn419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dzn5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dzn5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dzn5l9QEeiUmaJUhdo6Tw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7d3SQF9QEeiUmaJUhdo6Tw" name="ACP_CUMUL" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7d3SQV9QEeiUmaJUhdo6Tw" value="ACP_CUMUL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7d3SQl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7d3SQ19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7d3SRF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7d3SRV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7d3SRl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7d68oF9QEeiUmaJUhdo6Tw" name="ACP_EXE_VAR" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_7d68oV9QEeiUmaJUhdo6Tw" value="ACP_EXE_VAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7d68ol9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7d68o19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7d68pF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7d68pV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7d68pl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7eBqUF9QEeiUmaJUhdo6Tw" name="ACP_EXE_CLO" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_7eBqUV9QEeiUmaJUhdo6Tw" value="ACP_EXE_CLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7eBqUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7eBqU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7eBqVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7eBqVV9QEeiUmaJUhdo6Tw" value="CLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7eBqVl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7eFUsF9QEeiUmaJUhdo6Tw" name="ACP_EXE_BLO" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_7eFUsV9QEeiUmaJUhdo6Tw" value="ACP_EXE_BLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7eFUsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7eFUs19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7eFUtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7eFUtV9QEeiUmaJUhdo6Tw" value="BLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7eFUtl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7eI_EF9QEeiUmaJUhdo6Tw" name="ACP_EXE_FORMAT" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_7eI_EV9QEeiUmaJUhdo6Tw" value="ACP_EXE_FORMAT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7eI_El9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7eI_E19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7eI_FF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7eI_FV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7eI_Fl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7eLbUF9QEeiUmaJUhdo6Tw" name="ACP_SRC_VAR" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_7eLbUV9QEeiUmaJUhdo6Tw" value="ACP_SRC_VAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7eLbUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7eLbU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7eLbVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7eLbVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7eLbVl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ePFsF9QEeiUmaJUhdo6Tw" name="ACP_SRC_CLO" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ePFsV9QEeiUmaJUhdo6Tw" value="ACP_SRC_CLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ePFsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ePFs19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ePFtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ePFtV9QEeiUmaJUhdo6Tw" value="CLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ePFtl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7eTXIF9QEeiUmaJUhdo6Tw" name="ACP_SRC_BLO" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_7eTXIV9QEeiUmaJUhdo6Tw" value="ACP_SRC_BLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7eTXIl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7eTXI19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7eTXJF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7eTXJV9QEeiUmaJUhdo6Tw" value="BLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7eTXJl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7eXBgF9QEeiUmaJUhdo6Tw" name="ACP_SRC_FORMAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_7eXBgV9QEeiUmaJUhdo6Tw" value="ACP_SRC_FORMAT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7eXBgl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7eXBg19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7eXBhF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7eXBhV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7eXBhl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ear4F9QEeiUmaJUhdo6Tw" name="PTY_TYPE_N" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ear4V9QEeiUmaJUhdo6Tw" value="PTY_TYPE_N"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ear4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ear419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ear5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ear5V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ear5l9QEeiUmaJUhdo6Tw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7edvMF9QEeiUmaJUhdo6Tw" name="PCA_TYPE_N" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_7edvMV9QEeiUmaJUhdo6Tw" value="PCA_TYPE_N"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7edvMl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7edvM19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7edvNF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7edvNV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7edvNl9QEeiUmaJUhdo6Tw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ehZkF9QEeiUmaJUhdo6Tw" name="ACP_NUM" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ehZkV9QEeiUmaJUhdo6Tw" value="ACP_NUM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ehZkl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ehZk19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ehZlF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ehZlV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ehZll9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ekc4F9QEeiUmaJUhdo6Tw" name="ACP_BND_VAR" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ekc4V9QEeiUmaJUhdo6Tw" value="ACP_BND_VAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ekc4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ekc419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ekc5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ekc5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ekc5l9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7eoHQF9QEeiUmaJUhdo6Tw" name="ACP_BND_CLO" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_7eoHQV9QEeiUmaJUhdo6Tw" value="ACP_BND_CLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7eoHQl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7eoHQ19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7eoHRF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7eoHRV9QEeiUmaJUhdo6Tw" value="CLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7eoHRl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7erxoF9QEeiUmaJUhdo6Tw" name="ACP_BND_BLO" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_7erxoV9QEeiUmaJUhdo6Tw" value="ACP_BND_BLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7erxol9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7erxo19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7erxpF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7erxpV9QEeiUmaJUhdo6Tw" value="CLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7erxpl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7evcAF9QEeiUmaJUhdo6Tw" name="ACP_BND_FORMAT" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_7evcAV9QEeiUmaJUhdo6Tw" value="ACP_BND_FORMAT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7evcAl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7evcA19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7evcBF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7evcBV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7evcBl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7lfkQV9QEeiUmaJUhdo6Tw" name="STB_LOG_VERSION_HIST_VHS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7lfkQl9QEeiUmaJUhdo6Tw" value="STB_LOG_VERSION_HIST_VHS"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7lfkQ19QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7lfkRF9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7lgLUF9QEeiUmaJUhdo6Tw" name="V_VERSION" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lgLUV9QEeiUmaJUhdo6Tw" value="V_VERSION"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lgLUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lgLU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lgLVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lgLVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lgLVl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lj1sF9QEeiUmaJUhdo6Tw" name="VHS_NUM" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lj1sV9QEeiUmaJUhdo6Tw" value="VHS_NUM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lj1sl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lj1s19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lj1tF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lj1tV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lj1tl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lm5AF9QEeiUmaJUhdo6Tw" name="VHS_SQL" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lm5AV9QEeiUmaJUhdo6Tw" value="VHS_SQL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lm5Al9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lm5A19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lm5BF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lm5BV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lm5Bl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lqjYF9QEeiUmaJUhdo6Tw" name="VHS_USER_SQL" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lqjYV9QEeiUmaJUhdo6Tw" value="VHS_USER_SQL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lqjYl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lqjY19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lqjZF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lqjZV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lqjZl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ltmsF9QEeiUmaJUhdo6Tw" name="VHS_STK_TRACE" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ltmsV9QEeiUmaJUhdo6Tw" value="VHS_STK_TRACE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ltmsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ltms19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ltmtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ltmtV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ltmtl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lxREF9QEeiUmaJUhdo6Tw" name="VHS_UPD_DATE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lxREV9QEeiUmaJUhdo6Tw" value="VHS_UPD_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lxREl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lxRE19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lxRFF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lxRFV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lxRFl9QEeiUmaJUhdo6Tw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7l07cF9QEeiUmaJUhdo6Tw" name="VHS_STATUS" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7l07cV9QEeiUmaJUhdo6Tw" value="VHS_STATUS"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7l07cl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7l07c19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7l07dF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7l07dV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7l07dl9QEeiUmaJUhdo6Tw" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7c_vkF9QEeiUmaJUhdo6Tw" name="STB_LOG_ACTION_SPROP_ASP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7c_vkV9QEeiUmaJUhdo6Tw" value="STB_LOG_ACTION_SPROP_ASP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7c_vkl9QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7c_vk19QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7c_vlF9QEeiUmaJUhdo6Tw" name="DLV_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7c_vlV9QEeiUmaJUhdo6Tw" value="DLV_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7c_vll9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7c_vl19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7c_vmF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7c_vmV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7c_vml9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dEoEF9QEeiUmaJUhdo6Tw" name="ACT_ID" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dEoEV9QEeiUmaJUhdo6Tw" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dEoEl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dEoE19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dEoFF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dEoFV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dEoFl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dI5gF9QEeiUmaJUhdo6Tw" name="ACP_SHORT_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dI5gV9QEeiUmaJUhdo6Tw" value="ACP_SHORT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dI5gl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dI5g19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dI5hF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dI5hV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dI5hl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dMj4F9QEeiUmaJUhdo6Tw" name="ASP_SRC_VAR" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dMj4V9QEeiUmaJUhdo6Tw" value="ASP_SRC_VAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dMj4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dMj419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dMj5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dMj5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dMj5l9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dPnMF9QEeiUmaJUhdo6Tw" name="ASP_SRC_CLO" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dPnMV9QEeiUmaJUhdo6Tw" value="ASP_SRC_CLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dPnMl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dPnM19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dPnNF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dPnNV9QEeiUmaJUhdo6Tw" value="CLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dPnNl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dTRkF9QEeiUmaJUhdo6Tw" name="ASP_SRC_BLO" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dTRkV9QEeiUmaJUhdo6Tw" value="ASP_SRC_BLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dTRkl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dTRk19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dTRlF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dTRlV9QEeiUmaJUhdo6Tw" value="BLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dTRll9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dWU4F9QEeiUmaJUhdo6Tw" name="ASP_SRC_FORMAT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dWU4V9QEeiUmaJUhdo6Tw" value="ASP_SRC_FORMAT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dWU4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dWU419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dWU5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dWU5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dWU5l9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7dZ_QF9QEeiUmaJUhdo6Tw" name="PTY_TYPE_N" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7dZ_QV9QEeiUmaJUhdo6Tw" value="PTY_TYPE_N"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7dZ_Ql9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7dZ_Q19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7dZ_RF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7dZ_RV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7dZ_Rl9QEeiUmaJUhdo6Tw" value="2"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7bukQV9QEeiUmaJUhdo6Tw" name="STB_LOG_ACTION_ALTID_AAI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7bukQl9QEeiUmaJUhdo6Tw" value="STB_LOG_ACTION_ALTID_AAI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7bukQ19QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7bukRF9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7bvLUF9QEeiUmaJUhdo6Tw" name="DLV_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7bvLUV9QEeiUmaJUhdo6Tw" value="DLV_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7bvLUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7bvLU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7bvLVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7bvLVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7bvLVl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7by1sF9QEeiUmaJUhdo6Tw" name="ACT_ID" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7by1sV9QEeiUmaJUhdo6Tw" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7by1sl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7by1s19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7by1tF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7by1tV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7by1tl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7b2gEF9QEeiUmaJUhdo6Tw" name="AAI_ORIGIN" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7b2gEV9QEeiUmaJUhdo6Tw" value="AAI_ORIGIN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7b2gEl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7b2gE19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7b2gFF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7b2gFV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7b2gFl9QEeiUmaJUhdo6Tw" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7b6KcF9QEeiUmaJUhdo6Tw" name="AAI_ID" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7b6KcV9QEeiUmaJUhdo6Tw" value="AAI_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7b6Kcl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7b6Kc19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7b6KdF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7b6KdV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7b6Kdl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7gd6UV9QEeiUmaJUhdo6Tw" name="STB_LOG_SESSION_STAT_SST">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7gd6Ul9QEeiUmaJUhdo6Tw" value="STB_LOG_SESSION_STAT_SST"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7gd6U19QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7gd6VF9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7gehYF9QEeiUmaJUhdo6Tw" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gehYV9QEeiUmaJUhdo6Tw" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gehYl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gehY19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gehZF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gehZV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gehZl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7ghksF9QEeiUmaJUhdo6Tw" name="SESS_ITER" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7ghksV9QEeiUmaJUhdo6Tw" value="SESS_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7ghksl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7ghks19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7ghktF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7ghktV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7ghktl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gkoAF9QEeiUmaJUhdo6Tw" name="ACP_SHORT_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gkoAV9QEeiUmaJUhdo6Tw" value="ACP_SHORT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gkoAl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gkoA19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gkoBF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gkoBV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gkoBl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7goSYF9QEeiUmaJUhdo6Tw" name="SST_STAT_SUM" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7goSYV9QEeiUmaJUhdo6Tw" value="SST_STAT_SUM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7goSYl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7goSY19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7goSZF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7goSZV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7goSZl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7grVsF9QEeiUmaJUhdo6Tw" name="SST_STAT_REJ" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7grVsV9QEeiUmaJUhdo6Tw" value="SST_STAT_REJ"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gr8wF9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gr8wV9QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gr8wl9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gr8w19QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gr8xF9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gvAEF9QEeiUmaJUhdo6Tw" name="SST_STAT_MIN" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gvAEV9QEeiUmaJUhdo6Tw" value="SST_STAT_MIN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gvAEl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gvAE19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gvAFF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gvAFV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gvAFl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7gyqcF9QEeiUmaJUhdo6Tw" name="SST_STAT_MAX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7gyqcV9QEeiUmaJUhdo6Tw" value="SST_STAT_MAX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7gyqcl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7gyqc19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7gyqdF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7gyqdV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7gyqdl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7g1twF9QEeiUmaJUhdo6Tw" name="SST_STAT_COUNT" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7g1twV9QEeiUmaJUhdo6Tw" value="SST_STAT_COUNT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7g1twl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7g1tw19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7g1txF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7g1txV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7g1txl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7b_C8V9QEeiUmaJUhdo6Tw" name="STB_LOG_USER_ACTION_LOG_UAL">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7b_qAF9QEeiUmaJUhdo6Tw" value="STB_LOG_USER_ACTION_LOG_UAL"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7b_qAV9QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7b_qAl9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7cAREF9QEeiUmaJUhdo6Tw" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cAREV9QEeiUmaJUhdo6Tw" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cAREl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cARE19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cARFF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cARFV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cARFl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cDUYF9QEeiUmaJUhdo6Tw" name="SESS_ITER" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cDUYV9QEeiUmaJUhdo6Tw" value="SESS_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cDUYl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cDUY19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cDUZF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cDUZV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cDUZl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cG-wF9QEeiUmaJUhdo6Tw" name="UAL_NUM" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cG-wV9QEeiUmaJUhdo6Tw" value="UAL_NUM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cG-wl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cG-w19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cG-xF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cG-xV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cG-xl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cKpIF9QEeiUmaJUhdo6Tw" name="ACT_ID" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cKpIV9QEeiUmaJUhdo6Tw" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cKpIl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cKpI19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cKpJF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cKpJV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cKpJl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cNscF9QEeiUmaJUhdo6Tw" name="ACT_ITER" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cNscV9QEeiUmaJUhdo6Tw" value="ACT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cNscl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cNsc19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cNsdF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cNsdV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cNsdl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cRW0F9QEeiUmaJUhdo6Tw" name="ACT_ID_P" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cRW0V9QEeiUmaJUhdo6Tw" value="ACT_ID_P"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cRW0l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cRW019QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cRW1F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cRW1V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cRW1l9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cUaIF9QEeiUmaJUhdo6Tw" name="ACT_ITER_P" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cUaIV9QEeiUmaJUhdo6Tw" value="ACT_ITER_P"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cVBMF9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cVBMV9QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cVBMl9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cVBM19QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cVBNF9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cYEgF9QEeiUmaJUhdo6Tw" name="ACT_PATH" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cYEgV9QEeiUmaJUhdo6Tw" value="ACT_PATH"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cYEgl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cYEg19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cYEhF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cYEhV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cYEhl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cbu4F9QEeiUmaJUhdo6Tw" name="ACT_PATH_P" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cbu4V9QEeiUmaJUhdo6Tw" value="ACT_PATH_P"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cbu4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cbu419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cbu5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cbu5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cbu5l9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cgAUF9QEeiUmaJUhdo6Tw" name="UAL_LEVEL" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cgAUV9QEeiUmaJUhdo6Tw" value="UAL_LEVEL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cgAUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cgAU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cgAVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cgAVV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cgAVl9QEeiUmaJUhdo6Tw" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cjqsF9QEeiUmaJUhdo6Tw" name="UAL_TSTAMP" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cjqsV9QEeiUmaJUhdo6Tw" value="UAL_TSTAMP"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cjqsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cjqs19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cjqtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cjqtV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cjqtl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cmuAF9QEeiUmaJUhdo6Tw" name="UAL_PATH" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cmuAV9QEeiUmaJUhdo6Tw" value="UAL_PATH"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cmuAl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cmuA19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cmuBF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cmuBV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cmuBl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7csNkF9QEeiUmaJUhdo6Tw" name="UAL_VAR" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_7csNkV9QEeiUmaJUhdo6Tw" value="UAL_VAR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7csNkl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7csNk19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7csNlF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7csNlV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7csNll9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7cwfAF9QEeiUmaJUhdo6Tw" name="UAL_CLO" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_7cwfAV9QEeiUmaJUhdo6Tw" value="UAL_CLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7cwfAl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7cwfA19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7cwfBF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7cwfBV9QEeiUmaJUhdo6Tw" value="CLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7cwfBl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7c0wcF9QEeiUmaJUhdo6Tw" name="UAL_BLO" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_7c0wcV9QEeiUmaJUhdo6Tw" value="UAL_BLO"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7c0wcl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7c0wc19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7c0wdF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7c0wdV9QEeiUmaJUhdo6Tw" value="BLOB"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7c0wdl9QEeiUmaJUhdo6Tw" value="2147483647"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7c5B4F9QEeiUmaJUhdo6Tw" name="UAL_FORMAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_7c5B4V9QEeiUmaJUhdo6Tw" value="UAL_FORMAT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7c5B4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7c5B419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7c5B5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7c5B5V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7c5B5l9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7kSqYF9QEeiUmaJUhdo6Tw" name="STB_LOG_VERSION_V">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7kSqYV9QEeiUmaJUhdo6Tw" value="STB_LOG_VERSION_V"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7kSqYl9QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7kSqY19QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7kTRcF9QEeiUmaJUhdo6Tw" name="V_VERSION" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kTRcV9QEeiUmaJUhdo6Tw" value="V_VERSION"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kTRcl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kTRc19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kTRdF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kTRdV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kTRdl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kWUwF9QEeiUmaJUhdo6Tw" name="V_UPD_DATE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kWUwV9QEeiUmaJUhdo6Tw" value="V_UPD_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kWUwl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kW70F9QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kW70V9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kW70l9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kW7019QEeiUmaJUhdo6Tw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kamMF9QEeiUmaJUhdo6Tw" name="V_TSTAMP_SQL" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kamMV9QEeiUmaJUhdo6Tw" value="V_TSTAMP_SQL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kamMl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kamM19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kamNF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kamNV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kamNl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7kgFwV9QEeiUmaJUhdo6Tw" name="STB_LOG_ACTION_ACT">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7kgFwl9QEeiUmaJUhdo6Tw" value="STB_LOG_ACTION_ACT"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7kgFw19QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7kgFxF9QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7kgs0F9QEeiUmaJUhdo6Tw" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kgs0V9QEeiUmaJUhdo6Tw" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kgs0l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kgs019QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kgs1F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kgs1V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kgs1l9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kkXMF9QEeiUmaJUhdo6Tw" name="SESS_ITER" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kkXMV9QEeiUmaJUhdo6Tw" value="SESS_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kkXMl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kkXM19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kkXNF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kkXNV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kkXNl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7knagF9QEeiUmaJUhdo6Tw" name="ACT_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7knagV9QEeiUmaJUhdo6Tw" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7knagl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7knag19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7knahF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7knahV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7knahl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kqd0F9QEeiUmaJUhdo6Tw" name="ACT_ITER" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kqd0V9QEeiUmaJUhdo6Tw" value="ACT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kqd0l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kqd019QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kqd1F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kqd1V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kqd1l9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kuIMF9QEeiUmaJUhdo6Tw" name="ACT_NAME" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kuIMV9QEeiUmaJUhdo6Tw" value="ACT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kuIMl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kuIM19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kuINF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kuINV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kuINl9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7kxykF9QEeiUmaJUhdo6Tw" name="ACT_TYPE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7kxykV9QEeiUmaJUhdo6Tw" value="ACT_TYPE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7kxykl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7kxyk19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7kxylF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7kxylV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7kxyll9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7k1c8F9QEeiUmaJUhdo6Tw" name="ACT_BEGIN_DATE" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7k1c8V9QEeiUmaJUhdo6Tw" value="ACT_BEGIN_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7k1c8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7k1c819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7k1c9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7k1c9V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7k1c9l9QEeiUmaJUhdo6Tw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7k5HUF9QEeiUmaJUhdo6Tw" name="ACT_END_DATE" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7k5HUV9QEeiUmaJUhdo6Tw" value="ACT_END_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7k5HUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7k5HU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7k5HVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7k5HVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7k5HVl9QEeiUmaJUhdo6Tw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7k8xsF9QEeiUmaJUhdo6Tw" name="ACT_RET_CODE" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_7k8xsV9QEeiUmaJUhdo6Tw" value="ACT_RET_CODE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7k8xsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7k8xs19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7k8xtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7k8xtV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7k8xtl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7k_N8F9QEeiUmaJUhdo6Tw" name="ACT_RET_MSG" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_7k_N8V9QEeiUmaJUhdo6Tw" value="ACT_RET_MSG"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7k_N8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7k_N819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7k_N9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7k_N9V9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7k_N9l9QEeiUmaJUhdo6Tw" value="1000"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lC4UF9QEeiUmaJUhdo6Tw" name="ACT_FATHER_ENGINE_ID" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lC4UV9QEeiUmaJUhdo6Tw" value="ACT_FATHER_ENGINE_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lC4Ul9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lC4U19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lC4VF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lC4VV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lC4Vl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lI-8F9QEeiUmaJUhdo6Tw" name="ACT_PARENT_ITER" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lI-8V9QEeiUmaJUhdo6Tw" value="ACT_PARENT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lI-8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lI-819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lI-9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lI-9V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lI-9l9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lMpUF9QEeiUmaJUhdo6Tw" name="ACT_REAL_NAME" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lMpUV9QEeiUmaJUhdo6Tw" value="ACT_REAL_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lMpUl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lMpU19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lMpVF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lMpVV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lMpVl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lQTsF9QEeiUmaJUhdo6Tw" name="ACT_NB_EXE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lQTsV9QEeiUmaJUhdo6Tw" value="ACT_NB_EXE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lQTsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lQTs19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lQTtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lQTtV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lQTtl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lT-EF9QEeiUmaJUhdo6Tw" name="ACT_NB_BND_EXE" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lT-EV9QEeiUmaJUhdo6Tw" value="ACT_NB_BND_EXE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lT-El9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lT-E19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lT-FF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lT-FV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lT-Fl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7lXBYF9QEeiUmaJUhdo6Tw" name="ACT_IS_BEGIN" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_7lXBYV9QEeiUmaJUhdo6Tw" value="ACT_IS_BEGIN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7lXBYl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7lXBY19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7lXBZF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7lXBZV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7lXBZl9QEeiUmaJUhdo6Tw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7laEsF9QEeiUmaJUhdo6Tw" name="ACT_NUM" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_7laEsV9QEeiUmaJUhdo6Tw" value="ACT_NUM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7laEsl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7laEs19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7laEtF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7laEtV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7laEtl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7g7NUF9QEeiUmaJUhdo6Tw" name="STB_LOG_PROCESS_STAT_PST">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7g7NUV9QEeiUmaJUhdo6Tw" value="STB_LOG_PROCESS_STAT_PST"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7g7NUl9QEeiUmaJUhdo6Tw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_7g7NU19QEeiUmaJUhdo6Tw" value=""/>
      <node defType="com.stambia.rdbms.column" id="_7g7NVF9QEeiUmaJUhdo6Tw" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7g7NVV9QEeiUmaJUhdo6Tw" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7g7NVl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7g7NV19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7g7NWF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7g7NWV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7g7NWl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7g-QoF9QEeiUmaJUhdo6Tw" name="SESS_ITER" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7g-QoV9QEeiUmaJUhdo6Tw" value="SESS_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7g-Qol9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7g-3sF9QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7g-3sV9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7g-3sl9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7g-3s19QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hCiEF9QEeiUmaJUhdo6Tw" name="ACT_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hCiEV9QEeiUmaJUhdo6Tw" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hCiEl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hCiE19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hCiFF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hCiFV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hCiFl9QEeiUmaJUhdo6Tw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hGMcF9QEeiUmaJUhdo6Tw" name="ACT_ITER" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hGMcV9QEeiUmaJUhdo6Tw" value="ACT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hGMcl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hGMc19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hGMdF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hGMdV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hGMdl9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hJPwF9QEeiUmaJUhdo6Tw" name="ACP_SHORT_NAME" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hJPwV9QEeiUmaJUhdo6Tw" value="ACP_SHORT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hJPwl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hJPw19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hJPxF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hJPxV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hJPxl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hM6IF9QEeiUmaJUhdo6Tw" name="PST_STAT_SUM" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hM6IV9QEeiUmaJUhdo6Tw" value="PST_STAT_SUM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hM6Il9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hM6I19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hM6JF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hM6JV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hM6Jl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hQkgF9QEeiUmaJUhdo6Tw" name="PST_STAT_REJ" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hQkgV9QEeiUmaJUhdo6Tw" value="PST_STAT_REJ"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hQkgl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hQkg19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hQkhF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hQkhV9QEeiUmaJUhdo6Tw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hQkhl9QEeiUmaJUhdo6Tw" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hUO4F9QEeiUmaJUhdo6Tw" name="PST_STAT_MIN" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hUO4V9QEeiUmaJUhdo6Tw" value="PST_STAT_MIN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hUO4l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hUO419QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hUO5F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hUO5V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hUO5l9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hXSMF9QEeiUmaJUhdo6Tw" name="PST_STAT_MAX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hXSMV9QEeiUmaJUhdo6Tw" value="PST_STAT_MAX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hXSMl9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hXSM19QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hXSNF9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hXSNV9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hXSNl9QEeiUmaJUhdo6Tw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hem8F9QEeiUmaJUhdo6Tw" name="PST_STAT_COUNT" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hem8V9QEeiUmaJUhdo6Tw" value="PST_STAT_COUNT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hem8l9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hem819QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hem9F9QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hem9V9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hem9l9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7hhqQF9QEeiUmaJUhdo6Tw" name="PST_NUM" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_7hiRUF9QEeiUmaJUhdo6Tw" value="PST_NUM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7hiRUV9QEeiUmaJUhdo6Tw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7hiRUl9QEeiUmaJUhdo6Tw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7hiRU19QEeiUmaJUhdo6Tw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7hiRVF9QEeiUmaJUhdo6Tw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7hiRVV9QEeiUmaJUhdo6Tw" value="10"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_VYYCcFnWEemous-HzHdYcQ" name="IND_TABLES">
    <attribute defType="com.stambia.rdbms.schema.anonymStandardDictionaryPrefix" id="_WXEfUFnWEemous-HzHdYcQ" value="DIC_"/>
    <attribute defType="com.stambia.rdbms.schema.anonymOwnDictionaryPrefix" id="_WXEfUVnWEemous-HzHdYcQ" value="OWN_"/>
    <attribute defType="com.stambia.rdbms.schema.anonymTemporaryTablePrefix" id="_WXEfUlnWEemous-HzHdYcQ" value="A_"/>
    <attribute defType="com.stambia.rdbms.schema.anonymCorresTablePrefix" id="_WXEfU1nWEemous-HzHdYcQ" value="COR_"/>
    <attribute defType="com.stambia.rdbms.schema.anonymLogTableName" id="_WXEfVFnWEemous-HzHdYcQ" value="ANO_LOG"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_ZGvxoFnWEemous-HzHdYcQ" value="LOGS"/>
    <attribute defType="com.stambia.rdbms.schema.dataStoreFilter" id="_PsmQMJaDEemMVa6KSooldg" value="IND%"/>
    <node defType="com.stambia.rdbms.datastore" id="_58TUoFuSEemWi6RdV0yy2g" name="IND_SESSION_FILE_OP_LST">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_58T7sFuSEemWi6RdV0yy2g" value="IND_SESSION_FILE_OP_LST"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_58T7sVuSEemWi6RdV0yy2g" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_58T7sluSEemWi6RdV0yy2g" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.anonymEmptyTableToLoad" id="_5-tIYFuSEemWi6RdV0yy2g" value="false"/>
      <attribute defType="com.stambia.rdbms.datastore.anonymNewIdentifierColumnName" id="_5-tIYVuSEemWi6RdV0yy2g" value="ID_NEW_COL"/>
      <node defType="com.stambia.rdbms.column" id="_58VJ0FuSEemWi6RdV0yy2g" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_58VJ0VuSEemWi6RdV0yy2g" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_58Vw4FuSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_58Vw4VuSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_58Vw4luSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_58Vw41uSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_58Vw5FuSEemWi6RdV0yy2g" value="50"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIYluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIY1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIZFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIZVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_58dFoFuSEemWi6RdV0yy2g" name="SESS_NAME" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_58dFoVuSEemWi6RdV0yy2g" value="SESS_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_58dFoluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_58dFo1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_58dFpFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_58dssFuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_58dssVuSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIZluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIZ1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIaFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIaVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_58mPkFuSEemWi6RdV0yy2g" name="ACT_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_58mPkVuSEemWi6RdV0yy2g" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_58mPkluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_58mPk1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_58mPlFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_58mPlVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_58mPlluSEemWi6RdV0yy2g" value="50"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIaluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIa1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIbFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIbVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_58rvIFuSEemWi6RdV0yy2g" name="ACT_NAME" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_58rvIVuSEemWi6RdV0yy2g" value="ACT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_58rvIluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_58rvI1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_58rvJFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_58rvJVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_58rvJluSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIbluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIb1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIcFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIcVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_58wnoFuSEemWi6RdV0yy2g" name="ACT_ITER" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_58wnoVuSEemWi6RdV0yy2g" value="ACT_ITER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_58wnoluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_58wno1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_58wnpFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_58wnpVuSEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_58wnpluSEemWi6RdV0yy2g" value="10"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIcluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIc1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIdFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIdVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_582uQFuSEemWi6RdV0yy2g" name="FILE_ID" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_582uQVuSEemWi6RdV0yy2g" value="FILE_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_582uQluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_582uQ1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_582uRFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_582uRVuSEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_582uRluSEemWi6RdV0yy2g" value="10"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIdluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tId1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIeFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIeVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_587mwFuSEemWi6RdV0yy2g" name="FILE_OPERATION" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_587mwVuSEemWi6RdV0yy2g" value="FILE_OPERATION"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_587mwluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_587mw1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_587mxFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_587mxVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_587mxluSEemWi6RdV0yy2g" value="50"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIeluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIe1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIfFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIfVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59BtYFuSEemWi6RdV0yy2g" name="FILE_NAME" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_59BtYVuSEemWi6RdV0yy2g" value="FILE_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59BtYluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59BtY1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59BtZFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59BtZVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59BtZluSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIfluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIf1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIgFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIgVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59HM8FuSEemWi6RdV0yy2g" name="FILE_DIR" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_59HM8VuSEemWi6RdV0yy2g" value="FILE_DIR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59HM8luSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59HM81uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59HM9FuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59HM9VuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59HM9luSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIgluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIg1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIhFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIhVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59MsgFuSEemWi6RdV0yy2g" name="FILE_FROM_DIR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_59MsgVuSEemWi6RdV0yy2g" value="FILE_FROM_DIR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59MsgluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59Msg1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59MshFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59MshVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59MshluSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIhluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIh1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIiFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIiVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59SMEFuSEemWi6RdV0yy2g" name="FILE_FROM_FILE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_59SMEVuSEemWi6RdV0yy2g" value="FILE_FROM_FILE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59SMEluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59SME1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59SMFFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59SMFVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59SMFluSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIiluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIi1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIjFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIjVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59XroFuSEemWi6RdV0yy2g" name="FILE_TO_DIR" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_59XroVuSEemWi6RdV0yy2g" value="FILE_TO_DIR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59XroluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59Xro1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59XrpFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59YSsFuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59YSsVuSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIjluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIj1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIkFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIkVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59dyQFuSEemWi6RdV0yy2g" name="FILE_TO_FILE" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_59dyQVuSEemWi6RdV0yy2g" value="FILE_TO_FILE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59dyQluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59dyQ1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59dyRFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59dyRVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59dyRluSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIkluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIk1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIlFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIlVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59jR0FuSEemWi6RdV0yy2g" name="FILE_OPERATION_DATE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_59jR0VuSEemWi6RdV0yy2g" value="FILE_OPERATION_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59jR0luSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59jR01uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59jR1FuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59jR1VuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59jR1luSEemWi6RdV0yy2g" value="25"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIlluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIl1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tImFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tImVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59pYcFuSEemWi6RdV0yy2g" name="STATUS" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_59pYcVuSEemWi6RdV0yy2g" value="STATUS"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59pYcluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59pYc1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59pYdFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59pYdVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59pYdluSEemWi6RdV0yy2g" value="35"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tImluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIm1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tInFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tInVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59uQ8FuSEemWi6RdV0yy2g" name="STATUS_COMMENT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_59uQ8VuSEemWi6RdV0yy2g" value="STATUS_COMMENT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59uQ8luSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59uQ81uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59uQ9FuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59uQ9VuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59uQ9luSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tInluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIn1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIoFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIoVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_59zJcFuSEemWi6RdV0yy2g" name="STATUS_DATE" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_59zJcVuSEemWi6RdV0yy2g" value="STATUS_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_59zJcluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_59zJc1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_59zJdFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_59zJdVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_59zJdluSEemWi6RdV0yy2g" value="25"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIoluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIo1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIpFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIpVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_594B8FuSEemWi6RdV0yy2g" name="USER_COMMENT" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_594B8VuSEemWi6RdV0yy2g" value="USER_COMMENT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_594B8luSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_594B81uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_594B9FuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_594B9VuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_594B9luSEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIpluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIp1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIqFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIqVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_599hgFuSEemWi6RdV0yy2g" name="USER_FLAG" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_599hgVuSEemWi6RdV0yy2g" value="USER_FLAG"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_599hgluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_599hg1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_599hhFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_599hhVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_599hhluSEemWi6RdV0yy2g" value="35"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIqluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIq1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIrFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIrVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5-CaAFuSEemWi6RdV0yy2g" name="USER_DATE" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_5-CaAVuSEemWi6RdV0yy2g" value="USER_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5-CaAluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5-CaA1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_5-CaBFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5-CaBVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5-CaBluSEemWi6RdV0yy2g" value="25"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIrluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIr1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIsFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIsVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5-E2QFuSEemWi6RdV0yy2g" name="FILE_IS_HIDDEN" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_5-E2QVuSEemWi6RdV0yy2g" value="FILE_IS_HIDDEN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5-E2QluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5-E2Q1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_5-E2RFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5-E2RVuSEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5-E2RluSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIsluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIs1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tItFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tItVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5-E2R1uSEemWi6RdV0yy2g" name="FILE_LAST_MODIFIED" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_5-E2SFuSEemWi6RdV0yy2g" value="FILE_LAST_MODIFIED"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5-E2SVuSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5-E2SluSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_5-E2S1uSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5-E2TFuSEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5-E2TVuSEemWi6RdV0yy2g" value="20"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tItluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIt1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIuFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIuVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5-OnQFuSEemWi6RdV0yy2g" name="FILE_LAST_MODIFIED_DATE" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_5-OnQVuSEemWi6RdV0yy2g" value="FILE_LAST_MODIFIED_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5-OnQluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5-OnQ1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_5-OnRFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5-OnRVuSEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5-OnRluSEemWi6RdV0yy2g" value="25"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIuluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIu1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIvFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIvVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5-OnR1uSEemWi6RdV0yy2g" name="FILE_CAN_READ" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_5-OnSFuSEemWi6RdV0yy2g" value="FILE_CAN_READ"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5-OnSVuSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5-OnSluSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_5-OnS1uSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5-OnTFuSEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5-OnTVuSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIvluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIv1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIwFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIwVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5-cpsFuSEemWi6RdV0yy2g" name="FILE_CAN_WRITE" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_5-cpsVuSEemWi6RdV0yy2g" value="FILE_CAN_WRITE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5-cpsluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5-cps1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_5-cptFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5-cptVuSEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5-cptluSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIwluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIw1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIxFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIxVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5-g7IFuSEemWi6RdV0yy2g" name="FILE_CAN_EXECUTE" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_5-g7IVuSEemWi6RdV0yy2g" value="FILE_CAN_EXECUTE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5-g7IluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5-g7I1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_5-g7JFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5-g7JVuSEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5-g7JluSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIxluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIx1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIyFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIyVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5-lzoFuSEemWi6RdV0yy2g" name="FILE_IS_DIRECTORY" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_5-lzoVuSEemWi6RdV0yy2g" value="FILE_IS_DIRECTORY"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5-lzoluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5-lzo1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_5-lzpFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5-lzpVuSEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5-lzpluSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIyluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIy1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tIzFuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tIzVuSEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5-qsIFuSEemWi6RdV0yy2g" name="FILE_LENGTH" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_5-qsIVuSEemWi6RdV0yy2g" value="FILE_LENGTH"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5-qsIluSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5-qsI1uSEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_5-qsJFuSEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5-qsJVuSEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5-qsJluSEemWi6RdV0yy2g" value="20"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_5-tIzluSEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_5-tIz1uSEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_5-tI0FuSEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_5-tI0VuSEemWi6RdV0yy2g" value="None"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_jT4BIVuQEemWi6RdV0yy2g" name="IND_SESSION_MAIL_HEADER">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_jT5PQFuQEemWi6RdV0yy2g" value="IND_SESSION_MAIL_HEADER"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_jT5PQVuQEemWi6RdV0yy2g" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_jT5PQluQEemWi6RdV0yy2g" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.anonymEmptyTableToLoad" id="_jVd8kFuQEemWi6RdV0yy2g" value="false"/>
      <attribute defType="com.stambia.rdbms.datastore.anonymNewIdentifierColumnName" id="_jVd8kVuQEemWi6RdV0yy2g" value="ID_NEW_COL"/>
      <node defType="com.stambia.rdbms.column" id="_jT7EcFuQEemWi6RdV0yy2g" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_jT7EcVuQEemWi6RdV0yy2g" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jT7EcluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jT7Ec1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jT7EdFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jT7EdVuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jT7EdluQEemWi6RdV0yy2g" value="50"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejoFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejoVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejoluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejo1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jUDnUFuQEemWi6RdV0yy2g" name="SESS_NAME" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_jUDnUVuQEemWi6RdV0yy2g" value="SESS_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jUDnUluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jUDnU1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jUDnVFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jUDnVVuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jUDnVluQEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejpFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejpVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejpluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejp1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jUJt8FuQEemWi6RdV0yy2g" name="ACT_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_jUJt8VuQEemWi6RdV0yy2g" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jUJt8luQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jUJt81uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jUJt9FuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jUJt9VuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jUJt9luQEemWi6RdV0yy2g" value="50"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejqFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejqVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejqluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejq1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jUZlkFuQEemWi6RdV0yy2g" name="ACT_NAME" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_jUZlkVuQEemWi6RdV0yy2g" value="ACT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jUZlkluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jUZlk1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jUZllFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jUZllVuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jUZllluQEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejrFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejrVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejrluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejr1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jUfsMFuQEemWi6RdV0yy2g" name="MAIL_ID" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_jUfsMVuQEemWi6RdV0yy2g" value="MAIL_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jUfsMluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jUfsM1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jUfsNFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jUfsNVuQEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jUfsNluQEemWi6RdV0yy2g" value="10"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejsFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejsVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejsluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejs1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jUnA8FuQEemWi6RdV0yy2g" name="MAIL_SUBJECT" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_jUnA8VuQEemWi6RdV0yy2g" value="MAIL_SUBJECT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jUnoAFuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jUnoAVuQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jUnoAluQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jUnoA1uQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jUnoBFuQEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejtFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejtVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejtluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejt1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jUtuoFuQEemWi6RdV0yy2g" name="MAIL_FROM" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_jUtuoVuQEemWi6RdV0yy2g" value="MAIL_FROM"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jUtuoluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jUtuo1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jUtupFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jUtupVuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jUtupluQEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejuFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejuVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejuluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVeju1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jUzOMFuQEemWi6RdV0yy2g" name="MAIL_FOLDER" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_jUzOMVuQEemWi6RdV0yy2g" value="MAIL_FOLDER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jUzOMluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jUzOM1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jUzONFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jUzONVuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jUzONluQEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejvFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejvVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejvluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejv1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jU6i8FuQEemWi6RdV0yy2g" name="MAIL_ATT_PREFIX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_jU6i8VuQEemWi6RdV0yy2g" value="MAIL_ATT_PREFIX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jU6i8luQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jU6i81uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jU6i9FuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jU6i9VuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jU6i9luQEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejwFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejwVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejwluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejw1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jVACgFuQEemWi6RdV0yy2g" name="MAIL_MSG_PREFIX" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_jVACgVuQEemWi6RdV0yy2g" value="MAIL_MSG_PREFIX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jVACgluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jVACg1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jVAChFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jVAChVuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jVAChluQEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejxFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejxVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejxluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejx1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jVE7AFuQEemWi6RdV0yy2g" name="MAIL_SENT_DATE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_jVE7AVuQEemWi6RdV0yy2g" value="MAIL_SENT_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jVE7AluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jVE7A1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jVE7BFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jVE7BVuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jVE7BluQEemWi6RdV0yy2g" value="25"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejyFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejyVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejyluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejy1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jVKakFuQEemWi6RdV0yy2g" name="MAIL_RECEIVED_DATE" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_jVKakVuQEemWi6RdV0yy2g" value="MAIL_RECEIVED_DATE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jVKakluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jVKak1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jVKalFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jVKalVuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jVKalluQEemWi6RdV0yy2g" value="25"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVejzFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVejzVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVejzluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVejz1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jVPTEFuQEemWi6RdV0yy2g" name="MAIL_ACT_TYPE" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_jVPTEVuQEemWi6RdV0yy2g" value="MAIL_ACT_TYPE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jVPTEluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jVPTE1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jVPTFFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jVPTFVuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jVPTFluQEemWi6RdV0yy2g" value="25"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVej0FuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVej0VuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVej0luQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVej01uQEemWi6RdV0yy2g" value="None"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_jS0RMFuQEemWi6RdV0yy2g" name="IND_SESSION_MAIL_LST_INFO">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_jS04QFuQEemWi6RdV0yy2g" value="IND_SESSION_MAIL_LST_INFO"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_jS1fUFuQEemWi6RdV0yy2g" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.remarks" id="_jS1fUVuQEemWi6RdV0yy2g" value=""/>
      <attribute defType="com.stambia.rdbms.datastore.anonymEmptyTableToLoad" id="_jVcucFuQEemWi6RdV0yy2g" value="false"/>
      <attribute defType="com.stambia.rdbms.datastore.anonymNewIdentifierColumnName" id="_jVcucVuQEemWi6RdV0yy2g" value="ID_NEW_COL"/>
      <node defType="com.stambia.rdbms.column" id="_jS80EFuQEemWi6RdV0yy2g" name="SESS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_jS80EVuQEemWi6RdV0yy2g" value="SESS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jS9bIFuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jS9bIVuQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jS9bIluQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jS9bI1uQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jS9bJFuQEemWi6RdV0yy2g" value="50"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVcucluQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVcuc1uQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVcudFuQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVcudVuQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jTFW8FuQEemWi6RdV0yy2g" name="ACT_ID" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_jTFW8VuQEemWi6RdV0yy2g" value="ACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jTFW8luQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jTFW81uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jTFW9FuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jTFW9VuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jTFW9luQEemWi6RdV0yy2g" value="50"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVcudluQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVcud1uQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVcueFuQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVcueVuQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jTLdkFuQEemWi6RdV0yy2g" name="MAIL_ID" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_jTLdkVuQEemWi6RdV0yy2g" value="MAIL_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jTLdkluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jTLdk1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jTLdlFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jTLdlVuQEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jTLdlluQEemWi6RdV0yy2g" value="10"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVcueluQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVcue1uQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVcufFuQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVcufVuQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jTVOkFuQEemWi6RdV0yy2g" name="INFO_ID" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_jTVOkVuQEemWi6RdV0yy2g" value="INFO_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jTVOkluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jTVOk1uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jTVOlFuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jTVOlVuQEemWi6RdV0yy2g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jTVOlluQEemWi6RdV0yy2g" value="10"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVdVgFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVdVgVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVdVgluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVdVg1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jTXDwFuQEemWi6RdV0yy2g" name="INFO_TYPE" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_jTXDwVuQEemWi6RdV0yy2g" value="INFO_TYPE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jTXDwluQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jTgNsFuQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jTgNsVuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jTgNsluQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jTgNs1uQEemWi6RdV0yy2g" value="35"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVdVhFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVdVhVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVdVhluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVdVh1uQEemWi6RdV0yy2g" value="None"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jTrz4FuQEemWi6RdV0yy2g" name="INFO_VALUE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_jTrz4VuQEemWi6RdV0yy2g" value="INFO_VALUE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jTrz4luQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jTrz41uQEemWi6RdV0yy2g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jTrz5FuQEemWi6RdV0yy2g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jTrz5VuQEemWi6RdV0yy2g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jTrz5luQEemWi6RdV0yy2g" value="255"/>
        <attribute defType="com.stambia.rdbms.column.anonymActivated" id="_jVdViFuQEemWi6RdV0yy2g" value="true"/>
        <attribute defType="com.stambia.rdbms.column.anonymToIncludeInPseudonym" id="_jVdViVuQEemWi6RdV0yy2g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.anonymEmailPatternStandardDictionary" id="_jVdViluQEemWi6RdV0yy2g" value=":{first_name}:.:{last_name}:@:{email_provider_suffix}:"/>
        <attribute defType="com.stambia.rdbms.column.anonymNullValueRepartition" id="_jVdVi1uQEemWi6RdV0yy2g" value="None"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_8hnOIFneEemous-HzHdYcQ" name="PUBLIC">
    <attribute defType="com.stambia.rdbms.schema.anonymStandardDictionaryPrefix" id="_8hvJ8FneEemous-HzHdYcQ" value="DIC_"/>
    <attribute defType="com.stambia.rdbms.schema.anonymOwnDictionaryPrefix" id="_8hvJ8VneEemous-HzHdYcQ" value="OWN_"/>
    <attribute defType="com.stambia.rdbms.schema.anonymTemporaryTablePrefix" id="_8hvJ8lneEemous-HzHdYcQ" value="A_"/>
    <attribute defType="com.stambia.rdbms.schema.anonymCorresTablePrefix" id="_8hvJ81neEemous-HzHdYcQ" value="COR_"/>
    <attribute defType="com.stambia.rdbms.schema.anonymLogTableName" id="_8hvJ9FneEemous-HzHdYcQ" value="ANO_LOG"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_8ijpUFneEemous-HzHdYcQ" value="PUBLIC"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_8ikQYFneEemous-HzHdYcQ" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_8ikQYVneEemous-HzHdYcQ" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_8ik3cFneEemous-HzHdYcQ" value="I_[targetName]"/>
  </node>
</md:node>