<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_zhKmwNLmEeiL7cTvwX--2w" name="SQLServer_ECFDB" md:ref="platform:/plugin/com.indy.environment/technology/rdbms/mssql/mssql.rdbms.md#UUID_MD_RDBMS_MSSQL?fileId=UUID_MD_RDBMS_MSSQL$type=md$name=Microsoft%20SQL%20Server?">
  <attribute defType="com.stambia.rdbms.server.url" id="_VG184NLnEeiL7cTvwX--2w" value="jdbc:sqlserver://172.16.128.35;instance=SERVERNT52;databaseName=ECFDBTest"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_VG7ccNLnEeiL7cTvwX--2w" value="com.microsoft.sqlserver.jdbc.SQLServerDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_VG7ccdLnEeiL7cTvwX--2w" value="etude"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_VG7cctLnEeiL7cTvwX--2w" value="356C965F9E8F8893F3B9C9982987DD1F"/>
  <node defType="com.stambia.rdbms.schema" id="_zoJ_kNLmEeiL7cTvwX--2w" name="ECFDBTest.dbo">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_zoaeQNLmEeiL7cTvwX--2w" value="ECFDBTest"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_zoaeQdLmEeiL7cTvwX--2w" value="dbo"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_zoaeQtLmEeiL7cTvwX--2w" value="R_[targetName]__${/CORE_SESSION_ID}$"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_zoaeQ9LmEeiL7cTvwX--2w" value="L[number]_[targetName]__${/CORE_SESSION_ID}$"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_zoaeRNLmEeiL7cTvwX--2w" value="I_[targetName]__${/CORE_SESSION_ID}$"/>
    <attribute defType="com.stambia.rdbms.schema.stagingMask" id="_uxi1AD3yEeq2oaTfF82isg" value="S[number]_${/CORE_SESSION_ID}$"/>
    <attribute defType="com.stambia.rdbms.schema.work" id="_bcqfcD38Eeq2oaTfF82isg" ref="#_zX0TAD3mEeq2oaTfF82isg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=ECFDBstambia.dbo?"/>
    <node defType="com.stambia.rdbms.datastore" id="_UyzmkdLnEeiL7cTvwX--2w" name="Client_HoraireLiv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UyzmktLnEeiL7cTvwX--2w" value="Client_HoraireLiv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Uyzmk9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UzAa4NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzAa4dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzAa4tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzAa49LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzAa5NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzAa5dLnEeiL7cTvwX--2w" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzAa5tLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzMBENLnEeiL7cTvwX--2w" name="H_DebutLivLundi" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzMBEdLnEeiL7cTvwX--2w" value="H_DebutLivLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzMBEtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzMBE9LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzMBFNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzMBFdLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzMBFtLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzMBF9LnEeiL7cTvwX--2w" name="H_DebutLivMardi" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzMBGNLnEeiL7cTvwX--2w" value="H_DebutLivMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzMBGdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzMBGtLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzMBG9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzMBHNLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzMBHdLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzMBHtLnEeiL7cTvwX--2w" name="H_DebutLivMercredi" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzMBH9LnEeiL7cTvwX--2w" value="H_DebutLivMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzMBINLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzMBIdLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzMBItLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzMBI9LnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzMBJNLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzMBJdLnEeiL7cTvwX--2w" name="H_DebutLivJeudi" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzMBJtLnEeiL7cTvwX--2w" value="H_DebutLivJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzMBJ9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzMBKNLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzMBKdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzMBKtLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzMBK9LnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzSuwNLnEeiL7cTvwX--2w" name="H_DebutLivVendredi" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzSuwdLnEeiL7cTvwX--2w" value="H_DebutLivVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzSuwtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzSuw9LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzSuxNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzSuxdLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzSuxtLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzSux9LnEeiL7cTvwX--2w" name="H_DebutLivSamedi" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzSuyNLnEeiL7cTvwX--2w" value="H_DebutLivSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzSuydLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzSuytLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzSuy9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzSuzNLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzSuzdLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzSuztLnEeiL7cTvwX--2w" name="H_DebutLivDimanche" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzSuz9LnEeiL7cTvwX--2w" value="H_DebutLivDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzSu0NLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzSu0dLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzSu0tLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzSu09LnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzSu1NLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzSu1dLnEeiL7cTvwX--2w" name="H_FinLivLundi" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzSu1tLnEeiL7cTvwX--2w" value="H_FinLivLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzSu19LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzSu2NLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzSu2dLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzSu2tLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzSu29LnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzSu3NLnEeiL7cTvwX--2w" name="H_FinLivMardi" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzSu3dLnEeiL7cTvwX--2w" value="H_FinLivMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzSu3tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzSu39LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzSu4NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzSu4dLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzSu4tLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzSu49LnEeiL7cTvwX--2w" name="H_FinLivMercredi" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzSu5NLnEeiL7cTvwX--2w" value="H_FinLivMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzSu5dLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzSu5tLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzSu59LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzSu6NLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzSu6dLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzYOUNLnEeiL7cTvwX--2w" name="H_FinLivJeudi" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzYOUdLnEeiL7cTvwX--2w" value="H_FinLivJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzYOUtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzYOU9LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzYOVNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzYOVdLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzYOVtLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzYOV9LnEeiL7cTvwX--2w" name="H_FinLivVendredi" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzYOWNLnEeiL7cTvwX--2w" value="H_FinLivVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzYOWdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzYOWtLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzYOW9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzYOXNLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzYOXdLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzYOXtLnEeiL7cTvwX--2w" name="H_FinLivSamedi" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzYOX9LnEeiL7cTvwX--2w" value="H_FinLivSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzYOYNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzYOYdLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzYOYtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzYOY9LnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzYOZNLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzYOZdLnEeiL7cTvwX--2w" name="H_FinLivDimanche" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzYOZtLnEeiL7cTvwX--2w" value="H_FinLivDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzYOZ9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzYOaNLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzYOadLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzYOatLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzYOa9LnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UzeU8NLnEeiL7cTvwX--2w" name="PK_Horaire_Livraison">
        <node defType="com.stambia.rdbms.colref" id="_UzeU8dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UzeU8tLnEeiL7cTvwX--2w" ref="#_b2iK99X7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VC7GMNLnEeiL7cTvwX--2w" name="FK_Horaire_Livraison_Client">
        <node defType="com.stambia.rdbms.relation" id="_VC7GMdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VC7GMtLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VC7GM9LnEeiL7cTvwX--2w" ref="#_Ut2OENLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Code?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b2iK99X7Eeimuaax4vsiZw" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_b2iK-NX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b2iK-dX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b2iK-tX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b2iK-9X7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b2iK_NX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b2iK_dX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cEcrMNX7Eeimuaax4vsiZw" name="FK_Client_HoraireLiv_Client">
        <node defType="com.stambia.rdbms.relation" id="_cEcrMdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cEcrMtX7Eeimuaax4vsiZw" ref="#_b2iK99X7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cEcrM9X7Eeimuaax4vsiZw" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U3YkkdLnEeiL7cTvwX--2w" name="Client_UniversConso">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U3YkktLnEeiL7cTvwX--2w" value="Client_UniversConso"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U3Ykk9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U3lY59LnEeiL7cTvwX--2w" name="IdUniversProduit" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U3lY6NLnEeiL7cTvwX--2w" value="IdUniversProduit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U3lY6dLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U3lY6tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U3lY69LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U3lY7NLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U3lY7dLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U3q4cNLnEeiL7cTvwX--2w" name="PK_Client_UniversConso">
        <node defType="com.stambia.rdbms.colref" id="_U3q4cdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U3q4ctLnEeiL7cTvwX--2w" ref="#_b6zm9NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U3q4c9LnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U3q4dNLnEeiL7cTvwX--2w" ref="#_U3lY59LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUniversProduit?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDmboNLnEeiL7cTvwX--2w" name="FK_Client_UniversConso_Client">
        <node defType="com.stambia.rdbms.relation" id="_VDmbodLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDmbotLnEeiL7cTvwX--2w" ref="#_b6zm9NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDmbo9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDmbpNLnEeiL7cTvwX--2w" name="FK_Client_UniversConso_ref_UniversConso">
        <node defType="com.stambia.rdbms.relation" id="_VDmbpdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDmbptLnEeiL7cTvwX--2w" ref="#_U3lY59LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUniversProduit?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDmbp9LnEeiL7cTvwX--2w" ref="#_UrYI4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b6zm9NX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b6zm9dX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b6zm9tX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b6zm99X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b6zm-NX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b6zm-dX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b6zm-tX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cE5XHNX7Eeimuaax4vsiZw" name="FK_Client_UniversConso_ref_UniversConso1">
        <node defType="com.stambia.rdbms.relation" id="_cE5XHdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cE5XHtX7Eeimuaax4vsiZw" ref="#_U3lY59LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUniversProduit?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cE5XH9X7Eeimuaax4vsiZw" ref="#_UrYI4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U5rDkdLnEeiL7cTvwX--2w" name="ref_SpecialiteCulinaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U5rDktLnEeiL7cTvwX--2w" value="ref_SpecialiteCulinaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U5rDk9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U5334NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U5334dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U5334tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U53349LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U5335NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U5335dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U5335tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U59XcNLnEeiL7cTvwX--2w" name="PK_ref_SpecialiteCulinaire">
        <node defType="com.stambia.rdbms.colref" id="_U59XcdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U59XctLnEeiL7cTvwX--2w" ref="#_U5334NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EUXM9x1_EemosZyXDkDTkg" name="LibSpecialite" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_EUXM-B1_EemosZyXDkDTkg" value="LibSpecialite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EUXM-R1_EemosZyXDkDTkg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EUXM-h1_EemosZyXDkDTkg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EUXM-x1_EemosZyXDkDTkg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EUXM_B1_EemosZyXDkDTkg" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U_ZREdLnEeiL7cTvwX--2w" name="ref_Genre">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U_ZREtLnEeiL7cTvwX--2w" value="ref_Genre"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U_ZRE9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U_btUNLnEeiL7cTvwX--2w" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_btUdLnEeiL7cTvwX--2w" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_btUtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U_btU9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_btVNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_btVdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_btVtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U_f-wNLnEeiL7cTvwX--2w" name="Genre" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_f-wdLnEeiL7cTvwX--2w" value="Genre"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_f-wtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_f-w9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_f-xNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_f-xdLnEeiL7cTvwX--2w" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U_ibANLnEeiL7cTvwX--2w" name="Civilite" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_ibAdLnEeiL7cTvwX--2w" value="Civilite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_ibAtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_ibA9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_ibBNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_ibBdLnEeiL7cTvwX--2w" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U_ibBtLnEeiL7cTvwX--2w" name="Abreviation" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_ibB9LnEeiL7cTvwX--2w" value="Abreviation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_ibCNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_ibCdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_ibCtLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_ibC9LnEeiL7cTvwX--2w" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U_mFYNLnEeiL7cTvwX--2w" name="PK_ref_Genre">
        <node defType="com.stambia.rdbms.colref" id="_U_mFYdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U_mFYtLnEeiL7cTvwX--2w" ref="#_U_btUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U_sMAdLnEeiL7cTvwX--2w" name="ref_NbEmployes">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U_sMAtLnEeiL7cTvwX--2w" value="ref_NbEmployes"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U_sMA9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U_sMBNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_sMBdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_sMBtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U_sMB9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_sMCNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_sMCdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_sMCtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U_ySoNLnEeiL7cTvwX--2w" name="NbEmployes" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_ySodLnEeiL7cTvwX--2w" value="NbEmployes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_ySotLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_ySo9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_ySpNLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_ySpdLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U_ySptLnEeiL7cTvwX--2w" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_ySp9LnEeiL7cTvwX--2w" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_ySqNLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U_ySqdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_ySqtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_ySq9LnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_ySrNLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U_ySrdLnEeiL7cTvwX--2w" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_ySrtLnEeiL7cTvwX--2w" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_ySr9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U_ySsNLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_ySsdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_ySstLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_ySs9LnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U_6OcNLnEeiL7cTvwX--2w" name="PK_ref_NbEmployes">
        <node defType="com.stambia.rdbms.colref" id="_U_6OcdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U_6OctLnEeiL7cTvwX--2w" ref="#_U_sMBNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Uzwo0dLnEeiL7cTvwX--2w" name="ref_Canal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Uzwo0tLnEeiL7cTvwX--2w" value="ref_Canal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Uzwo09LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Uz2vcNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uz2vcdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uz2vctLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Uz2vc9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uz2vdNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uz2vddLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uz2vdtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uz2vd9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uz2veNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uz2vedLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uz2vetLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uz2ve9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uz2vfNLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uz2vfdLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uz2vftLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uz2vf9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uz2vgNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uz2vgdLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uz2vgtLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Uz82ENLnEeiL7cTvwX--2w" name="PK_ref_Canal">
        <node defType="com.stambia.rdbms.colref" id="_Uz82EdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Uz82EtLnEeiL7cTvwX--2w" ref="#_Uz2vcNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yYCH0DbREeqF3cPVFXUsDw" name="LibCanal" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yYCH0TbREeqF3cPVFXUsDw" value="LibCanal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yYCH0jbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yYCH0zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yYCH1DbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yYCH1TbREeqF3cPVFXUsDw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U6kbcdLnEeiL7cTvwX--2w" name="ref_ActiviteSegment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U6kbctLnEeiL7cTvwX--2w" value="ref_ActiviteSegment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U6kbc9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U6os4NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U6os4dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U6os4tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U6os49LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U6os5NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U6os5dLnEeiL7cTvwX--2w" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U6os5tLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U6os59LnEeiL7cTvwX--2w" name="IdSegment" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U6os6NLnEeiL7cTvwX--2w" value="IdSegment"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U6os6dLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U6os6tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U6os69LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U6os7NLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U6os7dLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U6os7tLnEeiL7cTvwX--2w" name="Libelle" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U6os79LnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U6os8NLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U6os8dLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U6os8tLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U6os89LnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U6os9NLnEeiL7cTvwX--2w" name="PK_ref_ActiviteSegment">
        <node defType="com.stambia.rdbms.colref" id="_U6os9dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U6os9tLnEeiL7cTvwX--2w" ref="#_U6os4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VEE8wNLnEeiL7cTvwX--2w" name="FK_ref_ActiviteSegment_ref_Segment">
        <node defType="com.stambia.rdbms.relation" id="_VEE8wdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VEE8wtLnEeiL7cTvwX--2w" ref="#_U6os59LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdSegment?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VEE8w9LnEeiL7cTvwX--2w" ref="#_U8DpNNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ygQPsDbREeqF3cPVFXUsDw" name="LibActivite" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_ygQPsTbREeqF3cPVFXUsDw" value="LibActivite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ygQPsjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ygQPszbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ygQPtDbREeqF3cPVFXUsDw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ygQPtTbREeqF3cPVFXUsDw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U-J69NLnEeiL7cTvwX--2w" name="Client_BI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U-J69dLnEeiL7cTvwX--2w" value="Client_BI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U-J69tLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U-c14NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-c14dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-c14tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U-c149LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-c15NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-c15dLnEeiL7cTvwX--2w" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-c15tLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-i8h9LnEeiL7cTvwX--2w" name="NbLigneConsomme" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-i8iNLnEeiL7cTvwX--2w" value="NbLigneConsomme"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-i8idLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U-i8itLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-i8i9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-i8jNLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-i8jdLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-i8jtLnEeiL7cTvwX--2w" name="FrequenceMoyCmd" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-i8j9LnEeiL7cTvwX--2w" value="FrequenceMoyCmd"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-i8kNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-i8kdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-i8ktLnEeiL7cTvwX--2w" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-i8k9LnEeiL7cTvwX--2w" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-ocENLnEeiL7cTvwX--2w" name="ProfilSaisonnier" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-ocEdLnEeiL7cTvwX--2w" value="ProfilSaisonnier"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-ocEtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-ocE9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-ocFNLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-ocFdLnEeiL7cTvwX--2w" value="150"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-ocFtLnEeiL7cTvwX--2w" name="PanierMoyen" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-ocF9LnEeiL7cTvwX--2w" value="PanierMoyen"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-ocGNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-ocGdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-ocGtLnEeiL7cTvwX--2w" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-ocG9LnEeiL7cTvwX--2w" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-ocHNLnEeiL7cTvwX--2w" name="NbLignesMoy" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-ocHdLnEeiL7cTvwX--2w" value="NbLignesMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-ocHtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-ocH9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-ocINLnEeiL7cTvwX--2w" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-ocIdLnEeiL7cTvwX--2w" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-uisNLnEeiL7cTvwX--2w" name="FreqVisiteWeb" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-uisdLnEeiL7cTvwX--2w" value="FreqVisiteWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-uistLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-uis9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-uitNLnEeiL7cTvwX--2w" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-uitdLnEeiL7cTvwX--2w" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-uittLnEeiL7cTvwX--2w" name="FreqVisiteCcial" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-uit9LnEeiL7cTvwX--2w" value="FreqVisiteCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-uiuNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-uiudLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-uiutLnEeiL7cTvwX--2w" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-uiu9LnEeiL7cTvwX--2w" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-uivNLnEeiL7cTvwX--2w" name="FreqAppel" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-uivdLnEeiL7cTvwX--2w" value="FreqAppel"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-uivtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-uiv9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-uiwNLnEeiL7cTvwX--2w" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-uiwdLnEeiL7cTvwX--2w" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-uiwtLnEeiL7cTvwX--2w" name="FreqEmailing" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-uiw9LnEeiL7cTvwX--2w" value="FreqEmailing"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-uixNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-uixdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-uixtLnEeiL7cTvwX--2w" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-uix9LnEeiL7cTvwX--2w" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-0pUNLnEeiL7cTvwX--2w" name="SecteurVacant" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-0pUdLnEeiL7cTvwX--2w" value="SecteurVacant"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-0pUtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-0pU9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-0pVNLnEeiL7cTvwX--2w" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-0pVdLnEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-0pVtLnEeiL7cTvwX--2w" name="ClientPrete" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-0pV9LnEeiL7cTvwX--2w" value="ClientPrete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-0pWNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-0pWdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-0pWtLnEeiL7cTvwX--2w" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-0pW9LnEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-0pXNLnEeiL7cTvwX--2w" name="CcialConge" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-0pXdLnEeiL7cTvwX--2w" value="CcialConge"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-0pXtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-0pX9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-0pYNLnEeiL7cTvwX--2w" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-0pYdLnEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-6v8NLnEeiL7cTvwX--2w" name="CcialEnCharge" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-6v8dLnEeiL7cTvwX--2w" value="CcialEnCharge"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-6v8tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-6v89LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-6v9NLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-6v9dLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-6v9tLnEeiL7cTvwX--2w" name="CcialEnPret" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-6v99LnEeiL7cTvwX--2w" value="CcialEnPret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-6v-NLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-6v-dLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-6v-tLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-6v-9LnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U-6v_NLnEeiL7cTvwX--2w" name="PK_Client_BI">
        <node defType="com.stambia.rdbms.colref" id="_U-6v_dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U-6v_tLnEeiL7cTvwX--2w" ref="#_U-c14NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VEdXQNLnEeiL7cTvwX--2w" name="FK_Client_BI_Client">
        <node defType="com.stambia.rdbms.relation" id="_VEdXQdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VEdXQtLnEeiL7cTvwX--2w" ref="#_cBgE19X7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VEdXQ9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cBgE19X7Eeimuaax4vsiZw" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_cBgE2NX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cBgE2dX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cBgE2tX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cBgE29X7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cBgE3NX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cBgE3dX7Eeimuaax4vsiZw" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Uwy0YdLnEeiL7cTvwX--2w" name="Client_Adresse">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Uwy0YtLnEeiL7cTvwX--2w" value="Client_Adresse"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Uwy0Y9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UxMdANLnEeiL7cTvwX--2w" name="Id" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxMdAdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxMdAtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UxMdA9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxMdBNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxMdBdLnEeiL7cTvwX--2w" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxMdBtLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxNrINLnEeiL7cTvwX--2w" name="Rue" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxNrIdLnEeiL7cTvwX--2w" value="Rue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxNrItLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxNrI9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxNrJNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxNrJdLnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxNrJtLnEeiL7cTvwX--2w" name="Complement" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxNrJ9LnEeiL7cTvwX--2w" value="Complement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxNrKNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxNrKdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxNrKtLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxNrK9LnEeiL7cTvwX--2w" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxNrLNLnEeiL7cTvwX--2w" name="CodePostale" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxNrLdLnEeiL7cTvwX--2w" value="CodePostale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxNrLtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxNrL9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxNrMNLnEeiL7cTvwX--2w" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxNrMdLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxR8kNLnEeiL7cTvwX--2w" name="Ville" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxR8kdLnEeiL7cTvwX--2w" value="Ville"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxR8ktLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxR8k9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxR8lNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxR8ldLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxSjoNLnEeiL7cTvwX--2w" name="IdPays" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxSjodLnEeiL7cTvwX--2w" value="IdPays"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxSjotLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UxSjo9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxSjpNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxSjpdLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxSjptLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxTxwNLnEeiL7cTvwX--2w" name="TypeAdresse" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxTxwdLnEeiL7cTvwX--2w" value="TypeAdresse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxTxwtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UxTxw9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxTxxNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxTxxdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxTxxtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxTxx9LnEeiL7cTvwX--2w" name="CodeCommune" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxTxyNLnEeiL7cTvwX--2w" value="CodeCommune"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxTxydLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxTxytLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxTxy9LnEeiL7cTvwX--2w" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxTxzNLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxTxzdLnEeiL7cTvwX--2w" name="CNT4" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxTxztLnEeiL7cTvwX--2w" value="CNT4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxTxz9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxTx0NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxTx0dLnEeiL7cTvwX--2w" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxTx0tLnEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxYDMNLnEeiL7cTvwX--2w" name="IRIS" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxYDMdLnEeiL7cTvwX--2w" value="IRIS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxYDMtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxYDM9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxYDNNLnEeiL7cTvwX--2w" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxYDNdLnEeiL7cTvwX--2w" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UxeJ0NLnEeiL7cTvwX--2w" name="PK_Adresse">
        <node defType="com.stambia.rdbms.colref" id="_UxeJ0dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UxeJ0tLnEeiL7cTvwX--2w" ref="#_UxMdANLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCr1qNLnEeiL7cTvwX--2w" name="FK_Adresse_Client">
        <node defType="com.stambia.rdbms.relation" id="_VCr1qdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCr1qtLnEeiL7cTvwX--2w" ref="#_UxMdANLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCr1q9LnEeiL7cTvwX--2w" ref="#_Ut2OENLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Code?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCr1rNLnEeiL7cTvwX--2w" name="FK_Adresse_ref_Pays">
        <node defType="com.stambia.rdbms.relation" id="_VCr1rdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCr1rtLnEeiL7cTvwX--2w" ref="#_UxSjoNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdPays?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCr1r9LnEeiL7cTvwX--2w" ref="#_Uv4OYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCr1sNLnEeiL7cTvwX--2w" name="FK_Adresse_ref_TypeAdresse">
        <node defType="com.stambia.rdbms.relation" id="_VCr1sdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCr1stLnEeiL7cTvwX--2w" ref="#_UxTxwNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeAdresse?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCr1s9LnEeiL7cTvwX--2w" ref="#_UzqiMNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b0VLg9X7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b0VLhNX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b0VLhdX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b0VLhtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b0VLh9X7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b0VLiNX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b0VLidX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cES6KNX7Eeimuaax4vsiZw" name="FK_Client_Adresse_Client">
        <node defType="com.stambia.rdbms.relation" id="_cES6KdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cES6KtX7Eeimuaax4vsiZw" ref="#_b0VLg9X7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cES6K9X7Eeimuaax4vsiZw" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jEkggOaNEeicCszix7eWDA" name="BureauDistributeur" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_jEkggeaNEeicCszix7eWDA" value="BureauDistributeur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jEkgguaNEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jEkgg-aNEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jEkghOaNEeicCszix7eWDA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jEkgheaNEeicCszix7eWDA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jEuRYOaNEeicCszix7eWDA" name="CodeDepartement" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_jEuRYeaNEeicCszix7eWDA" value="CodeDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jEuRYuaNEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jEuRY-aNEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jEuRZOaNEeicCszix7eWDA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jEuRZeaNEeicCszix7eWDA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_MtFRAOwfEeihWNvRK7oOPw" name="TypeZone" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_MtFRAewfEeihWNvRK7oOPw" value="TypeZone"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_MtFRAuwfEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_MtFRA-wfEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_MtFRBOwfEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_MtFRBewfEeihWNvRK7oOPw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_MtFRBuwfEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_MtGfIOwfEeihWNvRK7oOPw" name="EnvTouristique" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_MtGfIewfEeihWNvRK7oOPw" value="EnvTouristique"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_MtGfIuwfEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_MtGfI-wfEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_MtGfJOwfEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_MtGfJewfEeihWNvRK7oOPw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_MtGfJuwfEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_M79ahOwfEeihWNvRK7oOPw" name="FK_Client_Adresse_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.relation" id="_M79ahewfEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_M79ahuwfEeihWNvRK7oOPw" ref="#_MtGfIOwfEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=EnvTouristique?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_M79ah-wfEeihWNvRK7oOPw" ref="#_UxuohNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_M79akOwfEeihWNvRK7oOPw" name="FK_Client_Adresse_ref_TypeZone">
        <node defType="com.stambia.rdbms.relation" id="_M79akewfEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_M79akuwfEeihWNvRK7oOPw" ref="#_MtFRAOwfEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeZone?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_M79ak-wfEeihWNvRK7oOPw" ref="#_U7BHYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U1fHIdLnEeiL7cTvwX--2w" name="ref_PrestationsOffertes">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U1fHItLnEeiL7cTvwX--2w" value="ref_PrestationsOffertes"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U1fHI9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U1kmsNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U1kmsdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U1kmstLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U1kms9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U1kmtNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U1kmtdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U1kmttLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U1qtUNLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U1qtUdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U1qtUtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U1qtU9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U1qtVNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U1qtVdLnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U126kNLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U126kdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U126ktLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U126k9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U126lNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U126ldLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U126ltLnEeiL7cTvwX--2w" name="PK_ref_PrestationsOffertes">
        <node defType="com.stambia.rdbms.colref" id="_U126l9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U126mNLnEeiL7cTvwX--2w" ref="#_U1kmsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yaFWQDbREeqF3cPVFXUsDw" name="LibPrestation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yaFWQTbREeqF3cPVFXUsDw" value="LibPrestation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yaFWQjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yaFWQzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yaFWRDbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yaFWRTbREeqF3cPVFXUsDw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U0C8sdLnEeiL7cTvwX--2w" name="ref_Jours">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U0C8stLnEeiL7cTvwX--2w" value="ref_Jours"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U0C8s9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U0C8tNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0C8tdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0C8ttLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U0C8t9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0C8uNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0C8udLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0C8utLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U0JqYNLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0JqYdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0JqYtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0JqY9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0JqZNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0JqZdLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U0JqZtLnEeiL7cTvwX--2w" name="PK_ref_Jours">
        <node defType="com.stambia.rdbms.colref" id="_U0JqZ9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U0JqaNLnEeiL7cTvwX--2w" ref="#_U0C8tNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yYL40DbREeqF3cPVFXUsDw" name="LibJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yYL40TbREeqF3cPVFXUsDw" value="LibJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yYL40jbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yYL40zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yYL41DbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yYL41TbREeqF3cPVFXUsDw" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U4cUhNLnEeiL7cTvwX--2w" name="ref_TypeEtabClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U4cUhdLnEeiL7cTvwX--2w" value="ref_TypeEtabClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U4cUhtLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U4ibINLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U4ibIdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U4ibItLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U4ibI9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U4ibJNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U4ibJdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U4ibJtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U4k3YNLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U4k3YdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U4k3YtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U4k3Y9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U4k3ZNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U4k3ZdLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U4k3ZtLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U4k3Z9LnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U4k3aNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U4k3adLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U4k3atLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U4k3a9LnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U4n6sNLnEeiL7cTvwX--2w" name="PK_ref_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_U4n6sdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U4n6stLnEeiL7cTvwX--2w" ref="#_U4ibINLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ydWFoDbREeqF3cPVFXUsDw" name="LibTypeEtabClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ydWFoTbREeqF3cPVFXUsDw" value="LibTypeEtabClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ydWFojbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ydWFozbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ydWFpDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ydWFpTbREeqF3cPVFXUsDw" value="30"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UyJfRNLnEeiL7cTvwX--2w" name="ref_NbLits">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UyJfRdLnEeiL7cTvwX--2w" value="ref_NbLits"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UyJfRtLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UyO-0NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UyO-0dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UyO-0tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UyO-09LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UyO-1NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UyO-1dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UyO-1tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UyO-19LnEeiL7cTvwX--2w" name="Nblits" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UyO-2NLnEeiL7cTvwX--2w" value="Nblits"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UyO-2dLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UyO-2tLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UyO-29LnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UyO-3NLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UyO-3dLnEeiL7cTvwX--2w" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UyO-3tLnEeiL7cTvwX--2w" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UyO-39LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UyO-4NLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UyO-4dLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UyO-4tLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UyO-49LnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UyO-5NLnEeiL7cTvwX--2w" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_UyO-5dLnEeiL7cTvwX--2w" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UyO-5tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UyO-59LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UyO-6NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UyO-6dLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UyO-6tLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UyVFcNLnEeiL7cTvwX--2w" name="PK_ref_NbLits">
        <node defType="com.stambia.rdbms.colref" id="_UyVFcdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UyVFctLnEeiL7cTvwX--2w" ref="#_UyO-0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Us_ScdLnEeiL7cTvwX--2w" name="ref_Societe">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Us_SctLnEeiL7cTvwX--2w" value="ref_Societe"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Us_Sc9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UtLfsNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtLfsdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtLfstLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UtLfs9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UtLftNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtLftdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtLfttLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UtLft9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtLfuNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtLfudLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UtLfutLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtLfu9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtLfvNLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UtOjANLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtOjAdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtOjAtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UtOjA9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtOjBNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtOjBdLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UtRmUNLnEeiL7cTvwX--2w" name="PK_ref_Societe">
        <node defType="com.stambia.rdbms.colref" id="_UtRmUdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UtRmUtLnEeiL7cTvwX--2w" ref="#_UtLfsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yRNuIDbREeqF3cPVFXUsDw" name="LibSociete" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yRNuITbREeqF3cPVFXUsDw" value="LibSociete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yRNuIjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yRNuIzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yRNuJDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yRNuJTbREeqF3cPVFXUsDw" value="30"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UtdzmtLnEeiL7cTvwX--2w" name="ref_Mois">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Utdzm9LnEeiL7cTvwX--2w" value="ref_Mois"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UtdznNLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UtkhQNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtkhQdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtkhQtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UtkhQ9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UtkhRNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtkhRdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtkhRtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UtkhR9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtkhSNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtkhSdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UtkhStLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtkhS9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtkhTNLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UtqA0NLnEeiL7cTvwX--2w" name="PK_ref_Mois">
        <node defType="com.stambia.rdbms.colref" id="_UtqA0dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UtqA0tLnEeiL7cTvwX--2w" ref="#_UtkhQNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yR5DkDbREeqF3cPVFXUsDw" name="LibMois" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yR5DkTbREeqF3cPVFXUsDw" value="LibMois"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yR5DkjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yR5DkzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yR5DlDbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yR5DlTbREeqF3cPVFXUsDw" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_VAQMsdLnEeiL7cTvwX--2w" name="ref_PerimetreProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_VAQMstLnEeiL7cTvwX--2w" value="ref_PerimetreProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_VAQMs9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_VASo8NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_VASo8dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VASo8tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VASo89LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VASo9NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VASo9dLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VASo9tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VAWTUNLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_VAWTUdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VAWTUtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VAWTU9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VAWTVNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VAWTVdLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VAYvkNLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_VAYvkdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VAYvktLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VAYvk9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VAYvlNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VAYvldLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_VAdBANLnEeiL7cTvwX--2w" name="PK_ref_PerimetreDernierProjet">
        <node defType="com.stambia.rdbms.colref" id="_VAdBAdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_VAdBAtLnEeiL7cTvwX--2w" ref="#_VASo8NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ymYs4DbREeqF3cPVFXUsDw" name="LibPerimetreProj" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ymYs4TbREeqF3cPVFXUsDw" value="LibPerimetreProj"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ymYs4jbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ymYs4zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ymYs5DbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ymYs5TbREeqF3cPVFXUsDw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U2PVGtLnEeiL7cTvwX--2w" name="ref_TypeEtablissement">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U2PVG9LnEeiL7cTvwX--2w" value="ref_TypeEtablissement"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U2PVHNLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U2VbsNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2VbsdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2VbstLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U2Vbs9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2VbtNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2VbtdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2VbttLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U2Vbt9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2VbuNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2VbudLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2VbutLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2Vbu9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2VbvNLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U2VbvdLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2VbvtLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2Vbv9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2VbwNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2VbwdLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2VbwtLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U2biUNLnEeiL7cTvwX--2w" name="PK_TypeEtablissement">
        <node defType="com.stambia.rdbms.colref" id="_U2biUdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U2biUtLnEeiL7cTvwX--2w" ref="#_U2VbsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Uq_HUdLnEeiL7cTvwX--2w" name="Client_JourLiv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Uq_HUtLnEeiL7cTvwX--2w" value="Client_JourLiv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Uq_HU9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UrF1B9LnEeiL7cTvwX--2w" name="IdJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UrF1CNLnEeiL7cTvwX--2w" value="IdJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UrF1CdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UrF1CtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UrF1C9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UrF1DNLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UrF1DdLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UrLUkNLnEeiL7cTvwX--2w" name="PK_Client_JourLiv">
        <node defType="com.stambia.rdbms.colref" id="_UrLUkdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UrLUktLnEeiL7cTvwX--2w" ref="#_bvCb2dX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_UrLUk9LnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UrLUlNLnEeiL7cTvwX--2w" ref="#_UrF1B9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdJour?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCY6sNLnEeiL7cTvwX--2w" name="FK_Client_JourLiv_Client">
        <node defType="com.stambia.rdbms.relation" id="_VCY6sdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCY6stLnEeiL7cTvwX--2w" ref="#_bvCb2dX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCY6s9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCY6tNLnEeiL7cTvwX--2w" name="FK_Client_JourLiv_ref_Jours">
        <node defType="com.stambia.rdbms.relation" id="_VCY6tdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCY6ttLnEeiL7cTvwX--2w" ref="#_UrF1B9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCY6t9LnEeiL7cTvwX--2w" ref="#_U0C8tNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_bvCb2dX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_bvCb2tX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_bvCb29X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_bvCb3NX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_bvCb3dX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_bvCb3tX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_bvCb39X7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cEB0ZNX7Eeimuaax4vsiZw" name="FK_Client_JourLiv_ref_Jours1">
        <node defType="com.stambia.rdbms.relation" id="_cEB0ZdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cEB0ZtX7Eeimuaax4vsiZw" ref="#_UrF1B9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cEB0Z9X7Eeimuaax4vsiZw" ref="#_U0C8tNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U59XdNLnEeiL7cTvwX--2w" name="Client_PerimetreProchainProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U6DeENLnEeiL7cTvwX--2w" value="Client_PerimetreProchainProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U6DeEdLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.pk" id="_U6KLwNLnEeiL7cTvwX--2w" name="PK_Client_PerimetreProchainProjet">
        <node defType="com.stambia.rdbms.colref" id="_U6KLwdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U6KLwtLnEeiL7cTvwX--2w" ref="#_b9iK1NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U6KLw9LnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U6KLxNLnEeiL7cTvwX--2w" ref="#_1EV4YOwOEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUnivers?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VD-2INLnEeiL7cTvwX--2w" name="FK_Client_PerimetreProchainProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_VD-2IdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VD-2ItLnEeiL7cTvwX--2w" ref="#_b9iK1NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VD-2I9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VD-2JNLnEeiL7cTvwX--2w" name="FK_Client_PerimetreProchainProjet_ref_PerimetreProjet">
        <node defType="com.stambia.rdbms.relation" id="_VD-2JdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VD-2JtLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VD-2J9LnEeiL7cTvwX--2w" ref="#_VASo8NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b9iK1NX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b9iK1dX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b9iK1tX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b9iK19X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b9iK2NX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b9iK2dX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b9iK2tX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cFMSBNX7Eeimuaax4vsiZw" name="FK_Client_PerimetreProchainProjet_ref_PerimetreProjet1">
        <node defType="com.stambia.rdbms.relation" id="_cFMSBdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cFMSBtX7Eeimuaax4vsiZw"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cFMSB9X7Eeimuaax4vsiZw" ref="#_VASo8NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_1EV4YOwOEeihWNvRK7oOPw" name="IdUnivers" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_1EV4YewOEeihWNvRK7oOPw" value="IdUnivers"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1EV4YuwOEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_1EV4Y-wOEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1EV4ZOwOEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1EV4ZewOEeihWNvRK7oOPw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1EV4ZuwOEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_1MK-tOwOEeihWNvRK7oOPw" name="FK_Client_PerimetreProchainProjet_ref_UniversConso">
        <node defType="com.stambia.rdbms.relation" id="_1MK-tewOEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_1MK-tuwOEeihWNvRK7oOPw" ref="#_1EV4YOwOEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUnivers?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_1MK-t-wOEeihWNvRK7oOPw" ref="#_UrYI4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UxuogdLnEeiL7cTvwX--2w" name="ref_EnvTouristique">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UxuogtLnEeiL7cTvwX--2w" value="ref_EnvTouristique"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Uxuog9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UxuohNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxuohdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxuohtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Uxuoh9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxuoiNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxuoidLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxuoitLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ux19QNLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ux19QdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ux19QtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ux19Q9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ux19RNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ux19RdLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ux3LYNLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ux3LYdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ux3LYtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ux3LY9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ux3LZNLnEeiL7cTvwX--2w" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ux3LZdLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_qKRVEduHEei1-oCUCrJoUg" value="0"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Ux3LZtLnEeiL7cTvwX--2w" name="PK_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.colref" id="_Ux3LZ9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Ux3LaNLnEeiL7cTvwX--2w" ref="#_UxuohNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XrbIsBEpEeqPGbHwfkinWw" name="LibEnvTouristique" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_XrbIsREpEeqPGbHwfkinWw" value="LibEnvTouristique"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XrbIshEpEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XrbIsxEpEeqPGbHwfkinWw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XrbItBEpEeqPGbHwfkinWw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XrbItREpEeqPGbHwfkinWw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UtqA1NLnEeiL7cTvwX--2w" name="Client">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UtqA1dLnEeiL7cTvwX--2w" value="Client"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UtqA1tLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UtwugNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtwugdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtwugtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Utwug9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UtwuhNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtwuhdLnEeiL7cTvwX--2w" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtwuhtLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ut2OENLnEeiL7cTvwX--2w" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ut2OEdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ut2OEtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ut2OE9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ut2OFNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ut2OFdLnEeiL7cTvwX--2w" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ut2OFtLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ut87wNLnEeiL7cTvwX--2w" name="IdSociete" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ut87wdLnEeiL7cTvwX--2w" value="IdSociete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ut87wtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ut87w9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ut87xNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ut87xdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ut87xtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UuIh8NLnEeiL7cTvwX--2w" name="RaisonSociale" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_UuIh8dLnEeiL7cTvwX--2w" value="RaisonSociale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UuIh8tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UuIh89LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UuIh9NLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UuIh9dLnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UuIh9tLnEeiL7cTvwX--2w" name="Enseigne" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_UuIh99LnEeiL7cTvwX--2w" value="Enseigne"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UuIh-NLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UuIh-dLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UuIh-tLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UuIh-9LnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UuUvMNLnEeiL7cTvwX--2w" name="Siret" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_UuUvMdLnEeiL7cTvwX--2w" value="Siret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UuUvMtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UuUvM9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UuUvNNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UuUvNdLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UuUvNtLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UuUvN9LnEeiL7cTvwX--2w" name="Nic" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_UuUvONLnEeiL7cTvwX--2w" value="Nic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UuUvOdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UuUvOtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UuUvO9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UuUvPNLnEeiL7cTvwX--2w" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UuUvPdLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UuUvPtLnEeiL7cTvwX--2w" name="Tva" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_UuUvP9LnEeiL7cTvwX--2w" value="Tva"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UuUvQNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UuUvQdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UuUvQtLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UuUvQ9LnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UuUvRNLnEeiL7cTvwX--2w" name="Naf" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_UuUvRdLnEeiL7cTvwX--2w" value="Naf"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UuUvRtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UuUvR9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UuUvSNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UuUvSdLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uubc4NLnEeiL7cTvwX--2w" name="TypeEtab1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uubc4dLnEeiL7cTvwX--2w" value="TypeEtab1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uubc4tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Uubc49LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uubc5NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uubc5dLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uubc5tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uubc59LnEeiL7cTvwX--2w" name="TypeEtab2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uubc6NLnEeiL7cTvwX--2w" value="TypeEtab2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uubc6dLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Uubc6tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uubc69LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uubc7NLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uubc7dLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uubc7tLnEeiL7cTvwX--2w" name="TypeEtab3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uubc79LnEeiL7cTvwX--2w" value="TypeEtab3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uubc8NLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Uubc8dLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uubc8tLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uubc89LnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uubc9NLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uug8cNLnEeiL7cTvwX--2w" name="DateCreation" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uug8cdLnEeiL7cTvwX--2w" value="DateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uug8ctLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Uug8c9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uug8dNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uug8ddLnEeiL7cTvwX--2w" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uug8dtLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uug8fdLnEeiL7cTvwX--2w" name="SiteWeb" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uug8ftLnEeiL7cTvwX--2w" value="SiteWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uug8f9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uug8gNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uug8gdLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uug8gtLnEeiL7cTvwX--2w" value="350"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UunqJtLnEeiL7cTvwX--2w" name="Langue" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_UunqJ9LnEeiL7cTvwX--2w" value="Langue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UunqKNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UunqKdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UunqKtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UunqK9LnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UunqLNLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UuzQV9LnEeiL7cTvwX--2w" name="TypeCompte" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_UuzQWNLnEeiL7cTvwX--2w" value="TypeCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UuzQWdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UuzQWtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UuzQW9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UuzQXNLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UuzQXdLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UuzQXtLnEeiL7cTvwX--2w" name="Groupe" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_UuzQX9LnEeiL7cTvwX--2w" value="Groupe"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UuzQYNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UuzQYdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UuzQYtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UuzQY9LnEeiL7cTvwX--2w" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UuzQZNLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UuzQbNLnEeiL7cTvwX--2w" name="International" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_UuzQbdLnEeiL7cTvwX--2w" value="International"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UuzQbtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UuzQb9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UuzQcNLnEeiL7cTvwX--2w" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UuzQcdLnEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uu5W8NLnEeiL7cTvwX--2w" name="Directif" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uu5W8dLnEeiL7cTvwX--2w" value="Directif"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uu5W8tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uu5W89LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uu5W9NLnEeiL7cTvwX--2w" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uu5W9dLnEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UvLq19LnEeiL7cTvwX--2w" name="DateCrtCompte" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_UvLq2NLnEeiL7cTvwX--2w" value="DateCrtCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UvLq2dLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UvLq2tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UvLq29LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UvLq3NLnEeiL7cTvwX--2w" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UvLq3dLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UvLq3tLnEeiL7cTvwX--2w" name="Statut" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_UvLq39LnEeiL7cTvwX--2w" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UvLq4NLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UvLq4dLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UvLq4tLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UvLq49LnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UvLq5NLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UvksYNLnEeiL7cTvwX--2w" name="DateMaj_CHD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_UvksYdLnEeiL7cTvwX--2w" value="DateMaj_CHD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UvksYtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UvksY9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UvksZNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UvksZdLnEeiL7cTvwX--2w" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UvksZtLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UvksZ9LnEeiL7cTvwX--2w" name="DateMaj_Orion" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_UvksaNLnEeiL7cTvwX--2w" value="DateMaj_Orion"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UvksadLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UvksatLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uvksa9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UvksbNLnEeiL7cTvwX--2w" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UvksbdLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UvnIoNLnEeiL7cTvwX--2w" name="DateMaj_Ccial" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_UvnIodLnEeiL7cTvwX--2w" value="DateMaj_Ccial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UvnIotLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UvnIo9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UvnIpNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UvnIpdLnEeiL7cTvwX--2w" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UvnIptLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UvnIp9LnEeiL7cTvwX--2w" name="CompteChomette" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_UvnIqNLnEeiL7cTvwX--2w" value="CompteChomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UvnIqdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UvnIqtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UvnIq9LnEeiL7cTvwX--2w" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UvnIrNLnEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UvsBINLnEeiL7cTvwX--2w" name="PK_Client">
        <node defType="com.stambia.rdbms.colref" id="_UvsBIdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UvsBItLnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjSwNLnEeiL7cTvwX--2w" name="FK_Client_ref_ColonneTarifaire">
        <node defType="com.stambia.rdbms.relation" id="_VCjSwdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjSwtLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjSw9LnEeiL7cTvwX--2w" ref="#_UsI94NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjSxNLnEeiL7cTvwX--2w" name="FK_Client_ref_FormeJuridique">
        <node defType="com.stambia.rdbms.relation" id="_VCjSxdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjSxtLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjSx9LnEeiL7cTvwX--2w" ref="#_U4Vm0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjSyNLnEeiL7cTvwX--2w" name="FK_Client_ref_Langue">
        <node defType="com.stambia.rdbms.relation" id="_VCjSydLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjSytLnEeiL7cTvwX--2w" ref="#_UunqJtLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Langue?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjSy9LnEeiL7cTvwX--2w" ref="#_U7JDNNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjSzNLnEeiL7cTvwX--2w" name="FK_Client_ref_NiveauFidelite">
        <node defType="com.stambia.rdbms.relation" id="_VCjSzdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjSztLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjSz9LnEeiL7cTvwX--2w" ref="#_UrmLUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS0NLnEeiL7cTvwX--2w" name="FK_Client_ref_NiveauStanding">
        <node defType="com.stambia.rdbms.relation" id="_VCjS0dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS0tLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS09LnEeiL7cTvwX--2w" ref="#_U1S54NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS1NLnEeiL7cTvwX--2w" name="FK_Client_ref_ScoringCA">
        <node defType="com.stambia.rdbms.relation" id="_VCjS1dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS1tLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS19LnEeiL7cTvwX--2w" ref="#_U5Gb0tLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS2NLnEeiL7cTvwX--2w" name="FK_Client_ref_ScoringFinance">
        <node defType="com.stambia.rdbms.relation" id="_VCjS2dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS2tLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS29LnEeiL7cTvwX--2w" ref="#_UyCxkNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS3NLnEeiL7cTvwX--2w" name="FK_Client_ref_Societe">
        <node defType="com.stambia.rdbms.relation" id="_VCjS3dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS3tLnEeiL7cTvwX--2w" ref="#_Ut87wNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdSociete?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS39LnEeiL7cTvwX--2w" ref="#_UtLfsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS4NLnEeiL7cTvwX--2w" name="FK_Client_ref_StatutClient">
        <node defType="com.stambia.rdbms.relation" id="_VCjS4dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS4tLnEeiL7cTvwX--2w" ref="#_UvLq3tLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Statut?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS49LnEeiL7cTvwX--2w" ref="#_U054UNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS5NLnEeiL7cTvwX--2w" name="FK_Client_ref_StatutCOLL">
        <node defType="com.stambia.rdbms.relation" id="_VCjS5dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS5tLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS59LnEeiL7cTvwX--2w" ref="#_UtYUANLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS6NLnEeiL7cTvwX--2w" name="FK_Client_ref_TicketRepasMoyen">
        <node defType="com.stambia.rdbms.relation" id="_VCjS6dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS6tLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS69LnEeiL7cTvwX--2w" ref="#_U0PJ9NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS7NLnEeiL7cTvwX--2w" name="FK_Client_ref_TrancheCA">
        <node defType="com.stambia.rdbms.relation" id="_VCjS7dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS7tLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS79LnEeiL7cTvwX--2w" ref="#_U_-f5NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS8NLnEeiL7cTvwX--2w" name="FK_Client_ref_TypeCompte">
        <node defType="com.stambia.rdbms.relation" id="_VCjS8dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS8tLnEeiL7cTvwX--2w" ref="#_UuzQV9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeCompte?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS89LnEeiL7cTvwX--2w" ref="#_U9yHhNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCjS9NLnEeiL7cTvwX--2w" name="FK_Client_ref_TypeEtablissement">
        <node defType="com.stambia.rdbms.relation" id="_VCjS9dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCjS9tLnEeiL7cTvwX--2w" ref="#_Uubc4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeEtab1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCjS99LnEeiL7cTvwX--2w" ref="#_U2VbsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VClvANLnEeiL7cTvwX--2w" name="FK_Client_ref_TypeEtablissement1">
        <node defType="com.stambia.rdbms.relation" id="_VClvAdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VClvAtLnEeiL7cTvwX--2w" ref="#_Uubc59LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeEtab2?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VClvA9LnEeiL7cTvwX--2w" ref="#_U2VbsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VClvBNLnEeiL7cTvwX--2w" name="FK_Client_ref_TypeEtablissement2">
        <node defType="com.stambia.rdbms.relation" id="_VClvBdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VClvBtLnEeiL7cTvwX--2w" ref="#_Uubc7tLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeEtab3?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VClvB9LnEeiL7cTvwX--2w" ref="#_U2VbsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VClvCNLnEeiL7cTvwX--2w" name="FK_Client_ref_TypeZone">
        <node defType="com.stambia.rdbms.relation" id="_VClvCdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VClvCtLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VClvC9LnEeiL7cTvwX--2w" ref="#_U7BHYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LgCg7tesEei1-oCUCrJoUg" name="Siren" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_LgCg79esEei1-oCUCrJoUg" value="Siren"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LgCg8NesEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LgCg8desEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LgCg8tesEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LgCg89esEei1-oCUCrJoUg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LgCg9NesEei1-oCUCrJoUg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_uxx-BNexEei1-oCUCrJoUg" name="FK_Client_ref_Pays">
        <node defType="com.stambia.rdbms.relation" id="_uxx-BdexEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_uxx-BtexEei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_uxx-B9exEei1-oCUCrJoUg" ref="#_Uv4OYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_PlHbtNezEei1-oCUCrJoUg" name="FK_Client_ref_Departement">
        <node defType="com.stambia.rdbms.relation" id="_PlHbtdezEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_PlHbttezEei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_PlHbt9ezEei1-oCUCrJoUg" ref="#_PQX6wNezEei1-oCUCrJoUg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_YQJANNe1Eei1-oCUCrJoUg" name="FK_Client_ref_Commune">
        <node defType="com.stambia.rdbms.relation" id="_YQJANde1Eei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_YQJANte1Eei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_YQJAN9e1Eei1-oCUCrJoUg" ref="#_X-xjZ9e1Eei1-oCUCrJoUg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_acKpRthtEei1-oCUCrJoUg" name="DateSuppression" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_acKpR9htEei1-oCUCrJoUg" value="DateSuppression"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_acKpSNhtEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_acKpSdhtEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_acKpSthtEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_acKpS9htEei1-oCUCrJoUg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_acKpTNhtEei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_acKpTdhtEei1-oCUCrJoUg" name="TopSuppression" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_acKpTthtEei1-oCUCrJoUg" value="TopSuppression"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_acKpT9htEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_acKpUNhtEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_acKpUdhtEei1-oCUCrJoUg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_acKpUthtEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ToO8d9kfEei1-oCUCrJoUg" name="CodeSocMagento" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_ToO8eNkfEei1-oCUCrJoUg" value="CodeSocMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ToO8edkfEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ToO8etkfEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ToO8e9kfEei1-oCUCrJoUg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ToO8fNkfEei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TqSx8NkfEei1-oCUCrJoUg" name="CanalAcquisition" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_TqSx8dkfEei1-oCUCrJoUg" value="CanalAcquisition"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TqSx8tkfEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TqSx89kfEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TqSx9NkfEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TqSx9dkfEei1-oCUCrJoUg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TqSx9tkfEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_UAo1YNkfEei1-oCUCrJoUg" name="FK_Client_ref_Canal">
        <node defType="com.stambia.rdbms.relation" id="_UAo1YdkfEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_UAo1YtkfEei1-oCUCrJoUg" ref="#_TqSx8NkfEei1-oCUCrJoUg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=CanalAcquisition?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_UAo1Y9kfEei1-oCUCrJoUg" ref="#_Uz2vcNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_UAo1eNkfEei1-oCUCrJoUg" name="FK_Client_ref_NbChambres">
        <node defType="com.stambia.rdbms.relation" id="_UAo1edkfEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_UAo1etkfEei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_UAo1e9kfEei1-oCUCrJoUg" ref="#_U2iQBNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_UAo1fNkfEei1-oCUCrJoUg" name="FK_Client_ref_NbCouvertJour">
        <node defType="com.stambia.rdbms.relation" id="_UAo1fdkfEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_UAo1ftkfEei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_UAo1f9kfEei1-oCUCrJoUg" ref="#_U7Zh4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_UAo1gNkfEei1-oCUCrJoUg" name="FK_Client_ref_NbEleves">
        <node defType="com.stambia.rdbms.relation" id="_UAo1gdkfEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_UAo1gtkfEei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_UAo1g9kfEei1-oCUCrJoUg" ref="#_VAut0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_UAo1hNkfEei1-oCUCrJoUg" name="FK_Client_ref_NbLits">
        <node defType="com.stambia.rdbms.relation" id="_UAo1hdkfEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_UAo1htkfEei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_UAo1h9kfEei1-oCUCrJoUg" ref="#_UyO-0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_UAo1iNkfEei1-oCUCrJoUg" name="FK_Client_ref_NbRepasJour">
        <node defType="com.stambia.rdbms.relation" id="_UAo1idkfEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_UAo1itkfEei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_UAo1i9kfEei1-oCUCrJoUg" ref="#_U0oLgNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_UDJ94NkfEei1-oCUCrJoUg" name="FK_Client_ref_ComportementMultiCanal">
        <node defType="com.stambia.rdbms.relation" id="_UDJ94dkfEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_UDJ94tkfEei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_UDJ949kfEei1-oCUCrJoUg" ref="#_UC2b4NkfEei1-oCUCrJoUg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_RmccNNklEei1-oCUCrJoUg" name="FK_Client_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.relation" id="_RmccNdklEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_RmccNtklEei1-oCUCrJoUg"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_RmccN9klEei1-oCUCrJoUg" ref="#_UxuohNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_e_qw1t0nEeiUprvGXSQ59A" name="CHDID" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_e_qw190nEeiUprvGXSQ59A" value="CHDID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_e_qw2N0nEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_e_qw2d0nEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_e_qw2t0nEeiUprvGXSQ59A" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_e_qw290nEeiUprvGXSQ59A" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_N2kilt5_Eei89e-w6JO_mw" name="Latitude" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_N2kil95_Eei89e-w6JO_mw" value="Latitude"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_N2kimN5_Eei89e-w6JO_mw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_N2kimd5_Eei89e-w6JO_mw" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_N2kimt5_Eei89e-w6JO_mw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_N2kim95_Eei89e-w6JO_mw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_N2kinN5_Eei89e-w6JO_mw" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_N2kind5_Eei89e-w6JO_mw" name="Longitude" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_N2kint5_Eei89e-w6JO_mw" value="Longitude"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_N2kin95_Eei89e-w6JO_mw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_N2kioN5_Eei89e-w6JO_mw" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_N2kiod5_Eei89e-w6JO_mw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_N2kiot5_Eei89e-w6JO_mw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_N2kio95_Eei89e-w6JO_mw" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_1k8PYN7BEei89e-w6JO_mw" name="FK_Client_ref_NbEmployes">
        <node defType="com.stambia.rdbms.relation" id="_1k8PYd7BEei89e-w6JO_mw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_1k8PYt7BEei89e-w6JO_mw"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_1k8PY97BEei89e-w6JO_mw" ref="#_U_sMBNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_B703IN7DEei89e-w6JO_mw" name="Propriete" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_B703Id7DEei89e-w6JO_mw" value="Propriete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_B703It7DEei89e-w6JO_mw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_B703I97DEei89e-w6JO_mw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_B703JN7DEei89e-w6JO_mw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_B703Jd7DEei89e-w6JO_mw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_B703Jt7DEei89e-w6JO_mw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_CMUxEN7DEei89e-w6JO_mw" name="FK_Client_ref_Propriété">
        <node defType="com.stambia.rdbms.relation" id="_CMUxEd7DEei89e-w6JO_mw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_CMUxEt7DEei89e-w6JO_mw" ref="#_B703IN7DEei89e-w6JO_mw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Propriete?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_CMUxE97DEei89e-w6JO_mw" ref="#_CMBPEN7DEei89e-w6JO_mw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_CMUxFN7DEei89e-w6JO_mw" name="FK_Client_ref_TypeOperation">
        <node defType="com.stambia.rdbms.relation" id="_CMUxFd7DEei89e-w6JO_mw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_CMUxFt7DEei89e-w6JO_mw"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_CMUxF97DEei89e-w6JO_mw" ref="#_CLzztN7DEei89e-w6JO_mw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_-7BdcOQxEeigXob0_5VVUQ" name="FK_Client_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_-7BdceQxEeigXob0_5VVUQ" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_-7BdcuQxEeigXob0_5VVUQ" ref="#_Uubc4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeEtab1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_-7Bdc-QxEeigXob0_5VVUQ" ref="#_-zq4IOQxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_-7KnQOQxEeigXob0_5VVUQ" name="FK_Client_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.relation" id="_-7KnQeQxEeigXob0_5VVUQ" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_-7KnQuQxEeigXob0_5VVUQ" ref="#_Uubc59LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeEtab2?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_-7KnQ-QxEeigXob0_5VVUQ" ref="#_-zX9NOQxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_-7KnROQxEeigXob0_5VVUQ" name="FK_Client_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.relation" id="_-7KnReQxEeigXob0_5VVUQ" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_-7KnRuQxEeigXob0_5VVUQ" ref="#_Uubc7tLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeEtab3?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_-7KnR-QxEeigXob0_5VVUQ" ref="#_-y7RR-QxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_bRjLoOjgEeicCszix7eWDA" name="Nature" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_bRjLoejgEeicCszix7eWDA" value="Nature"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_bRjLoujgEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_bRjLo-jgEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_bRjLpOjgEeicCszix7eWDA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_bRjLpejgEeicCszix7eWDA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VlXHNvh-EeiL3uw9NeuELg" name="DateMaj_Web" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_VlXHN_h-EeiL3uw9NeuELg" value="DateMaj_Web"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VlXHOPh-EeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VlXHOfh-EeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VlXHOvh-EeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VlXHO_h-EeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VlXHPPh-EeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_g2KhbfoQEeiL3uw9NeuELg" name="MailPrincipal" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_g2KhbvoQEeiL3uw9NeuELg" value="MailPrincipal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_g2Khb_oQEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_g2KhcPoQEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_g2KhcfoQEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_g2KhcvoQEeiL3uw9NeuELg" value="150"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U3G3wdLnEeiL7cTvwX--2w" name="Client_JourOuvert">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U3G3wtLnEeiL7cTvwX--2w" value="Client_JourOuvert"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U3G3w9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U3MXUNLnEeiL7cTvwX--2w" name="IdJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U3MXUdLnEeiL7cTvwX--2w" value="IdJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U3MXUtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U3MXU9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U3MXVNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U3MXVdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U3MXVtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U3MXV9LnEeiL7cTvwX--2w" name="PK_Client_JourOuvert">
        <node defType="com.stambia.rdbms.colref" id="_U3MXWNLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U3MXWdLnEeiL7cTvwX--2w" ref="#_b6qdANX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U3MXWtLnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U3MXW9LnEeiL7cTvwX--2w" ref="#_U3MXUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdJour?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDgVANLnEeiL7cTvwX--2w" name="FK_Client_JourOuvert_Client">
        <node defType="com.stambia.rdbms.relation" id="_VDgVAdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDgVAtLnEeiL7cTvwX--2w" ref="#_b6qdANX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDgVA9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDgVBNLnEeiL7cTvwX--2w" name="FK_Client_JourOuvert_ref_Jours">
        <node defType="com.stambia.rdbms.relation" id="_VDgVBdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDgVBtLnEeiL7cTvwX--2w" ref="#_U3MXUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDgVB9LnEeiL7cTvwX--2w" ref="#_U0C8tNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b6qdANX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b6qdAdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b6qdAtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b6qdA9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b6qdBNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b6qdBdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b6qdBtX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cE5XFNX7Eeimuaax4vsiZw" name="FK_Client_JourOuvert_ref_Jours1">
        <node defType="com.stambia.rdbms.relation" id="_cE5XFdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cE5XFtX7Eeimuaax4vsiZw" ref="#_U3MXUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cE5XF9X7Eeimuaax4vsiZw" ref="#_U0C8tNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UyBjcdLnEeiL7cTvwX--2w" name="ref_ScoringFinance">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UyBjctLnEeiL7cTvwX--2w" value="ref_ScoringFinance"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UyBjc9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UyCxkNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UyCxkdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UyCxktLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UyCxk9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UyCxlNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UyCxldLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UyCxltLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UyCxl9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UyCxmNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UyCxmdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UyCxmtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UyCxm9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UyCxnNLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UyCxndLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UyCxntLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UyCxn9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UyCxoNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UyCxodLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UyCxotLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UyJfQNLnEeiL7cTvwX--2w" name="PK_ref_ScoringFinance">
        <node defType="com.stambia.rdbms.colref" id="_UyJfQdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UyJfQtLnEeiL7cTvwX--2w" ref="#_UyCxkNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yWWs0DbREeqF3cPVFXUsDw" name="LibScoringFinance" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yWWs0TbREeqF3cPVFXUsDw" value="LibScoringFinance"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yWWs0jbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yWWs0zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yWWs1DbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yWWs1TbREeqF3cPVFXUsDw" value="20"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U2iQAdLnEeiL7cTvwX--2w" name="ref_NbChambres">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U2iQAtLnEeiL7cTvwX--2w" value="ref_NbChambres"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U2iQA9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U2iQBNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2iQBdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2iQBtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U2iQB9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2iQCNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2iQCdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2iQCtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U2nvkNLnEeiL7cTvwX--2w" name="NbChambres" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2nvkdLnEeiL7cTvwX--2w" value="NbChambres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2nvktLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2nvk9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2nvlNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2nvldLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U2nvltLnEeiL7cTvwX--2w" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2nvl9LnEeiL7cTvwX--2w" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2nvmNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U2nvmdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2nvmtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2nvm9LnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2nvnNLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U2nvndLnEeiL7cTvwX--2w" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2nvntLnEeiL7cTvwX--2w" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2nvn9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U2nvoNLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2nvodLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2nvotLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2nvo9LnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U2nvpNLnEeiL7cTvwX--2w" name="PK_ref_NbChambres">
        <node defType="com.stambia.rdbms.colref" id="_U2nvpdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U2nvptLnEeiL7cTvwX--2w" ref="#_U2iQBNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_VBHvYdLnEeiL7cTvwX--2w" name="ref_Distinctions">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_VBHvYtLnEeiL7cTvwX--2w" value="ref_Distinctions"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_VBHvY9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_VBKLoNLnEeiL7cTvwX--2w" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_VBKLodLnEeiL7cTvwX--2w" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VBKLotLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VBKLo9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VBKLpNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VBKLpdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VBKLptLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VBN2ANLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_VBN2AdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VBN2AtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VBN2A9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VBN2BNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VBN2BdLnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VBQSQNLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_VBQSQdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VBQSQtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VBQSQ9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VBQSRNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VBQSRdLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_VBTVkNLnEeiL7cTvwX--2w" name="PK_ref_Distinctions">
        <node defType="com.stambia.rdbms.colref" id="_VBTVkdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_VBTVktLnEeiL7cTvwX--2w" ref="#_VBKLoNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ym8GgDbREeqF3cPVFXUsDw" name="LibDistinction" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ym8GgTbREeqF3cPVFXUsDw" value="LibDistinction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ym8GgjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ym8GgzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ym8GhDbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ym8GhTbREeqF3cPVFXUsDw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U4uBUdLnEeiL7cTvwX--2w" name="Client_PerimetreDernierProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U4uBUtLnEeiL7cTvwX--2w" value="Client_PerimetreDernierProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U4uBU9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.pk" id="_U5AVMNLnEeiL7cTvwX--2w" name="PK_Client_PerimetreDernierProjet">
        <node defType="com.stambia.rdbms.colref" id="_U5AVMdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U5AVMtLnEeiL7cTvwX--2w" ref="#_b8bXlNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U5AVM9LnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U5AVNNLnEeiL7cTvwX--2w" ref="#_1Ck90OwOEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUnivers?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDyo4NLnEeiL7cTvwX--2w" name="FK_Client_PerimetreDernierProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_VDyo4dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDyo4tLnEeiL7cTvwX--2w" ref="#_b8bXlNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDyo49LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDyo5NLnEeiL7cTvwX--2w" name="FK_Client_PerimetreDernierProjet_ref_PerimetreProjet">
        <node defType="com.stambia.rdbms.relation" id="_VDyo5dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDyo5tLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDyo59LnEeiL7cTvwX--2w" ref="#_VASo8NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b8bXlNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b8bXldX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b8bXltX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b8bXl9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b8bXmNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b8bXmdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b8bXmtX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cFDIFNX7Eeimuaax4vsiZw" name="FK_Client_PerimetreDernierProjet_ref_PerimetreProjet1">
        <node defType="com.stambia.rdbms.relation" id="_cFDIFdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cFDIFtX7Eeimuaax4vsiZw"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cFDIF9X7Eeimuaax4vsiZw" ref="#_VASo8NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_1Ck90OwOEeihWNvRK7oOPw" name="IdUnivers" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_1Ck90ewOEeihWNvRK7oOPw" value="IdUnivers"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1Ck90uwOEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_1Ck90-wOEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1Ck91OwOEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1Ck91ewOEeihWNvRK7oOPw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1Ck91uwOEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_1L88ROwOEeihWNvRK7oOPw" name="FK_Client_PerimetreDernierProjet_ref_UniversConso">
        <node defType="com.stambia.rdbms.relation" id="_1L88RewOEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_1L88RuwOEeihWNvRK7oOPw" ref="#_1Ck90OwOEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUnivers?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_1L88R-wOEeihWNvRK7oOPw" ref="#_UrYI4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U60TEdLnEeiL7cTvwX--2w" name="ref_NiveauUser">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U60TEtLnEeiL7cTvwX--2w" value="ref_NiveauUser"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U60TE9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U60TFNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U60TFdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U60TFtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U60TF9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U60TGNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U60TGdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U60TGtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U60TG9LnEeiL7cTvwX--2w" name="LibNiveau" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U60THNLnEeiL7cTvwX--2w" value="LibNiveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U60THdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U60THtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U60TH9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U60TINLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U60TIdLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U66ZsNLnEeiL7cTvwX--2w" name="PK_ref_NiveauUser">
        <node defType="com.stambia.rdbms.colref" id="_U66ZsdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U66ZstLnEeiL7cTvwX--2w" ref="#_U60TFNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UyVFdNLnEeiL7cTvwX--2w" name="Client_ActiviteSegment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UyVFddLnEeiL7cTvwX--2w" value="Client_ActiviteSegment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UyVFdtLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UybMF9LnEeiL7cTvwX--2w" name="IdActivite" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UybMGNLnEeiL7cTvwX--2w" value="IdActivite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UybMGdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UybMGtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UybMG9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UybMHNLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UybMHdLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UybMHtLnEeiL7cTvwX--2w" name="PK_Client_ActiviteSegment">
        <node defType="com.stambia.rdbms.colref" id="_UybMH9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UybMINLnEeiL7cTvwX--2w" ref="#_b1u5sNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_UybMIdLnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UybMItLnEeiL7cTvwX--2w" ref="#_UybMF9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdActivite?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VC0_kNLnEeiL7cTvwX--2w" name="FK_Client_ActiviteSegment_Client">
        <node defType="com.stambia.rdbms.relation" id="_VC0_kdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VC0_ktLnEeiL7cTvwX--2w" ref="#_b1u5sNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VC0_k9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VC0_lNLnEeiL7cTvwX--2w" name="FK_Client_ActiviteSegment_ref_ActiviteSegment">
        <node defType="com.stambia.rdbms.relation" id="_VC0_ldLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VC0_ltLnEeiL7cTvwX--2w" ref="#_UybMF9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdActivite?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VC0_l9LnEeiL7cTvwX--2w" ref="#_U6os4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b1u5sNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b1u5sdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b1u5stX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b1u5s9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b1u5tNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b1u5tdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b1u5ttX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cEcrJNX7Eeimuaax4vsiZw" name="FK_Client_ActiviteSegment_ref_ActiviteSegment1">
        <node defType="com.stambia.rdbms.relation" id="_cEcrJdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cEcrJtX7Eeimuaax4vsiZw" ref="#_UybMF9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdActivite?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cEcrJ9X7Eeimuaax4vsiZw" ref="#_U6os4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U8ixYdLnEeiL7cTvwX--2w" name="Client_UserWeb">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U8ixYtLnEeiL7cTvwX--2w" value="Client_UserWeb"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U8ixY9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U8o4ANLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U8o4AdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U8o4AtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U8o4A9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U8o4BNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U8o4BdLnEeiL7cTvwX--2w" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U8o4BtLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U8uXl9LnEeiL7cTvwX--2w" name="IdGenre" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U8uXmNLnEeiL7cTvwX--2w" value="IdGenre"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U8uXmdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U8uXmtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U8uXm9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U8uXnNLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U8uXndLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U8uXntLnEeiL7cTvwX--2w" name="Nom" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_U8uXn9LnEeiL7cTvwX--2w" value="Nom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U8uXoNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U8uXodLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U8uXotLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U8uXo9LnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U80eMNLnEeiL7cTvwX--2w" name="Prenom" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_U80eMdLnEeiL7cTvwX--2w" value="Prenom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U80eMtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U80eM9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U80eNNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U80eNdLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U80eNtLnEeiL7cTvwX--2w" name="IntitulePoste" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_U80eN9LnEeiL7cTvwX--2w" value="IntitulePoste"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U80eONLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U80eOdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U80eOtLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U80eO9LnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U80ePNLnEeiL7cTvwX--2w" name="Mail" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_U80ePdLnEeiL7cTvwX--2w" value="Mail"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U80ePtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U80eP9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U80eQNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U80eQdLnEeiL7cTvwX--2w" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U80eQtLnEeiL7cTvwX--2w" name="IdWeb" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_U80eQ9LnEeiL7cTvwX--2w" value="IdWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U80eRNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U80eRdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U80eRtLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U80eR9LnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U86k19LnEeiL7cTvwX--2w" name="IdTypeComSouhait" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_U86k2NLnEeiL7cTvwX--2w" value="IdTypeComSouhait"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U86k2dLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U86k2tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U86k29LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U86k3NLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U86k3dLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U9M4sNLnEeiL7cTvwX--2w" name="DateCrt" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_U9M4sdLnEeiL7cTvwX--2w" value="DateCrt"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U9M4stLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U9M4s9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U9M4tNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U9M4tdLnEeiL7cTvwX--2w" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U9M4ttLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U9M4t9LnEeiL7cTvwX--2w" name="DateSup" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_U9M4uNLnEeiL7cTvwX--2w" value="DateSup"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U9M4udLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U9M4utLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U9M4u9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U9M4vNLnEeiL7cTvwX--2w" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U9M4vdLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U9M4vtLnEeiL7cTvwX--2w" name="TopSup" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_U9M4v9LnEeiL7cTvwX--2w" value="TopSup"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U9M4wNLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U9M4wdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U9M4wtLnEeiL7cTvwX--2w" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U9M4w9LnEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U9ZtANLnEeiL7cTvwX--2w" name="IdNiveau" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_U9ZtAdLnEeiL7cTvwX--2w" value="IdNiveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U9ZtAtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U9ZtA9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U9ZtBNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U9ZtBdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U9ZtBtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U9ZtB9LnEeiL7cTvwX--2w" name="IdUserMagento" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_U9ZtCNLnEeiL7cTvwX--2w" value="IdUserMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U9ZtCdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U9ZtCtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U9ZtC9LnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U9ZtDNLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U9fMkNLnEeiL7cTvwX--2w" name="PK_Contact">
        <node defType="com.stambia.rdbms.colref" id="_U9fMkdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U9fMktLnEeiL7cTvwX--2w" ref="#_U8o4ANLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VERKCNLnEeiL7cTvwX--2w" name="FK_Client_UserWeb_Client">
        <node defType="com.stambia.rdbms.relation" id="_VERKCdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VERKCtLnEeiL7cTvwX--2w" ref="#_cAQHp9X7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VERKC9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VEWpkNLnEeiL7cTvwX--2w" name="FK_Client_UserWeb_Client_Telephone">
        <node defType="com.stambia.rdbms.relation" id="_VEWpkdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VEWpktLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VEWpk9LnEeiL7cTvwX--2w" ref="#_U1GFkNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VEWplNLnEeiL7cTvwX--2w" name="FK_Client_UserWeb_ref_Genre">
        <node defType="com.stambia.rdbms.relation" id="_VEWpldLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VEWpltLnEeiL7cTvwX--2w" ref="#_U8uXl9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdGenre?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VEWpl9LnEeiL7cTvwX--2w" ref="#_U_btUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VEWpmNLnEeiL7cTvwX--2w" name="FK_Client_UserWeb_ref_NiveauUser">
        <node defType="com.stambia.rdbms.relation" id="_VEWpmdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VEWpmtLnEeiL7cTvwX--2w" ref="#_U9ZtANLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdNiveau?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VEWpm9LnEeiL7cTvwX--2w" ref="#_U60TFNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VEWpnNLnEeiL7cTvwX--2w" name="FK_Client_UserWeb_ref_TypeComSouhaite">
        <node defType="com.stambia.rdbms.relation" id="_VEWpndLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VEWpntLnEeiL7cTvwX--2w" ref="#_U86k19LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdTypeComSouhait?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VEWpn9LnEeiL7cTvwX--2w" ref="#_U_Nq4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cAQHp9X7Eeimuaax4vsiZw" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_cAQHqNX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cAQHqdX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cAQHqtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cAQHq9X7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cAQHrNX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cAQHrdX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_T7M7x9kfEei1-oCUCrJoUg" name="OptinEmail" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_T7M7yNkfEei1-oCUCrJoUg" value="OptinEmail"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_T7M7ydkfEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_T7M7ytkfEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_T7M7y9kfEei1-oCUCrJoUg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_T7M7zNkfEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_T7Ws2tkfEei1-oCUCrJoUg" name="OptinSMS" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_T7Ws29kfEei1-oCUCrJoUg" value="OptinSMS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_T7Ws3NkfEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_T7Ws3dkfEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_T7Ws3tkfEei1-oCUCrJoUg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_T7Ws39kfEei1-oCUCrJoUg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U8P2cdLnEeiL7cTvwX--2w" name="ref_NewsletterType">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U8P2ctLnEeiL7cTvwX--2w" value="ref_NewsletterType"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U8P2c9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U8WkINLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U8WkIdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U8WkItLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U8WkI9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U8WkJNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U8WkJdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U8WkJtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U8WkJ9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U8WkKNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U8WkKdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U8WkKtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U8WkK9LnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U8WkLNLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U8WkLdLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U8WkLtLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U8WkL9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U8WkMNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U8WkMdLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U8WkMtLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U8cqwNLnEeiL7cTvwX--2w" name="PK_ref_NewsletterType">
        <node defType="com.stambia.rdbms.colref" id="_U8cqwdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U8cqwtLnEeiL7cTvwX--2w" ref="#_U8WkINLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yiRB4DbREeqF3cPVFXUsDw" name="LibType" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yiRB4TbREeqF3cPVFXUsDw" value="LibType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yiRB4jbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yiRB4zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yiRB5DbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yiRB5TbREeqF3cPVFXUsDw" value="30"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U6WZA9LnEeiL7cTvwX--2w" name="Client_SpeCulinaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U6WZBNLnEeiL7cTvwX--2w" value="Client_SpeCulinaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U6WZBdLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U6cfp9LnEeiL7cTvwX--2w" name="IdSpeCulinaire" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U6cfqNLnEeiL7cTvwX--2w" value="IdSpeCulinaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U6cfqdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U6cfqtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U6cfq9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U6cfrNLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U6cfrdLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U6cfrtLnEeiL7cTvwX--2w" name="PK_Client_SpeCulinaire">
        <node defType="com.stambia.rdbms.colref" id="_U6cfr9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U6cfsNLnEeiL7cTvwX--2w" ref="#_b-MSJNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U6cfsdLnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U6cfstLnEeiL7cTvwX--2w" ref="#_U6cfp9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdSpeCulinaire?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VD-2MNLnEeiL7cTvwX--2w" name="FK_Client_SpeCulinaire_Client">
        <node defType="com.stambia.rdbms.relation" id="_VD-2MdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VD-2MtLnEeiL7cTvwX--2w" ref="#_b-MSJNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VD-2M9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VD-2NNLnEeiL7cTvwX--2w" name="FK_Client_SpeCulinaire_ref_SpecialiteCulinaire">
        <node defType="com.stambia.rdbms.relation" id="_VD-2NdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VD-2NtLnEeiL7cTvwX--2w" ref="#_U6cfp9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdSpeCulinaire?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VD-2N9LnEeiL7cTvwX--2w" ref="#_U5334NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b-MSJNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b-MSJdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b-MSJtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b-MSJ9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b-MSKNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b-MSKdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b-MSKtX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cFQjdNX7Eeimuaax4vsiZw" name="FK_Client_SpeCulinaire_ref_SpecialiteCulinaire1">
        <node defType="com.stambia.rdbms.relation" id="_cFQjddX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cFQjdtX7Eeimuaax4vsiZw" ref="#_U6cfp9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdSpeCulinaire?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cFQjd9X7Eeimuaax4vsiZw" ref="#_U5334NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U1AmBNLnEeiL7cTvwX--2w" name="Client_Telephone">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U1AmBdLnEeiL7cTvwX--2w" value="Client_Telephone"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U1AmBtLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U1GFkNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U1GFkdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U1GFktLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U1GFk9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U1GFlNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U1GFldLnEeiL7cTvwX--2w" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U1GFltLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U1GFl9LnEeiL7cTvwX--2w" name="Numero" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U1GFmNLnEeiL7cTvwX--2w" value="Numero"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U1GFmdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U1GFmtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U1GFm9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U1GFnNLnEeiL7cTvwX--2w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U1GFndLnEeiL7cTvwX--2w" name="IdType" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_U1GFntLnEeiL7cTvwX--2w" value="IdType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U1GFn9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U1GFoNLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U1GFodLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U1GFotLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U1GFo9LnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U1MMMNLnEeiL7cTvwX--2w" name="PK_Telephone">
        <node defType="com.stambia.rdbms.colref" id="_U1MMMdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U1MMMtLnEeiL7cTvwX--2w" ref="#_U1GFkNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDHTcNLnEeiL7cTvwX--2w" name="FK_Telephone_ref_TypeTel">
        <node defType="com.stambia.rdbms.relation" id="_VDHTcdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDHTctLnEeiL7cTvwX--2w" ref="#_U1GFndLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdType?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDHTc9LnEeiL7cTvwX--2w" ref="#_U2Du4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_27BbR9YXEeimuaax4vsiZw" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_27BbSNYXEeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_27BbSdYXEeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_27BbStYXEeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_27BbS9YXEeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_27BbTNYXEeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_27BbTdYXEeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_3E4h4NYXEeimuaax4vsiZw" name="FK_Client_Telephone_Client">
        <node defType="com.stambia.rdbms.relation" id="_3E4h4dYXEeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_3E4h4tYXEeimuaax4vsiZw" ref="#_27BbR9YXEeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_3E4h49YXEeimuaax4vsiZw" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UtUpodLnEeiL7cTvwX--2w" name="ref_StatutCOLL">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UtUpotLnEeiL7cTvwX--2w" value="ref_StatutCOLL"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UtUpo9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UtYUANLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtYUAdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtYUAtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UtYUA9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UtYUBNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtYUBdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtYUBtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UtYUB9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtYUCNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtYUCdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UtYUCtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtYUC9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtYUDNLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UtdzkNLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtdzkdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtdzktLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Utdzk9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtdzlNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtdzldLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UtdzltLnEeiL7cTvwX--2w" name="PK_StatutCOLL">
        <node defType="com.stambia.rdbms.colref" id="_Utdzl9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UtdzmNLnEeiL7cTvwX--2w" ref="#_UtYUANLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yRok4DbREeqF3cPVFXUsDw" name="LibStatutCOLL" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yRok4TbREeqF3cPVFXUsDw" value="LibStatutCOLL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yRok4jbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yRok4zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yRok5DbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yRok5TbREeqF3cPVFXUsDw" value="30"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UrdocdLnEeiL7cTvwX--2w" name="ref_NiveauFidelite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UrdoctLnEeiL7cTvwX--2w" value="ref_NiveauFidelite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Urdoc9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UrmLUNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UrmLUdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UrmLUtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UrmLU9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UrmLVNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UrmLVdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UrmLVtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ury_oNLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ury_odLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ury_otLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ury_o9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ury_pNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ury_pdLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ur2C8NLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ur2C8dLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ur2C8tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ur2C89LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ur2C9NLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ur2C9dLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Ur8JkNLnEeiL7cTvwX--2w" name="PK_ref_NiveauFidelite">
        <node defType="com.stambia.rdbms.colref" id="_Ur8JkdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Ur8JktLnEeiL7cTvwX--2w" ref="#_UrmLUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yP-_EDbREeqF3cPVFXUsDw" name="LibNivFidelite" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yP-_ETbREeqF3cPVFXUsDw" value="LibNivFidelite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yP-_EjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yP-_EzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yP-_FDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yP-_FTbREeqF3cPVFXUsDw" value="20"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UxgmEdLnEeiL7cTvwX--2w" name="ref_MoyenPaiement">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UxgmEtLnEeiL7cTvwX--2w" value="ref_MoyenPaiement"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UxgmE9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UxmssNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxmssdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxmsstLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Uxmss9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxmstNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxmstdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxmsttLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uxmst9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxmsuNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxmsudLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxmsutLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uxmsu9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxmsvNLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UxpI8NLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UxpI8dLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UxpI8tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UxpI89LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UxpI9NLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UxpI9dLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UxszUNLnEeiL7cTvwX--2w" name="PK_ref_MoyenPaiement">
        <node defType="com.stambia.rdbms.colref" id="_UxszUdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UxszUtLnEeiL7cTvwX--2w" ref="#_UxmssNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U3t7wdLnEeiL7cTvwX--2w" name="Client_UserWeb_News">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U3t7wtLnEeiL7cTvwX--2w" value="Client_UserWeb_News"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U3t7w9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U3w_ENLnEeiL7cTvwX--2w" name="IdUserWeb" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U3w_EdLnEeiL7cTvwX--2w" value="IdUserWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U3w_EtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U3w_E9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U3w_FNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U3w_FdLnEeiL7cTvwX--2w" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U3w_FtLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U39MUNLnEeiL7cTvwX--2w" name="IdNews" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U39MUdLnEeiL7cTvwX--2w" value="IdNews"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U39MUtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U39MU9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U39MVNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U39MVdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U39MVtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U3_okNLnEeiL7cTvwX--2w" name="PK_Client_Contact_News">
        <node defType="com.stambia.rdbms.colref" id="_U3_okdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U3_oktLnEeiL7cTvwX--2w" ref="#_U3w_ENLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUserWeb?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U3_ok9LnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U3_olNLnEeiL7cTvwX--2w" ref="#_U39MUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdNews?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDmbqNLnEeiL7cTvwX--2w" name="FK_Client_UserWeb_News_Client_UserWeb">
        <node defType="com.stambia.rdbms.relation" id="_VDmbqdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDmbqtLnEeiL7cTvwX--2w" ref="#_U3w_ENLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUserWeb?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDmbq9LnEeiL7cTvwX--2w" ref="#_U8o4ANLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDmbrNLnEeiL7cTvwX--2w" name="FK_Client_UserWeb_News_ref_NewsletterType">
        <node defType="com.stambia.rdbms.relation" id="_VDmbrdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDmbrtLnEeiL7cTvwX--2w" ref="#_U39MUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdNews?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDmbr9LnEeiL7cTvwX--2w" ref="#_U8WkINLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U4DS8dLnEeiL7cTvwX--2w" name="Client_ActualiteProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U4DS8tLnEeiL7cTvwX--2w" value="Client_ActualiteProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U4DS89LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U4L10NLnEeiL7cTvwX--2w" name="IdProjet" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U4L10dLnEeiL7cTvwX--2w" value="IdProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U4L10tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U4L109LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U4L11NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U4L11dLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U4L11tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U4PgMNLnEeiL7cTvwX--2w" name="PK_Client_ActualiteProjet">
        <node defType="com.stambia.rdbms.colref" id="_U4PgMdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U4PgMtLnEeiL7cTvwX--2w" ref="#_b7-roNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U4PgM9LnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U4PgNNLnEeiL7cTvwX--2w" ref="#_U4L10NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdProjet?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDr7MNLnEeiL7cTvwX--2w" name="FK_Client_ActualiteProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_VDr7MdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDr7MtLnEeiL7cTvwX--2w" ref="#_b7-roNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDr7M9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VDr7NNLnEeiL7cTvwX--2w" name="FK_Client_ActualiteProjet_ref_ActualiteProjet">
        <node defType="com.stambia.rdbms.relation" id="_VDr7NdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VDr7NtLnEeiL7cTvwX--2w" ref="#_U4L10NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdProjet?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VDr7N9LnEeiL7cTvwX--2w" ref="#_U2J1hNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b7-roNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b7-rodX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b7-rotX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b7-ro9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b7-rpNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b7-rpdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b7-rptX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cE5XLNX7Eeimuaax4vsiZw" name="FK_Client_ActualiteProjet_ref_ActualiteProjet1">
        <node defType="com.stambia.rdbms.relation" id="_cE5XLdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cE5XLtX7Eeimuaax4vsiZw" ref="#_U4L10NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdProjet?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cE5XL9X7Eeimuaax4vsiZw" ref="#_U2J1hNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U5TQIdLnEeiL7cTvwX--2w" name="ref_RepasServis">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U5TQItLnEeiL7cTvwX--2w" value="ref_RepasServis"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U5TQI9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U5TQJNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U5TQJdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U5TQJtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U5TQJ9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U5TQKNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U5TQKdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U5TQKtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U5TQK9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U5TQLNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U5TQLdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U5TQLtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U5TQL9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U5TQMNLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U5YvsNLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U5YvsdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U5YvstLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U5Yvs9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U5YvtNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U5YvtdLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U5YvttLnEeiL7cTvwX--2w" name="PK_ref_RepasServi">
        <node defType="com.stambia.rdbms.colref" id="_U5Yvt9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U5YvuNLnEeiL7cTvwX--2w" ref="#_U5TQJNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yeSg0DbREeqF3cPVFXUsDw" name="LibRepas" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yeSg0TbREeqF3cPVFXUsDw" value="LibRepas"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yeSg0jbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yeSg0zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yeSg1DbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yeSg1TbREeqF3cPVFXUsDw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U0zxtNLnEeiL7cTvwX--2w" name="ref_StatutClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U0zxtdLnEeiL7cTvwX--2w" value="ref_StatutClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U0zxttLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U054UNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U054UdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U054UtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U054U9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U054VNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U054VdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U054VtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U054V9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U054WNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U054WdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U054WtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U054W9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U054XNLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U054XdLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U054XtLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U054X9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U054YNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U054YdLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U054YtLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U1AmANLnEeiL7cTvwX--2w" name="PK_ref_StatutClient">
        <node defType="com.stambia.rdbms.colref" id="_U1AmAdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U1AmAtLnEeiL7cTvwX--2w" ref="#_U054UNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yYroEDbREeqF3cPVFXUsDw" name="LibStatut" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yYroETbREeqF3cPVFXUsDw" value="LibStatut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yYroEjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yYroEzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yYroFDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yYroFTbREeqF3cPVFXUsDw" value="20"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U7S0NNLnEeiL7cTvwX--2w" name="ref_NbCouvertJour">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U7S0NdLnEeiL7cTvwX--2w" value="ref_NbCouvertJour"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U7S0NtLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U7Zh4NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7Zh4dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7Zh4tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U7Zh49LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7Zh5NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7Zh5dLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7Zh5tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U7Zh59LnEeiL7cTvwX--2w" name="NbCouvertsJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7Zh6NLnEeiL7cTvwX--2w" value="NbCouvertsJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7Zh6dLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7Zh6tLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7Zh69LnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7Zh7NLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U7Zh7dLnEeiL7cTvwX--2w" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7Zh7tLnEeiL7cTvwX--2w" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7Zh79LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U7Zh8NLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7Zh8dLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7Zh8tLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7Zh89LnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U7fBcNLnEeiL7cTvwX--2w" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7fBcdLnEeiL7cTvwX--2w" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7fBctLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U7fBc9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7fBdNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7fBddLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7fBdtLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U7fBd9LnEeiL7cTvwX--2w" name="PK_ref_NbCouvertJour">
        <node defType="com.stambia.rdbms.colref" id="_U7fBeNLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U7fBedLnEeiL7cTvwX--2w" ref="#_U7Zh4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U5AVNtLnEeiL7cTvwX--2w" name="ref_ScoringCA">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U5Gb0NLnEeiL7cTvwX--2w" value="ref_ScoringCA"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U5Gb0dLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U5Gb0tLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U5Gb09LnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U5Gb1NLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U5Gb1dLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U5Gb1tLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U5Gb19LnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U5Gb2NLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U5MicNLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U5MicdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U5MictLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U5Mic9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U5MidNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U5MiddLnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U5O-sNLnEeiL7cTvwX--2w" name="PK_ref_ScoringCA">
        <node defType="com.stambia.rdbms.colref" id="_U5O-sdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U5O-stLnEeiL7cTvwX--2w" ref="#_U5Gb0tLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_T2IOjdkfEei1-oCUCrJoUg" name="ValMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_T2IOjtkfEei1-oCUCrJoUg" value="ValMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_T2IOj9kfEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_T2RYcNkfEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_T2RYcdkfEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_T2RYctkfEei1-oCUCrJoUg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_T2RYc9kfEei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_T2RYdNkfEei1-oCUCrJoUg" name="ValMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_T2RYddkfEei1-oCUCrJoUg" value="ValMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_T2RYdtkfEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_T2RYd9kfEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_T2RYeNkfEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_T2RYedkfEei1-oCUCrJoUg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_T2RYetkfEei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yeCpMDbREeqF3cPVFXUsDw" name="LibScoring" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yeCpMTbREeqF3cPVFXUsDw" value="LibScoring"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yeCpMjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yeCpMzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yeCpNDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yeCpNTbREeqF3cPVFXUsDw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U9l6QdLnEeiL7cTvwX--2w" name="Client_TypeEtabClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U9l6QtLnEeiL7cTvwX--2w" value="Client_TypeEtabClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U9l6Q9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U9rZ0NLnEeiL7cTvwX--2w" name="IdClientType" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U9rZ0dLnEeiL7cTvwX--2w" value="IdClientType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U9rZ0tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U9rZ09LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U9rZ1NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U9rZ1dLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U9rZ1tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U9rZ19LnEeiL7cTvwX--2w" name="PK_Client_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_U9rZ2NLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U9rZ2dLnEeiL7cTvwX--2w" ref="#_cA5n4NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U9rZ2tLnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U9rZ29LnEeiL7cTvwX--2w" ref="#_U9rZ0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClientType?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VEWpoNLnEeiL7cTvwX--2w" name="FK_Client_TypeEtabClient_Client">
        <node defType="com.stambia.rdbms.relation" id="_VEWpodLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VEWpotLnEeiL7cTvwX--2w" ref="#_cA5n4NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VEWpo9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VEWppNLnEeiL7cTvwX--2w" name="FK_Client_TypeEtabClient_ref_TypeEtabClient">
        <node defType="com.stambia.rdbms.relation" id="_VEWppdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VEWpptLnEeiL7cTvwX--2w" ref="#_U9rZ0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClientType?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VEWpp9LnEeiL7cTvwX--2w" ref="#_U4ibINLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cA5n4NX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_cA5n4dX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cA5n4tX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cA5n49X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cA5n5NX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cA5n5dX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cA5n5tX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cFZtgNX7Eeimuaax4vsiZw" name="FK_Client_TypeEtabClient_ref_TypeEtabClient1">
        <node defType="com.stambia.rdbms.relation" id="_cFZtgdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cFZtgtX7Eeimuaax4vsiZw" ref="#_U9rZ0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClientType?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cFZtg9X7Eeimuaax4vsiZw" ref="#_U4ibINLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U_A2kdLnEeiL7cTvwX--2w" name="ref_TypeComSouhaite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U_A2ktLnEeiL7cTvwX--2w" value="ref_TypeComSouhaite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U_A2k9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U_Nq4NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_Nq4dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_Nq4tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U_Nq49LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_Nq5NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_Nq5dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_Nq5tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U_Nq59LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_Nq6NLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_Nq6dLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_Nq6tLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_Nq69LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_Nq7NLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U_Nq7dLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_Nq7tLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_Nq79LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_Nq8NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_Nq8dLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_Nq8tLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U_TKcNLnEeiL7cTvwX--2w" name="PK_ref_TypeComSouhaite">
        <node defType="com.stambia.rdbms.colref" id="_U_TKcdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U_TKctLnEeiL7cTvwX--2w" ref="#_U_Nq4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yk4RADbREeqF3cPVFXUsDw" name="LibTypeCom" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yk4RATbREeqF3cPVFXUsDw" value="LibTypeCom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yk4RAjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yk4RAzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yk4RBDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yk4RBTbREeqF3cPVFXUsDw" value="30"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U1MMNNLnEeiL7cTvwX--2w" name="ref_NiveauStanding">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U1MMNdLnEeiL7cTvwX--2w" value="ref_NiveauStanding"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U1MMNtLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U1S54NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U1S54dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U1S54tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U1S549LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U1S55NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U1S55dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U1S55tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U1S559LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U1S56NLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U1S56dLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U1S56tLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U1S569LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U1S57NLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U1YZcNLnEeiL7cTvwX--2w" name="PK_NiveauStanding">
        <node defType="com.stambia.rdbms.colref" id="_U1YZcdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U1YZctLnEeiL7cTvwX--2w" ref="#_U1S54NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UmNwUBUQEeqPGbHwfkinWw" name="LibNivStanding" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UmNwURUQEeqPGbHwfkinWw" value="LibNivStanding"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UmNwUhUQEeqPGbHwfkinWw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UmNwUxUQEeqPGbHwfkinWw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UmNwVBUQEeqPGbHwfkinWw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UmNwVRUQEeqPGbHwfkinWw" value="30"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U19BMdLnEeiL7cTvwX--2w" name="ref_TypeTel">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U19BMtLnEeiL7cTvwX--2w" value="ref_TypeTel"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U19BM9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U2Du4NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2Du4dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2Du4tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U2Du49LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2Du5NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2Du5dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2Du5tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U2Du59LnEeiL7cTvwX--2w" name="Type" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2Du6NLnEeiL7cTvwX--2w" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2Du6dLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2Du6tLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2Du69LnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2Du7NLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U2Du7dLnEeiL7cTvwX--2w" name="PK_ref_TypeTel">
        <node defType="com.stambia.rdbms.colref" id="_U2Du7tLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U2Du79LnEeiL7cTvwX--2w" ref="#_U2Du4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U4R8cdLnEeiL7cTvwX--2w" name="ref_FormeJuridique">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U4R8ctLnEeiL7cTvwX--2w" value="ref_FormeJuridique"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U4R8c9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U4Vm0NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U4Vm0dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U4Vm0tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U4Vm09LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U4Vm1NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U4Vm1dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U4Vm1tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U4YDENLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U4YDEdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U4YDEtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U4YDE9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U4YDFNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U4YDFdLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U4YDFtLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U4YDF9LnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U4YDGNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U4YDGdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U4YDGtLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U4YDG9LnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U4cUgNLnEeiL7cTvwX--2w" name="PK_FormeJuridique">
        <node defType="com.stambia.rdbms.colref" id="_U4cUgdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U4cUgtLnEeiL7cTvwX--2w" ref="#_U4Vm0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ydLGgDbREeqF3cPVFXUsDw" name="LibFormeJuridique" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ydLGgTbREeqF3cPVFXUsDw" value="LibFormeJuridique"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ydLGgjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ydLGgzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ydLGhDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ydLGhTbREeqF3cPVFXUsDw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U9yHgdLnEeiL7cTvwX--2w" name="ref_TypeCompte">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U9yHgtLnEeiL7cTvwX--2w" value="ref_TypeCompte"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U9yHg9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U9yHhNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U9yHhdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U9yHhtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U9yHh9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U9yHiNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U9yHidLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U9yHitLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U93nENLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U93nEdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U93nEtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U93nE9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U93nFNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U93nFdLnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U99tsNLnEeiL7cTvwX--2w" name="PK_ref_TypeCompte">
        <node defType="com.stambia.rdbms.colref" id="_U99tsdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U99tstLnEeiL7cTvwX--2w" ref="#_U9yHhNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yjLA0DbREeqF3cPVFXUsDw" name="LibTypeCompte" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yjLA0TbREeqF3cPVFXUsDw" value="LibTypeCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yjLA0jbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yjLA0zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yjLA1DbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yjLA1TbREeqF3cPVFXUsDw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UrSCQdLnEeiL7cTvwX--2w" name="ref_UniversConso">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UrSCQtLnEeiL7cTvwX--2w" value="ref_UniversConso"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UrSCQ9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UrYI4NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UrYI4dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UrYI4tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UrYI49LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UrYI5NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UrYI5dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UrYI5tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UrYI59LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UrYI6NLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UrYI6dLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UrYI6tLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UrYI69LnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UrYI7NLnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UralINLnEeiL7cTvwX--2w" name="PK_ref_UniversConso">
        <node defType="com.stambia.rdbms.colref" id="_UralIdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UralItLnEeiL7cTvwX--2w" ref="#_UrYI4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yPz_8DbREeqF3cPVFXUsDw" name="LibUniversConso" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yPz_8TbREeqF3cPVFXUsDw" value="LibUniversConso"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yPz_8jbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yPz_8zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yPz_9DbREeqF3cPVFXUsDw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yPz_9TbREeqF3cPVFXUsDw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UwgggdLnEeiL7cTvwX--2w" name="Client_RepasServis">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UwgggtLnEeiL7cTvwX--2w" value="Client_RepasServis"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Uwggg9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UwsGsNLnEeiL7cTvwX--2w" name="IdRepas" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwsGsdLnEeiL7cTvwX--2w" value="IdRepas"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwsGstLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwsGs9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwsGtNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwsGtdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwsGttLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Uwui8NLnEeiL7cTvwX--2w" name="PK_Client_RepasServis">
        <node defType="com.stambia.rdbms.colref" id="_Uwui8dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Uwui8tLnEeiL7cTvwX--2w" ref="#_b0HJFNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_Uwui89LnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Uwui9NLnEeiL7cTvwX--2w" ref="#_UwsGsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdRepas?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCr1oNLnEeiL7cTvwX--2w" name="FK_Client_RepasServis_Client">
        <node defType="com.stambia.rdbms.relation" id="_VCr1odLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCr1otLnEeiL7cTvwX--2w" ref="#_b0HJFNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCr1o9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCr1pNLnEeiL7cTvwX--2w" name="FK_Client_RepasServis_ref_RepasServis">
        <node defType="com.stambia.rdbms.relation" id="_VCr1pdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCr1ptLnEeiL7cTvwX--2w" ref="#_UwsGsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdRepas?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCr1p9LnEeiL7cTvwX--2w" ref="#_U5TQJNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b0HJFNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b0HJFdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b0HJFtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b0HJF9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b0HJGNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b0HJGdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b0HJGtX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cES6JNX7Eeimuaax4vsiZw" name="FK_Client_RepasServis_ref_RepasServis1">
        <node defType="com.stambia.rdbms.relation" id="_cES6JdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cES6JtX7Eeimuaax4vsiZw" ref="#_UwsGsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdRepas?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cES6J9X7Eeimuaax4vsiZw" ref="#_U5TQJNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_VAfdQdLnEeiL7cTvwX--2w" name="ref_NbEleves">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_VAfdQtLnEeiL7cTvwX--2w" value="ref_NbEleves"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_VAfdQ9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_VAut0NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_VAut0dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VAut0tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VAut09LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VAut1NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VAut1dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VAut1tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VA67ENLnEeiL7cTvwX--2w" name="NbEleves" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_VA67EdLnEeiL7cTvwX--2w" value="NbEleves"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VA67EtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VA67E9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VA67FNLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VA67FdLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VA9XUNLnEeiL7cTvwX--2w" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_VA9XUdLnEeiL7cTvwX--2w" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VA9XUtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VA9XU9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VA9XVNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VA9XVdLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VA9XVtLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VA9XV9LnEeiL7cTvwX--2w" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_VA9XWNLnEeiL7cTvwX--2w" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VA9XWdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VA9XWtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VA9XW9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VA9XXNLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VA9XXdLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_VBDd8NLnEeiL7cTvwX--2w" name="PK_ref_NbEleves">
        <node defType="com.stambia.rdbms.colref" id="_VBDd8dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_VBDd8tLnEeiL7cTvwX--2w" ref="#_VAut0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UvyHwdLnEeiL7cTvwX--2w" name="ref_Pays">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UvyHwtLnEeiL7cTvwX--2w" value="ref_Pays"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UvyHw9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Uv4OYNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uv4OYdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uv4OYtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Uv4OY9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uv4OZNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uv4OZdLnEeiL7cTvwX--2w" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uv4OZtLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uv4OZ9LnEeiL7cTvwX--2w" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uv4OaNLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uv4OadLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Uv4OatLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uv4Oa9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uv4ObNLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uv4ObdLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uv4ObtLnEeiL7cTvwX--2w" name="Alpha2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uv4Ob9LnEeiL7cTvwX--2w" value="Alpha2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uv4OcNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uv4OcdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uv4OctLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uv4Oc9LnEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uv8f0NLnEeiL7cTvwX--2w" name="Alpha3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uv8f0dLnEeiL7cTvwX--2w" value="Alpha3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uv8f0tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uv8f09LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uv8f1NLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uv8f1dLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uv9G4NLnEeiL7cTvwX--2w" name="Nom_en_gb" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uv9G4dLnEeiL7cTvwX--2w" value="Nom_en_gb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uv9G4tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uv9G49LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uv9G5NLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uv9G5dLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Uv-VANLnEeiL7cTvwX--2w" name="Nom_fr_fr" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_Uv-VAdLnEeiL7cTvwX--2w" value="Nom_fr_fr"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Uv-VAtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Uv-VA9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Uv-VBNLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Uv-VBdLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Uv-VBtLnEeiL7cTvwX--2w" name="PK_ref_Pays">
        <node defType="com.stambia.rdbms.colref" id="_Uv-VB9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Uv-VCNLnEeiL7cTvwX--2w" ref="#_Uv4OYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U2J1gdLnEeiL7cTvwX--2w" name="ref_ActualiteProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U2J1gtLnEeiL7cTvwX--2w" value="ref_ActualiteProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U2J1g9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U2J1hNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2J1hdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2J1htLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U2J1h9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2J1iNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2J1idLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2J1itLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U2J1i9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2J1jNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2J1jdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2J1jtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2J1j9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2J1kNLnEeiL7cTvwX--2w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U2PVENLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U2PVEdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U2PVEtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U2PVE9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U2PVFNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U2PVFdLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U2PVFtLnEeiL7cTvwX--2w" name="PK_ref_ActualiteProjet">
        <node defType="com.stambia.rdbms.colref" id="_U2PVF9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U2PVGNLnEeiL7cTvwX--2w" ref="#_U2J1hNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yaZfUDbREeqF3cPVFXUsDw" name="LibActualiteProjet" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yaZfUTbREeqF3cPVFXUsDw" value="LibActualiteProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yaZfUjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yaZfUzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yaZfVDbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yaZfVTbREeqF3cPVFXUsDw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U7r1wdLnEeiL7cTvwX--2w" name="Client_Distinction">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U7r1wtLnEeiL7cTvwX--2w" value="Client_Distinction"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U7r1w9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U7xVUNLnEeiL7cTvwX--2w" name="IdDistinction" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7xVUdLnEeiL7cTvwX--2w" value="IdDistinction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7xVUtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U7xVU9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7xVVNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7xVVdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7xVVtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U7xVV9LnEeiL7cTvwX--2w" name="PK_Client_Distinction">
        <node defType="com.stambia.rdbms.colref" id="_U7xVWNLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U7xVWdLnEeiL7cTvwX--2w" ref="#_b_mAUNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U7xVWtLnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U7xVW9LnEeiL7cTvwX--2w" ref="#_U7xVUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdDistinction?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VELDYNLnEeiL7cTvwX--2w" name="FK_Client_Distinction_Client">
        <node defType="com.stambia.rdbms.relation" id="_VELDYdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VELDYtLnEeiL7cTvwX--2w" ref="#_b_mAUNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VELDY9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VELDZNLnEeiL7cTvwX--2w" name="FK_Client_Distinction_ref_Distinctions">
        <node defType="com.stambia.rdbms.relation" id="_VELDZdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VELDZtLnEeiL7cTvwX--2w" ref="#_U7xVUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdDistinction?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VELDZ9LnEeiL7cTvwX--2w" ref="#_VBKLoNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b_mAUNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b_mAUdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b_mAUtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b_mAU9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b_mAVNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b_mAVdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b_mAVtX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cFQjgNX7Eeimuaax4vsiZw" name="FK_Client_Distinction_ref_Distinctions1">
        <node defType="com.stambia.rdbms.relation" id="_cFQjgdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cFQjgtX7Eeimuaax4vsiZw" ref="#_U7xVUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdDistinction?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cFQjg9X7Eeimuaax4vsiZw" ref="#_VBKLoNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U7JDMdLnEeiL7cTvwX--2w" name="ref_Langue">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U7JDMtLnEeiL7cTvwX--2w" value="ref_Langue"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U7JDM9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U7JDNNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7JDNdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7JDNtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U7JDN9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7JDONLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7JDOdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7JDOtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U7MtkNLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7MtkdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7MtktLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7Mtk9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7MtlNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7MtldLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U7MtltLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7Mtl9LnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7MtmNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7MtmdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7MtmtLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7Mtm9LnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U7S0MNLnEeiL7cTvwX--2w" name="PK_Langue">
        <node defType="com.stambia.rdbms.colref" id="_U7S0MdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U7S0MtLnEeiL7cTvwX--2w" ref="#_U7JDNNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yhCS0DbREeqF3cPVFXUsDw" name="LibLangue" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yhCS0TbREeqF3cPVFXUsDw" value="LibLangue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yhCS0jbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yhCS0zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yhCS1DbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yhCS1TbREeqF3cPVFXUsDw" value="20"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UwD0kdLnEeiL7cTvwX--2w" name="Client_HoraireOuv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UwD0ktLnEeiL7cTvwX--2w" value="Client_HoraireOuv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UwD0k9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UwFCsNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwFCsdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwFCstLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwFCs9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwFCtNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwFCtdLnEeiL7cTvwX--2w" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwFCttLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwJ7MNLnEeiL7cTvwX--2w" name="H_OuvertureLundi" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwJ7MdLnEeiL7cTvwX--2w" value="H_OuvertureLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwJ7MtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwJ7M9LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwJ7NNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwJ7NdLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwJ7NtLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwLJUNLnEeiL7cTvwX--2w" name="H_FermeLundi" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwLJUdLnEeiL7cTvwX--2w" value="H_FermeLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwLJUtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwLJU9LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwLJVNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwLJVdLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwLJVtLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwLJV9LnEeiL7cTvwX--2w" name="H_OuvertureMardi" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwLJWNLnEeiL7cTvwX--2w" value="H_OuvertureMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwLJWdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwLJWtLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwLJW9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwLJXNLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwLJXdLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwOMoNLnEeiL7cTvwX--2w" name="H_FermeMardi" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwOModLnEeiL7cTvwX--2w" value="H_FermeMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwOMotLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwOMo9LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwOMpNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwOMpdLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwOMptLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwQB0NLnEeiL7cTvwX--2w" name="H_OuvertureMercredi" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwQB0dLnEeiL7cTvwX--2w" value="H_OuvertureMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwQB0tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwQB09LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwQB1NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwQB1dLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwQB1tLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwRP8NLnEeiL7cTvwX--2w" name="H_FermeMercredi" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwRP8dLnEeiL7cTvwX--2w" value="H_FermeMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwRP8tLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwRP89LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwRP9NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwRP9dLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwRP9tLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwRP99LnEeiL7cTvwX--2w" name="H_OuvertureJeudi" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwRP-NLnEeiL7cTvwX--2w" value="H_OuvertureJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwRP-dLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwRP-tLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwRP-9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwRP_NLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwRP_dLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwRP_tLnEeiL7cTvwX--2w" name="H_FermeJeudi" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwTsMNLnEeiL7cTvwX--2w" value="H_FermeJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwTsMdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwTsMtLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwTsM9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwTsNNLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwTsNdLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwTsNtLnEeiL7cTvwX--2w" name="H_OuvertureVendredi" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwTsN9LnEeiL7cTvwX--2w" value="H_OuvertureVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwTsONLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwTsOdLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwTsOtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwTsO9LnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwTsPNLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwWIcNLnEeiL7cTvwX--2w" name="H_FermeVendredi" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwWIcdLnEeiL7cTvwX--2w" value="H_FermeVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwWIctLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwWIc9LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwWIdNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwWIddLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwWIdtLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwWId9LnEeiL7cTvwX--2w" name="H_OuvertureSamedi" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwWIeNLnEeiL7cTvwX--2w" value="H_OuvertureSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwWIedLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwWIetLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwWIe9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwWIfNLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwWIfdLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwXWkNLnEeiL7cTvwX--2w" name="H_FermeSamedi" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwXWkdLnEeiL7cTvwX--2w" value="H_FermeSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwXWktLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwXWk9LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwXWlNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwXWldLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwXWltLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwZLwNLnEeiL7cTvwX--2w" name="H_OuvertureDimanche" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwZLwdLnEeiL7cTvwX--2w" value="H_OuvertureDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwZLwtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwZLw9LnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwZLxNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwZLxdLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwZLxtLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UwZLx9LnEeiL7cTvwX--2w" name="H_FermeDimanche" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_UwZLyNLnEeiL7cTvwX--2w" value="H_FermeDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UwZLydLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UwZLytLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UwZLy9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UwZLzNLnEeiL7cTvwX--2w" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UwZLzdLnEeiL7cTvwX--2w" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UwcPENLnEeiL7cTvwX--2w" name="PK_Horaire_Ouverture">
        <node defType="com.stambia.rdbms.colref" id="_UwcPEdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UwcPEtLnEeiL7cTvwX--2w" ref="#_bzhTMNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCoyUNLnEeiL7cTvwX--2w" name="FK_Horaire_Ouverture_Client">
        <node defType="com.stambia.rdbms.relation" id="_VCoyUdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCoyUtLnEeiL7cTvwX--2w"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCoyU9LnEeiL7cTvwX--2w" ref="#_Ut2OENLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Code?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_bzhTMNX7Eeimuaax4vsiZw" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_bzhTMdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_bzhTMtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_bzhTM9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_bzhTNNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_bzhTNdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_bzhTNtX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cEMzxNX7Eeimuaax4vsiZw" name="FK_Client_HoraireOuv_Client">
        <node defType="com.stambia.rdbms.relation" id="_cEMzxdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cEMzxtX7Eeimuaax4vsiZw" ref="#_bzhTMNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cEMzx9X7Eeimuaax4vsiZw" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYf_kOZsEeicCszix7eWDA" name="H_OuvertureLundi2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYf_keZsEeicCszix7eWDA" value="H_OuvertureLundi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYf_kuZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYf_k-ZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYf_lOZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYf_leZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYf_luZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYf_l-ZsEeicCszix7eWDA" name="H_FermeLundi2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYf_mOZsEeicCszix7eWDA" value="H_FermeLundi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYf_meZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYf_muZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYf_m-ZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYf_nOZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYf_neZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYf_rOZsEeicCszix7eWDA" name="H_OuvertureMardi2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYf_reZsEeicCszix7eWDA" value="H_OuvertureMardi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYf_ruZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYf_r-ZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYf_sOZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYf_seZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYf_suZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYf_s-ZsEeicCszix7eWDA" name="H_FermeMardi2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYf_tOZsEeicCszix7eWDA" value="H_FermeMardi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYf_teZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYf_tuZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYf_t-ZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYf_uOZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYf_ueZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYpwnuZsEeicCszix7eWDA" name="H_OuvertureMercredi2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYpwn-ZsEeicCszix7eWDA" value="H_OuvertureMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYpwoOZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYpwoeZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYpwouZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYpwo-ZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYpwpOZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYpwpeZsEeicCszix7eWDA" name="H_FermeMercredi2" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYpwpuZsEeicCszix7eWDA" value="H_FermeMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYpwp-ZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYpwqOZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYpwqeZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYpwquZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYpwq-ZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYy6gOZsEeicCszix7eWDA" name="H_OuvertureJeudi2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYy6geZsEeicCszix7eWDA" value="H_OuvertureJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYy6guZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYy6g-ZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYy6hOZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYy6heZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYy6huZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYy6h-ZsEeicCszix7eWDA" name="H_FermeJeudi2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYy6iOZsEeicCszix7eWDA" value="H_FermeJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYy6ieZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYy6iuZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYy6i-ZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYy6jOZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYy6jeZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYy6nOZsEeicCszix7eWDA" name="H_OuvertureVendredi2" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYy6neZsEeicCszix7eWDA" value="H_OuvertureVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYy6nuZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYy6n-ZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYy6oOZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYy6oeZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYy6ouZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYy6o-ZsEeicCszix7eWDA" name="H_FermeVendredi2" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYy6pOZsEeicCszix7eWDA" value="H_FermeVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYy6peZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYy6puZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYy6p-ZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYy6qOZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYy6qeZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYy6uOZsEeicCszix7eWDA" name="H_OuvertureSamedi2" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYy6ueZsEeicCszix7eWDA" value="H_OuvertureSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYy6uuZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYy6u-ZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYy6vOZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYy6veZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYy6vuZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYy6v-ZsEeicCszix7eWDA" name="H_FermeSamedi2" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYy6wOZsEeicCszix7eWDA" value="H_FermeSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYy6weZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYy6wuZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYy6w-ZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYy6xOZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYy6xeZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pY8rjuZsEeicCszix7eWDA" name="H_OuvertureDimanche2" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_pY8rj-ZsEeicCszix7eWDA" value="H_OuvertureDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pY8rkOZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pY8rkeZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pY8rkuZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pY8rk-ZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pY8rlOZsEeicCszix7eWDA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pY8rleZsEeicCszix7eWDA" name="H_FermeDimanche2" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_pY8rluZsEeicCszix7eWDA" value="H_FermeDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pY8rl-ZsEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pY8rmOZsEeicCszix7eWDA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pY8rmeZsEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pY8rmuZsEeicCszix7eWDA" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pY8rm-ZsEeicCszix7eWDA" value="14"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Und5QdLnEeiL7cTvwX--2w" name="Client_PrestationOfferte">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Und5QtLnEeiL7cTvwX--2w" value="Client_PrestationOfferte"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Und5Q9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UpwYQNLnEeiL7cTvwX--2w" name="IdPrestation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UpwYQdLnEeiL7cTvwX--2w" value="IdPrestation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UpwYQtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UpwYQ9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UpwYRNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UpwYRdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UpwYRtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UqhNQNLnEeiL7cTvwX--2w" name="PK_Client_PrestationOfferte">
        <node defType="com.stambia.rdbms.colref" id="_UqhNQdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UqhNQtLnEeiL7cTvwX--2w" ref="#_btarMNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_UqhNQ9LnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UqhNRNLnEeiL7cTvwX--2w" ref="#_UpwYQNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdPrestation?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCS0ENLnEeiL7cTvwX--2w" name="FK_Client_PrestationOfferte_Client">
        <node defType="com.stambia.rdbms.relation" id="_VCS0EdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCS0EtLnEeiL7cTvwX--2w" ref="#_btarMNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCS0E9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCS0FNLnEeiL7cTvwX--2w" name="FK_Client_PrestationOfferte_ref_PrestationsOffertes">
        <node defType="com.stambia.rdbms.relation" id="_VCS0FdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCS0FtLnEeiL7cTvwX--2w" ref="#_UpwYQNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdPrestation?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCS0F9LnEeiL7cTvwX--2w" ref="#_U1kmsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_btarMNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_btarMdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_btarMtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_btarM9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_btarNNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_btarNdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_btarNtX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cD7txNX7Eeimuaax4vsiZw" name="FK_Client_PrestationOfferte_ref_PrestationsOffertes1">
        <node defType="com.stambia.rdbms.relation" id="_cD7txdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cD7txtX7Eeimuaax4vsiZw" ref="#_UpwYQNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdPrestation?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cD7tx9X7Eeimuaax4vsiZw" ref="#_U1kmsNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UzlCodLnEeiL7cTvwX--2w" name="ref_TypeAdresse">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UzlCotLnEeiL7cTvwX--2w" value="ref_TypeAdresse"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UzlCo9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UzqiMNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzqiMdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzqiMtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UzqiM9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzqiNNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzqiNdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzqiNtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UzqiN9LnEeiL7cTvwX--2w" name="Type" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UzqiONLnEeiL7cTvwX--2w" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UzqiOdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UzqiOtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UzqiO9LnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UzqiPNLnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UzqiPdLnEeiL7cTvwX--2w" name="PK_ref_TypeAdresse">
        <node defType="com.stambia.rdbms.colref" id="_UzqiPtLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UzqiP9LnEeiL7cTvwX--2w" ref="#_UzqiMNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U0iE4dLnEeiL7cTvwX--2w" name="ref_NbRepasJour">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U0iE4tLnEeiL7cTvwX--2w" value="ref_NbRepasJour"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U0iE49LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U0oLgNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0oLgdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0oLgtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U0oLg9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0oLhNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0oLhdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0oLhtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U0trENLnEeiL7cTvwX--2w" name="NbRepasJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0trEdLnEeiL7cTvwX--2w" value="NbRepasJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0trEtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0trE9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0trFNLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0trFdLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U0wuYNLnEeiL7cTvwX--2w" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0wuYdLnEeiL7cTvwX--2w" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0wuYtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U0wuY9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0wuZNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0wuZdLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0wuZtLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U0wuZ9LnEeiL7cTvwX--2w" name="nbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0wuaNLnEeiL7cTvwX--2w" value="nbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0wuadLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U0wuatLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0wua9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0wubNLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0wubdLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U0zxsNLnEeiL7cTvwX--2w" name="PK_ref_NbRepasJour">
        <node defType="com.stambia.rdbms.colref" id="_U0zxsdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U0zxstLnEeiL7cTvwX--2w" ref="#_U0oLgNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U6818dLnEeiL7cTvwX--2w" name="ref_TypeZone">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U6818tLnEeiL7cTvwX--2w" value="ref_TypeZone"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U68189LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U7BHYNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7BHYdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7BHYtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U7BHY9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7BHZNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7BHZdLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7BHZtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U7BHZ9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7BHaNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7BHadLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7BHatLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7BHa9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7BHbNLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U7BHbdLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U7BHbtLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U7BHb9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U7BHcNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U7BHcdLnEeiL7cTvwX--2w" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U7BHctLnEeiL7cTvwX--2w" value="5"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_qKRVENuHEei1-oCUCrJoUg" value="0"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U7Gm8NLnEeiL7cTvwX--2w" name="PK_TypeZone">
        <node defType="com.stambia.rdbms.colref" id="_U7Gm8dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U7Gm8tLnEeiL7cTvwX--2w" ref="#_U7BHYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ygpRQDbREeqF3cPVFXUsDw" name="LibTypeZone" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ygpRQTbREeqF3cPVFXUsDw" value="LibTypeZone"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ygpRQjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ygpRQzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ygpRRDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ygpRRTbREeqF3cPVFXUsDw" value="20"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U99ttNLnEeiL7cTvwX--2w" name="ref_Concurrent">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U99ttdLnEeiL7cTvwX--2w" value="ref_Concurrent"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U99tttLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U-EbYNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-EbYdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-EbYtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U-EbY9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-EbZNLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-EbZdLnEeiL7cTvwX--2w" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-EbZtLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-EbZ9LnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-EbaNLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-EbadLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-EbatLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-Eba9LnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-EbbNLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U-EbbdLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U-EbbtLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U-Ebb9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U-EbcNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U-EbcdLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U-EbctLnEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U-J68NLnEeiL7cTvwX--2w" name="PK_ref_Concurrent">
        <node defType="com.stambia.rdbms.colref" id="_U-J68dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U-J68tLnEeiL7cTvwX--2w" ref="#_U-EbYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yjhmIDbREeqF3cPVFXUsDw" name="LibConcurrent" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yjhmITbREeqF3cPVFXUsDw" value="LibConcurrent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yjhmIjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yjhmIzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yjhmJDbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yjhmJTbREeqF3cPVFXUsDw" value="30"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UqhNRtLnEeiL7cTvwX--2w" name="ActiviteParCanal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UqhNR9LnEeiL7cTvwX--2w" value="ActiviteParCanal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UqhNSNLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UqnT4NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UqnT4dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UqnT4tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UqnT49LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UqnT5NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UqnT5dLnEeiL7cTvwX--2w" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UqnT5tLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UqnT59LnEeiL7cTvwX--2w" name="CodeClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UqnT6NLnEeiL7cTvwX--2w" value="CodeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UqnT6dLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UqnT6tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UqnT69LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UqnT7NLnEeiL7cTvwX--2w" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UqnT7dLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UqzhINLnEeiL7cTvwX--2w" name="IdCanal" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UqzhIdLnEeiL7cTvwX--2w" value="IdCanal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UqzhItLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UqzhI9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UqzhJNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UqzhJdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UqzhJtLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UqzhJ9LnEeiL7cTvwX--2w" name="Actif12dm" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_UqzhKNLnEeiL7cTvwX--2w" value="Actif12dm"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UqzhKdLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UqzhKtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UqzhK9LnEeiL7cTvwX--2w" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UqzhLNLnEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Uq5AsNLnEeiL7cTvwX--2w" name="PK_ActiviteParCanal">
        <node defType="com.stambia.rdbms.colref" id="_Uq5AsdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Uq5AstLnEeiL7cTvwX--2w" ref="#_UqnT4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCXFgNLnEeiL7cTvwX--2w" name="FK_ActiviteParCanal_Client">
        <node defType="com.stambia.rdbms.relation" id="_VCXFgdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCXFgtLnEeiL7cTvwX--2w" ref="#_UqnT59LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=CodeClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCXFg9LnEeiL7cTvwX--2w" ref="#_Ut2OENLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Code?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VCXFhNLnEeiL7cTvwX--2w" name="FK_ActiviteParCanal_ref_Canal">
        <node defType="com.stambia.rdbms.relation" id="_VCXFhdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VCXFhtLnEeiL7cTvwX--2w" ref="#_UqzhINLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdCanal?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VCXFh9LnEeiL7cTvwX--2w" ref="#_Uz2vcNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U_-f4dLnEeiL7cTvwX--2w" name="ref_TrancheCA">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U_-f4tLnEeiL7cTvwX--2w" value="ref_TrancheCA"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U_-f49LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U_-f5NLnEeiL7cTvwX--2w" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U_-f5dLnEeiL7cTvwX--2w" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U_-f5tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U_-f59LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U_-f6NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U_-f6dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U_-f6tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VAEmgNLnEeiL7cTvwX--2w" name="TrancheCA" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_VAEmgdLnEeiL7cTvwX--2w" value="TrancheCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VAEmgtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VAEmg9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VAEmhNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VAEmhdLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VAHCwNLnEeiL7cTvwX--2w" name="TrancheCAMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_VAHCwdLnEeiL7cTvwX--2w" value="TrancheCAMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VAHCwtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VAHCw9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VAHCxNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VAHCxdLnEeiL7cTvwX--2w" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VAHCxtLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VAHCx9LnEeiL7cTvwX--2w" name="TrancheCAMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_VAHCyNLnEeiL7cTvwX--2w" value="TrancheCAMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VAHCydLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VAHCytLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VAHCy9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VAHCzNLnEeiL7cTvwX--2w" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VAHCzdLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_VANJYNLnEeiL7cTvwX--2w" name="PK_ref_TrancheCA">
        <node defType="com.stambia.rdbms.colref" id="_VANJYdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_VANJYtLnEeiL7cTvwX--2w" ref="#_U_-f5NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U6KLxtLnEeiL7cTvwX--2w" name="Client_EnvTouristique">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U6KLx9LnEeiL7cTvwX--2w" value="Client_EnvTouristique"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U6KLyNLnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U6SuoNLnEeiL7cTvwX--2w" name="IdEnvTouristique" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U6SuodLnEeiL7cTvwX--2w" value="IdEnvTouristique"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U6SuotLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U6Suo9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U6SupNLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U6SupdLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U6SuptLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U6Sup9LnEeiL7cTvwX--2w" name="PK_Client_EnvTouristique">
        <node defType="com.stambia.rdbms.colref" id="_U6SuqNLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U6SuqdLnEeiL7cTvwX--2w" ref="#_U6SuoNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdEnvTouristique?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U6WZANLnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U6WZAdLnEeiL7cTvwX--2w" ref="#_b9rUxNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VD-2KNLnEeiL7cTvwX--2w" name="FK_Client_EnvTouristique_Client">
        <node defType="com.stambia.rdbms.relation" id="_VD-2KdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VD-2KtLnEeiL7cTvwX--2w" ref="#_b9rUxNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VD-2K9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VD-2LNLnEeiL7cTvwX--2w" name="FK_Client_EnvTouristique_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.relation" id="_VD-2LdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VD-2LtLnEeiL7cTvwX--2w" ref="#_U6SuoNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdEnvTouristique?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VD-2L9LnEeiL7cTvwX--2w" ref="#_UxuohNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b9rUxNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b9rUxdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b9rUxtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b9rUx9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b9rUyNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b9rUydX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b9rUytX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cFMSDNX7Eeimuaax4vsiZw" name="FK_Client_EnvTouristique_ref_EnvTouristique1">
        <node defType="com.stambia.rdbms.relation" id="_cFMSDdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cFMSDtX7Eeimuaax4vsiZw" ref="#_U6SuoNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdEnvTouristique?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cFMSD9X7Eeimuaax4vsiZw" ref="#_UxuohNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Uyh5wdLnEeiL7cTvwX--2w" name="Client_MoyenPaiement">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Uyh5wtLnEeiL7cTvwX--2w" value="Client_MoyenPaiement"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Uyh5w9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UynZV9LnEeiL7cTvwX--2w" name="IdMoyPaiement" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UynZWNLnEeiL7cTvwX--2w" value="IdMoyPaiement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UynZWdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UynZWtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UynZW9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UynZXNLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UynZXdLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_Uytf8NLnEeiL7cTvwX--2w" name="PK_Client_MoyenPaiement">
        <node defType="com.stambia.rdbms.colref" id="_Uytf8dLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Uytf8tLnEeiL7cTvwX--2w" ref="#_b2LloNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_Uytf89LnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_Uytf9NLnEeiL7cTvwX--2w" ref="#_UynZV9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdMoyPaiement?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VC0_mNLnEeiL7cTvwX--2w" name="FK_Client_MoyenPaiement_Client">
        <node defType="com.stambia.rdbms.relation" id="_VC0_mdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VC0_mtLnEeiL7cTvwX--2w" ref="#_b2LloNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VC0_m9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VC0_nNLnEeiL7cTvwX--2w" name="FK_Client_MoyenPaiement_ref_MoyenPaiement">
        <node defType="com.stambia.rdbms.relation" id="_VC0_ndLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VC0_ntLnEeiL7cTvwX--2w" ref="#_UynZV9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdMoyPaiement?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VC0_n9LnEeiL7cTvwX--2w" ref="#_UxmssNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b2LloNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b2LlodX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b2LlotX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b2Llo9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b2LlpNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b2LlpdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b2LlptX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cEcrLNX7Eeimuaax4vsiZw" name="FK_Client_MoyenPaiement_ref_MoyenPaiement1">
        <node defType="com.stambia.rdbms.relation" id="_cEcrLdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cEcrLtX7Eeimuaax4vsiZw" ref="#_UynZV9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdMoyPaiement?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cEcrL9X7Eeimuaax4vsiZw" ref="#_UxmssNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U5e2UdLnEeiL7cTvwX--2w" name="Client_Concurrent">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U5e2UtLnEeiL7cTvwX--2w" value="Client_Concurrent"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U5e2U9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U5k88NLnEeiL7cTvwX--2w" name="IdConcurrent" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U5k88dLnEeiL7cTvwX--2w" value="IdConcurrent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U5k88tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U5k889LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U5k89NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U5k89dLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U5k89tLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U5k899LnEeiL7cTvwX--2w" name="PK_Client_Concurrent">
        <node defType="com.stambia.rdbms.colref" id="_U5k8-NLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U5k8-dLnEeiL7cTvwX--2w" ref="#_b87t5NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U5k8-tLnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U5k8-9LnEeiL7cTvwX--2w" ref="#_U5k88NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdConcurrent?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VD4IcNLnEeiL7cTvwX--2w" name="FK_Client_Concurrent_Client">
        <node defType="com.stambia.rdbms.relation" id="_VD4IcdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VD4IctLnEeiL7cTvwX--2w" ref="#_b87t5NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VD4Ic9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VD4IdNLnEeiL7cTvwX--2w" name="FK_Client_Concurrent_ref_Concurrent">
        <node defType="com.stambia.rdbms.relation" id="_VD4IddLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VD4IdtLnEeiL7cTvwX--2w" ref="#_U5k88NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdConcurrent?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VD4Id9LnEeiL7cTvwX--2w" ref="#_U-EbYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b87t5NX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b87t5dX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b87t5tX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b87t59X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b87t6NX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b87t6dX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b87t6tX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cFDIHNX7Eeimuaax4vsiZw" name="FK_Client_Concurrent_ref_Concurrent1">
        <node defType="com.stambia.rdbms.relation" id="_cFDIHdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cFDIHtX7Eeimuaax4vsiZw" ref="#_U5k88NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdConcurrent?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cFDIH9X7Eeimuaax4vsiZw" ref="#_U-EbYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U8DpMdLnEeiL7cTvwX--2w" name="ref_Segment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U8DpMtLnEeiL7cTvwX--2w" value="ref_Segment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U8DpM9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U8DpNNLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U8DpNdLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U8DpNtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U8DpN9LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U8DpONLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U8DpOdLnEeiL7cTvwX--2w" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U8DpOtLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U8Jv0NLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U8Jv0dLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U8Jv0tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U8Jv09LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U8Jv1NLnEeiL7cTvwX--2w" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U8Jv1dLnEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U8Jv1tLnEeiL7cTvwX--2w" name="PK_ref_Segment">
        <node defType="com.stambia.rdbms.colref" id="_U8Jv19LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U8Jv2NLnEeiL7cTvwX--2w" ref="#_U8DpNNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yiDmgDbREeqF3cPVFXUsDw" name="LibSegment" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yiDmgTbREeqF3cPVFXUsDw" value="LibSegment"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yiDmgjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yiDmgzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yiDmhDbREeqF3cPVFXUsDw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yiDmhTbREeqF3cPVFXUsDw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UsC3QdLnEeiL7cTvwX--2w" name="ref_ColonneTarifaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UsC3QtLnEeiL7cTvwX--2w" value="ref_ColonneTarifaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UsC3Q9LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UsI94NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UsI94dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UsI94tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UsI949LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UsI95NLnEeiL7cTvwX--2w" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UsI95dLnEeiL7cTvwX--2w" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UsI95tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UsVyMNLnEeiL7cTvwX--2w" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UsVyMdLnEeiL7cTvwX--2w" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UsVyMtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UsVyM9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UsVyNNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UsVyNdLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UsgxUNLnEeiL7cTvwX--2w" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_UsgxUdLnEeiL7cTvwX--2w" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UsgxUtLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UsgxU9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UsgxVNLnEeiL7cTvwX--2w" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UsgxVdLnEeiL7cTvwX--2w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UszFMNLnEeiL7cTvwX--2w" name="PK_ref_ColonneTarifaire">
        <node defType="com.stambia.rdbms.colref" id="_UszFMdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UszFMtLnEeiL7cTvwX--2w" ref="#_UsI94NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yQU9UDbREeqF3cPVFXUsDw" name="LibColonneTarif" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yQU9UTbREeqF3cPVFXUsDw" value="LibColonneTarif"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yQU9UjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yQU9UzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yQU9VDbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yQU9VTbREeqF3cPVFXUsDw" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U0PJ8dLnEeiL7cTvwX--2w" name="ref_TicketRepasMoyen">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U0PJ8tLnEeiL7cTvwX--2w" value="ref_TicketRepasMoyen"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U0PJ89LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U0PJ9NLnEeiL7cTvwX--2w" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0PJ9dLnEeiL7cTvwX--2w" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0PJ9tLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U0PJ99LnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0PJ-NLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0PJ-dLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0PJ-tLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U0V3oNLnEeiL7cTvwX--2w" name="TicketMoy" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0V3odLnEeiL7cTvwX--2w" value="TicketMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0V3otLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0V3o9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0V3pNLnEeiL7cTvwX--2w" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0V3pdLnEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U0V3ptLnEeiL7cTvwX--2w" name="MntMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0V3p9LnEeiL7cTvwX--2w" value="MntMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0V3qNLnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U0V3qdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0V3qtLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0V3q9LnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0V3rNLnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_U0V3rdLnEeiL7cTvwX--2w" name="MntMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_U0V3rtLnEeiL7cTvwX--2w" value="MntMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U0V3r9LnEeiL7cTvwX--2w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U0V3sNLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U0V3sdLnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U0V3stLnEeiL7cTvwX--2w" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U0V3s9LnEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U0bXMNLnEeiL7cTvwX--2w" name="PK_ref_TicketRepasMoyen">
        <node defType="com.stambia.rdbms.colref" id="_U0bXMdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U0bXMtLnEeiL7cTvwX--2w" ref="#_U0PJ9NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_U73b8dLnEeiL7cTvwX--2w" name="Client_MoisOuvert">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_U73b8tLnEeiL7cTvwX--2w" value="Client_MoisOuvert"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_U73b89LnEeiL7cTvwX--2w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_U79il9LnEeiL7cTvwX--2w" name="IdMois" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_U79imNLnEeiL7cTvwX--2w" value="IdMois"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_U79imdLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_U79imtLnEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_U79im9LnEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_U79inNLnEeiL7cTvwX--2w" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_U79indLnEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_U79intLnEeiL7cTvwX--2w" name="PK_Client_MoisOuvert">
        <node defType="com.stambia.rdbms.colref" id="_U79in9LnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U79ioNLnEeiL7cTvwX--2w" ref="#_b_vxUNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_U79iodLnEeiL7cTvwX--2w" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_U79iotLnEeiL7cTvwX--2w" ref="#_U79il9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdMois?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VERKANLnEeiL7cTvwX--2w" name="FK_Client_MoisOuvert_Client">
        <node defType="com.stambia.rdbms.relation" id="_VERKAdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VERKAtLnEeiL7cTvwX--2w" ref="#_b_vxUNX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VERKA9LnEeiL7cTvwX--2w" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_VERKBNLnEeiL7cTvwX--2w" name="FK_Client_MoisOuvert_ref_Mois">
        <node defType="com.stambia.rdbms.relation" id="_VERKBdLnEeiL7cTvwX--2w" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_VERKBtLnEeiL7cTvwX--2w" ref="#_U79il9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdMois?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_VERKB9LnEeiL7cTvwX--2w" ref="#_UtkhQNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b_vxUNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b_vxUdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b_vxUtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b_vxU9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b_vxVNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b_vxVdX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b_vxVtX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cFZtZNX7Eeimuaax4vsiZw" name="FK_Client_MoisOuvert_ref_Mois1">
        <node defType="com.stambia.rdbms.relation" id="_cFZtZdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cFZtZtX7Eeimuaax4vsiZw" ref="#_U79il9LnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdMois?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cFZtZ9X7Eeimuaax4vsiZw" ref="#_UtkhQNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_b9Fe4dX7Eeimuaax4vsiZw" name="ref_ClientType">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_b9Fe4tX7Eeimuaax4vsiZw" value="ref_ClientType"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_b9Fe49X7Eeimuaax4vsiZw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_b9Oo0NX7Eeimuaax4vsiZw" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b9Oo0dX7Eeimuaax4vsiZw" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b9Oo0tX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b9Oo09X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b9Oo1NX7Eeimuaax4vsiZw" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b9Oo1dX7Eeimuaax4vsiZw" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b9Oo1tX7Eeimuaax4vsiZw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b9Oo19X7Eeimuaax4vsiZw" name="LibTypeClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_b9Oo2NX7Eeimuaax4vsiZw" value="LibTypeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b9Oo2dX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b9Oo2tX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b9Oo29X7Eeimuaax4vsiZw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b9Oo3NX7Eeimuaax4vsiZw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_b9Oo3dX7Eeimuaax4vsiZw" name="PK_ref_ClientType">
        <node defType="com.stambia.rdbms.colref" id="_b9Oo3tX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_b9Oo39X7Eeimuaax4vsiZw" ref="#_b9Oo0NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_bulv4dX7Eeimuaax4vsiZw" name="Client_ActiviteParCanal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_bulv4tX7Eeimuaax4vsiZw" value="Client_ActiviteParCanal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_bulv49X7Eeimuaax4vsiZw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_buu50NX7Eeimuaax4vsiZw" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_buu50dX7Eeimuaax4vsiZw" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_buu50tX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_buu509X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_buu51NX7Eeimuaax4vsiZw" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_buu51dX7Eeimuaax4vsiZw" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_buu51tX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_buu519X7Eeimuaax4vsiZw" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_buu52NX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_buu52dX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_buu52tX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_buu529X7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_buu53NX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_buu53dX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_buu53tX7Eeimuaax4vsiZw" name="IdCanal" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_buu539X7Eeimuaax4vsiZw" value="IdCanal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_buu54NX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_buu54dX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_buu54tX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_buu549X7Eeimuaax4vsiZw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_buu55NX7Eeimuaax4vsiZw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_bu4q0NX7Eeimuaax4vsiZw" name="Actif12dm" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_bu4q0dX7Eeimuaax4vsiZw" value="Actif12dm"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_bu4q0tX7Eeimuaax4vsiZw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_bu4q09X7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_bu4q1NX7Eeimuaax4vsiZw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_bu4q1dX7Eeimuaax4vsiZw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_bu4q1tX7Eeimuaax4vsiZw" name="PK_ActiviteParCanal">
        <node defType="com.stambia.rdbms.colref" id="_bu4q19X7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_bu4q2NX7Eeimuaax4vsiZw" ref="#_buu50NX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cEAmQNX7Eeimuaax4vsiZw" name="FK_ActiviteParCanal_Client">
        <node defType="com.stambia.rdbms.relation" id="_cEAmQdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cEAmQtX7Eeimuaax4vsiZw" ref="#_buu519X7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cEAmQ9X7Eeimuaax4vsiZw" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_cEAmRNX7Eeimuaax4vsiZw" name="FK_ActiviteParCanal_ref_Canal1">
        <node defType="com.stambia.rdbms.relation" id="_cEAmRdX7Eeimuaax4vsiZw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_cEAmRtX7Eeimuaax4vsiZw" ref="#_buu53tX7Eeimuaax4vsiZw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdCanal?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_cEAmR9X7Eeimuaax4vsiZw" ref="#_Uz2vcNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_b4c2gdX7Eeimuaax4vsiZw" name="Client_Type">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_b4c2gtX7Eeimuaax4vsiZw" value="Client_Type"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_b4c2g9X7Eeimuaax4vsiZw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_b4c2hNX7Eeimuaax4vsiZw" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_b4c2hdX7Eeimuaax4vsiZw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b4c2htX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b4c2h9X7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b4c2iNX7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b4c2idX7Eeimuaax4vsiZw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b4c2itX7Eeimuaax4vsiZw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_b4c2i9X7Eeimuaax4vsiZw" name="IdTypeClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_b4c2jNX7Eeimuaax4vsiZw" value="IdTypeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_b4c2jdX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_b4c2jtX7Eeimuaax4vsiZw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_b4c2j9X7Eeimuaax4vsiZw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_b4c2kNX7Eeimuaax4vsiZw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_b4c2kdX7Eeimuaax4vsiZw" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_PQOw1NezEei1-oCUCrJoUg" name="ref_Departement">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_PQOw1dezEei1-oCUCrJoUg" value="ref_Departement"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_PQOw1tezEei1-oCUCrJoUg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_PQX6wNezEei1-oCUCrJoUg" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_PQX6wdezEei1-oCUCrJoUg" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_PQX6wtezEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_PQX6w9ezEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_PQX6xNezEei1-oCUCrJoUg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_PQX6xdezEei1-oCUCrJoUg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_PQX6xtezEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_PQX6x9ezEei1-oCUCrJoUg" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_PQX6yNezEei1-oCUCrJoUg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_PQX6ydezEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_PQX6ytezEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_PQX6y9ezEei1-oCUCrJoUg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_PQX6zNezEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_PQX6zdezEei1-oCUCrJoUg" name="Libelle" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_PQX6ztezEei1-oCUCrJoUg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_PQX6z9ezEei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_PQX60NezEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_PQX60dezEei1-oCUCrJoUg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_PQX60tezEei1-oCUCrJoUg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_PQX609ezEei1-oCUCrJoUg" name="PK_ref_Departement">
        <node defType="com.stambia.rdbms.colref" id="_PQX61NezEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_PQX61dezEei1-oCUCrJoUg" ref="#_PQX6wNezEei1-oCUCrJoUg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yQKlQDbREeqF3cPVFXUsDw" name="LibDepartement" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_yQKlQTbREeqF3cPVFXUsDw" value="LibDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yQKlQjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yQKlQzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yQKlRDbREeqF3cPVFXUsDw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yQKlRTbREeqF3cPVFXUsDw" value="30"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_X-xjZNe1Eei1-oCUCrJoUg" name="ref_Commune">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_X-xjZde1Eei1-oCUCrJoUg" value="ref_Commune"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_X-xjZte1Eei1-oCUCrJoUg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_X-xjZ9e1Eei1-oCUCrJoUg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-xjaNe1Eei1-oCUCrJoUg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-xjade1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_X-xjate1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-xja9e1Eei1-oCUCrJoUg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-xjbNe1Eei1-oCUCrJoUg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-xjbde1Eei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-xjbte1Eei1-oCUCrJoUg" name="IdDepartement" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-xjb9e1Eei1-oCUCrJoUg" value="IdDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-xjcNe1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_X-xjcde1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-xjcte1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-xjc9e1Eei1-oCUCrJoUg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-xjdNe1Eei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-xjdde1Eei1-oCUCrJoUg" name="NumINSEECommune" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-xjdte1Eei1-oCUCrJoUg" value="NumINSEECommune"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-xjd9e1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-xjeNe1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-xjede1Eei1-oCUCrJoUg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-xjete1Eei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-7UYNe1Eei1-oCUCrJoUg" name="PrtculariteCom" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-7UYde1Eei1-oCUCrJoUg" value="PrtculariteCom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-7UYte1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-7UY9e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-7UZNe1Eei1-oCUCrJoUg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-7UZde1Eei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-7UZte1Eei1-oCUCrJoUg" name="ComOuLieudit" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-7UZ9e1Eei1-oCUCrJoUg" value="ComOuLieudit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-7UaNe1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-7Uade1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-7Uate1Eei1-oCUCrJoUg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-7Ua9e1Eei1-oCUCrJoUg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-7UbNe1Eei1-oCUCrJoUg" name="CodePostal" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-7Ubde1Eei1-oCUCrJoUg" value="CodePostal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-7Ubte1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-7Ub9e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-7UcNe1Eei1-oCUCrJoUg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-7Ucde1Eei1-oCUCrJoUg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-7Ucte1Eei1-oCUCrJoUg" name="PrtculariteBDist" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-7Uc9e1Eei1-oCUCrJoUg" value="PrtculariteBDist"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-7UdNe1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-7Udde1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-7Udte1Eei1-oCUCrJoUg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-7Ud9e1Eei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-7UeNe1Eei1-oCUCrJoUg" name="LigneAcheminement" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-7Uede1Eei1-oCUCrJoUg" value="LigneAcheminement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-7Uete1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-7Ue9e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-7UfNe1Eei1-oCUCrJoUg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-7Ufde1Eei1-oCUCrJoUg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-7Ufte1Eei1-oCUCrJoUg" name="CodeComptaPTT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-7Uf9e1Eei1-oCUCrJoUg" value="CodeComptaPTT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-7UgNe1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-7Ugde1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-7Ugte1Eei1-oCUCrJoUg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-7Ug9e1Eei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-7UhNe1Eei1-oCUCrJoUg" name="Com" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-7Uhde1Eei1-oCUCrJoUg" value="Com"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-7Uhte1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-7Uh9e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-7UiNe1Eei1-oCUCrJoUg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-7Uide1Eei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X_FFYNe1Eei1-oCUCrJoUg" name="INSEEComRatache" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_X_FFYde1Eei1-oCUCrJoUg" value="INSEEComRatache"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X_FFYte1Eei1-oCUCrJoUg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X_FFY9e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X_FFZNe1Eei1-oCUCrJoUg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X_FFZde1Eei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_X_FFZte1Eei1-oCUCrJoUg" name="PK_ref_Commune">
        <node defType="com.stambia.rdbms.colref" id="_X_FFZ9e1Eei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_X_FFaNe1Eei1-oCUCrJoUg" ref="#_X-xjZ9e1Eei1-oCUCrJoUg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_UCtR8dkfEei1-oCUCrJoUg" name="ref_ComportementMultiCanal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_UCtR8tkfEei1-oCUCrJoUg" value="ref_ComportementMultiCanal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_UCtR89kfEei1-oCUCrJoUg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_UC2b4NkfEei1-oCUCrJoUg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_UC2b4dkfEei1-oCUCrJoUg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UC2b4tkfEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UC2b49kfEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UC2b5NkfEei1-oCUCrJoUg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UC2b5dkfEei1-oCUCrJoUg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UC2b5tkfEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UC2b59kfEei1-oCUCrJoUg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UC2b6NkfEei1-oCUCrJoUg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UC2b6dkfEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UC2b6tkfEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UC2b69kfEei1-oCUCrJoUg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UC2b7NkfEei1-oCUCrJoUg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_UC2b7dkfEei1-oCUCrJoUg" name="PK_ref_ComportementMultiCanal">
        <node defType="com.stambia.rdbms.colref" id="_UC2b7tkfEei1-oCUCrJoUg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_UC2b79kfEei1-oCUCrJoUg" ref="#_UC2b4NkfEei1-oCUCrJoUg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yjVY4DbREeqF3cPVFXUsDw" name="LibCompMultiCanal" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yjVY4TbREeqF3cPVFXUsDw" value="LibCompMultiCanal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yjVY4jbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yjVY4zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yjVY5DbREeqF3cPVFXUsDw" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yjVY5TbREeqF3cPVFXUsDw" value="30"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_CL9ksd7DEei89e-w6JO_mw" name="ref_Propriété">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_CL9kst7DEei89e-w6JO_mw" value="ref_Propriété"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_CL9ks97DEei89e-w6JO_mw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_CMBPEN7DEei89e-w6JO_mw" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_CMBPEd7DEei89e-w6JO_mw" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_CMBPEt7DEei89e-w6JO_mw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_CMBPE97DEei89e-w6JO_mw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_CMBPFN7DEei89e-w6JO_mw" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_CMBPFd7DEei89e-w6JO_mw" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_CMBPFt7DEei89e-w6JO_mw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_CMBPF97DEei89e-w6JO_mw" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_CMBPGN7DEei89e-w6JO_mw" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_CMBPGd7DEei89e-w6JO_mw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_CMBPGt7DEei89e-w6JO_mw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_CMBPG97DEei89e-w6JO_mw" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_CMBPHN7DEei89e-w6JO_mw" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_CMBPHd7DEei89e-w6JO_mw" name="PK_ref_Propriété">
        <node defType="com.stambia.rdbms.colref" id="_CMBPHt7DEei89e-w6JO_mw" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_CMBPH97DEei89e-w6JO_mw" ref="#_CMBPEN7DEei89e-w6JO_mw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yhkeUDbREeqF3cPVFXUsDw" name="LibPropriete" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yhkeUTbREeqF3cPVFXUsDw" value="LibPropriete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yhkeUjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yhkeUzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yhkeVDbREeqF3cPVFXUsDw" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yhkeVTbREeqF3cPVFXUsDw" value="15"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_CLzzsd7DEei89e-w6JO_mw" name="ref_TypeOperation">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_CLzzst7DEei89e-w6JO_mw" value="ref_TypeOperation"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_CLzzs97DEei89e-w6JO_mw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_CLzztN7DEei89e-w6JO_mw" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_CLzztd7DEei89e-w6JO_mw" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_CLzztt7DEei89e-w6JO_mw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_CLzzt97DEei89e-w6JO_mw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_CLzzuN7DEei89e-w6JO_mw" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_CLzzud7DEei89e-w6JO_mw" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_CLzzut7DEei89e-w6JO_mw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_CLzzu97DEei89e-w6JO_mw" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_CLzzvN7DEei89e-w6JO_mw" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_CLzzvd7DEei89e-w6JO_mw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_CLzzvt7DEei89e-w6JO_mw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_CLzzv97DEei89e-w6JO_mw" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_CLzzwN7DEei89e-w6JO_mw" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_CLzzwd7DEei89e-w6JO_mw" name="PK_ref_TypeOperation">
        <node defType="com.stambia.rdbms.colref" id="_CLzzwt7DEei89e-w6JO_mw" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_CLzzw97DEei89e-w6JO_mw" ref="#_CLzztN7DEei89e-w6JO_mw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ylLzADbREeqF3cPVFXUsDw" name="LibTypeOperation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ylLzATbREeqF3cPVFXUsDw" value="LibTypeOperation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ylLzAjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ylLzAzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ylLzBDbREeqF3cPVFXUsDw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ylLzBTbREeqF3cPVFXUsDw" value="15"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7GVQVODnEei8l6X2h9mGFQ" name="Infos_CHD">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7GVQVeDnEei8l6X2h9mGFQ" value="Infos_CHD"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7GVQVuDnEei8l6X2h9mGFQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_7GfBUODnEei8l6X2h9mGFQ" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7GfBUeDnEei8l6X2h9mGFQ" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7GfBUuDnEei8l6X2h9mGFQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7GfBU-DnEei8l6X2h9mGFQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7GfBVODnEei8l6X2h9mGFQ" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7GfBVeDnEei8l6X2h9mGFQ" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7GfBVuDnEei8l6X2h9mGFQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7GfBV-DnEei8l6X2h9mGFQ" name="ParamRequest" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7GfBWODnEei8l6X2h9mGFQ" value="ParamRequest"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7GfBWeDnEei8l6X2h9mGFQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7GfBWuDnEei8l6X2h9mGFQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7GfBW-DnEei8l6X2h9mGFQ" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7GfBXODnEei8l6X2h9mGFQ" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7GfBXeDnEei8l6X2h9mGFQ" name="TextResponse" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7GfBXuDnEei8l6X2h9mGFQ" value="TextResponse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7GfBX-DnEei8l6X2h9mGFQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7GfBYODnEei8l6X2h9mGFQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7GfBYeDnEei8l6X2h9mGFQ" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7GfBYuDnEei8l6X2h9mGFQ" value="max"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7GfBY-DnEei8l6X2h9mGFQ" name="CreateDate" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_7GfBZODnEei8l6X2h9mGFQ" value="CreateDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7GfBZeDnEei8l6X2h9mGFQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7GfBZuDnEei8l6X2h9mGFQ" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7GfBZ-DnEei8l6X2h9mGFQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7GfBaODnEei8l6X2h9mGFQ" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7GfBaeDnEei8l6X2h9mGFQ" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_7GfBauDnEei8l6X2h9mGFQ" name="PK_Infos_CHD">
        <node defType="com.stambia.rdbms.colref" id="_7GfBa-DnEei8l6X2h9mGFQ" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_7GfBbODnEei8l6X2h9mGFQ" ref="#_7GfBUODnEei8l6X2h9mGFQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="__LXsEPAzEem_mo4tieUuqA" name="Token" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="__LXsEfAzEem_mo4tieUuqA" value="Token"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="__LXsEvAzEem_mo4tieUuqA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="__LXsE_AzEem_mo4tieUuqA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="__LXsFPAzEem_mo4tieUuqA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="__LXsFfAzEem_mo4tieUuqA" value="500"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_RX99IPDpEem_mo4tieUuqA" name="Traite" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_RX99IfDpEem_mo4tieUuqA" value="Traite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_RX99IvDpEem_mo4tieUuqA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_RX99I_DpEem_mo4tieUuqA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_RX99JPDpEem_mo4tieUuqA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_RX99JfDpEem_mo4tieUuqA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QRdOwPDuEem_mo4tieUuqA" name="Status" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_QRdOwfDuEem_mo4tieUuqA" value="Status"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QRdOwvDuEem_mo4tieUuqA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QRdOw_DuEem_mo4tieUuqA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QRdOxPDuEem_mo4tieUuqA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QRdOxfDuEem_mo4tieUuqA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6uYi0PSmEem_mo4tieUuqA" name="FileName" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_6uYi0fSmEem_mo4tieUuqA" value="FileName"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6uYi0vSmEem_mo4tieUuqA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6uYi0_SmEem_mo4tieUuqA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6uYi1PSmEem_mo4tieUuqA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6uYi1fSmEem_mo4tieUuqA" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7DnTgeDnEei8l6X2h9mGFQ" name="ref_TypeClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7DnTguDnEei8l6X2h9mGFQ" value="ref_TypeClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7DnTg-DnEei8l6X2h9mGFQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_7DnThODnEei8l6X2h9mGFQ" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7DnTheDnEei8l6X2h9mGFQ" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7DnThuDnEei8l6X2h9mGFQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7DnTh-DnEei8l6X2h9mGFQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7DnTiODnEei8l6X2h9mGFQ" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7DnTieDnEei8l6X2h9mGFQ" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7DnTiuDnEei8l6X2h9mGFQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7DnTi-DnEei8l6X2h9mGFQ" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7DnTjODnEei8l6X2h9mGFQ" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7DnTjeDnEei8l6X2h9mGFQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7DnTjuDnEei8l6X2h9mGFQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7DnTj-DnEei8l6X2h9mGFQ" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7DnTkODnEei8l6X2h9mGFQ" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_7DxEgODnEei8l6X2h9mGFQ" name="PK_ref_TypeClient">
        <node defType="com.stambia.rdbms.colref" id="_7DxEgeDnEei8l6X2h9mGFQ" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_7DxEguDnEei8l6X2h9mGFQ" ref="#_7DnThODnEei8l6X2h9mGFQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yZzCYDbREeqF3cPVFXUsDw" name="LibTypeClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yZzCYTbREeqF3cPVFXUsDw" value="LibTypeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yZzCYjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yZzCYzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yZzCZDbREeqF3cPVFXUsDw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yZzCZTbREeqF3cPVFXUsDw" value="70"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_0dgq8eEjEeid_5OQDhd6Lw" name="Client_InfosCciales">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_0dgq8uEjEeid_5OQDhd6Lw" value="Client_InfosCciales"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_0dgq8-EjEeid_5OQDhd6Lw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_0dp04OEjEeid_5OQDhd6Lw" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_0dp04eEjEeid_5OQDhd6Lw" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0dp04uEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0dp04-EjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0dp05OEjEeid_5OQDhd6Lw" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0dp05eEjEeid_5OQDhd6Lw" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0dp05uEjEeid_5OQDhd6Lw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0dp05-EjEeid_5OQDhd6Lw" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_0dp06OEjEeid_5OQDhd6Lw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0dp06eEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0dp06uEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0dp06-EjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0dp07OEjEeid_5OQDhd6Lw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0dp07eEjEeid_5OQDhd6Lw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0duGXeEjEeid_5OQDhd6Lw" name="DateDebAffluence" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_0duGXuEjEeid_5OQDhd6Lw" value="DateDebAffluence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0duGX-EjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0duGYOEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0duGYeEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0duGYuEjEeid_5OQDhd6Lw" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0duGY-EjEeid_5OQDhd6Lw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0duGZOEjEeid_5OQDhd6Lw" name="DateFinAffluence" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_0duGZeEjEeid_5OQDhd6Lw" value="DateFinAffluence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0duGZuEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0duGZ-EjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0duGaOEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0duGaeEjEeid_5OQDhd6Lw" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0duGauEjEeid_5OQDhd6Lw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0duGa-EjEeid_5OQDhd6Lw" name="StatutCOLL" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_0duGbOEjEeid_5OQDhd6Lw" value="StatutCOLL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0duGbeEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0duGbuEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0duGb-EjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0duGcOEjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0duGceEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0duGcuEjEeid_5OQDhd6Lw" name="NiveauStanding" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_0duGc-EjEeid_5OQDhd6Lw" value="NiveauStanding"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0duGdOEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0duGdeEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0duGduEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0duGd-EjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0duGeOEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0duGeeEjEeid_5OQDhd6Lw" name="OuvertTouteAnnee" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_0duGeuEjEeid_5OQDhd6Lw" value="OuvertTouteAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0duGe-EjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0duGfOEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0duGfeEjEeid_5OQDhd6Lw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0duGfuEjEeid_5OQDhd6Lw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3QQOEjEeid_5OQDhd6Lw" name="DigitalFriendly" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3QQeEjEeid_5OQDhd6Lw" value="DigitalFriendly"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3QQuEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3QQ-EjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3QROEjEeid_5OQDhd6Lw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3QReEjEeid_5OQDhd6Lw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3QRuEjEeid_5OQDhd6Lw" name="NbLits" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3QR-EjEeid_5OQDhd6Lw" value="NbLits"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3QSOEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0d3QSeEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3QSuEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3QS-EjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3QTOEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3QTeEjEeid_5OQDhd6Lw" name="NbRepasJour" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3QTuEjEeid_5OQDhd6Lw" value="NbRepasJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3QT-EjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0d3QUOEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3QUeEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3QUuEjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3QU-EjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3QVOEjEeid_5OQDhd6Lw" name="NbCouvJour" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3QVeEjEeid_5OQDhd6Lw" value="NbCouvJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3QVuEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0d3QV-EjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3QWOEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3QWeEjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3QWuEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3QW-EjEeid_5OQDhd6Lw" name="NbChambres" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3QXOEjEeid_5OQDhd6Lw" value="NbChambres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3QXeEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0d3QXuEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3QX-EjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3QYOEjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3QYeEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3QYuEjEeid_5OQDhd6Lw" name="NbEleves" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3QY-EjEeid_5OQDhd6Lw" value="NbEleves"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3QZOEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0d3QZeEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3QZuEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3QZ-EjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3QaOEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3QaeEjEeid_5OQDhd6Lw" name="NbEmployes" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3QauEjEeid_5OQDhd6Lw" value="NbEmployes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3Qa-EjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0d3QbOEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3QbeEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3QbuEjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3Qb-EjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3QcOEjEeid_5OQDhd6Lw" name="TrancheCA" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3QceEjEeid_5OQDhd6Lw" value="TrancheCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3QcuEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0d3Qc-EjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3QdOEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3QdeEjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3QduEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3Qd-EjEeid_5OQDhd6Lw" name="TicketMoy" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3QeOEjEeid_5OQDhd6Lw" value="TicketMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3QeeEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0d3QeuEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3Qe-EjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3QfOEjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3QfeEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0d3QfuEjEeid_5OQDhd6Lw" name="PrxMoyNuit" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_0d3Qf-EjEeid_5OQDhd6Lw" value="PrxMoyNuit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0d3QgOEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0d3QgeEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0d3QguEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0d3Qg-EjEeid_5OQDhd6Lw" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0d3QhOEjEeid_5OQDhd6Lw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eBBQOEjEeid_5OQDhd6Lw" name="PotentielCArecur" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eBBQeEjEeid_5OQDhd6Lw" value="PotentielCArecur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eBBQuEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eBBQ-EjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eBBROEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eBBReEjEeid_5OQDhd6Lw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eBBRuEjEeid_5OQDhd6Lw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eBBR-EjEeid_5OQDhd6Lw" name="PotentielCAInvest" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eBBSOEjEeid_5OQDhd6Lw" value="PotentielCAInvest"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eBBSeEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eBBSuEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eBBS-EjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eBBTOEjEeid_5OQDhd6Lw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eBBTeEjEeid_5OQDhd6Lw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eBBTuEjEeid_5OQDhd6Lw" name="ScoringCA" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eBBT-EjEeid_5OQDhd6Lw" value="ScoringCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eBBUOEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eBBUeEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eBBUuEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eBBU-EjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eBBVOEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eBBe-EjEeid_5OQDhd6Lw" name="FamilleFermes" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eBBfOEjEeid_5OQDhd6Lw" value="FamilleFermes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eBBfeEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eBBfuEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eBBf-EjEeid_5OQDhd6Lw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eBBgOEjEeid_5OQDhd6Lw" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eKyQOEjEeid_5OQDhd6Lw" name="CondConcession" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eKyQeEjEeid_5OQDhd6Lw" value="CondConcession"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eKyQuEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eKyQ-EjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eKyROEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eKyReEjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eKyRuEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eKyR-EjEeid_5OQDhd6Lw" name="ScoringRelation" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eKySOEjEeid_5OQDhd6Lw" value="ScoringRelation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eKySeEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eKySuEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eKyS-EjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eKyTOEjEeid_5OQDhd6Lw" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eKyTeEjEeid_5OQDhd6Lw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eKyTuEjEeid_5OQDhd6Lw" name="Fidelite" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eKyT-EjEeid_5OQDhd6Lw" value="Fidelite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eKyUOEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eKyUeEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eKyUuEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eKyU-EjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eKyVOEjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eKyVeEjEeid_5OQDhd6Lw" name="Comportement" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eKyVuEjEeid_5OQDhd6Lw" value="Comportement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eKyV-EjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eKyWOEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eKyWeEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eKyWuEjEeid_5OQDhd6Lw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eKyW-EjEeid_5OQDhd6Lw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eKyaeEjEeid_5OQDhd6Lw" name="EcheanceProjet" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eKyauEjEeid_5OQDhd6Lw" value="EcheanceProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eKya-EjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eKybOEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eKybeEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eKybuEjEeid_5OQDhd6Lw" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eKyb-EjEeid_5OQDhd6Lw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eKycOEjEeid_5OQDhd6Lw" name="DernierProjet" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eKyceEjEeid_5OQDhd6Lw" value="DernierProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eKycuEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eKyc-EjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eKydOEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eKydeEjEeid_5OQDhd6Lw" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eKyduEjEeid_5OQDhd6Lw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eT8MOEjEeid_5OQDhd6Lw" name="CmdFinAnnee" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eT8MeEjEeid_5OQDhd6Lw" value="CmdFinAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eT8MuEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eT8M-EjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eT8NOEjEeid_5OQDhd6Lw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eT8NeEjEeid_5OQDhd6Lw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0eT8NuEjEeid_5OQDhd6Lw" name="PrchCmdFinAnnee" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_0eT8N-EjEeid_5OQDhd6Lw" value="PrchCmdFinAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0eT8OOEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0eT8OeEjEeid_5OQDhd6Lw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0eT8OuEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0eT8O-EjEeid_5OQDhd6Lw" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0eT8POEjEeid_5OQDhd6Lw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0edtWuEjEeid_5OQDhd6Lw" name="CompteChomette" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_0edtW-EjEeid_5OQDhd6Lw" value="CompteChomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0edtXOEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0edtXeEjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0edtXuEjEeid_5OQDhd6Lw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0edtX-EjEeid_5OQDhd6Lw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0em3IOEjEeid_5OQDhd6Lw" name="InvestCetteAnnee" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_0em3IeEjEeid_5OQDhd6Lw" value="InvestCetteAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0em3IuEjEeid_5OQDhd6Lw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0em3I-EjEeid_5OQDhd6Lw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0em3JOEjEeid_5OQDhd6Lw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0em3JeEjEeid_5OQDhd6Lw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_0erIneEjEeid_5OQDhd6Lw" name="PK_Client_InfosCciales">
        <node defType="com.stambia.rdbms.colref" id="_0erInuEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_0erIn-EjEeid_5OQDhd6Lw" ref="#_0dp04OEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4H4OEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_Client">
        <node defType="com.stambia.rdbms.relation" id="_0n4H4eEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4H4uEjEeid_5OQDhd6Lw" ref="#_0dp05-EjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4H4-EjEeid_5OQDhd6Lw" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4H5OEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_ColonneTarifaire">
        <node defType="com.stambia.rdbms.relation" id="_0n4H5eEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4H5uEjEeid_5OQDhd6Lw"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4H5-EjEeid_5OQDhd6Lw" ref="#_UsI94NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4H6OEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_ComportementMultiCanal">
        <node defType="com.stambia.rdbms.relation" id="_0n4H6eEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4H6uEjEeid_5OQDhd6Lw" ref="#_0eKyVeEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Comportement?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4H6-EjEeid_5OQDhd6Lw" ref="#_UC2b4NkfEei1-oCUCrJoUg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4H7OEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.relation" id="_0n4H7eEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4H7uEjEeid_5OQDhd6Lw"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4H7-EjEeid_5OQDhd6Lw" ref="#_UxuohNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4H8OEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_NbChambres">
        <node defType="com.stambia.rdbms.relation" id="_0n4H8eEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4H8uEjEeid_5OQDhd6Lw" ref="#_0d3QW-EjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=NbChambres?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4H8-EjEeid_5OQDhd6Lw" ref="#_U2iQBNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4H9OEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_NbCouvertJour">
        <node defType="com.stambia.rdbms.relation" id="_0n4H9eEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4H9uEjEeid_5OQDhd6Lw" ref="#_0d3QVOEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=NbCouvJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4H9-EjEeid_5OQDhd6Lw" ref="#_U7Zh4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4H-OEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_NbEleves">
        <node defType="com.stambia.rdbms.relation" id="_0n4H-eEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4H-uEjEeid_5OQDhd6Lw" ref="#_0d3QYuEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=NbEleves?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4H--EjEeid_5OQDhd6Lw" ref="#_VAut0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4H_OEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_NbEmployes">
        <node defType="com.stambia.rdbms.relation" id="_0n4H_eEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4H_uEjEeid_5OQDhd6Lw" ref="#_0d3QaeEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=NbEmployes?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4H_-EjEeid_5OQDhd6Lw" ref="#_U_sMBNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4IAOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_NbLits">
        <node defType="com.stambia.rdbms.relation" id="_0n4IAeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4IAuEjEeid_5OQDhd6Lw" ref="#_0d3QRuEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=NbLits?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4IA-EjEeid_5OQDhd6Lw" ref="#_UyO-0NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4IBOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_NbRepasJour">
        <node defType="com.stambia.rdbms.relation" id="_0n4IBeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4IBuEjEeid_5OQDhd6Lw" ref="#_0d3QTeEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=NbRepasJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4IB-EjEeid_5OQDhd6Lw" ref="#_U0oLgNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4ICOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_NiveauFidelite">
        <node defType="com.stambia.rdbms.relation" id="_0n4ICeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4ICuEjEeid_5OQDhd6Lw" ref="#_0eKyTuEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Fidelite?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4IC-EjEeid_5OQDhd6Lw" ref="#_UrmLUNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4IDOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_NiveauStanding">
        <node defType="com.stambia.rdbms.relation" id="_0n4IDeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4IDuEjEeid_5OQDhd6Lw" ref="#_0duGcuEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=NiveauStanding?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4ID-EjEeid_5OQDhd6Lw" ref="#_U1S54NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4IEOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_ScoringCA">
        <node defType="com.stambia.rdbms.relation" id="_0n4IEeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4IEuEjEeid_5OQDhd6Lw" ref="#_0eBBTuEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=ScoringCA?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4IE-EjEeid_5OQDhd6Lw" ref="#_U5Gb0tLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4IFOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_StatutCOLL">
        <node defType="com.stambia.rdbms.relation" id="_0n4IFeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4IFuEjEeid_5OQDhd6Lw" ref="#_0duGa-EjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=StatutCOLL?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4IF-EjEeid_5OQDhd6Lw" ref="#_UtYUANLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4IGOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_TicketRepasMoyen">
        <node defType="com.stambia.rdbms.relation" id="_0n4IGeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4IGuEjEeid_5OQDhd6Lw" ref="#_0d3Qd-EjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TicketMoy?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4IG-EjEeid_5OQDhd6Lw" ref="#_U0PJ9NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4IHOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_TrancheCA">
        <node defType="com.stambia.rdbms.relation" id="_0n4IHeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4IHuEjEeid_5OQDhd6Lw" ref="#_0d3QcOEjEeid_5OQDhd6Lw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TrancheCA?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4IH-EjEeid_5OQDhd6Lw" ref="#_U_-f5NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4IIOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_TypeOperation">
        <node defType="com.stambia.rdbms.relation" id="_0n4IIeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4IIuEjEeid_5OQDhd6Lw"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4II-EjEeid_5OQDhd6Lw" ref="#_CLzztN7DEei89e-w6JO_mw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_0n4IJOEjEeid_5OQDhd6Lw" name="FK_Client_InfosCciales_ref_TypeZone">
        <node defType="com.stambia.rdbms.relation" id="_0n4IJeEjEeid_5OQDhd6Lw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_0n4IJuEjEeid_5OQDhd6Lw"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_0n4IJ-EjEeid_5OQDhd6Lw" ref="#_U7BHYNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_-v2IF-QxEeigXob0_5VVUQ" name="CcurentRegion" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_-v2IGOQxEeigXob0_5VVUQ" value="CcurentRegion"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-v2IGeQxEeigXob0_5VVUQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-v2IGuQxEeigXob0_5VVUQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-v2IG-QxEeigXob0_5VVUQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-v2IHOQxEeigXob0_5VVUQ" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_-v2IHeQxEeigXob0_5VVUQ" name="DteCreation" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_-v2IHuQxEeigXob0_5VVUQ" value="DteCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-v2IH-QxEeigXob0_5VVUQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_-v2IIOQxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-v2IIeQxEeigXob0_5VVUQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-v2IIuQxEeigXob0_5VVUQ" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-v2II-QxEeigXob0_5VVUQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_-v2IJOQxEeigXob0_5VVUQ" name="DteMAJCcial" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_-v2IJeQxEeigXob0_5VVUQ" value="DteMAJCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-v2IJuQxEeigXob0_5VVUQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_-v2IJ-QxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-v2IKOQxEeigXob0_5VVUQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-v2IKeQxEeigXob0_5VVUQ" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-v2IKuQxEeigXob0_5VVUQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9UoYkO5IEeihWNvRK7oOPw" name="AnneeExistence" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_9UoYke5IEeihWNvRK7oOPw" value="AnneeExistence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9UoYku5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9UoYk-5IEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9UoYlO5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9UoYle5IEeihWNvRK7oOPw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9UoYlu5IEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_9dTMZO5IEeihWNvRK7oOPw" name="FK_Client_InfosCciales_ref_AnneesExistence">
        <node defType="com.stambia.rdbms.relation" id="_9dTMZe5IEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_9dTMZu5IEeihWNvRK7oOPw" ref="#_9UoYkO5IEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=AnneeExistence?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_9dTMZ-5IEeihWNvRK7oOPw" ref="#_9XwlEO5IEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4hbgd_cpEeiL3uw9NeuELg" name="Remise" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_4hbgePcpEeiL3uw9NeuELg" value="Remise"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4hbgefcpEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_4hbgevcpEeiL3uw9NeuELg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4hbge_cpEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4hbgfPcpEeiL3uw9NeuELg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4hbgffcpEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4hbgfvcpEeiL3uw9NeuELg" name="ColTarifaire" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_4hbgf_cpEeiL3uw9NeuELg" value="ColTarifaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4hbggPcpEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4hbggfcpEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4hbggvcpEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4hbgg_cpEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_RntKsH1YEem-bPviwv5ZLw" name="RepasServis" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_RntKsX1YEem-bPviwv5ZLw" value="RepasServis"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_RntKsn1YEem-bPviwv5ZLw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_RntKs31YEem-bPviwv5ZLw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_RntKtH1YEem-bPviwv5ZLw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_RntKtX1YEem-bPviwv5ZLw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_RntKtn1YEem-bPviwv5ZLw" name="Prestations" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_RntKt31YEem-bPviwv5ZLw" value="Prestations"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_RntKuH1YEem-bPviwv5ZLw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_RntKuX1YEem-bPviwv5ZLw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_RntKun1YEem-bPviwv5ZLw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_RntKu31YEem-bPviwv5ZLw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_-zX9MeQxEeigXob0_5VVUQ" name="ref_TypeEtabNiv2">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_-zX9MuQxEeigXob0_5VVUQ" value="ref_TypeEtabNiv2"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_-zX9M-QxEeigXob0_5VVUQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_-zX9NOQxEeigXob0_5VVUQ" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_-zX9NeQxEeigXob0_5VVUQ" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-zX9NuQxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_-zX9N-QxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-zX9OOQxEeigXob0_5VVUQ" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-zX9OeQxEeigXob0_5VVUQ" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-zX9OuQxEeigXob0_5VVUQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_-zX9O-QxEeigXob0_5VVUQ" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_-zX9POQxEeigXob0_5VVUQ" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-zX9PeQxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-zX9PuQxEeigXob0_5VVUQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-zX9P-QxEeigXob0_5VVUQ" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-zX9QOQxEeigXob0_5VVUQ" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_-zX9QeQxEeigXob0_5VVUQ" name="IdTypeEtabN1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_-zX9QuQxEeigXob0_5VVUQ" value="IdTypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-zX9Q-QxEeigXob0_5VVUQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_-zX9ROQxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-zX9ReQxEeigXob0_5VVUQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-zX9RuQxEeigXob0_5VVUQ" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-zX9R-QxEeigXob0_5VVUQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_-zhHIOQxEeigXob0_5VVUQ" name="PK_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.colref" id="_-zhHIeQxEeigXob0_5VVUQ" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_-zhHIuQxEeigXob0_5VVUQ" ref="#_-zX9NOQxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_-8RaiOQxEeigXob0_5VVUQ" name="FK_ref_TypeEtabNiv2_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_-8RaieQxEeigXob0_5VVUQ" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_-8RaiuQxEeigXob0_5VVUQ" ref="#_-zX9QeQxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdTypeEtabN1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_-8Rai-QxEeigXob0_5VVUQ" ref="#_-zq4IOQxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yftdIDbREeqF3cPVFXUsDw" name="LibTypeEtabN2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yftdITbREeqF3cPVFXUsDw" value="LibTypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yftdIjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yftdIzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yftdJDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yftdJTbREeqF3cPVFXUsDw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_-gF00eQxEeigXob0_5VVUQ" name="ref_TypeMenu">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_-gF00uQxEeigXob0_5VVUQ" value="ref_TypeMenu"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_-gPl0OQxEeigXob0_5VVUQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_-g5tIOQxEeigXob0_5VVUQ" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_-g5tIeQxEeigXob0_5VVUQ" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-g5tIuQxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_-g5tI-QxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-g5tJOQxEeigXob0_5VVUQ" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-g5tJeQxEeigXob0_5VVUQ" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-g5tJuQxEeigXob0_5VVUQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_-hVyAOQxEeigXob0_5VVUQ" name="PK_ref_TypeMenu">
        <node defType="com.stambia.rdbms.colref" id="_-hVyAeQxEeigXob0_5VVUQ" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_-hVyAuQxEeigXob0_5VVUQ" ref="#_-g5tIOQxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4EJKJugYEeicCszix7eWDA" name="IdSpecCulinaire" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_4EJKJ-gYEeicCszix7eWDA" value="IdSpecCulinaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4EJKKOgYEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_4EJKKegYEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4EJKKugYEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4EJKK-gYEeicCszix7eWDA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4EJKLOgYEeicCszix7eWDA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_-SFqa_coEem_mo4tieUuqA" name="LibTypeMenu" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_-SFqbPcoEem_mo4tieUuqA" value="LibTypeMenu"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-SFqbfcoEem_mo4tieUuqA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-SFqbvcoEem_mo4tieUuqA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-SFqb_coEem_mo4tieUuqA" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-SFqcPcoEem_mo4tieUuqA" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_-zhHJOQxEeigXob0_5VVUQ" name="ref_TypeEtabNiv1">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_-zhHJeQxEeigXob0_5VVUQ" value="ref_TypeEtabNiv1"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_-zhHJuQxEeigXob0_5VVUQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_-zq4IOQxEeigXob0_5VVUQ" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_-zq4IeQxEeigXob0_5VVUQ" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-zq4IuQxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_-zq4I-QxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-zq4JOQxEeigXob0_5VVUQ" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-zq4JeQxEeigXob0_5VVUQ" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-zq4JuQxEeigXob0_5VVUQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_-zq4J-QxEeigXob0_5VVUQ" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_-zq4KOQxEeigXob0_5VVUQ" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-zq4KeQxEeigXob0_5VVUQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-zq4KuQxEeigXob0_5VVUQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-zq4K-QxEeigXob0_5VVUQ" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-zq4LOQxEeigXob0_5VVUQ" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_-zq4LeQxEeigXob0_5VVUQ" name="PK_TypeEtablissement">
        <node defType="com.stambia.rdbms.colref" id="_-zq4LuQxEeigXob0_5VVUQ" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_-zq4L-QxEeigXob0_5VVUQ" ref="#_-zq4IOQxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yf5qYDbREeqF3cPVFXUsDw" name="LibTypeEtabN1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yf5qYTbREeqF3cPVFXUsDw" value="LibTypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yf5qYjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yf5qYzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yf5qZDbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yf5qZTbREeqF3cPVFXUsDw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_-y7RROQxEeigXob0_5VVUQ" name="ref_TypeEtabNiv3">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_-y7RReQxEeigXob0_5VVUQ" value="ref_TypeEtabNiv3"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_-y7RRuQxEeigXob0_5VVUQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_-y7RR-QxEeigXob0_5VVUQ" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_-y7RSOQxEeigXob0_5VVUQ" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-y7RSeQxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_-y7RSuQxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-y7RS-QxEeigXob0_5VVUQ" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-y7RTOQxEeigXob0_5VVUQ" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-y7RTeQxEeigXob0_5VVUQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_-zEbMOQxEeigXob0_5VVUQ" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_-zEbMeQxEeigXob0_5VVUQ" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-zEbMuQxEeigXob0_5VVUQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-zEbM-QxEeigXob0_5VVUQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-zEbNOQxEeigXob0_5VVUQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-zEbNeQxEeigXob0_5VVUQ" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_-zEbPeQxEeigXob0_5VVUQ" name="PK_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.colref" id="_-zEbPuQxEeigXob0_5VVUQ" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_-zEbP-QxEeigXob0_5VVUQ" ref="#_-y7RR-QxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_skmr1ui0EeicCszix7eWDA" name="IdTypeEtabN2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_skmr1-i0EeicCszix7eWDA" value="IdTypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_skmr2Oi0EeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_skmr2ei0EeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_skmr2ui0EeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_skmr2-i0EeicCszix7eWDA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_skmr3Oi0EeicCszix7eWDA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yfQKIDbREeqF3cPVFXUsDw" name="LibTypeEtabN3" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yfQKITbREeqF3cPVFXUsDw" value="LibTypeEtabN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yfQKIjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yfQKIzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yfQKJDbREeqF3cPVFXUsDw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yfQKJTbREeqF3cPVFXUsDw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_siB49ui0EeicCszix7eWDA" name="ref_CHD_TypeEtab">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_siB49-i0EeicCszix7eWDA" value="ref_CHD_TypeEtab"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_siB4-Oi0EeicCszix7eWDA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_siLp8Oi0EeicCszix7eWDA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_siLp8ei0EeicCszix7eWDA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_siLp8ui0EeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_siLp8-i0EeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_siLp9Oi0EeicCszix7eWDA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_siLp9ei0EeicCszix7eWDA" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_siLp9ui0EeicCszix7eWDA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_siUz4Oi0EeicCszix7eWDA" name="GFCCode" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_siUz4ei0EeicCszix7eWDA" value="GFCCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_siUz4ui0EeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_siUz4-i0EeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_siUz5Oi0EeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_siUz5ei0EeicCszix7eWDA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_siUz5ui0EeicCszix7eWDA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_siiPQOi0EeicCszix7eWDA" name="TypeEtabN1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_siiPQei0EeicCszix7eWDA" value="TypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_siiPQui0EeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_siiPQ-i0EeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_siiPROi0EeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_siiPRei0EeicCszix7eWDA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_siiPRui0EeicCszix7eWDA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sisAQOi0EeicCszix7eWDA" name="TypeEtabN2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_sisAQei0EeicCszix7eWDA" value="TypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sisAQui0EeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_sisAQ-i0EeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sisAROi0EeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sisARei0EeicCszix7eWDA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_sisARui0EeicCszix7eWDA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_si1xQOi0EeicCszix7eWDA" name="TypeEtabN3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_si1xQei0EeicCszix7eWDA" value="TypeEtabN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_si1xQui0EeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_si1xQ-i0EeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_si1xROi0EeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_si1xRei0EeicCszix7eWDA" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_si1xRui0EeicCszix7eWDA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_si1xR-i0EeicCszix7eWDA" name="PK_ref_CHD_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_si1xSOi0EeicCszix7eWDA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_si1xSei0EeicCszix7eWDA" ref="#_siLp8Oi0EeicCszix7eWDA?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_st3ViOi0EeicCszix7eWDA" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_st3Viei0EeicCszix7eWDA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_st3Viui0EeicCszix7eWDA" ref="#_siiPQOi0EeicCszix7eWDA?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeEtabN1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_st3Vi-i0EeicCszix7eWDA" ref="#_-zq4IOQxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_st3VjOi0EeicCszix7eWDA" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.relation" id="_st3Vjei0EeicCszix7eWDA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_st3Vjui0EeicCszix7eWDA" ref="#_sisAQOi0EeicCszix7eWDA?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeEtabN2?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_st3Vj-i0EeicCszix7eWDA" ref="#_-zX9NOQxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_st3VkOi0EeicCszix7eWDA" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.relation" id="_st3Vkei0EeicCszix7eWDA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_st3Vkui0EeicCszix7eWDA" ref="#_si1xQOi0EeicCszix7eWDA?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeEtabN3?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_st3Vk-i0EeicCszix7eWDA" ref="#_-y7RR-QxEeigXob0_5VVUQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_CN4WwAI7Eeqxq-mJ8CTEbw" name="CodeNature" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_CN4WwQI7Eeqxq-mJ8CTEbw" value="CodeNature"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_CN4WwgI7Eeqxq-mJ8CTEbw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_CN4WwwI7Eeqxq-mJ8CTEbw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_CN4WxAI7Eeqxq-mJ8CTEbw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_CN4WxQI7Eeqxq-mJ8CTEbw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_CN5k4AI7Eeqxq-mJ8CTEbw" name="CodeTypo" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_CN5k4QI7Eeqxq-mJ8CTEbw" value="CodeTypo"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_CN5k4gI7Eeqxq-mJ8CTEbw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_CN5k4wI7Eeqxq-mJ8CTEbw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_CN5k5AI7Eeqxq-mJ8CTEbw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_CN5k5QI7Eeqxq-mJ8CTEbw" value="2"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_JPXzAOwqEeihWNvRK7oOPw" name="Client_Organisation">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_JPXzAewqEeihWNvRK7oOPw" value="Client_Organisation"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_JPXzAuwqEeihWNvRK7oOPw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_JPbdYOwqEeihWNvRK7oOPw" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPbdYewqEeihWNvRK7oOPw" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPbdYuwqEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_JPbdY-wqEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPbdZOwqEeihWNvRK7oOPw" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPbdZewqEeihWNvRK7oOPw" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPbdZuwqEeihWNvRK7oOPw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPcrgOwqEeihWNvRK7oOPw" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPcrgewqEeihWNvRK7oOPw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPcrguwqEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_JPcrg-wqEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPcrhOwqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPcrhewqEeihWNvRK7oOPw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPcrhuwqEeihWNvRK7oOPw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPd5oOwqEeihWNvRK7oOPw" name="Secteur" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPd5oewqEeihWNvRK7oOPw" value="Secteur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPd5ouwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPd5o-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPd5pOwqEeihWNvRK7oOPw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPd5pewqEeihWNvRK7oOPw" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPfHwOwqEeihWNvRK7oOPw" name="Dic" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPfHwewqEeihWNvRK7oOPw" value="Dic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPfHwuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPfHw-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPfHxOwqEeihWNvRK7oOPw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPfHxewqEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPfu0OwqEeihWNvRK7oOPw" name="Dec" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPfu0ewqEeihWNvRK7oOPw" value="Dec"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPfu0uwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPfu0-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPfu1OwqEeihWNvRK7oOPw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPfu1ewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPg88OwqEeihWNvRK7oOPw" name="MagasinFavori" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPg88ewqEeihWNvRK7oOPw" value="MagasinFavori"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPg88uwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPg88-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPg89OwqEeihWNvRK7oOPw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPg89ewqEeihWNvRK7oOPw" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPiLEOwqEeihWNvRK7oOPw" name="CentraleAchat" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPiLEewqEeihWNvRK7oOPw" value="CentraleAchat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPiLEuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_JPiLE-wqEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPiLFOwqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPiLFewqEeihWNvRK7oOPw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPiLFuwqEeihWNvRK7oOPw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPiyIOwqEeihWNvRK7oOPw" name="AppelOffres" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPiyIewqEeihWNvRK7oOPw" value="AppelOffres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPiyIuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPiyI-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPiyJOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPiyJewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPkAQOwqEeihWNvRK7oOPw" name="MarchePublic" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPkAQewqEeihWNvRK7oOPw" value="MarchePublic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPkAQuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPkAQ-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPkAROwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPkARewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPlOYOwqEeihWNvRK7oOPw" name="DateFinContrat" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPlOYewqEeihWNvRK7oOPw" value="DateFinContrat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPlOYuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_JPlOY-wqEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPlOZOwqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPlOZewqEeihWNvRK7oOPw" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPlOZuwqEeihWNvRK7oOPw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPl1cOwqEeihWNvRK7oOPw" name="TaciteReconduction" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPl1cewqEeihWNvRK7oOPw" value="TaciteReconduction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPl1cuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPl1c-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPl1dOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPl1dewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPnDkOwqEeihWNvRK7oOPw" name="MercuFerme" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPnDkewqEeihWNvRK7oOPw" value="MercuFerme"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPnDkuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPnDk-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPnDlOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPnDlewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPpf0OwqEeihWNvRK7oOPw" name="Mercuriale" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPpf0ewqEeihWNvRK7oOPw" value="Mercuriale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPpf0uwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPpf0-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPpf1OwqEeihWNvRK7oOPw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPpf1ewqEeihWNvRK7oOPw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPqG4OwqEeihWNvRK7oOPw" name="FctParBudget" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPqG4ewqEeihWNvRK7oOPw" value="FctParBudget"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPqG4uwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPqG4-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPqG5OwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPqG5ewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPrVAOwqEeihWNvRK7oOPw" name="VisiteCcial" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPrVAewqEeihWNvRK7oOPw" value="VisiteCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPrVAuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPrVA-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPrVBOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPrVBewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPsjIOwqEeihWNvRK7oOPw" name="AppelOutcall" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPsjIewqEeihWNvRK7oOPw" value="AppelOutcall"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPsjIuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPsjI-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPsjJOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPsjJewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPtKMOwqEeihWNvRK7oOPw" name="PassageMag" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPtKMewqEeihWNvRK7oOPw" value="PassageMag"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPtKMuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPtKM-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPtKNOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPtKNewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPuYUOwqEeihWNvRK7oOPw" name="PrefOutcall" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPuYUewqEeihWNvRK7oOPw" value="PrefOutcall"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPuYUuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPuYU-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPuYVOwqEeihWNvRK7oOPw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPuYVewqEeihWNvRK7oOPw" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPvmcOwqEeihWNvRK7oOPw" name="PrefVisites" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPvmcewqEeihWNvRK7oOPw" value="PrefVisites"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPvmcuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPvmc-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPvmdOwqEeihWNvRK7oOPw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPvmdewqEeihWNvRK7oOPw" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPw0kOwqEeihWNvRK7oOPw" name="AdherentCtraleAch" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPw0kewqEeihWNvRK7oOPw" value="AdherentCtraleAch"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPw0kuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPw0k-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPw0lOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPw0lewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPyCsOwqEeihWNvRK7oOPw" name="NumAdherent" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPyCsewqEeihWNvRK7oOPw" value="NumAdherent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPyCsuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPyCs-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPyCtOwqEeihWNvRK7oOPw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPyCtewqEeihWNvRK7oOPw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPypwOwqEeihWNvRK7oOPw" name="HoraireLivSouhait" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPypwewqEeihWNvRK7oOPw" value="HoraireLivSouhait"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPypwuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPypw-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPypxOwqEeihWNvRK7oOPw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPypxewqEeihWNvRK7oOPw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JPz34OwqEeihWNvRK7oOPw" name="NumCtraleAch" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_JPz34ewqEeihWNvRK7oOPw" value="NumCtraleAch"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JPz34uwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JPz34-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JPz35OwqEeihWNvRK7oOPw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JPz35ewqEeihWNvRK7oOPw" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JP1GAOwqEeihWNvRK7oOPw" name="TypeOp" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_JP1GAewqEeihWNvRK7oOPw" value="TypeOp"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JP1GAuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_JP1GA-wqEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JP1GBOwqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JP1GBewqEeihWNvRK7oOPw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JP1GBuwqEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JP2UIOwqEeihWNvRK7oOPw" name="MercuContrat" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_JP2UIewqEeihWNvRK7oOPw" value="MercuContrat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JP2UIuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JP2UI-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JP2UJOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JP2UJewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JP27MOwqEeihWNvRK7oOPw" name="RespMercu" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_JP27MewqEeihWNvRK7oOPw" value="RespMercu"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JP27MuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JP27M-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JP27NOwqEeihWNvRK7oOPw" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JP27NewqEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JP4JUOwqEeihWNvRK7oOPw" name="FacturesDemat" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_JP4JUewqEeihWNvRK7oOPw" value="FacturesDemat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JP4JUuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JP4JU-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JP4JVOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JP4JVewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JP5XcOwqEeihWNvRK7oOPw" name="MailFacture" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_JP5XcewqEeihWNvRK7oOPw" value="MailFacture"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JP5XcuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JP5Xc-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JP5XdOwqEeihWNvRK7oOPw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JP5XdewqEeihWNvRK7oOPw" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JP6lkOwqEeihWNvRK7oOPw" name="multicompte" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_JP6lkewqEeihWNvRK7oOPw" value="multicompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JP6lkuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JP6lk-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JP6llOwqEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JP6llewqEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JP7zsOwqEeihWNvRK7oOPw" name="Chaine" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_JP7zsewqEeihWNvRK7oOPw" value="Chaine"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JP7zsuwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JP7zs-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JP7ztOwqEeihWNvRK7oOPw" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JP7ztewqEeihWNvRK7oOPw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JP9B0OwqEeihWNvRK7oOPw" name="Centralisateur" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_JP9B0ewqEeihWNvRK7oOPw" value="Centralisateur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JP9B0uwqEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JP9B0-wqEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JP9B1OwqEeihWNvRK7oOPw" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_JP9B1ewqEeihWNvRK7oOPw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_JQAFIOwqEeihWNvRK7oOPw" name="PK_Client_Organisation">
        <node defType="com.stambia.rdbms.colref" id="_JQAFIewqEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_JQAFIuwqEeihWNvRK7oOPw" ref="#_JPbdYOwqEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Jb5MEOwqEeihWNvRK7oOPw" name="FK_Client_Organisation_Client">
        <node defType="com.stambia.rdbms.relation" id="_Jb5MEewqEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Jb5MEuwqEeihWNvRK7oOPw" ref="#_JPcrgOwqEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Jb5ME-wqEeihWNvRK7oOPw" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_Jb5MFOwqEeihWNvRK7oOPw" name="FK_Client_Organisation_ref_TypeOperation">
        <node defType="com.stambia.rdbms.relation" id="_Jb5MFewqEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_Jb5MFuwqEeihWNvRK7oOPw" ref="#_JP1GAOwqEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=TypeOp?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_Jb5MF-wqEeihWNvRK7oOPw" ref="#_CLzztN7DEei89e-w6JO_mw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_1NXfAOyyEeihWNvRK7oOPw" name="HabitudeCmd" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_1NXfAeyyEeihWNvRK7oOPw" value="HabitudeCmd"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1NXfAuyyEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1NXfA-yyEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1NXfBOyyEeihWNvRK7oOPw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1NXfBeyyEeihWNvRK7oOPw" value="512"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_1GSmoeyyEeihWNvRK7oOPw" name="Client_Activite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_1GSmouyyEeihWNvRK7oOPw" value="Client_Activite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_1GSmo-yyEeihWNvRK7oOPw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_1GW4EOyyEeihWNvRK7oOPw" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_1GW4EeyyEeihWNvRK7oOPw" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1GW4EuyyEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_1GW4E-yyEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1GW4FOyyEeihWNvRK7oOPw" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1GW4FeyyEeihWNvRK7oOPw" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1GW4FuyyEeihWNvRK7oOPw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_1GYtQOyyEeihWNvRK7oOPw" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_1GYtQeyyEeihWNvRK7oOPw" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1GYtQuyyEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_1GYtQ-yyEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1GYtROyyEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1GYtReyyEeihWNvRK7oOPw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1GYtRuyyEeihWNvRK7oOPw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_1GbJgOyyEeihWNvRK7oOPw" name="PK_Client_Activite">
        <node defType="com.stambia.rdbms.colref" id="_1GbJgeyyEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_1GbJguyyEeihWNvRK7oOPw" ref="#_1GW4EOyyEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_1a_EQOyyEeihWNvRK7oOPw" name="FK_Client_Activite_Client">
        <node defType="com.stambia.rdbms.relation" id="_1a_EQeyyEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_1a_EQuyyEeihWNvRK7oOPw" ref="#_1GYtQOyyEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_1a_EQ-yyEeihWNvRK7oOPw" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9LH3QO5IEeihWNvRK7oOPw" name="ActifMoisClosN" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_9LH3Qe5IEeihWNvRK7oOPw" value="ActifMoisClosN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9LH3Qu5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9LH3Q-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9LH3RO5IEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9LH3Re5IEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9LJFYO5IEeihWNvRK7oOPw" name="ActifMoisClosN_1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_9LJFYe5IEeihWNvRK7oOPw" value="ActifMoisClosN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9LJFYu5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9LJFY-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9LJFZO5IEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9LJFZe5IEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9LKTgO5IEeihWNvRK7oOPw" name="Actif12DM" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_9LKTge5IEeihWNvRK7oOPw" value="Actif12DM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9LKTgu5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9LKTg-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9LKThO5IEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9LKThe5IEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9LLhoO5IEeihWNvRK7oOPw" name="Actif13_24DM" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_9LLhoe5IEeihWNvRK7oOPw" value="Actif13_24DM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9LLhou5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9LLho-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9LLhpO5IEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9LLhpe5IEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9LMvwO5IEeihWNvRK7oOPw" name="ActifN" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_9LMvwe5IEeihWNvRK7oOPw" value="ActifN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9LMvwu5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9LMvw-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9LMvxO5IEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9LMvxe5IEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9LN94O5IEeihWNvRK7oOPw" name="ActifPeriodeN_1" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_9LN94e5IEeihWNvRK7oOPw" value="ActifPeriodeN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9LN94u5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9LN94-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9LN95O5IEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9LN95e5IEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9LPMAO5IEeihWNvRK7oOPw" name="ActifN_1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_9LPMAe5IEeihWNvRK7oOPw" value="ActifN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9LPMAu5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9LPMA-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9LPMBO5IEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9LPMBe5IEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9LQaIO5IEeihWNvRK7oOPw" name="ActifN_2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_9LQaIe5IEeihWNvRK7oOPw" value="ActifN_2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9LQaIu5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9LQaI-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9LQaJO5IEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9LQaJe5IEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9LRoQO5IEeihWNvRK7oOPw" name="ActifN_3" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_9LRoQe5IEeihWNvRK7oOPw" value="ActifN_3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9LRoQu5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9LRoQ-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9LRoRO5IEeihWNvRK7oOPw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9LRoRe5IEeihWNvRK7oOPw" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_9Xrske5IEeihWNvRK7oOPw" name="ref_AnneesExistence">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_9XsToO5IEeihWNvRK7oOPw" value="ref_AnneesExistence"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_9XsToe5IEeihWNvRK7oOPw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_9XwlEO5IEeihWNvRK7oOPw" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_9XwlEe5IEeihWNvRK7oOPw" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9XwlEu5IEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9XwlE-5IEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9XwlFO5IEeihWNvRK7oOPw" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9XwlFe5IEeihWNvRK7oOPw" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9XwlFu5IEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9XyaQO5IEeihWNvRK7oOPw" name="libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_9XyaQe5IEeihWNvRK7oOPw" value="libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9XyaQu5IEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9XyaQ-5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9XyaRO5IEeihWNvRK7oOPw" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9XyaRe5IEeihWNvRK7oOPw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9XzoYO5IEeihWNvRK7oOPw" name="AnneeMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_9XzoYe5IEeihWNvRK7oOPw" value="AnneeMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9XzoYu5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9XzoY-5IEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9XzoZO5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9XzoZe5IEeihWNvRK7oOPw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9XzoZu5IEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9X02gO5IEeihWNvRK7oOPw" name="AnneeMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_9X02ge5IEeihWNvRK7oOPw" value="AnneeMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9X02gu5IEeihWNvRK7oOPw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9X02g-5IEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9X02hO5IEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9X02he5IEeihWNvRK7oOPw" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9X02hu5IEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_9X350O5IEeihWNvRK7oOPw" name="PK_ref_AnneesExistence">
        <node defType="com.stambia.rdbms.colref" id="_9X350e5IEeihWNvRK7oOPw" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_9X350u5IEeihWNvRK7oOPw" ref="#_9XwlEO5IEeihWNvRK7oOPw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UrqRABUQEeqPGbHwfkinWw" name="libAnneeExistence" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_UrqRARUQEeqPGbHwfkinWw" value="libAnneeExistence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UrqRAhUQEeqPGbHwfkinWw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UrqRAxUQEeqPGbHwfkinWw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UrqRBBUQEeqPGbHwfkinWw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UrqRBRUQEeqPGbHwfkinWw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.variable" id="_LhG2EfekEeiL3uw9NeuELg" name="MaxCodeClient">
      <attribute defType="com.stambia.rdbms.variable.sql" id="_QOo8MPekEeiL3uw9NeuELg" value="select max(Code) as MaxCode from client"/>
      <attribute defType="com.stambia.rdbms.variable.type" id="_ExnTsPeuEeiL3uw9NeuELg" value="Integer"/>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_VRnRZPelEeiL3uw9NeuELg" name="Wrk_CustomerTable">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_VRnRZfelEeiL3uw9NeuELg" value="Wrk_CustomerTable"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_VRnRZvelEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_VRnRZ_elEeiL3uw9NeuELg" name="CodeCli" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_VRnRaPelEeiL3uw9NeuELg" value="CodeCli"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VRnRafelEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VRnRavelEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VRnRa_elEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VRnRbPelEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VRnRbfelEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_03PthffIEeiL3uw9NeuELg" name="Statut" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_03PthvfIEeiL3uw9NeuELg" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_03Pth_fIEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_03PtiPfIEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_03PtiffIEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_03PtivfIEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_mfltjvibEeiL3uw9NeuELg" name="DateCreation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_mfltj_ibEeiL3uw9NeuELg" value="DateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mfltkPibEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_mfltkfibEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mfltkvibEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mfltk_ibEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mfltlPibEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_mfvegPibEeiL3uw9NeuELg" name="DateTraitement" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_mfvegfibEeiL3uw9NeuELg" value="DateTraitement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mfvegvibEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_mfveg_ibEeiL3uw9NeuELg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mfvehPibEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mfvehfibEeiL3uw9NeuELg" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mfvehvibEeiL3uw9NeuELg" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_mfveh_ibEeiL3uw9NeuELg" name="ProgrammeTRT" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_mfveiPibEeiL3uw9NeuELg" value="ProgrammeTRT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mfveifibEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mfveivibEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mfvei_ibEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mfvejPibEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ZAISPfiwEeiL3uw9NeuELg" name="Action" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_ZAISPviwEeiL3uw9NeuELg" value="Action"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ZAISP_iwEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ZAISQPiwEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ZAISQfiwEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ZAISQviwEeiL3uw9NeuELg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_29qnPvyFEeio0ZiqzluJkw" name="TimeCreation" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_29qnP_yFEeio0ZiqzluJkw" value="TimeCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_29qnQPyFEeio0ZiqzluJkw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_29qnQfyFEeio0ZiqzluJkw" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_29qnQvyFEeio0ZiqzluJkw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_29qnQ_yFEeio0ZiqzluJkw" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_29qnRPyFEeio0ZiqzluJkw" value="23"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_098yavfIEeiL3uw9NeuELg" name="Rejected_Customers">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_098ya_fIEeiL3uw9NeuELg" value="Rejected_Customers"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_098ybPfIEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_0-GjYPfIEeiL3uw9NeuELg" name="CodeClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_0-GjYffIEeiL3uw9NeuELg" value="CodeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0-GjYvfIEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0-GjY_fIEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0-GjZPfIEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0-GjZffIEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0-GjZvfIEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0-GjZ_fIEeiL3uw9NeuELg" name="dateRejet" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_0-GjaPfIEeiL3uw9NeuELg" value="dateRejet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0-GjaffIEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0-GjavfIEeiL3uw9NeuELg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0-Gja_fIEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0-GjbPfIEeiL3uw9NeuELg" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0-GjbffIEeiL3uw9NeuELg" value="23"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_cojQgf45EeiVrKKMla1zGg" name="Wrk_ChometteCustomers">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_cojQgv45EeiVrKKMla1zGg" value="Wrk_ChometteCustomers"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_cojQg_45EeiVrKKMla1zGg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_cokeoP45EeiVrKKMla1zGg" name="CodeClient" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_cokeof45EeiVrKKMla1zGg" value="CodeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cokeov45EeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cokeo_45EeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cokepP45EeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cokepf45EeiVrKKMla1zGg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cokepv45EeiVrKKMla1zGg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cooJBv45EeiVrKKMla1zGg" name="dateCreation" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_cooJB_45EeiVrKKMla1zGg" value="dateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cooJCP45EeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cooJCf45EeiVrKKMla1zGg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cooJCv45EeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cooJC_45EeiVrKKMla1zGg" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cooJDP45EeiVrKKMla1zGg" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ryCYF_79EeiVrKKMla1zGg" name="Statut" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_ryCYGP79EeiVrKKMla1zGg" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ryCYGf79EeiVrKKMla1zGg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ryCYGv79EeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ryCYG_79EeiVrKKMla1zGg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ryCYHP79EeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5MkDgP79EeiVrKKMla1zGg" name="FileName" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_5MkDgf79EeiVrKKMla1zGg" value="FileName"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5MkDgv79EeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5MkDg_79EeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5MkDhP79EeiVrKKMla1zGg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5MkDhf79EeiVrKKMla1zGg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_z9h9YP7-EeiVrKKMla1zGg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_z9h9Yf7-EeiVrKKMla1zGg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z9h9Yv7-EeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_z9h9Y_7-EeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z9h9ZP7-EeiVrKKMla1zGg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z9h9Zf7-EeiVrKKMla1zGg" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z9h9Zv7-EeiVrKKMla1zGg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_z9h9gf7-EeiVrKKMla1zGg" name="PK_Wrk_ChometteCustomers">
        <node defType="com.stambia.rdbms.colref" id="_z9h9gv7-EeiVrKKMla1zGg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_z9h9g_7-EeiVrKKMla1zGg" ref="#_z9h9YP7-EeiVrKKMla1zGg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_PKSprf7_EeiVrKKMla1zGg" name="TypeFile" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_PKSprv7_EeiVrKKMla1zGg" value="TypeFile"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_PKSpr_7_EeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_PKSpsP7_EeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_PKSpsf7_EeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_PKSpsv7_EeiVrKKMla1zGg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_PKSps_7_EeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="__q7CFv-BEeiVrKKMla1zGg" name="NewCode" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="__q7CF_-BEeiVrKKMla1zGg" value="NewCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="__q7CGP-BEeiVrKKMla1zGg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="__q7CGf-BEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="__q7CGv-BEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="__q7CG_-BEeiVrKKMla1zGg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="__q7CHP-BEeiVrKKMla1zGg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_1nLahwIZEem6Tdv8fAX8OA" name="CodeMagento" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_1nLaiAIZEem6Tdv8fAX8OA" value="CodeMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1nLaiQIZEem6Tdv8fAX8OA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_1nLaigIZEem6Tdv8fAX8OA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1nLaiwIZEem6Tdv8fAX8OA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1nLajAIZEem6Tdv8fAX8OA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1nLajQIZEem6Tdv8fAX8OA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6XzUKwIdEem6Tdv8fAX8OA" name="Mode" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_6XzULAIdEem6Tdv8fAX8OA" value="Mode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6XzULQIdEem6Tdv8fAX8OA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6XzULgIdEem6Tdv8fAX8OA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6XzULwIdEem6Tdv8fAX8OA" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6XzUMAIdEem6Tdv8fAX8OA" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_1brbGgd9EemyyJxuIKe_6A" name="wrk_Log_WS_Integration_Commande">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_1brbGwd9EemyyJxuIKe_6A" value="wrk_Log_WS_Integration_Commande"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_1brbHAd9EemyyJxuIKe_6A" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_1b1MEAd9EemyyJxuIKe_6A" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_1b1MEQd9EemyyJxuIKe_6A" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1b1MEgd9EemyyJxuIKe_6A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_1b1MEwd9EemyyJxuIKe_6A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1b1MFAd9EemyyJxuIKe_6A" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1b1MFQd9EemyyJxuIKe_6A" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1b1MFgd9EemyyJxuIKe_6A" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_1b1MFwd9EemyyJxuIKe_6A" name="InputXML" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_1b1MGAd9EemyyJxuIKe_6A" value="InputXML"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1b1MGQd9EemyyJxuIKe_6A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1b1MGgd9EemyyJxuIKe_6A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1b1MGwd9EemyyJxuIKe_6A" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1b1MHAd9EemyyJxuIKe_6A" value="max"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_1b1MHQd9EemyyJxuIKe_6A" name="OutputResponse" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_1b1MHgd9EemyyJxuIKe_6A" value="OutputResponse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1b1MHwd9EemyyJxuIKe_6A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1b1MIAd9EemyyJxuIKe_6A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1b1MIQd9EemyyJxuIKe_6A" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1b1MIgd9EemyyJxuIKe_6A" value="500"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_1b1MIwd9EemyyJxuIKe_6A" name="PK_wrk_Log_WS_Integration_Commande">
        <node defType="com.stambia.rdbms.colref" id="_1b1MJAd9EemyyJxuIKe_6A" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_1b1MJQd9EemyyJxuIKe_6A" ref="#_1b1MEAd9EemyyJxuIKe_6A?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_o0sLzgeFEemyyJxuIKe_6A" name="SessionId" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_o0sLzweFEemyyJxuIKe_6A" value="SessionId"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_o0sL0AeFEemyyJxuIKe_6A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_o0sL0QeFEemyyJxuIKe_6A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_o0sL0geFEemyyJxuIKe_6A" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_o0sL0weFEemyyJxuIKe_6A" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QWASkgeJEemyyJxuIKe_6A" name="RequestDate" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_QWASkweJEemyyJxuIKe_6A" value="RequestDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QWASlAeJEemyyJxuIKe_6A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_QWASlQeJEemyyJxuIKe_6A" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QWASlgeJEemyyJxuIKe_6A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QWASlweJEemyyJxuIKe_6A" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QWASmAeJEemyyJxuIKe_6A" value="23"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.variable" id="_MUyUoRQSEemsgZOghPO5Pg" name="CurrentDate">
      <attribute defType="com.stambia.rdbms.variable.type" id="_Qe6ywBQSEemsgZOghPO5Pg" value="String"/>
      <attribute defType="com.stambia.rdbms.variable.defaultValue" id="_RaEMYBQSEemsgZOghPO5Pg" value="%x{md:formatDate('yyyyMMdd_HHMMSS')}x%"/>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_GdFbRhQvEemsgZOghPO5Pg" name="Client_UserWebTel">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_GdFbRxQvEemsgZOghPO5Pg" value="Client_UserWebTel"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_GdFbSBQvEemsgZOghPO5Pg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_GdFbSRQvEemsgZOghPO5Pg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_GdFbShQvEemsgZOghPO5Pg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GdFbSxQvEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_GdFbTBQvEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GdFbTRQvEemsgZOghPO5Pg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GdFbThQvEemsgZOghPO5Pg" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GdFbTxQvEemsgZOghPO5Pg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GdPMQBQvEemsgZOghPO5Pg" name="IdUser" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_GdPMQRQvEemsgZOghPO5Pg" value="IdUser"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GdPMQhQvEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_GdPMQxQvEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GdPMRBQvEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GdPMRRQvEemsgZOghPO5Pg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GdPMRhQvEemsgZOghPO5Pg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GdPMRxQvEemsgZOghPO5Pg" name="Numero" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_GdPMSBQvEemsgZOghPO5Pg" value="Numero"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GdPMSRQvEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GdPMShQvEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GdPMSxQvEemsgZOghPO5Pg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GdPMTBQvEemsgZOghPO5Pg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GdPMTRQvEemsgZOghPO5Pg" name="Idtype_Tel" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_GdPMThQvEemsgZOghPO5Pg" value="Idtype_Tel"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GdPMTxQvEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_GdPMUBQvEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GdPMURQvEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GdPMUhQvEemsgZOghPO5Pg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GdPMUxQvEemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_GdY9QBQvEemsgZOghPO5Pg" name="PK_Client_UserTel">
        <node defType="com.stambia.rdbms.colref" id="_GdY9QRQvEemsgZOghPO5Pg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_GdY9QhQvEemsgZOghPO5Pg" ref="#_GdFbSRQvEemsgZOghPO5Pg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_G8Ay8BQvEemsgZOghPO5Pg" name="FK_Client_UserWebTel_Client_UserWeb">
        <node defType="com.stambia.rdbms.relation" id="_G8Ay8RQvEemsgZOghPO5Pg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_G8Ay8hQvEemsgZOghPO5Pg" ref="#_GdPMQBQvEemsgZOghPO5Pg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdUser?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_G8Ay8xQvEemsgZOghPO5Pg" ref="#_U8o4ANLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_G8Ay9BQvEemsgZOghPO5Pg" name="FK_Client_UserWebTel_ref_TypeTel">
        <node defType="com.stambia.rdbms.relation" id="_G8Ay9RQvEemsgZOghPO5Pg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_G8Ay9hQvEemsgZOghPO5Pg" ref="#_GdPMTRQvEemsgZOghPO5Pg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Idtype_Tel?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_G8Ay9xQvEemsgZOghPO5Pg" ref="#_U2Du4NLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_4MX0-iu3Eemv78OLPmYyMg" name="Mercuriales">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_4MX0-yu3Eemv78OLPmYyMg" value="Mercuriales"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_4MX0_Cu3Eemv78OLPmYyMg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_4MX0_Su3Eemv78OLPmYyMg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_4MX0_iu3Eemv78OLPmYyMg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4MX0_yu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_4MX1ACu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4MX1ASu3Eemv78OLPmYyMg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4MX1Aiu3Eemv78OLPmYyMg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4MX1Ayu3Eemv78OLPmYyMg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4MX1BCu3Eemv78OLPmYyMg" name="NomMercu" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_4MX1BSu3Eemv78OLPmYyMg" value="NomMercu"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4MX1Biu3Eemv78OLPmYyMg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4MX1Byu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4MX1CCu3Eemv78OLPmYyMg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4MX1CSu3Eemv78OLPmYyMg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_4MX1Ciu3Eemv78OLPmYyMg" name="PK_Mercuriales">
        <node defType="com.stambia.rdbms.colref" id="_4MX1Cyu3Eemv78OLPmYyMg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_4MX1DCu3Eemv78OLPmYyMg" ref="#_4MX0_Su3Eemv78OLPmYyMg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_VVp3QVe1EemGruvsZ9L3ag" name="sysdiagrams">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_VVp3Qle1EemGruvsZ9L3ag" value="sysdiagrams"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_VVp3Q1e1EemGruvsZ9L3ag" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_VVrscFe1EemGruvsZ9L3ag" name="name" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_VVrscVe1EemGruvsZ9L3ag" value="name"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VVrscle1EemGruvsZ9L3ag" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VVrsc1e1EemGruvsZ9L3ag" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VVrsdFe1EemGruvsZ9L3ag" value="sysname"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VVrsdVe1EemGruvsZ9L3ag" value="128"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VVs6kFe1EemGruvsZ9L3ag" name="principal_id" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_VVs6kVe1EemGruvsZ9L3ag" value="principal_id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VVs6kle1EemGruvsZ9L3ag" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VVs6k1e1EemGruvsZ9L3ag" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VVs6lFe1EemGruvsZ9L3ag" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VVs6lVe1EemGruvsZ9L3ag" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VVs6lle1EemGruvsZ9L3ag" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VVuIsFe1EemGruvsZ9L3ag" name="diagram_id" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_VVuIsVe1EemGruvsZ9L3ag" value="diagram_id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VVuIsle1EemGruvsZ9L3ag" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VVuIs1e1EemGruvsZ9L3ag" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VVuItFe1EemGruvsZ9L3ag" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VVuItVe1EemGruvsZ9L3ag" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VVuItle1EemGruvsZ9L3ag" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VVvW0Fe1EemGruvsZ9L3ag" name="version" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_VVvW0Ve1EemGruvsZ9L3ag" value="version"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VVvW0le1EemGruvsZ9L3ag" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VVvW01e1EemGruvsZ9L3ag" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VVvW1Fe1EemGruvsZ9L3ag" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VVvW1Ve1EemGruvsZ9L3ag" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VVvW1le1EemGruvsZ9L3ag" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VVwk8Fe1EemGruvsZ9L3ag" name="definition" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_VVwk8Ve1EemGruvsZ9L3ag" value="definition"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VVwk8le1EemGruvsZ9L3ag" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VVwk81e1EemGruvsZ9L3ag" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VVwk9Fe1EemGruvsZ9L3ag" value="varbinary"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VVwk9Ve1EemGruvsZ9L3ag" value="max"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_VVzoQFe1EemGruvsZ9L3ag" name="PK__sysdiagr__C2B05B61D789EBAD">
        <node defType="com.stambia.rdbms.colref" id="_VVzoQVe1EemGruvsZ9L3ag" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_VVzoQle1EemGruvsZ9L3ag" ref="#_VVuIsFe1EemGruvsZ9L3ag?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=diagram_id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_VVSD0Ve1EemGruvsZ9L3ag" name="ref_NiveauConfiance">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_VVSD0le1EemGruvsZ9L3ag" value="ref_NiveauConfiance"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_VVSD01e1EemGruvsZ9L3ag" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_VVXjYFe1EemGruvsZ9L3ag" name="IdNivConfiance" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_VVXjYVe1EemGruvsZ9L3ag" value="IdNivConfiance"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VVXjYle1EemGruvsZ9L3ag" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_VVXjY1e1EemGruvsZ9L3ag" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VVXjZFe1EemGruvsZ9L3ag" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VVXjZVe1EemGruvsZ9L3ag" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VVXjZle1EemGruvsZ9L3ag" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_VVZYkFe1EemGruvsZ9L3ag" name="Niveau" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_VVZYkVe1EemGruvsZ9L3ag" value="Niveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_VVZYkle1EemGruvsZ9L3ag" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_VVZYk1e1EemGruvsZ9L3ag" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_VVZYlFe1EemGruvsZ9L3ag" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_VVZYlVe1EemGruvsZ9L3ag" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_VVcb4Fe1EemGruvsZ9L3ag" name="PK_ref_NiveauConfiance">
        <node defType="com.stambia.rdbms.colref" id="_VVcb4Ve1EemGruvsZ9L3ag" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_VVcb4le1EemGruvsZ9L3ag" ref="#_VVXjYFe1EemGruvsZ9L3ag?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=IdNivConfiance?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_S8bOoVrtEem5CN5A2MFeMQ" name="Wrk_ChometteUsers">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_S8bOolrtEem5CN5A2MFeMQ" value="Wrk_ChometteUsers"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_S8bOo1rtEem5CN5A2MFeMQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_S8e5AFrtEem5CN5A2MFeMQ" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_S8e5AVrtEem5CN5A2MFeMQ" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S8e5AlrtEem5CN5A2MFeMQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S8e5A1rtEem5CN5A2MFeMQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S8e5BFrtEem5CN5A2MFeMQ" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S8e5BVrtEem5CN5A2MFeMQ" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S8e5BlrtEem5CN5A2MFeMQ" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S8gHIFrtEem5CN5A2MFeMQ" name="CodeMagento" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_S8gHIVrtEem5CN5A2MFeMQ" value="CodeMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S8gHIlrtEem5CN5A2MFeMQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S8gHI1rtEem5CN5A2MFeMQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S8gHJFrtEem5CN5A2MFeMQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S8gHJVrtEem5CN5A2MFeMQ" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S8gHJlrtEem5CN5A2MFeMQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S8guMFrtEem5CN5A2MFeMQ" name="CodeMagentoUser" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_S8guMVrtEem5CN5A2MFeMQ" value="CodeMagentoUser"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S8guMlrtEem5CN5A2MFeMQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S8guM1rtEem5CN5A2MFeMQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S8guNFrtEem5CN5A2MFeMQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S8guNVrtEem5CN5A2MFeMQ" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S8guNlrtEem5CN5A2MFeMQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S8h8UFrtEem5CN5A2MFeMQ" name="CodeClient" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_S8h8UVrtEem5CN5A2MFeMQ" value="CodeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S8h8UlrtEem5CN5A2MFeMQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S8h8U1rtEem5CN5A2MFeMQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S8h8VFrtEem5CN5A2MFeMQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S8h8VVrtEem5CN5A2MFeMQ" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S8h8VlrtEem5CN5A2MFeMQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S8ijYFrtEem5CN5A2MFeMQ" name="FileName" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_S8ijYVrtEem5CN5A2MFeMQ" value="FileName"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S8ijYlrtEem5CN5A2MFeMQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S8ijY1rtEem5CN5A2MFeMQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S8ijZFrtEem5CN5A2MFeMQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S8ijZVrtEem5CN5A2MFeMQ" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S8jxgFrtEem5CN5A2MFeMQ" name="dateCreation" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_S8jxgVrtEem5CN5A2MFeMQ" value="dateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S8jxglrtEem5CN5A2MFeMQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S8jxg1rtEem5CN5A2MFeMQ" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S8jxhFrtEem5CN5A2MFeMQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S8jxhVrtEem5CN5A2MFeMQ" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S8jxhlrtEem5CN5A2MFeMQ" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S8k_oFrtEem5CN5A2MFeMQ" name="Statut" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_S8k_oVrtEem5CN5A2MFeMQ" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S8k_olrtEem5CN5A2MFeMQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S8k_o1rtEem5CN5A2MFeMQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S8k_pFrtEem5CN5A2MFeMQ" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S8k_pVrtEem5CN5A2MFeMQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S8lmsFrtEem5CN5A2MFeMQ" name="Mode" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_S8lmsVrtEem5CN5A2MFeMQ" value="Mode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S8lmslrtEem5CN5A2MFeMQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S8lms1rtEem5CN5A2MFeMQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S8lmtFrtEem5CN5A2MFeMQ" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S8lmtVrtEem5CN5A2MFeMQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_S8oC8FrtEem5CN5A2MFeMQ" name="PK_Wrk_ChometteUsers">
        <node defType="com.stambia.rdbms.colref" id="_S8oC8VrtEem5CN5A2MFeMQ" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_S8oC8lrtEem5CN5A2MFeMQ" ref="#_S8e5AFrtEem5CN5A2MFeMQ?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_O9CQ4PZ9Eem_mo4tieUuqA" name="CltAEnrichir">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_O9CQ4fZ9Eem_mo4tieUuqA" value="CltAEnrichir"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_O9CQ4vZ9Eem_mo4tieUuqA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_O9mRkPZ9Eem_mo4tieUuqA" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_O9mRkfZ9Eem_mo4tieUuqA" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_O9mRkvZ9Eem_mo4tieUuqA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_O9mRk_Z9Eem_mo4tieUuqA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_O9mRlPZ9Eem_mo4tieUuqA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_O9mRlfZ9Eem_mo4tieUuqA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_O9mRlvZ9Eem_mo4tieUuqA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_O9oGwPZ9Eem_mo4tieUuqA" name="Siret" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_O9oGwfZ9Eem_mo4tieUuqA" value="Siret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_O9oGwvZ9Eem_mo4tieUuqA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_O9oGw_Z9Eem_mo4tieUuqA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_O9oGxPZ9Eem_mo4tieUuqA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_O9oGxfZ9Eem_mo4tieUuqA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_O9ot0PZ9Eem_mo4tieUuqA" name="Traite" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_O9ot0fZ9Eem_mo4tieUuqA" value="Traite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_O9ot0vZ9Eem_mo4tieUuqA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_O9ot0_Z9Eem_mo4tieUuqA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_O9ot1PZ9Eem_mo4tieUuqA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_O9ot1fZ9Eem_mo4tieUuqA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_O9sYMPZ9Eem_mo4tieUuqA" name="PK_CltAEnrichir">
        <node defType="com.stambia.rdbms.colref" id="_O9sYMfZ9Eem_mo4tieUuqA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_O9sYMvZ9Eem_mo4tieUuqA" ref="#_O9mRkPZ9Eem_mo4tieUuqA?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_PSm4QPZ9Eem_mo4tieUuqA" name="FK_CltAEnrichir_Client">
        <node defType="com.stambia.rdbms.relation" id="_PSm4QfZ9Eem_mo4tieUuqA" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_PSm4QvZ9Eem_mo4tieUuqA"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_PSm4Q_Z9Eem_mo4tieUuqA" ref="#_UtwugNLnEeiL7cTvwX--2w?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_1JkrEPdGEem_mo4tieUuqA" name="CreateDate" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_1JkrEfdGEem_mo4tieUuqA" value="CreateDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1JkrEvdGEem_mo4tieUuqA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_1JkrE_dGEem_mo4tieUuqA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1JkrFPdGEem_mo4tieUuqA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1JkrFfdGEem_mo4tieUuqA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_1JkrFvdGEem_mo4tieUuqA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9lKFYPdIEem_mo4tieUuqA" name="ClientCode" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_9lKFYfdIEem_mo4tieUuqA" value="ClientCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9lKFYvdIEem_mo4tieUuqA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9lKFY_dIEem_mo4tieUuqA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9lKFZPdIEem_mo4tieUuqA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9lKFZfdIEem_mo4tieUuqA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9lKFZvdIEem_mo4tieUuqA" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_yQnRMTbREeqF3cPVFXUsDw" name="wrk_wsSociete_Out">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_yQnRMjbREeqF3cPVFXUsDw" value="wrk_wsSociete_Out"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_yQnRMzbREeqF3cPVFXUsDw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_yQvNADbREeqF3cPVFXUsDw" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_yQvNATbREeqF3cPVFXUsDw" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yQvNAjbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_yQvNAzbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yQvNBDbREeqF3cPVFXUsDw" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yQvNBTbREeqF3cPVFXUsDw" value="bigint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yQvNBjbREeqF3cPVFXUsDw" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yQxCMDbREeqF3cPVFXUsDw" name="id_societe_chomette" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_yQxCMTbREeqF3cPVFXUsDw" value="id_societe_chomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yQxCMjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_yQxCMzbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yQxCNDbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yQxCNTbREeqF3cPVFXUsDw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yQxCNjbREeqF3cPVFXUsDw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yQ0FgDbREeqF3cPVFXUsDw" name="id_societe_magento" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_yQ0FgTbREeqF3cPVFXUsDw" value="id_societe_magento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yQ0FgjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_yQ0FgzbREeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yQ0FhDbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yQ0FhTbREeqF3cPVFXUsDw" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yQ0FhjbREeqF3cPVFXUsDw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yQ2hwDbREeqF3cPVFXUsDw" name="statut" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_yQ2hwTbREeqF3cPVFXUsDw" value="statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yQ2hwjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yQ2hwzbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yQ2hxDbREeqF3cPVFXUsDw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yQ2hxTbREeqF3cPVFXUsDw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yQ4W8DbREeqF3cPVFXUsDw" name="message" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_yQ4W8TbREeqF3cPVFXUsDw" value="message"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yQ4W8jbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yQ4W8zbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yQ4W9DbREeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yQ4W9TbREeqF3cPVFXUsDw" value="500"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yQ6MIDbREeqF3cPVFXUsDw" name="datecreation" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_yQ6MITbREeqF3cPVFXUsDw" value="datecreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yQ6MIjbREeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_yQ6MIzbREeqF3cPVFXUsDw" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yQ6MJDbREeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yQ6MJTbREeqF3cPVFXUsDw" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yQ6MJjbREeqF3cPVFXUsDw" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_yQ_EoDbREeqF3cPVFXUsDw" name="PK_wrk_wsSociete_Out">
        <node defType="com.stambia.rdbms.colref" id="_yQ_EoTbREeqF3cPVFXUsDw" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_yQ_EojbREeqF3cPVFXUsDw" ref="#_yQvNADbREeqF3cPVFXUsDw?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LsstMDbWEeqF3cPVFXUsDw" name="response_code" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_LstUQDbWEeqF3cPVFXUsDw" value="response_code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LstUQTbWEeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LstUQjbWEeqF3cPVFXUsDw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LstUQzbWEeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LstURDbWEeqF3cPVFXUsDw" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LstURTbWEeqF3cPVFXUsDw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Lst7UDbWEeqF3cPVFXUsDw" name="response_msg" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_Lst7UTbWEeqF3cPVFXUsDw" value="response_msg"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Lst7UjbWEeqF3cPVFXUsDw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Lst7UzbWEeqF3cPVFXUsDw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Lst7VDbWEeqF3cPVFXUsDw" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Lst7VTbWEeqF3cPVFXUsDw" value="500"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_hutOYUDzEeq2oaTfF82isg" name="CltMaj_DI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_hutOYkDzEeq2oaTfF82isg" value="CltMaj_DI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_hutOY0DzEeq2oaTfF82isg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_huwRsEDzEeq2oaTfF82isg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_huwRsUDzEeq2oaTfF82isg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_huwRskDzEeq2oaTfF82isg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_huwRs0DzEeq2oaTfF82isg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_huwRtEDzEeq2oaTfF82isg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_huwRtUDzEeq2oaTfF82isg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_huwRtkDzEeq2oaTfF82isg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_huw4wEDzEeq2oaTfF82isg" name="CltCode" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_huw4wUDzEeq2oaTfF82isg" value="CltCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_huw4wkDzEeq2oaTfF82isg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_huw4w0DzEeq2oaTfF82isg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_huw4xEDzEeq2oaTfF82isg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_huw4xUDzEeq2oaTfF82isg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_huw4xkDzEeq2oaTfF82isg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_huyG4EDzEeq2oaTfF82isg" name="Traite" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_huyG4UDzEeq2oaTfF82isg" value="Traite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_huyG4kDzEeq2oaTfF82isg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_huyG40DzEeq2oaTfF82isg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_huyG5EDzEeq2oaTfF82isg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_huyG5UDzEeq2oaTfF82isg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_huzVAEDzEeq2oaTfF82isg" name="CreateDate" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_huzVAUDzEeq2oaTfF82isg" value="CreateDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_huzVAkDzEeq2oaTfF82isg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_huzVA0DzEeq2oaTfF82isg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_huzVBEDzEeq2oaTfF82isg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_huzVBUDzEeq2oaTfF82isg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_huzVBkDzEeq2oaTfF82isg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_hu1KMEDzEeq2oaTfF82isg" name="PK_CltMaj_DI">
        <node defType="com.stambia.rdbms.colref" id="_hu1KMUDzEeq2oaTfF82isg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_hu1KMkDzEeq2oaTfF82isg" ref="#_huwRsEDzEeq2oaTfF82isg?fileId=_zhKmwNLmEeiL7cTvwX--2w$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_mIK3gdVoEeq6RL_1vDZCgg" name="wrktrg_importcustomer">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_mIK3gtVoEeq6RL_1vDZCgg" value="wrktrg_importcustomer"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_mIK3g9VoEeq6RL_1vDZCgg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_mINTwNVoEeq6RL_1vDZCgg" name="idtask" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_mINTwdVoEeq6RL_1vDZCgg" value="idtask"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mINTwtVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_mINTw9VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mINTxNVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mINTxdVoEeq6RL_1vDZCgg" value="bigint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mINTxtVoEeq6RL_1vDZCgg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_mIOh4NVoEeq6RL_1vDZCgg" name="action" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_mIOh4dVoEeq6RL_1vDZCgg" value="action"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mIOh4tVoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_mIOh49VoEeq6RL_1vDZCgg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mIOh5NVoEeq6RL_1vDZCgg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mIOh5dVoEeq6RL_1vDZCgg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mIOh5tVoEeq6RL_1vDZCgg" value="5"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_zX0TAD3mEeq2oaTfF82isg" name="ECFDBstambia.dbo">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_1hvM4D3mEeq2oaTfF82isg" value="ECFDBstambia"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_1hvz8D3mEeq2oaTfF82isg" value="dbo"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_1hvz8T3mEeq2oaTfF82isg" value="R_[targetName]__${/CORE_SESSION_ID}$"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_1hwbAD3mEeq2oaTfF82isg" value="L[number]_[targetName]__${/CORE_SESSION_ID}$"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_1hwbAT3mEeq2oaTfF82isg" value="I_[targetName]__${/CORE_SESSION_ID}$"/>
    <attribute defType="com.stambia.rdbms.schema.stagingMask" id="_xHFwED3yEeq2oaTfF82isg" value="S[number]_${/CORE_SESSION_ID}$"/>
  </node>
</md:node>