<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_gdbxgAkoEemca7hN01bE4g" name="CHOMETTE" md:ref="../Dialog_Insight/ibm_db2_400.rdbms.md#UUID_MD_RDBMS_DB2_400?fileId=UUID_MD_RDBMS_DB2_400$type=md$name=IBM%20DB2/400?">
  <attribute defType="com.stambia.rdbms.server.url" id="_p9hAoAkoEemca7hN01bE4g" value="jdbc:as400://chomette"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_p9hAoQkoEemca7hN01bE4g" value="com.ibm.as400.access.AS400JDBCDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_p9hAogkoEemca7hN01bE4g" value="gescom"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_qimAkHfqEema_LGoZACb8w" value="86E98B5A4EEE31BDBD8DF145839CA9DF"/>
  <node defType="com.stambia.rdbms.schema" id="_gkbxYAkoEemca7hN01bE4g" name="ORIONBD">
    <attribute defType="com.stambia.rdbms.schema.name" id="_glBnQAkoEemca7hN01bE4g" value="ORIONBD"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_glBnQQkoEemca7hN01bE4g" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_glBnQgkoEemca7hN01bE4g" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_glBnQwkoEemca7hN01bE4g" value="I_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.work" id="_45HqEP8JEemVAft-X6rPVg" ref="#_Ub_nsP7zEemVAft-X6rPVg?fileId=_gdbxgAkoEemca7hN01bE4g$type=md$name=STAMBIA?"/>
    <node defType="com.stambia.rdbms.datastore" id="_piss4QkoEemca7hN01bE4g" name="ORB0REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_piss4gkoEemca7hN01bE4g" value="ORB0REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_piss4wkoEemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_puBMEAkoEemca7hN01bE4g" name="B0BPCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMEQkoEemca7hN01bE4g" value="B0BPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMEgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puBMEwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMFAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMFQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMFgkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMFwkoEemca7hN01bE4g" name="B0EONB" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMGAkoEemca7hN01bE4g" value="B0EONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMGQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puBMGgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMGwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMHAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMHQkoEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMHgkoEemca7hN01bE4g" name="B0BTST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMHwkoEemca7hN01bE4g" value="B0BTST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMIAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMIQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMIgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMIwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMJAkoEemca7hN01bE4g" name="B0BFTX" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMJQkoEemca7hN01bE4g" value="B0BFTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMJgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMJwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMKAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMKQkoEemca7hN01bE4g" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMKgkoEemca7hN01bE4g" name="B0BUST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMKwkoEemca7hN01bE4g" value="B0BUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMLAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMLQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMLgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMLwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMMAkoEemca7hN01bE4g" name="B0BWST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMMQkoEemca7hN01bE4g" value="B0BWST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMMgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMMwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMNAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMNQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMNgkoEemca7hN01bE4g" name="B0CAS1" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMNwkoEemca7hN01bE4g" value="B0CAS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMOAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMOQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMOgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMOwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMPAkoEemca7hN01bE4g" name="B0BXST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMPQkoEemca7hN01bE4g" value="B0BXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMPgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMPwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMQAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMQQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMQgkoEemca7hN01bE4g" name="B0JGST" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMQwkoEemca7hN01bE4g" value="B0JGST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMRAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMRQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMRgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMRwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMSAkoEemca7hN01bE4g" name="B0JHST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMSQkoEemca7hN01bE4g" value="B0JHST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMSgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMSwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMTAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMTQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMTgkoEemca7hN01bE4g" name="B0C0CD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMTwkoEemca7hN01bE4g" value="B0C0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMUAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puBMUQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMUgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMUwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMVAkoEemca7hN01bE4g" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMVQkoEemca7hN01bE4g" name="B0M5ST" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMVgkoEemca7hN01bE4g" value="B0M5ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMVwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMWAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMWQkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMWgkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puBMWwkoEemca7hN01bE4g" name="B0JNST" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_puBMXAkoEemca7hN01bE4g" value="B0JNST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puBMXQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puBMXgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puBMXwkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puBMYAkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9EAkoEemca7hN01bE4g" name="B0ARCD" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9EQkoEemca7hN01bE4g" value="B0ARCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9EgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9EwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9FAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9FQkoEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9FgkoEemca7hN01bE4g" name="B0AMCD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9FwkoEemca7hN01bE4g" value="B0AMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9GAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9GQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9GgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9GwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9HAkoEemca7hN01bE4g" name="B0GHCD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9HQkoEemca7hN01bE4g" value="B0GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9HgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9HwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9IAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9IQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9IgkoEemca7hN01bE4g" name="B0AQCD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9IwkoEemca7hN01bE4g" value="B0AQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9JAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9JQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9JgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9JwkoEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9KAkoEemca7hN01bE4g" name="B0BHCD" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9KQkoEemca7hN01bE4g" value="B0BHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9KgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9KwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9LAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9LQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9LgkoEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9LwkoEemca7hN01bE4g" name="B0D1NB" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9MAkoEemca7hN01bE4g" value="B0D1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9MQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9MgkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9MwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9NAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9NQkoEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9NgkoEemca7hN01bE4g" name="B0I1NB" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9NwkoEemca7hN01bE4g" value="B0I1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9OAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9OQkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9OgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9OwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9PAkoEemca7hN01bE4g" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9PQkoEemca7hN01bE4g" name="B0ADCD" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9PgkoEemca7hN01bE4g" value="B0ADCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9PwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9QAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9QQkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9QgkoEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9QwkoEemca7hN01bE4g" name="B0D3C1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9RAkoEemca7hN01bE4g" value="B0D3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9RQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9RgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9RwkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9SAkoEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9SQkoEemca7hN01bE4g" name="B0GMST" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9SgkoEemca7hN01bE4g" value="B0GMST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9SwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9TAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9TQkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9TgkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9TwkoEemca7hN01bE4g" name="B0JPST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9UAkoEemca7hN01bE4g" value="B0JPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9UQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9UgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9UwkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9VAkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9VQkoEemca7hN01bE4g" name="B0FIPR" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9VgkoEemca7hN01bE4g" value="B0FIPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9VwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9WAkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9WQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9WgkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9WwkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9XAkoEemca7hN01bE4g" name="B0GTP1" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9XQkoEemca7hN01bE4g" value="B0GTP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9XgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9XwkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9YAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9YQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9YgkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9YwkoEemca7hN01bE4g" name="B0DWPR" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9ZAkoEemca7hN01bE4g" value="B0DWPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9ZQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9ZgkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9ZwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9aAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9aQkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9agkoEemca7hN01bE4g" name="B0DMVA" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9awkoEemca7hN01bE4g" value="B0DMVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9bAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9bQkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9bgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9bwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9cAkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9cQkoEemca7hN01bE4g" name="B0BTPR" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9cgkoEemca7hN01bE4g" value="B0BTPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9cwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9dAkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9dQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9dgkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9dwkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9eAkoEemca7hN01bE4g" name="B0GWP1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9eQkoEemca7hN01bE4g" value="B0GWP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9egkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9ewkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9fAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9fQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9fgkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9fwkoEemca7hN01bE4g" name="B0GVP1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9gAkoEemca7hN01bE4g" value="B0GVP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9gQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9ggkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9gwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9hAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9hQkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9hgkoEemca7hN01bE4g" name="B0GUP1" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9hwkoEemca7hN01bE4g" value="B0GUP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9iAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9iQkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9igkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9iwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9jAkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9jQkoEemca7hN01bE4g" name="B0HIP1" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9jgkoEemca7hN01bE4g" value="B0HIP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9jwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9kAkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9kQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9kgkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9kwkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9lAkoEemca7hN01bE4g" name="B0DKVA" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9lQkoEemca7hN01bE4g" value="B0DKVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9lgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9lwkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9mAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9mQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9mgkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9mwkoEemca7hN01bE4g" name="B0GXP2" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9nAkoEemca7hN01bE4g" value="B0GXP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9nQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9ngkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9nwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9oAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9oQkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9ogkoEemca7hN01bE4g" name="B0BFDT" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9owkoEemca7hN01bE4g" value="B0BFDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9pAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9pQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9pgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9pwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9qAkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9qQkoEemca7hN01bE4g" name="B0ASCD" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9qgkoEemca7hN01bE4g" value="B0ASCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9qwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9rAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9rQkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9rgkoEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9rwkoEemca7hN01bE4g" name="B0JQST" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9sAkoEemca7hN01bE4g" value="B0JQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9sQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9sgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9swkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9tAkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9tQkoEemca7hN01bE4g" name="B0BZST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9tgkoEemca7hN01bE4g" value="B0BZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9twkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9uAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9uQkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9ugkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9uwkoEemca7hN01bE4g" name="B0E6DT" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9vAkoEemca7hN01bE4g" value="B0E6DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9vQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9vgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9vwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9wAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9wQkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9wgkoEemca7hN01bE4g" name="B0BYST" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9wwkoEemca7hN01bE4g" value="B0BYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9xAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9xQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9xgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9xwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9yAkoEemca7hN01bE4g" name="B0KMDT" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9yQkoEemca7hN01bE4g" value="B0KMDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9ygkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK9ywkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9zAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9zQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9zgkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9zwkoEemca7hN01bE4g" name="B0B0ST" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK90AkoEemca7hN01bE4g" value="B0B0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK90QkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK90gkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK90wkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK91AkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK91QkoEemca7hN01bE4g" name="B0A4DT" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK91gkoEemca7hN01bE4g" value="B0A4DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK91wkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puK92AkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK92QkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK92gkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK92wkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK93AkoEemca7hN01bE4g" name="B0B1ST" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK93QkoEemca7hN01bE4g" value="B0B1ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK93gkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK93wkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK94AkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK94QkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK94gkoEemca7hN01bE4g" name="B0JIST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK94wkoEemca7hN01bE4g" value="B0JIST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK95AkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK95QkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK95gkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK95wkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK96AkoEemca7hN01bE4g" name="B0JJST" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK96QkoEemca7hN01bE4g" value="B0JJST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK96gkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK96wkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK97AkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK97QkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK97gkoEemca7hN01bE4g" name="B0JKST" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK97wkoEemca7hN01bE4g" value="B0JKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK98AkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK98QkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK98gkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK98wkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK99AkoEemca7hN01bE4g" name="B0JMST" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK99QkoEemca7hN01bE4g" value="B0JMST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK99gkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK99wkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9-AkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9-QkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK9-gkoEemca7hN01bE4g" name="B0HSST" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK9-wkoEemca7hN01bE4g" value="B0HSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK9_AkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK9_QkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK9_gkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK9_wkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK-AAkoEemca7hN01bE4g" name="B0FRCD" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK-AQkoEemca7hN01bE4g" value="B0FRCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK-AgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK-AwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK-BAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK-BQkoEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK-BgkoEemca7hN01bE4g" name="B0B4ST" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK-BwkoEemca7hN01bE4g" value="B0B4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK-CAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK-CQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK-CgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK-CwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK-DAkoEemca7hN01bE4g" name="B0B7ST" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK-DQkoEemca7hN01bE4g" value="B0B7ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK-DgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK-DwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK-EAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK-EQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puK-EgkoEemca7hN01bE4g" name="B0PJST" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_puK-EwkoEemca7hN01bE4g" value="B0PJST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puK-FAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puK-FQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puK-FgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puK-FwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHAAkoEemca7hN01bE4g" name="B0PKST" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHAQkoEemca7hN01bE4g" value="B0PKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHAgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHAwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHBAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHBQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHBgkoEemca7hN01bE4g" name="B0CECD" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHBwkoEemca7hN01bE4g" value="B0CECD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHCAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHCQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHCgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHCwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHDAkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHDQkoEemca7hN01bE4g" name="B0AZCD" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHDgkoEemca7hN01bE4g" value="B0AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHDwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHEAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHEQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHEgkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHEwkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHFAkoEemca7hN01bE4g" name="B0FTNB" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHFQkoEemca7hN01bE4g" value="B0FTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHFgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHFwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHGAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHGQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHGgkoEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHGwkoEemca7hN01bE4g" name="B0AFPC" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHHAkoEemca7hN01bE4g" value="B0AFPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHHQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHHgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHHwkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHIAkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHIQkoEemca7hN01bE4g" name="B0HRNB" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHIgkoEemca7hN01bE4g" value="B0HRNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHIwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHJAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHJQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHJgkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHJwkoEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHKAkoEemca7hN01bE4g" name="B0AGPC" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHKQkoEemca7hN01bE4g" value="B0AGPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHKgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHKwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHLAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHLQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHLgkoEemca7hN01bE4g" name="B0HSNB" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHLwkoEemca7hN01bE4g" value="B0HSNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHMAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHMQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHMgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHMwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHNAkoEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHNQkoEemca7hN01bE4g" name="B0AHPC" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHNgkoEemca7hN01bE4g" value="B0AHPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHNwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHOAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHOQkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHOgkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHOwkoEemca7hN01bE4g" name="B0B1CD" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHPAkoEemca7hN01bE4g" value="B0B1CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHPQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHPgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHPwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHQAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHQQkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHQgkoEemca7hN01bE4g" name="B0F7NB" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHQwkoEemca7hN01bE4g" value="B0F7NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHRAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHRQkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHRgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHRwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHSAkoEemca7hN01bE4g" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHSQkoEemca7hN01bE4g" name="B0GLCD" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHSgkoEemca7hN01bE4g" value="B0GLCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHSwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHTAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHTQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHTgkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHTwkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHUAkoEemca7hN01bE4g" name="B0F8NB" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHUQkoEemca7hN01bE4g" value="B0F8NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHUgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHUwkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHVAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHVQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHVgkoEemca7hN01bE4g" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHVwkoEemca7hN01bE4g" name="B0BCCD" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHWAkoEemca7hN01bE4g" value="B0BCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHWQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHWgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHWwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHXAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHXQkoEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHXgkoEemca7hN01bE4g" name="B0A5DT" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHXwkoEemca7hN01bE4g" value="B0A5DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHYAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHYQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHYgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHYwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHZAkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHZQkoEemca7hN01bE4g" name="B0A6DT" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHZgkoEemca7hN01bE4g" value="B0A6DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHZwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHaAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHaQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHagkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHawkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHbAkoEemca7hN01bE4g" name="B0A7DT" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHbQkoEemca7hN01bE4g" value="B0A7DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHbgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHbwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHcAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHcQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHcgkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHcwkoEemca7hN01bE4g" name="B0DVDT" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHdAkoEemca7hN01bE4g" value="B0DVDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHdQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHdgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHdwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHeAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHeQkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHegkoEemca7hN01bE4g" name="B0B8S1" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHewkoEemca7hN01bE4g" value="B0B8S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHfAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHfQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHfgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHfwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHgAkoEemca7hN01bE4g" name="B0RPST" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHgQkoEemca7hN01bE4g" value="B0RPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHggkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHgwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHhAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHhQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHhgkoEemca7hN01bE4g" name="B0GLDT" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHhwkoEemca7hN01bE4g" value="B0GLDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHiAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHiQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHigkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHiwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHjAkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHjQkoEemca7hN01bE4g" name="B0GMDT" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHjgkoEemca7hN01bE4g" value="B0GMDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHjwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHkAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHkQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHkgkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHkwkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHlAkoEemca7hN01bE4g" name="B0I2QT" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHlQkoEemca7hN01bE4g" value="B0I2QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHlgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHlwkoEemca7hN01bE4g" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHmAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHmQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHmgkoEemca7hN01bE4g" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHmwkoEemca7hN01bE4g" name="B0I3QT" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHnAkoEemca7hN01bE4g" value="B0I3QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHnQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHngkoEemca7hN01bE4g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHnwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHoAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHoQkoEemca7hN01bE4g" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHogkoEemca7hN01bE4g" name="B0E9S1" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHowkoEemca7hN01bE4g" value="B0E9S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHpAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHpQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHpgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHpwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHqAkoEemca7hN01bE4g" name="B0FAS1" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHqQkoEemca7hN01bE4g" value="B0FAS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHqgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHqwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHrAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHrQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHrgkoEemca7hN01bE4g" name="B0C9CD" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHrwkoEemca7hN01bE4g" value="B0C9CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHsAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHsQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHsgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHswkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHtAkoEemca7hN01bE4g" name="B0FHS1" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHtQkoEemca7hN01bE4g" value="B0FHS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHtgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHtwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHuAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHuQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHugkoEemca7hN01bE4g" name="B0DRN1" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHuwkoEemca7hN01bE4g" value="B0DRN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHvAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHvQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHvgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHvwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHwAkoEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHwQkoEemca7hN01bE4g" name="B0DSN1" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHwgkoEemca7hN01bE4g" value="B0DSN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHwwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHxAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHxQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHxgkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHxwkoEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHyAkoEemca7hN01bE4g" name="B0DTN1" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUHyQkoEemca7hN01bE4g" value="B0DTN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUHygkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUHywkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUHzAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUHzQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUHzgkoEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUHzwkoEemca7hN01bE4g" name="B0K9S2" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUH0AkoEemca7hN01bE4g" value="B0K9S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUH0QkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUH0gkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUH0wkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUH1AkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUH1QkoEemca7hN01bE4g" name="B0LAS2" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUH1gkoEemca7hN01bE4g" value="B0LAS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUH1wkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUH2AkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUH2QkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUH2gkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUH2wkoEemca7hN01bE4g" name="B0SIN2" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUH3AkoEemca7hN01bE4g" value="B0SIN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUH3QkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUH3gkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUH3wkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUH4AkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUH4QkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUH4gkoEemca7hN01bE4g" name="B0SJN2" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUH4wkoEemca7hN01bE4g" value="B0SJN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUH5AkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUH5QkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUH5gkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUH5wkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUH6AkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUH6QkoEemca7hN01bE4g" name="B0F4N1" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUH6gkoEemca7hN01bE4g" value="B0F4N1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUH6wkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUH7AkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUH7QkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUH7gkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUH7wkoEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUH8AkoEemca7hN01bE4g" name="B0QGDT" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUH8QkoEemca7hN01bE4g" value="B0QGDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUH8gkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_puUH8wkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUH9AkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUH9QkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUH9gkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_puUH9wkoEemca7hN01bE4g" name="B0XES2" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_puUH-AkoEemca7hN01bE4g" value="B0XES2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_puUH-QkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_puUH-gkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_puUH-wkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_puUH_AkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pud4AAkoEemca7hN01bE4g" name="B0QHDT" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_pud4AQkoEemca7hN01bE4g" value="B0QHDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pud4AgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pud4AwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pud4BAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pud4BQkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pud4BgkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pud4BwkoEemca7hN01bE4g" name="B0QIDT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_pud4CAkoEemca7hN01bE4g" value="B0QIDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pud4CQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pud4CgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pud4CwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pud4DAkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pud4DQkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pud4DgkoEemca7hN01bE4g" name="B0RNP2" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_pud4DwkoEemca7hN01bE4g" value="B0RNP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pud4EAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pud4EQkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pud4EgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pud4EwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pud4FAkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pud4FQkoEemca7hN01bE4g" name="B0ROP2" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_pud4FgkoEemca7hN01bE4g" value="B0ROP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pud4FwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pud4GAkoEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pud4GQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pud4GgkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pud4GwkoEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pud4HAkoEemca7hN01bE4g" name="B0WWS2" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_pud4HQkoEemca7hN01bE4g" value="B0WWS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pud4HgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pud4HwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pud4IAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pud4IQkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pud4IgkoEemca7hN01bE4g" name="B0WXS2" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_pud4IwkoEemca7hN01bE4g" value="B0WXS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pud4JAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pud4JQkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pud4JgkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pud4JwkoEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pud4KAkoEemca7hN01bE4g" name="B0JHC1" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_pud4KQkoEemca7hN01bE4g" value="B0JHC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pud4KgkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pud4KwkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pud4LAkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pud4LQkoEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pud4LgkoEemca7hN01bE4g" name="B0AADT" position="100">
        <attribute defType="com.stambia.rdbms.column.name" id="_pud4LwkoEemca7hN01bE4g" value="B0AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pud4MAkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pud4MQkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pud4MgkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pud4MwkoEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pud4NAkoEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pud4NQkoEemca7hN01bE4g" name="B0AATX" position="101">
        <attribute defType="com.stambia.rdbms.column.name" id="_pud4NgkoEemca7hN01bE4g" value="B0AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pud4NwkoEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pud4OAkoEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pud4OQkoEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pud4OgkoEemca7hN01bE4g" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Fgy6AQkqEemca7hN01bE4g" name="WEB_EXPORTCMDMAGENTO">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Fgy6AgkqEemca7hN01bE4g" value="WEB_EXPORTCMDMAGENTO"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Fgy6AwkqEemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_FkqtYAkqEemca7hN01bE4g" name="WEAMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_FkqtYQkqEemca7hN01bE4g" value="WEAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FkqtYgkqEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FkqtYwkqEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FkqtZAkqEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_FkqtZQkqEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_FkqtZgkqEemca7hN01bE4g" name="WEBPCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_FkqtZwkqEemca7hN01bE4g" value="WEBPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_FkqtaAkqEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_FkqtaQkqEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_FkqtagkqEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_FkqtawkqEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_FkqtbAkqEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Fk0eYAkqEemca7hN01bE4g" name="WENUMSEQ" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Fk0eYQkqEemca7hN01bE4g" value="WENUMSEQ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Fk0eYgkqEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Fk0eYwkqEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Fk0eZAkqEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Fk0eZQkqEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Fk0eZgkqEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Fk0eZwkqEemca7hN01bE4g" name="WEDATE" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Fk0eaAkqEemca7hN01bE4g" value="WEDATE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Fk0eaQkqEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Fk0eagkqEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Fk0eawkqEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Fk0ebAkqEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Fk0ebQkqEemca7hN01bE4g" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_NhQ-QQkvEemca7hN01bE4g" name="ORB1REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_NhQ-QgkvEemca7hN01bE4g" value="ORB1REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_NhQ-QwkvEemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_NjWo8AkvEemca7hN01bE4g" name="B1BPCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjWo8QkvEemca7hN01bE4g" value="B1BPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjWo8gkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjWo8wkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjWo9AkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjWo9QkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjWo9gkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjWo9wkvEemca7hN01bE4g" name="B1EONB" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjWo-AkvEemca7hN01bE4g" value="B1EONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjWo-QkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjWo-gkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjWo-wkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjWo_AkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjWo_QkvEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjWo_gkvEemca7hN01bE4g" name="B1BQCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgZ8AkvEemca7hN01bE4g" value="B1BQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgZ8QkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgZ8gkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgZ8wkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgZ9AkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgZ9QkvEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgZ9gkvEemca7hN01bE4g" name="B1RHNB" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgZ9wkvEemca7hN01bE4g" value="B1RHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgZ-AkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgZ-QkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgZ-gkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgZ-wkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgZ_AkvEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgZ_QkvEemca7hN01bE4g" name="B1RINB" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgZ_gkvEemca7hN01bE4g" value="B1RINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgZ_wkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaAAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaAQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaAgkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaAwkvEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaBAkvEemca7hN01bE4g" name="B1DFN1" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaBQkvEemca7hN01bE4g" value="B1DFN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaBgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaBwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaCAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaCQkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaCgkvEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaCwkvEemca7hN01bE4g" name="B1BGTX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaDAkvEemca7hN01bE4g" value="B1BGTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaDQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaDgkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaDwkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaEAkvEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaEQkvEemca7hN01bE4g" name="B1XGST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaEgkvEemca7hN01bE4g" value="B1XGST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaEwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaFAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaFQkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaFgkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaFwkvEemca7hN01bE4g" name="B1JGCD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaGAkvEemca7hN01bE4g" value="B1JGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaGQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaGgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaGwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaHAkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaHQkvEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaHgkvEemca7hN01bE4g" name="B1CEST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaHwkvEemca7hN01bE4g" value="B1CEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaIAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaIQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaIgkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaIwkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaJAkvEemca7hN01bE4g" name="B1W6ST" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaJQkvEemca7hN01bE4g" value="B1W6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaJgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaJwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaKAkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaKQkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaKgkvEemca7hN01bE4g" name="B1AZCD" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaKwkvEemca7hN01bE4g" value="B1AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaLAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaLQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaLgkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaLwkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaMAkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaMQkvEemca7hN01bE4g" name="B1AIQT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaMgkvEemca7hN01bE4g" value="B1AIQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaMwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaNAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaNQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaNgkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaNwkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaOAkvEemca7hN01bE4g" name="B1EWQT" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaOQkvEemca7hN01bE4g" value="B1EWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaOgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaOwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaPAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaPQkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaPgkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaPwkvEemca7hN01bE4g" name="B1EXQT" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaQAkvEemca7hN01bE4g" value="B1EXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaQQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaQgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaQwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaRAkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaRQkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaRgkvEemca7hN01bE4g" name="B1CAQT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaRwkvEemca7hN01bE4g" value="B1CAQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaSAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaSQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaSgkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaSwkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaTAkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaTQkvEemca7hN01bE4g" name="B1AJQT" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaTgkvEemca7hN01bE4g" value="B1AJQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaTwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaUAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaUQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaUgkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaUwkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaVAkvEemca7hN01bE4g" name="B1TGQT" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaVQkvEemca7hN01bE4g" value="B1TGQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaVgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaVwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaWAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaWQkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaWgkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaWwkvEemca7hN01bE4g" name="B1I7QT" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaXAkvEemca7hN01bE4g" value="B1I7QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaXQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaXgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaXwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaYAkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaYQkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaYgkvEemca7hN01bE4g" name="B1FZS1" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaYwkvEemca7hN01bE4g" value="B1FZS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaZAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaZQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaZgkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaZwkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaaAkvEemca7hN01bE4g" name="B1AFPR" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaaQkvEemca7hN01bE4g" value="B1AFPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaagkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaawkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgabAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgabQkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgabgkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgabwkvEemca7hN01bE4g" name="B1AGPR" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgacAkvEemca7hN01bE4g" value="B1AGPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgacQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgacgkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgacwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgadAkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgadQkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgadgkvEemca7hN01bE4g" name="B1O1PR" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgadwkvEemca7hN01bE4g" value="B1O1PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaeAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaeQkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaegkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaewkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgafAkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgafQkvEemca7hN01bE4g" name="B1AHPR" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgafgkvEemca7hN01bE4g" value="B1AHPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgafwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgagAkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgagQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaggkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgagwkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgahAkvEemca7hN01bE4g" name="B1GXP1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgahQkvEemca7hN01bE4g" value="B1GXP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgahgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgahwkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaiAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaiQkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaigkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaiwkvEemca7hN01bE4g" name="B1GYP2" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgajAkvEemca7hN01bE4g" value="B1GYP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgajQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgajgkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgajwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgakAkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgakQkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgakgkvEemca7hN01bE4g" name="B1GZP2" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgakwkvEemca7hN01bE4g" value="B1GZP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgalAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgalQkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgalgkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgalwkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgamAkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgamQkvEemca7hN01bE4g" name="B1HTPR" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgamgkvEemca7hN01bE4g" value="B1HTPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgamwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjganAkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjganQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgangkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjganwkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgaoAkvEemca7hN01bE4g" name="B1LAP2" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaoQkvEemca7hN01bE4g" value="B1LAP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaogkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaowkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgapAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgapQkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgapgkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgapwkvEemca7hN01bE4g" name="B1GYP1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgaqAkvEemca7hN01bE4g" value="B1GYP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaqQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgaqgkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaqwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgarAkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgarQkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgargkvEemca7hN01bE4g" name="B1GZP1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgarwkvEemca7hN01bE4g" value="B1GZP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgasAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgasQkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgasgkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaswkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgatAkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgatQkvEemca7hN01bE4g" name="B1AKPC" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgatgkvEemca7hN01bE4g" value="B1AKPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgatwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjgauAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgauQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaugkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgauwkvEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgavAkvEemca7hN01bE4g" name="B1CHST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgavQkvEemca7hN01bE4g" value="B1CHST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgavgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgavwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgawAkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgawQkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgawgkvEemca7hN01bE4g" name="B1XLST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgawwkvEemca7hN01bE4g" value="B1XLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaxAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaxQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgaxgkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgaxwkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgayAkvEemca7hN01bE4g" name="B1HGCD" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgayQkvEemca7hN01bE4g" value="B1HGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjgaygkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjgaywkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjgazAkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjgazQkvEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjgazgkvEemca7hN01bE4g" name="B1ERNB" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjgazwkvEemca7hN01bE4g" value="B1ERNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njga0AkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njga0QkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Njga0gkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Njga0wkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Njga1AkvEemca7hN01bE4g" name="B1BFCD" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_Njga1QkvEemca7hN01bE4g" value="B1BFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njga1gkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njga1wkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Njga2AkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Njga2QkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Njga2gkvEemca7hN01bE4g" name="B1C0CD" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_Njga2wkvEemca7hN01bE4g" value="B1C0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njga3AkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Njga3QkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njga3gkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Njga3wkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Njga4AkvEemca7hN01bE4g" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Njga4QkvEemca7hN01bE4g" name="B1BOCD" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_Njga4gkvEemca7hN01bE4g" value="B1BOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njga4wkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njga5AkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Njga5QkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Njga5gkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Njga5wkvEemca7hN01bE4g" name="B1HRST" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_Njga6AkvEemca7hN01bE4g" value="B1HRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njga6QkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njga6gkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Njga6wkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Njga7AkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Njpj4AkvEemca7hN01bE4g" name="B1HLCD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_Njpj4QkvEemca7hN01bE4g" value="B1HLCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njpj4gkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Njpj4wkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njpj5AkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Njpj5QkvEemca7hN01bE4g" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Njpj5gkvEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Njpj5wkvEemca7hN01bE4g" name="B1HMCD" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_Njpj6AkvEemca7hN01bE4g" value="B1HMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njpj6QkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Njpj6gkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njpj6wkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Njpj7AkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Njpj7QkvEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Njpj7gkvEemca7hN01bE4g" name="B1I3NB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_Njpj7wkvEemca7hN01bE4g" value="B1I3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njpj8AkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Njpj8QkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njpj8gkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Njpj8wkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Njpj9AkvEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Njpj9QkvEemca7hN01bE4g" name="B1JGC1" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_Njpj9gkvEemca7hN01bE4g" value="B1JGC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njpj9wkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njpj-AkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Njpj-QkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Njpj-gkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Njpj-wkvEemca7hN01bE4g" name="B1LUPC" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_Njpj_AkvEemca7hN01bE4g" value="B1LUPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Njpj_QkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Njpj_gkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Njpj_wkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkAAkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkAQkvEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkAgkvEemca7hN01bE4g" name="B1FRST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkAwkvEemca7hN01bE4g" value="B1FRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkBAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkBQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkBgkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkBwkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkCAkvEemca7hN01bE4g" name="B1JRST" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkCQkvEemca7hN01bE4g" value="B1JRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkCgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkCwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkDAkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkDQkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkDgkvEemca7hN01bE4g" name="B1JSST" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkDwkvEemca7hN01bE4g" value="B1JSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkEAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkEQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkEgkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkEwkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkFAkvEemca7hN01bE4g" name="B1PLST" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkFQkvEemca7hN01bE4g" value="B1PLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkFgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkFwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkGAkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkGQkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkGgkvEemca7hN01bE4g" name="B1LBS2" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkGwkvEemca7hN01bE4g" value="B1LBS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkHAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkHQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkHgkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkHwkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkIAkvEemca7hN01bE4g" name="B1B9S1" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkIQkvEemca7hN01bE4g" value="B1B9S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkIgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkIwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkJAkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkJQkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkJgkvEemca7hN01bE4g" name="B1RQST" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkJwkvEemca7hN01bE4g" value="B1RQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkKAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkKQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkKgkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkKwkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkLAkvEemca7hN01bE4g" name="B1QMDT" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkLQkvEemca7hN01bE4g" value="B1QMDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkLgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjpkLwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkMAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkMQkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkMgkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkMwkvEemca7hN01bE4g" name="B1QNDT" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkNAkvEemca7hN01bE4g" value="B1QNDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkNQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjpkNgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkNwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkOAkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkOQkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkOgkvEemca7hN01bE4g" name="B1RRP2" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkOwkvEemca7hN01bE4g" value="B1RRP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkPAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjpkPQkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkPgkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkPwkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkQAkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkQQkvEemca7hN01bE4g" name="B1RSP2" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkQgkvEemca7hN01bE4g" value="B1RSP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkQwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjpkRAkvEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkRQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkRgkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkRwkvEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkSAkvEemca7hN01bE4g" name="B1W0S2" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkSQkvEemca7hN01bE4g" value="B1W0S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkSgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkSwkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkTAkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkTQkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkTgkvEemca7hN01bE4g" name="B1W1S2" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkTwkvEemca7hN01bE4g" value="B1W1S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkUAkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkUQkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkUgkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkUwkvEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkVAkvEemca7hN01bE4g" name="B1AADT" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkVQkvEemca7hN01bE4g" value="B1AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkVgkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_NjpkVwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkWAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkWQkvEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkWgkvEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkWwkvEemca7hN01bE4g" name="B1AATX" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkXAkvEemca7hN01bE4g" value="B1AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkXQkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkXgkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkXwkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkYAkvEemca7hN01bE4g" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_NjpkYQkvEemca7hN01bE4g" name="B1OWST" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_NjpkYgkvEemca7hN01bE4g" value="B1OWST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_NjpkYwkvEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_NjpkZAkvEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_NjpkZQkvEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_NjpkZgkvEemca7hN01bE4g" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_lQfUwQnAEemca7hN01bE4g" name="ORI1REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_lQfUwgnAEemca7hN01bE4g" value="ORI1REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_lQfUwwnAEemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_lRrnkAnAEemca7hN01bE4g" name="I1B3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnkQnAEemca7hN01bE4g" value="I1B3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnkgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRrnkwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrnlAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrnlQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrnlgnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrnlwnAEemca7hN01bE4g" name="I1AZCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnmAnAEemca7hN01bE4g" value="I1AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnmQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRrnmgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrnmwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrnnAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrnnQnAEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrnngnAEemca7hN01bE4g" name="I1GHCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnnwnAEemca7hN01bE4g" value="I1GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnoAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrnoQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrnognAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrnownAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrnpAnAEemca7hN01bE4g" name="I1J2CD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnpQnAEemca7hN01bE4g" value="I1J2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnpgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrnpwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrnqAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrnqQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrnqgnAEemca7hN01bE4g" name="I1J3CD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnqwnAEemca7hN01bE4g" value="I1J3CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnrAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrnrQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrnrgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrnrwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrnsAnAEemca7hN01bE4g" name="I1HMCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnsQnAEemca7hN01bE4g" value="I1HMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnsgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRrnswnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrntAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrntQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrntgnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrntwnAEemca7hN01bE4g" name="I1JCCD" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnuAnAEemca7hN01bE4g" value="I1JCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnuQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrnugnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrnuwnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrnvAnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrnvQnAEemca7hN01bE4g" name="I1CPCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnvgnAEemca7hN01bE4g" value="I1CPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnvwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrnwAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrnwQnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrnwgnAEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrnwwnAEemca7hN01bE4g" name="I1ISTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnxAnAEemca7hN01bE4g" value="I1ISTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnxQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrnxgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrnxwnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrnyAnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrnyQnAEemca7hN01bE4g" name="I1JVCD" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrnygnAEemca7hN01bE4g" value="I1JVCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrnywnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrnzAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrnzQnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrnzgnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrnzwnAEemca7hN01bE4g" name="I1JWCD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrn0AnAEemca7hN01bE4g" value="I1JWCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrn0QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrn0gnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrn0wnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrn1AnAEemca7hN01bE4g" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrn1QnAEemca7hN01bE4g" name="I1MINB" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrn1gnAEemca7hN01bE4g" value="I1MINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrn1wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRrn2AnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrn2QnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrn2gnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrn2wnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrn3AnAEemca7hN01bE4g" name="I1MJNB" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrn3QnAEemca7hN01bE4g" value="I1MJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrn3gnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRrn3wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrn4AnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrn4QnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrn4gnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrn4wnAEemca7hN01bE4g" name="I1MKNB" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrn5AnAEemca7hN01bE4g" value="I1MKNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrn5QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRrn5gnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrn5wnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrn6AnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrn6QnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrn6gnAEemca7hN01bE4g" name="I1MLNB" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrn6wnAEemca7hN01bE4g" value="I1MLNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrn7AnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRrn7QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrn7gnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrn7wnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrn8AnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrn8QnAEemca7hN01bE4g" name="I1MMNB" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrn8gnAEemca7hN01bE4g" value="I1MMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrn8wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRrn9AnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrn9QnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrn9gnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrn9wnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrn-AnAEemca7hN01bE4g" name="I1MNNB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrn-QnAEemca7hN01bE4g" value="I1MNNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrn-gnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRrn-wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrn_AnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrn_QnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrn_gnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrn_wnAEemca7hN01bE4g" name="I1NMNB" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroAAnAEemca7hN01bE4g" value="I1NMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroAQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRroAgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroAwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroBAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroBQnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroBgnAEemca7hN01bE4g" name="I1KYNB" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroBwnAEemca7hN01bE4g" value="I1KYNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroCAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRroCQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroCgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroCwnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroDAnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroDQnAEemca7hN01bE4g" name="I1CNN3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroDgnAEemca7hN01bE4g" value="I1CNN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroDwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroEAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroEQnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroEgnAEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroEwnAEemca7hN01bE4g" name="I1CON3" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroFAnAEemca7hN01bE4g" value="I1CON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroFQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroFgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroFwnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroGAnAEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroGQnAEemca7hN01bE4g" name="I1I4TX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroGgnAEemca7hN01bE4g" value="I1I4TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroGwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroHAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroHQnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroHgnAEemca7hN01bE4g" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroHwnAEemca7hN01bE4g" name="I1QQST" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroIAnAEemca7hN01bE4g" value="I1QQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroIQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroIgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroIwnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroJAnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroJQnAEemca7hN01bE4g" name="I1QPST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroJgnAEemca7hN01bE4g" value="I1QPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroJwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroKAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroKQnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroKgnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroKwnAEemca7hN01bE4g" name="I1CXS1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroLAnAEemca7hN01bE4g" value="I1CXS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroLQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroLgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroLwnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroMAnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroMQnAEemca7hN01bE4g" name="I1KZNB" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroMgnAEemca7hN01bE4g" value="I1KZNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroMwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroNAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroNQnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroNgnAEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroNwnAEemca7hN01bE4g" name="I1NCNB" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroOAnAEemca7hN01bE4g" value="I1NCNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroOQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRroOgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroOwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroPAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroPQnAEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroPgnAEemca7hN01bE4g" name="I1K0NB" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroPwnAEemca7hN01bE4g" value="I1K0NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroQAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRroQQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroQgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroQwnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroRAnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroRQnAEemca7hN01bE4g" name="I1HPN1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroRgnAEemca7hN01bE4g" value="I1HPN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroRwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lRroSAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroSQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroSgnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroSwnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroTAnAEemca7hN01bE4g" name="I1QRST" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroTQnAEemca7hN01bE4g" value="I1QRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroTgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroTwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroUAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroUQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroUgnAEemca7hN01bE4g" name="I1CYS1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroUwnAEemca7hN01bE4g" value="I1CYS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroVAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroVQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroVgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroVwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroWAnAEemca7hN01bE4g" name="I1QSST" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroWQnAEemca7hN01bE4g" value="I1QSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroWgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroWwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroXAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroXQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroXgnAEemca7hN01bE4g" name="I1QTST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroXwnAEemca7hN01bE4g" value="I1QTST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroYAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroYQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroYgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroYwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroZAnAEemca7hN01bE4g" name="I1QVST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroZQnAEemca7hN01bE4g" value="I1QVST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroZgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroZwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroaAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroaQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRroagnAEemca7hN01bE4g" name="I1QUST" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRroawnAEemca7hN01bE4g" value="I1QUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrobAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrobQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrobgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrobwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrocAnAEemca7hN01bE4g" name="I1FKS2" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrocQnAEemca7hN01bE4g" value="I1FKS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrocgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrocwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrodAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrodQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrodgnAEemca7hN01bE4g" name="I1FLS2" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrodwnAEemca7hN01bE4g" value="I1FLS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRroeAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRroeQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRroegnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRroewnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lRrofAnAEemca7hN01bE4g" name="I1FMS2" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_lRrofQnAEemca7hN01bE4g" value="I1FMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lRrofgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lRrofwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lRrogAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lRrogQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YkAnAEemca7hN01bE4g" name="I1QXST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YkQnAEemca7hN01bE4g" value="I1QXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YkgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YkwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YlAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YlQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YlgnAEemca7hN01bE4g" name="I1QYST" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YlwnAEemca7hN01bE4g" value="I1QYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YmAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YmQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YmgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YmwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YnAnAEemca7hN01bE4g" name="I1QZST" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YnQnAEemca7hN01bE4g" value="I1QZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YngnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YnwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YoAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YoQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YognAEemca7hN01bE4g" name="I1Q0ST" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YownAEemca7hN01bE4g" value="I1Q0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YpAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YpQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YpgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YpwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YqAnAEemca7hN01bE4g" name="I1K1NB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YqQnAEemca7hN01bE4g" value="I1K1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YqgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1YqwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YrAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YrQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YrgnAEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YrwnAEemca7hN01bE4g" name="I1K2NB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YsAnAEemca7hN01bE4g" value="I1K2NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YsQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YsgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YswnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YtAnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YtQnAEemca7hN01bE4g" name="I1K3NB" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YtgnAEemca7hN01bE4g" value="I1K3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YtwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YuAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YuQnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YugnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YuwnAEemca7hN01bE4g" name="I1S6ST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YvAnAEemca7hN01bE4g" value="I1S6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YvQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YvgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YvwnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YwAnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YwQnAEemca7hN01bE4g" name="I1XEST" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YwgnAEemca7hN01bE4g" value="I1XEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YwwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YxAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YxQnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YxgnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YxwnAEemca7hN01bE4g" name="I1DHQT" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YyAnAEemca7hN01bE4g" value="I1DHQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1YyQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1YygnAEemca7hN01bE4g" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1YywnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1YzAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1YzQnAEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1YzgnAEemca7hN01bE4g" name="I1AFNB" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1YzwnAEemca7hN01bE4g" value="I1AFNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1Y0AnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1Y0QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1Y0gnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1Y0wnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1Y1AnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1Y1QnAEemca7hN01bE4g" name="I1L9QT" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1Y1gnAEemca7hN01bE4g" value="I1L9QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1Y1wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1Y2AnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1Y2QnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1Y2gnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1Y2wnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1Y3AnAEemca7hN01bE4g" name="I1L9N1" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1Y3QnAEemca7hN01bE4g" value="I1L9N1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1Y3gnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1Y3wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1Y4AnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1Y4QnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1Y4gnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1Y4wnAEemca7hN01bE4g" name="I1DAT1" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1Y5AnAEemca7hN01bE4g" value="I1DAT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1Y5QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1Y5gnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1Y5wnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1Y6AnAEemca7hN01bE4g" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1Y6QnAEemca7hN01bE4g" name="I1CZS1" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1Y6gnAEemca7hN01bE4g" value="I1CZS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1Y6wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1Y7AnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1Y7QnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1Y7gnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1Y7wnAEemca7hN01bE4g" name="I1C9T1" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1Y8AnAEemca7hN01bE4g" value="I1C9T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1Y8QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1Y8gnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1Y8wnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1Y9AnAEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1Y9QnAEemca7hN01bE4g" name="I1TPTX" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1Y9gnAEemca7hN01bE4g" value="I1TPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1Y9wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1Y-AnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1Y-QnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1Y-gnAEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1Y-wnAEemca7hN01bE4g" name="I1TQTX" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1Y_AnAEemca7hN01bE4g" value="I1TQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1Y_QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1Y_gnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1Y_wnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZAAnAEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZAQnAEemca7hN01bE4g" name="I1GVPR" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZAgnAEemca7hN01bE4g" value="I1GVPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZAwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZBAnAEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZBQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZBgnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZBwnAEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZCAnAEemca7hN01bE4g" name="I1CJPC" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZCQnAEemca7hN01bE4g" value="I1CJPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZCgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZCwnAEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZDAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZDQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZDgnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZDwnAEemca7hN01bE4g" name="I1C0S1" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZEAnAEemca7hN01bE4g" value="I1C0S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZEQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZEgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZEwnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZFAnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZFQnAEemca7hN01bE4g" name="I1DYQT" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZFgnAEemca7hN01bE4g" value="I1DYQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZFwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZGAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZGQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZGgnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZGwnAEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZHAnAEemca7hN01bE4g" name="I1DXQT" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZHQnAEemca7hN01bE4g" value="I1DXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZHgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZHwnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZIAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZIQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZIgnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZIwnAEemca7hN01bE4g" name="I1GQQT" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZJAnAEemca7hN01bE4g" value="I1GQQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZJQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZJgnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZJwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZKAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZKQnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZKgnAEemca7hN01bE4g" name="I1GRQT" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZKwnAEemca7hN01bE4g" value="I1GRQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZLAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZLQnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZLgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZLwnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZMAnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZMQnAEemca7hN01bE4g" name="I1GSQT" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZMgnAEemca7hN01bE4g" value="I1GSQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZMwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZNAnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZNQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZNgnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZNwnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZOAnAEemca7hN01bE4g" name="I1GTQT" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZOQnAEemca7hN01bE4g" value="I1GTQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZOgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZOwnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZPAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZPQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZPgnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZPwnAEemca7hN01bE4g" name="I1GUQT" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZQAnAEemca7hN01bE4g" value="I1GUQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZQQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZQgnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZQwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZRAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZRQnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZRgnAEemca7hN01bE4g" name="I1GVQT" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZRwnAEemca7hN01bE4g" value="I1GVQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZSAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZSQnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZSgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZSwnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZTAnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZTQnAEemca7hN01bE4g" name="I1GWQT" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZTgnAEemca7hN01bE4g" value="I1GWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZTwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZUAnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZUQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZUgnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZUwnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZVAnAEemca7hN01bE4g" name="I1GXQT" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZVQnAEemca7hN01bE4g" value="I1GXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZVgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZVwnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZWAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZWQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZWgnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZWwnAEemca7hN01bE4g" name="I1GYQT" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZXAnAEemca7hN01bE4g" value="I1GYQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZXQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZXgnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZXwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZYAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZYQnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZYgnAEemca7hN01bE4g" name="I1GZQT" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZYwnAEemca7hN01bE4g" value="I1GZQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZZAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZZQnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZZgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZZwnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZaAnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZaQnAEemca7hN01bE4g" name="I1G0QT" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZagnAEemca7hN01bE4g" value="I1G0QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZawnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZbAnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZbQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZbgnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZbwnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZcAnAEemca7hN01bE4g" name="I1G1QT" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZcQnAEemca7hN01bE4g" value="I1G1QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZcgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZcwnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZdAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZdQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZdgnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZdwnAEemca7hN01bE4g" name="I1DWQT" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZeAnAEemca7hN01bE4g" value="I1DWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZeQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZegnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZewnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZfAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZfQnAEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZfgnAEemca7hN01bE4g" name="I1HUDT" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZfwnAEemca7hN01bE4g" value="I1HUDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZgAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZgQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZggnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZgwnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZhAnAEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR1ZhQnAEemca7hN01bE4g" name="I1YVNB" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR1ZhgnAEemca7hN01bE4g" value="I1YVNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR1ZhwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR1ZiAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR1ZiQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR1ZignAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR1ZiwnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-igAnAEemca7hN01bE4g" name="I1YWNB" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-igQnAEemca7hN01bE4g" value="I1YWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-iggnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-igwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-ihAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-ihQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-ihgnAEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-ihwnAEemca7hN01bE4g" name="I1GOQT" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-iiAnAEemca7hN01bE4g" value="I1GOQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-iiQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-iignAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-iiwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-ijAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-ijQnAEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-ijgnAEemca7hN01bE4g" name="I1FNS2" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-ijwnAEemca7hN01bE4g" value="I1FNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-ikAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-ikQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-ikgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-ikwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-ilAnAEemca7hN01bE4g" name="I1FOS2" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-ilQnAEemca7hN01bE4g" value="I1FOS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-ilgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-ilwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-imAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-imQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-imgnAEemca7hN01bE4g" name="I1FPS2" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-imwnAEemca7hN01bE4g" value="I1FPS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-inAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-inQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-ingnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-inwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-ioAnAEemca7hN01bE4g" name="I1FQS2" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-ioQnAEemca7hN01bE4g" value="I1FQS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-iognAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-iownAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-ipAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-ipQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-ipgnAEemca7hN01bE4g" name="I1FRS2" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-ipwnAEemca7hN01bE4g" value="I1FRS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-iqAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-iqQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-iqgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-iqwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-irAnAEemca7hN01bE4g" name="I1FSS2" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-irQnAEemca7hN01bE4g" value="I1FSS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-irgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-irwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-isAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-isQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-isgnAEemca7hN01bE4g" name="I1FTS2" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-iswnAEemca7hN01bE4g" value="I1FTS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-itAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-itQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-itgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-itwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-iuAnAEemca7hN01bE4g" name="I1FUS2" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-iuQnAEemca7hN01bE4g" value="I1FUS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-iugnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-iuwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-ivAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-ivQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-ivgnAEemca7hN01bE4g" name="I1FVS2" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-ivwnAEemca7hN01bE4g" value="I1FVS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-iwAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-iwQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-iwgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-iwwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-ixAnAEemca7hN01bE4g" name="I1FWS2" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-ixQnAEemca7hN01bE4g" value="I1FWS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-ixgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-ixwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-iyAnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-iyQnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-iygnAEemca7hN01bE4g" name="I1FXS2" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-iywnAEemca7hN01bE4g" value="I1FXS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-izAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-izQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-izgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-izwnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-i0AnAEemca7hN01bE4g" name="I1FYS2" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-i0QnAEemca7hN01bE4g" value="I1FYS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-i0gnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-i0wnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-i1AnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-i1QnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-i1gnAEemca7hN01bE4g" name="I1YSNB" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-i1wnAEemca7hN01bE4g" value="I1YSNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-i2AnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-i2QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-i2gnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-i2wnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-i3AnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-i3QnAEemca7hN01bE4g" name="I1YTNB" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-i3gnAEemca7hN01bE4g" value="I1YTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-i3wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-i4AnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-i4QnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-i4gnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-i4wnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-i5AnAEemca7hN01bE4g" name="I1YUNB" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-i5QnAEemca7hN01bE4g" value="I1YUNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-i5gnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-i5wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-i6AnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-i6QnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-i6gnAEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-i6wnAEemca7hN01bE4g" name="I1GLQT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-i7AnAEemca7hN01bE4g" value="I1GLQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-i7QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-i7gnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-i7wnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-i8AnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-i8QnAEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-i8gnAEemca7hN01bE4g" name="I1GMQT" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-i8wnAEemca7hN01bE4g" value="I1GMQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-i9AnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-i9QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-i9gnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-i9wnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-i-AnAEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-i-QnAEemca7hN01bE4g" name="I1GNQT" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-i-gnAEemca7hN01bE4g" value="I1GNQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-i-wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-i_AnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-i_QnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-i_gnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-i_wnAEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-jAAnAEemca7hN01bE4g" name="I1JLDT" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-jAQnAEemca7hN01bE4g" value="I1JLDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-jAgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-jAwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-jBAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-jBQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-jBgnAEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-jBwnAEemca7hN01bE4g" name="I1AADT" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-jCAnAEemca7hN01bE4g" value="I1AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-jCQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lR-jCgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-jCwnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-jDAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-jDQnAEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lR-jDgnAEemca7hN01bE4g" name="I1AATX" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_lR-jDwnAEemca7hN01bE4g" value="I1AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lR-jEAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lR-jEQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lR-jEgnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lR-jEwnAEemca7hN01bE4g" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_lSuJYQnAEemca7hN01bE4g" name="OTKYCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_lSuJYgnAEemca7hN01bE4g" value="OTKYCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_lSuJYwnAEemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_lS36YAnAEemca7hN01bE4g" name="KYB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_lS36YQnAEemca7hN01bE4g" value="KYB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lS36YgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lS36YwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lS36ZAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lS36ZQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lS36ZgnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lS36ZwnAEemca7hN01bE4g" name="KYYHC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_lS36aAnAEemca7hN01bE4g" value="KYYHC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lS36aQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lS36agnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lS36awnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lS36bAnAEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lS36bQnAEemca7hN01bE4g" name="KYRKS3" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_lS36bgnAEemca7hN01bE4g" value="KYRKS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lS36bwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lS36cAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lS36cQnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lS36cgnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lS36cwnAEemca7hN01bE4g" name="KYRLS3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_lS36dAnAEemca7hN01bE4g" value="KYRLS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lS36dQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lS36dgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lS36dwnAEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lS36eAnAEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lS36eQnAEemca7hN01bE4g" name="KYW9N6" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_lS36egnAEemca7hN01bE4g" value="KYW9N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lS36ewnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lS36fAnAEemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lS36fQnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lS36fgnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lS36fwnAEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lS36gAnAEemca7hN01bE4g" name="KYDGP4" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_lS36gQnAEemca7hN01bE4g" value="KYDGP4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lS36ggnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lS36gwnAEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lS36hAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lS36hQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lS36hgnAEemca7hN01bE4g" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_lGBKIAnAEemca7hN01bE4g" name="OTCACPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_lGBKIQnAEemca7hN01bE4g" value="OTCACPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_lGBKIgnAEemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_lNUE8AnAEemca7hN01bE4g" name="CAB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_lNUE8QnAEemca7hN01bE4g" value="CAB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lNUE8gnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lNUE8wnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lNUE9AnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lNUE9QnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lNUE9gnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lNUE9wnAEemca7hN01bE4g" name="CAKWN6" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_lNUE-AnAEemca7hN01bE4g" value="CAKWN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lNUE-QnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lNUE-gnAEemca7hN01bE4g" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lNUE-wnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lNUE_AnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lNUE_QnAEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lNUE_gnAEemca7hN01bE4g" name="CAM1N6" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_lNUE_wnAEemca7hN01bE4g" value="CAM1N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lNUFAAnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lNUFAQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lNUFAgnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lNUFAwnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lNUFBAnAEemca7hN01bE4g" value="5"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_lSRdcQnAEemca7hN01bE4g" name="OTBLCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_lSRdcgnAEemca7hN01bE4g" value="OTBLCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_lSRdcwnAEemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_lSk_cAnAEemca7hN01bE4g" name="BLB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_lSk_cQnAEemca7hN01bE4g" value="BLB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lSk_cgnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lSk_cwnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lSk_dAnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lSk_dQnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lSk_dgnAEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_lSk_dwnAEemca7hN01bE4g" name="BLLQP3" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_lSk_eAnAEemca7hN01bE4g" value="BLLQP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_lSk_eQnAEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_lSk_egnAEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_lSk_ewnAEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_lSk_fAnAEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_lSk_fQnAEemca7hN01bE4g" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_SyxWEAndEemca7hN01bE4g" name="ORAZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_SyxWEQndEemca7hN01bE4g" value="ORAZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_SyxWEgndEemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_S7X4cAndEemca7hN01bE4g" name="AZAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4cQndEemca7hN01bE4g" value="AZAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4cgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7X4cwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4dAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4dQndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4dgndEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4dwndEemca7hN01bE4g" name="AZAMCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4eAndEemca7hN01bE4g" value="AZAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4eQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4egndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4ewndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4fAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4fQndEemca7hN01bE4g" name="AZC0CD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4fgndEemca7hN01bE4g" value="AZC0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4fwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7X4gAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4gQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4ggndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4gwndEemca7hN01bE4g" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4hAndEemca7hN01bE4g" name="AZF6PR" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4hQndEemca7hN01bE4g" value="AZF6PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4hgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7X4hwndEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4iAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4iQndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4igndEemca7hN01bE4g" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4iwndEemca7hN01bE4g" name="AZTEST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4jAndEemca7hN01bE4g" value="AZTEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4jQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4jgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4jwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4kAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4kQndEemca7hN01bE4g" name="AZTKST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4kgndEemca7hN01bE4g" value="AZTKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4kwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4lAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4lQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4lgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4lwndEemca7hN01bE4g" name="AZTLST" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4mAndEemca7hN01bE4g" value="AZTLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4mQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4mgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4mwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4nAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4nQndEemca7hN01bE4g" name="AZNDCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4ngndEemca7hN01bE4g" value="AZNDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4nwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4oAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4oQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4ogndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4owndEemca7hN01bE4g" name="AZNECD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4pAndEemca7hN01bE4g" value="AZNECD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4pQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4pgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4pwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4qAndEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4qQndEemca7hN01bE4g" name="AZAQST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4qgndEemca7hN01bE4g" value="AZAQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4qwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4rAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4rQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4rgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4rwndEemca7hN01bE4g" name="AZARST" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4sAndEemca7hN01bE4g" value="AZARST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4sQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4sgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4swndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4tAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4tQndEemca7hN01bE4g" name="AZASST" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4tgndEemca7hN01bE4g" value="AZASST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4twndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4uAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4uQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4ugndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4uwndEemca7hN01bE4g" name="AZBHS2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4vAndEemca7hN01bE4g" value="AZBHS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4vQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4vgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4vwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4wAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4wQndEemca7hN01bE4g" name="AZAWNA" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4wgndEemca7hN01bE4g" value="AZAWNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4wwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4xAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4xQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4xgndEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4xwndEemca7hN01bE4g" name="AZAUTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4yAndEemca7hN01bE4g" value="AZAUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4yQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4ygndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4ywndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4zAndEemca7hN01bE4g" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4zQndEemca7hN01bE4g" name="AZA0CD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4zgndEemca7hN01bE4g" value="AZA0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4zwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7X40AndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X40QndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X40gndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X40wndEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X41AndEemca7hN01bE4g" name="AZATTX" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X41QndEemca7hN01bE4g" value="AZATTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X41gndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X41wndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X42AndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X42QndEemca7hN01bE4g" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X42gndEemca7hN01bE4g" name="AZOEST" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X42wndEemca7hN01bE4g" value="AZOEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X43AndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X43QndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X43gndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X43wndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X44AndEemca7hN01bE4g" name="AZA8ST" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X44QndEemca7hN01bE4g" value="AZA8ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X44gndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X44wndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X45AndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X45QndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X45gndEemca7hN01bE4g" name="AZAOST" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X45wndEemca7hN01bE4g" value="AZAOST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X46AndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X46QndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X46gndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X46wndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X47AndEemca7hN01bE4g" name="AZFNTX" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X47QndEemca7hN01bE4g" value="AZFNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X47gndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X47wndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X48AndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X48QndEemca7hN01bE4g" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X48gndEemca7hN01bE4g" name="AZZLTX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X48wndEemca7hN01bE4g" value="AZZLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X49AndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7X49QndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X49gndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X49wndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4-AndEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X4-QndEemca7hN01bE4g" name="AZFMTX" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X4-gndEemca7hN01bE4g" value="AZFMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X4-wndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7X4_AndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X4_QndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X4_gndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X4_wndEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5AAndEemca7hN01bE4g" name="AZIWST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5AQndEemca7hN01bE4g" value="AZIWST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5AgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5AwndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5BAndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5BQndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5BgndEemca7hN01bE4g" name="AZIXST" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5BwndEemca7hN01bE4g" value="AZIXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5CAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5CQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5CgndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5CwndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5DAndEemca7hN01bE4g" name="AZIYST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5DQndEemca7hN01bE4g" value="AZIYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5DgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5DwndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5EAndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5EQndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5EgndEemca7hN01bE4g" name="AZIZST" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5EwndEemca7hN01bE4g" value="AZIZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5FAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5FQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5FgndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5FwndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5GAndEemca7hN01bE4g" name="AZI0ST" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5GQndEemca7hN01bE4g" value="AZI0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5GgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5GwndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5HAndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5HQndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5HgndEemca7hN01bE4g" name="AZI2ST" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5HwndEemca7hN01bE4g" value="AZI2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5IAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5IQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5IgndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5IwndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5JAndEemca7hN01bE4g" name="AZHKCD" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5JQndEemca7hN01bE4g" value="AZHKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5JgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5JwndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5KAndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5KQndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5KgndEemca7hN01bE4g" name="AZCKTX" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5KwndEemca7hN01bE4g" value="AZCKTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5LAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7X5LQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5LgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5LwndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5MAndEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5MQndEemca7hN01bE4g" name="AZAACD" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5MgndEemca7hN01bE4g" value="AZAACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5MwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5NAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5NQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5NgndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5NwndEemca7hN01bE4g" name="AZSEST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5OAndEemca7hN01bE4g" value="AZSEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5OQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5OgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5OwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5PAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5PQndEemca7hN01bE4g" name="AZSFST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5PgndEemca7hN01bE4g" value="AZSFST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5PwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5QAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5QQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5QgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5QwndEemca7hN01bE4g" name="AZAFCD" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5RAndEemca7hN01bE4g" value="AZAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5RQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5RgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5RwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5SAndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7X5SQndEemca7hN01bE4g" name="AZABCD" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7X5SgndEemca7hN01bE4g" value="AZABCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7X5SwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7X5TAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7X5TQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7X5TgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCYAndEemca7hN01bE4g" name="AZACCD" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCYQndEemca7hN01bE4g" value="AZACCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCYgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCYwndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCZAndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCZQndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCZgndEemca7hN01bE4g" name="AZAHCD" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCZwndEemca7hN01bE4g" value="AZAHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCaAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCaQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCagndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCawndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCbAndEemca7hN01bE4g" name="AZE3ST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCbQndEemca7hN01bE4g" value="AZE3ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCbgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCbwndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCcAndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCcQndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCcgndEemca7hN01bE4g" name="AZCGCD" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCcwndEemca7hN01bE4g" value="AZCGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCdAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCdQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCdgndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCdwndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCeAndEemca7hN01bE4g" name="AZJ4CD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCeQndEemca7hN01bE4g" value="AZJ4CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCegndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCewndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCfAndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCfQndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCfgndEemca7hN01bE4g" name="AZBCCD" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCfwndEemca7hN01bE4g" value="AZBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCgAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7hCgQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCggndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCgwndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hChAndEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hChQndEemca7hN01bE4g" name="AZF3CD" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hChgndEemca7hN01bE4g" value="AZF3CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hChwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCiAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCiQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCigndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCiwndEemca7hN01bE4g" name="AZADNB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCjAndEemca7hN01bE4g" value="AZADNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCjQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7hCjgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCjwndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCkAndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCkQndEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCkgndEemca7hN01bE4g" name="AZAENB" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCkwndEemca7hN01bE4g" value="AZAENB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hClAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7hClQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hClgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hClwndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCmAndEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCmQndEemca7hN01bE4g" name="AZFTNB" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCmgndEemca7hN01bE4g" value="AZFTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCmwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7hCnAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCnQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCngndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCnwndEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCoAndEemca7hN01bE4g" name="AZBOCD" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCoQndEemca7hN01bE4g" value="AZBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCogndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCowndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCpAndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCpQndEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCpgndEemca7hN01bE4g" name="AZHRST" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCpwndEemca7hN01bE4g" value="AZHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCqAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCqQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCqgndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCqwndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCrAndEemca7hN01bE4g" name="AZA4ST" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCrQndEemca7hN01bE4g" value="AZA4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCrgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCrwndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCsAndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCsQndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCsgndEemca7hN01bE4g" name="AZFWNB" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCswndEemca7hN01bE4g" value="AZFWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCtAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7hCtQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCtgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCtwndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCuAndEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCuQndEemca7hN01bE4g" name="AZA0ST" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCugndEemca7hN01bE4g" value="AZA0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCuwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCvAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCvQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCvgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCvwndEemca7hN01bE4g" name="AZA3ST" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCwAndEemca7hN01bE4g" value="AZA3ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCwQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCwgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCwwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCxAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCxQndEemca7hN01bE4g" name="AZBDCD" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCxgndEemca7hN01bE4g" value="AZBDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCxwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCyAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCyQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hCygndEemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hCywndEemca7hN01bE4g" name="AZA1ST" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hCzAndEemca7hN01bE4g" value="AZA1ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hCzQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hCzgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hCzwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hC0AndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hC0QndEemca7hN01bE4g" name="AZAJNB" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hC0gndEemca7hN01bE4g" value="AZAJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hC0wndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hC1AndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hC1QndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hC1gndEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hC1wndEemca7hN01bE4g" name="AZA6NA" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hC2AndEemca7hN01bE4g" value="AZA6NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hC2QndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hC2gndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hC2wndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hC3AndEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hC3QndEemca7hN01bE4g" name="AZBACD" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hC3gndEemca7hN01bE4g" value="AZBACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hC3wndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7hC4AndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hC4QndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hC4gndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hC4wndEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hC5AndEemca7hN01bE4g" name="AZBBCD" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hC5QndEemca7hN01bE4g" value="AZBBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hC5gndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7hC5wndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hC6AndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hC6QndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hC6gndEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hC6wndEemca7hN01bE4g" name="AZA2NA" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hC7AndEemca7hN01bE4g" value="AZA2NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hC7QndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hC7gndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hC7wndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hC8AndEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hC8QndEemca7hN01bE4g" name="AZA3NA" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hC8gndEemca7hN01bE4g" value="AZA3NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hC8wndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hC9AndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hC9QndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hC9gndEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hC9wndEemca7hN01bE4g" name="AZCFCD" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hC-AndEemca7hN01bE4g" value="AZCFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hC-QndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hC-gndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hC-wndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hC_AndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hC_QndEemca7hN01bE4g" name="AZI4ST" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hC_gndEemca7hN01bE4g" value="AZI4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hC_wndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDAAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDAQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDAgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDAwndEemca7hN01bE4g" name="AZB7S1" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDBAndEemca7hN01bE4g" value="AZB7S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDBQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDBgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDBwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDCAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDCQndEemca7hN01bE4g" name="AZNHST" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDCgndEemca7hN01bE4g" value="AZNHST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDCwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDDAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDDQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDDgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDDwndEemca7hN01bE4g" name="AZD3C1" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDEAndEemca7hN01bE4g" value="AZD3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDEQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDEgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDEwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDFAndEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDFQndEemca7hN01bE4g" name="AZAUST" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDFgndEemca7hN01bE4g" value="AZAUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDFwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDGAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDGQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDGgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDGwndEemca7hN01bE4g" name="AZI5ST" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDHAndEemca7hN01bE4g" value="AZI5ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDHQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDHgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDHwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDIAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDIQndEemca7hN01bE4g" name="AZADVA" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDIgndEemca7hN01bE4g" value="AZADVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDIwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7hDJAndEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDJQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDJgndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDJwndEemca7hN01bE4g" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDKAndEemca7hN01bE4g" name="AZCBPR" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDKQndEemca7hN01bE4g" value="AZCBPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDKgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7hDKwndEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDLAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDLQndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDLgndEemca7hN01bE4g" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDLwndEemca7hN01bE4g" name="AZBCNA" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDMAndEemca7hN01bE4g" value="AZBCNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDMQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDMgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDMwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDNAndEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDNQndEemca7hN01bE4g" name="AZALNB" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDNgndEemca7hN01bE4g" value="AZALNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDNwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDOAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDOQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDOgndEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDOwndEemca7hN01bE4g" name="AZAMNB" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDPAndEemca7hN01bE4g" value="AZAMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDPQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDPgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDPwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDQAndEemca7hN01bE4g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDQQndEemca7hN01bE4g" name="AZANNB" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDQgndEemca7hN01bE4g" value="AZANNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDQwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDRAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDRQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDRgndEemca7hN01bE4g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDRwndEemca7hN01bE4g" name="AZAONB" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDSAndEemca7hN01bE4g" value="AZAONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDSQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDSgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDSwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDTAndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDTQndEemca7hN01bE4g" name="AZBGNA" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDTgndEemca7hN01bE4g" value="AZBGNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDTwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDUAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDUQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDUgndEemca7hN01bE4g" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDUwndEemca7hN01bE4g" name="AZBHNA" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDVAndEemca7hN01bE4g" value="AZBHNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDVQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDVgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDVwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDWAndEemca7hN01bE4g" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDWQndEemca7hN01bE4g" name="AZE2ST" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDWgndEemca7hN01bE4g" value="AZE2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDWwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDXAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDXQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDXgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7hDXwndEemca7hN01bE4g" name="AZCHCD" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7hDYAndEemca7hN01bE4g" value="AZCHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7hDYQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7hDYgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7hDYwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7hDZAndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzYAndEemca7hN01bE4g" name="AZAJCD" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzYQndEemca7hN01bE4g" value="AZAJCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzYgndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7qzYwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzZAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzZQndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzZgndEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzZwndEemca7hN01bE4g" name="AZBINA" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzaAndEemca7hN01bE4g" value="AZBINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzaQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzagndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzawndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzbAndEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzbQndEemca7hN01bE4g" name="AZCQTX" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzbgndEemca7hN01bE4g" value="AZCQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzbwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzcAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzcQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzcgndEemca7hN01bE4g" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzcwndEemca7hN01bE4g" name="AZI6ST" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzdAndEemca7hN01bE4g" value="AZI6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzdQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzdgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzdwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzeAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzeQndEemca7hN01bE4g" name="AZBJN3" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzegndEemca7hN01bE4g" value="AZBJN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzewndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzfAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzfQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzfgndEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzfwndEemca7hN01bE4g" name="AZBON3" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzgAndEemca7hN01bE4g" value="AZBON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzgQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzggndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzgwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzhAndEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzhQndEemca7hN01bE4g" name="AZBPN3" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzhgndEemca7hN01bE4g" value="AZBPN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzhwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qziAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qziQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzigndEemca7hN01bE4g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qziwndEemca7hN01bE4g" name="AZHNTX" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzjAndEemca7hN01bE4g" value="AZHNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzjQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzjgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzjwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzkAndEemca7hN01bE4g" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzkQndEemca7hN01bE4g" name="AZHOTX" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzkgndEemca7hN01bE4g" value="AZHOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzkwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzlAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzlQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzlgndEemca7hN01bE4g" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzlwndEemca7hN01bE4g" name="AZHPTX" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzmAndEemca7hN01bE4g" value="AZHPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzmQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzmgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzmwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qznAndEemca7hN01bE4g" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qznQndEemca7hN01bE4g" name="AZHQTX" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzngndEemca7hN01bE4g" value="AZHQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qznwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzoAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzoQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzogndEemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzowndEemca7hN01bE4g" name="AZBQN3" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzpAndEemca7hN01bE4g" value="AZBQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzpQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzpgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzpwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzqAndEemca7hN01bE4g" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzqQndEemca7hN01bE4g" name="AZAKCD" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzqgndEemca7hN01bE4g" value="AZAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzqwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzrAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzrQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzrgndEemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzrwndEemca7hN01bE4g" name="AZFVPR" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzsAndEemca7hN01bE4g" value="AZFVPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzsQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7qzsgndEemca7hN01bE4g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzswndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qztAndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qztQndEemca7hN01bE4g" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qztgndEemca7hN01bE4g" name="AZGHDT" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qztwndEemca7hN01bE4g" value="AZGHDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzuAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7qzuQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzugndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzuwndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzvAndEemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_S7qzvQndEemca7hN01bE4g" name="AZGIDT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_S7qzvgndEemca7hN01bE4g" value="AZGIDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_S7qzvwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_S7qzwAndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_S7qzwQndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_S7qzwgndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_S7qzwwndEemca7hN01bE4g" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_396XAQndEemca7hN01bE4g" name="ORAJREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_396XAgndEemca7hN01bE4g" value="ORAJREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_396XAwndEemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_4AfJ4AndEemca7hN01bE4g" name="AJAMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_4AfJ4QndEemca7hN01bE4g" value="AJAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4AfJ4gndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4AfJ4wndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4AfJ5AndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4AfJ5QndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4AfJ5gndEemca7hN01bE4g" name="AJAJCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_4AfJ5wndEemca7hN01bE4g" value="AJAJCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4AfJ6AndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_4AfJ6QndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4AfJ6gndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4AfJ6wndEemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4AfJ7AndEemca7hN01bE4g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4AfJ7QndEemca7hN01bE4g" name="AJAFTX" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_4AfJ7gndEemca7hN01bE4g" value="AJAFTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4AfJ7wndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4AfJ8AndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4AfJ8QndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4AfJ8gndEemca7hN01bE4g" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4AfJ8wndEemca7hN01bE4g" name="AJAEST" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_4AfJ9AndEemca7hN01bE4g" value="AJAEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4AfJ9QndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4AfJ9gndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4AfJ9wndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4AfJ-AndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4AfJ-QndEemca7hN01bE4g" name="AJAFST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_4AfJ-gndEemca7hN01bE4g" value="AJAFST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4AfJ-wndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4AfJ_AndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4AfJ_QndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4AfJ_gndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4AfJ_wndEemca7hN01bE4g" name="AJE9ST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_4AfKAAndEemca7hN01bE4g" value="AJE9ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4AfKAQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4AfKAgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4AfKAwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4AfKBAndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4AfKBQndEemca7hN01bE4g" name="AJLQST" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_4AfKBgndEemca7hN01bE4g" value="AJLQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4AfKBwndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4AfKCAndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4AfKCQndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4AfKCgndEemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4AfKCwndEemca7hN01bE4g" name="AJNUST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_4AfKDAndEemca7hN01bE4g" value="AJNUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4AfKDQndEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4AfKDgndEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4AfKDwndEemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4AfKEAndEemca7hN01bE4g" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_s892cQn7Eemca7hN01bE4g" name="ORJNREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_s892cgn7Eemca7hN01bE4g" value="ORJNREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_s892cwn7Eemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_s9aiYAn7Eemca7hN01bE4g" name="JNB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_s9aiYQn7Eemca7hN01bE4g" value="JNB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s9aiYgn7Eemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s9aiYwn7Eemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s9aiZAn7Eemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s9aiZQn7Eemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s9aiZgn7Eemca7hN01bE4g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s9aiZwn7Eemca7hN01bE4g" name="JNAMCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_s9aiaAn7Eemca7hN01bE4g" value="JNAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s9aiaQn7Eemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s9aiagn7Eemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s9aiawn7Eemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s9aibAn7Eemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s9ez0An7Eemca7hN01bE4g" name="JNDMCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_s9ez0Qn7Eemca7hN01bE4g" value="JNDMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s9ez0gn7Eemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s9ez0wn7Eemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s9ez1An7Eemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s9ez1Qn7Eemca7hN01bE4g" value="2"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_suyQgAn7Eemca7hN01bE4g" name="ORESREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_suyQgQn7Eemca7hN01bE4g" value="ORESREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_suyQggn7Eemca7hN01bE4g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_s6GvsAn7Eemca7hN01bE4g" name="ESAMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_s6GvsQn7Eemca7hN01bE4g" value="ESAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s6Gvsgn7Eemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s6Gvswn7Eemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s6GvtAn7Eemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s6GvtQn7Eemca7hN01bE4g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s6Gvtgn7Eemca7hN01bE4g" name="ESDMCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_s6Gvtwn7Eemca7hN01bE4g" value="ESDMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s6GvuAn7Eemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s6GvuQn7Eemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s6Gvugn7Eemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s6Gvuwn7Eemca7hN01bE4g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s6GvvAn7Eemca7hN01bE4g" name="ESA3PC" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_s6GvvQn7Eemca7hN01bE4g" value="ESA3PC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s6Gvvgn7Eemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s6Gvvwn7Eemca7hN01bE4g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s6GvwAn7Eemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s6GvwQn7Eemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s6Gvwgn7Eemca7hN01bE4g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s6Gvwwn7Eemca7hN01bE4g" name="ESBUVA" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_s6GvxAn7Eemca7hN01bE4g" value="ESBUVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s6GvxQn7Eemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s6Gvxgn7Eemca7hN01bE4g" value="8"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s6Gvxwn7Eemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s6GvyAn7Eemca7hN01bE4g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s6GvyQn7Eemca7hN01bE4g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s6Gvygn7Eemca7hN01bE4g" name="ESS8ST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_s6Gvywn7Eemca7hN01bE4g" value="ESS8ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s6GvzAn7Eemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s6GvzQn7Eemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s6Gvzgn7Eemca7hN01bE4g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s6Gvzwn7Eemca7hN01bE4g" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_cb8CIQrBEempwYQVaQNkuA" name="PIXEL_MAGENTO_CLIENTS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_cb8CIgrBEempwYQVaQNkuA" value="PIXEL_MAGENTO_CLIENTS"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_ccFMEArBEempwYQVaQNkuA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_cc5EYArBEempwYQVaQNkuA" name="PMC_CLIENT_CODE" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_cc5EYQrBEempwYQVaQNkuA" value="PMC_CLIENT_CODE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cc5EYgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cc5EYwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cc5EZArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cc5EZQrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cc5EZgrBEempwYQVaQNkuA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cc5EZwrBEempwYQVaQNkuA" name="PMC_SOCIETE_CODE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_cc5EaArBEempwYQVaQNkuA" value="PMC_SOCIETE_CODE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cc5EaQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cc5EagrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cc5EawrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cc5EbArBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cc5EbQrBEempwYQVaQNkuA" name="PMC_SOCIETE_ID_MAGENTO" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_cc5EbgrBEempwYQVaQNkuA" value="PMC_SOCIETE_ID_MAGENTO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cc5EbwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cc5EcArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cc5EcQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cc5EcgrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cc5EcwrBEempwYQVaQNkuA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cc5EdArBEempwYQVaQNkuA" name="PMC_DATE_CREATION" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_cc5EdQrBEempwYQVaQNkuA" value="PMC_DATE_CREATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cc5EdgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cc5EdwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cc5EeArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cc5EeQrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cc5EegrBEempwYQVaQNkuA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cc5EewrBEempwYQVaQNkuA" name="PMC_TOP_SUPPRESSION" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_cc5EfArBEempwYQVaQNkuA" value="PMC_TOP_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cc5EfQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cc5EfgrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cc5EfwrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cc5EgArBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cc5EgQrBEempwYQVaQNkuA" name="PMC_DATE_SUPPRESSION" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_cc5EggrBEempwYQVaQNkuA" value="PMC_DATE_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cc5EgwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cc5EhArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cc5EhQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cc5EhgrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cc5EhwrBEempwYQVaQNkuA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cc5EiArBEempwYQVaQNkuA" name="PMC_DATE_MODIFICATION" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_cc5EiQrBEempwYQVaQNkuA" value="PMC_DATE_MODIFICATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cc5EigrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cc5EiwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cc5EjArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cc5EjQrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cc5EjgrBEempwYQVaQNkuA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_WdnhYGBnEema_r8Hg2-1dA" name="PMC_NOM" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_WdnhYWBnEema_r8Hg2-1dA" value="PMC_NOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_WdnhYmBnEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_WdnhY2BnEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_WdnhZGBnEema_r8Hg2-1dA" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_WdnhZWBnEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_WdnhZmBnEema_r8Hg2-1dA" name="PMC_PRENOM" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_WdnhZ2BnEema_r8Hg2-1dA" value="PMC_PRENOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_WdnhaGBnEema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_WdnhaWBnEema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_WdnhamBnEema_r8Hg2-1dA" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Wdnha2BnEema_r8Hg2-1dA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UtlaYHJ_EemIMIaOcxSI7A" name="PMC_STORE_ID" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_UtlaYXJ_EemIMIaOcxSI7A" value="PMC_STORE_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UtlaYnJ_EemIMIaOcxSI7A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UtlaY3J_EemIMIaOcxSI7A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UtlaZHJ_EemIMIaOcxSI7A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UtlaZXJ_EemIMIaOcxSI7A" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UtlaZnJ_EemIMIaOcxSI7A" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_URSTN55hEem6jcmdEfXK_g" name="PMC_MAIL_SOCIETE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_URSTOJ5hEem6jcmdEfXK_g" value="PMC_MAIL_SOCIETE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_URSTOZ5hEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_URSTOp5hEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_URSTO55hEem6jcmdEfXK_g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_URSTPJ5hEem6jcmdEfXK_g" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_URS6QJ5hEem6jcmdEfXK_g" name="PMC_MAIL_SOCIETE_BIS" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_URS6QZ5hEem6jcmdEfXK_g" value="PMC_MAIL_SOCIETE_BIS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_URS6Qp5hEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_URS6Q55hEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_URS6RJ5hEem6jcmdEfXK_g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_URS6RZ5hEem6jcmdEfXK_g" value="250"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_cWEDoArBEempwYQVaQNkuA" name="WEBCLI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_cWEDoQrBEempwYQVaQNkuA" value="WEBCLI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_cWEDogrBEempwYQVaQNkuA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_cbIJ0ArBEempwYQVaQNkuA" name="IDWEBC" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbIJ0QrBEempwYQVaQNkuA" value="IDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbIJ0grBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbIJ0wrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbIJ1ArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbIJ1QrBEempwYQVaQNkuA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbIJ1grBEempwYQVaQNkuA" name="CPTCLI" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbIJ1wrBEempwYQVaQNkuA" value="CPTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbIJ2ArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbIJ2QrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbIJ2grBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbIJ2wrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbIJ3ArBEempwYQVaQNkuA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0MArBEempwYQVaQNkuA" name="NOMCLI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0MQrBEempwYQVaQNkuA" value="NOMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0MgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0MwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0NArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0NQrBEempwYQVaQNkuA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0NgrBEempwYQVaQNkuA" name="PRECLI" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0NwrBEempwYQVaQNkuA" value="PRECLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0OArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0OQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0OgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0OwrBEempwYQVaQNkuA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0PArBEempwYQVaQNkuA" name="PWDCLI" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0PQrBEempwYQVaQNkuA" value="PWDCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0PgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0PwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0QArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0QQrBEempwYQVaQNkuA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0QgrBEempwYQVaQNkuA" name="MELADR" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0QwrBEempwYQVaQNkuA" value="MELADR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0RArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0RQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0RgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0RwrBEempwYQVaQNkuA" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0SArBEempwYQVaQNkuA" name="MELSIT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0SQrBEempwYQVaQNkuA" value="MELSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0SgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0SwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0TArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0TQrBEempwYQVaQNkuA" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0TgrBEempwYQVaQNkuA" name="NIVCLI" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0TwrBEempwYQVaQNkuA" value="NIVCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0UArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0UQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0UgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0UwrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0VArBEempwYQVaQNkuA" name="ACTCLI" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0VQrBEempwYQVaQNkuA" value="ACTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0VgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0VwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0WArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0WQrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0WgrBEempwYQVaQNkuA" name="TYPTAR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0WwrBEempwYQVaQNkuA" value="TYPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0XArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0XQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0XgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0XwrBEempwYQVaQNkuA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0YArBEempwYQVaQNkuA" name="TXREMI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0YQrBEempwYQVaQNkuA" value="TXREMI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0YgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL0YwrBEempwYQVaQNkuA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0ZArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0ZQrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0ZgrBEempwYQVaQNkuA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0ZwrBEempwYQVaQNkuA" name="FRANCO" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0aArBEempwYQVaQNkuA" value="FRANCO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0aQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL0agrBEempwYQVaQNkuA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0awrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0bArBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0bQrBEempwYQVaQNkuA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0bgrBEempwYQVaQNkuA" name="FRPORT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0bwrBEempwYQVaQNkuA" value="FRPORT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0cArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL0cQrBEempwYQVaQNkuA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0cgrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0cwrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0dArBEempwYQVaQNkuA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0dQrBEempwYQVaQNkuA" name="MINCDE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0dgrBEempwYQVaQNkuA" value="MINCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0dwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL0eArBEempwYQVaQNkuA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0eQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0egrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0ewrBEempwYQVaQNkuA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0fArBEempwYQVaQNkuA" name="MERCUR" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0fQrBEempwYQVaQNkuA" value="MERCUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0fgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0fwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0gArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0gQrBEempwYQVaQNkuA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0ggrBEempwYQVaQNkuA" name="CDECAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0gwrBEempwYQVaQNkuA" value="CDECAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0hArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0hQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0hgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0hwrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0iArBEempwYQVaQNkuA" name="CDELST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0iQrBEempwYQVaQNkuA" value="CDELST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0igrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0iwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0jArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0jQrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0jgrBEempwYQVaQNkuA" name="CDEMER" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0jwrBEempwYQVaQNkuA" value="CDEMER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0kArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0kQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0kgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0kwrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0lArBEempwYQVaQNkuA" name="CDEFAV" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0lQrBEempwYQVaQNkuA" value="CDEFAV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0lgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0lwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0mArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0mQrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0mgrBEempwYQVaQNkuA" name="TARCLI" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0mwrBEempwYQVaQNkuA" value="TARCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0nArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0nQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0ngrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0nwrBEempwYQVaQNkuA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0oArBEempwYQVaQNkuA" name="UNIVCL" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0oQrBEempwYQVaQNkuA" value="UNIVCL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0ogrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0owrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0pArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0pQrBEempwYQVaQNkuA" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0pgrBEempwYQVaQNkuA" name="REMISE" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0pwrBEempwYQVaQNkuA" value="REMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0qArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL0qQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0qgrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0qwrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0rArBEempwYQVaQNkuA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0rQrBEempwYQVaQNkuA" name="IMPTAR" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0rgrBEempwYQVaQNkuA" value="IMPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0rwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0sArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0sQrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0sgrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0swrBEempwYQVaQNkuA" name="CENTRA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0tArBEempwYQVaQNkuA" value="CENTRA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0tQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL0tgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0twrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0uArBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0uQrBEempwYQVaQNkuA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0ugrBEempwYQVaQNkuA" name="MAILVRP" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0uwrBEempwYQVaQNkuA" value="MAILVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0vArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0vQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0vgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0vwrBEempwYQVaQNkuA" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0wArBEempwYQVaQNkuA" name="CLIMAIL" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0wQrBEempwYQVaQNkuA" value="CLIMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0wgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0wwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0xArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0xQrBEempwYQVaQNkuA" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0xgrBEempwYQVaQNkuA" name="SPECIAL" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0xwrBEempwYQVaQNkuA" value="SPECIAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0yArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0yQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0ygrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0ywrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0zArBEempwYQVaQNkuA" name="CODSOC" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0zQrBEempwYQVaQNkuA" value="CODSOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0zgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0zwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL00ArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL00QrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL00grBEempwYQVaQNkuA" name="TELVRP" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL00wrBEempwYQVaQNkuA" value="TELVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL01ArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL01QrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL01grBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL01wrBEempwYQVaQNkuA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL02ArBEempwYQVaQNkuA" name="NOMVRP" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL02QrBEempwYQVaQNkuA" value="NOMVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL02grBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL02wrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL03ArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL03QrBEempwYQVaQNkuA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL03grBEempwYQVaQNkuA" name="LNGCLI" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL03wrBEempwYQVaQNkuA" value="LNGCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL04ArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL04QrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL04grBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL04wrBEempwYQVaQNkuA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL05ArBEempwYQVaQNkuA" name="PWDMD5" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL05QrBEempwYQVaQNkuA" value="PWDMD5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL05grBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL05wrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL06ArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL06QrBEempwYQVaQNkuA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL06grBEempwYQVaQNkuA" name="NOMCOM" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL06wrBEempwYQVaQNkuA" value="NOMCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL07ArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL07QrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL07grBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL07wrBEempwYQVaQNkuA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL08ArBEempwYQVaQNkuA" name="MELCOM" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL08QrBEempwYQVaQNkuA" value="MELCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL08grBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL08wrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL09ArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL09QrBEempwYQVaQNkuA" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL09grBEempwYQVaQNkuA" name="TOPCRE" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL09wrBEempwYQVaQNkuA" value="TOPCRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0-ArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL0-QrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL0-grBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL0-wrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL0_ArBEempwYQVaQNkuA" name="TOPPROMO" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL0_QrBEempwYQVaQNkuA" value="TOPPROMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL0_grBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL0_wrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL1AArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL1AQrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL1AgrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL1AwrBEempwYQVaQNkuA" name="TOPMERCURIALE" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL1BArBEempwYQVaQNkuA" value="TOPMERCURIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL1BQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL1BgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL1BwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL1CArBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL1CQrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL1CgrBEempwYQVaQNkuA" name="TOPKIT" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL1CwrBEempwYQVaQNkuA" value="TOPKIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL1DArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL1DQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL1DgrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL1DwrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL1EArBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL1EQrBEempwYQVaQNkuA" name="TOPPRINCIPAL" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbL1EgrBEempwYQVaQNkuA" value="TOPPRINCIPAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbL1EwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbL1FArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbL1FQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbL1FgrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbL1FwrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbL1GArBEempwYQVaQNkuA" name="TOPSUPERVISION" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlMArBEempwYQVaQNkuA" value="TOPSUPERVISION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlMQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbVlMgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlMwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlNArBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlNQrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlNgrBEempwYQVaQNkuA" name="MODRGLMT" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlNwrBEempwYQVaQNkuA" value="MODRGLMT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlOArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbVlOQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlOgrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlOwrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlPArBEempwYQVaQNkuA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlPQrBEempwYQVaQNkuA" name="TOPFA" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlPgrBEempwYQVaQNkuA" value="TOPFA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlPwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbVlQArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlQQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlQgrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlQwrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlRArBEempwYQVaQNkuA" name="TOPLE" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlRQrBEempwYQVaQNkuA" value="TOPLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlRgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbVlRwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlSArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlSQrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlSgrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlSwrBEempwYQVaQNkuA" name="FACTURATION" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlTArBEempwYQVaQNkuA" value="FACTURATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlTQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlTgrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlTwrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlUArBEempwYQVaQNkuA" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlUQrBEempwYQVaQNkuA" name="ADRESSE" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlUgrBEempwYQVaQNkuA" value="ADRESSE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlUwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlVArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlVQrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlVgrBEempwYQVaQNkuA" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlVwrBEempwYQVaQNkuA" name="TXREMISE" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlWArBEempwYQVaQNkuA" value="TXREMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlWQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlWgrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlWwrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlXArBEempwYQVaQNkuA" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlXQrBEempwYQVaQNkuA" name="TOPCHR" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlXgrBEempwYQVaQNkuA" value="TOPCHR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlXwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbVlYArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlYQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlYgrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlYwrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlZArBEempwYQVaQNkuA" name="MAXCDE" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlZQrBEempwYQVaQNkuA" value="MAXCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlZgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbVlZwrBEempwYQVaQNkuA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlaArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlaQrBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlagrBEempwYQVaQNkuA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlawrBEempwYQVaQNkuA" name="MERCUGRP" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlbArBEempwYQVaQNkuA" value="MERCUGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlbQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlbgrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlbwrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlcArBEempwYQVaQNkuA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlcQrBEempwYQVaQNkuA" name="CDEMERC" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlcgrBEempwYQVaQNkuA" value="CDEMERC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlcwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVldArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVldQrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVldgrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVldwrBEempwYQVaQNkuA" name="TOPEFAC" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVleArBEempwYQVaQNkuA" value="TOPEFAC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVleQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cbVlegrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlewrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlfArBEempwYQVaQNkuA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlfQrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cbVlfgrBEempwYQVaQNkuA" name="AUTOLOGKEY" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_cbVlfwrBEempwYQVaQNkuA" value="AUTOLOGKEY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cbVlgArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cbVlgQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cbVlggrBEempwYQVaQNkuA" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cbVlgwrBEempwYQVaQNkuA" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_cF9yIQrBEempwYQVaQNkuA" name="PIXEL_MAGENTO_USERS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_cF9yIgrBEempwYQVaQNkuA" value="PIXEL_MAGENTO_USERS"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_cF9yIwrBEempwYQVaQNkuA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_cQ1lYArBEempwYQVaQNkuA" name="PMU_CLIENT_CODE" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1lYQrBEempwYQVaQNkuA" value="PMU_CLIENT_CODE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1lYgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cQ1lYwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lZArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lZQrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lZgrBEempwYQVaQNkuA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1lZwrBEempwYQVaQNkuA" name="PMU_SOCIETE_CODE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1laArBEempwYQVaQNkuA" value="PMU_SOCIETE_CODE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1laQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lagrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lawrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lbArBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1lbQrBEempwYQVaQNkuA" name="PMU_ID_SOCIETE_MAGENTO" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1lbgrBEempwYQVaQNkuA" value="PMU_ID_SOCIETE_MAGENTO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1lbwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cQ1lcArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lcQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lcgrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lcwrBEempwYQVaQNkuA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1ldArBEempwYQVaQNkuA" name="PMU_USER_ID_MAGENTO" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1ldQrBEempwYQVaQNkuA" value="PMU_USER_ID_MAGENTO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1ldgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cQ1ldwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1leArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1leQrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1legrBEempwYQVaQNkuA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1lewrBEempwYQVaQNkuA" name="PMU_DATE_CREATION" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1lfArBEempwYQVaQNkuA" value="PMU_DATE_CREATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1lfQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cQ1lfgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lfwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lgArBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lgQrBEempwYQVaQNkuA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1lggrBEempwYQVaQNkuA" name="PMU_CIVILITE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1lgwrBEempwYQVaQNkuA" value="PMU_CIVILITE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1lhArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lhQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lhgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lhwrBEempwYQVaQNkuA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1liArBEempwYQVaQNkuA" name="PMU_PRENOM_USER" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1liQrBEempwYQVaQNkuA" value="PMU_PRENOM_USER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1ligrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1liwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1ljArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1ljQrBEempwYQVaQNkuA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1ljgrBEempwYQVaQNkuA" name="PMU_NOM_USER" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1ljwrBEempwYQVaQNkuA" value="PMU_NOM_USER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1lkArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lkQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lkgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lkwrBEempwYQVaQNkuA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1llArBEempwYQVaQNkuA" name="PMU_INTITULE_POSTE" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1llQrBEempwYQVaQNkuA" value="PMU_INTITULE_POSTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1llgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1llwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lmArBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lmQrBEempwYQVaQNkuA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1lmgrBEempwYQVaQNkuA" name="PMU_ADRESSE_EMAIL" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1lmwrBEempwYQVaQNkuA" value="PMU_ADRESSE_EMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1lnArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lnQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lngrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lnwrBEempwYQVaQNkuA" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1loArBEempwYQVaQNkuA" name="PMU_NIVEAU_USER" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1loQrBEempwYQVaQNkuA" value="PMU_NIVEAU_USER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1logrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cQ1lowrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lpArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lpQrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lpgrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1lpwrBEempwYQVaQNkuA" name="PMU_DATE_SUPPRESSION" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1lqArBEempwYQVaQNkuA" value="PMU_DATE_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1lqQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cQ1lqgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lqwrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lrArBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lrQrBEempwYQVaQNkuA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1lrgrBEempwYQVaQNkuA" name="PMU_SUPPRESSION_LOGIQUE" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1lrwrBEempwYQVaQNkuA" value="PMU_SUPPRESSION_LOGIQUE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1lsArBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lsQrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lsgrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lswrBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1ltArBEempwYQVaQNkuA" name="PMU_DATE_MODIF" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1ltQrBEempwYQVaQNkuA" value="PMU_DATE_MODIF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1ltgrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_cQ1ltwrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1luArBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1luQrBEempwYQVaQNkuA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lugrBEempwYQVaQNkuA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_cQ1luwrBEempwYQVaQNkuA" name="PMU_ABONNEMENT_NEWSLETTER" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_cQ1lvArBEempwYQVaQNkuA" value="PMU_ABONNEMENT_NEWSLETTER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_cQ1lvQrBEempwYQVaQNkuA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_cQ1lvgrBEempwYQVaQNkuA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_cQ1lvwrBEempwYQVaQNkuA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_cQ1lwArBEempwYQVaQNkuA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UmLLHXJ_EemIMIaOcxSI7A" name="PMU_STORE_ID" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_UmLLHnJ_EemIMIaOcxSI7A" value="PMU_STORE_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UmLLH3J_EemIMIaOcxSI7A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UmLLIHJ_EemIMIaOcxSI7A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UmLLIXJ_EemIMIaOcxSI7A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UmLLInJ_EemIMIaOcxSI7A" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UmLLI3J_EemIMIaOcxSI7A" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_IVF1JA6lEem8rKIEbYLbhQ" name="OTKHCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_IVF1JQ6lEem8rKIEbYLbhQ" value="OTKHCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_IVF1Jg6lEem8rKIEbYLbhQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_IVZXEA6lEem8rKIEbYLbhQ" name="KHBPCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_IVZXEQ6lEem8rKIEbYLbhQ" value="KHBPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IVZXEg6lEem8rKIEbYLbhQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_IVZXEw6lEem8rKIEbYLbhQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IVZXFA6lEem8rKIEbYLbhQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IVZXFQ6lEem8rKIEbYLbhQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IVZXFg6lEem8rKIEbYLbhQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IVZXFw6lEem8rKIEbYLbhQ" name="KHEONB" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_IVZXGA6lEem8rKIEbYLbhQ" value="KHEONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IVZXGQ6lEem8rKIEbYLbhQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_IVZXGg6lEem8rKIEbYLbhQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IVZXGw6lEem8rKIEbYLbhQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IVZXHA6lEem8rKIEbYLbhQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IVZXHQ6lEem8rKIEbYLbhQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IVZXHg6lEem8rKIEbYLbhQ" name="KHYBC1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_IVZXHw6lEem8rKIEbYLbhQ" value="KHYBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IVZXIA6lEem8rKIEbYLbhQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IVZXIQ6lEem8rKIEbYLbhQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IVZXIg6lEem8rKIEbYLbhQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IVZXIw6lEem8rKIEbYLbhQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IVZXJA6lEem8rKIEbYLbhQ" name="KHQ7S3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_IVZXJQ6lEem8rKIEbYLbhQ" value="KHQ7S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IVZXJg6lEem8rKIEbYLbhQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IVZXJw6lEem8rKIEbYLbhQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IVZXKA6lEem8rKIEbYLbhQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IVZXKQ6lEem8rKIEbYLbhQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IVZXKg6lEem8rKIEbYLbhQ" name="KHQ8S3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_IVZXKw6lEem8rKIEbYLbhQ" value="KHQ8S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IVZXLA6lEem8rKIEbYLbhQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IVZXLQ6lEem8rKIEbYLbhQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IVZXLg6lEem8rKIEbYLbhQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IVZXLw6lEem8rKIEbYLbhQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IVZXMA6lEem8rKIEbYLbhQ" name="KHQ9S3" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_IVZXMQ6lEem8rKIEbYLbhQ" value="KHQ9S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IVZXMg6lEem8rKIEbYLbhQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IVZXMw6lEem8rKIEbYLbhQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IVZXNA6lEem8rKIEbYLbhQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IVZXNQ6lEem8rKIEbYLbhQ" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_pvMi8RS8EemsgZOghPO5Pg" name="ORB6REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_pvMi8hS8EemsgZOghPO5Pg" value="ORB6REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_pvMi8xS8EemsgZOghPO5Pg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_pv_0MBS8EemsgZOghPO5Pg" name="B6AMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_pv_0MRS8EemsgZOghPO5Pg" value="B6AMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pv_0MhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pv_0MxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pv_0NBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pv_0NRS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pv_0NhS8EemsgZOghPO5Pg" name="B6BTCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_pv_0NxS8EemsgZOghPO5Pg" value="B6BTCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pv_0OBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pv_0ORS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pv_0OhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pv_0OxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pv_0PBS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pv_0PRS8EemsgZOghPO5Pg" name="B6AUNB" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_pv_0PhS8EemsgZOghPO5Pg" value="B6AUNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pv_0PxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pv_0QBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pv_0QRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pv_0QhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pv_0QxS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pv_0RBS8EemsgZOghPO5Pg" name="B6RUNB" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_pv_0RRS8EemsgZOghPO5Pg" value="B6RUNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pv_0RhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pv_0RxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pv_0SBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pv_0SRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pv_0ShS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlMBS8EemsgZOghPO5Pg" name="B6AZCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlMRS8EemsgZOghPO5Pg" value="B6AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlMhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlMxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlNBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlNRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlNhS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlNxS8EemsgZOghPO5Pg" name="B6BPCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlOBS8EemsgZOghPO5Pg" value="B6BPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlORS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlOhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlOxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlPBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlPRS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlPhS8EemsgZOghPO5Pg" name="B6EONB" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlPxS8EemsgZOghPO5Pg" value="B6EONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlQBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlQRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlQhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlQxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlRBS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlRRS8EemsgZOghPO5Pg" name="B6BQCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlRhS8EemsgZOghPO5Pg" value="B6BQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlRxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlSBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlSRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlShS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlSxS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlTBS8EemsgZOghPO5Pg" name="B6RHNB" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlTRS8EemsgZOghPO5Pg" value="B6RHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlThS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlTxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlUBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlURS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlUhS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlUxS8EemsgZOghPO5Pg" name="B6RINB" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlVBS8EemsgZOghPO5Pg" value="B6RINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlVRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlVhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlVxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlWBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlWRS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlWhS8EemsgZOghPO5Pg" name="B6CACD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlWxS8EemsgZOghPO5Pg" value="B6CACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlXBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlXRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlXhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlXxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlYBS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlYRS8EemsgZOghPO5Pg" name="B6B9CD" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlYhS8EemsgZOghPO5Pg" value="B6B9CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlYxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlZBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlZRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlZhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlZxS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlaBS8EemsgZOghPO5Pg" name="B6RFNB" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlaRS8EemsgZOghPO5Pg" value="B6RFNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlahS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlaxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlbBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlbRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlbhS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlbxS8EemsgZOghPO5Pg" name="B6RGNB" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlcBS8EemsgZOghPO5Pg" value="B6RGNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlcRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlchS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlcxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJldBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJldRS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJldhS8EemsgZOghPO5Pg" name="B6S2CD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJldxS8EemsgZOghPO5Pg" value="B6S2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJleBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJleRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlehS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlexS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlfBS8EemsgZOghPO5Pg" name="B6DKN1" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlfRS8EemsgZOghPO5Pg" value="B6DKN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlfhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlfxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlgBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlgRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlghS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlgxS8EemsgZOghPO5Pg" name="B6LKNB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlhBS8EemsgZOghPO5Pg" value="B6LKNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlhRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlhhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlhxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJliBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJliRS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlihS8EemsgZOghPO5Pg" name="B6RWNB" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlixS8EemsgZOghPO5Pg" value="B6RWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJljBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJljRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJljhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJljxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlkBS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlkRS8EemsgZOghPO5Pg" name="B6BITX" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlkhS8EemsgZOghPO5Pg" value="B6BITX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlkxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJllBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJllRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJllhS8EemsgZOghPO5Pg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJllxS8EemsgZOghPO5Pg" name="B6BFCD" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlmBS8EemsgZOghPO5Pg" value="B6BFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlmRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlmhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlmxS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlnBS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlnRS8EemsgZOghPO5Pg" name="B6XIST" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlnhS8EemsgZOghPO5Pg" value="B6XIST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlnxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJloBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJloRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlohS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJloxS8EemsgZOghPO5Pg" name="B6R2NB" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlpBS8EemsgZOghPO5Pg" value="B6R2NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlpRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlphS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlpxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlqBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlqRS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlqhS8EemsgZOghPO5Pg" name="B6HMCD" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlqxS8EemsgZOghPO5Pg" value="B6HMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlrBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlrRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlrhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlrxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlsBS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlsRS8EemsgZOghPO5Pg" name="B6XFST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlshS8EemsgZOghPO5Pg" value="B6XFST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlsxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJltBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJltRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlthS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJltxS8EemsgZOghPO5Pg" name="B6HBCD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJluBS8EemsgZOghPO5Pg" value="B6HBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJluRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJluhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJluxS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlvBS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlvRS8EemsgZOghPO5Pg" name="B6LGST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlvhS8EemsgZOghPO5Pg" value="B6LGST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlvxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlwBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlwRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlwhS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlwxS8EemsgZOghPO5Pg" name="B6AKQT" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlxBS8EemsgZOghPO5Pg" value="B6AKQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlxRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJlxhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlxxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlyBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlyRS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJlyhS8EemsgZOghPO5Pg" name="B6HCCD" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJlyxS8EemsgZOghPO5Pg" value="B6HCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJlzBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJlzRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJlzhS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJlzxS8EemsgZOghPO5Pg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJl0BS8EemsgZOghPO5Pg" name="B6C4PC" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJl0RS8EemsgZOghPO5Pg" value="B6C4PC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJl0hS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJl0xS8EemsgZOghPO5Pg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJl1BS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJl1RS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJl1hS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJl1xS8EemsgZOghPO5Pg" name="B6E3PR" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJl2BS8EemsgZOghPO5Pg" value="B6E3PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJl2RS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJl2hS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJl2xS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJl3BS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJl3RS8EemsgZOghPO5Pg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJl3hS8EemsgZOghPO5Pg" name="B6E4PR" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJl3xS8EemsgZOghPO5Pg" value="B6E4PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJl4BS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJl4RS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJl4hS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJl4xS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJl5BS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJl5RS8EemsgZOghPO5Pg" name="B6OTPR" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJl5hS8EemsgZOghPO5Pg" value="B6OTPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJl5xS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJl6BS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJl6RS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJl6hS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJl6xS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJl7BS8EemsgZOghPO5Pg" name="B6AOPR" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJl7RS8EemsgZOghPO5Pg" value="B6AOPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJl7hS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJl7xS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJl8BS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJl8RS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJl8hS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJl8xS8EemsgZOghPO5Pg" name="B6JNP2" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJl9BS8EemsgZOghPO5Pg" value="B6JNP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJl9RS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJl9hS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJl9xS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJl-BS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJl-RS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJl-hS8EemsgZOghPO5Pg" name="B6HBP1" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJl-xS8EemsgZOghPO5Pg" value="B6HBP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJl_BS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJl_RS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJl_hS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJl_xS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJmABS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwJmARS8EemsgZOghPO5Pg" name="B6JOP2" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwJmAhS8EemsgZOghPO5Pg" value="B6JOP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwJmAxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwJmBBS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwJmBRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwJmBhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwJmBxS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvIBS8EemsgZOghPO5Pg" name="B6G9P1" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvIRS8EemsgZOghPO5Pg" value="B6G9P1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvIhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvIxS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvJBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvJRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvJhS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvJxS8EemsgZOghPO5Pg" name="B6HAP1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvKBS8EemsgZOghPO5Pg" value="B6HAP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvKRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvKhS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvKxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvLBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvLRS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvLhS8EemsgZOghPO5Pg" name="B6APPR" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvLxS8EemsgZOghPO5Pg" value="B6APPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvMBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvMRS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvMhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvMxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvNBS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvNRS8EemsgZOghPO5Pg" name="B6LDP2" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvNhS8EemsgZOghPO5Pg" value="B6LDP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvNxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvOBS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvORS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvOhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvOxS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvPBS8EemsgZOghPO5Pg" name="B6CBPC" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvPRS8EemsgZOghPO5Pg" value="B6CBPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvPhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvPxS8EemsgZOghPO5Pg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvQBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvQRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvQhS8EemsgZOghPO5Pg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvQxS8EemsgZOghPO5Pg" name="B6HNNB" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvRBS8EemsgZOghPO5Pg" value="B6HNNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvRRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvRhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvRxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvSBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvSRS8EemsgZOghPO5Pg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvShS8EemsgZOghPO5Pg" name="B6JFNB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvSxS8EemsgZOghPO5Pg" value="B6JFNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvTBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvTRS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvThS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvTxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvUBS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvURS8EemsgZOghPO5Pg" name="B6HECD" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvUhS8EemsgZOghPO5Pg" value="B6HECD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvUxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvVBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvVRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvVhS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvVxS8EemsgZOghPO5Pg" name="B6HLCD" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvWBS8EemsgZOghPO5Pg" value="B6HLCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvWRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvWhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvWxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvXBS8EemsgZOghPO5Pg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvXRS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvXhS8EemsgZOghPO5Pg" name="B6LIST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvXxS8EemsgZOghPO5Pg" value="B6LIST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvYBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvYRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvYhS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvYxS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvZBS8EemsgZOghPO5Pg" name="B6BCCD" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvZRS8EemsgZOghPO5Pg" value="B6BCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvZhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvZxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvaBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvaRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvahS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvaxS8EemsgZOghPO5Pg" name="B6DDS1" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvbBS8EemsgZOghPO5Pg" value="B6DDS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvbRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvbhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvbxS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvcBS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvcRS8EemsgZOghPO5Pg" name="B6JGNB" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvchS8EemsgZOghPO5Pg" value="B6JGNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvcxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvdBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvdRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvdhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvdxS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSveBS8EemsgZOghPO5Pg" name="B6DES1" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSveRS8EemsgZOghPO5Pg" value="B6DES1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvehS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvexS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvfBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvfRS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvfhS8EemsgZOghPO5Pg" name="B6JHNB" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvfxS8EemsgZOghPO5Pg" value="B6JHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvgBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvgRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvghS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvgxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvhBS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvhRS8EemsgZOghPO5Pg" name="B6DFS1" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvhhS8EemsgZOghPO5Pg" value="B6DFS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvhxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSviBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSviRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvihS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvixS8EemsgZOghPO5Pg" name="B6DGS1" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvjBS8EemsgZOghPO5Pg" value="B6DGS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvjRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvjhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvjxS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvkBS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvkRS8EemsgZOghPO5Pg" name="B6JJC1" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvkhS8EemsgZOghPO5Pg" value="B6JJC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvkxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvlBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvlRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvlhS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvlxS8EemsgZOghPO5Pg" name="B6QSDT" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvmBS8EemsgZOghPO5Pg" value="B6QSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvmRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvmhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvmxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvnBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvnRS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvnhS8EemsgZOghPO5Pg" name="B6QTDT" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvnxS8EemsgZOghPO5Pg" value="B6QTDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvoBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvoRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvohS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvoxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvpBS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvpRS8EemsgZOghPO5Pg" name="B6RXP2" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvphS8EemsgZOghPO5Pg" value="B6RXP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvpxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvqBS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvqRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvqhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvqxS8EemsgZOghPO5Pg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvrBS8EemsgZOghPO5Pg" name="B6RYP2" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvrRS8EemsgZOghPO5Pg" value="B6RYP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvrhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pwSvrxS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvsBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvsRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvshS8EemsgZOghPO5Pg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvsxS8EemsgZOghPO5Pg" name="B6W6S2" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvtBS8EemsgZOghPO5Pg" value="B6W6S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvtRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvthS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvtxS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvuBS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pwSvuRS8EemsgZOghPO5Pg" name="B6W7S2" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_pwSvuhS8EemsgZOghPO5Pg" value="B6W7S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pwSvuxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pwSvvBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pwSvvRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pwSvvhS8EemsgZOghPO5Pg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_pkOCARS8EemsgZOghPO5Pg" name="ORB7REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_pkOCAhS8EemsgZOghPO5Pg" value="ORB7REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_pkOCAxS8EemsgZOghPO5Pg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_psBTIBS8EemsgZOghPO5Pg" name="B7AMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTIRS8EemsgZOghPO5Pg" value="B7AMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTIhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTIxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTJBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTJRS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTJhS8EemsgZOghPO5Pg" name="B7BTCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTJxS8EemsgZOghPO5Pg" value="B7BTCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTKBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTKRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTKhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTKxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTLBS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTLRS8EemsgZOghPO5Pg" name="B7CQST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTLhS8EemsgZOghPO5Pg" value="B7CQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTLxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTMBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTMRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTMhS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTMxS8EemsgZOghPO5Pg" name="B7PMST" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTNBS8EemsgZOghPO5Pg" value="B7PMST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTNRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTNhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTNxS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTOBS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTORS8EemsgZOghPO5Pg" name="B7ATPR" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTOhS8EemsgZOghPO5Pg" value="B7ATPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTOxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTPBS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTPRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTPhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTPxS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTQBS8EemsgZOghPO5Pg" name="B7JLP2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTQRS8EemsgZOghPO5Pg" value="B7JLP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTQhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTQxS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTRBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTRRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTRhS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTRxS8EemsgZOghPO5Pg" name="B7ASPR" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTSBS8EemsgZOghPO5Pg" value="B7ASPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTSRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTShS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTSxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTTBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTTRS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTThS8EemsgZOghPO5Pg" name="B7JYP2" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTTxS8EemsgZOghPO5Pg" value="B7JYP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTUBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTURS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTUhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTUxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTVBS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTVRS8EemsgZOghPO5Pg" name="B7K8PR" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTVhS8EemsgZOghPO5Pg" value="B7K8PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTVxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTWBS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTWRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTWhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTWxS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTXBS8EemsgZOghPO5Pg" name="B7HCP1" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTXRS8EemsgZOghPO5Pg" value="B7HCP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTXhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTXxS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTYBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTYRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTYhS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTYxS8EemsgZOghPO5Pg" name="B7HDP1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTZBS8EemsgZOghPO5Pg" value="B7HDP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTZRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTZhS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTZxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTaBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTaRS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTahS8EemsgZOghPO5Pg" name="B7HEP1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTaxS8EemsgZOghPO5Pg" value="B7HEP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTbBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTbRS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTbhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTbxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTcBS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTcRS8EemsgZOghPO5Pg" name="B7JMP2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTchS8EemsgZOghPO5Pg" value="B7JMP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTcxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTdBS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTdRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTdhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTdxS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTeBS8EemsgZOghPO5Pg" name="B7C3PC" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTeRS8EemsgZOghPO5Pg" value="B7C3PC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTehS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTexS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTfBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTfRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTfhS8EemsgZOghPO5Pg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTfxS8EemsgZOghPO5Pg" name="B7LDST" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTgBS8EemsgZOghPO5Pg" value="B7LDST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTgRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTghS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTgxS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBThBS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBThRS8EemsgZOghPO5Pg" name="B7K7ST" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBThhS8EemsgZOghPO5Pg" value="B7K7ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBThxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTiBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTiRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTihS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTixS8EemsgZOghPO5Pg" name="B7HHNB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTjBS8EemsgZOghPO5Pg" value="B7HHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTjRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTjhS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTjxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTkBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTkRS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTkhS8EemsgZOghPO5Pg" name="B7CSST" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTkxS8EemsgZOghPO5Pg" value="B7CSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTlBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTlRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTlhS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTlxS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTmBS8EemsgZOghPO5Pg" name="B7K8ST" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTmRS8EemsgZOghPO5Pg" value="B7K8ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTmhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTmxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTnBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTnRS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTnhS8EemsgZOghPO5Pg" name="B7LEST" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTnxS8EemsgZOghPO5Pg" value="B7LEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBToBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBToRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTohS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBToxS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTpBS8EemsgZOghPO5Pg" name="B7K9ST" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTpRS8EemsgZOghPO5Pg" value="B7K9ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTphS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTpxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTqBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTqRS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTqhS8EemsgZOghPO5Pg" name="B7E0DT" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTqxS8EemsgZOghPO5Pg" value="B7E0DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTrBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTrRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTrhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTrxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTsBS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTsRS8EemsgZOghPO5Pg" name="B7ASCD" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTshS8EemsgZOghPO5Pg" value="B7ASCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTsxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTtBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTtRS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTthS8EemsgZOghPO5Pg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTtxS8EemsgZOghPO5Pg" name="B7E1DT" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTuBS8EemsgZOghPO5Pg" value="B7E1DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTuRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTuhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTuxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTvBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTvRS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTvhS8EemsgZOghPO5Pg" name="B7LBST" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTvxS8EemsgZOghPO5Pg" value="B7LBST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTwBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTwRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTwhS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTwxS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTxBS8EemsgZOghPO5Pg" name="B7LCST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTxRS8EemsgZOghPO5Pg" value="B7LCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTxhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTxxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTyBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBTyRS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBTyhS8EemsgZOghPO5Pg" name="B7FTNB" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBTyxS8EemsgZOghPO5Pg" value="B7FTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBTzBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBTzRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBTzhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBTzxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBT0BS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBT0RS8EemsgZOghPO5Pg" name="B7AZCD" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBT0hS8EemsgZOghPO5Pg" value="B7AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBT0xS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBT1BS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBT1RS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBT1hS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBT1xS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBT2BS8EemsgZOghPO5Pg" name="B7JENB" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBT2RS8EemsgZOghPO5Pg" value="B7JENB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBT2hS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBT2xS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBT3BS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBT3RS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBT3hS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBT3xS8EemsgZOghPO5Pg" name="B7NPTX" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBT4BS8EemsgZOghPO5Pg" value="B7NPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBT4RS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBT4hS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBT4xS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBT5BS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBT5RS8EemsgZOghPO5Pg" name="B7BCCD" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBT5hS8EemsgZOghPO5Pg" value="B7BCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBT5xS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBT6BS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBT6RS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBT6hS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBT6xS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBT7BS8EemsgZOghPO5Pg" name="B7GHCD" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBT7RS8EemsgZOghPO5Pg" value="B7GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBT7hS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBT7xS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBT8BS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBT8RS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBT8hS8EemsgZOghPO5Pg" name="B7AQCD" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBT8xS8EemsgZOghPO5Pg" value="B7AQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBT9BS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBT9RS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBT9hS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBT9xS8EemsgZOghPO5Pg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBT-BS8EemsgZOghPO5Pg" name="B7BHCD" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBT-RS8EemsgZOghPO5Pg" value="B7BHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBT-hS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBT-xS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBT_BS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBT_RS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBT_hS8EemsgZOghPO5Pg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBT_xS8EemsgZOghPO5Pg" name="B7B1CD" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBUABS8EemsgZOghPO5Pg" value="B7B1CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBUARS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBUAhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBUAxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBUBBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBUBRS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psBUBhS8EemsgZOghPO5Pg" name="B7GYCD" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_psBUBxS8EemsgZOghPO5Pg" value="B7GYCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psBUCBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psBUCRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psBUChS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psBUCxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psBUDBS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEIBS8EemsgZOghPO5Pg" name="B7BWPC" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEIRS8EemsgZOghPO5Pg" value="B7BWPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEIhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psLEIxS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEJBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEJRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEJhS8EemsgZOghPO5Pg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEJxS8EemsgZOghPO5Pg" name="B7BXPC" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEKBS8EemsgZOghPO5Pg" value="B7BXPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEKRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psLEKhS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEKxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLELBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLELRS8EemsgZOghPO5Pg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLELhS8EemsgZOghPO5Pg" name="B7ADCD" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLELxS8EemsgZOghPO5Pg" value="B7ADCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEMBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEMRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEMhS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEMxS8EemsgZOghPO5Pg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLENBS8EemsgZOghPO5Pg" name="B7J2ST" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLENRS8EemsgZOghPO5Pg" value="B7J2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLENhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLENxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEOBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEORS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEOhS8EemsgZOghPO5Pg" name="B7D0S1" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEOxS8EemsgZOghPO5Pg" value="B7D0S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEPBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEPRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEPhS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEPxS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEQBS8EemsgZOghPO5Pg" name="B7PNST" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEQRS8EemsgZOghPO5Pg" value="B7PNST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEQhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psLEQxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLERBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLERRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLERhS8EemsgZOghPO5Pg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLERxS8EemsgZOghPO5Pg" name="B7D1S1" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLESBS8EemsgZOghPO5Pg" value="B7D1S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLESRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEShS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLESxS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLETBS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLETRS8EemsgZOghPO5Pg" name="B7LHS2" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEThS8EemsgZOghPO5Pg" value="B7LHS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLETxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEUBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEURS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEUhS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEUxS8EemsgZOghPO5Pg" name="B7X0CD" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEVBS8EemsgZOghPO5Pg" value="B7X0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEVRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEVhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEVxS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEWBS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEWRS8EemsgZOghPO5Pg" name="B7AADT" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEWhS8EemsgZOghPO5Pg" value="B7AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEWxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psLEXBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEXRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEXhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEXxS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEYBS8EemsgZOghPO5Pg" name="B7AATX" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEYRS8EemsgZOghPO5Pg" value="B7AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEYhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEYxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEZBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEZRS8EemsgZOghPO5Pg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEZhS8EemsgZOghPO5Pg" name="B7LBTX" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEZxS8EemsgZOghPO5Pg" value="B7LBTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEaBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEaRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEahS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEaxS8EemsgZOghPO5Pg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEbBS8EemsgZOghPO5Pg" name="B7QYDT" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEbRS8EemsgZOghPO5Pg" value="B7QYDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEbhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psLEbxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEcBS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEcRS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEchS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEcxS8EemsgZOghPO5Pg" name="B7QZDT" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEdBS8EemsgZOghPO5Pg" value="B7QZDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEdRS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psLEdhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEdxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEeBS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEeRS8EemsgZOghPO5Pg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEehS8EemsgZOghPO5Pg" name="B7R3P2" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEexS8EemsgZOghPO5Pg" value="B7R3P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEfBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psLEfRS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEfhS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEfxS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEgBS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEgRS8EemsgZOghPO5Pg" name="B7R4P2" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEghS8EemsgZOghPO5Pg" value="B7R4P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEgxS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_psLEhBS8EemsgZOghPO5Pg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEhRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEhhS8EemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEhxS8EemsgZOghPO5Pg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEiBS8EemsgZOghPO5Pg" name="B7XCS2" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEiRS8EemsgZOghPO5Pg" value="B7XCS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEihS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEixS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEjBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEjRS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLEjhS8EemsgZOghPO5Pg" name="B7XDS2" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLEjxS8EemsgZOghPO5Pg" value="B7XDS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLEkBS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLEkRS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEkhS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEkxS8EemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_psLElBS8EemsgZOghPO5Pg" name="B7WST1" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_psLElRS8EemsgZOghPO5Pg" value="B7WST1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_psLElhS8EemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_psLElxS8EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_psLEmBS8EemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_psLEmRS8EemsgZOghPO5Pg" value="35"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_pYgl0SOlEemYGNC7g14LyQ" name="OSNCREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_pYgl0iOlEemYGNC7g14LyQ" value="OSNCREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_pYgl0yOlEemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_pYzgwCOlEemYGNC7g14LyQ" name="NCAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzgwSOlEemYGNC7g14LyQ" value="NCAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzgwiOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYzgwyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzgxCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzgxSOlEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzgxiOlEemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzgxyOlEemYGNC7g14LyQ" name="NCYCS2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzgyCOlEemYGNC7g14LyQ" value="NCYCS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzgySOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzgyiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzgyyOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzgzCOlEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzgzSOlEemYGNC7g14LyQ" name="NCYMS2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzgziOlEemYGNC7g14LyQ" value="NCYMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzgzyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzg0COlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzg0SOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzg0iOlEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzg0yOlEemYGNC7g14LyQ" name="NCYDS2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzg1COlEemYGNC7g14LyQ" value="NCYDS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzg1SOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzg1iOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzg1yOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzg2COlEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzg2SOlEemYGNC7g14LyQ" name="NCRSDT" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzg2iOlEemYGNC7g14LyQ" value="NCRSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzg2yOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYzg3COlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzg3SOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzg3iOlEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzg3yOlEemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzg4COlEemYGNC7g14LyQ" name="NCYES2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzg4SOlEemYGNC7g14LyQ" value="NCYES2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzg4iOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzg4yOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzg5COlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzg5SOlEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzg5iOlEemYGNC7g14LyQ" name="NCRTDT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzg5yOlEemYGNC7g14LyQ" value="NCRTDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzg6COlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYzg6SOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzg6iOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzg6yOlEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzg7COlEemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzg7SOlEemYGNC7g14LyQ" name="NCZ5N5" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzg7iOlEemYGNC7g14LyQ" value="NCZ5N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzg7yOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYzg8COlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzg8SOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzg8iOlEemYGNC7g14LyQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzg8yOlEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzg9COlEemYGNC7g14LyQ" name="NCRUDT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzg9SOlEemYGNC7g14LyQ" value="NCRUDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzg9iOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYzg9yOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzg-COlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzg-SOlEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzg-iOlEemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzg-yOlEemYGNC7g14LyQ" name="NCZ6N5" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzg_COlEemYGNC7g14LyQ" value="NCZ6N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzg_SOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYzg_iOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzg_yOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzhACOlEemYGNC7g14LyQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzhASOlEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pYzhAiOlEemYGNC7g14LyQ" name="NCRVDT" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_pYzhAyOlEemYGNC7g14LyQ" value="NCRVDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pYzhBCOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pYzhBSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pYzhBiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pYzhByOlEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pYzhCCOlEemYGNC7g14LyQ" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_o6RxsSOlEemYGNC7g14LyQ" name="ORAKREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_o6RxsiOlEemYGNC7g14LyQ" value="ORAKREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_o6RxsyOlEemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_pB1LMCOlEemYGNC7g14LyQ" name="AKAKCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_pB1LMSOlEemYGNC7g14LyQ" value="AKAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pB1LMiOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pB1LMyOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pB1LNCOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pB1LNSOlEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pB1LNiOlEemYGNC7g14LyQ" name="AKAGTX" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_pB1LNyOlEemYGNC7g14LyQ" value="AKAGTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pB1LOCOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pB1LOSOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pB1LOiOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pB1LOyOlEemYGNC7g14LyQ" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pB1LPCOlEemYGNC7g14LyQ" name="AKAGST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_pB1LPSOlEemYGNC7g14LyQ" value="AKAGST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pB1LPiOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pB1LPyOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pB1LQCOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pB1LQSOlEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pB1LQiOlEemYGNC7g14LyQ" name="AKNSST" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_pB1LQyOlEemYGNC7g14LyQ" value="AKNSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pB1LRCOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pB1LRSOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pB1LRiOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pB1LRyOlEemYGNC7g14LyQ" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_pW5cQSOlEemYGNC7g14LyQ" name="ORGQREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_pW5cQiOlEemYGNC7g14LyQ" value="ORGQREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_pW5cQyOlEemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_pXMXMCOlEemYGNC7g14LyQ" name="GQFWNB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXMSOlEemYGNC7g14LyQ" value="GQFWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXMiOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pXMXMyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXNCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXNSOlEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXNiOlEemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXNyOlEemYGNC7g14LyQ" name="GQAKCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXOCOlEemYGNC7g14LyQ" value="GQAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXOSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXOiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXOyOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXPCOlEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXPSOlEemYGNC7g14LyQ" name="GQBDCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXPiOlEemYGNC7g14LyQ" value="GQBDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXPyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXQCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXQSOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXQiOlEemYGNC7g14LyQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXQyOlEemYGNC7g14LyQ" name="GQFOTX" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXRCOlEemYGNC7g14LyQ" value="GQFOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXRSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXRiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXRyOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXSCOlEemYGNC7g14LyQ" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXSSOlEemYGNC7g14LyQ" name="GQFQTX" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXSiOlEemYGNC7g14LyQ" value="GQFQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXSyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXTCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXTSOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXTiOlEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXTyOlEemYGNC7g14LyQ" name="GQFRTX" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXUCOlEemYGNC7g14LyQ" value="GQFRTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXUSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXUiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXUyOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXVCOlEemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXVSOlEemYGNC7g14LyQ" name="GQHUNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXViOlEemYGNC7g14LyQ" value="GQHUNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXVyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXWCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXWSOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXWiOlEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXWyOlEemYGNC7g14LyQ" name="GQOVST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXXCOlEemYGNC7g14LyQ" value="GQOVST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXXSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXXiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXXyOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXYCOlEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXYSOlEemYGNC7g14LyQ" name="GQDQN3" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXYiOlEemYGNC7g14LyQ" value="GQDQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXYyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXZCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXZSOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXZiOlEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXZyOlEemYGNC7g14LyQ" name="GQDRN3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXaCOlEemYGNC7g14LyQ" value="GQDRN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXaSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXaiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXayOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXbCOlEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXbSOlEemYGNC7g14LyQ" name="GQKINA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXbiOlEemYGNC7g14LyQ" value="GQKINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXbyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXcCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXcSOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXciOlEemYGNC7g14LyQ" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXcyOlEemYGNC7g14LyQ" name="GQKJNA" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXdCOlEemYGNC7g14LyQ" value="GQKJNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXdSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXdiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXdyOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXeCOlEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXeSOlEemYGNC7g14LyQ" name="GQMKTX" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXeiOlEemYGNC7g14LyQ" value="GQMKTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXeyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXfCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXfSOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXfiOlEemYGNC7g14LyQ" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXfyOlEemYGNC7g14LyQ" name="GQMLTX" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXgCOlEemYGNC7g14LyQ" value="GQMLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXgSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXgiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXgyOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXhCOlEemYGNC7g14LyQ" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXhSOlEemYGNC7g14LyQ" name="GQMMTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXhiOlEemYGNC7g14LyQ" value="GQMMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXhyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXiCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXiSOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXiiOlEemYGNC7g14LyQ" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXMXiyOlEemYGNC7g14LyQ" name="GQMNTX" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXMXjCOlEemYGNC7g14LyQ" value="GQMNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXMXjSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXMXjiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXMXjyOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXMXkCOlEemYGNC7g14LyQ" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXVhICOlEemYGNC7g14LyQ" name="GQN8CD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXVhISOlEemYGNC7g14LyQ" value="GQN8CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXVhIiOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXVhIyOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXVhJCOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXVhJSOlEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pXVhJiOlEemYGNC7g14LyQ" name="GQN9CD" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_pXVhJyOlEemYGNC7g14LyQ" value="GQN9CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pXVhKCOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pXVhKSOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pXVhKiOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pXVhKyOlEemYGNC7g14LyQ" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_pXjjkSOlEemYGNC7g14LyQ" name="OSNEREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_pXjjkiOlEemYGNC7g14LyQ" value="OSNEREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_pXjjkyOlEemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_pX2egCOlEemYGNC7g14LyQ" name="NEZ7N5" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_pX2egSOlEemYGNC7g14LyQ" value="NEZ7N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pX2egiOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_pX2egyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pX2ehCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pX2ehSOlEemYGNC7g14LyQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pX2ehiOlEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pX2ehyOlEemYGNC7g14LyQ" name="NEYFS2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_pX2eiCOlEemYGNC7g14LyQ" value="NEYFS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pX2eiSOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pX2eiiOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pX2eiyOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pX2ejCOlEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_pX2ejSOlEemYGNC7g14LyQ" name="NEYGS2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_pX2ejiOlEemYGNC7g14LyQ" value="NEYGS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_pX2ejyOlEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_pX2ekCOlEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_pX2ekSOlEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_pX2ekiOlEemYGNC7g14LyQ" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7ESisSOzEemYGNC7g14LyQ" name="OSLZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7ESisiOzEemYGNC7g14LyQ" value="OSLZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7ESisyOzEemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_7EmEsCOzEemYGNC7g14LyQ" name="LZJKC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7EmEsSOzEemYGNC7g14LyQ" value="LZJKC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7EmEsiOzEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7EmEsyOzEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7EmEtCOzEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7EmEtSOzEemYGNC7g14LyQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7EmEtiOzEemYGNC7g14LyQ" name="LZJLC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7EmEtyOzEemYGNC7g14LyQ" value="LZJLC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7EmEuCOzEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7EmEuSOzEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7EmEuiOzEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7EmEuyOzEemYGNC7g14LyQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7EmEvCOzEemYGNC7g14LyQ" name="LZXGN5" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7EmEvSOzEemYGNC7g14LyQ" value="LZXGN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7EmEviOzEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7EmEvyOzEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7EmEwCOzEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7EmEwSOzEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7EmEwiOzEemYGNC7g14LyQ" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7EmEwyOzEemYGNC7g14LyQ" name="LZWUT1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7EmExCOzEemYGNC7g14LyQ" value="LZWUT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7EmExSOzEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7EmExiOzEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7EmExyOzEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7EmEyCOzEemYGNC7g14LyQ" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7EmEySOzEemYGNC7g14LyQ" name="LZWVT1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_7EmEyiOzEemYGNC7g14LyQ" value="LZWVT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7EmEyyOzEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7EmEzCOzEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7EmEzSOzEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7EmEziOzEemYGNC7g14LyQ" value="70"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_27AqASSAEemYGNC7g14LyQ" name="OSMVCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_27AqAiSAEemYGNC7g14LyQ" value="OSMVCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_27AqAySAEemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_3C9sICSAEemYGNC7g14LyQ" name="MVAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sISSAEemYGNC7g14LyQ" value="MVAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sIiSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3C9sIySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sJCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sJSSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sJiSAEemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sJySAEemYGNC7g14LyQ" name="MVGHCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sKCSAEemYGNC7g14LyQ" value="MVGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sKSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sKiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sKySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sLCSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sLSSAEemYGNC7g14LyQ" name="MVJ6C1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sLiSAEemYGNC7g14LyQ" value="MVJ6C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sLySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sMCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sMSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sMiSAEemYGNC7g14LyQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sMySAEemYGNC7g14LyQ" name="MVLQN3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sNCSAEemYGNC7g14LyQ" value="MVLQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sNSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sNiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sNySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sOCSAEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sOSSAEemYGNC7g14LyQ" name="MVJZC1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sOiSAEemYGNC7g14LyQ" value="MVJZC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sOySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sPCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sPSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sPiSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sPySAEemYGNC7g14LyQ" name="MVJ0C1" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sQCSAEemYGNC7g14LyQ" value="MVJ0C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sQSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sQiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sQySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sRCSAEemYGNC7g14LyQ" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sRSSAEemYGNC7g14LyQ" name="MVYAT1" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sRiSAEemYGNC7g14LyQ" value="MVYAT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sRySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sSCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sSSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sSiSAEemYGNC7g14LyQ" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sSySAEemYGNC7g14LyQ" name="MVYBT1" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sTCSAEemYGNC7g14LyQ" value="MVYBT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sTSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sTiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sTySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sUCSAEemYGNC7g14LyQ" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sUSSAEemYGNC7g14LyQ" name="MVYCT1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sUiSAEemYGNC7g14LyQ" value="MVYCT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sUySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sVCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sVSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sViSAEemYGNC7g14LyQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sVySAEemYGNC7g14LyQ" name="MVYDT1" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sWCSAEemYGNC7g14LyQ" value="MVYDT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sWSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sWiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sWySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sXCSAEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sXSSAEemYGNC7g14LyQ" name="MVYET1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sXiSAEemYGNC7g14LyQ" value="MVYET1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sXySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sYCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sYSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sYiSAEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sYySAEemYGNC7g14LyQ" name="MVYFT1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sZCSAEemYGNC7g14LyQ" value="MVYFT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sZSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sZiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sZySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9saCSAEemYGNC7g14LyQ" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9saSSAEemYGNC7g14LyQ" name="MVYGT1" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9saiSAEemYGNC7g14LyQ" value="MVYGT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9saySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sbCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sbSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sbiSAEemYGNC7g14LyQ" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sbySAEemYGNC7g14LyQ" name="MVZ1N5" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9scCSAEemYGNC7g14LyQ" value="MVZ1N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9scSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3C9sciSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9scySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sdCSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sdSSAEemYGNC7g14LyQ" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sdiSAEemYGNC7g14LyQ" name="MVZ2N5" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sdySAEemYGNC7g14LyQ" value="MVZ2N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9seCSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3C9seSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9seiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9seySAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sfCSAEemYGNC7g14LyQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sfSSAEemYGNC7g14LyQ" name="MVYAS2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sfiSAEemYGNC7g14LyQ" value="MVYAS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sfySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sgCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sgSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sgiSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sgySAEemYGNC7g14LyQ" name="MVYHT1" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9shCSAEemYGNC7g14LyQ" value="MVYHT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9shSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9shiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9shySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9siCSAEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9siSSAEemYGNC7g14LyQ" name="MVYIT1" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9siiSAEemYGNC7g14LyQ" value="MVYIT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9siySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sjCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sjSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sjiSAEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sjySAEemYGNC7g14LyQ" name="MVYJT1" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9skCSAEemYGNC7g14LyQ" value="MVYJT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9skSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9skiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9skySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9slCSAEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9slSSAEemYGNC7g14LyQ" name="MVYKT1" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sliSAEemYGNC7g14LyQ" value="MVYKT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9slySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9smCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9smSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9smiSAEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9smySAEemYGNC7g14LyQ" name="MVYLT1" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9snCSAEemYGNC7g14LyQ" value="MVYLT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9snSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sniSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9snySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9soCSAEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9soSSAEemYGNC7g14LyQ" name="MVYMT1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9soiSAEemYGNC7g14LyQ" value="MVYMT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9soySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9spCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9spSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9spiSAEemYGNC7g14LyQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9spySAEemYGNC7g14LyQ" name="MVYNT1" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sqCSAEemYGNC7g14LyQ" value="MVYNT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sqSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sqiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sqySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9srCSAEemYGNC7g14LyQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9srSSAEemYGNC7g14LyQ" name="MVYOT1" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sriSAEemYGNC7g14LyQ" value="MVYOT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9srySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9ssCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9ssSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9ssiSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9ssySAEemYGNC7g14LyQ" name="MVYPT1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9stCSAEemYGNC7g14LyQ" value="MVYPT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9stSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9stiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9stySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9suCSAEemYGNC7g14LyQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9suSSAEemYGNC7g14LyQ" name="MVYQT1" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9suiSAEemYGNC7g14LyQ" value="MVYQT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9suySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9svCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9svSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sviSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9svySAEemYGNC7g14LyQ" name="MVYRT1" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9swCSAEemYGNC7g14LyQ" value="MVYRT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9swSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9swiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9swySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9sxCSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9sxSSAEemYGNC7g14LyQ" name="MVYST1" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9sxiSAEemYGNC7g14LyQ" value="MVYST1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9sxySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9syCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9sySSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9syiSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9syySAEemYGNC7g14LyQ" name="MVYTT1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9szCSAEemYGNC7g14LyQ" value="MVYTT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9szSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9sziSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9szySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9s0CSAEemYGNC7g14LyQ" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9s0SSAEemYGNC7g14LyQ" name="MVYUT1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9s0iSAEemYGNC7g14LyQ" value="MVYUT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9s0ySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9s1CSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9s1SSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9s1iSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9s1ySAEemYGNC7g14LyQ" name="MVYVT1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9s2CSAEemYGNC7g14LyQ" value="MVYVT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9s2SSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9s2iSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9s2ySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9s3CSAEemYGNC7g14LyQ" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9s3SSAEemYGNC7g14LyQ" name="MVYWT1" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9s3iSAEemYGNC7g14LyQ" value="MVYWT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9s3ySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9s4CSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9s4SSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9s4iSAEemYGNC7g14LyQ" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9s4ySAEemYGNC7g14LyQ" name="MVYXT1" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9s5CSAEemYGNC7g14LyQ" value="MVYXT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9s5SSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9s5iSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9s5ySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9s6CSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9s6SSAEemYGNC7g14LyQ" name="MVY2T1" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9s6iSAEemYGNC7g14LyQ" value="MVY2T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9s6ySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9s7CSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9s7SSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9s7iSAEemYGNC7g14LyQ" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9s7ySAEemYGNC7g14LyQ" name="MVY3T1" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9s8CSAEemYGNC7g14LyQ" value="MVY3T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9s8SSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9s8iSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9s8ySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9s9CSAEemYGNC7g14LyQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9s9SSAEemYGNC7g14LyQ" name="MVY4T1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9s9iSAEemYGNC7g14LyQ" value="MVY4T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9s9ySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9s-CSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9s-SSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9s-iSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3C9s-ySAEemYGNC7g14LyQ" name="MVY5T1" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_3C9s_CSAEemYGNC7g14LyQ" value="MVY5T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3C9s_SSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3C9s_iSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3C9s_ySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3C9tACSAEemYGNC7g14LyQ" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdICSAEemYGNC7g14LyQ" name="MVY6T1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdISSAEemYGNC7g14LyQ" value="MVY6T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdIiSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdIySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdJCSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdJSSAEemYGNC7g14LyQ" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdJiSAEemYGNC7g14LyQ" name="MVYYT1" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdJySAEemYGNC7g14LyQ" value="MVYYT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdKCSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdKSSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdKiSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdKySAEemYGNC7g14LyQ" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdLCSAEemYGNC7g14LyQ" name="MVYZT1" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdLSSAEemYGNC7g14LyQ" value="MVYZT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdLiSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdLySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdMCSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdMSSAEemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdMiSAEemYGNC7g14LyQ" name="MVY0T1" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdMySAEemYGNC7g14LyQ" value="MVY0T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdNCSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdNSSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdNiSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdNySAEemYGNC7g14LyQ" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdOCSAEemYGNC7g14LyQ" name="MVY1T1" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdOSSAEemYGNC7g14LyQ" value="MVY1T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdOiSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdOySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdPCSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdPSSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdPiSAEemYGNC7g14LyQ" name="MVEHN6" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdPySAEemYGNC7g14LyQ" value="MVEHN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdQCSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3DHdQSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdQiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdQySAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdRCSAEemYGNC7g14LyQ" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdRSSAEemYGNC7g14LyQ" name="MVNPC1" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdRiSAEemYGNC7g14LyQ" value="MVNPC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdRySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdSCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdSSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdSiSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdSySAEemYGNC7g14LyQ" name="MVNQC1" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdTCSAEemYGNC7g14LyQ" value="MVNQC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdTSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdTiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdTySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdUCSAEemYGNC7g14LyQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdUSSAEemYGNC7g14LyQ" name="MVHHT2" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdUiSAEemYGNC7g14LyQ" value="MVHHT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdUySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdVCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdVSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdViSAEemYGNC7g14LyQ" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdVySAEemYGNC7g14LyQ" name="MVHIT2" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdWCSAEemYGNC7g14LyQ" value="MVHIT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdWSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdWiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdWySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdXCSAEemYGNC7g14LyQ" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3DHdXSSAEemYGNC7g14LyQ" name="MVHGT2" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_3DHdXiSAEemYGNC7g14LyQ" value="MVHGT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3DHdXySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3DHdYCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3DHdYSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3DHdYiSAEemYGNC7g14LyQ" value="76"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_3EumsSSAEemYGNC7g14LyQ" name="OSOZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_3EumsiSAEemYGNC7g14LyQ" value="OSOZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_3EumsySAEemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_3Fh38CSAEemYGNC7g14LyQ" name="OZDBC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh38SSAEemYGNC7g14LyQ" value="OZDBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh38iSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh38ySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh39CSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh39SSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh39iSAEemYGNC7g14LyQ" name="OZDCC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh39ySAEemYGNC7g14LyQ" value="OZDCC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh3-CSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh3-SSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh3-iSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh3-ySAEemYGNC7g14LyQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh3_CSAEemYGNC7g14LyQ" name="OZDDC1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh3_SSAEemYGNC7g14LyQ" value="OZDDC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh3_iSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh3_ySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4ACSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4ASSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4AiSAEemYGNC7g14LyQ" name="OZDEC1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4AySAEemYGNC7g14LyQ" value="OZDEC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4BCSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4BSSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4BiSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4BySAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4CCSAEemYGNC7g14LyQ" name="OZZKS2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4CSSAEemYGNC7g14LyQ" value="OZZKS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4CiSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4CySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4DCSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4DSSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4DiSAEemYGNC7g14LyQ" name="OZQ8S2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4DySAEemYGNC7g14LyQ" value="OZQ8S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4ECSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4ESSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4EiSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4EySAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4FCSAEemYGNC7g14LyQ" name="OZQ7S2" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4FSSAEemYGNC7g14LyQ" value="OZQ7S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4FiSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4FySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4GCSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4GSSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4GiSAEemYGNC7g14LyQ" name="OZDZN6" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4GySAEemYGNC7g14LyQ" value="OZDZN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4HCSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4HSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4HiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4HySAEemYGNC7g14LyQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4ICSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4ISSAEemYGNC7g14LyQ" name="OZDAC1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4IiSAEemYGNC7g14LyQ" value="OZDAC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4IySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4JCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4JSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4JiSAEemYGNC7g14LyQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4JySAEemYGNC7g14LyQ" name="OZZLS2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4KCSAEemYGNC7g14LyQ" value="OZZLS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4KSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4KiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4KySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4LCSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4LSSAEemYGNC7g14LyQ" name="OZM3C1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4LiSAEemYGNC7g14LyQ" value="OZM3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4LySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4MCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4MSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4MiSAEemYGNC7g14LyQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4MySAEemYGNC7g14LyQ" name="OZQ9S2" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4NCSAEemYGNC7g14LyQ" value="OZQ9S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4NSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4NiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4NySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4OCSAEemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4OSSAEemYGNC7g14LyQ" name="OZQ4T1" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4OiSAEemYGNC7g14LyQ" value="OZQ4T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4OySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4PCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4PSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4PiSAEemYGNC7g14LyQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4PySAEemYGNC7g14LyQ" name="OZQ5T1" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4QCSAEemYGNC7g14LyQ" value="OZQ5T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4QSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4QiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4QySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4RCSAEemYGNC7g14LyQ" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4RSSAEemYGNC7g14LyQ" name="OZGJT2" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4RiSAEemYGNC7g14LyQ" value="OZGJT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4RySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4SCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4SSSAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4SiSAEemYGNC7g14LyQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4SySAEemYGNC7g14LyQ" name="OZGKT2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4TCSAEemYGNC7g14LyQ" value="OZGKT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4TSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4TiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4TySAEemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4UCSAEemYGNC7g14LyQ" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4USSAEemYGNC7g14LyQ" name="OZD8VA" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4UiSAEemYGNC7g14LyQ" value="OZD8VA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4UySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4VCSAEemYGNC7g14LyQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4VSSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4ViSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4VySAEemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4WCSAEemYGNC7g14LyQ" name="OZD4N6" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4WSSAEemYGNC7g14LyQ" value="OZD4N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4WiSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4WySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4XCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4XSSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4XiSAEemYGNC7g14LyQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4XySAEemYGNC7g14LyQ" name="OZOWPC" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4YCSAEemYGNC7g14LyQ" value="OZOWPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4YSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4YiSAEemYGNC7g14LyQ" value="6"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4YySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4ZCSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4ZSSAEemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4ZiSAEemYGNC7g14LyQ" name="OZBCCD" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4ZySAEemYGNC7g14LyQ" value="OZBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4aCSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4aSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4aiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4aySAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4bCSAEemYGNC7g14LyQ" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4bSSAEemYGNC7g14LyQ" name="OZD9VA" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4biSAEemYGNC7g14LyQ" value="OZD9VA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4bySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4cCSAEemYGNC7g14LyQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4cSSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4ciSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4cySAEemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4dCSAEemYGNC7g14LyQ" name="OZEAVA" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4dSSAEemYGNC7g14LyQ" value="OZEAVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4diSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4dySAEemYGNC7g14LyQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4eCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4eSSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4eiSAEemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4eySAEemYGNC7g14LyQ" name="OZEBVA" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4fCSAEemYGNC7g14LyQ" value="OZEBVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4fSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4fiSAEemYGNC7g14LyQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4fySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4gCSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4gSSAEemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4giSAEemYGNC7g14LyQ" name="OZECVA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4gySAEemYGNC7g14LyQ" value="OZECVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4hCSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4hSSAEemYGNC7g14LyQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4hiSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4hySAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4iCSAEemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4iSSAEemYGNC7g14LyQ" name="OZEDVA" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4iiSAEemYGNC7g14LyQ" value="OZEDVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4iySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4jCSAEemYGNC7g14LyQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4jSSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4jiSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4jySAEemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4kCSAEemYGNC7g14LyQ" name="OZEEVA" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4kSSAEemYGNC7g14LyQ" value="OZEEVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4kiSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4kySAEemYGNC7g14LyQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4lCSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4lSSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4liSAEemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fh4lySAEemYGNC7g14LyQ" name="OZD2N6" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fh4mCSAEemYGNC7g14LyQ" value="OZD2N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fh4mSSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fh4miSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fh4mySAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fh4nCSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fh4nSSAEemYGNC7g14LyQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_3Fro8CSAEemYGNC7g14LyQ" name="OZD3N6" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_3Fro8SSAEemYGNC7g14LyQ" value="OZD3N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_3Fro8iSAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_3Fro8ySAEemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_3Fro9CSAEemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_3Fro9SSAEemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_3Fro9iSAEemYGNC7g14LyQ" value="5"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_9foIkSS3EemYGNC7g14LyQ" name="KITCLI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_9foIkiS3EemYGNC7g14LyQ" value="KITCLI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_9foIkyS3EemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_9gZkoCS3EemYGNC7g14LyQ" name="KCLNOM" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_9gZkoSS3EemYGNC7g14LyQ" value="KCLNOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9gZkoiS3EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9gZkoyS3EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9gZkpCS3EemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9gZkpSS3EemYGNC7g14LyQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9gZkpiS3EemYGNC7g14LyQ" name="KCLCLI" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_9gZkpyS3EemYGNC7g14LyQ" value="KCLCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9gZkqCS3EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9gZkqSS3EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9gZkqiS3EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9gZkqyS3EemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9gZkrCS3EemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9giukCS3EemYGNC7g14LyQ" name="KCLPRX" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_9giukSS3EemYGNC7g14LyQ" value="KCLPRX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9giukiS3EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9giukyS3EemYGNC7g14LyQ" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9giulCS3EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9giulSS3EemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9giuliS3EemYGNC7g14LyQ" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UbqXRJ5hEem6jcmdEfXK_g" name="KCLMOD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_UbqXRZ5hEem6jcmdEfXK_g" value="KCLMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UbqXRp5hEem6jcmdEfXK_g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UbqXR55hEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UbqXSJ5hEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UbqXSZ5hEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UbqXSp5hEem6jcmdEfXK_g" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_9QECkSS3EemYGNC7g14LyQ" name="KITDEF">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_9QECkiS3EemYGNC7g14LyQ" value="KITDEF"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_9QECkyS3EemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_9bTCMCS3EemYGNC7g14LyQ" name="KITNOM" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_9bTCMSS3EemYGNC7g14LyQ" value="KITNOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9bTCMiS3EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9bTCMyS3EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9bTCNCS3EemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9bTCNSS3EemYGNC7g14LyQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9bTCNiS3EemYGNC7g14LyQ" name="KITLIB" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_9bTCNyS3EemYGNC7g14LyQ" value="KITLIB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9bTCOCS3EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9bTCOSS3EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9bTCOiS3EemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9bTCOyS3EemYGNC7g14LyQ" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9bTCPCS3EemYGNC7g14LyQ" name="KITSUP" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_9bTCPSS3EemYGNC7g14LyQ" value="KITSUP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9bTCPiS3EemYGNC7g14LyQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9bTCPyS3EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9bTCQCS3EemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9bTCQSS3EemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9bTCQiS3EemYGNC7g14LyQ" name="KITMEL" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_9bTCQyS3EemYGNC7g14LyQ" value="KITMEL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9bTCRCS3EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9bTCRSS3EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9bTCRiS3EemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9bTCRyS3EemYGNC7g14LyQ" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UEGy6J5hEem6jcmdEfXK_g" name="KITMOD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_UEGy6Z5hEem6jcmdEfXK_g" value="KITMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UEGy6p5hEem6jcmdEfXK_g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UEGy655hEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UEGy7J5hEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UEGy7Z5hEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UEGy7p5hEem6jcmdEfXK_g" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_fx6bISS6EemYGNC7g14LyQ" name="KITART">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_fx6bIiS6EemYGNC7g14LyQ" value="KITART"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_fx6bIyS6EemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_fykicCS6EemYGNC7g14LyQ" name="KARNOM" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_fykicSS6EemYGNC7g14LyQ" value="KARNOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_fykiciS6EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_fykicyS6EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_fykidCS6EemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_fykidSS6EemYGNC7g14LyQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_fykidiS6EemYGNC7g14LyQ" name="KARART" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_fykidyS6EemYGNC7g14LyQ" value="KARART"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_fykieCS6EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_fykieSS6EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_fykieiS6EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_fykieyS6EemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_fykifCS6EemYGNC7g14LyQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_fykifSS6EemYGNC7g14LyQ" name="KARQTE" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_fykifiS6EemYGNC7g14LyQ" value="KARQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_fykifyS6EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_fykigCS6EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_fykigSS6EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_fykigiS6EemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_fykigyS6EemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_UY8aYJ5hEem6jcmdEfXK_g" name="KARMOD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_UY8aYZ5hEem6jcmdEfXK_g" value="KARMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_UY8aYp5hEem6jcmdEfXK_g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_UY8aY55hEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_UY8aZJ5hEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_UY8aZZ5hEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_UY8aZp5hEem6jcmdEfXK_g" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_zsANwSU4EemYGNC7g14LyQ" name="TRIGGER_PXMAMAVP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_zsANwiU4EemYGNC7g14LyQ" value="TRIGGER_PXMAMAVP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_zsANwyU4EemYGNC7g14LyQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_z0UcQCU4EemYGNC7g14LyQ" name="TMV_CODE_MERCURIALE_" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_z0UcQSU4EemYGNC7g14LyQ" value="TMV_CODE_MERCURIALE_"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z0UcQiU4EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z0UcQyU4EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z0UcRCU4EemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z0UcRSU4EemYGNC7g14LyQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_z0UcRiU4EemYGNC7g14LyQ" name="TMV_CODE_ARTICLE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_z0UcRyU4EemYGNC7g14LyQ" value="TMV_CODE_ARTICLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z0UcSCU4EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_z0UcSSU4EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z0UcSiU4EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z0UcSyU4EemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z0UcTCU4EemYGNC7g14LyQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_z0UcTSU4EemYGNC7g14LyQ" name="TMV_PRIX" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_z0UcTiU4EemYGNC7g14LyQ" value="TMV_PRIX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z0UcTyU4EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_z0UcUCU4EemYGNC7g14LyQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z0UcUSU4EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z0UcUiU4EemYGNC7g14LyQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z0UcUyU4EemYGNC7g14LyQ" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_z0UcVCU4EemYGNC7g14LyQ" name="TMV_TYPE_MODIFICATION_AMS" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_z0UcVSU4EemYGNC7g14LyQ" value="TMV_TYPE_MODIFICATION_AMS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z0UcViU4EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z0UcVyU4EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z0UcWCU4EemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z0UcWSU4EemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_z0UcWiU4EemYGNC7g14LyQ" name="TMV_TOP_TRAITE_O_N" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_z0UcWyU4EemYGNC7g14LyQ" value="TMV_TOP_TRAITE_O_N"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z0UcXCU4EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z0UcXSU4EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z0UcXiU4EemYGNC7g14LyQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z0UcXyU4EemYGNC7g14LyQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_z0UcYCU4EemYGNC7g14LyQ" name="TMV_DATE_HEURE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_z0UcYSU4EemYGNC7g14LyQ" value="TMV_DATE_HEURE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z0UcYiU4EemYGNC7g14LyQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_z0UcYyU4EemYGNC7g14LyQ" value="6"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z0UcZCU4EemYGNC7g14LyQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z0UcZSU4EemYGNC7g14LyQ" value="TIMESTAMP"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z0UcZiU4EemYGNC7g14LyQ" value="26"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="__4b9EChnEemDyPGH1D5Zjw" name="WDFAVCLP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="__4b9EShnEemDyPGH1D5Zjw" value="WDFAVCLP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="__4b9EihnEemDyPGH1D5Zjw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_AJTqoChoEemDyPGH1D5Zjw" name="FACREP" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTqoShoEemDyPGH1D5Zjw" value="FACREP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTqoihoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTqoyhoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTqpChoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTqpShoEemDyPGH1D5Zjw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTqpihoEemDyPGH1D5Zjw" name="FACSOC" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTqpyhoEemDyPGH1D5Zjw" value="FACSOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTqqChoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTqqShoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTqqihoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTqqyhoEemDyPGH1D5Zjw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTqrChoEemDyPGH1D5Zjw" name="FACMVT" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTqrShoEemDyPGH1D5Zjw" value="FACMVT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTqrihoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTqryhoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTqsChoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTqsShoEemDyPGH1D5Zjw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTqsihoEemDyPGH1D5Zjw" name="FACREG" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTqsyhoEemDyPGH1D5Zjw" value="FACREG"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTqtChoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTqtShoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTqtihoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTqtyhoEemDyPGH1D5Zjw" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTquChoEemDyPGH1D5Zjw" name="FACART" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTquShoEemDyPGH1D5Zjw" value="FACART"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTquihoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTquyhoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTqvChoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTqvShoEemDyPGH1D5Zjw" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTqvihoEemDyPGH1D5Zjw" name="FALART" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTqvyhoEemDyPGH1D5Zjw" value="FALART"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTqwChoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTqwShoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTqwihoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTqwyhoEemDyPGH1D5Zjw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTqxChoEemDyPGH1D5Zjw" name="FAQTEE" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTqxShoEemDyPGH1D5Zjw" value="FAQTEE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTqxihoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTqxyhoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTqyChoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTqyShoEemDyPGH1D5Zjw" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTqyihoEemDyPGH1D5Zjw" name="FAPUHT" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTqyyhoEemDyPGH1D5Zjw" value="FAPUHT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTqzChoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTqzShoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTqzihoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTqzyhoEemDyPGH1D5Zjw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTq0ChoEemDyPGH1D5Zjw" name="FATREM" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTq0ShoEemDyPGH1D5Zjw" value="FATREM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTq0ihoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTq0yhoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTq1ChoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTq1ShoEemDyPGH1D5Zjw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTq1ihoEemDyPGH1D5Zjw" name="FAREMI" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTq1yhoEemDyPGH1D5Zjw" value="FAREMI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTq2ChoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTq2ShoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTq2ihoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTq2yhoEemDyPGH1D5Zjw" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTq3ChoEemDyPGH1D5Zjw" name="FATPXN" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTq3ShoEemDyPGH1D5Zjw" value="FATPXN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTq3ihoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTq3yhoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTq4ChoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTq4ShoEemDyPGH1D5Zjw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTq4ihoEemDyPGH1D5Zjw" name="FAPOSI" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTq4yhoEemDyPGH1D5Zjw" value="FAPOSI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTq5ChoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTq5ShoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTq5ihoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTq5yhoEemDyPGH1D5Zjw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTq6ChoEemDyPGH1D5Zjw" name="FATGRA" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTq6ShoEemDyPGH1D5Zjw" value="FATGRA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTq6ihoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTq6yhoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTq7ChoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTq7ShoEemDyPGH1D5Zjw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTq7ihoEemDyPGH1D5Zjw" name="FADERC" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTq7yhoEemDyPGH1D5Zjw" value="FADERC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTq8ChoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTq8ShoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTq8ihoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTq8yhoEemDyPGH1D5Zjw" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTq9ChoEemDyPGH1D5Zjw" name="FAFREQ" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTq9ShoEemDyPGH1D5Zjw" value="FAFREQ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTq9ihoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTq9yhoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTq-ChoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTq-ShoEemDyPGH1D5Zjw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTq-ihoEemDyPGH1D5Zjw" name="FAQTEC" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTq-yhoEemDyPGH1D5Zjw" value="FAQTEC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTq_ChoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTq_ShoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTq_ihoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTq_yhoEemDyPGH1D5Zjw" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTrAChoEemDyPGH1D5Zjw" name="FALIGP" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTrAShoEemDyPGH1D5Zjw" value="FALIGP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTrAihoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AJTrAyhoEemDyPGH1D5Zjw" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTrBChoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTrBShoEemDyPGH1D5Zjw" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTrBihoEemDyPGH1D5Zjw" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTrByhoEemDyPGH1D5Zjw" name="FACOPE" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTrCChoEemDyPGH1D5Zjw" value="FACOPE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTrCShoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTrCihoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTrCyhoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTrDChoEemDyPGH1D5Zjw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTrDShoEemDyPGH1D5Zjw" name="FADATE" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTrDihoEemDyPGH1D5Zjw" value="FADATE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTrDyhoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTrEChoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTrEShoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTrEihoEemDyPGH1D5Zjw" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AJTrEyhoEemDyPGH1D5Zjw" name="FAHEUR" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_AJTrFChoEemDyPGH1D5Zjw" value="FAHEUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AJTrFShoEemDyPGH1D5Zjw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AJTrFihoEemDyPGH1D5Zjw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AJTrFyhoEemDyPGH1D5Zjw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AJTrGChoEemDyPGH1D5Zjw" value="6"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_AC46sCu3Eemv78OLPmYyMg" name="ORBQREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_AC46sSu3Eemv78OLPmYyMg" value="ORBQREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_AC46siu3Eemv78OLPmYyMg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_ATmQMCu3Eemv78OLPmYyMg" name="BQBOCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATmQMSu3Eemv78OLPmYyMg" value="BQBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATmQMiu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATmQMyu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATmQNCu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATmQNSu3Eemv78OLPmYyMg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATmQNiu3Eemv78OLPmYyMg" name="BQHRST" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATmQNyu3Eemv78OLPmYyMg" value="BQHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATmQOCu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATmQOSu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATmQOiu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATmQOyu3Eemv78OLPmYyMg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATmQPCu3Eemv78OLPmYyMg" name="BQAFCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATmQPSu3Eemv78OLPmYyMg" value="BQAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATmQPiu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATmQPyu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATmQQCu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATmQQSu3Eemv78OLPmYyMg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATmQQiu3Eemv78OLPmYyMg" name="BQAMCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATmQQyu3Eemv78OLPmYyMg" value="BQAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATmQRCu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATmQRSu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATmQRiu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATmQRyu3Eemv78OLPmYyMg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATmQSCu3Eemv78OLPmYyMg" name="BQGHCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATmQSSu3Eemv78OLPmYyMg" value="BQGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATmQSiu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATmQSyu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATmQTCu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATmQTSu3Eemv78OLPmYyMg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATwBMCu3Eemv78OLPmYyMg" name="BQJ4CD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATwBMSu3Eemv78OLPmYyMg" value="BQJ4CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATwBMiu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATwBMyu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATwBNCu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATwBNSu3Eemv78OLPmYyMg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATwBNiu3Eemv78OLPmYyMg" name="BQBYNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATwBNyu3Eemv78OLPmYyMg" value="BQBYNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATwBOCu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATwBOSu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATwBOiu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATwBOyu3Eemv78OLPmYyMg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATwBPCu3Eemv78OLPmYyMg" name="BQAZDT" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATwBPSu3Eemv78OLPmYyMg" value="BQAZDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATwBPiu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ATwBPyu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATwBQCu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATwBQSu3Eemv78OLPmYyMg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATwBQiu3Eemv78OLPmYyMg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATwBQyu3Eemv78OLPmYyMg" name="BQA0DT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATwBRCu3Eemv78OLPmYyMg" value="BQA0DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATwBRSu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ATwBRiu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATwBRyu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATwBSCu3Eemv78OLPmYyMg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATwBSSu3Eemv78OLPmYyMg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATwBSiu3Eemv78OLPmYyMg" name="BQBQST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATwBSyu3Eemv78OLPmYyMg" value="BQBQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATwBTCu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATwBTSu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATwBTiu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATwBTyu3Eemv78OLPmYyMg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATwBUCu3Eemv78OLPmYyMg" name="BQBRST" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATwBUSu3Eemv78OLPmYyMg" value="BQBRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATwBUiu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATwBUyu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATwBVCu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATwBVSu3Eemv78OLPmYyMg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATwBViu3Eemv78OLPmYyMg" name="BQAEQT" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATwBVyu3Eemv78OLPmYyMg" value="BQAEQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATwBWCu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ATwBWSu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATwBWiu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATwBWyu3Eemv78OLPmYyMg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATwBXCu3Eemv78OLPmYyMg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATwBXSu3Eemv78OLPmYyMg" name="BQDQDT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATwBXiu3Eemv78OLPmYyMg" value="BQDQDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATwBXyu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ATwBYCu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATwBYSu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATwBYiu3Eemv78OLPmYyMg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATwBYyu3Eemv78OLPmYyMg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ATwBZCu3Eemv78OLPmYyMg" name="BQDRDT" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_ATwBZSu3Eemv78OLPmYyMg" value="BQDRDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ATwBZiu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ATwBZyu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ATwBaCu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ATwBaSu3Eemv78OLPmYyMg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ATwBaiu3Eemv78OLPmYyMg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AT0SoCu3Eemv78OLPmYyMg" name="BQO1ST" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_AT0SoSu3Eemv78OLPmYyMg" value="BQO1ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AT0Soiu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AT0Soyu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AT0SpCu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AT0SpSu3Eemv78OLPmYyMg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AT0Spiu3Eemv78OLPmYyMg" name="BQD3C1" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_AT0Spyu3Eemv78OLPmYyMg" value="BQD3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AT0SqCu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AT0SqSu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AT0Sqiu3Eemv78OLPmYyMg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AT0Sqyu3Eemv78OLPmYyMg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AT0SrCu3Eemv78OLPmYyMg" name="BQBCCD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_AT0SrSu3Eemv78OLPmYyMg" value="BQBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AT0Sriu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AT0Sryu3Eemv78OLPmYyMg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AT0SsCu3Eemv78OLPmYyMg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AT0SsSu3Eemv78OLPmYyMg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AT0Ssiu3Eemv78OLPmYyMg" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rhACYS7UEemi8scpwjq3_w" name="CLIENTCD">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rhACYi7UEemi8scpwjq3_w" value="CLIENTCD"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rhACYy7UEemi8scpwjq3_w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rhgYsC7UEemi8scpwjq3_w" name="IDCWEB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgYsS7UEemi8scpwjq3_w" value="IDCWEB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgYsi7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgYsy7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgYtC7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgYtS7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgYti7UEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgYty7UEemi8scpwjq3_w" name="MFERME" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgYuC7UEemi8scpwjq3_w" value="MFERME"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgYuS7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgYui7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgYuy7UEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgYvC7UEemi8scpwjq3_w" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgYvS7UEemi8scpwjq3_w" name="PRENOM" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgYvi7UEemi8scpwjq3_w" value="PRENOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgYvy7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgYwC7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgYwS7UEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgYwi7UEemi8scpwjq3_w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgYwy7UEemi8scpwjq3_w" name="ANNIV" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgYxC7UEemi8scpwjq3_w" value="ANNIV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgYxS7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgYxi7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgYxy7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgYyC7UEemi8scpwjq3_w" value="DATE"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgYyS7UEemi8scpwjq3_w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgYyi7UEemi8scpwjq3_w" name="NEWSCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgYyy7UEemi8scpwjq3_w" value="NEWSCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgYzC7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgYzS7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgYzi7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgYzy7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgY0C7UEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgY0S7UEemi8scpwjq3_w" name="NEWSPA" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgY0i7UEemi8scpwjq3_w" value="NEWSPA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgY0y7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgY1C7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgY1S7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgY1i7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgY1y7UEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgY2C7UEemi8scpwjq3_w" name="NSIRET" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgY2S7UEemi8scpwjq3_w" value="NSIRET"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgY2i7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgY2y7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgY3C7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgY3S7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgY3i7UEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgY3y7UEemi8scpwjq3_w" name="BGROUP" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgY4C7UEemi8scpwjq3_w" value="BGROUP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgY4S7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgY4i7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgY4y7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgY5C7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgY5S7UEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgY5i7UEemi8scpwjq3_w" name="TYPGRP" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgY5y7UEemi8scpwjq3_w" value="TYPGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgY6C7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgY6S7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgY6i7UEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgY6y7UEemi8scpwjq3_w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgY7C7UEemi8scpwjq3_w" name="IDMARC" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgY7S7UEemi8scpwjq3_w" value="IDMARC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgY7i7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgY7y7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgY8C7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgY8S7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgY8i7UEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgY8y7UEemi8scpwjq3_w" name="IDACTI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgY9C7UEemi8scpwjq3_w" value="IDACTI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgY9S7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgY9i7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgY9y7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgY-C7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgY-S7UEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgY-i7UEemi8scpwjq3_w" name="IDDETA" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgY-y7UEemi8scpwjq3_w" value="IDDETA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgY_C7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgY_S7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgY_i7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgY_y7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgZAC7UEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgZAS7UEemi8scpwjq3_w" name="TOPMOD" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgZAi7UEemi8scpwjq3_w" value="TOPMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgZAy7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgZBC7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgZBS7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgZBi7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgZBy7UEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgZCC7UEemi8scpwjq3_w" name="NUMCLI" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgZCS7UEemi8scpwjq3_w" value="NUMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgZCi7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgZCy7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgZDC7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgZDS7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgZDi7UEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgZDy7UEemi8scpwjq3_w" name="TOPCWB" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgZEC7UEemi8scpwjq3_w" value="TOPCWB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgZES7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgZEi7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgZEy7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgZFC7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgZFS7UEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgZFi7UEemi8scpwjq3_w" name="TOPMWB" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgZFy7UEemi8scpwjq3_w" value="TOPMWB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgZGC7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhgZGS7UEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgZGi7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgZGy7UEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgZHC7UEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgZHS7UEemi8scpwjq3_w" name="TOPVWB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgZHi7UEemi8scpwjq3_w" value="TOPVWB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgZHy7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgZIC7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgZIS7UEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgZIi7UEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhgZIy7UEemi8scpwjq3_w" name="TOPCDI" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhgZJC7UEemi8scpwjq3_w" value="TOPCDI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhgZJS7UEemi8scpwjq3_w" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhgZJi7UEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhgZJy7UEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhgZKC7UEemi8scpwjq3_w" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_mpNG0S7xEemi8scpwjq3_w" name="WEBCTRL">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_mpNG0i7xEemi8scpwjq3_w" value="WEBCTRL"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_mpNG0y7xEemi8scpwjq3_w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_mpW30C7xEemi8scpwjq3_w" name="CLIPAY" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_mpW30S7xEemi8scpwjq3_w" value="CLIPAY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mpW30i7xEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_mpW30y7xEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mpW31C7xEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mpW31S7xEemi8scpwjq3_w" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mpW31i7xEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_mpW31y7xEemi8scpwjq3_w" name="CLICEN" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_mpW32C7xEemi8scpwjq3_w" value="CLICEN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mpW32S7xEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_mpW32i7xEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mpW32y7xEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mpW33C7xEemi8scpwjq3_w" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mpW33S7xEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_mpgo0C7xEemi8scpwjq3_w" name="CDEMAX" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_mpgo0S7xEemi8scpwjq3_w" value="CDEMAX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mpgo0i7xEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_mpgo0y7xEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mpgo1C7xEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mpgo1S7xEemi8scpwjq3_w" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mpgo1i7xEemi8scpwjq3_w" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_XuQNITAzEemi8scpwjq3_w" name="ELICLIP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_XuQNIjAzEemi8scpwjq3_w" value="ELICLIP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_XuQNIzAzEemi8scpwjq3_w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_XvDeYDAzEemi8scpwjq3_w" name="IDWEBC" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvDeYTAzEemi8scpwjq3_w" value="IDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvDeYjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvDeYzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvDeZDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvDeZTAzEemi8scpwjq3_w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvDeZjAzEemi8scpwjq3_w" name="CPTCLI" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvDeZzAzEemi8scpwjq3_w" value="CPTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvDeaDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvDeaTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvDeajAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvDeazAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvDebDAzEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvDebTAzEemi8scpwjq3_w" name="NOMCLI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvDebjAzEemi8scpwjq3_w" value="NOMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvDebzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvDecDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvDecTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvDecjAzEemi8scpwjq3_w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvDeczAzEemi8scpwjq3_w" name="PRECLI" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvDedDAzEemi8scpwjq3_w" value="PRECLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvDedTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvDedjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvDedzAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvDeeDAzEemi8scpwjq3_w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvDeeTAzEemi8scpwjq3_w" name="PWDCLI" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvDeejAzEemi8scpwjq3_w" value="PWDCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvDeezAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvDefDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvDefTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvDefjAzEemi8scpwjq3_w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPYDAzEemi8scpwjq3_w" name="MELADR" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPYTAzEemi8scpwjq3_w" value="MELADR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPYjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPYzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPZDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPZTAzEemi8scpwjq3_w" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPZjAzEemi8scpwjq3_w" name="MELSIT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPZzAzEemi8scpwjq3_w" value="MELSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPaDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPaTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPajAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPazAzEemi8scpwjq3_w" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPbDAzEemi8scpwjq3_w" name="NIVCLI" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPbTAzEemi8scpwjq3_w" value="NIVCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPbjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPbzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPcDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPcTAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPcjAzEemi8scpwjq3_w" name="ACTCLI" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPczAzEemi8scpwjq3_w" value="ACTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPdDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPdTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPdjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPdzAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPeDAzEemi8scpwjq3_w" name="TYPTAR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPeTAzEemi8scpwjq3_w" value="TYPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPejAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPezAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPfDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPfTAzEemi8scpwjq3_w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPfjAzEemi8scpwjq3_w" name="TXREMI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPfzAzEemi8scpwjq3_w" value="TXREMI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPgDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNPgTAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPgjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPgzAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPhDAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPhTAzEemi8scpwjq3_w" name="FRANCO" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPhjAzEemi8scpwjq3_w" value="FRANCO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPhzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNPiDAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPiTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPijAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPizAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPjDAzEemi8scpwjq3_w" name="FRPORT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPjTAzEemi8scpwjq3_w" value="FRPORT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPjjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNPjzAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPkDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPkTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPkjAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPkzAzEemi8scpwjq3_w" name="MINCDE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPlDAzEemi8scpwjq3_w" value="MINCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPlTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNPljAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPlzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPmDAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPmTAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPmjAzEemi8scpwjq3_w" name="MERCUR" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPmzAzEemi8scpwjq3_w" value="MERCUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPnDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPnTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPnjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPnzAzEemi8scpwjq3_w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPoDAzEemi8scpwjq3_w" name="CDECAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPoTAzEemi8scpwjq3_w" value="CDECAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPojAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPozAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPpDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPpTAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPpjAzEemi8scpwjq3_w" name="CDELST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPpzAzEemi8scpwjq3_w" value="CDELST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPqDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPqTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPqjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPqzAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPrDAzEemi8scpwjq3_w" name="CDEMER" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPrTAzEemi8scpwjq3_w" value="CDEMER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPrjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPrzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPsDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPsTAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPsjAzEemi8scpwjq3_w" name="CDEFAV" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPszAzEemi8scpwjq3_w" value="CDEFAV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPtDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPtTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPtjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPtzAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPuDAzEemi8scpwjq3_w" name="TARCLI" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPuTAzEemi8scpwjq3_w" value="TARCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPujAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPuzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPvDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPvTAzEemi8scpwjq3_w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPvjAzEemi8scpwjq3_w" name="UNIVCL" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPvzAzEemi8scpwjq3_w" value="UNIVCL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPwDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPwTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPwjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPwzAzEemi8scpwjq3_w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPxDAzEemi8scpwjq3_w" name="REMISE" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPxTAzEemi8scpwjq3_w" value="REMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPxjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNPxzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPyDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPyTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNPyjAzEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNPyzAzEemi8scpwjq3_w" name="IMPTAR" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNPzDAzEemi8scpwjq3_w" value="IMPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNPzTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNPzjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNPzzAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNP0DAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNP0TAzEemi8scpwjq3_w" name="CENTRA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNP0jAzEemi8scpwjq3_w" value="CENTRA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNP0zAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNP1DAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNP1TAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNP1jAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNP1zAzEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNP2DAzEemi8scpwjq3_w" name="MAILVRP" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNP2TAzEemi8scpwjq3_w" value="MAILVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNP2jAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNP2zAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNP3DAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNP3TAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNP3jAzEemi8scpwjq3_w" name="CLIMAIL" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNP3zAzEemi8scpwjq3_w" value="CLIMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNP4DAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNP4TAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNP4jAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNP4zAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNP5DAzEemi8scpwjq3_w" name="SPECIAL" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNP5TAzEemi8scpwjq3_w" value="SPECIAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNP5jAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNP5zAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNP6DAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNP6TAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNP6jAzEemi8scpwjq3_w" name="CODSOC" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNP6zAzEemi8scpwjq3_w" value="CODSOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNP7DAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNP7TAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNP7jAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNP7zAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNP8DAzEemi8scpwjq3_w" name="TELVRP" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNP8TAzEemi8scpwjq3_w" value="TELVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNP8jAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNP8zAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNP9DAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNP9TAzEemi8scpwjq3_w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNP9jAzEemi8scpwjq3_w" name="NOMVRP" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNP9zAzEemi8scpwjq3_w" value="NOMVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNP-DAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNP-TAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNP-jAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNP-zAzEemi8scpwjq3_w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNP_DAzEemi8scpwjq3_w" name="LNGCLI" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNP_TAzEemi8scpwjq3_w" value="LNGCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNP_jAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNP_zAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQADAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQATAzEemi8scpwjq3_w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQAjAzEemi8scpwjq3_w" name="PWDMD5" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQAzAzEemi8scpwjq3_w" value="PWDMD5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQBDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQBTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQBjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQBzAzEemi8scpwjq3_w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQCDAzEemi8scpwjq3_w" name="NOMCOM" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQCTAzEemi8scpwjq3_w" value="NOMCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQCjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQCzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQDDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQDTAzEemi8scpwjq3_w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQDjAzEemi8scpwjq3_w" name="MELCOM" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQDzAzEemi8scpwjq3_w" value="MELCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQEDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQETAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQEjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQEzAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQFDAzEemi8scpwjq3_w" name="TOPCRE" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQFTAzEemi8scpwjq3_w" value="TOPCRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQFjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQFzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQGDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQGTAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQGjAzEemi8scpwjq3_w" name="TOPPROMO" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQGzAzEemi8scpwjq3_w" value="TOPPROMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQHDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQHTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQHjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQHzAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQIDAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQITAzEemi8scpwjq3_w" name="TOPMERCURIALE" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQIjAzEemi8scpwjq3_w" value="TOPMERCURIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQIzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQJDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQJTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQJjAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQJzAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQKDAzEemi8scpwjq3_w" name="TOPKIT" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQKTAzEemi8scpwjq3_w" value="TOPKIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQKjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQKzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQLDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQLTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQLjAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQLzAzEemi8scpwjq3_w" name="TOPPRINCIPAL" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQMDAzEemi8scpwjq3_w" value="TOPPRINCIPAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQMTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQMjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQMzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQNDAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQNTAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQNjAzEemi8scpwjq3_w" name="TOPSUPERVISION" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQNzAzEemi8scpwjq3_w" value="TOPSUPERVISION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQODAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQOTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQOjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQOzAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQPDAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQPTAzEemi8scpwjq3_w" name="MODRGLMT" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQPjAzEemi8scpwjq3_w" value="MODRGLMT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQPzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQQDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQQTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQQjAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQQzAzEemi8scpwjq3_w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQRDAzEemi8scpwjq3_w" name="TOPFA" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQRTAzEemi8scpwjq3_w" value="TOPFA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQRjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQRzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQSDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQSTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQSjAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQSzAzEemi8scpwjq3_w" name="TOPLE" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQTDAzEemi8scpwjq3_w" value="TOPLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQTTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQTjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQTzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQUDAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQUTAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQUjAzEemi8scpwjq3_w" name="FACTURATION" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQUzAzEemi8scpwjq3_w" value="FACTURATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQVDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQVTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQVjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQVzAzEemi8scpwjq3_w" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQWDAzEemi8scpwjq3_w" name="ADRESSE" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQWTAzEemi8scpwjq3_w" value="ADRESSE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQWjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQWzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQXDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQXTAzEemi8scpwjq3_w" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQXjAzEemi8scpwjq3_w" name="TXREMISE" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQXzAzEemi8scpwjq3_w" value="TXREMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQYDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQYTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQYjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQYzAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQZDAzEemi8scpwjq3_w" name="TOPCHR" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQZTAzEemi8scpwjq3_w" value="TOPCHR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQZjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQZzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQaDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQaTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQajAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvNQazAzEemi8scpwjq3_w" name="MAXCDE" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvNQbDAzEemi8scpwjq3_w" value="MAXCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvNQbTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvNQbjAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvNQbzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvNQcDAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvNQcTAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAYDAzEemi8scpwjq3_w" name="MERCUGRP" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAYTAzEemi8scpwjq3_w" value="MERCUGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAYjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAYzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAZDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAZTAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAZjAzEemi8scpwjq3_w" name="CDEMERC" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAZzAzEemi8scpwjq3_w" value="CDEMERC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAaDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAaTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAajAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAazAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAbDAzEemi8scpwjq3_w" name="TOPEFAC" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAbTAzEemi8scpwjq3_w" value="TOPEFAC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAbjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvXAbzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAcDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAcTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAcjAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAczAzEemi8scpwjq3_w" name="AUTOLOGKEY" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAdDAzEemi8scpwjq3_w" value="AUTOLOGKEY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAdTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAdjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAdzAzEemi8scpwjq3_w" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAeDAzEemi8scpwjq3_w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAeTAzEemi8scpwjq3_w" name="USRCDF" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAejAzEemi8scpwjq3_w" value="USRCDF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAezAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAfDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAfTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAfjAzEemi8scpwjq3_w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAfzAzEemi8scpwjq3_w" name="USRPRF" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAgDAzEemi8scpwjq3_w" value="USRPRF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAgTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvXAgjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAgzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAhDAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAhTAzEemi8scpwjq3_w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAhjAzEemi8scpwjq3_w" name="USRSIT" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAhzAzEemi8scpwjq3_w" value="USRSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAiDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAiTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAijAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAizAzEemi8scpwjq3_w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAjDAzEemi8scpwjq3_w" name="USRDIV" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAjTAzEemi8scpwjq3_w" value="USRDIV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAjjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XvXAjzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAkDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAkTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAkjAzEemi8scpwjq3_w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAkzAzEemi8scpwjq3_w" name="USRHIER" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAlDAzEemi8scpwjq3_w" value="USRHIER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAlTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAljAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAlzAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAmDAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XvXAmTAzEemi8scpwjq3_w" name="TOPCST" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_XvXAmjAzEemi8scpwjq3_w" value="TOPCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XvXAmzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XvXAnDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XvXAnTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XvXAnjAzEemi8scpwjq3_w" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_XjbdMTAzEemi8scpwjq3_w" name="WSOCLIP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_XjbdMjAzEemi8scpwjq3_w" value="WSOCLIP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_XjbdMzAzEemi8scpwjq3_w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_XkOucDAzEemi8scpwjq3_w" name="IDWEBC" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOucTAzEemi8scpwjq3_w" value="IDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOucjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOuczAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOudDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOudTAzEemi8scpwjq3_w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOudjAzEemi8scpwjq3_w" name="CPTCLI" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOudzAzEemi8scpwjq3_w" value="CPTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOueDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkOueTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOuejAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOuezAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOufDAzEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOufTAzEemi8scpwjq3_w" name="NOMCLI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOufjAzEemi8scpwjq3_w" value="NOMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOufzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOugDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOugTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOugjAzEemi8scpwjq3_w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOugzAzEemi8scpwjq3_w" name="PRECLI" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOuhDAzEemi8scpwjq3_w" value="PRECLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOuhTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOuhjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOuhzAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOuiDAzEemi8scpwjq3_w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOuiTAzEemi8scpwjq3_w" name="PWDCLI" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOuijAzEemi8scpwjq3_w" value="PWDCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOuizAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOujDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOujTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOujjAzEemi8scpwjq3_w" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOujzAzEemi8scpwjq3_w" name="MELADR" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOukDAzEemi8scpwjq3_w" value="MELADR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOukTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOukjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOukzAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOulDAzEemi8scpwjq3_w" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOulTAzEemi8scpwjq3_w" name="MELSIT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOuljAzEemi8scpwjq3_w" value="MELSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOulzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOumDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOumTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOumjAzEemi8scpwjq3_w" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOumzAzEemi8scpwjq3_w" name="NIVCLI" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOunDAzEemi8scpwjq3_w" value="NIVCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOunTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOunjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOunzAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOuoDAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOuoTAzEemi8scpwjq3_w" name="ACTCLI" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOuojAzEemi8scpwjq3_w" value="ACTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOuozAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOupDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOupTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOupjAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOupzAzEemi8scpwjq3_w" name="TYPTAR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOuqDAzEemi8scpwjq3_w" value="TYPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOuqTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOuqjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOuqzAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOurDAzEemi8scpwjq3_w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOurTAzEemi8scpwjq3_w" name="TXREMI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOurjAzEemi8scpwjq3_w" value="TXREMI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOurzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkOusDAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOusTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOusjAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOuszAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOutDAzEemi8scpwjq3_w" name="FRANCO" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOutTAzEemi8scpwjq3_w" value="FRANCO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOutjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkOutzAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOuuDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOuuTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOuujAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOuuzAzEemi8scpwjq3_w" name="FRPORT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOuvDAzEemi8scpwjq3_w" value="FRPORT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOuvTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkOuvjAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOuvzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOuwDAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOuwTAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkOuwjAzEemi8scpwjq3_w" name="MINCDE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkOuwzAzEemi8scpwjq3_w" value="MINCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkOuxDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkOuxTAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkOuxjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkOuxzAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkOuyDAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfcDAzEemi8scpwjq3_w" name="MERCUR" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfcTAzEemi8scpwjq3_w" value="MERCUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfcjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfczAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfdDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfdTAzEemi8scpwjq3_w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfdjAzEemi8scpwjq3_w" name="CDECAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfdzAzEemi8scpwjq3_w" value="CDECAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfeDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfeTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfejAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfezAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYffDAzEemi8scpwjq3_w" name="CDELST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYffTAzEemi8scpwjq3_w" value="CDELST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYffjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYffzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfgDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfgTAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfgjAzEemi8scpwjq3_w" name="CDEMER" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfgzAzEemi8scpwjq3_w" value="CDEMER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfhDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfhTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfhjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfhzAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfiDAzEemi8scpwjq3_w" name="CDEFAV" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfiTAzEemi8scpwjq3_w" value="CDEFAV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfijAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfizAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfjDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfjTAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfjjAzEemi8scpwjq3_w" name="TARCLI" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfjzAzEemi8scpwjq3_w" value="TARCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfkDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfkTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfkjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfkzAzEemi8scpwjq3_w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYflDAzEemi8scpwjq3_w" name="UNIVCL" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYflTAzEemi8scpwjq3_w" value="UNIVCL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfljAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYflzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfmDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfmTAzEemi8scpwjq3_w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfmjAzEemi8scpwjq3_w" name="REMISE" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfmzAzEemi8scpwjq3_w" value="REMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfnDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYfnTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfnjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfnzAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfoDAzEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfoTAzEemi8scpwjq3_w" name="IMPTAR" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfojAzEemi8scpwjq3_w" value="IMPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfozAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfpDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfpTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfpjAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfpzAzEemi8scpwjq3_w" name="CENTRA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfqDAzEemi8scpwjq3_w" value="CENTRA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfqTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYfqjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfqzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfrDAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfrTAzEemi8scpwjq3_w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfrjAzEemi8scpwjq3_w" name="MAILVRP" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfrzAzEemi8scpwjq3_w" value="MAILVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfsDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfsTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfsjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfszAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYftDAzEemi8scpwjq3_w" name="CLIMAIL" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYftTAzEemi8scpwjq3_w" value="CLIMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYftjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYftzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfuDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfuTAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfujAzEemi8scpwjq3_w" name="SPECIAL" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfuzAzEemi8scpwjq3_w" value="SPECIAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfvDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfvTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfvjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfvzAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfwDAzEemi8scpwjq3_w" name="CODSOC" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfwTAzEemi8scpwjq3_w" value="CODSOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfwjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfwzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfxDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfxTAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfxjAzEemi8scpwjq3_w" name="TELVRP" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfxzAzEemi8scpwjq3_w" value="TELVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfyDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfyTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYfyjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYfyzAzEemi8scpwjq3_w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYfzDAzEemi8scpwjq3_w" name="NOMVRP" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYfzTAzEemi8scpwjq3_w" value="NOMVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYfzjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYfzzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYf0DAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYf0TAzEemi8scpwjq3_w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYf0jAzEemi8scpwjq3_w" name="LNGCLI" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYf0zAzEemi8scpwjq3_w" value="LNGCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYf1DAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYf1TAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYf1jAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYf1zAzEemi8scpwjq3_w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYf2DAzEemi8scpwjq3_w" name="PWDMD5" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYf2TAzEemi8scpwjq3_w" value="PWDMD5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYf2jAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYf2zAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYf3DAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYf3TAzEemi8scpwjq3_w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYf3jAzEemi8scpwjq3_w" name="NOMCOM" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYf3zAzEemi8scpwjq3_w" value="NOMCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYf4DAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYf4TAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYf4jAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYf4zAzEemi8scpwjq3_w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYf5DAzEemi8scpwjq3_w" name="MELCOM" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYf5TAzEemi8scpwjq3_w" value="MELCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYf5jAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYf5zAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYf6DAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYf6TAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYf6jAzEemi8scpwjq3_w" name="TOPCRE" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYf6zAzEemi8scpwjq3_w" value="TOPCRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYf7DAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYf7TAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYf7jAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYf7zAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYf8DAzEemi8scpwjq3_w" name="TOPPROMO" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYf8TAzEemi8scpwjq3_w" value="TOPPROMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYf8jAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYf8zAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYf9DAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYf9TAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYf9jAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYf9zAzEemi8scpwjq3_w" name="TOPMERCURIALE" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYf-DAzEemi8scpwjq3_w" value="TOPMERCURIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYf-TAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYf-jAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYf-zAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYf_DAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYf_TAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYf_jAzEemi8scpwjq3_w" name="TOPKIT" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYf_zAzEemi8scpwjq3_w" value="TOPKIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgADAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYgATAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgAjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgAzAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgBDAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgBTAzEemi8scpwjq3_w" name="TOPPRINCIPAL" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgBjAzEemi8scpwjq3_w" value="TOPPRINCIPAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgBzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYgCDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgCTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgCjAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgCzAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgDDAzEemi8scpwjq3_w" name="TOPSUPERVISION" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgDTAzEemi8scpwjq3_w" value="TOPSUPERVISION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgDjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYgDzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgEDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgETAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgEjAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgEzAzEemi8scpwjq3_w" name="MODRGLMT" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgFDAzEemi8scpwjq3_w" value="MODRGLMT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgFTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYgFjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgFzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgGDAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgGTAzEemi8scpwjq3_w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgGjAzEemi8scpwjq3_w" name="TOPFA" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgGzAzEemi8scpwjq3_w" value="TOPFA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgHDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYgHTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgHjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgHzAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgIDAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgITAzEemi8scpwjq3_w" name="TOPLE" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgIjAzEemi8scpwjq3_w" value="TOPLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgIzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYgJDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgJTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgJjAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgJzAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgKDAzEemi8scpwjq3_w" name="FACTURATION" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgKTAzEemi8scpwjq3_w" value="FACTURATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgKjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgKzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgLDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgLTAzEemi8scpwjq3_w" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgLjAzEemi8scpwjq3_w" name="ADRESSE" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgLzAzEemi8scpwjq3_w" value="ADRESSE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgMDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgMTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgMjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgMzAzEemi8scpwjq3_w" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgNDAzEemi8scpwjq3_w" name="TXREMISE" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgNTAzEemi8scpwjq3_w" value="TXREMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgNjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgNzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgODAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgOTAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgOjAzEemi8scpwjq3_w" name="TOPCHR" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgOzAzEemi8scpwjq3_w" value="TOPCHR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgPDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYgPTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgPjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgPzAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgQDAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkYgQTAzEemi8scpwjq3_w" name="MAXCDE" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkYgQjAzEemi8scpwjq3_w" value="MAXCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkYgQzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkYgRDAzEemi8scpwjq3_w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkYgRTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkYgRjAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkYgRzAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQcDAzEemi8scpwjq3_w" name="MERCUGRP" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQcTAzEemi8scpwjq3_w" value="MERCUGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQcjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQczAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQdDAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQdTAzEemi8scpwjq3_w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQdjAzEemi8scpwjq3_w" name="CDEMERC" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQdzAzEemi8scpwjq3_w" value="CDEMERC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQeDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQeTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQejAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQezAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQfDAzEemi8scpwjq3_w" name="TOPEFAC" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQfTAzEemi8scpwjq3_w" value="TOPEFAC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQfjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkiQfzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQgDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQgTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQgjAzEemi8scpwjq3_w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQgzAzEemi8scpwjq3_w" name="AUTOLOGKEY" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQhDAzEemi8scpwjq3_w" value="AUTOLOGKEY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQhTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQhjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQhzAzEemi8scpwjq3_w" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQiDAzEemi8scpwjq3_w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQiTAzEemi8scpwjq3_w" name="USRCDF" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQijAzEemi8scpwjq3_w" value="USRCDF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQizAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQjDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQjTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQjjAzEemi8scpwjq3_w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQjzAzEemi8scpwjq3_w" name="USRPRF" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQkDAzEemi8scpwjq3_w" value="USRPRF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQkTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkiQkjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQkzAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQlDAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQlTAzEemi8scpwjq3_w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQljAzEemi8scpwjq3_w" name="USRSIT" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQlzAzEemi8scpwjq3_w" value="USRSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQmDAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQmTAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQmjAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQmzAzEemi8scpwjq3_w" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQnDAzEemi8scpwjq3_w" name="USRDIV" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQnTAzEemi8scpwjq3_w" value="USRDIV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQnjAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XkiQnzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQoDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQoTAzEemi8scpwjq3_w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQojAzEemi8scpwjq3_w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQozAzEemi8scpwjq3_w" name="USRHIER" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQpDAzEemi8scpwjq3_w" value="USRHIER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQpTAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQpjAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQpzAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQqDAzEemi8scpwjq3_w" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XkiQqTAzEemi8scpwjq3_w" name="TOPCST" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_XkiQqjAzEemi8scpwjq3_w" value="TOPCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XkiQqzAzEemi8scpwjq3_w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XkiQrDAzEemi8scpwjq3_w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XkiQrTAzEemi8scpwjq3_w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XkiQrjAzEemi8scpwjq3_w" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_9nU5sDQmEemjULX4DWixSg" name="OTEFCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_9nU5sTQmEemjULX4DWixSg" value="OTEFCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_9nU5sjQmEemjULX4DWixSg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_9ypY4DQmEemjULX4DWixSg" name="EFAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_9ypY4TQmEemjULX4DWixSg" value="EFAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9ypY4jQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9ypY4zQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9ypY5DQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9ypY5TQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9ypY5jQmEemjULX4DWixSg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9ypY5zQmEemjULX4DWixSg" name="EFTAC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_9ypY6DQmEemjULX4DWixSg" value="EFTAC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9ypY6TQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9ypY6jQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9ypY6zQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9ypY7DQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9ypY7TQmEemjULX4DWixSg" name="EFNBN3" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_9ypY7jQmEemjULX4DWixSg" value="EFNBN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9ypY7zQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9ypY8DQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9ypY8TQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9ypY8jQmEemjULX4DWixSg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9ypY8zQmEemjULX4DWixSg" name="EFPMT2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_9ypY9DQmEemjULX4DWixSg" value="EFPMT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9ypY9TQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9ypY9jQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9ypY9zQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9ypY-DQmEemjULX4DWixSg" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9ypY-TQmEemjULX4DWixSg" name="EFAWNA" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_9ypY-jQmEemjULX4DWixSg" value="EFAWNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9ypY-zQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9ypY_DQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9ypY_TQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9ypY_jQmEemjULX4DWixSg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9ypY_zQmEemjULX4DWixSg" name="EFNDCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_9ypZADQmEemjULX4DWixSg" value="EFNDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9ypZATQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9ypZAjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9ypZAzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9ypZBDQmEemjULX4DWixSg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9ypZBTQmEemjULX4DWixSg" name="EFHQTX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_9ypZBjQmEemjULX4DWixSg" value="EFHQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9ypZBzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9ypZCDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9ypZCTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9ypZCjQmEemjULX4DWixSg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9ypZCzQmEemjULX4DWixSg" name="EFHPTX" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_9ypZDDQmEemjULX4DWixSg" value="EFHPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9ypZDTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9ypZDjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9ypZDzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9ypZEDQmEemjULX4DWixSg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyi0DQmEemjULX4DWixSg" name="EFAKCD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyi0TQmEemjULX4DWixSg" value="EFAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyi0jQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyi0zQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyi1DQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyi1TQmEemjULX4DWixSg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyi1jQmEemjULX4DWixSg" name="EFAGTX" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyi1zQmEemjULX4DWixSg" value="EFAGTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyi2DQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyi2TQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyi2jQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyi2zQmEemjULX4DWixSg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyi3DQmEemjULX4DWixSg" name="EFAUTX" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyi3TQmEemjULX4DWixSg" value="EFAUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyi3jQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyi3zQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyi4DQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyi4TQmEemjULX4DWixSg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyi4jQmEemjULX4DWixSg" name="EFHOTX" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyi4zQmEemjULX4DWixSg" value="EFHOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyi5DQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyi5TQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyi5jQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyi5zQmEemjULX4DWixSg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyi6DQmEemjULX4DWixSg" name="EFU9NA" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyi6TQmEemjULX4DWixSg" value="EFU9NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyi6jQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyi6zQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyi7DQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyi7TQmEemjULX4DWixSg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyi7jQmEemjULX4DWixSg" name="EFZLTX" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyi7zQmEemjULX4DWixSg" value="EFZLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyi8DQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9yyi8TQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyi8jQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyi8zQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyi9DQmEemjULX4DWixSg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyi9TQmEemjULX4DWixSg" name="EFFMTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyi9jQmEemjULX4DWixSg" value="EFFMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyi9zQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9yyi-DQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyi-TQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyi-jQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyi-zQmEemjULX4DWixSg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyi_DQmEemjULX4DWixSg" name="EFIZST" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyi_TQmEemjULX4DWixSg" value="EFIZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyi_jQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyi_zQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjADQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjATQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjAjQmEemjULX4DWixSg" name="EFABCD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjAzQmEemjULX4DWixSg" value="EFABCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjBDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjBTQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjBjQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjBzQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjCDQmEemjULX4DWixSg" name="EFABNA" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjCTQmEemjULX4DWixSg" value="EFABNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjCjQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjCzQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjDDQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjDTQmEemjULX4DWixSg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjDjQmEemjULX4DWixSg" name="EFBCCD" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjDzQmEemjULX4DWixSg" value="EFBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjEDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9yyjETQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjEjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjEzQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjFDQmEemjULX4DWixSg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjFTQmEemjULX4DWixSg" name="EFBSNA" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjFjQmEemjULX4DWixSg" value="EFBSNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjFzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjGDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjGTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjGjQmEemjULX4DWixSg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjGzQmEemjULX4DWixSg" name="EFBTNA" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjHDQmEemjULX4DWixSg" value="EFBTNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjHTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjHjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjHzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjIDQmEemjULX4DWixSg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjITQmEemjULX4DWixSg" name="EFDNTX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjIjQmEemjULX4DWixSg" value="EFDNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjIzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9yyjJDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjJTQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjJjQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjJzQmEemjULX4DWixSg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjKDQmEemjULX4DWixSg" name="EFBHCD" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjKTQmEemjULX4DWixSg" value="EFBHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjKjQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9yyjKzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjLDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjLTQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjLjQmEemjULX4DWixSg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjLzQmEemjULX4DWixSg" name="EFBBTX" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjMDQmEemjULX4DWixSg" value="EFBBTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjMTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjMjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjMzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjNDQmEemjULX4DWixSg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjNTQmEemjULX4DWixSg" name="EFAQCD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjNjQmEemjULX4DWixSg" value="EFAQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjNzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjODQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjOTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjOjQmEemjULX4DWixSg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjOzQmEemjULX4DWixSg" name="EFAOTX" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjPDQmEemjULX4DWixSg" value="EFAOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjPTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjPjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjPzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjQDQmEemjULX4DWixSg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjQTQmEemjULX4DWixSg" name="EFGHCD" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjQjQmEemjULX4DWixSg" value="EFGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjQzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjRDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjRTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjRjQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjRzQmEemjULX4DWixSg" name="EFHBNA" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjSDQmEemjULX4DWixSg" value="EFHBNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjSTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjSjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjSzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjTDQmEemjULX4DWixSg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjTTQmEemjULX4DWixSg" name="EFAMCD" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjTjQmEemjULX4DWixSg" value="EFAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjTzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjUDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjUTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjUjQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjUzQmEemjULX4DWixSg" name="EFSINA" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjVDQmEemjULX4DWixSg" value="EFSINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjVTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjVjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjVzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjWDQmEemjULX4DWixSg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjWTQmEemjULX4DWixSg" name="EFBOCD" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjWjQmEemjULX4DWixSg" value="EFBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjWzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjXDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjXTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjXjQmEemjULX4DWixSg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjXzQmEemjULX4DWixSg" name="EFHRST" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjYDQmEemjULX4DWixSg" value="EFHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjYTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjYjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjYzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjZDQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjZTQmEemjULX4DWixSg" name="EFBYNA" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjZjQmEemjULX4DWixSg" value="EFBYNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjZzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjaDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjaTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjajQmEemjULX4DWixSg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjazQmEemjULX4DWixSg" name="EFPFN6" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjbDQmEemjULX4DWixSg" value="EFPFN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjbTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9yyjbjQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjbzQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjcDQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjcTQmEemjULX4DWixSg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9yyjcjQmEemjULX4DWixSg" name="EFVANA" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_9yyjczQmEemjULX4DWixSg" value="EFVANA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9yyjdDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9yyjdTQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9yyjdjQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9yyjdzQmEemjULX4DWixSg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8T0DQmEemjULX4DWixSg" name="EFTFC1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8T0TQmEemjULX4DWixSg" value="EFTFC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8T0jQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8T0zQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8T1DQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8T1TQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8T1jQmEemjULX4DWixSg" name="EFFTNB" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8T1zQmEemjULX4DWixSg" value="EFFTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8T2DQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8T2TQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8T2jQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8T2zQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8T3DQmEemjULX4DWixSg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8T3TQmEemjULX4DWixSg" name="EFFJTX" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8T3jQmEemjULX4DWixSg" value="EFFJTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8T3zQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8T4DQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8T4TQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8T4jQmEemjULX4DWixSg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8T4zQmEemjULX4DWixSg" name="EFPGN6" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8T5DQmEemjULX4DWixSg" value="EFPGN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8T5TQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8T5jQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8T5zQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8T6DQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8T6TQmEemjULX4DWixSg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8T6jQmEemjULX4DWixSg" name="EFVBNA" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8T6zQmEemjULX4DWixSg" value="EFVBNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8T7DQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8T7TQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8T7jQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8T7zQmEemjULX4DWixSg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8T8DQmEemjULX4DWixSg" name="EFAGCD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8T8TQmEemjULX4DWixSg" value="EFAGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8T8jQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8T8zQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8T9DQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8T9TQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8T9jQmEemjULX4DWixSg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8T9zQmEemjULX4DWixSg" name="EFACTX" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8T-DQmEemjULX4DWixSg" value="EFACTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8T-TQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8T-jQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8T-zQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8T_DQmEemjULX4DWixSg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8T_TQmEemjULX4DWixSg" name="EFF2CD" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8T_jQmEemjULX4DWixSg" value="EFF2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8T_zQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UADQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UATQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UAjQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UAzQmEemjULX4DWixSg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UBDQmEemjULX4DWixSg" name="EFHNNA" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UBTQmEemjULX4DWixSg" value="EFHNNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UBjQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UBzQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UCDQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UCTQmEemjULX4DWixSg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UCjQmEemjULX4DWixSg" name="EFKVS3" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UCzQmEemjULX4DWixSg" value="EFKVS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UDDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UDTQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UDjQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UDzQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UEDQmEemjULX4DWixSg" name="EFTWP3" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UETQmEemjULX4DWixSg" value="EFTWP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UEjQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UEzQmEemjULX4DWixSg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UFDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UFTQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UFjQmEemjULX4DWixSg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UFzQmEemjULX4DWixSg" name="EFTXP3" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UGDQmEemjULX4DWixSg" value="EFTXP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UGTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UGjQmEemjULX4DWixSg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UGzQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UHDQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UHTQmEemjULX4DWixSg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UHjQmEemjULX4DWixSg" name="EFVUN2" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UHzQmEemjULX4DWixSg" value="EFVUN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UIDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UITQmEemjULX4DWixSg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UIjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UIzQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UJDQmEemjULX4DWixSg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UJTQmEemjULX4DWixSg" name="EFV1N2" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UJjQmEemjULX4DWixSg" value="EFV1N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UJzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UKDQmEemjULX4DWixSg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UKTQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UKjQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UKzQmEemjULX4DWixSg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8ULDQmEemjULX4DWixSg" name="EFV2N2" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8ULTQmEemjULX4DWixSg" value="EFV2N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8ULjQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8ULzQmEemjULX4DWixSg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UMDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UMTQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UMjQmEemjULX4DWixSg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UMzQmEemjULX4DWixSg" name="EFV3N2" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UNDQmEemjULX4DWixSg" value="EFV3N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UNTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UNjQmEemjULX4DWixSg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UNzQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UODQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UOTQmEemjULX4DWixSg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UOjQmEemjULX4DWixSg" name="EFV4N2" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UOzQmEemjULX4DWixSg" value="EFV4N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UPDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UPTQmEemjULX4DWixSg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UPjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UPzQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UQDQmEemjULX4DWixSg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UQTQmEemjULX4DWixSg" name="EFBACD" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UQjQmEemjULX4DWixSg" value="EFBACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UQzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8URDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8URTQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8URjQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8URzQmEemjULX4DWixSg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8USDQmEemjULX4DWixSg" name="EFVCNA" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8USTQmEemjULX4DWixSg" value="EFVCNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8USjQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8USzQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UTDQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UTTQmEemjULX4DWixSg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UTjQmEemjULX4DWixSg" name="EFPHN6" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UTzQmEemjULX4DWixSg" value="EFPHN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UUDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UUTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UUjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UUzQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UVDQmEemjULX4DWixSg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UVTQmEemjULX4DWixSg" name="EFBBCD" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UVjQmEemjULX4DWixSg" value="EFBBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UVzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UWDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UWTQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UWjQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UWzQmEemjULX4DWixSg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UXDQmEemjULX4DWixSg" name="EFVDNA" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UXTQmEemjULX4DWixSg" value="EFVDNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UXjQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UXzQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UYDQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UYTQmEemjULX4DWixSg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UYjQmEemjULX4DWixSg" name="EFPIN6" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UYzQmEemjULX4DWixSg" value="EFPIN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UZDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UZTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UZjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UZzQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UaDQmEemjULX4DWixSg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UaTQmEemjULX4DWixSg" name="EFBNNA" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UajQmEemjULX4DWixSg" value="EFBNNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UazQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UbDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UbTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UbjQmEemjULX4DWixSg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UbzQmEemjULX4DWixSg" name="EFPNT2" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UcDQmEemjULX4DWixSg" value="EFPNT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UcTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UcjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UczQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UdDQmEemjULX4DWixSg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UdTQmEemjULX4DWixSg" name="EFVENA" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UdjQmEemjULX4DWixSg" value="EFVENA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UdzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UeDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UeTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UejQmEemjULX4DWixSg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UezQmEemjULX4DWixSg" name="EFQON6" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UfDQmEemjULX4DWixSg" value="EFQON6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UfTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UfjQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UfzQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UgDQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UgTQmEemjULX4DWixSg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UgjQmEemjULX4DWixSg" name="EFQPN6" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UgzQmEemjULX4DWixSg" value="EFQPN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UhDQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9y8UhTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UhjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UhzQmEemjULX4DWixSg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UiDQmEemjULX4DWixSg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UiTQmEemjULX4DWixSg" name="EFK8S3" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UijQmEemjULX4DWixSg" value="EFK8S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UizQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UjDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UjTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UjjQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UjzQmEemjULX4DWixSg" name="EFK9S3" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UkDQmEemjULX4DWixSg" value="EFK9S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UkTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UkjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UkzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UlDQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UlTQmEemjULX4DWixSg" name="EFLBS3" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UljQmEemjULX4DWixSg" value="EFLBS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UlzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UmDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UmTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UmjQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UmzQmEemjULX4DWixSg" name="EFLAS3" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UnDQmEemjULX4DWixSg" value="EFLAS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UnTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UnjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UnzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UoDQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UoTQmEemjULX4DWixSg" name="EFOMS3" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UojQmEemjULX4DWixSg" value="EFOMS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UozQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UpDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UpTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UpjQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UpzQmEemjULX4DWixSg" name="EFLCS3" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UqDQmEemjULX4DWixSg" value="EFLCS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UqTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UqjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UqzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UrDQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UrTQmEemjULX4DWixSg" name="EFLDS3" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UrjQmEemjULX4DWixSg" value="EFLDS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UrzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UsDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UsTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UsjQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UszQmEemjULX4DWixSg" name="EFLES3" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UtDQmEemjULX4DWixSg" value="EFLES3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UtTQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UtjQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UtzQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UuDQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9y8UuTQmEemjULX4DWixSg" name="EFLFS3" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_9y8UujQmEemjULX4DWixSg" value="EFLFS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9y8UuzQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9y8UvDQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9y8UvTQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9y8UvjQmEemjULX4DWixSg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9zGE0DQmEemjULX4DWixSg" name="EFLGS3" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_9zGE0TQmEemjULX4DWixSg" value="EFLGS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9zGE0jQmEemjULX4DWixSg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9zGE0zQmEemjULX4DWixSg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9zGE1DQmEemjULX4DWixSg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9zGE1TQmEemjULX4DWixSg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_N47YcDqUEemaHK9T15k6XQ" name="ORBUREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_N47YcTqUEemaHK9T15k6XQ" value="ORBUREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_N47YcjqUEemaHK9T15k6XQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_OH6PoDqUEemaHK9T15k6XQ" name="BUBOCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_OH6PoTqUEemaHK9T15k6XQ" value="BUBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_OH6PojqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_OH6PozqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_OH6PpDqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_OH6PpTqUEemaHK9T15k6XQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_OH6PpjqUEemaHK9T15k6XQ" name="BUHRST" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_OH6PpzqUEemaHK9T15k6XQ" value="BUHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_OH6PqDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_OH6PqTqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_OH6PqjqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_OH6PqzqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_OH6PrDqUEemaHK9T15k6XQ" name="BUFHNB" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_OH6PrTqUEemaHK9T15k6XQ" value="BUFHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_OH6PrjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_OH6PrzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_OH6PsDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_OH6PsTqUEemaHK9T15k6XQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_OH6PsjqUEemaHK9T15k6XQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_OIEAoDqUEemaHK9T15k6XQ" name="BUC7PR" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_OIEAoTqUEemaHK9T15k6XQ" value="BUC7PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_OIEAojqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_OIEAozqUEemaHK9T15k6XQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_OIEApDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_OIEApTqUEemaHK9T15k6XQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_OIEApjqUEemaHK9T15k6XQ" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_OIEApzqUEemaHK9T15k6XQ" name="BUHGNA" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_OIEAqDqUEemaHK9T15k6XQ" value="BUHGNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_OIEAqTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_OIEAqjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_OIEAqzqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_OIEArDqUEemaHK9T15k6XQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_OIEArTqUEemaHK9T15k6XQ" name="BUFINB" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_OIEArjqUEemaHK9T15k6XQ" value="BUFINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_OIEArzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_OIEAsDqUEemaHK9T15k6XQ" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_OIEAsTqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_OIEAsjqUEemaHK9T15k6XQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_OIEAszqUEemaHK9T15k6XQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_OIEAtDqUEemaHK9T15k6XQ" name="BUN1NB" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_OIEAtTqUEemaHK9T15k6XQ" value="BUN1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_OIEAtjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_OIEAtzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_OIEAuDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_OIEAuTqUEemaHK9T15k6XQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_OIEAujqUEemaHK9T15k6XQ" value="6"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_swqhoDqUEemaHK9T15k6XQ" name="MYCARTP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_swqhoTqUEemaHK9T15k6XQ" value="MYCARTP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_swqhojqUEemaHK9T15k6XQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_s1cT8DqUEemaHK9T15k6XQ" name="IDPREF" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cT8TqUEemaHK9T15k6XQ" value="IDPREF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cT8jqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1cT8zqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cT9DqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cT9TqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cT9jqUEemaHK9T15k6XQ" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cT9zqUEemaHK9T15k6XQ" name="ARTCOD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cT-DqUEemaHK9T15k6XQ" value="ARTCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cT-TqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1cT-jqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cT-zqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cT_DqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cT_TqUEemaHK9T15k6XQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cT_jqUEemaHK9T15k6XQ" name="TOPTRT" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cT_zqUEemaHK9T15k6XQ" value="TOPTRT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUADqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1cUATqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUAjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUAzqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUBDqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUBTqUEemaHK9T15k6XQ" name="I1K0NB" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cUBjqUEemaHK9T15k6XQ" value="I1K0NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUBzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1cUCDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUCTqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUCjqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUCzqUEemaHK9T15k6XQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUDDqUEemaHK9T15k6XQ" name="I1FPS2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cUDTqUEemaHK9T15k6XQ" value="I1FPS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUDjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUDzqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUEDqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUETqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUEjqUEemaHK9T15k6XQ" name="I1CNN3" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cUEzqUEemaHK9T15k6XQ" value="I1CNN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUFDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUFTqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUFjqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUFzqUEemaHK9T15k6XQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUGDqUEemaHK9T15k6XQ" name="I1JWCD" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cUGTqUEemaHK9T15k6XQ" value="I1JWCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUGjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUGzqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUHDqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUHTqUEemaHK9T15k6XQ" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUHjqUEemaHK9T15k6XQ" name="DTEMAJ" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cUHzqUEemaHK9T15k6XQ" value="DTEMAJ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUIDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1cUITqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUIjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUIzqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUJDqUEemaHK9T15k6XQ" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUJTqUEemaHK9T15k6XQ" name="I1QQST" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cUJjqUEemaHK9T15k6XQ" value="I1QQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUJzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUKDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUKTqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUKjqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUKzqUEemaHK9T15k6XQ" name="I1QPST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cULDqUEemaHK9T15k6XQ" value="I1QPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cULTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cULjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cULzqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUMDqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUMTqUEemaHK9T15k6XQ" name="I1CPCD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cUMjqUEemaHK9T15k6XQ" value="I1CPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUMzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUNDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUNTqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUNjqUEemaHK9T15k6XQ" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUNzqUEemaHK9T15k6XQ" name="I1ISTX" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cUODqUEemaHK9T15k6XQ" value="I1ISTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUOTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUOjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUOzqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUPDqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUPTqUEemaHK9T15k6XQ" name="B8IOS3" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cUPjqUEemaHK9T15k6XQ" value="B8IOS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cUPzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cUQDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cUQTqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUQjqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1cUQzqUEemaHK9T15k6XQ" name="B8I7S3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1cURDqUEemaHK9T15k6XQ" value="B8I7S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1cURTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1cURjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1cURzqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1cUSDqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1ld4DqUEemaHK9T15k6XQ" name="B8IMS3" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1ld4TqUEemaHK9T15k6XQ" value="B8IMS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1ld4jqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1ld4zqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1ld5DqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1ld5TqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1ld5jqUEemaHK9T15k6XQ" name="A00F20" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1ld5zqUEemaHK9T15k6XQ" value="A00F20"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1ld6DqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1ld6TqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1ld6jqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1ld6zqUEemaHK9T15k6XQ" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1ld7DqUEemaHK9T15k6XQ" name="A00F90" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1ld7TqUEemaHK9T15k6XQ" value="A00F90"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1ld7jqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1ld7zqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1ld8DqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1ld8TqUEemaHK9T15k6XQ" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1ld8jqUEemaHK9T15k6XQ" name="A00F92" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1ld8zqUEemaHK9T15k6XQ" value="A00F92"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1ld9DqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1ld9TqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1ld9jqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1ld9zqUEemaHK9T15k6XQ" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1ld-DqUEemaHK9T15k6XQ" name="A00F30" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1ld-TqUEemaHK9T15k6XQ" value="A00F30"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1ld-jqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1ld-zqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1ld_DqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1ld_TqUEemaHK9T15k6XQ" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1ld_jqUEemaHK9T15k6XQ" name="A00F91" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1ld_zqUEemaHK9T15k6XQ" value="A00F91"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leADqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leATqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leAjqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leAzqUEemaHK9T15k6XQ" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leBDqUEemaHK9T15k6XQ" name="A00F93" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leBTqUEemaHK9T15k6XQ" value="A00F93"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leBjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leBzqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leCDqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leCTqUEemaHK9T15k6XQ" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leCjqUEemaHK9T15k6XQ" name="A00E25" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leCzqUEemaHK9T15k6XQ" value="A00E25"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leDDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leDTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leDjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leDzqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leEDqUEemaHK9T15k6XQ" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leETqUEemaHK9T15k6XQ" name="A00F40" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leEjqUEemaHK9T15k6XQ" value="A00F40"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leEzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leFDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leFTqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leFjqUEemaHK9T15k6XQ" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leFzqUEemaHK9T15k6XQ" name="MQXYNA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leGDqUEemaHK9T15k6XQ" value="MQXYNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leGTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leGjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leGzqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leHDqUEemaHK9T15k6XQ" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leHTqUEemaHK9T15k6XQ" name="ITMU" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leHjqUEemaHK9T15k6XQ" value="ITMU"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leHzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leIDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leITqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leIjqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leIzqUEemaHK9T15k6XQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leJDqUEemaHK9T15k6XQ" name="ITMR" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leJTqUEemaHK9T15k6XQ" value="ITMR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leJjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leJzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leKDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leKTqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leKjqUEemaHK9T15k6XQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leKzqUEemaHK9T15k6XQ" name="ITMF" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leLDqUEemaHK9T15k6XQ" value="ITMF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leLTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leLjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leLzqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leMDqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leMTqUEemaHK9T15k6XQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leMjqUEemaHK9T15k6XQ" name="ITMDNG" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leMzqUEemaHK9T15k6XQ" value="ITMDNG"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leNDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leNTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leNjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leNzqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leODqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leOTqUEemaHK9T15k6XQ" name="X00001" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leOjqUEemaHK9T15k6XQ" value="X00001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leOzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1lePDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1lePTqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1lePjqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1lePzqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leQDqUEemaHK9T15k6XQ" name="X00002" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leQTqUEemaHK9T15k6XQ" value="X00002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leQjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leQzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leRDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leRTqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leRjqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leRzqUEemaHK9T15k6XQ" name="X00003" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leSDqUEemaHK9T15k6XQ" value="X00003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leSTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leSjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leSzqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leTDqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leTTqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leTjqUEemaHK9T15k6XQ" name="X00004" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leTzqUEemaHK9T15k6XQ" value="X00004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leUDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leUTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leUjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leUzqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leVDqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leVTqUEemaHK9T15k6XQ" name="X00005" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leVjqUEemaHK9T15k6XQ" value="X00005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leVzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leWDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leWTqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leWjqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leWzqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leXDqUEemaHK9T15k6XQ" name="X00006" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leXTqUEemaHK9T15k6XQ" value="X00006"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leXjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leXzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leYDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leYTqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leYjqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leYzqUEemaHK9T15k6XQ" name="X00007" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leZDqUEemaHK9T15k6XQ" value="X00007"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leZTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leZjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leZzqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leaDqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leaTqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leajqUEemaHK9T15k6XQ" name="X00008" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leazqUEemaHK9T15k6XQ" value="X00008"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1lebDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1lebTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1lebjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1lebzqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1lecDqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1lecTqUEemaHK9T15k6XQ" name="X00009" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1lecjqUEemaHK9T15k6XQ" value="X00009"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leczqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1ledDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1ledTqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1ledjqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1ledzqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leeDqUEemaHK9T15k6XQ" name="X00010" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leeTqUEemaHK9T15k6XQ" value="X00010"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leejqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leezqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1lefDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1lefTqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1lefjqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1lefzqUEemaHK9T15k6XQ" name="X00011" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1legDqUEemaHK9T15k6XQ" value="X00011"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1legTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1legjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1legzqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1lehDqUEemaHK9T15k6XQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1lehTqUEemaHK9T15k6XQ" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1lehjqUEemaHK9T15k6XQ" name="I1MINB" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1lehzqUEemaHK9T15k6XQ" value="I1MINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leiDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1leiTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leijqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leizqUEemaHK9T15k6XQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1lejDqUEemaHK9T15k6XQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1lejTqUEemaHK9T15k6XQ" name="I1MJNB" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1lejjqUEemaHK9T15k6XQ" value="I1MJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1lejzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1lekDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1lekTqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1lekjqUEemaHK9T15k6XQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1lekzqUEemaHK9T15k6XQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1lelDqUEemaHK9T15k6XQ" name="I1MKNB" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1lelTqUEemaHK9T15k6XQ" value="I1MKNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leljqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1lelzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1lemDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1lemTqUEemaHK9T15k6XQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1lemjqUEemaHK9T15k6XQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1lemzqUEemaHK9T15k6XQ" name="I1MLNB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1lenDqUEemaHK9T15k6XQ" value="I1MLNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1lenTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1lenjqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1lenzqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leoDqUEemaHK9T15k6XQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leoTqUEemaHK9T15k6XQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leojqUEemaHK9T15k6XQ" name="I1MMNB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leozqUEemaHK9T15k6XQ" value="I1MMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1lepDqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_s1lepTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1lepjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1lepzqUEemaHK9T15k6XQ" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leqDqUEemaHK9T15k6XQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leqTqUEemaHK9T15k6XQ" name="CDECOT" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1leqjqUEemaHK9T15k6XQ" value="CDECOT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1leqzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1lerDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1lerTqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1lerjqUEemaHK9T15k6XQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1lerzqUEemaHK9T15k6XQ" name="CDCORP" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1lesDqUEemaHK9T15k6XQ" value="CDCORP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1lesTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1lesjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leszqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1letDqUEemaHK9T15k6XQ" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1letTqUEemaHK9T15k6XQ" name="TITRE_AKENEO" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1letjqUEemaHK9T15k6XQ" value="TITRE_AKENEO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1letzqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1leuDqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1leuTqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1leujqUEemaHK9T15k6XQ" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1leuzqUEemaHK9T15k6XQ" name="DESCRIPTIF_AKENEO" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1levDqUEemaHK9T15k6XQ" value="DESCRIPTIF_AKENEO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1levTqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1levjqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1levzqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1lewDqUEemaHK9T15k6XQ" value="2048"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1vO4DqUEemaHK9T15k6XQ" name="CARACTERISTIQUE_AKENEO" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1vO4TqUEemaHK9T15k6XQ" value="CARACTERISTIQUE_AKENEO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1vO4jqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1vO4zqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1vO5DqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1vO5TqUEemaHK9T15k6XQ" value="2048"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_s1vO5jqUEemaHK9T15k6XQ" name="CARACTERISTIQUE_NORME" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_s1vO5zqUEemaHK9T15k6XQ" value="CARACTERISTIQUE_NORME"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_s1vO6DqUEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_s1vO6TqUEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_s1vO6jqUEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_s1vO6zqUEemaHK9T15k6XQ" value="2048"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QSYj4AqtEeqIs-I1fHZ1cw" name="U00F20" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_QSYj4QqtEeqIs-I1fHZ1cw" value="U00F20"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QSYj4gqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QSYj4wqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QSYj5AqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QSYj5QqtEeqIs-I1fHZ1cw" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QSYj5gqtEeqIs-I1fHZ1cw" name="U00F30" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_QSYj5wqtEeqIs-I1fHZ1cw" value="U00F30"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QSYj6AqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QSYj6QqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QSYj6gqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QSYj6wqtEeqIs-I1fHZ1cw" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QSZK8AqtEeqIs-I1fHZ1cw" name="U00F40" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_QSZK8QqtEeqIs-I1fHZ1cw" value="U00F40"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QSZK8gqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QSZK8wqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QSZK9AqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QSZK9QqtEeqIs-I1fHZ1cw" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QSZK9gqtEeqIs-I1fHZ1cw" name="U00F90" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_QSZK9wqtEeqIs-I1fHZ1cw" value="U00F90"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QSZK-AqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QSZK-QqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QSZK-gqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QSZK-wqtEeqIs-I1fHZ1cw" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QSZyAAqtEeqIs-I1fHZ1cw" name="U00F91" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_QSZyAQqtEeqIs-I1fHZ1cw" value="U00F91"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QSZyAgqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QSZyAwqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QSZyBAqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QSZyBQqtEeqIs-I1fHZ1cw" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QSZyBgqtEeqIs-I1fHZ1cw" name="U00F92" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_QSZyBwqtEeqIs-I1fHZ1cw" value="U00F92"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QSZyCAqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QSZyCQqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QSZyCgqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QSZyCwqtEeqIs-I1fHZ1cw" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QSZyDAqtEeqIs-I1fHZ1cw" name="U00F93" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_QSZyDQqtEeqIs-I1fHZ1cw" value="U00F93"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QSZyDgqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QSZyDwqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QSZyEAqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QSZyEQqtEeqIs-I1fHZ1cw" value="250"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_AE1FMEWyEem8hIAZ2622yA" name="ORBDREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_AE1FMUWyEem8hIAZ2622yA" value="ORBDREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_AE1FMkWyEem8hIAZ2622yA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_AWmKoEWyEem8hIAZ2622yA" name="BDBCCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmKoUWyEem8hIAZ2622yA" value="BDBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmKokWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWmKo0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmKpEWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmKpUWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmKpkWyEem8hIAZ2622yA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmKp0WyEem8hIAZ2622yA" name="BDSTST" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmKqEWyEem8hIAZ2622yA" value="BDSTST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmKqUWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmKqkWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmKq0WyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmKrEWyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmKrUWyEem8hIAZ2622yA" name="BDJXNA" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmKrkWyEem8hIAZ2622yA" value="BDJXNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmKr0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmKsEWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmKsUWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmKskWyEem8hIAZ2622yA" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmKs0WyEem8hIAZ2622yA" name="BDNPCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmKtEWyEem8hIAZ2622yA" value="BDNPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmKtUWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmKtkWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmKt0WyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmKuEWyEem8hIAZ2622yA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmKuUWyEem8hIAZ2622yA" name="BDNQCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmKukWyEem8hIAZ2622yA" value="BDNQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmKu0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmKvEWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmKvUWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmKvkWyEem8hIAZ2622yA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmKv0WyEem8hIAZ2622yA" name="BDBSNA" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmKwEWyEem8hIAZ2622yA" value="BDBSNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmKwUWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmKwkWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmKw0WyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmKxEWyEem8hIAZ2622yA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmKxUWyEem8hIAZ2622yA" name="BDBTNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmKxkWyEem8hIAZ2622yA" value="BDBTNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmKx0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmKyEWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmKyUWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmKykWyEem8hIAZ2622yA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmKy0WyEem8hIAZ2622yA" name="BDA9TX" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmKzEWyEem8hIAZ2622yA" value="BDA9TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmKzUWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmKzkWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmKz0WyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmK0EWyEem8hIAZ2622yA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmK0UWyEem8hIAZ2622yA" name="BDFZTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmK0kWyEem8hIAZ2622yA" value="BDFZTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmK00WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmK1EWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmK1UWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmK1kWyEem8hIAZ2622yA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmK10WyEem8hIAZ2622yA" name="BDL2CD" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmK2EWyEem8hIAZ2622yA" value="BDL2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmK2UWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmK2kWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmK20WyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmK3EWyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmK3UWyEem8hIAZ2622yA" name="BDBHCD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmK3kWyEem8hIAZ2622yA" value="BDBHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmK30WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWmK4EWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmK4UWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmK4kWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmK40WyEem8hIAZ2622yA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmK5EWyEem8hIAZ2622yA" name="BDAMCD" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmK5UWyEem8hIAZ2622yA" value="BDAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmK5kWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmK50WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmK6EWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmK6UWyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmK6kWyEem8hIAZ2622yA" name="BDGHCD" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmK60WyEem8hIAZ2622yA" value="BDGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmK7EWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmK7UWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmK7kWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmK70WyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmK8EWyEem8hIAZ2622yA" name="BDAQCD" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmK8UWyEem8hIAZ2622yA" value="BDAQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmK8kWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmK80WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmK9EWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmK9UWyEem8hIAZ2622yA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWmK9kWyEem8hIAZ2622yA" name="BDAZCD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWmK90WyEem8hIAZ2622yA" value="BDAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWmK-EWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWmK-UWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWmK-kWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWmK-0WyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWmK_EWyEem8hIAZ2622yA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUkEWyEem8hIAZ2622yA" name="BDDNTX" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUkUWyEem8hIAZ2622yA" value="BDDNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUkkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvUk0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUlEWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvUlUWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvUlkWyEem8hIAZ2622yA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUl0WyEem8hIAZ2622yA" name="BDBJST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUmEWyEem8hIAZ2622yA" value="BDBJST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUmUWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUmkWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvUm0WyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvUnEWyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUnUWyEem8hIAZ2622yA" name="BDARDT" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUnkWyEem8hIAZ2622yA" value="BDARDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUn0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvUoEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUoUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvUokWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvUo0WyEem8hIAZ2622yA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUpEWyEem8hIAZ2622yA" name="BDASDT" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUpUWyEem8hIAZ2622yA" value="BDASDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUpkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvUp0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUqEWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvUqUWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvUqkWyEem8hIAZ2622yA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUq0WyEem8hIAZ2622yA" name="BDXCST" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUrEWyEem8hIAZ2622yA" value="BDXCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUrUWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUrkWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvUr0WyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvUsEWyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUsUWyEem8hIAZ2622yA" name="BDJBST" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUskWyEem8hIAZ2622yA" value="BDJBST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUs0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUtEWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvUtUWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvUtkWyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUt0WyEem8hIAZ2622yA" name="BDF0TX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUuEWyEem8hIAZ2622yA" value="BDF0TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUuUWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUukWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvUu0WyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvUvEWyEem8hIAZ2622yA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUvUWyEem8hIAZ2622yA" name="BDHTNB" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUvkWyEem8hIAZ2622yA" value="BDHTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUv0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvUwEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUwUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvUwkWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvUw0WyEem8hIAZ2622yA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUxEWyEem8hIAZ2622yA" name="BDAVDT" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUxUWyEem8hIAZ2622yA" value="BDAVDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUxkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvUx0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUyEWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvUyUWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvUykWyEem8hIAZ2622yA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvUy0WyEem8hIAZ2622yA" name="BDAWDT" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvUzEWyEem8hIAZ2622yA" value="BDAWDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvUzUWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvUzkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvUz0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvU0EWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvU0UWyEem8hIAZ2622yA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvU0kWyEem8hIAZ2622yA" name="BDJCST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvU00WyEem8hIAZ2622yA" value="BDJCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvU1EWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvU1UWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvU1kWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvU10WyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvU2EWyEem8hIAZ2622yA" name="BDJDST" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvU2UWyEem8hIAZ2622yA" value="BDJDST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvU2kWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvU20WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvU3EWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvU3UWyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvU3kWyEem8hIAZ2622yA" name="BDJEST" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvU30WyEem8hIAZ2622yA" value="BDJEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvU4EWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvU4UWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvU4kWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvU40WyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvU5EWyEem8hIAZ2622yA" name="BDDOPR" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvU5UWyEem8hIAZ2622yA" value="BDDOPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvU5kWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvU50WyEem8hIAZ2622yA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvU6EWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvU6UWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvU6kWyEem8hIAZ2622yA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvU60WyEem8hIAZ2622yA" name="BDDPPR" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvU7EWyEem8hIAZ2622yA" value="BDDPPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvU7UWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvU7kWyEem8hIAZ2622yA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvU70WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvU8EWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvU8UWyEem8hIAZ2622yA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvU8kWyEem8hIAZ2622yA" name="BDDQPR" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvU80WyEem8hIAZ2622yA" value="BDDQPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvU9EWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvU9UWyEem8hIAZ2622yA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvU9kWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvU90WyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvU-EWyEem8hIAZ2622yA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvU-UWyEem8hIAZ2622yA" name="BDF2NB" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvU-kWyEem8hIAZ2622yA" value="BDF2NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvU-0WyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvU_EWyEem8hIAZ2622yA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvU_UWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvU_kWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvU_0WyEem8hIAZ2622yA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVAEWyEem8hIAZ2622yA" name="BDF3NB" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVAUWyEem8hIAZ2622yA" value="BDF3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVAkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvVA0WyEem8hIAZ2622yA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVBEWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVBUWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVBkWyEem8hIAZ2622yA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVB0WyEem8hIAZ2622yA" name="BDF4NB" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVCEWyEem8hIAZ2622yA" value="BDF4NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVCUWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_AWvVCkWyEem8hIAZ2622yA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVC0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVDEWyEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVDUWyEem8hIAZ2622yA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVDkWyEem8hIAZ2622yA" name="BDTSST" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVD0WyEem8hIAZ2622yA" value="BDTSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVEEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVEUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVEkWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVE0WyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVFEWyEem8hIAZ2622yA" name="BDC6N3" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVFUWyEem8hIAZ2622yA" value="BDC6N3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVFkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVF0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVGEWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVGUWyEem8hIAZ2622yA" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVGkWyEem8hIAZ2622yA" name="BDC7N3" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVG0WyEem8hIAZ2622yA" value="BDC7N3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVHEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVHUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVHkWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVH0WyEem8hIAZ2622yA" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVIEWyEem8hIAZ2622yA" name="BDKSTX" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVIUWyEem8hIAZ2622yA" value="BDKSTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVIkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVI0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVJEWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVJUWyEem8hIAZ2622yA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVJkWyEem8hIAZ2622yA" name="BDKTTX" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVJ0WyEem8hIAZ2622yA" value="BDKTTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVKEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVKUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVKkWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVK0WyEem8hIAZ2622yA" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVLEWyEem8hIAZ2622yA" name="BDAKCD" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVLUWyEem8hIAZ2622yA" value="BDAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVLkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVL0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVMEWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVMUWyEem8hIAZ2622yA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVMkWyEem8hIAZ2622yA" name="BDKVTX" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVM0WyEem8hIAZ2622yA" value="BDKVTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVNEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVNUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVNkWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVN0WyEem8hIAZ2622yA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVOEWyEem8hIAZ2622yA" name="BDKWTX" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVOUWyEem8hIAZ2622yA" value="BDKWTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVOkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVO0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVPEWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVPUWyEem8hIAZ2622yA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVPkWyEem8hIAZ2622yA" name="BDZUST" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVP0WyEem8hIAZ2622yA" value="BDZUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVQEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVQUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVQkWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVQ0WyEem8hIAZ2622yA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVREWyEem8hIAZ2622yA" name="BDWNTX" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVRUWyEem8hIAZ2622yA" value="BDWNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVRkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVR0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVSEWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVSUWyEem8hIAZ2622yA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVSkWyEem8hIAZ2622yA" name="BDWOTX" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVS0WyEem8hIAZ2622yA" value="BDWOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVTEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVTUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVTkWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVT0WyEem8hIAZ2622yA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVUEWyEem8hIAZ2622yA" name="BDWPTX" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVUUWyEem8hIAZ2622yA" value="BDWPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVUkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVU0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVVEWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVVUWyEem8hIAZ2622yA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVVkWyEem8hIAZ2622yA" name="BDWQTX" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVV0WyEem8hIAZ2622yA" value="BDWQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVWEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVWUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVWkWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVW0WyEem8hIAZ2622yA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AWvVXEWyEem8hIAZ2622yA" name="BDWRTX" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_AWvVXUWyEem8hIAZ2622yA" value="BDWRTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AWvVXkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AWvVX0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AWvVYEWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AWvVYUWyEem8hIAZ2622yA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AW5FkEWyEem8hIAZ2622yA" name="BDWSTX" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_AW5FkUWyEem8hIAZ2622yA" value="BDWSTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AW5FkkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AW5Fk0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AW5FlEWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AW5FlUWyEem8hIAZ2622yA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AW5FlkWyEem8hIAZ2622yA" name="BDWTTX" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_AW5Fl0WyEem8hIAZ2622yA" value="BDWTTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AW5FmEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AW5FmUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AW5FmkWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AW5Fm0WyEem8hIAZ2622yA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AW5FnEWyEem8hIAZ2622yA" name="BDWUTX" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_AW5FnUWyEem8hIAZ2622yA" value="BDWUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AW5FnkWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AW5Fn0WyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AW5FoEWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AW5FoUWyEem8hIAZ2622yA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AW5FokWyEem8hIAZ2622yA" name="BDWVTX" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_AW5Fo0WyEem8hIAZ2622yA" value="BDWVTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AW5FpEWyEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AW5FpUWyEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AW5FpkWyEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AW5Fp0WyEem8hIAZ2622yA" value="25"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_8WA2gEwGEemn-PTqagJaAg" name="WEBSUIVI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_8WA2gUwGEemn-PTqagJaAg" value="WEBSUIVI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_8WA2gkwGEemn-PTqagJaAg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_8e2CYEwGEemn-PTqagJaAg" name="WSAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_8e2CYUwGEemn-PTqagJaAg" value="WSAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8e2CYkwGEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_8e2CY0wGEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8e2CZEwGEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8e2CZUwGEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8e2CZkwGEemn-PTqagJaAg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8e2CZ0wGEemn-PTqagJaAg" name="WSDATE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_8e2CaEwGEemn-PTqagJaAg" value="WSDATE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8e2CaUwGEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_8e2CakwGEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8e2Ca0wGEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8e2CbEwGEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8e2CbUwGEemn-PTqagJaAg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8e2CbkwGEemn-PTqagJaAg" name="WSHEURE" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_8e2Cb0wGEemn-PTqagJaAg" value="WSHEURE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8e2CcEwGEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_8e2CcUwGEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8e2CckwGEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8e2Cc0wGEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8e2CdEwGEemn-PTqagJaAg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8e2CdUwGEemn-PTqagJaAg" name="WSTYPE" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_8e2CdkwGEemn-PTqagJaAg" value="WSTYPE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8e2Cd0wGEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_8e2CeEwGEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8e2CeUwGEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8e2CekwGEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8e2Ce0wGEemn-PTqagJaAg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8e2CfEwGEemn-PTqagJaAg" name="WSIDWEBC" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_8e2CfUwGEemn-PTqagJaAg" value="WSIDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8e2CfkwGEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8e2Cf0wGEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8e2CgEwGEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8e2CgUwGEemn-PTqagJaAg" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_SXiQ8Ga2EemwHJMcwY694w" name="OSI6REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_SXiQ8Wa2EemwHJMcwY694w" value="OSI6REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_SXiQ8ma2EemwHJMcwY694w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_SXra4Ga2EemwHJMcwY694w" name="I6GHCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_SXra4Wa2EemwHJMcwY694w" value="I6GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SXra4ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SXra42a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SXra5Ga2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SXra5Wa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SXsB8Ga2EemwHJMcwY694w" name="I6AQCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_SXsB8Wa2EemwHJMcwY694w" value="I6AQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SXsB8ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SXsB82a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SXsB9Ga2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SXsB9Wa2EemwHJMcwY694w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SXsB9ma2EemwHJMcwY694w" name="I6PFP2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_SXsB92a2EemwHJMcwY694w" value="I6PFP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SXsB-Ga2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SXsB-Wa2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SXsB-ma2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SXsB-2a2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SXsB_Ga2EemwHJMcwY694w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SXspAGa2EemwHJMcwY694w" name="I6PGP2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_SXspAWa2EemwHJMcwY694w" value="I6PGP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SXspAma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SXspA2a2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SXspBGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SXspBWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SXspBma2EemwHJMcwY694w" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_SYZMkWa2EemwHJMcwY694w" name="OSI4REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_SYZMkma2EemwHJMcwY694w" value="OSI4REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_SYZMk2a2EemwHJMcwY694w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_SYt8sGa2EemwHJMcwY694w" name="I4FTNB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYt8sWa2EemwHJMcwY694w" value="I4FTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYt8sma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYt8s2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYt8tGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYt8tWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYt8tma2EemwHJMcwY694w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYt8t2a2EemwHJMcwY694w" name="I4O8P2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYt8uGa2EemwHJMcwY694w" value="I4O8P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYt8uWa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYt8uma2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYt8u2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYt8vGa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYt8vWa2EemwHJMcwY694w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYujwGa2EemwHJMcwY694w" name="I4O9P2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYujwWa2EemwHJMcwY694w" value="I4O9P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYujwma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYujw2a2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYujxGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYujxWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYujxma2EemwHJMcwY694w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYujx2a2EemwHJMcwY694w" name="I4PAP2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYujyGa2EemwHJMcwY694w" value="I4PAP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYujyWa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYujyma2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYujy2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYujzGa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYujzWa2EemwHJMcwY694w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYujzma2EemwHJMcwY694w" name="I4SUN5" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYujz2a2EemwHJMcwY694w" value="I4SUN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYuj0Ga2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYuj0Wa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYuj0ma2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYuj02a2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYuj1Ga2EemwHJMcwY694w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYvK0Ga2EemwHJMcwY694w" name="I4PBP2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYvK0Wa2EemwHJMcwY694w" value="I4PBP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYvK0ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYvK02a2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYvK1Ga2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYvK1Wa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYvK1ma2EemwHJMcwY694w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYvK12a2EemwHJMcwY694w" name="I4PCP2" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYvK2Ga2EemwHJMcwY694w" value="I4PCP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYvK2Wa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYvK2ma2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYvK22a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYvK3Ga2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYvK3Wa2EemwHJMcwY694w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYvK3ma2EemwHJMcwY694w" name="I4SVN5" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYvK32a2EemwHJMcwY694w" value="I4SVN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYvK4Ga2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYvK4Wa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYvK4ma2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYvK42a2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYvK5Ga2EemwHJMcwY694w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYvx4Ga2EemwHJMcwY694w" name="I4SWN5" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYvx4Wa2EemwHJMcwY694w" value="I4SWN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYvx4ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYvx42a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYvx5Ga2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYvx5Wa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYvx5ma2EemwHJMcwY694w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYvx52a2EemwHJMcwY694w" name="I4UNS2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYvx6Ga2EemwHJMcwY694w" value="I4UNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYvx6Wa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYvx6ma2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYvx62a2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYvx7Ga2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYwY8Ga2EemwHJMcwY694w" name="I4UOS2" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYwY8Wa2EemwHJMcwY694w" value="I4UOS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYwY8ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYwY82a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYwY9Ga2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYwY9Wa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYwY9ma2EemwHJMcwY694w" name="I4GST2" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYwY92a2EemwHJMcwY694w" value="I4GST2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYwY-Ga2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYwY-Wa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYwY-ma2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYwY-2a2EemwHJMcwY694w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYwY_Ga2EemwHJMcwY694w" name="I4OXPC" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYwY_Wa2EemwHJMcwY694w" value="I4OXPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYwY_ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYwY_2a2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYwZAGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYwZAWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYwZAma2EemwHJMcwY694w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYxAAGa2EemwHJMcwY694w" name="I4OYPC" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYxAAWa2EemwHJMcwY694w" value="I4OYPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYxAAma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYxAA2a2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYxABGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYxABWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYxABma2EemwHJMcwY694w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYxAB2a2EemwHJMcwY694w" name="I4OZPC" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYxACGa2EemwHJMcwY694w" value="I4OZPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYxACWa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYxACma2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYxAC2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYxADGa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYxADWa2EemwHJMcwY694w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYxADma2EemwHJMcwY694w" name="I4GTT2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYxAD2a2EemwHJMcwY694w" value="I4GTT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYxAEGa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYxAEWa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYxAEma2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYxAE2a2EemwHJMcwY694w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYxnEGa2EemwHJMcwY694w" name="I4GUT2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYxnEWa2EemwHJMcwY694w" value="I4GUT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYxnEma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYxnE2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYxnFGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYxnFWa2EemwHJMcwY694w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYxnFma2EemwHJMcwY694w" name="I4GVT2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYxnF2a2EemwHJMcwY694w" value="I4GVT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYxnGGa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYxnGWa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYxnGma2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYxnG2a2EemwHJMcwY694w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYxnHGa2EemwHJMcwY694w" name="I4EFN6" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYxnHWa2EemwHJMcwY694w" value="I4EFN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYxnHma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYxnH2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYxnIGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYxnIWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYxnIma2EemwHJMcwY694w" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYyOIGa2EemwHJMcwY694w" name="I4ABCD" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYyOIWa2EemwHJMcwY694w" value="I4ABCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYyOIma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYyOI2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYyOJGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYyOJWa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYyOJma2EemwHJMcwY694w" name="I4ACCD" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYyOJ2a2EemwHJMcwY694w" value="I4ACCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYyOKGa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYyOKWa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYyOKma2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYyOK2a2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYyOLGa2EemwHJMcwY694w" name="I4NBC1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYyOLWa2EemwHJMcwY694w" value="I4NBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYy1MGa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYy1MWa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYy1Mma2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYy1M2a2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYy1NGa2EemwHJMcwY694w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYy1NWa2EemwHJMcwY694w" name="I4ZQS2" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYy1Nma2EemwHJMcwY694w" value="I4ZQS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYy1N2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYy1OGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYy1OWa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYy1Oma2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYy1O2a2EemwHJMcwY694w" name="I4AZCD" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYy1PGa2EemwHJMcwY694w" value="I4AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYy1PWa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYy1Pma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYy1P2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYy1QGa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYy1QWa2EemwHJMcwY694w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYzcQGa2EemwHJMcwY694w" name="I4EGN6" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYzcQWa2EemwHJMcwY694w" value="I4EGN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYzcQma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SYzcQ2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYzcRGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYzcRWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYzcRma2EemwHJMcwY694w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYzcR2a2EemwHJMcwY694w" name="I4ZRS2" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYzcSGa2EemwHJMcwY694w" value="I4ZRS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYzcSWa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYzcSma2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYzcS2a2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYzcTGa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SYzcTWa2EemwHJMcwY694w" name="I4NCC1" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_SYzcTma2EemwHJMcwY694w" value="I4NCC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SYzcT2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SYzcUGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SYzcUWa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SYzcUma2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY0DUGa2EemwHJMcwY694w" name="I4NDC1" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY0DUWa2EemwHJMcwY694w" value="I4NDC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY0DUma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY0DU2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY0DVGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY0DVWa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY0qYGa2EemwHJMcwY694w" name="I4ZSS2" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY0qYWa2EemwHJMcwY694w" value="I4ZSS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY0qYma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY0qY2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY0qZGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY0qZWa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY0qZma2EemwHJMcwY694w" name="I4IGS3" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY0qZ2a2EemwHJMcwY694w" value="I4IGS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY0qaGa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY0qaWa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY0qama2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY0qa2a2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY0qbGa2EemwHJMcwY694w" name="I4IIS3" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY0qbWa2EemwHJMcwY694w" value="I4IIS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY0qbma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY0qb2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY0qcGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY0qcWa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY1RcGa2EemwHJMcwY694w" name="I4IJS3" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY1RcWa2EemwHJMcwY694w" value="I4IJS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY1Rcma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY1Rc2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY1RdGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY1RdWa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY1Rdma2EemwHJMcwY694w" name="I4IKS3" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY1Rd2a2EemwHJMcwY694w" value="I4IKS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY1ReGa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY1ReWa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY1Rema2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY1Re2a2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY1RfGa2EemwHJMcwY694w" name="I4RYC1" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY1RfWa2EemwHJMcwY694w" value="I4RYC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY1Rfma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY1Rf2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY1RgGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY1RgWa2EemwHJMcwY694w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY14gGa2EemwHJMcwY694w" name="I4KSN6" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY14gWa2EemwHJMcwY694w" value="I4KSN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY14gma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SY14g2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY14hGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY14hWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY14hma2EemwHJMcwY694w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY14h2a2EemwHJMcwY694w" name="I4NKT2" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY14iGa2EemwHJMcwY694w" value="I4NKT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY14iWa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY14ima2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY14i2a2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY14jGa2EemwHJMcwY694w" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY2fkGa2EemwHJMcwY694w" name="I4ILS3" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY2fkWa2EemwHJMcwY694w" value="I4ILS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY2fkma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY2fk2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY2flGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY2flWa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY2flma2EemwHJMcwY694w" name="I4RZC1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY2fl2a2EemwHJMcwY694w" value="I4RZC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY2fmGa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY2fmWa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY2fmma2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY2fm2a2EemwHJMcwY694w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY2fnGa2EemwHJMcwY694w" name="I4KTN6" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY2fnWa2EemwHJMcwY694w" value="I4KTN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY2fnma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SY2fn2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY2foGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY2foWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY2foma2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY3GoGa2EemwHJMcwY694w" name="I4KUN6" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY3GoWa2EemwHJMcwY694w" value="I4KUN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY3Goma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SY3Go2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY3GpGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY3GpWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY3Gpma2EemwHJMcwY694w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY3Gp2a2EemwHJMcwY694w" name="I4NLT2" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY3GqGa2EemwHJMcwY694w" value="I4NLT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY3GqWa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY3Gqma2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY3Gq2a2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY3GrGa2EemwHJMcwY694w" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY3GrWa2EemwHJMcwY694w" name="I4IHS3" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY3Grma2EemwHJMcwY694w" value="I4IHS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY3Gr2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY3GsGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY3GsWa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY3Gsma2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY3tsGa2EemwHJMcwY694w" name="I4R0C1" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY3tsWa2EemwHJMcwY694w" value="I4R0C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY3tsma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY3ts2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY3ttGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY3ttWa2EemwHJMcwY694w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY3ttma2EemwHJMcwY694w" name="I4LNS3" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY3tt2a2EemwHJMcwY694w" value="I4LNS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY3tuGa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY3tuWa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY3tuma2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY3tu2a2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY4UwGa2EemwHJMcwY694w" name="I4UWC1" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY4UwWa2EemwHJMcwY694w" value="I4UWC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY4Uwma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY4Uw2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY4UxGa2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY4UxWa2EemwHJMcwY694w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY4Uxma2EemwHJMcwY694w" name="I4LOS3" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY4Ux2a2EemwHJMcwY694w" value="I4LOS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY4UyGa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY4UyWa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY4Uyma2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY4Uy2a2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY4UzGa2EemwHJMcwY694w" name="I4Q5T2" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY4UzWa2EemwHJMcwY694w" value="I4Q5T2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY4Uzma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY4Uz2a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY4U0Ga2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY4U0Wa2EemwHJMcwY694w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY470Ga2EemwHJMcwY694w" name="I4W4DT" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY470Wa2EemwHJMcwY694w" value="I4W4DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY470ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SY4702a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY471Ga2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY471Wa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY471ma2EemwHJMcwY694w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY4712a2EemwHJMcwY694w" name="I4W5DT" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY472Ga2EemwHJMcwY694w" value="I4W5DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY472Wa2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SY472ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY4722a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY473Ga2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY473Wa2EemwHJMcwY694w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY473ma2EemwHJMcwY694w" name="I4NLN3" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY4732a2EemwHJMcwY694w" value="I4NLN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY474Ga2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY474Wa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY474ma2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY4742a2EemwHJMcwY694w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY5i4Ga2EemwHJMcwY694w" name="I4NMN3" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY5i4Wa2EemwHJMcwY694w" value="I4NMN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY5i4ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY5i42a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY5i5Ga2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY5i5Wa2EemwHJMcwY694w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY5i5ma2EemwHJMcwY694w" name="I4NNN3" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY5i52a2EemwHJMcwY694w" value="I4NNN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY5i6Ga2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY5i6Wa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY5i6ma2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY5i62a2EemwHJMcwY694w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY5i7Ga2EemwHJMcwY694w" name="I4NON3" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY5i7Wa2EemwHJMcwY694w" value="I4NON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY5i7ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY5i72a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY5i8Ga2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY5i8Wa2EemwHJMcwY694w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SY6J8Ga2EemwHJMcwY694w" name="I4NPN3" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_SY6J8Wa2EemwHJMcwY694w" value="I4NPN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SY6J8ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SY6J82a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SY6J9Ga2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SY6J9Wa2EemwHJMcwY694w" value="35"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_SBQe8Wa2EemwHJMcwY694w" name="OSI5REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_SBQe8ma2EemwHJMcwY694w" value="OSI5REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_SBQe82a2EemwHJMcwY694w" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_SB3i8Ga2EemwHJMcwY694w" name="I5GHCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_SB3i8Wa2EemwHJMcwY694w" value="I5GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SB3i8ma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SB3i82a2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SB3i9Ga2EemwHJMcwY694w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SB3i9Wa2EemwHJMcwY694w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SB4xEGa2EemwHJMcwY694w" name="I5PDP2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_SB4xEWa2EemwHJMcwY694w" value="I5PDP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SB4xEma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SB4xE2a2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SB4xFGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SB4xFWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SB4xFma2EemwHJMcwY694w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SB5YIGa2EemwHJMcwY694w" name="I5PEP2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_SB5YIWa2EemwHJMcwY694w" value="I5PEP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SB5YIma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SB5YI2a2EemwHJMcwY694w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SB5YJGa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SB5YJWa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SB5YJma2EemwHJMcwY694w" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_PZ3zoYweEemLBMgyIKzxTw" name="PIXEL_MAGENTO_CLIENTS_MAILS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_PZ3zooweEemLBMgyIKzxTw" value="PIXEL_MAGENTO_CLIENTS_MAILS"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_PZ3zo4weEemLBMgyIKzxTw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_PlrbAIweEemLBMgyIKzxTw" name="CODE_CLIENT" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_PlrbAYweEemLBMgyIKzxTw" value="CODE_CLIENT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_PlrbAoweEemLBMgyIKzxTw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Pl1MAIweEemLBMgyIKzxTw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Pl1MAYweEemLBMgyIKzxTw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Pl1MAoweEemLBMgyIKzxTw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Pl1MA4weEemLBMgyIKzxTw" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Pl1MBIweEemLBMgyIKzxTw" name="STATUT" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Pl1MBYweEemLBMgyIKzxTw" value="STATUT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Pl1MBoweEemLBMgyIKzxTw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Pl1MB4weEemLBMgyIKzxTw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Pl1MCIweEemLBMgyIKzxTw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Pl1MCYweEemLBMgyIKzxTw" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Pl1MCoweEemLBMgyIKzxTw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Pl1MC4weEemLBMgyIKzxTw" name="MAIL_MAGENTO" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Pl1MDIweEemLBMgyIKzxTw" value="MAIL_MAGENTO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Pl1MDYweEemLBMgyIKzxTw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Pl1MDoweEemLBMgyIKzxTw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Pl1MD4weEemLBMgyIKzxTw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Pl1MEIweEemLBMgyIKzxTw" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Pl1MEYweEemLBMgyIKzxTw" name="MAIL_MYCHOMETTE" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Pl1MEoweEemLBMgyIKzxTw" value="MAIL_MYCHOMETTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Pl1ME4weEemLBMgyIKzxTw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Pl1MFIweEemLBMgyIKzxTw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Pl1MFYweEemLBMgyIKzxTw" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Pl1MFoweEemLBMgyIKzxTw" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_v9B_kJZgEem6jcmdEfXK_g" name="ORQ3REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_v9B_kZZgEem6jcmdEfXK_g" value="ORQ3REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_v9CmoJZgEem6jcmdEfXK_g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_wJ7zIJZgEem6jcmdEfXK_g" name="Q3AZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_wJ7zIZZgEem6jcmdEfXK_g" value="Q3AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wJ7zIpZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wJ7zI5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wJ7zJJZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wJ7zJZZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wJ7zJpZgEem6jcmdEfXK_g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9EJZgEem6jcmdEfXK_g" name="Q3VTN2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9EZZgEem6jcmdEfXK_g" value="Q3VTN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9EpZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9E5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9FJZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9FZZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9FpZgEem6jcmdEfXK_g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9F5ZgEem6jcmdEfXK_g" name="Q3VUN2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9GJZgEem6jcmdEfXK_g" value="Q3VUN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9GZZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9GpZgEem6jcmdEfXK_g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9G5ZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9HJZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9HZZgEem6jcmdEfXK_g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9HpZgEem6jcmdEfXK_g" name="Q3VVN2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9H5ZgEem6jcmdEfXK_g" value="Q3VVN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9IJZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9IZZgEem6jcmdEfXK_g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9IpZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9I5ZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9JJZgEem6jcmdEfXK_g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9JZZgEem6jcmdEfXK_g" name="Q3VWN2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9JpZgEem6jcmdEfXK_g" value="Q3VWN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9J5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9KJZgEem6jcmdEfXK_g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9KZZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9KpZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9K5ZgEem6jcmdEfXK_g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9LJZgEem6jcmdEfXK_g" name="Q3LMS2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9LZZgEem6jcmdEfXK_g" value="Q3LMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9LpZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9L5ZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9MJZgEem6jcmdEfXK_g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9MZZgEem6jcmdEfXK_g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9MpZgEem6jcmdEfXK_g" name="Q3KODT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9M5ZgEem6jcmdEfXK_g" value="Q3KODT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9NJZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9NZZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9NpZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9N5ZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9OJZgEem6jcmdEfXK_g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9OZZgEem6jcmdEfXK_g" name="Q3VXN2" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9OpZgEem6jcmdEfXK_g" value="Q3VXN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9O5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9PJZgEem6jcmdEfXK_g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9PZZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9PpZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9P5ZgEem6jcmdEfXK_g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9QJZgEem6jcmdEfXK_g" name="Q3VYN2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9QZZgEem6jcmdEfXK_g" value="Q3VYN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9QpZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9Q5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9RJZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9RZZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9RpZgEem6jcmdEfXK_g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9R5ZgEem6jcmdEfXK_g" name="Q3VZN2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9SJZgEem6jcmdEfXK_g" value="Q3VZN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9SZZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9SpZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9S5ZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9TJZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9TZZgEem6jcmdEfXK_g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9TpZgEem6jcmdEfXK_g" name="Q3V0N2" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9T5ZgEem6jcmdEfXK_g" value="Q3V0N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9UJZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9UZZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9UpZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9U5ZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9VJZgEem6jcmdEfXK_g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9VZZgEem6jcmdEfXK_g" name="Q3V1N2" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9VpZgEem6jcmdEfXK_g" value="Q3V1N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9V5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9WJZgEem6jcmdEfXK_g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9WZZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9WpZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9W5ZgEem6jcmdEfXK_g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9XJZgEem6jcmdEfXK_g" name="Q3V2N2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9XZZgEem6jcmdEfXK_g" value="Q3V2N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9XpZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9X5ZgEem6jcmdEfXK_g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9YJZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9YZZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9YpZgEem6jcmdEfXK_g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9Y5ZgEem6jcmdEfXK_g" name="Q3V3N2" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9ZJZgEem6jcmdEfXK_g" value="Q3V3N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9ZZZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9ZpZgEem6jcmdEfXK_g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9Z5ZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9aJZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9aZZgEem6jcmdEfXK_g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9apZgEem6jcmdEfXK_g" name="Q3V4N2" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9a5ZgEem6jcmdEfXK_g" value="Q3V4N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9bJZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9bZZgEem6jcmdEfXK_g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9bpZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9b5ZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9cJZgEem6jcmdEfXK_g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9cZZgEem6jcmdEfXK_g" name="Q3V5N2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9cpZgEem6jcmdEfXK_g" value="Q3V5N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9c5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9dJZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9dZZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9dpZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9d5ZgEem6jcmdEfXK_g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9eJZgEem6jcmdEfXK_g" name="Q3V6N2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9eZZgEem6jcmdEfXK_g" value="Q3V6N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9epZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9e5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9fJZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9fZZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9fpZgEem6jcmdEfXK_g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9f5ZgEem6jcmdEfXK_g" name="Q3V7N2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9gJZgEem6jcmdEfXK_g" value="Q3V7N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9gZZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9gpZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9g5ZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9hJZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9hZZgEem6jcmdEfXK_g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9hpZgEem6jcmdEfXK_g" name="Q3V8N2" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9h5ZgEem6jcmdEfXK_g" value="Q3V8N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9iJZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9iZZgEem6jcmdEfXK_g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9ipZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9i5ZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9jJZgEem6jcmdEfXK_g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9jZZgEem6jcmdEfXK_g" name="Q3KPDT" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9jpZgEem6jcmdEfXK_g" value="Q3KPDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9j5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9kJZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9kZZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9kpZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9k5ZgEem6jcmdEfXK_g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9lJZgEem6jcmdEfXK_g" name="Q3LNS2" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9lZZgEem6jcmdEfXK_g" value="Q3LNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9lpZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9l5ZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9mJZgEem6jcmdEfXK_g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9mZZgEem6jcmdEfXK_g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9mpZgEem6jcmdEfXK_g" name="Q3V9N2" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9m5ZgEem6jcmdEfXK_g" value="Q3V9N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9nJZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9nZZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9npZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9n5ZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9oJZgEem6jcmdEfXK_g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9oZZgEem6jcmdEfXK_g" name="Q3WAN2" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9opZgEem6jcmdEfXK_g" value="Q3WAN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9o5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9pJZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9pZZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9ppZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9p5ZgEem6jcmdEfXK_g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9qJZgEem6jcmdEfXK_g" name="Q3KQDT" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9qZZgEem6jcmdEfXK_g" value="Q3KQDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9qpZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wKE9q5ZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9rJZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9rZZgEem6jcmdEfXK_g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9rpZgEem6jcmdEfXK_g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wKE9r5ZgEem6jcmdEfXK_g" name="Q3QHNA" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_wKE9sJZgEem6jcmdEfXK_g" value="Q3QHNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wKE9sZZgEem6jcmdEfXK_g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wKE9spZgEem6jcmdEfXK_g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wKE9s5ZgEem6jcmdEfXK_g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wKE9tJZgEem6jcmdEfXK_g" value="60"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_F_VuMLICEemOpe_YQX7pBw" name="PIX_CLIENT_1ERE_COMMANDE">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_F_VuMbICEemOpe_YQX7pBw" value="PIX_CLIENT_1ERE_COMMANDE"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_F_WVQLICEemOpe_YQX7pBw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_GPrQQLICEemOpe_YQX7pBw" name="P1CCLT" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_GPrQQbICEemOpe_YQX7pBw" value="P1CCLT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GPrQQrICEemOpe_YQX7pBw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_GPrQQ7ICEemOpe_YQX7pBw" value="Code client"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_GPvhsLICEemOpe_YQX7pBw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GPvhsbICEemOpe_YQX7pBw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GPvhsrICEemOpe_YQX7pBw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GPvhs7ICEemOpe_YQX7pBw" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GPwv0LICEemOpe_YQX7pBw" name="P1CDTE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_GPwv0bICEemOpe_YQX7pBw" value="P1CDTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GPwv0rICEemOpe_YQX7pBw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_GPwv07ICEemOpe_YQX7pBw" value="Date 1ere commande"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_GPwv1LICEemOpe_YQX7pBw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GPwv1bICEemOpe_YQX7pBw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GPwv1rICEemOpe_YQX7pBw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GPwv17ICEemOpe_YQX7pBw" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GPxW4LICEemOpe_YQX7pBw" name="P1CCANAL" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_GPxW4bICEemOpe_YQX7pBw" value="P1CCANAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GPxW4rICEemOpe_YQX7pBw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_GPxW47ICEemOpe_YQX7pBw" value="Mode de Passation 1ere Commande"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GPxW5LICEemOpe_YQX7pBw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GPxW5bICEemOpe_YQX7pBw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GPxW5rICEemOpe_YQX7pBw" value="20"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_IkG_ENVDEem5ebMPg-wyFg" name="ORGPREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_IkG_EdVDEem5ebMPg-wyFg" value="ORGPREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_IkG_EtVDEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_Ik-hwNVDEem5ebMPg-wyFg" name="GPFTNB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ik-hwdVDEem5ebMPg-wyFg" value="GPFTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ik-hwtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ik-hw9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ik-hxNVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ik-hxdVDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ik-hxtVDEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ik-hx9VDEem5ebMPg-wyFg" name="GPAFCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ik-hyNVDEem5ebMPg-wyFg" value="GPAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ik-hydVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ik-hytVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ik-hy9VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ik-hzNVDEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ik-hzdVDEem5ebMPg-wyFg" name="GPAGCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ik-hztVDEem5ebMPg-wyFg" value="GPAGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ik-hz9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ik-h0NVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ik-h0dVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ik-h0tVDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ik-h09VDEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ik-h1NVDEem5ebMPg-wyFg" name="GPAKCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ik-h1dVDEem5ebMPg-wyFg" value="GPAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ik-h1tVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ik-h19VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ik-h2NVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ik-h2dVDEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ik-h2tVDEem5ebMPg-wyFg" name="GPAMCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ik-h29VDEem5ebMPg-wyFg" value="GPAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ik-h3NVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ik-h3dVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ik-h3tVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ik-h39VDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ik-h4NVDEem5ebMPg-wyFg" name="GPBCCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ik-h4dVDEem5ebMPg-wyFg" value="GPBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ik-h4tVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ik-h49VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ik-h5NVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ik-h5dVDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ik-h5tVDEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ik-h59VDEem5ebMPg-wyFg" name="GPBOCD" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ik-h6NVDEem5ebMPg-wyFg" value="GPBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ik-h6dVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ik-h6tVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ik-h69VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ik-h7NVDEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ik-h7dVDEem5ebMPg-wyFg" name="GPHRST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ik-h7tVDEem5ebMPg-wyFg" value="GPHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ik-h79VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ik-h8NVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ik-h8dVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ik-h8tVDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ik-h89VDEem5ebMPg-wyFg" name="GPFJTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ik-h9NVDEem5ebMPg-wyFg" value="GPFJTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ik-h9dVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ik-h9tVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ik-h99VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ik-h-NVDEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlISwNVDEem5ebMPg-wyFg" name="GPFKTX" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlISwdVDEem5ebMPg-wyFg" value="GPFKTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlISwtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlISw9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlISxNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlISxdVDEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlISxtVDEem5ebMPg-wyFg" name="GPBLPC" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlISx9VDEem5ebMPg-wyFg" value="GPBLPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlISyNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_IlISydVDEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlISytVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlISy9VDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlISzNVDEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlISzdVDEem5ebMPg-wyFg" name="GPBMPC" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlISztVDEem5ebMPg-wyFg" value="GPBMPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlISz9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_IlIS0NVDEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlIS0dVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlIS0tVDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlIS09VDEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlIS1NVDEem5ebMPg-wyFg" name="GPITST" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlIS1dVDEem5ebMPg-wyFg" value="GPITST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlIS1tVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlIS19VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlIS2NVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlIS2dVDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlIS2tVDEem5ebMPg-wyFg" name="GPIUST" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlIS29VDEem5ebMPg-wyFg" value="GPIUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlIS3NVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlIS3dVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlIS3tVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlIS39VDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlIS4NVDEem5ebMPg-wyFg" name="GPIVST" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlIS4dVDEem5ebMPg-wyFg" value="GPIVST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlIS4tVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlIS49VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlIS5NVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlIS5dVDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlIS5tVDEem5ebMPg-wyFg" name="GPEIDT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlIS59VDEem5ebMPg-wyFg" value="GPEIDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlIS6NVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_IlIS6dVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlIS6tVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlIS69VDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlIS7NVDEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlIS7dVDEem5ebMPg-wyFg" name="GPLKST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlIS7tVDEem5ebMPg-wyFg" value="GPLKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlIS79VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlIS8NVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlIS8dVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlIS8tVDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlIS89VDEem5ebMPg-wyFg" name="GPBSN3" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlIS9NVDEem5ebMPg-wyFg" value="GPBSN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlIS9dVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlIS9tVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlIS99VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlIS-NVDEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlIS-dVDEem5ebMPg-wyFg" name="GPBTN3" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlIS-tVDEem5ebMPg-wyFg" value="GPBTN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlIS-9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlIS_NVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlIS_dVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlIS_tVDEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlIS_9VDEem5ebMPg-wyFg" name="GPHRTX" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlITANVDEem5ebMPg-wyFg" value="GPHRTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlITAdVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlITAtVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlITA9VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlITBNVDEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlITBdVDEem5ebMPg-wyFg" name="GPHSTX" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlITBtVDEem5ebMPg-wyFg" value="GPHSTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlITB9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlITCNVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlITCdVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlITCtVDEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlITC9VDEem5ebMPg-wyFg" name="GPITNA" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlITDNVDEem5ebMPg-wyFg" value="GPITNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlITDdVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlITDtVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlITD9VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlITENVDEem5ebMPg-wyFg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlITEdVDEem5ebMPg-wyFg" name="GPHTTX" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlITEtVDEem5ebMPg-wyFg" value="GPHTTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlITE9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlITFNVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlITFdVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlITFtVDEem5ebMPg-wyFg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlITF9VDEem5ebMPg-wyFg" name="GPHUTX" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlITGNVDEem5ebMPg-wyFg" value="GPHUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlITGdVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlITGtVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlITG9VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlITHNVDEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlITHdVDEem5ebMPg-wyFg" name="GPNXCD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlITHtVDEem5ebMPg-wyFg" value="GPNXCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlITH9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlITINVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlITIdVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlITItVDEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlITI9VDEem5ebMPg-wyFg" name="GPNYCD" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlITJNVDEem5ebMPg-wyFg" value="GPNYCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlITJdVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlITJtVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlITJ9VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlITKNVDEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlITKdVDEem5ebMPg-wyFg" name="GPAADT" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlITKtVDEem5ebMPg-wyFg" value="GPAADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlITK9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_IlITLNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlITLdVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlITLtVDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlITL9VDEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IlITMNVDEem5ebMPg-wyFg" name="GPAATX" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_IlITMdVDEem5ebMPg-wyFg" value="GPAATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IlITMtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IlITM9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IlITNNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IlITNdVDEem5ebMPg-wyFg" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_ILbZUdVDEem5ebMPg-wyFg" name="ORGKREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_ILbZUtVDEem5ebMPg-wyFg" value="ORGKREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_ILbZU9VDEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_IY-tINVDEem5ebMPg-wyFg" name="GKF2CD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_IY-tIdVDEem5ebMPg-wyFg" value="GKF2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IY-tItVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_IY-tI9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IY-tJNVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IY-tJdVDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IY-tJtVDEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZAiUNVDEem5ebMPg-wyFg" name="GKAKCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZAiUdVDEem5ebMPg-wyFg" value="GKAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZAiUtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZAiU9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZAiVNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZAiVdVDEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZBwcNVDEem5ebMPg-wyFg" name="GKAMCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZBwcdVDEem5ebMPg-wyFg" value="GKAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZBwctVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZBwc9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZBwdNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZBwddVDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZBwdtVDEem5ebMPg-wyFg" name="GKHNNA" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZBwd9VDEem5ebMPg-wyFg" value="GKHNNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZBweNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZBwedVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZBwetVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZBwe9VDEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZCXgNVDEem5ebMPg-wyFg" name="GKFHTX" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZCXgdVDEem5ebMPg-wyFg" value="GKFHTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZCXgtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZCXg9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZCXhNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZCXhdVDEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZCXhtVDEem5ebMPg-wyFg" name="GKLPST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZCXh9VDEem5ebMPg-wyFg" value="GKLPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZCXiNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZCXidVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZCXitVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZCXi9VDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZCXjNVDEem5ebMPg-wyFg" name="GKBWN3" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZC-kNVDEem5ebMPg-wyFg" value="GKBWN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZC-kdVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZC-ktVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZC-k9VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZC-lNVDEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZC-ldVDEem5ebMPg-wyFg" name="GKBXN3" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZC-ltVDEem5ebMPg-wyFg" value="GKBXN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZC-l9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZC-mNVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZC-mdVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZC-mtVDEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZC-m9VDEem5ebMPg-wyFg" name="GKHZTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZC-nNVDEem5ebMPg-wyFg" value="GKHZTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZC-ndVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZC-ntVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZC-n9VDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZC-oNVDEem5ebMPg-wyFg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZDloNVDEem5ebMPg-wyFg" name="GKH0TX" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZDlodVDEem5ebMPg-wyFg" value="GKH0TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZDlotVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZDlo9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZDlpNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZDlpdVDEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZDlptVDEem5ebMPg-wyFg" name="GKH1TX" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZDlp9VDEem5ebMPg-wyFg" value="GKH1TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZDlqNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZDlqdVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZDlqtVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZDlq9VDEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZEMsNVDEem5ebMPg-wyFg" name="GKH2TX" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZEMsdVDEem5ebMPg-wyFg" value="GKH2TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZEMstVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZEMs9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZEMtNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZEMtdVDEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZEMttVDEem5ebMPg-wyFg" name="GKJWNA" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZEMt9VDEem5ebMPg-wyFg" value="GKJWNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZEMuNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZEMudVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZEMutVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZEMu9VDEem5ebMPg-wyFg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZEzwNVDEem5ebMPg-wyFg" name="GKN1CD" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZEzwdVDEem5ebMPg-wyFg" value="GKN1CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZEzwtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZEzw9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZEzxNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZEzxdVDEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_IZEzxtVDEem5ebMPg-wyFg" name="GKN2CD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_IZEzx9VDEem5ebMPg-wyFg" value="GKN2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_IZEzyNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_IZEzydVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_IZEzytVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_IZEzy9VDEem5ebMPg-wyFg" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_IluIodVDEem5ebMPg-wyFg" name="ORAGREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_IluIotVDEem5ebMPg-wyFg" value="ORAGREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_IluIo9VDEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_ImPtENVDEem5ebMPg-wyFg" name="AGAGCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImPtEdVDEem5ebMPg-wyFg" value="AGAGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImPtEtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ImPtE9VDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImPtFNVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImPtFdVDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImPtFtVDEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImPtF9VDEem5ebMPg-wyFg" name="AGAKCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImPtGNVDEem5ebMPg-wyFg" value="AGAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImQUINVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImQUIdVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImQUItVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImQUI9VDEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImQUJNVDEem5ebMPg-wyFg" name="AGAMCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImQUJdVDEem5ebMPg-wyFg" value="AGAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImQUJtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImQUJ9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImQUKNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImQUKdVDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImQUKtVDEem5ebMPg-wyFg" name="AGF2CD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImQUK9VDEem5ebMPg-wyFg" value="AGF2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImQULNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ImQULdVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImQULtVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImQUL9VDEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImQUMNVDEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImQ7MNVDEem5ebMPg-wyFg" name="AGACTX" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImQ7MdVDEem5ebMPg-wyFg" value="AGACTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImQ7MtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImQ7M9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImQ7NNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImQ7NdVDEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImQ7NtVDEem5ebMPg-wyFg" name="AGABST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImQ7N9VDEem5ebMPg-wyFg" value="AGABST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImQ7ONVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImQ7OdVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImQ7OtVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImQ7O9VDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImRiQNVDEem5ebMPg-wyFg" name="AGEJTX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImRiQdVDEem5ebMPg-wyFg" value="AGEJTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImRiQtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImRiQ9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImRiRNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImRiRdVDEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImRiRtVDEem5ebMPg-wyFg" name="AGLLST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImRiR9VDEem5ebMPg-wyFg" value="AGLLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImRiSNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImRiSdVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImRiStVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImRiS9VDEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImRiTNVDEem5ebMPg-wyFg" name="AGBUN3" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImRiTdVDEem5ebMPg-wyFg" value="AGBUN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImRiTtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImRiT9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImRiUNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImRiUdVDEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImSJUNVDEem5ebMPg-wyFg" name="AGBVN3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImSJUdVDEem5ebMPg-wyFg" value="AGBVN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImSJUtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImSJU9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImSJVNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImSJVdVDEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImSJVtVDEem5ebMPg-wyFg" name="AGIUNA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImSJV9VDEem5ebMPg-wyFg" value="AGIUNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImSJWNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImSJWdVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImSJWtVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImSJW9VDEem5ebMPg-wyFg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImSwYNVDEem5ebMPg-wyFg" name="AGHVTX" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImSwYdVDEem5ebMPg-wyFg" value="AGHVTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImSwYtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImSwY9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImSwZNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImSwZdVDEem5ebMPg-wyFg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImSwZtVDEem5ebMPg-wyFg" name="AGHWTX" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImSwZ9VDEem5ebMPg-wyFg" value="AGHWTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImSwaNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImSwadVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImSwatVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImSwa9VDEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImSwbNVDEem5ebMPg-wyFg" name="AGHXTX" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImSwbdVDEem5ebMPg-wyFg" value="AGHXTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImSwbtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImSwb9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImSwcNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImSwcdVDEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImTXcNVDEem5ebMPg-wyFg" name="AGHYTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImTXcdVDEem5ebMPg-wyFg" value="AGHYTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImTXctVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImTXc9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImTXdNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImTXddVDEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImTXdtVDEem5ebMPg-wyFg" name="AGNZCD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImTXd9VDEem5ebMPg-wyFg" value="AGNZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImTXeNVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImTXedVDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImTXetVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImTXe9VDEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ImT-gNVDEem5ebMPg-wyFg" name="AGN0CD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_ImT-gdVDEem5ebMPg-wyFg" value="AGN0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ImT-gtVDEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ImT-g9VDEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ImT-hNVDEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ImT-hdVDEem5ebMPg-wyFg" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_QA7ngAqtEeqIs-I1fHZ1cw" name="AKENEO_ARTICLE">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_QA7ngQqtEeqIs-I1fHZ1cw" value="AKENEO_ARTICLE"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_QA7nggqtEeqIs-I1fHZ1cw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_QBMGMAqtEeqIs-I1fHZ1cw" name="CODE_ECF" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_QBMGMQqtEeqIs-I1fHZ1cw" value="CODE_ECF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QBMGMgqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_QBMGMwqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QBMGNAqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QBMGNQqtEeqIs-I1fHZ1cw" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QBMGNgqtEeqIs-I1fHZ1cw" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QBMtQAqtEeqIs-I1fHZ1cw" name="AKENEO_I" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_QBMtQQqtEeqIs-I1fHZ1cw" value="AKENEO_I"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QBMtQgqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_QBMtQwqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QBMtRAqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QBMtRQqtEeqIs-I1fHZ1cw" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QBMtRgqtEeqIs-I1fHZ1cw" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QBNUUAqtEeqIs-I1fHZ1cw" name="AKENEO_S" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_QBNUUQqtEeqIs-I1fHZ1cw" value="AKENEO_S"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QBNUUgqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QBNUUwqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QBNUVAqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QBNUVQqtEeqIs-I1fHZ1cw" value="50"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_QMMcUAqtEeqIs-I1fHZ1cw" name="AKENEO_FICHE_SECURITE_OU_PRODUIT">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_QMNDYAqtEeqIs-I1fHZ1cw" value="AKENEO_FICHE_SECURITE_OU_PRODUIT"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_QMNDYQqtEeqIs-I1fHZ1cw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_QMc7AAqtEeqIs-I1fHZ1cw" name="IDPREREF" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_QMc7AQqtEeqIs-I1fHZ1cw" value="IDPREREF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QMc7AgqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_QMc7AwqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QMc7BAqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QMc7BQqtEeqIs-I1fHZ1cw" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QMc7BgqtEeqIs-I1fHZ1cw" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QMc7BwqtEeqIs-I1fHZ1cw" name="TYPE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_QMdiEAqtEeqIs-I1fHZ1cw" value="TYPE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QMdiEQqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QMdiEgqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QMdiEwqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QMdiFAqtEeqIs-I1fHZ1cw" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QMdiFQqtEeqIs-I1fHZ1cw" name="LOCALE" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_QMdiFgqtEeqIs-I1fHZ1cw" value="LOCALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QMdiFwqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QMdiGAqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QMdiGQqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QMdiGgqtEeqIs-I1fHZ1cw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_QMdiGwqtEeqIs-I1fHZ1cw" name="FICHIER" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_QMdiHAqtEeqIs-I1fHZ1cw" value="FICHIER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_QMdiHQqtEeqIs-I1fHZ1cw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_QMdiHgqtEeqIs-I1fHZ1cw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_QMdiHwqtEeqIs-I1fHZ1cw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_QMdiIAqtEeqIs-I1fHZ1cw" value="255"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.queryFolder" id="_7sHJUQkoEemca7hN01bE4g" name="Requetes utiles">
    <node defType="com.stambia.rdbms.query" id="_9i_yeAkoEemca7hN01bE4g" name="Sysdate_formatdujour">
      <attribute defType="com.stambia.rdbms.query.expression" id="_Hei1kAkpEemca7hN01bE4g" value="select * &#xD;&#xA;from (&#xD;&#xA;&#x9;SELECT concat(concat(year(CURRENT DATE)-1900,right(concat('0',month(CURRENT DATE)),2)),right(concat('0',day(CURRENT DATE)),2)) AS DateDuJourSynon &#xD;&#xA;&#x9;FROM sysibm.sysdummy1&#xD;&#xA;&#x9;)  T"/>
      <node defType="com.stambia.rdbms.column" id="_J8hT4AkpEemca7hN01bE4g" name="DATEDUJOURSYNON" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_J8hT4QkpEemca7hN01bE4g" value="DATEDUJOURSYNON"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_J8hT4gkpEemca7hN01bE4g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_J8hT4wkpEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_J8hT5AkpEemca7hN01bE4g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_J8hT5QkpEemca7hN01bE4g" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_J8hT5gkpEemca7hN01bE4g" value="33"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_i5h_sBWNEemsgZOghPO5Pg" name="SysDateN-1">
      <attribute defType="com.stambia.rdbms.query.expression" id="_kQ3mEBWNEemsgZOghPO5Pg" value="select                                                       &#xD;&#xA;VARCHAR_FORMAT(current date - 1 month  -  day(current date) day  + 1 day, 'YYYYMMDD') - 19000000 AS DteDeb,&#xD;&#xA;VARCHAR_FORMAT(current date -  day(current date) day, 'YYYYMMDD') - 19000000   AS DteFin,&#xD;&#xA;trim(VARCHAR_FORMAT((current date - 1 year) - 1 month  -  day(current date) day  + 1 day, 'YYYYMMDD') )AS DteDebN_1&#xD;&#xA;from sysibm.sysdummy1        "/>
      <node defType="com.stambia.rdbms.column" id="_-WJs4BWNEemsgZOghPO5Pg" name="DTEDEB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_-WJs4RWNEemsgZOghPO5Pg" value="DTEDEB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-WJs4hWNEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-WJs4xWNEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_-WJs5BWNEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-WJs5RWNEemsgZOghPO5Pg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-WJs5hWNEemsgZOghPO5Pg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_-WJs5xWNEemsgZOghPO5Pg" name="DTEFIN" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_-WJs6BWNEemsgZOghPO5Pg" value="DTEFIN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_-WJs6RWNEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_-WJs6hWNEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_-WJs6xWNEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_-WJs7BWNEemsgZOghPO5Pg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_-WJs7RWNEemsgZOghPO5Pg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BmiScGg2EemwHJMcwY694w" name="DTEDEBN_1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_BmiScWg2EemwHJMcwY694w" value="DTEDEBN_1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BmiScmg2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BmiSc2g2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BmiSdGg2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BmiSdWg2EemwHJMcwY694w" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BmiSdmg2EemwHJMcwY694w" value="255"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_9h08SDqVEemaHK9T15k6XQ" name="Mercur_tous_sites_unique">
      <attribute defType="com.stambia.rdbms.query.expression" id="_AEXKcDqWEemaHK9T15k6XQ" value="SELECT DISTINCT  MERCUR FROM orionbd.Eliclip  where MERCUR &lt;> '' &#xD;&#xA;union&#xD;&#xA;SELECT DISTINCT  MERCUR  FROM orionbd.Webcli  where MERCUR &lt;> '' &#xD;&#xA;union&#xD;&#xA;SELECT DISTINCT  MERCUR  FROM orionbd.Wsoclip where MERCUR &lt;> ''&#xD;&#xA;"/>
      <node defType="com.stambia.rdbms.column" id="_BpYngDqWEemaHK9T15k6XQ" name="MERCUR" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_BpYngTqWEemaHK9T15k6XQ" value="MERCUR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BpYngjqWEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BpYngzqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BpYnhDqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BpYnhTqWEemaHK9T15k6XQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BpYnhjqWEemaHK9T15k6XQ" value="20"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_J_NC8DqWEemaHK9T15k6XQ" name="DateSynon">
      <attribute defType="com.stambia.rdbms.query.expression" id="_MTvRYDqWEemaHK9T15k6XQ" value="select                                                             &#xD;&#xA; VARCHAR_FORMAT(current date - 01 month  -  day(current date) day  +&#xD;&#xA;1 day, 'YYYYMMDD') - 19000000 AS DteDebM1,                         &#xD;&#xA; VARCHAR_FORMAT(current date   -  day(current date) day             &#xD;&#xA;      , 'YYYYMMDD') - 19000000 AS DteFinM1,                         &#xD;&#xA; VARCHAR_FORMAT(current date - 01 day, 'YYYYMMDD') - 19000000 AS    &#xD;&#xA; Hier  ,                                                            &#xD;&#xA; VARCHAR_FORMAT(current date , 'YYYYMMDD') - 19000000 AS Today      &#xD;&#xA; ,VARCHAR_FORMAT(current date + 01 day, 'YYYYMMDD') - 19000000 AS   &#xD;&#xA; Demain                                                             &#xD;&#xA; from sysibm.sysdummy1                                              &#xD;&#xA;"/>
      <node defType="com.stambia.rdbms.column" id="_YHAEEDqWEemaHK9T15k6XQ" name="DTEDEBM1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_YHAEETqWEemaHK9T15k6XQ" value="DTEDEBM1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YHAEEjqWEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YHAEEzqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YHAEFDqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YHAEFTqWEemaHK9T15k6XQ" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YHAEFjqWEemaHK9T15k6XQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YHAEFzqWEemaHK9T15k6XQ" name="DTEFINM1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_YHAEGDqWEemaHK9T15k6XQ" value="DTEFINM1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YHAEGTqWEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YHAEGjqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YHAEGzqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YHAEHDqWEemaHK9T15k6XQ" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YHAEHTqWEemaHK9T15k6XQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YHAEHjqWEemaHK9T15k6XQ" name="HIER" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_YHAEHzqWEemaHK9T15k6XQ" value="HIER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YHAEIDqWEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YHAEITqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YHAEIjqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YHAEIzqWEemaHK9T15k6XQ" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YHAEJDqWEemaHK9T15k6XQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YHAEJTqWEemaHK9T15k6XQ" name="TODAY" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_YHAEJjqWEemaHK9T15k6XQ" value="TODAY"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YHAEJzqWEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YHAEKDqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YHAEKTqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YHAEKjqWEemaHK9T15k6XQ" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YHAEKzqWEemaHK9T15k6XQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YHAELDqWEemaHK9T15k6XQ" name="DEMAIN" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_YHAELTqWEemaHK9T15k6XQ" value="DEMAIN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YHAELjqWEemaHK9T15k6XQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YHAELzqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YHAEMDqWEemaHK9T15k6XQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YHAEMTqWEemaHK9T15k6XQ" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YHAEMjqWEemaHK9T15k6XQ" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_p7so0EaLEem8hIAZ2622yA" name="Oslzrep_with_rownumber">
      <attribute defType="com.stambia.rdbms.query.expression" id="_uluH8EaLEem8hIAZ2622yA" value="select &#x9;LZJKC1, &#xD;&#xA;&#x9;LZJLC1,&#xD;&#xA;&#x9;LZXGN5,&#x9;&#xD;&#xA;&#x9;trim(LZWUT1) as LZWUT1,&#xD;&#xA;&#x9;trim(LZWVT1) as LZWVT1,&#xD;&#xA;&#x9; concat('username',ROW_NUMBER() OVER () ) as username, &#xD;&#xA;&#x9;ROW_NUMBER() OVER () rownumber,&#xD;&#xA;&#x9;case&#xD;&#xA;&#x9;when ( ltrim(rtrim(LZWUT1)) = '' or ltrim(rtrim(LZWVT1)) = '' ) then concat(concat('ecftest',ROW_NUMBER() OVER ()),'@ecf.fr')&#xD;&#xA;&#x9;else concat(concat(ltrim(rtrim(LZWUT1)),'@') , ltrim(rtrim(LZWVT1)))&#xD;&#xA;&#x9;end as mail&#xD;&#xA;from orionbd.oslzrep&#xD;&#xA;order by LZJKC1"/>
      <node defType="com.stambia.rdbms.column" id="_D-sQoEaMEem8hIAZ2622yA" name="LZJKC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_D-sQoUaMEem8hIAZ2622yA" value="LZJKC1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_D-sQokaMEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_D-sQo0aMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_D-sQpEaMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_D-sQpUaMEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_D-sQpkaMEem8hIAZ2622yA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_D-sQp0aMEem8hIAZ2622yA" name="LZJLC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_D-sQqEaMEem8hIAZ2622yA" value="LZJLC1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_D-sQqUaMEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_D-sQqkaMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_D-sQq0aMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_D-sQrEaMEem8hIAZ2622yA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_D-sQrUaMEem8hIAZ2622yA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_D-sQrkaMEem8hIAZ2622yA" name="LZXGN5" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_D-sQr0aMEem8hIAZ2622yA" value="LZXGN5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_D-sQsEaMEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_D-sQsUaMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_D-sQskaMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_D-sQs0aMEem8hIAZ2622yA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_D-sQtEaMEem8hIAZ2622yA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_D-sQtUaMEem8hIAZ2622yA" name="USERNAME" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_D-sQtkaMEem8hIAZ2622yA" value="USERNAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_D-sQt0aMEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_D-sQuEaMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_D-sQuUaMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_D-sQukaMEem8hIAZ2622yA" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_D-sQu0aMEem8hIAZ2622yA" value="28"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_D-sQvEaMEem8hIAZ2622yA" name="ROWNUMBER" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_D-sQvUaMEem8hIAZ2622yA" value="ROWNUMBER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_D-sQvkaMEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_D-sQv0aMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_D-sQwEaMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_D-sQwUaMEem8hIAZ2622yA" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_D-sQwkaMEem8hIAZ2622yA" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_D-sQw0aMEem8hIAZ2622yA" name="MAIL" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_D-sQxEaMEem8hIAZ2622yA" value="MAIL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_D-sQxUaMEem8hIAZ2622yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_D-sQxkaMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_D-sQx0aMEem8hIAZ2622yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_D-sQyEaMEem8hIAZ2622yA" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_D-sQyUaMEem8hIAZ2622yA" value="141"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4fF6pVYSEemUmsztjFvuIg" name="LZWUT1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_4fF6plYSEemUmsztjFvuIg" value="LZWUT1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4fF6p1YSEemUmsztjFvuIg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4fF6qFYSEemUmsztjFvuIg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_4fF6qVYSEemUmsztjFvuIg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4fF6qlYSEemUmsztjFvuIg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4fF6q1YSEemUmsztjFvuIg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_4fF6rFYSEemUmsztjFvuIg" name="LZWVT1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_4fF6rVYSEemUmsztjFvuIg" value="LZWVT1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_4fF6rlYSEemUmsztjFvuIg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_4fF6r1YSEemUmsztjFvuIg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_4fF6sFYSEemUmsztjFvuIg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_4fF6sVYSEemUmsztjFvuIg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_4fF6slYSEemUmsztjFvuIg" value="70"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_JPhbQEslEemn-PTqagJaAg" name="Orazrep_with_rownum">
      <attribute defType="com.stambia.rdbms.query.expression" id="_P1sokEsmEemn-PTqagJaAg" value="select ROW_NUMBER() OVER () rownumber,azazcd,azamcd,aztkst,azasst,azawna,azautx,azzltx,azfmtx,azcgcd,azj4cd,azbccd,azbocd,azfwnb,azbacd,azajcd,azcqtx,azi6st,azbon3,azbpn3,azhotx,azhqtx,azbqn3,azakcd,azgidt,aza6na,aza2na,aza3na,aza4st,azftnb, rtrim(azajnb) as azajnb from orionbd.orazrep"/>
      <node defType="com.stambia.rdbms.column" id="_SQxj4EsmEemn-PTqagJaAg" name="ROWNUMBER" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxj4UsmEemn-PTqagJaAg" value="ROWNUMBER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxj4ksmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxj40smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxj5EsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxj5UsmEemn-PTqagJaAg" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxj5ksmEemn-PTqagJaAg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQxj50smEemn-PTqagJaAg" name="AZAZCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxj6EsmEemn-PTqagJaAg" value="AZAZCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxj6UsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxj6ksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxj60smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxj7EsmEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxj7UsmEemn-PTqagJaAg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQxj7ksmEemn-PTqagJaAg" name="AZAMCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxj70smEemn-PTqagJaAg" value="AZAMCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxj8EsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxj8UsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxj8ksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxj80smEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxj9EsmEemn-PTqagJaAg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQxj9UsmEemn-PTqagJaAg" name="AZTKST" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxj9ksmEemn-PTqagJaAg" value="AZTKST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxj90smEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxj-EsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxj-UsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxj-ksmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxj-0smEemn-PTqagJaAg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQxj_EsmEemn-PTqagJaAg" name="AZASST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxj_UsmEemn-PTqagJaAg" value="AZASST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxj_ksmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxj_0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxkAEsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxkAUsmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxkAksmEemn-PTqagJaAg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQxkA0smEemn-PTqagJaAg" name="AZAWNA" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxkBEsmEemn-PTqagJaAg" value="AZAWNA"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxkBUsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxkBksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxkB0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxkCEsmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxkCUsmEemn-PTqagJaAg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQxkCksmEemn-PTqagJaAg" name="AZAUTX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxkC0smEemn-PTqagJaAg" value="AZAUTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxkDEsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxkDUsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxkDksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxkD0smEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxkEEsmEemn-PTqagJaAg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQxkEUsmEemn-PTqagJaAg" name="AZZLTX" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxkEksmEemn-PTqagJaAg" value="AZZLTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxkE0smEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxkFEsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxkFUsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxkFksmEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxkF0smEemn-PTqagJaAg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQxkGEsmEemn-PTqagJaAg" name="AZFMTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxkGUsmEemn-PTqagJaAg" value="AZFMTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxkGksmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxkG0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxkHEsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxkHUsmEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxkHksmEemn-PTqagJaAg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQxkH0smEemn-PTqagJaAg" name="AZCGCD" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQxkIEsmEemn-PTqagJaAg" value="AZCGCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQxkIUsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQxkIksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQxkI0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQxkJEsmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQxkJUsmEemn-PTqagJaAg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7U4EsmEemn-PTqagJaAg" name="AZJ4CD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7U4UsmEemn-PTqagJaAg" value="AZJ4CD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7U4ksmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7U40smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7U5EsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7U5UsmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7U5ksmEemn-PTqagJaAg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7U50smEemn-PTqagJaAg" name="AZBCCD" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7U6EsmEemn-PTqagJaAg" value="AZBCCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7U6UsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7U6ksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7U60smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7U7EsmEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7U7UsmEemn-PTqagJaAg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7U7ksmEemn-PTqagJaAg" name="AZBOCD" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7U70smEemn-PTqagJaAg" value="AZBOCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7U8EsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7U8UsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7U8ksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7U80smEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7U9EsmEemn-PTqagJaAg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7U9UsmEemn-PTqagJaAg" name="AZFWNB" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7U9ksmEemn-PTqagJaAg" value="AZFWNB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7U90smEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7U-EsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7U-UsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7U-ksmEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7U-0smEemn-PTqagJaAg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7U_EsmEemn-PTqagJaAg" name="AZBACD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7U_UsmEemn-PTqagJaAg" value="AZBACD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7U_ksmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7U_0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VAEsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VAUsmEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VAksmEemn-PTqagJaAg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VA0smEemn-PTqagJaAg" name="AZAJCD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VBEsmEemn-PTqagJaAg" value="AZAJCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VBUsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VBksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VB0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VCEsmEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VCUsmEemn-PTqagJaAg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VCksmEemn-PTqagJaAg" name="AZCQTX" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VC0smEemn-PTqagJaAg" value="AZCQTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VDEsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VDUsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VDksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VD0smEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VEEsmEemn-PTqagJaAg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VEUsmEemn-PTqagJaAg" name="AZI6ST" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VEksmEemn-PTqagJaAg" value="AZI6ST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VE0smEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VFEsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VFUsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VFksmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VF0smEemn-PTqagJaAg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VGEsmEemn-PTqagJaAg" name="AZBON3" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VGUsmEemn-PTqagJaAg" value="AZBON3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VGksmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VG0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VHEsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VHUsmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VHksmEemn-PTqagJaAg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VH0smEemn-PTqagJaAg" name="AZBPN3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VIEsmEemn-PTqagJaAg" value="AZBPN3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VIUsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VIksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VI0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VJEsmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VJUsmEemn-PTqagJaAg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VJksmEemn-PTqagJaAg" name="AZHOTX" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VJ0smEemn-PTqagJaAg" value="AZHOTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VKEsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VKUsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VKksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VK0smEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VLEsmEemn-PTqagJaAg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VLUsmEemn-PTqagJaAg" name="AZHQTX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VLksmEemn-PTqagJaAg" value="AZHQTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VL0smEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VMEsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VMUsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VMksmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VM0smEemn-PTqagJaAg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VNEsmEemn-PTqagJaAg" name="AZBQN3" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VNUsmEemn-PTqagJaAg" value="AZBQN3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VNksmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VN0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VOEsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VOUsmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VOksmEemn-PTqagJaAg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VO0smEemn-PTqagJaAg" name="AZAKCD" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VPEsmEemn-PTqagJaAg" value="AZAKCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VPUsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VPksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VP0smEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VQEsmEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VQUsmEemn-PTqagJaAg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_SQ7VQksmEemn-PTqagJaAg" name="AZGIDT" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_SQ7VQ0smEemn-PTqagJaAg" value="AZGIDT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_SQ7VREsmEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_SQ7VRUsmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_SQ7VRksmEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_SQ7VR0smEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_SQ7VSEsmEemn-PTqagJaAg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LPtYZ11PEem_Ra0eAY2hSw" name="AZA6NA" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_LPtYaF1PEem_Ra0eAY2hSw" value="AZA6NA"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LPtYaV1PEem_Ra0eAY2hSw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LPtYal1PEem_Ra0eAY2hSw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LPtYa11PEem_Ra0eAY2hSw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LPtYbF1PEem_Ra0eAY2hSw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LPtYbV1PEem_Ra0eAY2hSw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LPtYbl1PEem_Ra0eAY2hSw" name="AZA2NA" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_LPtYb11PEem_Ra0eAY2hSw" value="AZA2NA"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LPtYcF1PEem_Ra0eAY2hSw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LPtYcV1PEem_Ra0eAY2hSw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LPtYcl1PEem_Ra0eAY2hSw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LPtYc11PEem_Ra0eAY2hSw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LPtYdF1PEem_Ra0eAY2hSw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LPt_cF1PEem_Ra0eAY2hSw" name="AZA3NA" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_LPt_cV1PEem_Ra0eAY2hSw" value="AZA3NA"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LPt_cl1PEem_Ra0eAY2hSw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LPt_c11PEem_Ra0eAY2hSw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LPt_dF1PEem_Ra0eAY2hSw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LPt_dV1PEem_Ra0eAY2hSw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LPt_dl1PEem_Ra0eAY2hSw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_yq4tp1-REema_r8Hg2-1dA" name="AZA4ST" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_yq4tqF-REema_r8Hg2-1dA" value="AZA4ST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_yq4tqV-REema_r8Hg2-1dA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_yq4tql-REema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_yq4tq1-REema_r8Hg2-1dA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_yq4trF-REema_r8Hg2-1dA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_yq4trV-REema_r8Hg2-1dA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X1w0N2a2EemwHJMcwY694w" name="AZFTNB" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_X1w0OGa2EemwHJMcwY694w" value="AZFTNB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X1w0OWa2EemwHJMcwY694w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X1w0Oma2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_X1w0O2a2EemwHJMcwY694w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X1w0PGa2EemwHJMcwY694w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X1w0PWa2EemwHJMcwY694w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_X-D7dXJ5EemIMIaOcxSI7A" name="AZAJNB" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_X-D7dnJ5EemIMIaOcxSI7A" value="AZAJNB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_X-D7d3J5EemIMIaOcxSI7A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_X-D7eHJ5EemIMIaOcxSI7A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_X-D7eXJ5EemIMIaOcxSI7A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_X-D7enJ5EemIMIaOcxSI7A" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_X-D7e3J5EemIMIaOcxSI7A" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_wZBLAEtAEemn-PTqagJaAg" name="WEBCLI_with_rownum">
      <attribute defType="com.stambia.rdbms.query.expression" id="_xdm0gEtAEemn-PTqagJaAg" value="select ROW_NUMBER() OVER () rownumber,idwebc,cptcli,nomcli,precli,nivcli,actcli, climail,pwdmd5,maxcde,lngcli from orionbd.webcli"/>
      <node defType="com.stambia.rdbms.column" id="_0jFXsEtAEemn-PTqagJaAg" name="ROWNUMBER" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jFXsUtAEemn-PTqagJaAg" value="ROWNUMBER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jFXsktAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jFXs0tAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jFXtEtAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jFXtUtAEemn-PTqagJaAg" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jFXtktAEemn-PTqagJaAg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0jFXt0tAEemn-PTqagJaAg" name="IDWEBC" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jFXuEtAEemn-PTqagJaAg" value="IDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jFXuUtAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jFXuktAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jFXu0tAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jFXvEtAEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jFXvUtAEemn-PTqagJaAg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0jFXvktAEemn-PTqagJaAg" name="CPTCLI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jFXv0tAEemn-PTqagJaAg" value="CPTCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jFXwEtAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jFXwUtAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jFXwktAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jFXw0tAEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jFXxEtAEemn-PTqagJaAg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0jFXxUtAEemn-PTqagJaAg" name="NOMCLI" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jFXxktAEemn-PTqagJaAg" value="NOMCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jFXx0tAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jFXyEtAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jFXyUtAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jFXyktAEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jFXy0tAEemn-PTqagJaAg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0jFXzEtAEemn-PTqagJaAg" name="PRECLI" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jFXzUtAEemn-PTqagJaAg" value="PRECLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jFXzktAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jFXz0tAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jFX0EtAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jFX0UtAEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jFX0ktAEemn-PTqagJaAg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0jFX00tAEemn-PTqagJaAg" name="NIVCLI" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jFX1EtAEemn-PTqagJaAg" value="NIVCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jFX1UtAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jFX1ktAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jFX10tAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jFX2EtAEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jFX2UtAEemn-PTqagJaAg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0jFX2ktAEemn-PTqagJaAg" name="ACTCLI" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jFX20tAEemn-PTqagJaAg" value="ACTCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jFX3EtAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jFX3UtAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jFX3ktAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jFX30tAEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jFX4EtAEemn-PTqagJaAg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0jFX4UtAEemn-PTqagJaAg" name="CLIMAIL" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jFX4ktAEemn-PTqagJaAg" value="CLIMAIL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jFX40tAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jFX5EtAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jFX5UtAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jFX5ktAEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jFX50tAEemn-PTqagJaAg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0jOhoEtAEemn-PTqagJaAg" name="PWDMD5" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jOhoUtAEemn-PTqagJaAg" value="PWDMD5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jOhoktAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jOho0tAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jOhpEtAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jOhpUtAEemn-PTqagJaAg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jOhpktAEemn-PTqagJaAg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0jOhp0tAEemn-PTqagJaAg" name="MAXCDE" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_0jOhqEtAEemn-PTqagJaAg" value="MAXCDE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0jOhqUtAEemn-PTqagJaAg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0jOhqktAEemn-PTqagJaAg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0jOhq0tAEemn-PTqagJaAg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0jOhrEtAEemn-PTqagJaAg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0jOhrUtAEemn-PTqagJaAg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_nLCZcHMJEemIMIaOcxSI7A" name="LNGCLI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_nLCZcXMJEemIMIaOcxSI7A" value="LNGCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_nLCZcnMJEemIMIaOcxSI7A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_nLCZc3MJEemIMIaOcxSI7A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_nLCZdHMJEemIMIaOcxSI7A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_nLCZdXMJEemIMIaOcxSI7A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_nLCZdnMJEemIMIaOcxSI7A" value="2"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_Ub_nsP7zEemVAft-X6rPVg" name="STAMBIA">
    <attribute defType="com.stambia.rdbms.schema.name" id="_UcZQUP7zEemVAft-X6rPVg" value="STAMBIA"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_UcZ3YP7zEemVAft-X6rPVg" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_UcZ3Yf7zEemVAft-X6rPVg" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_UcZ3Yv7zEemVAft-X6rPVg" value="I_[targetName]"/>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_nygysD4REeuqcoacVmMPtQ" name="TRANSFERT">
    <attribute defType="com.stambia.rdbms.schema.name" id="_nzmX0D4REeuqcoacVmMPtQ" value="TRANSFERT"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_nzm-4D4REeuqcoacVmMPtQ" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_nzm-4T4REeuqcoacVmMPtQ" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_nzm-4j4REeuqcoacVmMPtQ" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_sAw70D4REeuqcoacVmMPtQ" name="AKENEO_ARTICLE">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_sAw70T4REeuqcoacVmMPtQ" value="AKENEO_ARTICLE"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_sAw70j4REeuqcoacVmMPtQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_sOAGkD4REeuqcoacVmMPtQ" name="CODE_ECF" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_sOAGkT4REeuqcoacVmMPtQ" value="CODE_ECF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sOAGkj4REeuqcoacVmMPtQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_sOAGkz4REeuqcoacVmMPtQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sOAGlD4REeuqcoacVmMPtQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sOAtoD4REeuqcoacVmMPtQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_sOAtoT4REeuqcoacVmMPtQ" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sOCi0D4REeuqcoacVmMPtQ" name="AKENEO_I" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_sOCi0T4REeuqcoacVmMPtQ" value="AKENEO_I"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sOCi0j4REeuqcoacVmMPtQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_sOCi0z4REeuqcoacVmMPtQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sOCi1D4REeuqcoacVmMPtQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sOCi1T4REeuqcoacVmMPtQ" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_sOCi1j4REeuqcoacVmMPtQ" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sOEYAD4REeuqcoacVmMPtQ" name="AKENEO_S" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_sOEYAT4REeuqcoacVmMPtQ" value="AKENEO_S"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sOEYAj4REeuqcoacVmMPtQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sOEYAz4REeuqcoacVmMPtQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sOEYBD4REeuqcoacVmMPtQ" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_sOEYBT4REeuqcoacVmMPtQ" value="50"/>
      </node>
    </node>
  </node>
</md:node>