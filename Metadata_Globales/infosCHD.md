<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_3L5LMPF_Eem_mo4tieUuqA" md:ref="platform:/plugin/com.indy.environment/technology/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootArray" id="_3Wl_UPF_Eem_mo4tieUuqA" name="chdinfos">
    <attribute defType="com.stambia.json.rootArray.reverseURLPath" id="_3W7WgPF_Eem_mo4tieUuqA"/>
    <attribute defType="com.stambia.json.rootArray.reverseFilePath" id="_3W9LsPF_Eem_mo4tieUuqA" value="C:\app_dev\Used_Files\In_Files\Json\chdinfos.json"/>
    <attribute defType="com.stambia.json.rootArray.filePath" id="_Zfc5MPG7Eem_mo4tieUuqA" value="\\servernt46\Stambia\CHD\Temp_files\${~/NomFichierExport}$"/>
    <node defType="com.stambia.json.object" id="_5didsfF_Eem_mo4tieUuqA" name="item" position="1">
      <node defType="com.stambia.json.value" id="_5didsvF_Eem_mo4tieUuqA" name="chd_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_5dids_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didtPF_Eem_mo4tieUuqA" name="dba_name" position="2">
        <attribute defType="com.stambia.json.value.type" id="_5didtfF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didtvF_Eem_mo4tieUuqA" name="legal_name" position="3">
        <attribute defType="com.stambia.json.value.type" id="_5didt_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5diduPF_Eem_mo4tieUuqA" name="address" position="4">
        <attribute defType="com.stambia.json.value.type" id="_5didufF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5diduvF_Eem_mo4tieUuqA" name="address2" position="5">
        <attribute defType="com.stambia.json.value.type" id="_5didu_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didvPF_Eem_mo4tieUuqA" name="postal_code" position="6">
        <attribute defType="com.stambia.json.value.type" id="_5didvfF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didvvF_Eem_mo4tieUuqA" name="city" position="7">
        <attribute defType="com.stambia.json.value.type" id="_5didv_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didwPF_Eem_mo4tieUuqA" name="phone" position="8">
        <attribute defType="com.stambia.json.value.type" id="_5didwfF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didwvF_Eem_mo4tieUuqA" name="fax" position="9">
        <attribute defType="com.stambia.json.value.type" id="_5didw_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didxPF_Eem_mo4tieUuqA" name="legal_number1" position="10">
        <attribute defType="com.stambia.json.value.type" id="_5didxfF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didxvF_Eem_mo4tieUuqA" name="website" position="11">
        <attribute defType="com.stambia.json.value.type" id="_5didx_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didyPF_Eem_mo4tieUuqA" name="chain_name" position="12">
        <attribute defType="com.stambia.json.value.type" id="_5didyfF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didyvF_Eem_mo4tieUuqA" name="organisation_type" position="13">
        <attribute defType="com.stambia.json.value.type" id="_5didy_F_Eem_mo4tieUuqA" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didzPF_Eem_mo4tieUuqA" name="operation_type" position="14">
        <attribute defType="com.stambia.json.value.type" id="_5didzfF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5didzvF_Eem_mo4tieUuqA" name="market_segment" position="15">
        <attribute defType="com.stambia.json.value.type" id="_5didz_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did0PF_Eem_mo4tieUuqA" name="menu_type" position="16">
        <attribute defType="com.stambia.json.value.type" id="_5did0fF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did0vF_Eem_mo4tieUuqA" name="number_of_rooms" position="17">
        <attribute defType="com.stambia.json.value.type" id="_5did0_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did1PF_Eem_mo4tieUuqA" name="number_of_rooms_range" position="18">
        <attribute defType="com.stambia.json.value.type" id="_5did1fF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did1vF_Eem_mo4tieUuqA" name="annual_sales_range" position="19">
        <attribute defType="com.stambia.json.value.type" id="_5did1_F_Eem_mo4tieUuqA" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did2PF_Eem_mo4tieUuqA" name="confidence_level" position="20">
        <attribute defType="com.stambia.json.value.type" id="_5did2fF_Eem_mo4tieUuqA" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did2vF_Eem_mo4tieUuqA" name="average_check_range" position="21">
        <attribute defType="com.stambia.json.value.type" id="_5did2_F_Eem_mo4tieUuqA" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did3PF_Eem_mo4tieUuqA" name="years_in_business_range" position="22">
        <attribute defType="com.stambia.json.value.type" id="_5did3fF_Eem_mo4tieUuqA" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did3vF_Eem_mo4tieUuqA" name="number_of_employees_range" position="23">
        <attribute defType="com.stambia.json.value.type" id="_5did3_F_Eem_mo4tieUuqA" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did4PF_Eem_mo4tieUuqA" name="number_of_pieces_of_bred_range" position="24">
        <attribute defType="com.stambia.json.value.type" id="_5did4fF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did4vF_Eem_mo4tieUuqA" name="ownership" position="25">
        <attribute defType="com.stambia.json.value.type" id="_5did4_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did5PF_Eem_mo4tieUuqA" name="number_of_meals_range" position="26">
        <attribute defType="com.stambia.json.value.type" id="_5did5fF_Eem_mo4tieUuqA" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did5vF_Eem_mo4tieUuqA" name="number_of_beds_range" position="27">
        <attribute defType="com.stambia.json.value.type" id="_5did5_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did6PF_Eem_mo4tieUuqA" name="number_of_students_range" position="28">
        <attribute defType="com.stambia.json.value.type" id="_5did6fF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did6vF_Eem_mo4tieUuqA" name="closed_date" position="29">
        <attribute defType="com.stambia.json.value.type" id="_5did6_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did7PF_Eem_mo4tieUuqA" name="open_date" position="30">
        <attribute defType="com.stambia.json.value.type" id="_5did7fF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did7vF_Eem_mo4tieUuqA" name="contact_name" position="31">
        <attribute defType="com.stambia.json.value.type" id="_5did7_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did8PF_Eem_mo4tieUuqA" name="latitude" position="32">
        <attribute defType="com.stambia.json.value.type" id="_5did8fF_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did8vF_Eem_mo4tieUuqA" name="longitude" position="33">
        <attribute defType="com.stambia.json.value.type" id="_5did8_F_Eem_mo4tieUuqA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_5did9PF_Eem_mo4tieUuqA" name="terrace" position="34">
        <attribute defType="com.stambia.json.value.type" id="_5did9fF_Eem_mo4tieUuqA" value="boolean"/>
      </node>
      <node defType="com.stambia.json.object" id="_GBaqcPtDEemqBpbY2gDAZQ" name="hours" position="35">
        <node defType="com.stambia.json.array" id="_GBaqcftDEemqBpbY2gDAZQ" name="fri" position="1">
          <node defType="com.stambia.json.object" id="_GBaqcvtDEemqBpbY2gDAZQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_GBaqc_tDEemqBpbY2gDAZQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_GBaqdPtDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqdftDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqdvtDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqd_tDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_GBaqePtDEemqBpbY2gDAZQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_GBaqeftDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqevtDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqe_tDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqfPtDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.xml.propertyField" id="_I8mwIwV-Eeqxq-mJ8CTEbw" name="OccurrenceF">
              <attribute defType="com.stambia.xml.propertyField.property" id="_KpvAUAV-Eeqxq-mJ8CTEbw" value="nodeLocalPosition"/>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_GBaqhPtDEemqBpbY2gDAZQ" name="mon" position="2">
          <node defType="com.stambia.json.object" id="_GBaqhftDEemqBpbY2gDAZQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_GBaqhvtDEemqBpbY2gDAZQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_GBaqh_tDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqiPtDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqiftDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqivtDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_GBaqi_tDEemqBpbY2gDAZQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_GBaqjPtDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqjftDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqjvtDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqj_tDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.xml.propertyField" id="_Gh9SowV-Eeqxq-mJ8CTEbw" name="OccurrenceM">
              <attribute defType="com.stambia.xml.propertyField.property" id="_IYEfsAV-Eeqxq-mJ8CTEbw" value="nodeLocalPosition"/>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_GBaql_tDEemqBpbY2gDAZQ" name="sat" position="3">
          <node defType="com.stambia.json.object" id="_GBaqmPtDEemqBpbY2gDAZQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_GBaqmftDEemqBpbY2gDAZQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_GBaqmvtDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqm_tDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqnPtDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqnftDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_GBaqnvtDEemqBpbY2gDAZQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_GBaqn_tDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqoPtDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqoftDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqovtDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.xml.propertyField" id="_EDu8swV-Eeqxq-mJ8CTEbw" name="OccurrenceSa">
              <attribute defType="com.stambia.xml.propertyField.property" id="_F8XSQAV-Eeqxq-mJ8CTEbw" value="nodeLocalPosition"/>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_GBaqqvtDEemqBpbY2gDAZQ" name="sun" position="4">
          <node defType="com.stambia.json.object" id="_GBaqq_tDEemqBpbY2gDAZQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_GBaqrPtDEemqBpbY2gDAZQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_GBaqrftDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqrvtDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqr_tDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqsPtDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_GBaqsftDEemqBpbY2gDAZQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_GBaqsvtDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqs_tDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqtPtDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqtftDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.xml.propertyField" id="_Bt5twwV-Eeqxq-mJ8CTEbw" name="OccurrenceSu">
              <attribute defType="com.stambia.xml.propertyField.property" id="_Cg20sAV-Eeqxq-mJ8CTEbw" value="nodeLocalPosition"/>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_GBaqvftDEemqBpbY2gDAZQ" name="thu" position="5">
          <node defType="com.stambia.json.object" id="_GBaqvvtDEemqBpbY2gDAZQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_GBaqv_tDEemqBpbY2gDAZQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_GBaqwPtDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqwftDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqwvtDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqw_tDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_GBaqxPtDEemqBpbY2gDAZQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_GBaqxftDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaqxvtDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaqx_tDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaqyPtDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.xml.propertyField" id="_-ekrAwV9Eeqxq-mJ8CTEbw" name="OccurrenceTh">
              <attribute defType="com.stambia.xml.propertyField.property" id="_A70JgAV-Eeqxq-mJ8CTEbw" value="nodeLocalPosition"/>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_GBaq0PtDEemqBpbY2gDAZQ" name="tue" position="6">
          <node defType="com.stambia.json.object" id="_GBaq0ftDEemqBpbY2gDAZQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_GBaq0vtDEemqBpbY2gDAZQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_GBaq0_tDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaq1PtDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaq1ftDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaq1vtDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_GBaq1_tDEemqBpbY2gDAZQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_GBaq2PtDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaq2ftDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaq2vtDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaq2_tDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.xml.propertyField" id="_5d18EwV9Eeqxq-mJ8CTEbw" name="OccurrenceTu">
              <attribute defType="com.stambia.xml.propertyField.property" id="_8Ebj0AV9Eeqxq-mJ8CTEbw" value="nodeLocalPosition"/>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_GBaq4_tDEemqBpbY2gDAZQ" name="wed" position="7">
          <node defType="com.stambia.json.object" id="_GBaq5PtDEemqBpbY2gDAZQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_GBaq5ftDEemqBpbY2gDAZQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_GBaq5vtDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaq5_tDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaq6PtDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaq6ftDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_GBaq6vtDEemqBpbY2gDAZQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_GBaq6_tDEemqBpbY2gDAZQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_GBaq7PtDEemqBpbY2gDAZQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_GBaq7ftDEemqBpbY2gDAZQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_GBaq7vtDEemqBpbY2gDAZQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.xml.propertyField" id="_YFnRQwVrEeqxq-mJ8CTEbw" name="OccurrenceW">
              <attribute defType="com.stambia.xml.propertyField.property" id="_ZekoQAVrEeqxq-mJ8CTEbw" value="nodeLocalPosition"/>
            </node>
          </node>
        </node>
      </node>
      <node defType="com.stambia.xml.propertyField" id="_QsvGozCWEeq3399qC-CuVg" name="NumEnr">
        <attribute defType="com.stambia.xml.propertyField.property" id="_R-QY4DCWEeq3399qC-CuVg" value="nodeLocalPosition"/>
      </node>
    </node>
    <node defType="com.stambia.xml.propertyField" id="_hj0ko_GBEem_mo4tieUuqA" name="file_path"/>
    <node defType="com.stambia.xml.propertyField" id="_vvO6c_GBEem_mo4tieUuqA" name="string_content"/>
    <node defType="com.stambia.xml.propertyField" id="_8PxLI_SoEem_mo4tieUuqA" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_9lb9kPSoEem_mo4tieUuqA" value="fileName"/>
    </node>
  </node>
</md:node>