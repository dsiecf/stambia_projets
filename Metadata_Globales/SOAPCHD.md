<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.wsdl.wsdl" id="_8nD1gODiEei8l6X2h9mGFQ" name="Mywebservices" md:ref="platform:/plugin/com.indy.environment/technology/web/wsdl.tech#UUID_TECH_WSDL1?fileId=UUID_TECH_WSDL1$type=tech$name=wsdl?">
  <attribute defType="com.stambia.wsdl.wsdl.xsdReverseVersion" id="_8nNm3ODiEei8l6X2h9mGFQ" value="1"/>
  <attribute defType="com.stambia.wsdl.wsdl.url" id="_8uQpsODiEei8l6X2h9mGFQ"/>
  <node defType="com.stambia.wsdl.service" id="__6c44ODiEei8l6X2h9mGFQ" name="MyRestCHDService">
    <node defType="com.stambia.wsdl.port" id="_J851QODjEei8l6X2h9mGFQ" name="POST">
      <attribute defType="com.stambia.wsdl.port.address" id="_7-fkMODjEei8l6X2h9mGFQ" value="https://api.chd-expert.com/api/auth/signin"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_9Ojd4ODjEei8l6X2h9mGFQ" value="HTTP"/>
      <attribute defType="com.stambia.wsdl.port.verb" id="_-PunAODjEei8l6X2h9mGFQ" value="POST"/>
      <attribute defType="com.stambia.wsdl.port.httpAuthorization" id="_TngOQuDqEeid_5OQDhd6Lw"/>
      <attribute defType="com.stambia.wsdl.port.httpProxyAuthorization" id="_TngOQ-DqEeid_5OQDhd6Lw"/>
      <node defType="com.stambia.wsdl.operation" id="_oiYJwODjEei8l6X2h9mGFQ" name="SignIn">
        <attribute defType="com.stambia.wsdl.operation.address" id="_TngORODqEeid_5OQDhd6Lw" value="/"/>
        <attribute defType="com.stambia.wsdl.operation.httpHeaderProperties" id="_TngOReDqEeid_5OQDhd6Lw" value="Content-Type:application/json"/>
        <node defType="com.stambia.wsdl.input" id="_TngOLeDqEeid_5OQDhd6Lw">
          <node defType="com.stambia.wsdl.part" id="_TngOLuDqEeid_5OQDhd6Lw" name="content">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_TngOL-DqEeid_5OQDhd6Lw" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_TngOMODqEeid_5OQDhd6Lw" value="application/json"/>
            <node defType="com.stambia.json.rootObject" id="_TngOMeDqEeid_5OQDhd6Lw" name="result">
              <node defType="com.stambia.json.value" id="_TngOMuDqEeid_5OQDhd6Lw" name="password" position="2">
                <attribute defType="com.stambia.json.value.type" id="_TngOM-DqEeid_5OQDhd6Lw" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_TngONODqEeid_5OQDhd6Lw" name="email" position="1">
                <attribute defType="com.stambia.json.value.type" id="_TngONeDqEeid_5OQDhd6Lw" value="string"/>
              </node>
            </node>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="_TngONuDqEeid_5OQDhd6Lw">
          <node defType="com.stambia.wsdl.part" id="_TngON-DqEeid_5OQDhd6Lw" name="result" position="1">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_TngOOODqEeid_5OQDhd6Lw" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_TngOOeDqEeid_5OQDhd6Lw" value="application/json"/>
            <node defType="com.stambia.json.rootObject" id="_TngOOuDqEeid_5OQDhd6Lw" name="root">
              <node defType="com.stambia.json.value" id="_TngOO-DqEeid_5OQDhd6Lw" name="email" position="1">
                <attribute defType="com.stambia.json.value.type" id="_TngOPODqEeid_5OQDhd6Lw" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_TngOPeDqEeid_5OQDhd6Lw" name="token" position="2">
                <attribute defType="com.stambia.json.value.type" id="_TngOPuDqEeid_5OQDhd6Lw" value="string"/>
              </node>
            </node>
          </node>
          <node defType="com.stambia.wsdl.part" id="_TngOP-DqEeid_5OQDhd6Lw" name="Date" position="8">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_TngOQODqEeid_5OQDhd6Lw" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_TngOQeDqEeid_5OQDhd6Lw" value="string"/>
          </node>
        </node>
      </node>
    </node>
    <node defType="com.stambia.wsdl.port" id="_xbqf0OD9Eeid_5OQDhd6Lw" name="Get">
      <attribute defType="com.stambia.wsdl.port.address" id="_vyoQAOD-Eeid_5OQDhd6Lw" value="https://api.chd-expert.com/api/search/"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_wDDRoOD-Eeid_5OQDhd6Lw" value="HTTP"/>
      <attribute defType="com.stambia.wsdl.port.verb" id="_wwJBIOD-Eeid_5OQDhd6Lw" value="GET"/>
      <attribute defType="com.stambia.wsdl.port.httpAuthorization" id="_ksM6MeG4EeiPZPA44Xkqhw"/>
      <attribute defType="com.stambia.wsdl.port.httpProxyAuthorization" id="_ksM6MuG4EeiPZPA44Xkqhw"/>
      <node defType="com.stambia.wsdl.operation" id="_r6ikIOD-Eeid_5OQDhd6Lw" name="getInfosBySiret">
        <attribute defType="com.stambia.wsdl.operation.address" id="_ksM6M-G4EeiPZPA44Xkqhw" value="legal_number?legal_numbers={siret}"/>
        <attribute defType="com.stambia.wsdl.operation.httpHeaderProperties" id="_ksM6NOG4EeiPZPA44Xkqhw" value="Content_Type:application-json"/>
        <node defType="com.stambia.wsdl.input" id="_ksM5y-G4EeiPZPA44Xkqhw">
          <node defType="com.stambia.wsdl.part" id="_iRAWzOHFEeiPZPA44Xkqhw" name="siret">
            <attribute defType="com.stambia.wsdl.part.type" id="_iRAWzeHFEeiPZPA44Xkqhw" value="string"/>
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_iRAWzuHFEeiPZPA44Xkqhw" value="http:urlReplacement"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_vntVYuHJEeiPZPA44Xkqhw" name="Authorization">
            <attribute defType="com.stambia.wsdl.part.type" id="_vntVY-HJEeiPZPA44Xkqhw" value="string"/>
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_vntVZOHJEeiPZPA44Xkqhw" value="http:header"/>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="_vntVZeHJEeiPZPA44Xkqhw">
          <node defType="com.stambia.wsdl.part" id="_vntVZuHJEeiPZPA44Xkqhw" name="result" position="1">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_vntVZ-HJEeiPZPA44Xkqhw" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_vntVaOHJEeiPZPA44Xkqhw" value="application/json"/>
            <node defType="com.stambia.json.rootArray" id="_vntVaeHJEeiPZPA44Xkqhw" name="root">
              <node defType="com.stambia.json.object" id="_vntVauHJEeiPZPA44Xkqhw" name="item" position="1">
                <node defType="com.stambia.json.value" id="_404M2ArMEempwYQVaQNkuA" name="chd_id" position="1">
                  <attribute defType="com.stambia.json.value.type" id="_404M2QrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M2grMEempwYQVaQNkuA" name="dba_name" position="2">
                  <attribute defType="com.stambia.json.value.type" id="_404M2wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M3ArMEempwYQVaQNkuA" name="legal_name" position="3">
                  <attribute defType="com.stambia.json.value.type" id="_404M3QrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M3grMEempwYQVaQNkuA" name="address" position="4">
                  <attribute defType="com.stambia.json.value.type" id="_404M3wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M4ArMEempwYQVaQNkuA" name="address2" position="5">
                  <attribute defType="com.stambia.json.value.type" id="_404M4QrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M4grMEempwYQVaQNkuA" name="postal_code" position="6">
                  <attribute defType="com.stambia.json.value.type" id="_404M4wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M5ArMEempwYQVaQNkuA" name="city" position="7">
                  <attribute defType="com.stambia.json.value.type" id="_404M5QrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M5grMEempwYQVaQNkuA" name="phone" position="8">
                  <attribute defType="com.stambia.json.value.type" id="_404M5wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M6ArMEempwYQVaQNkuA" name="legal_number1" position="10">
                  <attribute defType="com.stambia.json.value.type" id="_404M6QrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M6grMEempwYQVaQNkuA" name="website" position="11">
                  <attribute defType="com.stambia.json.value.type" id="_404M6wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M7ArMEempwYQVaQNkuA" name="units" position="13">
                  <attribute defType="com.stambia.json.value.type" id="_404M7QrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M7grMEempwYQVaQNkuA" name="market_segment" position="14">
                  <attribute defType="com.stambia.json.value.type" id="_404M7wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M8ArMEempwYQVaQNkuA" name="menu_type" position="15">
                  <attribute defType="com.stambia.json.value.type" id="_404M8QrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M8grMEempwYQVaQNkuA" name="number_of_rooms_range" position="16">
                  <attribute defType="com.stambia.json.value.type" id="_404M8wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M9ArMEempwYQVaQNkuA" name="annual_sales" position="17">
                  <attribute defType="com.stambia.json.value.type" id="_404M9QrMEempwYQVaQNkuA" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M9grMEempwYQVaQNkuA" name="number_of_employees_range" position="18">
                  <attribute defType="com.stambia.json.value.type" id="_404M9wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M-ArMEempwYQVaQNkuA" name="number_of_meals" position="19">
                  <attribute defType="com.stambia.json.value.type" id="_404M-QrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M-grMEempwYQVaQNkuA" name="average_check" position="20">
                  <attribute defType="com.stambia.json.value.type" id="_404M-wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M_ArMEempwYQVaQNkuA" name="number_of_pieces_of_bred" position="21">
                  <attribute defType="com.stambia.json.value.type" id="_404M_QrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404M_grMEempwYQVaQNkuA" name="ownership" position="22">
                  <attribute defType="com.stambia.json.value.type" id="_404M_wrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404NAArMEempwYQVaQNkuA" name="operation_type" position="23">
                  <attribute defType="com.stambia.json.value.type" id="_404NAQrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404NAgrMEempwYQVaQNkuA" name="number_of_meals_range" position="24">
                  <attribute defType="com.stambia.json.value.type" id="_404NAwrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404NBArMEempwYQVaQNkuA" name="number_of_beds_range" position="25">
                  <attribute defType="com.stambia.json.value.type" id="_404NBQrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404NBgrMEempwYQVaQNkuA" name="number_of_students_range" position="26">
                  <attribute defType="com.stambia.json.value.type" id="_404NBwrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404NCArMEempwYQVaQNkuA" name="confidence_level" position="27">
                  <attribute defType="com.stambia.json.value.type" id="_404NCQrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404NCgrMEempwYQVaQNkuA" name="contact_name" position="29">
                  <attribute defType="com.stambia.json.value.type" id="_404NCwrMEempwYQVaQNkuA" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_404NDArMEempwYQVaQNkuA" name="latitude" position="30">
                  <attribute defType="com.stambia.json.value.type" id="_404NDQrMEempwYQVaQNkuA" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_404NDgrMEempwYQVaQNkuA" name="longitude" position="31">
                  <attribute defType="com.stambia.json.value.type" id="_404NDwrMEempwYQVaQNkuA" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_404NEArMEempwYQVaQNkuA" name="terrace" position="32">
                  <attribute defType="com.stambia.json.value.type" id="_404NEQrMEempwYQVaQNkuA" value="boolean"/>
                </node>
                <node defType="com.stambia.json.object" id="_404NEgrMEempwYQVaQNkuA" name="hours" position="33"/>
              </node>
            </node>
          </node>
          <node defType="com.stambia.wsdl.part" id="_vntVxOHJEeiPZPA44Xkqhw" name="Date" position="8">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_vntVxeHJEeiPZPA44Xkqhw" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_vntVxuHJEeiPZPA44Xkqhw" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_IdRBxgrOEempwYQVaQNkuA" name="Content-Length" position="18">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_IdRBxwrOEempwYQVaQNkuA" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_IdRByArOEempwYQVaQNkuA" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_IdRByQrOEempwYQVaQNkuA" name="Content-Type" position="20">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_IdRBygrOEempwYQVaQNkuA" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_IdRBywrOEempwYQVaQNkuA" value="string"/>
          </node>
        </node>
      </node>
    </node>
    <node defType="com.stambia.wsdl.port" id="_LVaVkBjkEemyO4lAnue3pA" name="GetResponseText">
      <attribute defType="com.stambia.wsdl.port.address" id="_TqJy8BjkEemyO4lAnue3pA" value="https://api.chd-expert.com/api/search/"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_UAzYYBjkEemyO4lAnue3pA" value="HTTP"/>
      <attribute defType="com.stambia.wsdl.port.verb" id="_Ud6LIBjkEemyO4lAnue3pA" value="GET"/>
      <attribute defType="com.stambia.wsdl.port.httpAuthorization" id="__7WRKBjkEemyO4lAnue3pA"/>
      <attribute defType="com.stambia.wsdl.port.httpProxyAuthorization" id="__7WRKRjkEemyO4lAnue3pA"/>
      <node defType="com.stambia.wsdl.operation" id="_WYr1wBjkEemyO4lAnue3pA" name="getInfosBySiretBrut">
        <attribute defType="com.stambia.wsdl.operation.address" id="_ZlvAkBjkEemyO4lAnue3pA" value="legal_number?legal_numbers={siret}"/>
        <attribute defType="com.stambia.wsdl.operation.httpHeaderProperties" id="_aoxx8BjkEemyO4lAnue3pA" value="Content_Type:application-json"/>
        <node defType="com.stambia.wsdl.input" id="__7WRFxjkEemyO4lAnue3pA">
          <node defType="com.stambia.wsdl.part" id="__7WRGBjkEemyO4lAnue3pA" name="siret">
            <attribute defType="com.stambia.wsdl.part.type" id="__7WRGRjkEemyO4lAnue3pA" value="string"/>
            <attribute defType="com.stambia.wsdl.part.bindingType" id="__7WRGhjkEemyO4lAnue3pA" value="http:urlReplacement"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="__7WRGxjkEemyO4lAnue3pA" name="Authorization">
            <attribute defType="com.stambia.wsdl.part.type" id="__7WRHBjkEemyO4lAnue3pA" value="string"/>
            <attribute defType="com.stambia.wsdl.part.bindingType" id="__7WRHRjkEemyO4lAnue3pA" value="http:header"/>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="__7WRHhjkEemyO4lAnue3pA">
          <node defType="com.stambia.wsdl.part" id="__7WRHxjkEemyO4lAnue3pA" name="Date" position="8">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="__7WRIBjkEemyO4lAnue3pA" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="__7WRIRjkEemyO4lAnue3pA" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="__7WRIhjkEemyO4lAnue3pA" name="Content-Length" position="18">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="__7WRIxjkEemyO4lAnue3pA" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="__7WRJBjkEemyO4lAnue3pA" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="__7WRJRjkEemyO4lAnue3pA" name="Content-Type" position="20">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="__7WRJhjkEemyO4lAnue3pA" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="__7WRJxjkEemyO4lAnue3pA" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_DmSRQBjlEemyO4lAnue3pA" name="text">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_I0SAIBjlEemyO4lAnue3pA" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_Q6c2ABjlEemyO4lAnue3pA" value="text/plain"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_R0cZEBjlEemyO4lAnue3pA" value="string"/>
          </node>
        </node>
      </node>
      <node defType="com.stambia.wsdl.operation" id="_pbAhwJ7NEeu81ZNiQYZNxg" name="getInfosBySiretArray">
        <attribute defType="com.stambia.wsdl.operation.address" id="_v5XsUJ7NEeu81ZNiQYZNxg" value="legal_number?legal_numbers={siret}"/>
        <attribute defType="com.stambia.wsdl.operation.httpHeaderProperties" id="_w1D4gJ7NEeu81ZNiQYZNxg" value="Content_Type:application-json"/>
        <node defType="com.stambia.wsdl.input" id="_wea6IJ7NEeu81ZNiQYZNxg">
          <node defType="com.stambia.wsdl.part" id="_0E878J7NEeu81ZNiQYZNxg" name="Authorization">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_3Pkd0J7NEeu81ZNiQYZNxg" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_3_LV0J7NEeu81ZNiQYZNxg" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_6UTaIJ7NEeu81ZNiQYZNxg" name="siret">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_78rtMJ7NEeu81ZNiQYZNxg" value="http:urlReplacement"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_8f_1oJ7NEeu81ZNiQYZNxg" value="string"/>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="_ydICgZ7NEeu81ZNiQYZNxg">
          <node defType="com.stambia.wsdl.part" id="_-VTxcJ7NEeu81ZNiQYZNxg" name="Date">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_BDdZwJ7OEeu81ZNiQYZNxg" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_CWtxYJ7OEeu81ZNiQYZNxg" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_E91koJ7OEeu81ZNiQYZNxg" name="Content-Length">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_HdFOQJ7OEeu81ZNiQYZNxg" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_IGewkJ7OEeu81ZNiQYZNxg" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_IahG4J7OEeu81ZNiQYZNxg" name="Content-Type">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_K2faEJ7OEeu81ZNiQYZNxg" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_Lgy7UJ7OEeu81ZNiQYZNxg" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_RhYkwJ7OEeu81ZNiQYZNxg" name="result">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_bHHv8J7OEeu81ZNiQYZNxg" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_caeOMJ7OEeu81ZNiQYZNxg" value="application/json"/>
            <node defType="com.stambia.json.rootArray" id="_dQOxkZ7OEeu81ZNiQYZNxg" name="chdinfos">
              <attribute defType="com.stambia.json.rootArray.reverseFilePath" id="_jEUQAJ7OEeu81ZNiQYZNxg" value="C:\app_dev\Used_Files\In_Files\Json\chdinfos.json"/>
              <node defType="com.stambia.json.object" id="_mnKrsZ7OEeu81ZNiQYZNxg" name="item" position="1">
                <node defType="com.stambia.json.value" id="_mnKrsp7OEeu81ZNiQYZNxg" name="chd_id" position="1">
                  <attribute defType="com.stambia.json.value.type" id="_mnKrs57OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKrtJ7OEeu81ZNiQYZNxg" name="dba_name" position="2">
                  <attribute defType="com.stambia.json.value.type" id="_mnKrtZ7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKrtp7OEeu81ZNiQYZNxg" name="legal_name" position="3">
                  <attribute defType="com.stambia.json.value.type" id="_mnKrt57OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKruJ7OEeu81ZNiQYZNxg" name="address" position="4">
                  <attribute defType="com.stambia.json.value.type" id="_mnKruZ7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKrup7OEeu81ZNiQYZNxg" name="address2" position="5">
                  <attribute defType="com.stambia.json.value.type" id="_mnKru57OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKrvJ7OEeu81ZNiQYZNxg" name="postal_code" position="6">
                  <attribute defType="com.stambia.json.value.type" id="_mnKrvZ7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKrvp7OEeu81ZNiQYZNxg" name="city" position="7">
                  <attribute defType="com.stambia.json.value.type" id="_mnKrv57OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKrwJ7OEeu81ZNiQYZNxg" name="phone" position="8">
                  <attribute defType="com.stambia.json.value.type" id="_mnKrwZ7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKrwp7OEeu81ZNiQYZNxg" name="fax" position="9">
                  <attribute defType="com.stambia.json.value.type" id="_mnKrw57OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKrxJ7OEeu81ZNiQYZNxg" name="legal_number1" position="10">
                  <attribute defType="com.stambia.json.value.type" id="_mnKrxZ7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKrxp7OEeu81ZNiQYZNxg" name="website" position="11">
                  <attribute defType="com.stambia.json.value.type" id="_mnKrx57OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnKryJ7OEeu81ZNiQYZNxg" name="chain_name" position="12">
                  <attribute defType="com.stambia.json.value.type" id="_mnKryZ7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLSwJ7OEeu81ZNiQYZNxg" name="organisation_type" position="13">
                  <attribute defType="com.stambia.json.value.type" id="_mnLSwZ7OEeu81ZNiQYZNxg" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLSwp7OEeu81ZNiQYZNxg" name="operation_type" position="14">
                  <attribute defType="com.stambia.json.value.type" id="_mnLSw57OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLSxJ7OEeu81ZNiQYZNxg" name="market_segment" position="15">
                  <attribute defType="com.stambia.json.value.type" id="_mnLSxZ7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLSxp7OEeu81ZNiQYZNxg" name="menu_type" position="16">
                  <attribute defType="com.stambia.json.value.type" id="_mnLSx57OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLSyJ7OEeu81ZNiQYZNxg" name="number_of_rooms" position="17">
                  <attribute defType="com.stambia.json.value.type" id="_mnLSyZ7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLSyp7OEeu81ZNiQYZNxg" name="number_of_rooms_range" position="18">
                  <attribute defType="com.stambia.json.value.type" id="_mnLSy57OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLSzJ7OEeu81ZNiQYZNxg" name="annual_sales_range" position="19">
                  <attribute defType="com.stambia.json.value.type" id="_mnLSzZ7OEeu81ZNiQYZNxg" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLSzp7OEeu81ZNiQYZNxg" name="confidence_level" position="20">
                  <attribute defType="com.stambia.json.value.type" id="_mnLSz57OEeu81ZNiQYZNxg" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS0J7OEeu81ZNiQYZNxg" name="average_check_range" position="21">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS0Z7OEeu81ZNiQYZNxg" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS0p7OEeu81ZNiQYZNxg" name="years_in_business_range" position="22">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS057OEeu81ZNiQYZNxg" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS1J7OEeu81ZNiQYZNxg" name="number_of_employees_range" position="23">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS1Z7OEeu81ZNiQYZNxg" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS1p7OEeu81ZNiQYZNxg" name="number_of_pieces_of_bred_range" position="24">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS157OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS2J7OEeu81ZNiQYZNxg" name="ownership" position="25">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS2Z7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS2p7OEeu81ZNiQYZNxg" name="number_of_meals_range" position="26">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS257OEeu81ZNiQYZNxg" value="number"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS3J7OEeu81ZNiQYZNxg" name="number_of_beds_range" position="27">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS3Z7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS3p7OEeu81ZNiQYZNxg" name="number_of_students_range" position="28">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS357OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS4J7OEeu81ZNiQYZNxg" name="closed_date" position="29">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS4Z7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS4p7OEeu81ZNiQYZNxg" name="open_date" position="30">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS457OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnLS5J7OEeu81ZNiQYZNxg" name="contact_name" position="31">
                  <attribute defType="com.stambia.json.value.type" id="_mnLS5Z7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnMg4J7OEeu81ZNiQYZNxg" name="latitude" position="32">
                  <attribute defType="com.stambia.json.value.type" id="_mnMg4Z7OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnMg4p7OEeu81ZNiQYZNxg" name="longitude" position="33">
                  <attribute defType="com.stambia.json.value.type" id="_mnMg457OEeu81ZNiQYZNxg" value="string"/>
                </node>
                <node defType="com.stambia.json.value" id="_mnMg5J7OEeu81ZNiQYZNxg" name="terrace" position="34">
                  <attribute defType="com.stambia.json.value.type" id="_mnMg5Z7OEeu81ZNiQYZNxg" value="boolean"/>
                </node>
                <node defType="com.stambia.json.object" id="_mnMg5p7OEeu81ZNiQYZNxg" name="hours" position="35">
                  <node defType="com.stambia.json.array" id="_mnMg557OEeu81ZNiQYZNxg" name="fri" position="1">
                    <node defType="com.stambia.json.object" id="_mnMg6J7OEeu81ZNiQYZNxg" name="item" position="1">
                      <node defType="com.stambia.json.object" id="_mnMg6Z7OEeu81ZNiQYZNxg" name="open" position="1">
                        <node defType="com.stambia.json.value" id="_mnMg6p7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnMg657OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnMg7J7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnMg7Z7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                      <node defType="com.stambia.json.object" id="_mnMg7p7OEeu81ZNiQYZNxg" name="close" position="2">
                        <node defType="com.stambia.json.value" id="_mnMg757OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnMg8J7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnMg8Z7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnMg8p7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node defType="com.stambia.json.array" id="_mnNH8p7OEeu81ZNiQYZNxg" name="mon" position="2">
                    <node defType="com.stambia.json.object" id="_mnNH857OEeu81ZNiQYZNxg" name="item" position="1">
                      <node defType="com.stambia.json.object" id="_mnNH9J7OEeu81ZNiQYZNxg" name="open" position="1">
                        <node defType="com.stambia.json.value" id="_mnNH9Z7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNH9p7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNH957OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNH-J7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                      <node defType="com.stambia.json.object" id="_mnNH-Z7OEeu81ZNiQYZNxg" name="close" position="2">
                        <node defType="com.stambia.json.value" id="_mnNH-p7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNH-57OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNH_J7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNH_Z7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node defType="com.stambia.json.array" id="_mnNIBZ7OEeu81ZNiQYZNxg" name="sat" position="3">
                    <node defType="com.stambia.json.object" id="_mnNIBp7OEeu81ZNiQYZNxg" name="item" position="1">
                      <node defType="com.stambia.json.object" id="_mnNIB57OEeu81ZNiQYZNxg" name="open" position="1">
                        <node defType="com.stambia.json.value" id="_mnNICJ7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNICZ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNICp7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIC57OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                      <node defType="com.stambia.json.object" id="_mnNIDJ7OEeu81ZNiQYZNxg" name="close" position="2">
                        <node defType="com.stambia.json.value" id="_mnNIDZ7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIDp7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNID57OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIEJ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node defType="com.stambia.json.array" id="_mnNIGJ7OEeu81ZNiQYZNxg" name="sun" position="4">
                    <node defType="com.stambia.json.object" id="_mnNIGZ7OEeu81ZNiQYZNxg" name="item" position="1">
                      <node defType="com.stambia.json.object" id="_mnNIGp7OEeu81ZNiQYZNxg" name="open" position="1">
                        <node defType="com.stambia.json.value" id="_mnNIG57OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIHJ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNIHZ7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIHp7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                      <node defType="com.stambia.json.object" id="_mnNIH57OEeu81ZNiQYZNxg" name="close" position="2">
                        <node defType="com.stambia.json.value" id="_mnNIIJ7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIIZ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNIIp7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNII57OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node defType="com.stambia.json.array" id="_mnNIK57OEeu81ZNiQYZNxg" name="thu" position="5">
                    <node defType="com.stambia.json.object" id="_mnNILJ7OEeu81ZNiQYZNxg" name="item" position="1">
                      <node defType="com.stambia.json.object" id="_mnNILZ7OEeu81ZNiQYZNxg" name="open" position="1">
                        <node defType="com.stambia.json.value" id="_mnNILp7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIL57OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNIMJ7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIMZ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                      <node defType="com.stambia.json.object" id="_mnNIMp7OEeu81ZNiQYZNxg" name="close" position="2">
                        <node defType="com.stambia.json.value" id="_mnNIM57OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNINJ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNINZ7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNINp7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node defType="com.stambia.json.array" id="_mnNIPp7OEeu81ZNiQYZNxg" name="tue" position="6">
                    <node defType="com.stambia.json.object" id="_mnNIP57OEeu81ZNiQYZNxg" name="item" position="1">
                      <node defType="com.stambia.json.object" id="_mnNIQJ7OEeu81ZNiQYZNxg" name="open" position="1">
                        <node defType="com.stambia.json.value" id="_mnNIQZ7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIQp7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNIQ57OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIRJ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                      <node defType="com.stambia.json.object" id="_mnNIRZ7OEeu81ZNiQYZNxg" name="close" position="2">
                        <node defType="com.stambia.json.value" id="_mnNIRp7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIR57OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNISJ7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNISZ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node defType="com.stambia.json.array" id="_mnNIUZ7OEeu81ZNiQYZNxg" name="wed" position="7">
                    <node defType="com.stambia.json.object" id="_mnNIUp7OEeu81ZNiQYZNxg" name="item" position="1">
                      <node defType="com.stambia.json.object" id="_mnNIU57OEeu81ZNiQYZNxg" name="open" position="1">
                        <node defType="com.stambia.json.value" id="_mnNIVJ7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIVZ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNIVp7OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIV57OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                      <node defType="com.stambia.json.object" id="_mnNIWJ7OEeu81ZNiQYZNxg" name="close" position="2">
                        <node defType="com.stambia.json.value" id="_mnNIWZ7OEeu81ZNiQYZNxg" name="hour" position="1">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIWp7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                        <node defType="com.stambia.json.value" id="_mnNIW57OEeu81ZNiQYZNxg" name="minute" position="2">
                          <attribute defType="com.stambia.json.value.type" id="_mnNIXJ7OEeu81ZNiQYZNxg" value="number"/>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node defType="com.stambia.xml.propertyField" id="_zztwE57PEeu81ZNiQYZNxg" name="string_content"/>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node defType="com.stambia.wsdl.port" id="_wU_qgPAvEem_mo4tieUuqA" name="GetErrorResponse">
      <attribute defType="com.stambia.wsdl.port.address" id="_0rfwsPAvEem_mo4tieUuqA" value="https://api.chd-expert.com/api/search/"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_1WRzEPAvEem_mo4tieUuqA" value="HTTP"/>
      <attribute defType="com.stambia.wsdl.port.verb" id="_15PWMPAvEem_mo4tieUuqA" value="GET"/>
      <node defType="com.stambia.wsdl.operation" id="_9gkT8PAvEem_mo4tieUuqA" name="getInfosError">
        <attribute defType="com.stambia.wsdl.operation.address" id="__KNLQPAvEem_mo4tieUuqA" value="legal_number?legal_numbers={siret}"/>
        <attribute defType="com.stambia.wsdl.operation.httpHeaderProperties" id="_CZt0MPAwEem_mo4tieUuqA" value="Content_Type:application-json"/>
        <node defType="com.stambia.wsdl.input" id="_MNVqYPAwEem_mo4tieUuqA">
          <node defType="com.stambia.wsdl.part" id="_Oxlv0PAwEem_mo4tieUuqA" name="Authorization">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_VB0XwPAwEem_mo4tieUuqA" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_XdlPkPAwEem_mo4tieUuqA" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_ZG4IoPAwEem_mo4tieUuqA" name="siret">
            <attribute defType="com.stambia.wsdl.part.type" id="_ahwyQPAwEem_mo4tieUuqA" value="string"/>
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_bNZwQPAwEem_mo4tieUuqA" value="http:urlReplacement"/>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="_tQPqofAwEem_mo4tieUuqA">
          <node defType="com.stambia.wsdl.part" id="_tnshUPAwEem_mo4tieUuqA" name="result">
            <node defType="com.stambia.json.rootObject" id="_86gkEPAwEem_mo4tieUuqA" name="chdinfos">
              <attribute defType="com.stambia.json.rootObject.encoding" id="_863JYPAwEem_mo4tieUuqA" value="UTF-8"/>
              <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_866MsPAwEem_mo4tieUuqA"/>
              <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_866MsfAwEem_mo4tieUuqA" value="C:\app_dev\Used_Files\In_Files\Json\chdinfos.json"/>
              <node defType="com.stambia.json.value" id="_Q364YfAyEem_mo4tieUuqA" name="number_of_rooms" position="17">
                <attribute defType="com.stambia.json.value.type" id="_Q364YvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364afAyEem_mo4tieUuqA" name="average_check_range" position="21">
                <attribute defType="com.stambia.json.value.type" id="_Q364avAyEem_mo4tieUuqA" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364gfAyEem_mo4tieUuqA" name="longitude" position="33">
                <attribute defType="com.stambia.json.value.type" id="_Q364gvAyEem_mo4tieUuqA" value="number"/>
              </node>
              <node defType="com.stambia.json.object" id="_Q364hfAyEem_mo4tieUuqA" name="hours" position="35">
                <node defType="com.stambia.json.array" id="_Q364hvAyEem_mo4tieUuqA" name="fri" position="1">
                  <node defType="com.stambia.json.object" id="_Q364h_AyEem_mo4tieUuqA" name="item" position="1">
                    <node defType="com.stambia.json.object" id="_Q364iPAyEem_mo4tieUuqA" name="open" position="1">
                      <node defType="com.stambia.json.value" id="_Q364ifAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q364ivAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q364i_AyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q364jPAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_Q364jfAyEem_mo4tieUuqA" name="close" position="2">
                      <node defType="com.stambia.json.value" id="_Q364jvAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q364j_AyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q364kPAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q364kfAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                  </node>
                </node>
                <node defType="com.stambia.json.array" id="_Q38GZPAyEem_mo4tieUuqA" name="mon" position="2">
                  <node defType="com.stambia.json.object" id="_Q38GZfAyEem_mo4tieUuqA" name="item" position="1">
                    <node defType="com.stambia.json.object" id="_Q38GZvAyEem_mo4tieUuqA" name="open" position="1">
                      <node defType="com.stambia.json.value" id="_Q38GZ_AyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GaPAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38GafAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GavAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_Q38Ga_AyEem_mo4tieUuqA" name="close" position="2">
                      <node defType="com.stambia.json.value" id="_Q38GbPAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GbfAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38GbvAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38Gb_AyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                  </node>
                </node>
                <node defType="com.stambia.json.array" id="_Q38Gd_AyEem_mo4tieUuqA" name="sat" position="3">
                  <node defType="com.stambia.json.object" id="_Q38GePAyEem_mo4tieUuqA" name="item" position="1">
                    <node defType="com.stambia.json.object" id="_Q38GefAyEem_mo4tieUuqA" name="open" position="1">
                      <node defType="com.stambia.json.value" id="_Q38GevAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38Ge_AyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38GfPAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GffAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_Q38GfvAyEem_mo4tieUuqA" name="close" position="2">
                      <node defType="com.stambia.json.value" id="_Q38Gf_AyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GgPAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38GgfAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GgvAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                  </node>
                </node>
                <node defType="com.stambia.json.array" id="_Q38GivAyEem_mo4tieUuqA" name="sun" position="4">
                  <node defType="com.stambia.json.object" id="_Q38Gi_AyEem_mo4tieUuqA" name="item" position="1">
                    <node defType="com.stambia.json.object" id="_Q38GjPAyEem_mo4tieUuqA" name="open" position="1">
                      <node defType="com.stambia.json.value" id="_Q38GjfAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GjvAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38Gj_AyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GkPAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_Q38GkfAyEem_mo4tieUuqA" name="close" position="2">
                      <node defType="com.stambia.json.value" id="_Q38GkvAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38Gk_AyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38GlPAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GlfAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                  </node>
                </node>
                <node defType="com.stambia.json.array" id="_Q38GlvAyEem_mo4tieUuqA" name="thu" position="5">
                  <node defType="com.stambia.json.object" id="_Q38Gl_AyEem_mo4tieUuqA" name="item" position="1">
                    <node defType="com.stambia.json.object" id="_Q38GmPAyEem_mo4tieUuqA" name="open" position="1">
                      <node defType="com.stambia.json.value" id="_Q38GmfAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GmvAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38Gm_AyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GnPAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_Q38GnfAyEem_mo4tieUuqA" name="close" position="2">
                      <node defType="com.stambia.json.value" id="_Q38GnvAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38Gn_AyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38GoPAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GofAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                  </node>
                </node>
                <node defType="com.stambia.json.array" id="_Q38GqfAyEem_mo4tieUuqA" name="tue" position="6">
                  <node defType="com.stambia.json.object" id="_Q38GqvAyEem_mo4tieUuqA" name="item" position="1">
                    <node defType="com.stambia.json.object" id="_Q38Gq_AyEem_mo4tieUuqA" name="open" position="1">
                      <node defType="com.stambia.json.value" id="_Q38GrPAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GrfAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38GrvAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38Gr_AyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_Q38GsPAyEem_mo4tieUuqA" name="close" position="2">
                      <node defType="com.stambia.json.value" id="_Q38GsfAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GsvAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38Gs_AyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GtPAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                  </node>
                </node>
                <node defType="com.stambia.json.array" id="_Q38GvPAyEem_mo4tieUuqA" name="wed" position="7">
                  <node defType="com.stambia.json.object" id="_Q38GvfAyEem_mo4tieUuqA" name="item" position="1">
                    <node defType="com.stambia.json.object" id="_Q38GvvAyEem_mo4tieUuqA" name="open" position="1">
                      <node defType="com.stambia.json.value" id="_Q38Gv_AyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GwPAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38GwfAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GwvAyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                    <node defType="com.stambia.json.object" id="_Q38Gw_AyEem_mo4tieUuqA" name="close" position="2">
                      <node defType="com.stambia.json.value" id="_Q38GxPAyEem_mo4tieUuqA" name="hour" position="1">
                        <attribute defType="com.stambia.json.value.type" id="_Q38GxfAyEem_mo4tieUuqA" value="number"/>
                      </node>
                      <node defType="com.stambia.json.value" id="_Q38GxvAyEem_mo4tieUuqA" name="minute" position="2">
                        <attribute defType="com.stambia.json.value.type" id="_Q38Gx_AyEem_mo4tieUuqA" value="number"/>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node defType="com.stambia.json.value" id="_Q364X_AyEem_mo4tieUuqA" name="menu_type" position="16">
                <attribute defType="com.stambia.json.value.type" id="_Q364YPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364UfAyEem_mo4tieUuqA" name="fax" position="9">
                <attribute defType="com.stambia.json.value.type" id="_Q364UvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364f_AyEem_mo4tieUuqA" name="latitude" position="32">
                <attribute defType="com.stambia.json.value.type" id="_Q364gPAyEem_mo4tieUuqA" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364Y_AyEem_mo4tieUuqA" name="number_of_rooms_range" position="18">
                <attribute defType="com.stambia.json.value.type" id="_Q364ZPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364efAyEem_mo4tieUuqA" name="closed_date" position="29">
                <attribute defType="com.stambia.json.value.type" id="_Q364evAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364R_AyEem_mo4tieUuqA" name="address" position="4">
                <attribute defType="com.stambia.json.value.type" id="_Q364SPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364TfAyEem_mo4tieUuqA" name="city" position="7">
                <attribute defType="com.stambia.json.value.type" id="_Q364TvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364a_AyEem_mo4tieUuqA" name="years_in_business_range" position="22">
                <attribute defType="com.stambia.json.value.type" id="_Q364bPAyEem_mo4tieUuqA" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364g_AyEem_mo4tieUuqA" name="terrace" position="34">
                <attribute defType="com.stambia.json.value.type" id="_Q364hPAyEem_mo4tieUuqA" value="boolean"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364V_AyEem_mo4tieUuqA" name="chain_name" position="12">
                <attribute defType="com.stambia.json.value.type" id="_Q364WPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364ZfAyEem_mo4tieUuqA" name="annual_sales_range" position="19">
                <attribute defType="com.stambia.json.value.type" id="_Q364ZvAyEem_mo4tieUuqA" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364RfAyEem_mo4tieUuqA" name="legal_name" position="3">
                <attribute defType="com.stambia.json.value.type" id="_Q364RvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364Q_AyEem_mo4tieUuqA" name="dba_name" position="2">
                <attribute defType="com.stambia.json.value.type" id="_Q364RPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364T_AyEem_mo4tieUuqA" name="phone" position="8">
                <attribute defType="com.stambia.json.value.type" id="_Q364UPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364VfAyEem_mo4tieUuqA" name="website" position="11">
                <attribute defType="com.stambia.json.value.type" id="_Q364VvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364e_AyEem_mo4tieUuqA" name="open_date" position="30">
                <attribute defType="com.stambia.json.value.type" id="_Q364fPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364W_AyEem_mo4tieUuqA" name="operation_type" position="14">
                <attribute defType="com.stambia.json.value.type" id="_Q364XPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364SfAyEem_mo4tieUuqA" name="address2" position="5">
                <attribute defType="com.stambia.json.value.type" id="_Q364SvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364ffAyEem_mo4tieUuqA" name="contact_name" position="31">
                <attribute defType="com.stambia.json.value.type" id="_Q364fvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364S_AyEem_mo4tieUuqA" name="postal_code" position="6">
                <attribute defType="com.stambia.json.value.type" id="_Q364TPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364b_AyEem_mo4tieUuqA" name="number_of_pieces_of_bred_range" position="24">
                <attribute defType="com.stambia.json.value.type" id="_Q364cPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364c_AyEem_mo4tieUuqA" name="number_of_meals_range" position="26">
                <attribute defType="com.stambia.json.value.type" id="_Q364dPAyEem_mo4tieUuqA" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364WfAyEem_mo4tieUuqA" name="organisation_type" position="13">
                <attribute defType="com.stambia.json.value.type" id="_Q364WvAyEem_mo4tieUuqA" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364bfAyEem_mo4tieUuqA" name="number_of_employees_range" position="23">
                <attribute defType="com.stambia.json.value.type" id="_Q364bvAyEem_mo4tieUuqA" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364Z_AyEem_mo4tieUuqA" name="confidence_level" position="20">
                <attribute defType="com.stambia.json.value.type" id="_Q364aPAyEem_mo4tieUuqA" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364cfAyEem_mo4tieUuqA" name="ownership" position="25">
                <attribute defType="com.stambia.json.value.type" id="_Q364cvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364d_AyEem_mo4tieUuqA" name="number_of_students_range" position="28">
                <attribute defType="com.stambia.json.value.type" id="_Q364ePAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364U_AyEem_mo4tieUuqA" name="legal_number1" position="10">
                <attribute defType="com.stambia.json.value.type" id="_Q364VPAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364XfAyEem_mo4tieUuqA" name="market_segment" position="15">
                <attribute defType="com.stambia.json.value.type" id="_Q364XvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364QfAyEem_mo4tieUuqA" name="chd_id" position="1">
                <attribute defType="com.stambia.json.value.type" id="_Q364QvAyEem_mo4tieUuqA" value="string"/>
              </node>
              <node defType="com.stambia.json.value" id="_Q364dfAyEem_mo4tieUuqA" name="number_of_beds_range" position="27">
                <attribute defType="com.stambia.json.value.type" id="_Q364dvAyEem_mo4tieUuqA" value="string"/>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>