<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_UL4fgE5mEeujLNG6r1heKg" md:ref="platform:/plugin/com.indy.environment/technology/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_UU-KEE5mEeujLNG6r1heKg" name="wsLog_kibana">
    <attribute defType="com.stambia.json.rootObject.encoding" id="_UWplEE5mEeujLNG6r1heKg" value="UTF-8"/>
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_UWvEoE5mEeujLNG6r1heKg"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_UWxg4E5mEeujLNG6r1heKg" value="C:\app_dev\Used_Files\In_Files\Json\wsLog_kibana.json"/>
    <node defType="com.stambia.json.value" id="_5K7lMU5mEeujLNG6r1heKg" name="index" position="1">
      <attribute defType="com.stambia.json.value.type" id="_5K7lMk5mEeujLNG6r1heKg" value="string"/>
    </node>
    <node defType="com.stambia.json.array" id="_5K7lM05mEeujLNG6r1heKg" name="log" position="2">
      <node defType="com.stambia.json.object" id="_5K7lNE5mEeujLNG6r1heKg" name="item" position="1">
        <node defType="com.stambia.json.object" id="_5K7lNU5mEeujLNG6r1heKg" name="context" position="1">
          <node defType="com.stambia.json.value" id="_5K7lNk5mEeujLNG6r1heKg" name="id_societe_magento" position="1">
            <attribute defType="com.stambia.json.value.type" id="_5K7lN05mEeujLNG6r1heKg" value="number"/>
          </node>
          <node defType="com.stambia.json.value" id="_5K7lOE5mEeujLNG6r1heKg" name="id_user_magento" position="2">
            <attribute defType="com.stambia.json.value.type" id="_5K7lOU5mEeujLNG6r1heKg" value="number"/>
          </node>
          <node defType="com.stambia.json.value" id="_5K7lOk5mEeujLNG6r1heKg" name="id_client_chomette" position="3">
            <attribute defType="com.stambia.json.value.type" id="_5K7lO05mEeujLNG6r1heKg" value="number"/>
          </node>
          <node defType="com.stambia.json.value" id="_5K7lPE5mEeujLNG6r1heKg" name="id_commande_magento" position="4">
            <attribute defType="com.stambia.json.value.type" id="_5K7lPU5mEeujLNG6r1heKg" value="number"/>
          </node>
          <node defType="com.stambia.json.value" id="_5K7lPk5mEeujLNG6r1heKg" name="nom_fichier" position="5">
            <attribute defType="com.stambia.json.value.type" id="_5K7lP05mEeujLNG6r1heKg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_5K7lQE5mEeujLNG6r1heKg" name="id_commande_chomette" position="6">
            <attribute defType="com.stambia.json.value.type" id="_5K7lQU5mEeujLNG6r1heKg" value="number"/>
          </node>
          <node defType="com.stambia.json.value" id="_5K7lQk5mEeujLNG6r1heKg" name="nombre_contexte" position="7">
            <attribute defType="com.stambia.json.value.type" id="_5K7lQ05mEeujLNG6r1heKg" value="number"/>
          </node>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lRE5mEeujLNG6r1heKg" name="duration" position="2">
          <attribute defType="com.stambia.json.value.type" id="_5K7lRU5mEeujLNG6r1heKg" value="number"/>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lRk5mEeujLNG6r1heKg" name="ending_process" position="3">
          <attribute defType="com.stambia.json.value.type" id="_5K7lR05mEeujLNG6r1heKg" value="number"/>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lSE5mEeujLNG6r1heKg" name="hostname" position="4">
          <attribute defType="com.stambia.json.value.type" id="_5K7lSU5mEeujLNG6r1heKg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lSk5mEeujLNG6r1heKg" name="ip" position="5">
          <attribute defType="com.stambia.json.value.type" id="_5K7lS05mEeujLNG6r1heKg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lTE5mEeujLNG6r1heKg" name="level_id" position="6">
          <attribute defType="com.stambia.json.value.type" id="_5K7lTU5mEeujLNG6r1heKg" value="number"/>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lTk5mEeujLNG6r1heKg" name="level_name" position="7">
          <attribute defType="com.stambia.json.value.type" id="_5K7lT05mEeujLNG6r1heKg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lUE5mEeujLNG6r1heKg" name="message" position="8">
          <attribute defType="com.stambia.json.value.type" id="_5K7lUU5mEeujLNG6r1heKg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lUk5mEeujLNG6r1heKg" name="process" position="9">
          <attribute defType="com.stambia.json.value.type" id="_5K7lU05mEeujLNG6r1heKg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lVE5mEeujLNG6r1heKg" name="process_uid" position="10">
          <attribute defType="com.stambia.json.value.type" id="_5K7lVU5mEeujLNG6r1heKg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_5K7lVk5mEeujLNG6r1heKg" name="timestamp" position="11">
          <attribute defType="com.stambia.json.value.type" id="_5K7lV05mEeujLNG6r1heKg" value="string"/>
        </node>
      </node>
    </node>
  </node>
</md:node>