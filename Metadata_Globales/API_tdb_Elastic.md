<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.wsdl.wsdl" id="_onYocE5oEeujLNG6r1heKg" name="MyWebservicesElastic" md:ref="platform:/plugin/com.indy.environment/technology/web/wsdl.tech#UUID_TECH_WSDL1?fileId=UUID_TECH_WSDL1$type=tech$name=wsdl?">
  <attribute defType="com.stambia.wsdl.wsdl.xsdReverseVersion" id="_onf9ME5oEeujLNG6r1heKg" value="1"/>
  <attribute defType="com.stambia.wsdl.wsdl.url" id="_ouof8E5oEeujLNG6r1heKg"/>
  <node defType="com.stambia.wsdl.service" id="_8RmJQE5oEeujLNG6r1heKg" name="ServicelogElastic">
    <node defType="com.stambia.wsdl.port" id="_PabbUE5pEeujLNG6r1heKg" name="LogElastic">
      <attribute defType="com.stambia.wsdl.port.address" id="_cfMSIE5pEeujLNG6r1heKg" value="https://tdbapi.ecf-info.net/message.php"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_crEyAE5pEeujLNG6r1heKg" value="HTTP"/>
      <attribute defType="com.stambia.wsdl.port.verb" id="_dSA2ME5pEeujLNG6r1heKg" value="POST"/>
      <node defType="com.stambia.wsdl.operation" id="_kXLiEE5pEeujLNG6r1heKg" name="PostMessage">
        <attribute defType="com.stambia.wsdl.operation.httpHeaderProperties" id="_on9awE5pEeujLNG6r1heKg" value="Content-Type: application-json"/>
        <attribute defType="com.stambia.wsdl.operation.address" id="_2Ls_4E6mEeujLNG6r1heKg" value="/"/>
        <node defType="com.stambia.wsdl.input" id="_uXWmgE5pEeujLNG6r1heKg">
          <node defType="com.stambia.wsdl.part" id="_vfLxQE5pEeujLNG6r1heKg" name="Authorization">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_y-BPoE5pEeujLNG6r1heKg" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_0cyTsE5pEeujLNG6r1heKg" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_3PoOwE5pEeujLNG6r1heKg" name="content">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_5wnk0E5pEeujLNG6r1heKg" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_6sRUwE5pEeujLNG6r1heKg" value="application/json"/>
            <node defType="com.stambia.json.rootObject" id="_RkON1U5zEeujLNG6r1heKg" name="wsElasticMsg">
              <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_c0avsE5zEeujLNG6r1heKg" value="C:\app_dev\Used_Files\In_Files\Json\wsLog_kibana.json"/>
              <node defType="com.stambia.json.array" id="_froNY05zEeujLNG6r1heKg" name="log" position="2">
                <node defType="com.stambia.json.object" id="_froNZE5zEeujLNG6r1heKg" name="item" position="1">
                  <node defType="com.stambia.json.object" id="_froNZU5zEeujLNG6r1heKg" name="context" position="1">
                    <node defType="com.stambia.json.value" id="_froNZk5zEeujLNG6r1heKg" name="id_societe_magento" position="1">
                      <attribute defType="com.stambia.json.value.type" id="_froNZ05zEeujLNG6r1heKg" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_froNaE5zEeujLNG6r1heKg" name="id_user_magento" position="2">
                      <attribute defType="com.stambia.json.value.type" id="_froNaU5zEeujLNG6r1heKg" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_froNak5zEeujLNG6r1heKg" name="id_client_chomette" position="3">
                      <attribute defType="com.stambia.json.value.type" id="_froNa05zEeujLNG6r1heKg" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_froNbE5zEeujLNG6r1heKg" name="id_commande_magento" position="4">
                      <attribute defType="com.stambia.json.value.type" id="_froNbU5zEeujLNG6r1heKg" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_froNbk5zEeujLNG6r1heKg" name="nom_fichier" position="5">
                      <attribute defType="com.stambia.json.value.type" id="_froNb05zEeujLNG6r1heKg" value="string"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_froNcE5zEeujLNG6r1heKg" name="id_commande_chomette" position="6">
                      <attribute defType="com.stambia.json.value.type" id="_froNcU5zEeujLNG6r1heKg" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_froNck5zEeujLNG6r1heKg" name="nombre_contexte" position="7">
                      <attribute defType="com.stambia.json.value.type" id="_froNc05zEeujLNG6r1heKg" value="number"/>
                    </node>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNdE5zEeujLNG6r1heKg" name="duration" position="2">
                    <attribute defType="com.stambia.json.value.type" id="_froNdU5zEeujLNG6r1heKg" value="number"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNdk5zEeujLNG6r1heKg" name="ending_process" position="3">
                    <attribute defType="com.stambia.json.value.type" id="_froNd05zEeujLNG6r1heKg" value="number"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNeE5zEeujLNG6r1heKg" name="hostname" position="4">
                    <attribute defType="com.stambia.json.value.type" id="_froNeU5zEeujLNG6r1heKg" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNek5zEeujLNG6r1heKg" name="ip" position="5">
                    <attribute defType="com.stambia.json.value.type" id="_froNe05zEeujLNG6r1heKg" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNfE5zEeujLNG6r1heKg" name="level_id" position="6">
                    <attribute defType="com.stambia.json.value.type" id="_froNfU5zEeujLNG6r1heKg" value="number"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNfk5zEeujLNG6r1heKg" name="level_name" position="7">
                    <attribute defType="com.stambia.json.value.type" id="_froNf05zEeujLNG6r1heKg" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNgE5zEeujLNG6r1heKg" name="message" position="8">
                    <attribute defType="com.stambia.json.value.type" id="_froNgU5zEeujLNG6r1heKg" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNgk5zEeujLNG6r1heKg" name="process" position="9">
                    <attribute defType="com.stambia.json.value.type" id="_froNg05zEeujLNG6r1heKg" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNhE5zEeujLNG6r1heKg" name="process_uid" position="10">
                    <attribute defType="com.stambia.json.value.type" id="_froNhU5zEeujLNG6r1heKg" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_froNhk5zEeujLNG6r1heKg" name="timestamp" position="11">
                    <attribute defType="com.stambia.json.value.type" id="_froNh05zEeujLNG6r1heKg" value="string"/>
                  </node>
                </node>
              </node>
              <node defType="com.stambia.json.value" id="_froNYU5zEeujLNG6r1heKg" name="index" position="1">
                <attribute defType="com.stambia.json.value.type" id="_froNYk5zEeujLNG6r1heKg" value="string"/>
              </node>
            </node>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="_CMgMAU5qEeujLNG6r1heKg">
          <node defType="com.stambia.wsdl.part" id="_D2_X4E5qEeujLNG6r1heKg" name="response-code">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_Fk7gYE5qEeujLNG6r1heKg" value="http:responseCode"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_G-VjUE5qEeujLNG6r1heKg" value="integer"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_IaZ4sE5qEeujLNG6r1heKg" name="response-msg">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_J14YME5qEeujLNG6r1heKg" value="http:responseMessage"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_LNxYME5qEeujLNG6r1heKg" value="string"/>
          </node>
        </node>
      </node>
    </node>
    <node defType="com.stambia.wsdl.port" id="_vTTDEI1vEeuZUfdHLPu9yA" name="LogElasticV2">
      <attribute defType="com.stambia.wsdl.port.address" id="_zOyoQI1vEeuZUfdHLPu9yA" value="https://tdbapi.ecf-info.net/message.php"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_znJd4I1vEeuZUfdHLPu9yA" value="HTTP"/>
      <attribute defType="com.stambia.wsdl.port.verb" id="_0HACoI1vEeuZUfdHLPu9yA" value="POST"/>
      <node defType="com.stambia.wsdl.operation" id="_yo_MgI1vEeuZUfdHLPu9yA" name="PostMsgV2">
        <attribute defType="com.stambia.wsdl.operation.address" id="_5duPsI1vEeuZUfdHLPu9yA" value="/"/>
        <attribute defType="com.stambia.wsdl.operation.httpHeaderProperties" id="_6sUJwI1vEeuZUfdHLPu9yA" value="Content-Type: application-json"/>
        <node defType="com.stambia.wsdl.input" id="_8SfdYI1vEeuZUfdHLPu9yA">
          <node defType="com.stambia.wsdl.part" id="_AUhIYI1wEeuZUfdHLPu9yA" name="Authorization">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_FlLJsI1wEeuZUfdHLPu9yA" value="http:header"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_GZI9QI1wEeuZUfdHLPu9yA" value="string"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_HfBAMI1wEeuZUfdHLPu9yA" name="content">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_LMXbsI1wEeuZUfdHLPu9yA" value="mime:content"/>
            <attribute defType="com.stambia.wsdl.part.contentType" id="_L-NIUI1wEeuZUfdHLPu9yA" value="application/json"/>
            <node defType="com.stambia.json.rootObject" id="_aG2eAI1wEeuZUfdHLPu9yA" name="wsElasticMsgV2">
              <attribute defType="com.stambia.json.rootObject.encoding" id="_aHvO0I1wEeuZUfdHLPu9yA" value="UTF-8"/>
              <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_aH18gI1wEeuZUfdHLPu9yA"/>
              <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_aH2jkI1wEeuZUfdHLPu9yA" value="C:\app_dev\Used_Files\In_Files\Json\wsLog_kibana.json"/>
              <node defType="com.stambia.json.value" id="_i8IcoY1wEeuZUfdHLPu9yA" name="index" position="1">
                <attribute defType="com.stambia.json.value.type" id="_i8Icoo1wEeuZUfdHLPu9yA" value="string"/>
              </node>
              <node defType="com.stambia.json.array" id="_i8Ico41wEeuZUfdHLPu9yA" name="log" position="2">
                <node defType="com.stambia.json.object" id="_i8IcpI1wEeuZUfdHLPu9yA" name="item" position="1">
                  <node defType="com.stambia.json.object" id="_i8IcpY1wEeuZUfdHLPu9yA" name="context" position="1">
                    <node defType="com.stambia.json.value" id="_i8Icpo1wEeuZUfdHLPu9yA" name="id_societe_magento" position="1">
                      <attribute defType="com.stambia.json.value.type" id="_i8Icp41wEeuZUfdHLPu9yA" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_i8IcqI1wEeuZUfdHLPu9yA" name="id_user_magento" position="2">
                      <attribute defType="com.stambia.json.value.type" id="_i8IcqY1wEeuZUfdHLPu9yA" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_i8Icqo1wEeuZUfdHLPu9yA" name="id_client_chomette" position="3">
                      <attribute defType="com.stambia.json.value.type" id="_i8Icq41wEeuZUfdHLPu9yA" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_i8IcrI1wEeuZUfdHLPu9yA" name="id_commande_magento" position="4">
                      <attribute defType="com.stambia.json.value.type" id="_i8IcrY1wEeuZUfdHLPu9yA" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_i8Icro1wEeuZUfdHLPu9yA" name="nom_fichier" position="5">
                      <attribute defType="com.stambia.json.value.type" id="_i8Icr41wEeuZUfdHLPu9yA" value="string"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_i8IcsI1wEeuZUfdHLPu9yA" name="id_commande_chomette" position="6">
                      <attribute defType="com.stambia.json.value.type" id="_i8IcsY1wEeuZUfdHLPu9yA" value="number"/>
                    </node>
                    <node defType="com.stambia.json.value" id="_i8Icso1wEeuZUfdHLPu9yA" name="nombre_contexte" position="7">
                      <attribute defType="com.stambia.json.value.type" id="_i8Ics41wEeuZUfdHLPu9yA" value="number"/>
                    </node>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8IctI1wEeuZUfdHLPu9yA" name="duration" position="2">
                    <attribute defType="com.stambia.json.value.type" id="_i8IctY1wEeuZUfdHLPu9yA" value="number"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8Icto1wEeuZUfdHLPu9yA" name="ending_process" position="3">
                    <attribute defType="com.stambia.json.value.type" id="_i8Ict41wEeuZUfdHLPu9yA" value="number"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8IcuI1wEeuZUfdHLPu9yA" name="hostname" position="4">
                    <attribute defType="com.stambia.json.value.type" id="_i8IcuY1wEeuZUfdHLPu9yA" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8Icuo1wEeuZUfdHLPu9yA" name="ip" position="5">
                    <attribute defType="com.stambia.json.value.type" id="_i8Icu41wEeuZUfdHLPu9yA" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8IcvI1wEeuZUfdHLPu9yA" name="level_id" position="6">
                    <attribute defType="com.stambia.json.value.type" id="_i8IcvY1wEeuZUfdHLPu9yA" value="number"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8Icvo1wEeuZUfdHLPu9yA" name="level_name" position="7">
                    <attribute defType="com.stambia.json.value.type" id="_i8Icv41wEeuZUfdHLPu9yA" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8IcwI1wEeuZUfdHLPu9yA" name="message" position="8">
                    <attribute defType="com.stambia.json.value.type" id="_i8IcwY1wEeuZUfdHLPu9yA" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8Icwo1wEeuZUfdHLPu9yA" name="process" position="9">
                    <attribute defType="com.stambia.json.value.type" id="_i8Icw41wEeuZUfdHLPu9yA" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8IcxI1wEeuZUfdHLPu9yA" name="process_uid" position="10">
                    <attribute defType="com.stambia.json.value.type" id="_i8IcxY1wEeuZUfdHLPu9yA" value="string"/>
                  </node>
                  <node defType="com.stambia.json.value" id="_i8Icxo1wEeuZUfdHLPu9yA" name="timestamp" position="11">
                    <attribute defType="com.stambia.json.value.type" id="_i8Icx41wEeuZUfdHLPu9yA" value="string"/>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="_-4OwkY1vEeuZUfdHLPu9yA">
          <node defType="com.stambia.wsdl.part" id="_OkTA0I1wEeuZUfdHLPu9yA" name="response-code">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_R4EesI1wEeuZUfdHLPu9yA" value="http:responseCode"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_TNSlMI1wEeuZUfdHLPu9yA" value="integer"/>
          </node>
          <node defType="com.stambia.wsdl.part" id="_Uetw0I1wEeuZUfdHLPu9yA" name="response-msg">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_WdyFkI1wEeuZUfdHLPu9yA" value="http:responseMessage"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_XKaiEI1wEeuZUfdHLPu9yA" value="string"/>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>