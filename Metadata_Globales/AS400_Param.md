<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_qVvFwHfpEema_LGoZACb8w" name="AS400_Param" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/db2_400/ibm_db2_400.rdbms.md#UUID_MD_RDBMS_DB2_400?fileId=UUID_MD_RDBMS_DB2_400$type=md$name=IBM%20DB2/400?">
  <attribute defType="com.stambia.rdbms.server.url" id="_3GdmQHfpEema_LGoZACb8w" value="~/p_url_jdbc_as400"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_3GmwMHfpEema_LGoZACb8w" value="com.ibm.as400.access.AS400JDBCDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_3GmwMXfpEema_LGoZACb8w" value="~/p_usr_jdbc_as400"/>
</md:node>