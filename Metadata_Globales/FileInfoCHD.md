<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.file.server" id="_ebVz8PGFEem_mo4tieUuqA" md:ref="platform:/plugin/com.indy.environment/.tech/file/standard.file.md#UUID_TECH_FILE_MD?fileId=UUID_TECH_FILE_MD$type=md$name=File?">
  <node defType="com.stambia.file.directory" id="_ejXukPGFEem_mo4tieUuqA" name="Temp_file">
    <attribute defType="com.stambia.file.directory.path" id="_1esAsPG6Eem_mo4tieUuqA" value="\\servernt46\Stambia\CHD\Temp_files\"/>
    <node defType="com.stambia.file.file" id="_ej3d0PGFEem_mo4tieUuqA" name="Tempfile">
      <attribute defType="com.stambia.file.file.type" id="_ek-REPGFEem_mo4tieUuqA" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_elAGQfGFEem_mo4tieUuqA" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_elAtUPGFEem_mo4tieUuqA" value="3B"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_elAtUfGFEem_mo4tieUuqA"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_elAtUvGFEem_mo4tieUuqA" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_elBUYPGFEem_mo4tieUuqA" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_elBUYfGFEem_mo4tieUuqA" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_elBUYvGFEem_mo4tieUuqA" value="0"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_rUuq8PG2Eem_mo4tieUuqA" value="${~/NomFichierExport}$"/>
      <node defType="com.stambia.file.propertyField" id="_WgKQpPGGEem_mo4tieUuqA" name="string_content"/>
      <node defType="com.stambia.file.propertyField" id="_Z-zhxPGGEem_mo4tieUuqA" name="file_path"/>
    </node>
  </node>
</md:node>