<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_mtv_cMGfEeirvMQPb5aNLA" name="TestAccesMSSQL" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/mssql/mssql.rdbms.md#UUID_MD_RDBMS_MSSQL?fileId=UUID_MD_RDBMS_MSSQL$type=md$name=Microsoft%20SQL%20Server?">
  <attribute defType="com.stambia.rdbms.server.url" id="_r7L4MMGgEeirvMQPb5aNLA" value="jdbc:sqlserver://10.17.10.4;instance=ECFERP001;databaseName=ERPDIVALTO"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_r7OUcMGgEeirvMQPb5aNLA" value="com.microsoft.sqlserver.jdbc.SQLServerDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_r7OUccGgEeirvMQPb5aNLA" value="sa"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_r7O7gMGgEeirvMQPb5aNLA" value="14FC4407FCFA8D98D7138492054AFBA1"/>
  <node defType="com.stambia.rdbms.schema" id="_m1pXMMGfEeirvMQPb5aNLA" name="ERPDIVALTO.dbo">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_m2C_0MGfEeirvMQPb5aNLA" value="ERPDIVALTO"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_m2Dm4MGfEeirvMQPb5aNLA" value="dbo"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_m2EN8MGfEeirvMQPb5aNLA" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_m2EN8cGfEeirvMQPb5aNLA" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_m2E1AMGfEeirvMQPb5aNLA" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_rvsZ4MGgEeirvMQPb5aNLA" name="ACTION_CONTROLE_V">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rvsZ4cGgEeirvMQPb5aNLA" value="ACTION_CONTROLE_V"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rvsZ4sGgEeirvMQPb5aNLA" value="VIEW"/>
      <node defType="com.stambia.rdbms.column" id="_rxtMEMGgEeirvMQPb5aNLA" name="COACT_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rxtMEcGgEeirvMQPb5aNLA" value="COACT_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rxtMEsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rxtzIMGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rxtzIcGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rxtzIsGgEeirvMQPb5aNLA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rxtzI8GgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rx0g0MGgEeirvMQPb5aNLA" name="DOSSIER" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rx0g0cGgEeirvMQPb5aNLA" value="DOSSIER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rx1H4MGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rx1H4cGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rx1H4sGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rx1H48GgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rx1H5MGgEeirvMQPb5aNLA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rx9DsMGgEeirvMQPb5aNLA" name="IDTABLE" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rx9DscGgEeirvMQPb5aNLA" value="IDTABLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rx9DssGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rx9Ds8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rx9DtMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rx9DtcGgEeirvMQPb5aNLA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ryFmkMGgEeirvMQPb5aNLA" name="NUMERODOCUMENT" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_ryGNoMGgEeirvMQPb5aNLA" value="NUMERODOCUMENT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ryGNocGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ryGNosGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ryGNo8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ryGNpMGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ryGNpcGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ryOJcMGgEeirvMQPb5aNLA" name="INDICEDOCUMENT" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_ryOJccGgEeirvMQPb5aNLA" value="INDICEDOCUMENT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ryOJcsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ryOJc8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ryOJdMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ryOJdcGgEeirvMQPb5aNLA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ryU3IMGgEeirvMQPb5aNLA" name="NUMEROCARACTERISTIQUE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_ryU3IcGgEeirvMQPb5aNLA" value="NUMEROCARACTERISTIQUE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ryVeMMGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ryVeMcGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ryVeMsGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ryVeM8GgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ryVeNMGgEeirvMQPb5aNLA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rycy8MGgEeirvMQPb5aNLA" name="NUMEROACTION" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rycy8cGgEeirvMQPb5aNLA" value="NUMEROACTION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rycy8sGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rycy88GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rycy9MGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rycy9cGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rycy9sGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ryi5kMGgEeirvMQPb5aNLA" name="UTILISATEURMODIF" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_ryi5kcGgEeirvMQPb5aNLA" value="UTILISATEURMODIF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ryi5ksGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ryi5k8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ryi5lMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ryi5lcGgEeirvMQPb5aNLA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rypAMMGgEeirvMQPb5aNLA" name="DATECREATION" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rypAMcGgEeirvMQPb5aNLA" value="DATECREATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rypAMsGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rypAM8GgEeirvMQPb5aNLA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rypANMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rypANcGgEeirvMQPb5aNLA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rypANsGgEeirvMQPb5aNLA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ryufwMGgEeirvMQPb5aNLA" name="DATEMODIF" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_ryufwcGgEeirvMQPb5aNLA" value="DATEMODIF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ryufwsGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ryufw8GgEeirvMQPb5aNLA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ryufxMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ryufxcGgEeirvMQPb5aNLA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ryufxsGgEeirvMQPb5aNLA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ry0mYMGgEeirvMQPb5aNLA" name="PRESENCENOTE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_ry0mYcGgEeirvMQPb5aNLA" value="PRESENCENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ry0mYsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ry0mY8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ry0mZMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ry0mZcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ry0mZsGgEeirvMQPb5aNLA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ry8iMMGgEeirvMQPb5aNLA" name="NUMERONOTE" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_ry8iMcGgEeirvMQPb5aNLA" value="NUMERONOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ry8iMsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ry8iM8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ry8iNMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ry8iNcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ry8iNsGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rzEeAMGgEeirvMQPb5aNLA" name="CODEINTERVENTION" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rzEeAcGgEeirvMQPb5aNLA" value="CODEINTERVENTION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rzEeAsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rzEeA8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rzEeBMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rzEeBcGgEeirvMQPb5aNLA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rzNn8MGgEeirvMQPb5aNLA" name="STATUSACTIONCONTROLE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rzNn8cGgEeirvMQPb5aNLA" value="STATUSACTIONCONTROLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rzNn8sGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rzNn88GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rzNn9MGgEeirvMQPb5aNLA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rzNn9cGgEeirvMQPb5aNLA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rzSgcMGgEeirvMQPb5aNLA" name="REFERENCE" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rzSgccGgEeirvMQPb5aNLA" value="REFERENCE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rzSgcsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rzSgc8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rzSgdMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rzSgdcGgEeirvMQPb5aNLA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rzXY8MGgEeirvMQPb5aNLA" name="SOUSREFERENCE1" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_rzXY8cGgEeirvMQPb5aNLA" value="SOUSREFERENCE1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rzXY8sGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rzXY88GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rzXY9MGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rzXY9cGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rzcRcMGgEeirvMQPb5aNLA" name="SOUSREFERENCE2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_rzcRccGgEeirvMQPb5aNLA" value="SOUSREFERENCE2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rzcRcsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rzcRc8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rzcRdMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rzcRdcGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rzkNQMGgEeirvMQPb5aNLA" name="DATEDEBUT" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_rzkNQcGgEeirvMQPb5aNLA" value="DATEDEBUT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rzkNQsGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rzkNQ8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rzkNRMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rzkNRcGgEeirvMQPb5aNLA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rzkNRsGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rzsJEMGgEeirvMQPb5aNLA" name="DATEFIN" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_rzsJEcGgEeirvMQPb5aNLA" value="DATEFIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rzsJEsGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rzsJE8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rzsJFMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rzsJFcGgEeirvMQPb5aNLA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rzsJFsGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rzy2wMGgEeirvMQPb5aNLA" name="TYPESURVEILLANCE" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_rzy2wcGgEeirvMQPb5aNLA" value="TYPESURVEILLANCE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rzy2wsGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rzy2w8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rzy2xMGgEeirvMQPb5aNLA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rzy2xcGgEeirvMQPb5aNLA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rz6LgMGgEeirvMQPb5aNLA" name="TYPERESULTAT" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_rz6LgcGgEeirvMQPb5aNLA" value="TYPERESULTAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rz6LgsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rz6Lg8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rz6LhMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rz6LhcGgEeirvMQPb5aNLA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r0BgQMGgEeirvMQPb5aNLA" name="TYPESAISIERESULTAT" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_r0BgQcGgEeirvMQPb5aNLA" value="TYPESAISIERESULTAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r0BgQsGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r0BgQ8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r0BgRMGgEeirvMQPb5aNLA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r0BgRcGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r0JcEMGgEeirvMQPb5aNLA" name="TAILLEECHANTILLONAGE" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_r0JcEcGgEeirvMQPb5aNLA" value="TAILLEECHANTILLONAGE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r0JcEsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r0JcE8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r0JcFMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r0JcFcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r0JcFsGgEeirvMQPb5aNLA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r0SmAMGgEeirvMQPb5aNLA" name="NOMBREVALEURS" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_r0SmAcGgEeirvMQPb5aNLA" value="NOMBREVALEURS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r0SmAsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r0SmA8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r0SmBMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r0SmBcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r0SmBsGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r0ah0MGgEeirvMQPb5aNLA" name="NOMBREVALEURSNEUTRES" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_r0ah0cGgEeirvMQPb5aNLA" value="NOMBREVALEURSNEUTRES"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r0ah0sGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r0ah08GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r0ah1MGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r0ah1cGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r0ah1sGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r0jEsMGgEeirvMQPb5aNLA" name="VALEURMINI" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_r0jEscGgEeirvMQPb5aNLA" value="VALEURMINI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r0jEssGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r0jEs8GgEeirvMQPb5aNLA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r0jEtMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r0jEtcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r0jEtsGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r0qZcMGgEeirvMQPb5aNLA" name="VALEURMAXI" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_r0qZccGgEeirvMQPb5aNLA" value="VALEURMAXI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r0qZcsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r0qZc8GgEeirvMQPb5aNLA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r0qZdMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r0rAgMGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r0rAgcGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r0zjYMGgEeirvMQPb5aNLA" name="NOMBREDEFECTUEUX" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_r0zjYcGgEeirvMQPb5aNLA" value="NOMBREDEFECTUEUX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r0zjYsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r0zjY8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r0zjZMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r0zjZcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r0zjZsGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r064IMGgEeirvMQPb5aNLA" name="INDICATEURCP" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_r064IcGgEeirvMQPb5aNLA" value="INDICATEURCP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r064IsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r064I8GgEeirvMQPb5aNLA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r064JMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r064JcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r064JsGgEeirvMQPb5aNLA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r1ECEMGgEeirvMQPb5aNLA" name="INDICATEURCPK" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_r1ECEcGgEeirvMQPb5aNLA" value="INDICATEURCPK"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r1ECEsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r1ECE8GgEeirvMQPb5aNLA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r1ECFMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r1ECFcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r1ECFsGgEeirvMQPb5aNLA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r1OaIMGgEeirvMQPb5aNLA" name="INDICATEURPP" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_r1OaIcGgEeirvMQPb5aNLA" value="INDICATEURPP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r1PBMMGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r1PBMcGgEeirvMQPb5aNLA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r1PBMsGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r1PBM8GgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r1PBNMGgEeirvMQPb5aNLA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r1YyMMGgEeirvMQPb5aNLA" name="INDICATEURPPK" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_r1YyMcGgEeirvMQPb5aNLA" value="INDICATEURPPK"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r1YyMsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r1YyM8GgEeirvMQPb5aNLA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r1YyNMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r1YyNcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r1YyNsGgEeirvMQPb5aNLA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r1ff4MGgEeirvMQPb5aNLA" name="INDICATEURPOSITIONCPK" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_r1ff4cGgEeirvMQPb5aNLA" value="INDICATEURPOSITIONCPK"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r1ff4sGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r1ff48GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r1ff5MGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r1ff5cGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r1ff5sGgEeirvMQPb5aNLA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r1mNkMGgEeirvMQPb5aNLA" name="MOYENNEVALEURS" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_r1mNkcGgEeirvMQPb5aNLA" value="MOYENNEVALEURS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r1mNksGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r1mNk8GgEeirvMQPb5aNLA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r1mNlMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r1mNlcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r1mNlsGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r1rtIMGgEeirvMQPb5aNLA" name="ECARTTYPE" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_r1rtIcGgEeirvMQPb5aNLA" value="ECARTTYPE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r1rtIsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r1rtI8GgEeirvMQPb5aNLA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r1rtJMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r1rtJcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r1rtJsGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r1zo8MGgEeirvMQPb5aNLA" name="PROPORTIONDEFECTUEUXESTIMEE_INF" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_r1zo8cGgEeirvMQPb5aNLA" value="PROPORTIONDEFECTUEUXESTIMEE_INF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r1zo8sGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r1zo88GgEeirvMQPb5aNLA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r1zo9MGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r1zo9cGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r1zo9sGgEeirvMQPb5aNLA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r15vkMGgEeirvMQPb5aNLA" name="PROPORTIONDEFECTUEUXESTIMEE_SUP" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_r15vkcGgEeirvMQPb5aNLA" value="PROPORTIONDEFECTUEUXESTIMEE_SUP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r15vksGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r15vk8GgEeirvMQPb5aNLA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r15vlMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r15vlcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r15vlsGgEeirvMQPb5aNLA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r2BrYMGgEeirvMQPb5aNLA" name="NUMEROLOI" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_r2BrYcGgEeirvMQPb5aNLA" value="NUMEROLOI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r2BrYsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r2BrY8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r2BrZMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r2BrZcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r2BrZsGgEeirvMQPb5aNLA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r2KOQMGgEeirvMQPb5aNLA" name="INDICATEURTESTLOI" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_r2KOQcGgEeirvMQPb5aNLA" value="INDICATEURTESTLOI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r2KOQsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r2KOQ8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r2KORMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r2KORcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r2KORsGgEeirvMQPb5aNLA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r2RjAMGgEeirvMQPb5aNLA" name="DISPERSION" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_r2RjAcGgEeirvMQPb5aNLA" value="DISPERSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r2RjAsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r2RjA8GgEeirvMQPb5aNLA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r2RjBMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r2RjBcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r2RjBsGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r2aF4MGgEeirvMQPb5aNLA" name="NUMEROOF" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_r2aF4cGgEeirvMQPb5aNLA" value="NUMEROOF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r2aF4sGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r2aF48GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r2aF5MGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r2aF5cGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r2aF5sGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r2haoMGgEeirvMQPb5aNLA" name="SEQUENCEGAMME" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_r2haocGgEeirvMQPb5aNLA" value="SEQUENCEGAMME"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r2haosGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r2hao8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r2hapMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r2hapcGgEeirvMQPb5aNLA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r2rLoMGgEeirvMQPb5aNLA" name="POSTEDETRAVAIL" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_r2rLocGgEeirvMQPb5aNLA" value="POSTEDETRAVAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r2rLosGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r2rLo8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r2rLpMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r2rLpcGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r2zHcMGgEeirvMQPb5aNLA" name="SERIELOT" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_r2zHccGgEeirvMQPb5aNLA" value="SERIELOT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r2zHcsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r2zHc8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r2zHdMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r2zHdcGgEeirvMQPb5aNLA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r26cMMGgEeirvMQPb5aNLA" name="INDICATEURPRECARTE" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_r26cMcGgEeirvMQPb5aNLA" value="INDICATEURPRECARTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r26cMsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r26cM8GgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r26cNMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r26cNcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r26cNsGgEeirvMQPb5aNLA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r3Bw8MGgEeirvMQPb5aNLA" name="TYPEOBJECTIF" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_r3Bw8cGgEeirvMQPb5aNLA" value="TYPEOBJECTIF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r3Bw8sGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r3Bw88GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r3Bw9MGgEeirvMQPb5aNLA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r3Bw9cGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r3Lh8MGgEeirvMQPb5aNLA" name="LIMITECONTROLEMOYENNE_INF" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_r3Lh8cGgEeirvMQPb5aNLA" value="LIMITECONTROLEMOYENNE_INF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r3Lh8sGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r3Lh88GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r3Lh9MGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r3Lh9cGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r3SPoMGgEeirvMQPb5aNLA" name="LIMITECONTROLEMOYENNE_SUP" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_r3SPocGgEeirvMQPb5aNLA" value="LIMITECONTROLEMOYENNE_SUP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r3SPosGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r3SPo8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r3SPpMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r3SPpcGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r3aLcMGgEeirvMQPb5aNLA" name="VALEURCENTRALESPC" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_r3aLccGgEeirvMQPb5aNLA" value="VALEURCENTRALESPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r3aLcsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r3aLc8GgEeirvMQPb5aNLA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r3aLdMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r3aLdcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r3aLdsGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r3iHQMGgEeirvMQPb5aNLA" name="MOYENNEECARTTYPESPC" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_r3iHQcGgEeirvMQPb5aNLA" value="MOYENNEECARTTYPESPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r3iHQsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_r3iHQ8GgEeirvMQPb5aNLA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r3iHRMGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r3iHRcGgEeirvMQPb5aNLA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r3iHRsGgEeirvMQPb5aNLA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r3pcAMGgEeirvMQPb5aNLA" name="CODELOCALISATION" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_r3pcAcGgEeirvMQPb5aNLA" value="CODELOCALISATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r3pcAsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r3pcA8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r3pcBMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r3pcBcGgEeirvMQPb5aNLA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r3xX0MGgEeirvMQPb5aNLA" name="CODEOPERATIONELEMENTAIRE" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_r3xX0cGgEeirvMQPb5aNLA" value="CODEOPERATIONELEMENTAIRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r3xX0sGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r3xX08GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r3xX1MGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r3xX1cGgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r35ToMGgEeirvMQPb5aNLA" name="VALEURCHAMPMETIER_1" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_r35TocGgEeirvMQPb5aNLA" value="VALEURCHAMPMETIER_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r35TosGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r35To8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r35TpMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r35TpcGgEeirvMQPb5aNLA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r4AoYMGgEeirvMQPb5aNLA" name="VALEURCHAMPMETIER_2" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_r4AoYcGgEeirvMQPb5aNLA" value="VALEURCHAMPMETIER_2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r4AoYsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r4AoY8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r4AoZMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r4AoZcGgEeirvMQPb5aNLA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r4H9IMGgEeirvMQPb5aNLA" name="VALEURCHAMPMETIER_3" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_r4H9IcGgEeirvMQPb5aNLA" value="VALEURCHAMPMETIER_3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r4H9IsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r4H9I8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r4H9JMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r4H9JcGgEeirvMQPb5aNLA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r4SVMMGgEeirvMQPb5aNLA" name="VALEURCHAMPMETIER_4" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_r4SVMcGgEeirvMQPb5aNLA" value="VALEURCHAMPMETIER_4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r4SVMsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r4SVM8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r4SVNMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r4SVNcGgEeirvMQPb5aNLA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r4eicMGgEeirvMQPb5aNLA" name="VALEURCHAMPMETIER_5" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_r4eiccGgEeirvMQPb5aNLA" value="VALEURCHAMPMETIER_5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r4eicsGgEeirvMQPb5aNLA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r4eic8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r4eidMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r4eidcGgEeirvMQPb5aNLA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r4nFUMGgEeirvMQPb5aNLA" name="TYPEDECISION" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_r4nFUcGgEeirvMQPb5aNLA" value="TYPEDECISION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r4nFUsGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r4nFU8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r4nFVMGgEeirvMQPb5aNLA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r4nFVcGgEeirvMQPb5aNLA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r4voMMGgEeirvMQPb5aNLA" name="DESIGNATIONARTICLE" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_r4voMcGgEeirvMQPb5aNLA" value="DESIGNATIONARTICLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r4voMsGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r4voM8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r4voNMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r4voNcGgEeirvMQPb5aNLA" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r43kAMGgEeirvMQPb5aNLA" name="LIBELLECODEINTERVENTIONCONTROLE" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_r43kAcGgEeirvMQPb5aNLA" value="LIBELLECODEINTERVENTIONCONTROLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r43kAsGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r43kA8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r43kBMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r43kBcGgEeirvMQPb5aNLA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r4_f0MGgEeirvMQPb5aNLA" name="SPECIFICATION" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_r4_f0cGgEeirvMQPb5aNLA" value="SPECIFICATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r4_f0sGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r4_f08GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r4_f1MGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r4_f1cGgEeirvMQPb5aNLA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r5HboMGgEeirvMQPb5aNLA" name="SOUSSPECIFICATION1" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_r5HbocGgEeirvMQPb5aNLA" value="SOUSSPECIFICATION1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r5ICsMGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r5ICscGgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r5ICssGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r5ICs8GgEeirvMQPb5aNLA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r5RMoMGgEeirvMQPb5aNLA" name="SOUSSPECIFICATION2" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_r5RMocGgEeirvMQPb5aNLA" value="SOUSSPECIFICATION2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r5RMosGgEeirvMQPb5aNLA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r5RMo8GgEeirvMQPb5aNLA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r5RMpMGgEeirvMQPb5aNLA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_r5RMpcGgEeirvMQPb5aNLA" value="8"/>
      </node>
    </node>
  </node>
</md:node>