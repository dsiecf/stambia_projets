<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.file.server" id="_bFK8cBFmEeuICcDqEKrXUw" md:ref="platform:/plugin/com.indy.environment/technology/file/standard.file.md#UUID_TECH_FILE_MD?fileId=UUID_TECH_FILE_MD$type=md$name=File?">
  <node defType="com.stambia.file.directory" id="_bXHBABFmEeuICcDqEKrXUw" name="Export_devis">
    <attribute defType="com.stambia.file.directory.path" id="_bYbPoBFmEeuICcDqEKrXUw" value="C:\app_dev\Used_Files\In_Files\Reference_Files"/>
    <node defType="com.stambia.file.file" id="_bYe6ABFmEeuICcDqEKrXUw" name="export_devis">
      <attribute defType="com.stambia.file.file.type" id="_bZ52UBFmEeuICcDqEKrXUw" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_bZ8SkBFmEeuICcDqEKrXUw" value="Cp1250"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_bZ85oBFmEeuICcDqEKrXUw" value="0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_bZ9gsBFmEeuICcDqEKrXUw" value="2C"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_bZ9gsRFmEeuICcDqEKrXUw"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_bZ_V4BFmEeuICcDqEKrXUw" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_bZ_88BFmEeuICcDqEKrXUw" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_baAkABFmEeuICcDqEKrXUw" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_baAkARFmEeuICcDqEKrXUw" value="0"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_iNBv8BFmEeuICcDqEKrXUw" value="export_devis.csv"/>
      <node defType="com.stambia.file.field" id="_PGMvLBINEeuBboI6hjD-Zg" name="id_chomette_quote" position="4">
        <attribute defType="com.stambia.file.field.size" id="_PGMvLRINEeuBboI6hjD-Zg" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_PGMvLhINEeuBboI6hjD-Zg" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_PGMvLxINEeuBboI6hjD-Zg" value="F4"/>
      </node>
      <node defType="com.stambia.file.field" id="_PGMvJBINEeuBboI6hjD-Zg" name="id_societe_chomette" position="2">
        <attribute defType="com.stambia.file.field.size" id="_PGMvJRINEeuBboI6hjD-Zg" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_PGMvJhINEeuBboI6hjD-Zg" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_PGMvJxINEeuBboI6hjD-Zg" value="F2"/>
      </node>
      <node defType="com.stambia.file.field" id="_PGMvKBINEeuBboI6hjD-Zg" name="id_magento_quote" position="3">
        <attribute defType="com.stambia.file.field.size" id="_PGMvKRINEeuBboI6hjD-Zg" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_PGMvKhINEeuBboI6hjD-Zg" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_PGMvKxINEeuBboI6hjD-Zg" value="F3"/>
      </node>
      <node defType="com.stambia.file.field" id="_PGMvIBINEeuBboI6hjD-Zg" name="id_societe_magento" position="1">
        <attribute defType="com.stambia.file.field.size" id="_PGMvIRINEeuBboI6hjD-Zg" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_PGMvIhINEeuBboI6hjD-Zg" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_PGMvIxINEeuBboI6hjD-Zg" value="F1"/>
      </node>
    </node>
  </node>
</md:node>