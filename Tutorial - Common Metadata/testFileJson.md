<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.file.server" id="_TnduoP72EeiVrKKMla1zGg" md:ref="platform:/plugin/com.indy.environment/.tech/file/standard.file.md#UUID_TECH_FILE_MD?fileId=UUID_TECH_FILE_MD$type=md$name=File?">
  <node defType="com.stambia.file.directory" id="_TudugP72EeiVrKKMla1zGg" name="testJson">
    <attribute defType="com.stambia.file.directory.path" id="_TvWfUP72EeiVrKKMla1zGg" value="C:\app_dev\Used_Files\In_Files\Json"/>
    <node defType="com.stambia.file.file" id="_5LuaMP72EeiVrKKMla1zGg" name="Societe">
      <attribute defType="com.stambia.file.file.type" id="_5M6tAP72EeiVrKKMla1zGg" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_5M6tAf72EeiVrKKMla1zGg"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_5M6tAv72EeiVrKKMla1zGg" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_5M6tA_72EeiVrKKMla1zGg" value="3B"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_5M6tBP72EeiVrKKMla1zGg"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_5M6tBf72EeiVrKKMla1zGg" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_5M6tBv72EeiVrKKMla1zGg" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_5NEeAP72EeiVrKKMla1zGg" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_5NEeAf72EeiVrKKMla1zGg" value="1"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_53rhQP72EeiVrKKMla1zGg" value="Societe.json"/>
      <node defType="com.stambia.file.field" id="_-kKegP72EeiVrKKMla1zGg" name="id_societe_magento" position="1">
        <attribute defType="com.stambia.file.field.size" id="_-kKegf72EeiVrKKMla1zGg" value="15"/>
        <attribute defType="com.stambia.file.field.type" id="_-kKegv72EeiVrKKMla1zGg" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_-kKeg_72EeiVrKKMla1zGg" value="A_"/>
      </node>
      <node defType="com.stambia.file.field" id="_BRIbEP73EeiVrKKMla1zGg" name="id_client_chomette" position="2">
        <attribute defType="com.stambia.file.field.physicalName" id="_BRRlAP73EeiVrKKMla1zGg" value="C2"/>
        <attribute defType="com.stambia.file.field.type" id="_BRRlAf73EeiVrKKMla1zGg" value="String"/>
        <attribute defType="com.stambia.file.field.size" id="_BRRlAv73EeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.file.propertyField" id="_Kg9ApP73EeiVrKKMla1zGg" name="string_content"/>
    </node>
  </node>
</md:node>