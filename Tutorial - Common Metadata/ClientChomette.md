<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_1S1WgNLXEeiL7cTvwX--2w" name="ClientChomette" md:ref="../Stambia_chomette_com/ibm_db2_400.rdbms.md#UUID_MD_RDBMS_DB2_400?fileId=UUID_MD_RDBMS_DB2_400$type=md$name=IBM%20DB2/400?">
  <attribute defType="com.stambia.rdbms.server.url" id="_TqTOwNLZEeiL7cTvwX--2w" value="jdbc:as400://chomette"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_TqVD8NLZEeiL7cTvwX--2w" value="com.ibm.as400.access.AS400JDBCDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_TqVD8dLZEeiL7cTvwX--2w" value="RHANEM"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_TqVrANLZEeiL7cTvwX--2w" value="7847363B1AB82F7E59AE55C31E6FA3F5"/>
  <node defType="com.stambia.rdbms.schema" id="_1anZgNLXEeiL7cTvwX--2w" name="ORIONBD">
    <attribute defType="com.stambia.rdbms.schema.name" id="_1ayYoNLXEeiL7cTvwX--2w" value="ORIONBD"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_1ayYodLXEeiL7cTvwX--2w" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_1ay_sNLXEeiL7cTvwX--2w" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_1ay_sdLXEeiL7cTvwX--2w" value="I_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.work" id="_t8pXcP7zEemVAft-X6rPVg" ref="#_pDWtQP7zEemVAft-X6rPVg?fileId=_1S1WgNLXEeiL7cTvwX--2w$type=md$name=STAMBIA?"/>
    <node defType="com.stambia.rdbms.datastore" id="_Tgsm0NLZEeiL7cTvwX--2w" name="OTE4CPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Tgsm0dLZEeiL7cTvwX--2w" value="OTE4CPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Tgsm0tLZEeiL7cTvwX--2w" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_TgzUgNLZEeiL7cTvwX--2w" value="OTE4CPP   "/>
      <node defType="com.stambia.rdbms.column" id="_Tg73YNLZEeiL7cTvwX--2w" name="E4AMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tg73YdLZEeiL7cTvwX--2w" value="E4AMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tg73YtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tg73Y9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tg73ZNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tg73ZdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tg8ecNLZEeiL7cTvwX--2w" name="E4AZCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tg8ecdLZEeiL7cTvwX--2w" value="E4AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tg8ectLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tg8ec9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tg8edNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tg8eddLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tg8edtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tg9FgNLZEeiL7cTvwX--2w" name="E4T9C1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tg9FgdLZEeiL7cTvwX--2w" value="E4T9C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tg9FgtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tg9Fg9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tg9FhNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tg9FhdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tg9FhtLZEeiL7cTvwX--2w" name="E4BCCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tg9Fh9LZEeiL7cTvwX--2w" value="E4BCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tg9FiNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tg9FidLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tg9FitLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tg9Fi9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tg9FjNLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tg9FjdLZEeiL7cTvwX--2w" name="E4PXN6" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tg9FjtLZEeiL7cTvwX--2w" value="E4PXN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tg9Fj9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tg9FkNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tg9FkdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tg9FktLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tg9Fk9LZEeiL7cTvwX--2w" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_ThDMIdLZEeiL7cTvwX--2w" name="ORN8CPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_ThDMItLZEeiL7cTvwX--2w" value="ORN8CPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_ThDMI9LZEeiL7cTvwX--2w" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_ThJ50NLZEeiL7cTvwX--2w" value="ORN8CPP   "/>
      <node defType="com.stambia.rdbms.column" id="_ThWHENLZEeiL7cTvwX--2w" name="N8JYN5" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThWHEdLZEeiL7cTvwX--2w" value="N8JYN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThWHEtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThWHE9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThWHFNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThWHFdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThWHFtLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThWHF9LZEeiL7cTvwX--2w" name="N8AZCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThWHGNLZEeiL7cTvwX--2w" value="N8AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThWHGdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThWHGtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThWHG9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThWHHNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThWHHdLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThWuINLZEeiL7cTvwX--2w" name="N8NGN1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThWuIdLZEeiL7cTvwX--2w" value="N8NGN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThWuItLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThWuI9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThWuJNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThWuJdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThWuJtLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThWuJ9LZEeiL7cTvwX--2w" name="N8SBPR" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThWuKNLZEeiL7cTvwX--2w" value="N8SBPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThWuKdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThWuKtLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThWuK9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThWuLNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThWuLdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThXVMNLZEeiL7cTvwX--2w" name="N8SCPR" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThXVMdLZEeiL7cTvwX--2w" value="N8SCPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThXVMtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThXVM9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThXVNNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThXVNdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThXVNtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThXVN9LZEeiL7cTvwX--2w" name="N8K5P2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThXVONLZEeiL7cTvwX--2w" value="N8K5P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThXVOdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThXVOtLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThXVO9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThXVPNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThXVPdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThXVPtLZEeiL7cTvwX--2w" name="N8K6P2" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThXVP9LZEeiL7cTvwX--2w" value="N8K6P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThXVQNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThXVQdLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThXVQtLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThXVQ9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThXVRNLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThX8QNLZEeiL7cTvwX--2w" name="N8J5P2" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThX8QdLZEeiL7cTvwX--2w" value="N8J5P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThX8QtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThX8Q9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThX8RNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThX8RdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThX8RtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThX8R9LZEeiL7cTvwX--2w" name="N8J6P2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThX8SNLZEeiL7cTvwX--2w" value="N8J6P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThX8SdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThX8StLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThX8S9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThX8TNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThX8TdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThYjUNLZEeiL7cTvwX--2w" name="N8NHN1" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThYjUdLZEeiL7cTvwX--2w" value="N8NHN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThYjUtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThYjU9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThYjVNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThYjVdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThYjVtLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThYjV9LZEeiL7cTvwX--2w" name="N8NIN1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThYjWNLZEeiL7cTvwX--2w" value="N8NIN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThYjWdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThYjWtLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThYjW9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThYjXNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThYjXdLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThZKYNLZEeiL7cTvwX--2w" name="N8SDPR" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThZKYdLZEeiL7cTvwX--2w" value="N8SDPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThZKYtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThZKY9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThZKZNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThZKZdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThZKZtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThZKZ9LZEeiL7cTvwX--2w" name="N8SEPR" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThZKaNLZEeiL7cTvwX--2w" value="N8SEPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThZKadLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThZKatLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThZKa9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThZKbNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThZKbdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThZxcNLZEeiL7cTvwX--2w" name="N8K3P2" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThZxcdLZEeiL7cTvwX--2w" value="N8K3P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThZxctLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThZxc9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThZxdNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThZxddLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThZxdtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThZxd9LZEeiL7cTvwX--2w" name="N8K4P2" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThZxeNLZEeiL7cTvwX--2w" value="N8K4P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThZxedLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThZxetLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThZxe9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThZxfNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThZxfdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThaYgNLZEeiL7cTvwX--2w" name="N8J3P2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThaYgdLZEeiL7cTvwX--2w" value="N8J3P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThaYgtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThaYg9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThaYhNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThaYhdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThaYhtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThaYh9LZEeiL7cTvwX--2w" name="N8J4P2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThaYiNLZEeiL7cTvwX--2w" value="N8J4P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThaYidLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThaYitLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThaYi9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThaYjNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThaYjdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThaYjtLZEeiL7cTvwX--2w" name="N8SFPR" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThaYj9LZEeiL7cTvwX--2w" value="N8SFPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThaYkNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ThaYkdLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThaYktLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThaYk9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThaYlNLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tha_kNLZEeiL7cTvwX--2w" name="N8SGPR" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tha_kdLZEeiL7cTvwX--2w" value="N8SGPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tha_ktLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tha_k9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tha_lNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tha_ldLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tha_ltLZEeiL7cTvwX--2w" value="13"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_TiunINLZEeiL7cTvwX--2w" name="ORA1REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_TiunIdLZEeiL7cTvwX--2w" value="ORA1REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_TiunItLZEeiL7cTvwX--2w" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_TiwcUNLZEeiL7cTvwX--2w" value="ORA1REP   "/>
      <node defType="com.stambia.rdbms.column" id="_Ti8CgNLZEeiL7cTvwX--2w" name="A1A2CD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti8CgdLZEeiL7cTvwX--2w" value="A1A2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti8CgtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ti8Cg9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti8ChNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti8ChdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti8ChtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ti8Ch9LZEeiL7cTvwX--2w" name="A1LVNB" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti8CiNLZEeiL7cTvwX--2w" value="A1LVNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti8CidLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ti8CitLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti8Ci9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti8CjNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti8CjdLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ti8pkNLZEeiL7cTvwX--2w" name="A1C8CD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti8pkdLZEeiL7cTvwX--2w" value="A1C8CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti8pktLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti8pk9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti8plNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti8pldLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ti8pltLZEeiL7cTvwX--2w" name="A1FRCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti8pl9LZEeiL7cTvwX--2w" value="A1FRCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti8pmNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti8pmdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti8pmtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti8pm9LZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ti9QoNLZEeiL7cTvwX--2w" name="A1GECD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti9QodLZEeiL7cTvwX--2w" value="A1GECD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti9QotLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti9Qo9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti9QpNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti9QpdLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ti9QptLZEeiL7cTvwX--2w" name="A1GFCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti9Qp9LZEeiL7cTvwX--2w" value="A1GFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti9QqNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti9QqdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti9QqtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti9Qq9LZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ti93sNLZEeiL7cTvwX--2w" name="A1BNNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti93sdLZEeiL7cTvwX--2w" value="A1BNNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti93stLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti93s9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti93tNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti93tdLZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ti93ttLZEeiL7cTvwX--2w" name="A1FWTX" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti93t9LZEeiL7cTvwX--2w" value="A1FWTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti93uNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti93udLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti93utLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti93u9LZEeiL7cTvwX--2w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ti-ewNLZEeiL7cTvwX--2w" name="A1FXTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti-ewdLZEeiL7cTvwX--2w" value="A1FXTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti-ewtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti-ew9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti-exNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti-exdLZEeiL7cTvwX--2w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ti-extLZEeiL7cTvwX--2w" name="A1FYTX" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ti-ex9LZEeiL7cTvwX--2w" value="A1FYTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ti-eyNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ti-eydLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ti-eytLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ti-ey9LZEeiL7cTvwX--2w" value="25"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_THBikNLZEeiL7cTvwX--2w" name="ORQFCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_THBikdLZEeiL7cTvwX--2w" value="ORQFCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_THCJoNLZEeiL7cTvwX--2w" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_THs4ANLZEeiL7cTvwX--2w" value="ORQFCPP   "/>
      <node defType="com.stambia.rdbms.column" id="_TX2z4NLZEeiL7cTvwX--2w" name="QFNFN1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX2z4dLZEeiL7cTvwX--2w" value="QFNFN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX2z4tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX2z49LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX2z5NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX2z5dLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX2z5tLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX3a8NLZEeiL7cTvwX--2w" name="QFNGN1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX4CANLZEeiL7cTvwX--2w" value="QFNGN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX4CAdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX4CAtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX4CA9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX4CBNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX4CBdLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX4pENLZEeiL7cTvwX--2w" name="QFAMCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX4pEdLZEeiL7cTvwX--2w" value="QFAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX4pEtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX4pE9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX4pFNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX4pFdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX5QINLZEeiL7cTvwX--2w" name="QFGHCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX5QIdLZEeiL7cTvwX--2w" value="QFGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX5QItLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX5QI9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX5QJNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX5QJdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX53MNLZEeiL7cTvwX--2w" name="QFAQCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX53MdLZEeiL7cTvwX--2w" value="QFAQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX53MtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX53M9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX53NNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX53NdLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX6eQNLZEeiL7cTvwX--2w" name="QFBHCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX6eQdLZEeiL7cTvwX--2w" value="QFBHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX6eQtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX6eQ9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX6eRNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX6eRdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX6eRtLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX6eR9LZEeiL7cTvwX--2w" name="QFBCCD" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX6eSNLZEeiL7cTvwX--2w" value="QFBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX6eSdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX6eStLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX6eS9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX6eTNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX6eTdLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX7FUNLZEeiL7cTvwX--2w" name="QFAFCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX7FUdLZEeiL7cTvwX--2w" value="QFAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX7FUtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX7FU9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX7FVNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX7FVdLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX8TcNLZEeiL7cTvwX--2w" name="QFIIN2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX8TcdLZEeiL7cTvwX--2w" value="QFIIN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX8TctLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX8Tc9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX8TdNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX8TddLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX8TdtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX8Td9LZEeiL7cTvwX--2w" name="QFH9N2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX8TeNLZEeiL7cTvwX--2w" value="QFH9N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX8TedLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX8TetLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX8Te9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX86gNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX86gdLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX86gtLZEeiL7cTvwX--2w" name="QFIHN2" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX9hkNLZEeiL7cTvwX--2w" value="QFIHN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX9hkdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX9hktLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX9hk9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX9hlNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX9hldLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX9hltLZEeiL7cTvwX--2w" name="QFBMP1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX-IoNLZEeiL7cTvwX--2w" value="QFBMP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX-IodLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX-IotLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX-Io9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX-IpNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX-IpdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX-IptLZEeiL7cTvwX--2w" name="QFBTP1" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX-Ip9LZEeiL7cTvwX--2w" value="QFBTP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX-IqNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX-IqdLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX-IqtLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX-Iq9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX-IrNLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX-vsNLZEeiL7cTvwX--2w" name="QFIAN2" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX-vsdLZEeiL7cTvwX--2w" value="QFIAN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX-vstLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX-vs9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX-vtNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX-vtdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX-vttLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX_WwNLZEeiL7cTvwX--2w" name="QFBFP1" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX_WwdLZEeiL7cTvwX--2w" value="QFBFP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX_WwtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX_Ww9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX_WxNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX_WxdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX_WxtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX_90NLZEeiL7cTvwX--2w" name="QFBNP1" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX_90dLZEeiL7cTvwX--2w" value="QFBNP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX_90tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX_909LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX_91NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX_91dLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX_91tLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TX_919LZEeiL7cTvwX--2w" name="QFBUP1" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_TX_92NLZEeiL7cTvwX--2w" value="QFBUP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TX_92dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TX_92tLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TX_929LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TX_93NLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TX_93dLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYAk4NLZEeiL7cTvwX--2w" name="QFIBN2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYAk4dLZEeiL7cTvwX--2w" value="QFIBN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYAk4tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYAk49LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYAk5NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYAk5dLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYAk5tLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYBL8NLZEeiL7cTvwX--2w" name="QFBGP1" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYBL8dLZEeiL7cTvwX--2w" value="QFBGP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYBL8tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYBL89LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYBL9NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYBL9dLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYBL9tLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYBzANLZEeiL7cTvwX--2w" name="QFBOP1" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYBzAdLZEeiL7cTvwX--2w" value="QFBOP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYBzAtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYBzA9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYBzBNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYBzBdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYBzBtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYCaENLZEeiL7cTvwX--2w" name="QFBVP1" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYCaEdLZEeiL7cTvwX--2w" value="QFBVP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYCaEtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYCaE9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYCaFNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYCaFdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYCaFtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYDBINLZEeiL7cTvwX--2w" name="QFICN2" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYDBIdLZEeiL7cTvwX--2w" value="QFICN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYDBItLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYDBI9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYDBJNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYDBJdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYDBJtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYDBJ9LZEeiL7cTvwX--2w" name="QFBHP1" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYDBKNLZEeiL7cTvwX--2w" value="QFBHP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYDBKdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYDBKtLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYDBK9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYDBLNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYDBLdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYDoMNLZEeiL7cTvwX--2w" name="QFBPP1" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYDoMdLZEeiL7cTvwX--2w" value="QFBPP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYDoMtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYDoM9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYDoNNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYDoNdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYDoNtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYEPQNLZEeiL7cTvwX--2w" name="QFBWP1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYEPQdLZEeiL7cTvwX--2w" value="QFBWP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYEPQtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYEPQ9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYEPRNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYEPRdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYEPRtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYE2UNLZEeiL7cTvwX--2w" name="QFIDN2" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYE2UdLZEeiL7cTvwX--2w" value="QFIDN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYE2UtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYE2U9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYE2VNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYE2VdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYE2VtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYFdYNLZEeiL7cTvwX--2w" name="QFBIP1" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYFdYdLZEeiL7cTvwX--2w" value="QFBIP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYFdYtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYFdY9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYFdZNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYFdZdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYFdZtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYFdZ9LZEeiL7cTvwX--2w" name="QFBQP1" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYFdaNLZEeiL7cTvwX--2w" value="QFBQP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYFdadLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYFdatLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYFda9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYFdbNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYGEcNLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYGEcdLZEeiL7cTvwX--2w" name="QFBXP1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYGrgNLZEeiL7cTvwX--2w" value="QFBXP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYGrgdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYGrgtLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYGrg9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYGrhNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYGrhdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYGrhtLZEeiL7cTvwX--2w" name="QFIEN2" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYGrh9LZEeiL7cTvwX--2w" value="QFIEN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYGriNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYGridLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYGritLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYHSkNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYHSkdLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYHSktLZEeiL7cTvwX--2w" name="QFBJP1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYHSk9LZEeiL7cTvwX--2w" value="QFBJP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYHSlNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYHSldLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYHSltLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYHSl9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYHSmNLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYH5oNLZEeiL7cTvwX--2w" name="QFBRP1" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYH5odLZEeiL7cTvwX--2w" value="QFBRP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYH5otLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYH5o9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYH5pNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYH5pdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYH5ptLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYIgsNLZEeiL7cTvwX--2w" name="QFBYP1" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYIgsdLZEeiL7cTvwX--2w" value="QFBYP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYIgstLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYIgs9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYIgtNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYIgtdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYIgttLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYJHwNLZEeiL7cTvwX--2w" name="QFIFN2" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYJHwdLZEeiL7cTvwX--2w" value="QFIFN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYJHwtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYJHw9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYJHxNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYJHxdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYJHxtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYJHx9LZEeiL7cTvwX--2w" name="QFBKP1" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYJHyNLZEeiL7cTvwX--2w" value="QFBKP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYJHydLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYJHytLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYJHy9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYJHzNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYJu0NLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYJu0dLZEeiL7cTvwX--2w" name="QFBSP1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYJu0tLZEeiL7cTvwX--2w" value="QFBSP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYJu09LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYJu1NLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYJu1dLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYJu1tLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYJu19LZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYKV4NLZEeiL7cTvwX--2w" name="QFBZP1" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYKV4dLZEeiL7cTvwX--2w" value="QFBZP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYKV4tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYKV49LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYKV5NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYKV5dLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYKV5tLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYKV59LZEeiL7cTvwX--2w" name="QFIGN2" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYK88NLZEeiL7cTvwX--2w" value="QFIGN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYK88dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYK88tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYK889LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYK89NLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYK89dLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYK89tLZEeiL7cTvwX--2w" name="QFBLP1" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYK899LZEeiL7cTvwX--2w" value="QFBLP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYK8-NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYK8-dLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYK8-tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYK8-9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYK8_NLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYLkANLZEeiL7cTvwX--2w" name="QFBEP1" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYLkAdLZEeiL7cTvwX--2w" value="QFBEP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYLkAtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYLkA9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYLkBNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYLkBdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYLkBtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TYLkB9LZEeiL7cTvwX--2w" name="QFB0P1" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_TYMLENLZEeiL7cTvwX--2w" value="QFB0P1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TYMLEdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TYMLEtLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TYMLE9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TYMLFNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TYMLFdLZEeiL7cTvwX--2w" value="13"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Te2MsNLZEeiL7cTvwX--2w" name="ORAZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Te2MsdLZEeiL7cTvwX--2w" value="ORAZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Te2MstLZEeiL7cTvwX--2w" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_TfGEUNLZEeiL7cTvwX--2w" value="ORAZREP   "/>
      <node defType="com.stambia.rdbms.column" id="_Tf-1INLZEeiL7cTvwX--2w" name="AZAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tf-1IdLZEeiL7cTvwX--2w" value="AZAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tf-1ItLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tf-1I9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tf-1JNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tf-1JdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tf-1JtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tf_cMNLZEeiL7cTvwX--2w" name="AZAMCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tf_cMdLZEeiL7cTvwX--2w" value="AZAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tf_cMtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tf_cM9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tf_cNNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tf_cNdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgADQNLZEeiL7cTvwX--2w" name="AZC0CD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgADQdLZEeiL7cTvwX--2w" value="AZC0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgADQtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgADQ9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgADRNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgADRdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgADRtLZEeiL7cTvwX--2w" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgADR9LZEeiL7cTvwX--2w" name="AZF6PR" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgADSNLZEeiL7cTvwX--2w" value="AZF6PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgADSdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgADStLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgADS9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgADTNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgADTdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgAqUNLZEeiL7cTvwX--2w" name="AZTEST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgAqUdLZEeiL7cTvwX--2w" value="AZTEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgAqUtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgAqU9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgAqVNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgAqVdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgAqVtLZEeiL7cTvwX--2w" name="AZTKST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgAqV9LZEeiL7cTvwX--2w" value="AZTKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgAqWNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgAqWdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgAqWtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgAqW9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgBRYNLZEeiL7cTvwX--2w" name="AZTLST" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgBRYdLZEeiL7cTvwX--2w" value="AZTLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgBRYtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgBRY9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgBRZNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgBRZdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgB4cNLZEeiL7cTvwX--2w" name="AZNDCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgB4cdLZEeiL7cTvwX--2w" value="AZNDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgB4ctLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgB4c9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgB4dNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgB4ddLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgB4dtLZEeiL7cTvwX--2w" name="AZNECD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgB4d9LZEeiL7cTvwX--2w" value="AZNECD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgB4eNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgB4edLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgB4etLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgB4e9LZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgCfgNLZEeiL7cTvwX--2w" name="AZAQST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgCfgdLZEeiL7cTvwX--2w" value="AZAQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgCfgtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgCfg9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgCfhNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgCfhdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgDGkNLZEeiL7cTvwX--2w" name="AZARST" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgDGkdLZEeiL7cTvwX--2w" value="AZARST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgDGktLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgDGk9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgDGlNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgDGldLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgDGltLZEeiL7cTvwX--2w" name="AZASST" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgDGl9LZEeiL7cTvwX--2w" value="AZASST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgDGmNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgDGmdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgDGmtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgDGm9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgDtoNLZEeiL7cTvwX--2w" name="AZBHS2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgDtodLZEeiL7cTvwX--2w" value="AZBHS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgDtotLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgDto9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgDtpNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgDtpdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgEUsNLZEeiL7cTvwX--2w" name="AZAWNA" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgEUsdLZEeiL7cTvwX--2w" value="AZAWNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgEUstLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgEUs9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgEUtNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgEUtdLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgEUttLZEeiL7cTvwX--2w" name="AZAUTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgEUt9LZEeiL7cTvwX--2w" value="AZAUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgEUuNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgEUudLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgEUutLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgEUu9LZEeiL7cTvwX--2w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgE7wNLZEeiL7cTvwX--2w" name="AZA0CD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgE7wdLZEeiL7cTvwX--2w" value="AZA0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgE7wtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgE7w9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgE7xNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgE7xdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgE7xtLZEeiL7cTvwX--2w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgFi0NLZEeiL7cTvwX--2w" name="AZATTX" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgFi0dLZEeiL7cTvwX--2w" value="AZATTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgFi0tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgFi09LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgFi1NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgFi1dLZEeiL7cTvwX--2w" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgGJ4NLZEeiL7cTvwX--2w" name="AZOEST" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgGJ4dLZEeiL7cTvwX--2w" value="AZOEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgGJ4tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgGJ49LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgGJ5NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgGJ5dLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgGJ5tLZEeiL7cTvwX--2w" name="AZA8ST" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgGJ59LZEeiL7cTvwX--2w" value="AZA8ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgGJ6NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgGJ6dLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgGJ6tLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgGJ69LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgGw8NLZEeiL7cTvwX--2w" name="AZAOST" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgGw8dLZEeiL7cTvwX--2w" value="AZAOST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgGw8tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgGw89LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgGw9NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgGw9dLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgGw9tLZEeiL7cTvwX--2w" name="AZFNTX" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgGw99LZEeiL7cTvwX--2w" value="AZFNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgGw-NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgGw-dLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgGw-tLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgGw-9LZEeiL7cTvwX--2w" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgHYANLZEeiL7cTvwX--2w" name="AZZLTX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgHYAdLZEeiL7cTvwX--2w" value="AZZLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgHYAtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgHYA9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgHYBNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgHYBdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgHYBtLZEeiL7cTvwX--2w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgHYB9LZEeiL7cTvwX--2w" name="AZFMTX" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgHYCNLZEeiL7cTvwX--2w" value="AZFMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgHYCdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgHYCtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgHYC9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgHYDNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgHYDdLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgH_ENLZEeiL7cTvwX--2w" name="AZIWST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgH_EdLZEeiL7cTvwX--2w" value="AZIWST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgH_EtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgH_E9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgH_FNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgH_FdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgH_FtLZEeiL7cTvwX--2w" name="AZIXST" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgH_F9LZEeiL7cTvwX--2w" value="AZIXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgH_GNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgH_GdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgH_GtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgH_G9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgImINLZEeiL7cTvwX--2w" name="AZIYST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgImIdLZEeiL7cTvwX--2w" value="AZIYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgImItLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgImI9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgImJNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgImJdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgJNMNLZEeiL7cTvwX--2w" name="AZIZST" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgJNMdLZEeiL7cTvwX--2w" value="AZIZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgJNMtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgJNM9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgJNNNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgJNNdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgJNNtLZEeiL7cTvwX--2w" name="AZI0ST" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgJNN9LZEeiL7cTvwX--2w" value="AZI0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgJNONLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgJNOdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgJNOtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgJNO9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgJ0QNLZEeiL7cTvwX--2w" name="AZI2ST" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgJ0QdLZEeiL7cTvwX--2w" value="AZI2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgJ0QtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgJ0Q9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgJ0RNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgJ0RdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgJ0RtLZEeiL7cTvwX--2w" name="AZHKCD" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgJ0R9LZEeiL7cTvwX--2w" value="AZHKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgJ0SNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgJ0SdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgJ0StLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgJ0S9LZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgKbUNLZEeiL7cTvwX--2w" name="AZCKTX" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgKbUdLZEeiL7cTvwX--2w" value="AZCKTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgKbUtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgKbU9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgKbVNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgKbVdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgKbVtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgKbV9LZEeiL7cTvwX--2w" name="AZAACD" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgKbWNLZEeiL7cTvwX--2w" value="AZAACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgKbWdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgKbWtLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgKbW9LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgKbXNLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgLCYNLZEeiL7cTvwX--2w" name="AZSEST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgLCYdLZEeiL7cTvwX--2w" value="AZSEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgLCYtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgLCY9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgLCZNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgLCZdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgLCZtLZEeiL7cTvwX--2w" name="AZSFST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgLCZ9LZEeiL7cTvwX--2w" value="AZSFST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgLCaNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgLCadLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgLCatLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgLCa9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgLpcNLZEeiL7cTvwX--2w" name="AZAFCD" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgLpcdLZEeiL7cTvwX--2w" value="AZAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgLpctLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgLpc9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgLpdNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgLpddLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgLpdtLZEeiL7cTvwX--2w" name="AZABCD" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgLpd9LZEeiL7cTvwX--2w" value="AZABCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgLpeNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgMQgNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgMQgdLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgMQgtLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgMQg9LZEeiL7cTvwX--2w" name="AZACCD" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgMQhNLZEeiL7cTvwX--2w" value="AZACCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgMQhdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgMQhtLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgMQh9LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgMQiNLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgM3kNLZEeiL7cTvwX--2w" name="AZAHCD" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgM3kdLZEeiL7cTvwX--2w" value="AZAHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgM3ktLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgM3k9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgM3lNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgM3ldLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgM3ltLZEeiL7cTvwX--2w" name="AZE3ST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgM3l9LZEeiL7cTvwX--2w" value="AZE3ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgM3mNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgM3mdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgM3mtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgM3m9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgNeoNLZEeiL7cTvwX--2w" name="AZCGCD" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgNeodLZEeiL7cTvwX--2w" value="AZCGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgNeotLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgNeo9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgNepNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgNepdLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgNeptLZEeiL7cTvwX--2w" name="AZJ4CD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgNep9LZEeiL7cTvwX--2w" value="AZJ4CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgNeqNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgNeqdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgNeqtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgNeq9LZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgOFsNLZEeiL7cTvwX--2w" name="AZBCCD" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgOFsdLZEeiL7cTvwX--2w" value="AZBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgOFstLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgOFs9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgOFtNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgOFtdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgOFttLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgOFt9LZEeiL7cTvwX--2w" name="AZF3CD" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgOFuNLZEeiL7cTvwX--2w" value="AZF3CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgOFudLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgOFutLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgOFu9LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgOFvNLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgOswNLZEeiL7cTvwX--2w" name="AZADNB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgOswdLZEeiL7cTvwX--2w" value="AZADNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgOswtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgOsw9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgOsxNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgOsxdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgOsxtLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgOsx9LZEeiL7cTvwX--2w" name="AZAENB" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgOsyNLZEeiL7cTvwX--2w" value="AZAENB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgOsydLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgOsytLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgOsy9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgPT0NLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgPT0dLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgPT0tLZEeiL7cTvwX--2w" name="AZFTNB" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgPT09LZEeiL7cTvwX--2w" value="AZFTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgPT1NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgPT1dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgPT1tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgPT19LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgPT2NLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgP64NLZEeiL7cTvwX--2w" name="AZBOCD" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgP64dLZEeiL7cTvwX--2w" value="AZBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgP64tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgP649LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgP65NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgP65dLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgP65tLZEeiL7cTvwX--2w" name="AZHRST" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgP659LZEeiL7cTvwX--2w" value="AZHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgP66NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgP66dLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgP66tLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgP669LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgQh8NLZEeiL7cTvwX--2w" name="AZA4ST" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgQh8dLZEeiL7cTvwX--2w" value="AZA4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgQh8tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgQh89LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgQh9NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgQh9dLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgQh9tLZEeiL7cTvwX--2w" name="AZFWNB" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgQh99LZEeiL7cTvwX--2w" value="AZFWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgQh-NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgQh-dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgQh-tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgQh-9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgQh_NLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgRJANLZEeiL7cTvwX--2w" name="AZA0ST" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgRJAdLZEeiL7cTvwX--2w" value="AZA0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgRJAtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgRJA9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgRJBNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgRJBdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgRJBtLZEeiL7cTvwX--2w" name="AZA3ST" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgRJB9LZEeiL7cTvwX--2w" value="AZA3ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgRwENLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgRwEdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgRwEtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgRwE9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgRwFNLZEeiL7cTvwX--2w" name="AZBDCD" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgRwFdLZEeiL7cTvwX--2w" value="AZBDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgRwFtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgRwF9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgRwGNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgRwGdLZEeiL7cTvwX--2w" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgSXINLZEeiL7cTvwX--2w" name="AZA1ST" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgSXIdLZEeiL7cTvwX--2w" value="AZA1ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgSXItLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgSXI9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgSXJNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgSXJdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgSXJtLZEeiL7cTvwX--2w" name="AZAJNB" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgSXJ9LZEeiL7cTvwX--2w" value="AZAJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgSXKNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgSXKdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgSXKtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgSXK9LZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgS-MNLZEeiL7cTvwX--2w" name="AZA6NA" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgS-MdLZEeiL7cTvwX--2w" value="AZA6NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgS-MtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgS-M9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgS-NNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgS-NdLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgS-NtLZEeiL7cTvwX--2w" name="AZBACD" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgS-N9LZEeiL7cTvwX--2w" value="AZBACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgS-ONLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgS-OdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgS-OtLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgS-O9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgS-PNLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgTlQNLZEeiL7cTvwX--2w" name="AZBBCD" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgTlQdLZEeiL7cTvwX--2w" value="AZBBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgTlQtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgTlQ9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgTlRNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgTlRdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgTlRtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgTlR9LZEeiL7cTvwX--2w" name="AZA2NA" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgTlSNLZEeiL7cTvwX--2w" value="AZA2NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgTlSdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgTlStLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgTlS9LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgTlTNLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgUMUNLZEeiL7cTvwX--2w" name="AZA3NA" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgUMUdLZEeiL7cTvwX--2w" value="AZA3NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgUMUtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgUMU9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgUMVNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgUMVdLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgUzYNLZEeiL7cTvwX--2w" name="AZCFCD" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgUzYdLZEeiL7cTvwX--2w" value="AZCFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgUzYtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgUzY9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgUzZNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgUzZdLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgUzZtLZEeiL7cTvwX--2w" name="AZI4ST" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgUzZ9LZEeiL7cTvwX--2w" value="AZI4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgUzaNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgUzadLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgUzatLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgUza9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgVacNLZEeiL7cTvwX--2w" name="AZB7S1" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgVacdLZEeiL7cTvwX--2w" value="AZB7S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgVactLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgVac9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgVadNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgVaddLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgVadtLZEeiL7cTvwX--2w" name="AZNHST" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgVad9LZEeiL7cTvwX--2w" value="AZNHST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgVaeNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgVaedLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgVaetLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgVae9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgVafNLZEeiL7cTvwX--2w" name="AZD3C1" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgVafdLZEeiL7cTvwX--2w" value="AZD3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgVaftLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgVaf9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgWBgNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgWBgdLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgWBgtLZEeiL7cTvwX--2w" name="AZAUST" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgWBg9LZEeiL7cTvwX--2w" value="AZAUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgWBhNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgWBhdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgWBhtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgWBh9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgWBiNLZEeiL7cTvwX--2w" name="AZI5ST" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgWBidLZEeiL7cTvwX--2w" value="AZI5ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgWBitLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgWBi9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgWBjNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgWBjdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgWokNLZEeiL7cTvwX--2w" name="AZADVA" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgWokdLZEeiL7cTvwX--2w" value="AZADVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgWoktLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgWok9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgWolNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgWoldLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgWoltLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgXPoNLZEeiL7cTvwX--2w" name="AZCBPR" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgXPodLZEeiL7cTvwX--2w" value="AZCBPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgXPotLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgXPo9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgXPpNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgXPpdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgXPptLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgXPp9LZEeiL7cTvwX--2w" name="AZBCNA" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgXPqNLZEeiL7cTvwX--2w" value="AZBCNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgXPqdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgXPqtLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgXPq9LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgXPrNLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgX2sNLZEeiL7cTvwX--2w" name="AZALNB" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgX2sdLZEeiL7cTvwX--2w" value="AZALNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgX2stLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgX2s9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgX2tNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgX2tdLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgX2ttLZEeiL7cTvwX--2w" name="AZAMNB" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgX2t9LZEeiL7cTvwX--2w" value="AZAMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgX2uNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgX2udLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgX2utLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgX2u9LZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgYdwNLZEeiL7cTvwX--2w" name="AZANNB" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgYdwdLZEeiL7cTvwX--2w" value="AZANNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgYdwtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgYdw9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgYdxNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgYdxdLZEeiL7cTvwX--2w" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgYdxtLZEeiL7cTvwX--2w" name="AZAONB" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgYdx9LZEeiL7cTvwX--2w" value="AZAONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgYdyNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgYdydLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgYdytLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgYdy9LZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgZE0NLZEeiL7cTvwX--2w" name="AZBGNA" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgZE0dLZEeiL7cTvwX--2w" value="AZBGNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgZE0tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgZE09LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgZE1NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgZE1dLZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgZE1tLZEeiL7cTvwX--2w" name="AZBHNA" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgZE19LZEeiL7cTvwX--2w" value="AZBHNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgZE2NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgZE2dLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgZE2tLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgZE29LZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgZE3NLZEeiL7cTvwX--2w" name="AZE2ST" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgZr4NLZEeiL7cTvwX--2w" value="AZE2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgZr4dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgZr4tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgZr49LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgZr5NLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgZr5dLZEeiL7cTvwX--2w" name="AZCHCD" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgZr5tLZEeiL7cTvwX--2w" value="AZCHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgZr59LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgZr6NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgZr6dLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgZr6tLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgZr69LZEeiL7cTvwX--2w" name="AZAJCD" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgZr7NLZEeiL7cTvwX--2w" value="AZAJCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgZr7dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgZr7tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgZr79LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgZr8NLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgZr8dLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgaS8NLZEeiL7cTvwX--2w" name="AZBINA" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgaS8dLZEeiL7cTvwX--2w" value="AZBINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgaS8tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgaS89LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgaS9NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgaS9dLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgaS9tLZEeiL7cTvwX--2w" name="AZCQTX" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgaS99LZEeiL7cTvwX--2w" value="AZCQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgaS-NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgaS-dLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgaS-tLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgaS-9LZEeiL7cTvwX--2w" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tga6ANLZEeiL7cTvwX--2w" name="AZI6ST" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tga6AdLZEeiL7cTvwX--2w" value="AZI6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tga6AtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tga6A9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tga6BNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tga6BdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tga6BtLZEeiL7cTvwX--2w" name="AZBJN3" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tga6B9LZEeiL7cTvwX--2w" value="AZBJN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tga6CNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tga6CdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tga6CtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tga6C9LZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgbhENLZEeiL7cTvwX--2w" name="AZBON3" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgbhEdLZEeiL7cTvwX--2w" value="AZBON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgbhEtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgbhE9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgbhFNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgbhFdLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgbhFtLZEeiL7cTvwX--2w" name="AZBPN3" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgbhF9LZEeiL7cTvwX--2w" value="AZBPN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgbhGNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgbhGdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgbhGtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgbhG9LZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgcIINLZEeiL7cTvwX--2w" name="AZHNTX" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgcIIdLZEeiL7cTvwX--2w" value="AZHNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgcIItLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgcII9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgcIJNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgcIJdLZEeiL7cTvwX--2w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgcIJtLZEeiL7cTvwX--2w" name="AZHOTX" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgcIJ9LZEeiL7cTvwX--2w" value="AZHOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgcIKNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgcIKdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgcIKtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgcIK9LZEeiL7cTvwX--2w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgcvMNLZEeiL7cTvwX--2w" name="AZHPTX" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgcvMdLZEeiL7cTvwX--2w" value="AZHPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgcvMtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgcvM9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgcvNNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgcvNdLZEeiL7cTvwX--2w" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgcvNtLZEeiL7cTvwX--2w" name="AZHQTX" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgcvN9LZEeiL7cTvwX--2w" value="AZHQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgcvONLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgcvOdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgcvOtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgcvO9LZEeiL7cTvwX--2w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgdWQNLZEeiL7cTvwX--2w" name="AZBQN3" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgdWQdLZEeiL7cTvwX--2w" value="AZBQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgdWQtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgdWQ9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgdWRNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgdWRdLZEeiL7cTvwX--2w" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tgd9UNLZEeiL7cTvwX--2w" name="AZAKCD" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tgd9UdLZEeiL7cTvwX--2w" value="AZAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tgd9UtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tgd9U9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tgd9VNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tgd9VdLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tgd9VtLZEeiL7cTvwX--2w" name="AZFVPR" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tgd9V9LZEeiL7cTvwX--2w" value="AZFVPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tgd9WNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tgd9WdLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tgd9WtLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tgd9W9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tgd9XNLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgekYNLZEeiL7cTvwX--2w" name="AZGHDT" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgekYdLZEeiL7cTvwX--2w" value="AZGHDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgekYtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgekY9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TgekZNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgekZdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgekZtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TgekZ9LZEeiL7cTvwX--2w" name="AZGIDT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_TgekaNLZEeiL7cTvwX--2w" value="AZGIDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TgekadLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TgekatLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tgeka9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TgekbNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TgekbdLZEeiL7cTvwX--2w" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Th5gsdLZEeiL7cTvwX--2w" name="OTEFCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Th5gstLZEeiL7cTvwX--2w" value="OTEFCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Th5gs9LZEeiL7cTvwX--2w" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_Th7V4NLZEeiL7cTvwX--2w" value="OTEFCPP   "/>
      <node defType="com.stambia.rdbms.column" id="_TiO34NLZEeiL7cTvwX--2w" name="EFAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiO34dLZEeiL7cTvwX--2w" value="EFAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiO34tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiO349LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiO35NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiO35dLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiO35tLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiO359LZEeiL7cTvwX--2w" name="EFTAC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiO36NLZEeiL7cTvwX--2w" value="EFTAC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiO36dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiO36tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiO369LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiO37NLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiPe8NLZEeiL7cTvwX--2w" name="EFNBN3" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiPe8dLZEeiL7cTvwX--2w" value="EFNBN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiPe8tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiPe89LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiPe9NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiPe9dLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiPe9tLZEeiL7cTvwX--2w" name="EFPMT2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiPe99LZEeiL7cTvwX--2w" value="EFPMT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiPe-NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiPe-dLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiPe-tLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiPe-9LZEeiL7cTvwX--2w" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiQGANLZEeiL7cTvwX--2w" name="EFAWNA" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiQGAdLZEeiL7cTvwX--2w" value="EFAWNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiQGAtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiQGA9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiQGBNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiQGBdLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiQGBtLZEeiL7cTvwX--2w" name="EFNDCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiQGB9LZEeiL7cTvwX--2w" value="EFNDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiQGCNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiQGCdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiQGCtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiQGC9LZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiQtENLZEeiL7cTvwX--2w" name="EFHQTX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiQtEdLZEeiL7cTvwX--2w" value="EFHQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiQtEtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiQtE9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiQtFNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiQtFdLZEeiL7cTvwX--2w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiQtFtLZEeiL7cTvwX--2w" name="EFHPTX" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiQtF9LZEeiL7cTvwX--2w" value="EFHPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiQtGNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiQtGdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiQtGtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiQtG9LZEeiL7cTvwX--2w" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiRUINLZEeiL7cTvwX--2w" name="EFAKCD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiRUIdLZEeiL7cTvwX--2w" value="EFAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiRUItLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiRUI9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiRUJNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiRUJdLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiRUJtLZEeiL7cTvwX--2w" name="EFAGTX" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiRUJ9LZEeiL7cTvwX--2w" value="EFAGTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiRUKNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiRUKdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiRUKtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiRUK9LZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiR7MNLZEeiL7cTvwX--2w" name="EFAUTX" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiR7MdLZEeiL7cTvwX--2w" value="EFAUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiR7MtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiR7M9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiR7NNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiR7NdLZEeiL7cTvwX--2w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiR7NtLZEeiL7cTvwX--2w" name="EFHOTX" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiR7N9LZEeiL7cTvwX--2w" value="EFHOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiR7ONLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiR7OdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiR7OtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiR7O9LZEeiL7cTvwX--2w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiSiQNLZEeiL7cTvwX--2w" name="EFU9NA" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiSiQdLZEeiL7cTvwX--2w" value="EFU9NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiSiQtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiSiQ9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiSiRNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiSiRdLZEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiSiRtLZEeiL7cTvwX--2w" name="EFZLTX" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiSiR9LZEeiL7cTvwX--2w" value="EFZLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiSiSNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiSiSdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiSiStLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiSiS9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiSiTNLZEeiL7cTvwX--2w" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiSiTdLZEeiL7cTvwX--2w" name="EFFMTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiSiTtLZEeiL7cTvwX--2w" value="EFFMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiSiT9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiSiUNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiSiUdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiSiUtLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiSiU9LZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiTJUNLZEeiL7cTvwX--2w" name="EFIZST" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiTJUdLZEeiL7cTvwX--2w" value="EFIZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiTJUtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiTJU9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiTJVNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiTJVdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiTJVtLZEeiL7cTvwX--2w" name="EFABCD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiTJV9LZEeiL7cTvwX--2w" value="EFABCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiTJWNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiTJWdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiTJWtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiTJW9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiTwYNLZEeiL7cTvwX--2w" name="EFABNA" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiTwYdLZEeiL7cTvwX--2w" value="EFABNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiTwYtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiTwY9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiTwZNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiTwZdLZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiTwZtLZEeiL7cTvwX--2w" name="EFBCCD" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiTwZ9LZEeiL7cTvwX--2w" value="EFBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiTwaNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiTwadLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiTwatLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiTwa9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiTwbNLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiUXcNLZEeiL7cTvwX--2w" name="EFBSNA" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiUXcdLZEeiL7cTvwX--2w" value="EFBSNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiUXctLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiUXc9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiUXdNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiUXddLZEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiUXdtLZEeiL7cTvwX--2w" name="EFBTNA" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiUXd9LZEeiL7cTvwX--2w" value="EFBTNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiUXeNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiUXedLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiUXetLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiUXe9LZEeiL7cTvwX--2w" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiUXfNLZEeiL7cTvwX--2w" name="EFDNTX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiUXfdLZEeiL7cTvwX--2w" value="EFDNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiUXftLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiUXf9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiUXgNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiUXgdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiUXgtLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiU-gNLZEeiL7cTvwX--2w" name="EFBHCD" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiU-gdLZEeiL7cTvwX--2w" value="EFBHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiU-gtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiU-g9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiU-hNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiU-hdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiU-htLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiU-h9LZEeiL7cTvwX--2w" name="EFBBTX" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiU-iNLZEeiL7cTvwX--2w" value="EFBBTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiU-idLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiU-itLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiU-i9LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiU-jNLZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiVlkNLZEeiL7cTvwX--2w" name="EFAQCD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiVlkdLZEeiL7cTvwX--2w" value="EFAQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiVlktLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiVlk9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiVllNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiVlldLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiVlltLZEeiL7cTvwX--2w" name="EFAOTX" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiVll9LZEeiL7cTvwX--2w" value="EFAOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiVlmNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiVlmdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiVlmtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiVlm9LZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiWMoNLZEeiL7cTvwX--2w" name="EFGHCD" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiWModLZEeiL7cTvwX--2w" value="EFGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiWMotLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiWMo9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiWMpNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiWMpdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiWMptLZEeiL7cTvwX--2w" name="EFHBNA" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiWMp9LZEeiL7cTvwX--2w" value="EFHBNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiWMqNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiWMqdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiWMqtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiWMq9LZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiWMrNLZEeiL7cTvwX--2w" name="EFAMCD" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiWMrdLZEeiL7cTvwX--2w" value="EFAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiWMrtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiWMr9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiWMsNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiWMsdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiWzsNLZEeiL7cTvwX--2w" name="EFSINA" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiWzsdLZEeiL7cTvwX--2w" value="EFSINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiWzstLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiWzs9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiWztNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiWztdLZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiWzttLZEeiL7cTvwX--2w" name="EFBOCD" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiWzt9LZEeiL7cTvwX--2w" value="EFBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiWzuNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiWzudLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiWzutLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiWzu9LZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiXawNLZEeiL7cTvwX--2w" name="EFHRST" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiXawdLZEeiL7cTvwX--2w" value="EFHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiXawtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiXaw9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiXaxNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiXaxdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiXaxtLZEeiL7cTvwX--2w" name="EFBYNA" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiXax9LZEeiL7cTvwX--2w" value="EFBYNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiXayNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiXaydLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiXaytLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiXay9LZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiYB0NLZEeiL7cTvwX--2w" name="EFPFN6" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiYB0dLZEeiL7cTvwX--2w" value="EFPFN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiYB0tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiYB09LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiYB1NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiYB1dLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiYB1tLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiYB19LZEeiL7cTvwX--2w" name="EFVANA" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiYB2NLZEeiL7cTvwX--2w" value="EFVANA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiYB2dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiYB2tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiYB29LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiYB3NLZEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiYo4NLZEeiL7cTvwX--2w" name="EFTFC1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiYo4dLZEeiL7cTvwX--2w" value="EFTFC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiYo4tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiYo49LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiYo5NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiYo5dLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiYo5tLZEeiL7cTvwX--2w" name="EFFTNB" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiYo59LZEeiL7cTvwX--2w" value="EFFTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiYo6NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiYo6dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiYo6tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiYo69LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiYo7NLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiYo7dLZEeiL7cTvwX--2w" name="EFFJTX" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiYo7tLZEeiL7cTvwX--2w" value="EFFJTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiYo79LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiYo8NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiYo8dLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiYo8tLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiZP8NLZEeiL7cTvwX--2w" name="EFPGN6" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiZP8dLZEeiL7cTvwX--2w" value="EFPGN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiZP8tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiZP89LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiZP9NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiZP9dLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiZP9tLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiZP99LZEeiL7cTvwX--2w" name="EFVBNA" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiZP-NLZEeiL7cTvwX--2w" value="EFVBNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiZP-dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiZP-tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiZP-9LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiZP_NLZEeiL7cTvwX--2w" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiZ3ANLZEeiL7cTvwX--2w" name="EFAGCD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiZ3AdLZEeiL7cTvwX--2w" value="EFAGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiZ3AtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiZ3A9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiZ3BNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiZ3BdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiZ3BtLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiZ3B9LZEeiL7cTvwX--2w" name="EFACTX" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiZ3CNLZEeiL7cTvwX--2w" value="EFACTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiZ3CdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiZ3CtLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiZ3C9LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiZ3DNLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiZ3DdLZEeiL7cTvwX--2w" name="EFF2CD" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiZ3DtLZEeiL7cTvwX--2w" value="EFF2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiZ3D9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TiZ3ENLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiZ3EdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiZ3EtLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiZ3E9LZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiaeENLZEeiL7cTvwX--2w" name="EFHNNA" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiaeEdLZEeiL7cTvwX--2w" value="EFHNNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiaeEtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiaeE9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiaeFNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiaeFdLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiaeFtLZEeiL7cTvwX--2w" name="EFKVS3" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiaeF9LZEeiL7cTvwX--2w" value="EFKVS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiaeGNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiaeGdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiaeGtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiaeG9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TibFINLZEeiL7cTvwX--2w" name="EFTWP3" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_TibFIdLZEeiL7cTvwX--2w" value="EFTWP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TibFItLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TibFI9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TibFJNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TibFJdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TibFJtLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TibFJ9LZEeiL7cTvwX--2w" name="EFTXP3" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_TibFKNLZEeiL7cTvwX--2w" value="EFTXP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TibFKdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TibFKtLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TibFK9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TibFLNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TibFLdLZEeiL7cTvwX--2w" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TibsMNLZEeiL7cTvwX--2w" name="EFVUN2" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_TibsMdLZEeiL7cTvwX--2w" value="EFVUN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TibsMtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TibsM9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TibsNNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TibsNdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TicTQNLZEeiL7cTvwX--2w" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TicTQdLZEeiL7cTvwX--2w" name="EFV1N2" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_TicTQtLZEeiL7cTvwX--2w" value="EFV1N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TicTQ9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TicTRNLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TicTRdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TicTRtLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TicTR9LZEeiL7cTvwX--2w" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TicTSNLZEeiL7cTvwX--2w" name="EFV2N2" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_TicTSdLZEeiL7cTvwX--2w" value="EFV2N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TicTStLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TicTS9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TicTTNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TicTTdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TicTTtLZEeiL7cTvwX--2w" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tic6UNLZEeiL7cTvwX--2w" name="EFV3N2" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tic6UdLZEeiL7cTvwX--2w" value="EFV3N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tic6UtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tic6U9LZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tic6VNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tic6VdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tic6VtLZEeiL7cTvwX--2w" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tic6V9LZEeiL7cTvwX--2w" name="EFV4N2" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tic6WNLZEeiL7cTvwX--2w" value="EFV4N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tic6WdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tic6WtLZEeiL7cTvwX--2w" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tic6W9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tic6XNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tic6XdLZEeiL7cTvwX--2w" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TidhYNLZEeiL7cTvwX--2w" name="EFBACD" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_TidhYdLZEeiL7cTvwX--2w" value="EFBACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TidhYtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TidhY9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TidhZNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TidhZdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TidhZtLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TidhZ9LZEeiL7cTvwX--2w" name="EFVCNA" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_TidhaNLZEeiL7cTvwX--2w" value="EFVCNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TidhadLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TidhatLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tidha9LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TidhbNLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TieIcNLZEeiL7cTvwX--2w" name="EFPHN6" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_TieIcdLZEeiL7cTvwX--2w" value="EFPHN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TieIctLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TieIc9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TieIdNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TieIddLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TieIdtLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TieId9LZEeiL7cTvwX--2w" name="EFBBCD" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_TieIeNLZEeiL7cTvwX--2w" value="EFBBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TieIedLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TieIetLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TieIe9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TieIfNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TieIfdLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TievgNLZEeiL7cTvwX--2w" name="EFVDNA" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_TievgdLZEeiL7cTvwX--2w" value="EFVDNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TievgtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tievg9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TievhNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TievhdLZEeiL7cTvwX--2w" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TievhtLZEeiL7cTvwX--2w" name="EFPIN6" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tievh9LZEeiL7cTvwX--2w" value="EFPIN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TieviNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TievidLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TievitLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tievi9LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TievjNLZEeiL7cTvwX--2w" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TifWkNLZEeiL7cTvwX--2w" name="EFBNNA" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_TifWkdLZEeiL7cTvwX--2w" value="EFBNNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TifWktLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TifWk9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TifWlNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TifWldLZEeiL7cTvwX--2w" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TifWltLZEeiL7cTvwX--2w" name="EFPNT2" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_TifWl9LZEeiL7cTvwX--2w" value="EFPNT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TifWmNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TifWmdLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TifWmtLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TifWm9LZEeiL7cTvwX--2w" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TifWnNLZEeiL7cTvwX--2w" name="EFVENA" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_TifWndLZEeiL7cTvwX--2w" value="EFVENA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TifWntLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TifWn9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TifWoNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TifWodLZEeiL7cTvwX--2w" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tif9oNLZEeiL7cTvwX--2w" name="EFQON6" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tif9odLZEeiL7cTvwX--2w" value="EFQON6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tif9otLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tif9o9LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tif9pNLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tif9pdLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tif9ptLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tif9p9LZEeiL7cTvwX--2w" name="EFQPN6" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tif9qNLZEeiL7cTvwX--2w" value="EFQPN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tif9qdLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Tif9qtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tif9q9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tif9rNLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tif9rdLZEeiL7cTvwX--2w" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TigksNLZEeiL7cTvwX--2w" name="EFK8S3" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_TigksdLZEeiL7cTvwX--2w" value="EFK8S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TigkstLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tigks9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TigktNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TigktdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TigkttLZEeiL7cTvwX--2w" name="EFK9S3" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tigkt9LZEeiL7cTvwX--2w" value="EFK9S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TigkuNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TigkudLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TigkutLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tigku9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TihLwNLZEeiL7cTvwX--2w" name="EFLBS3" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_TihLwdLZEeiL7cTvwX--2w" value="EFLBS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TihLwtLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TihLw9LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TihLxNLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TihLxdLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TihLxtLZEeiL7cTvwX--2w" name="EFLAS3" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_TihLx9LZEeiL7cTvwX--2w" value="EFLAS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TihLyNLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TihLydLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TihLytLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TihLy9LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tihy0NLZEeiL7cTvwX--2w" name="EFOMS3" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tihy0dLZEeiL7cTvwX--2w" value="EFOMS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tihy0tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tihy09LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tihy1NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tihy1dLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tihy1tLZEeiL7cTvwX--2w" name="EFLCS3" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tihy19LZEeiL7cTvwX--2w" value="EFLCS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tihy2NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tihy2dLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tihy2tLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Tihy29LZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Tihy3NLZEeiL7cTvwX--2w" name="EFLDS3" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_Tihy3dLZEeiL7cTvwX--2w" value="EFLDS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Tihy3tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Tihy39LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Tihy4NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiiZ4NLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiiZ4dLZEeiL7cTvwX--2w" name="EFLES3" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiiZ4tLZEeiL7cTvwX--2w" value="EFLES3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiiZ49LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiiZ5NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiiZ5dLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiiZ5tLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TiiZ59LZEeiL7cTvwX--2w" name="EFLFS3" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_TiiZ6NLZEeiL7cTvwX--2w" value="EFLFS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TiiZ6dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TiiZ6tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TiiZ69LZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TiiZ7NLZEeiL7cTvwX--2w" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TijA8NLZEeiL7cTvwX--2w" name="EFLGS3" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_TijA8dLZEeiL7cTvwX--2w" value="EFLGS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TijA8tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TijA89LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TijA9NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TijA9dLZEeiL7cTvwX--2w" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_ThhtQNLZEeiL7cTvwX--2w" name="ORIIREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_ThhtQdLZEeiL7cTvwX--2w" value="ORIIREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_ThhtQtLZEeiL7cTvwX--2w" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_Thnz4NLZEeiL7cTvwX--2w" value="ORIIREP   "/>
      <node defType="com.stambia.rdbms.column" id="_Thw90NLZEeiL7cTvwX--2w" name="IIAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Thw90dLZEeiL7cTvwX--2w" value="IIAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Thw90tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Thw909LZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Thw91NLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Thw91dLZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Thw91tLZEeiL7cTvwX--2w" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Thxk4NLZEeiL7cTvwX--2w" name="IIA5CD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Thxk4dLZEeiL7cTvwX--2w" value="IIA5CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Thxk4tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Thxk49LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Thxk5NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Thxk5dLZEeiL7cTvwX--2w" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Thxk5tLZEeiL7cTvwX--2w" name="IIJCNB" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_Thxk59LZEeiL7cTvwX--2w" value="IIJCNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Thxk6NLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Thxk6dLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Thxk6tLZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Thxk69LZEeiL7cTvwX--2w" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Thxk7NLZEeiL7cTvwX--2w" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ThyL8NLZEeiL7cTvwX--2w" name="IIBRN3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_ThyL8dLZEeiL7cTvwX--2w" value="IIBRN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ThyL8tLZEeiL7cTvwX--2w" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ThyL89LZEeiL7cTvwX--2w" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ThyL9NLZEeiL7cTvwX--2w" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ThyL9dLZEeiL7cTvwX--2w" value="35"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_mHT2QdezEei1-oCUCrJoUg" name="ORJ3REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_mHT2QtezEei1-oCUCrJoUg" value="ORJ3REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_mHT2Q9ezEei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_mHdAMNezEei1-oCUCrJoUg" value="ORJ3REP   "/>
      <node defType="com.stambia.rdbms.column" id="_mH9WgNezEei1-oCUCrJoUg" name="J3AOCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_mH9WgdezEei1-oCUCrJoUg" value="J3AOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mH9WgtezEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mH9Wg9ezEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mH9WhNezEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mH9WhdezEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_mH9WhtezEei1-oCUCrJoUg" name="J3ANTX" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_mH9Wh9ezEei1-oCUCrJoUg" value="J3ANTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mH9WiNezEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mH9WidezEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mH9WitezEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mH9Wi9ezEei1-oCUCrJoUg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_mH9WjNezEei1-oCUCrJoUg" name="J3XAST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_mH9WjdezEei1-oCUCrJoUg" value="J3XAST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_mH9WjtezEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_mH9Wj9ezEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_mH9WkNezEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_mH9WkdezEei1-oCUCrJoUg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_8IlqMde1Eei1-oCUCrJoUg" name="COMUNEP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_8IlqMte1Eei1-oCUCrJoUg" value="COMUNEP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_8IlqM9e1Eei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_8IlqNNe1Eei1-oCUCrJoUg" value="COMUNEP   "/>
      <node defType="com.stambia.rdbms.column" id="_8J540Ne1Eei1-oCUCrJoUg" name="COMDPT" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_8J540de1Eei1-oCUCrJoUg" value="COMDPT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8J540te1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8J5409e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8J541Ne1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8J541de1Eei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8KDp0Ne1Eei1-oCUCrJoUg" name="COMINS" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_8KDp0de1Eei1-oCUCrJoUg" value="COMINS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8KDp0te1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8KDp09e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8KDp1Ne1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8KDp1de1Eei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8KDp1te1Eei1-oCUCrJoUg" name="COMPCO" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_8KDp19e1Eei1-oCUCrJoUg" value="COMPCO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8KDp2Ne1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8KDp2de1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8KDp2te1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8KDp29e1Eei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8KDp3Ne1Eei1-oCUCrJoUg" name="COMVIL" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_8KDp3de1Eei1-oCUCrJoUg" value="COMVIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8KDp3te1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8KDp39e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8KDp4Ne1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8KDp4de1Eei1-oCUCrJoUg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8KDp4te1Eei1-oCUCrJoUg" name="COMPOS" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_8KDp49e1Eei1-oCUCrJoUg" value="COMPOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8KDp5Ne1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8KDp5de1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8KDp5te1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8KDp59e1Eei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8KDp6Ne1Eei1-oCUCrJoUg" name="COMPCB" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_8KDp6de1Eei1-oCUCrJoUg" value="COMPCB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8KDp6te1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8KDp69e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8KDp7Ne1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8KDp7de1Eei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8KDp7te1Eei1-oCUCrJoUg" name="COMDIS" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_8KDp79e1Eei1-oCUCrJoUg" value="COMDIS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8KDp8Ne1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8KDp8de1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8KDp8te1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8KDp89e1Eei1-oCUCrJoUg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8KDp9Ne1Eei1-oCUCrJoUg" name="COMPTA" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_8KDp9de1Eei1-oCUCrJoUg" value="COMPTA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8KDp9te1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8KDp99e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8KDp-Ne1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8KDp-de1Eei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8KDp-te1Eei1-oCUCrJoUg" name="COMLIB" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_8KDp-9e1Eei1-oCUCrJoUg" value="COMLIB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8KDp_Ne1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8KDp_de1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8KDp_te1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8KDp_9e1Eei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_8KDqANe1Eei1-oCUCrJoUg" name="COMCHO" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_8KDqAde1Eei1-oCUCrJoUg" value="COMCHO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_8KDqAte1Eei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_8KDqA9e1Eei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_8KDqBNe1Eei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_8KDqBde1Eei1-oCUCrJoUg" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_wsqA4dhOEei1-oCUCrJoUg" name="ORADREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_wsqA4thOEei1-oCUCrJoUg" value="ORADREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_wsqA49hOEei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_wsqA5NhOEei1-oCUCrJoUg" value="ORADREP   "/>
      <node defType="com.stambia.rdbms.column" id="_wszx4NhOEei1-oCUCrJoUg" name="ADADCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_wszx4dhOEei1-oCUCrJoUg" value="ADADCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wszx4thOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wszx49hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wszx5NhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wszx5dhOEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wszx5thOEei1-oCUCrJoUg" name="ADADNA" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_wszx59hOEei1-oCUCrJoUg" value="ADADNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wszx6NhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wszx6dhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wszx6thOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wszx69hOEei1-oCUCrJoUg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wszx7NhOEei1-oCUCrJoUg" name="ADNRST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_wszx7dhOEei1-oCUCrJoUg" value="ADNRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wszx7thOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wszx79hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wszx8NhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wszx8dhOEei1-oCUCrJoUg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_wp8EEdhOEei1-oCUCrJoUg" name="OSGWREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_wp8EEthOEei1-oCUCrJoUg" value="OSGWREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_wp8EE9hOEei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_wqF1ENhOEei1-oCUCrJoUg" value="OSGWREP   "/>
      <node defType="com.stambia.rdbms.column" id="_wqTQcNhOEei1-oCUCrJoUg" name="GWEHC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqTQcdhOEei1-oCUCrJoUg" value="GWEHC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqTQcthOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqTQc9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqTQdNhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqTQddhOEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqTQdthOEei1-oCUCrJoUg" name="GWAACD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqTQd9hOEei1-oCUCrJoUg" value="GWAACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqTQeNhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqTQedhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqTQethOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqTQe9hOEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqTQfNhOEei1-oCUCrJoUg" name="GWAKCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqTQfdhOEei1-oCUCrJoUg" value="GWAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqTQfthOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqTQf9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqTQgNhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqTQgdhOEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBcNhOEei1-oCUCrJoUg" name="GWSINA" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBcdhOEei1-oCUCrJoUg" value="GWSINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBcthOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBc9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBdNhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBddhOEei1-oCUCrJoUg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBdthOEei1-oCUCrJoUg" name="GWESC1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBd9hOEei1-oCUCrJoUg" value="GWESC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBeNhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBedhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBethOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBe9hOEei1-oCUCrJoUg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBfNhOEei1-oCUCrJoUg" name="GWABVN" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBfdhOEei1-oCUCrJoUg" value="GWABVN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBfthOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBf9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBgNhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBgdhOEei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBgthOEei1-oCUCrJoUg" name="GWJ6N3" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBg9hOEei1-oCUCrJoUg" value="GWJ6N3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBhNhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBhdhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBhthOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBh9hOEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBiNhOEei1-oCUCrJoUg" name="GWJ7N3" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBidhOEei1-oCUCrJoUg" value="GWJ7N3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBithOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBi9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBjNhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBjdhOEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBjthOEei1-oCUCrJoUg" name="GWSONA" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBj9hOEei1-oCUCrJoUg" value="GWSONA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBkNhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBkdhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBkthOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBk9hOEei1-oCUCrJoUg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBlNhOEei1-oCUCrJoUg" name="GWSYT1" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBldhOEei1-oCUCrJoUg" value="GWSYT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBlthOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBl9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBmNhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBmdhOEei1-oCUCrJoUg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBmthOEei1-oCUCrJoUg" name="GWSZT1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBm9hOEei1-oCUCrJoUg" value="GWSZT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBnNhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBndhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBnthOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBn9hOEei1-oCUCrJoUg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBoNhOEei1-oCUCrJoUg" name="GWS0T1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBodhOEei1-oCUCrJoUg" value="GWS0T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBothOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBo9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBpNhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBpdhOEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBpthOEei1-oCUCrJoUg" name="GWS1T1" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBp9hOEei1-oCUCrJoUg" value="GWS1T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBqNhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBqdhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBqthOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBq9hOEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBrNhOEei1-oCUCrJoUg" name="GWS2T1" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBrdhOEei1-oCUCrJoUg" value="GWS2T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBrthOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBr9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBsNhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBsdhOEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBsthOEei1-oCUCrJoUg" name="GWNYN5" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBs9hOEei1-oCUCrJoUg" value="GWNYN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBtNhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wqdBtdhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBtthOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBt9hOEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBuNhOEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBudhOEei1-oCUCrJoUg" name="GWETC1" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdButhOEei1-oCUCrJoUg" value="GWETC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBu9hOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBvNhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBvdhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBvthOEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBv9hOEei1-oCUCrJoUg" name="GWEUC1" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBwNhOEei1-oCUCrJoUg" value="GWEUC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBwdhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBwthOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBw9hOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBxNhOEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBxdhOEei1-oCUCrJoUg" name="GWS3T1" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBxthOEei1-oCUCrJoUg" value="GWS3T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBx9hOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdByNhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBydhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdBythOEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdBy9hOEei1-oCUCrJoUg" name="GWS4T1" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdBzNhOEei1-oCUCrJoUg" value="GWS4T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdBzdhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdBzthOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdBz9hOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdB0NhOEei1-oCUCrJoUg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdB0dhOEei1-oCUCrJoUg" name="GWNZN5" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdB0thOEei1-oCUCrJoUg" value="GWNZN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdB09hOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wqdB1NhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdB1dhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdB1thOEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdB19hOEei1-oCUCrJoUg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdB2NhOEei1-oCUCrJoUg" name="GWN0N5" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdB2dhOEei1-oCUCrJoUg" value="GWN0N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdB2thOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wqdB29hOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdB3NhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdB3dhOEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdB3thOEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wqdB39hOEei1-oCUCrJoUg" name="GWS6S2" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_wqdB4NhOEei1-oCUCrJoUg" value="GWS6S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wqdB4dhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wqdB4thOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wqdB49hOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wqdB5NhOEei1-oCUCrJoUg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_wkWZcdhOEei1-oCUCrJoUg" name="ORAKREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_wkWZcthOEei1-oCUCrJoUg" value="ORAKREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_wkWZc9hOEei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_wkj00NhOEei1-oCUCrJoUg" value="ORAKREP   "/>
      <node defType="com.stambia.rdbms.column" id="_wmHUANhOEei1-oCUCrJoUg" name="AKAKCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_wmHUAdhOEei1-oCUCrJoUg" value="AKAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wmHUAthOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wmHUA9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wmHUBNhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wmHUBdhOEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wmHUBthOEei1-oCUCrJoUg" name="AKAGTX" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_wmHUB9hOEei1-oCUCrJoUg" value="AKAGTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wmHUCNhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wmHUCdhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wmHUCthOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wmHUC9hOEei1-oCUCrJoUg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wmHUDNhOEei1-oCUCrJoUg" name="AKAGST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_wmHUDdhOEei1-oCUCrJoUg" value="AKAGST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wmHUDthOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wmHUD9hOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wmHUENhOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wmHUEdhOEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wmHUEthOEei1-oCUCrJoUg" name="AKNSST" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_wmHUE9hOEei1-oCUCrJoUg" value="AKNSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wmHUFNhOEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wmHUFdhOEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wmHUFthOEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wmHUF9hOEei1-oCUCrJoUg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_GtoxcdkjEei1-oCUCrJoUg" name="OSOZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_GtoxctkjEei1-oCUCrJoUg" value="OSOZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Gtoxc9kjEei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_GtoxdNkjEei1-oCUCrJoUg" value="OSOZREP   "/>
      <node defType="com.stambia.rdbms.column" id="_GtyicNkjEei1-oCUCrJoUg" name="OZDBC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_GtyicdkjEei1-oCUCrJoUg" value="OZDBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GtyictkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gtyic9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GtyidNkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GtyiddkjEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GtyidtkjEei1-oCUCrJoUg" name="OZDCC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gtyid9kjEei1-oCUCrJoUg" value="OZDCC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GtyieNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GtyiedkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GtyietkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gtyie9kjEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GtyifNkjEei1-oCUCrJoUg" name="OZDDC1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_GtyifdkjEei1-oCUCrJoUg" value="OZDDC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GtyiftkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gtyif9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GtyigNkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GtyigdkjEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GtyigtkjEei1-oCUCrJoUg" name="OZDEC1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gtyig9kjEei1-oCUCrJoUg" value="OZDEC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GtyihNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GtyihdkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GtyihtkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gtyih9kjEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GtyiiNkjEei1-oCUCrJoUg" name="OZZKS2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_GtyiidkjEei1-oCUCrJoUg" value="OZZKS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GtyiitkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gtyii9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GtyijNkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GtyijdkjEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GtyijtkjEei1-oCUCrJoUg" name="OZQ8S2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gtyij9kjEei1-oCUCrJoUg" value="OZQ8S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GtyikNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GtyikdkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GtyiktkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gtyik9kjEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GtyilNkjEei1-oCUCrJoUg" name="OZQ7S2" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_GtyildkjEei1-oCUCrJoUg" value="OZQ7S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GtyiltkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gtyil9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GtyimNkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GtyimdkjEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GtyimtkjEei1-oCUCrJoUg" name="OZDZN6" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gtyim9kjEei1-oCUCrJoUg" value="OZDZN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GtyinNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_GtyindkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GtyintkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gtyin9kjEei1-oCUCrJoUg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GtyioNkjEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_GtyiodkjEei1-oCUCrJoUg" name="OZDAC1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_GtyiotkjEei1-oCUCrJoUg" value="OZDAC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gtyio9kjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GtyipNkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_GtyipdkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GtyiptkjEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gtyip9kjEei1-oCUCrJoUg" name="OZZLS2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_GtyiqNkjEei1-oCUCrJoUg" value="OZZLS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_GtyiqdkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_GtyiqtkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gtyiq9kjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_GtyirNkjEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7sYNkjEei1-oCUCrJoUg" name="OZM3C1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sYdkjEei1-oCUCrJoUg" value="OZM3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7sYtkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sY9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7sZNkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7sZdkjEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7sZtkjEei1-oCUCrJoUg" name="OZQ9S2" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sZ9kjEei1-oCUCrJoUg" value="OZQ9S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7saNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sadkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7satkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7sa9kjEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7sbNkjEei1-oCUCrJoUg" name="OZQ4T1" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sbdkjEei1-oCUCrJoUg" value="OZQ4T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7sbtkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sb9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7scNkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7scdkjEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7sctkjEei1-oCUCrJoUg" name="OZQ5T1" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sc9kjEei1-oCUCrJoUg" value="OZQ5T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7sdNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sddkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7sdtkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7sd9kjEei1-oCUCrJoUg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7seNkjEei1-oCUCrJoUg" name="OZGJT2" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sedkjEei1-oCUCrJoUg" value="OZGJT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7setkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7se9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7sfNkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7sfdkjEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7sftkjEei1-oCUCrJoUg" name="OZGKT2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sf9kjEei1-oCUCrJoUg" value="OZGKT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7sgNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sgdkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7sgtkjEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7sg9kjEei1-oCUCrJoUg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7shNkjEei1-oCUCrJoUg" name="OZD8VA" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7shdkjEei1-oCUCrJoUg" value="OZD8VA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7shtkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7sh9kjEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7siNkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7sidkjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7sitkjEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7si9kjEei1-oCUCrJoUg" name="OZD4N6" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sjNkjEei1-oCUCrJoUg" value="OZD4N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7sjdkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7sjtkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sj9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7skNkjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7skdkjEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7sktkjEei1-oCUCrJoUg" name="OZOWPC" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sk9kjEei1-oCUCrJoUg" value="OZOWPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7slNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7sldkjEei1-oCUCrJoUg" value="6"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sltkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7sl9kjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7smNkjEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7smdkjEei1-oCUCrJoUg" name="OZBCCD" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7smtkjEei1-oCUCrJoUg" value="OZBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7sm9kjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7snNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sndkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7sntkjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7sn9kjEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7soNkjEei1-oCUCrJoUg" name="OZD9VA" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sodkjEei1-oCUCrJoUg" value="OZD9VA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7sotkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7so9kjEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7spNkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7spdkjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7sptkjEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7sp9kjEei1-oCUCrJoUg" name="OZEAVA" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sqNkjEei1-oCUCrJoUg" value="OZEAVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7sqdkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7sqtkjEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sq9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7srNkjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7srdkjEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7srtkjEei1-oCUCrJoUg" name="OZEBVA" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sr9kjEei1-oCUCrJoUg" value="OZEBVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7ssNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7ssdkjEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sstkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7ss9kjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7stNkjEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7stdkjEei1-oCUCrJoUg" name="OZECVA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sttkjEei1-oCUCrJoUg" value="OZECVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7st9kjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7suNkjEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sudkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7sutkjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7su9kjEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7svNkjEei1-oCUCrJoUg" name="OZEDVA" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7svdkjEei1-oCUCrJoUg" value="OZEDVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7svtkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7sv9kjEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7swNkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7swdkjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7swtkjEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7sw9kjEei1-oCUCrJoUg" name="OZEEVA" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sxNkjEei1-oCUCrJoUg" value="OZEEVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7sxdkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7sxtkjEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sx9kjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7syNkjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7sydkjEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7sytkjEei1-oCUCrJoUg" name="OZD2N6" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7sy9kjEei1-oCUCrJoUg" value="OZD2N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7szNkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7szdkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7sztkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7sz9kjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7s0NkjEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Gt7s0dkjEei1-oCUCrJoUg" name="OZD3N6" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_Gt7s0tkjEei1-oCUCrJoUg" value="OZD3N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Gt7s09kjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Gt7s1NkjEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Gt7s1dkjEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Gt7s1tkjEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Gt7s19kjEei1-oCUCrJoUg" value="5"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_2tW9AduZEei1-oCUCrJoUg" name="OSMVCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_2tW9AtuZEei1-oCUCrJoUg" value="OSMVCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_2tW9A9uZEei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_2t3TUNuZEei1-oCUCrJoUg" value="OSMVCPP   "/>
      <node defType="com.stambia.rdbms.column" id="_21XpgNuZEei1-oCUCrJoUg" name="MVAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpgduZEei1-oCUCrJoUg" value="MVAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21XpgtuZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_21Xpg9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XphNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21XphduZEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XphtuZEei1-oCUCrJoUg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21Xph9uZEei1-oCUCrJoUg" name="MVGHCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpiNuZEei1-oCUCrJoUg" value="MVGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21XpiduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XpituZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21Xpi9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpjNuZEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21XpjduZEei1-oCUCrJoUg" name="MVJ6C1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpjtuZEei1-oCUCrJoUg" value="MVJ6C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21Xpj9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XpkNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21XpkduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpktuZEei1-oCUCrJoUg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21Xpk9uZEei1-oCUCrJoUg" name="MVLQN3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XplNuZEei1-oCUCrJoUg" value="MVLQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21XplduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XpltuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21Xpl9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpmNuZEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21XpmduZEei1-oCUCrJoUg" name="MVJZC1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpmtuZEei1-oCUCrJoUg" value="MVJZC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21Xpm9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XpnNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21XpnduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpntuZEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21Xpn9uZEei1-oCUCrJoUg" name="MVJ0C1" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpoNuZEei1-oCUCrJoUg" value="MVJ0C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21XpoduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XpotuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21Xpo9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XppNuZEei1-oCUCrJoUg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21XppduZEei1-oCUCrJoUg" name="MVYAT1" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpptuZEei1-oCUCrJoUg" value="MVYAT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21Xpp9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XpqNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21XpqduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpqtuZEei1-oCUCrJoUg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21Xpq9uZEei1-oCUCrJoUg" name="MVYBT1" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XprNuZEei1-oCUCrJoUg" value="MVYBT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21XprduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XprtuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21Xpr9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpsNuZEei1-oCUCrJoUg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21XpsduZEei1-oCUCrJoUg" name="MVYCT1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpstuZEei1-oCUCrJoUg" value="MVYCT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21Xps9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XptNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21XptduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpttuZEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21Xpt9uZEei1-oCUCrJoUg" name="MVYDT1" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpuNuZEei1-oCUCrJoUg" value="MVYDT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21XpuduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XputuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21Xpu9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpvNuZEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21XpvduZEei1-oCUCrJoUg" name="MVYET1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpvtuZEei1-oCUCrJoUg" value="MVYET1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21Xpv9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XpwNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21XpwduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpwtuZEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21Xpw9uZEei1-oCUCrJoUg" name="MVYFT1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpxNuZEei1-oCUCrJoUg" value="MVYFT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21XpxduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XpxtuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21Xpx9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpyNuZEei1-oCUCrJoUg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21XpyduZEei1-oCUCrJoUg" name="MVYGT1" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_21XpytuZEei1-oCUCrJoUg" value="MVYGT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21Xpy9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21XpzNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21XpzduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21XpztuZEei1-oCUCrJoUg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21Xpz9uZEei1-oCUCrJoUg" name="MVZ1N5" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_21Xp0NuZEei1-oCUCrJoUg" value="MVZ1N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21Xp0duZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_21Xp0tuZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21Xp09uZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21Xp1NuZEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21Xp1duZEei1-oCUCrJoUg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzcNuZEei1-oCUCrJoUg" name="MVZ2N5" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzcduZEei1-oCUCrJoUg" value="MVZ2N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzctuZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_21gzc9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzdNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzdduZEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzdtuZEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzd9uZEei1-oCUCrJoUg" name="MVYAS2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzeNuZEei1-oCUCrJoUg" value="MVYAS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzeduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzetuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gze9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzfNuZEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzfduZEei1-oCUCrJoUg" name="MVYHT1" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzftuZEei1-oCUCrJoUg" value="MVYHT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzf9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzgNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzgduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzgtuZEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzg9uZEei1-oCUCrJoUg" name="MVYIT1" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzhNuZEei1-oCUCrJoUg" value="MVYIT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzhduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzhtuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzh9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gziNuZEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gziduZEei1-oCUCrJoUg" name="MVYJT1" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzituZEei1-oCUCrJoUg" value="MVYJT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzi9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzjNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzjduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzjtuZEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzj9uZEei1-oCUCrJoUg" name="MVYKT1" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzkNuZEei1-oCUCrJoUg" value="MVYKT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzkduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzktuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzk9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzlNuZEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzlduZEei1-oCUCrJoUg" name="MVYLT1" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzltuZEei1-oCUCrJoUg" value="MVYLT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzl9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzmNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzmduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzmtuZEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzm9uZEei1-oCUCrJoUg" name="MVYMT1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gznNuZEei1-oCUCrJoUg" value="MVYMT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gznduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzntuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzn9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzoNuZEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzoduZEei1-oCUCrJoUg" name="MVYNT1" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzotuZEei1-oCUCrJoUg" value="MVYNT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzo9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzpNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzpduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzptuZEei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzp9uZEei1-oCUCrJoUg" name="MVYOT1" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzqNuZEei1-oCUCrJoUg" value="MVYOT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzqduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzqtuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzq9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzrNuZEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzrduZEei1-oCUCrJoUg" name="MVYPT1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzrtuZEei1-oCUCrJoUg" value="MVYPT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzr9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzsNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzsduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzstuZEei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzs9uZEei1-oCUCrJoUg" name="MVYQT1" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gztNuZEei1-oCUCrJoUg" value="MVYQT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gztduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzttuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzt9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzuNuZEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzuduZEei1-oCUCrJoUg" name="MVYRT1" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzutuZEei1-oCUCrJoUg" value="MVYRT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzu9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzvNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzvduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzvtuZEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzv9uZEei1-oCUCrJoUg" name="MVYST1" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzwNuZEei1-oCUCrJoUg" value="MVYST1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzwduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzwtuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzw9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzxNuZEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzxduZEei1-oCUCrJoUg" name="MVYTT1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzxtuZEei1-oCUCrJoUg" value="MVYTT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzx9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzyNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzyduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gzytuZEei1-oCUCrJoUg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gzy9uZEei1-oCUCrJoUg" name="MVYUT1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gzzNuZEei1-oCUCrJoUg" value="MVYUT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gzzduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gzztuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gzz9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gz0NuZEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gz0duZEei1-oCUCrJoUg" name="MVYVT1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gz0tuZEei1-oCUCrJoUg" value="MVYVT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gz09uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gz1NuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gz1duZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gz1tuZEei1-oCUCrJoUg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gz19uZEei1-oCUCrJoUg" name="MVYWT1" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gz2NuZEei1-oCUCrJoUg" value="MVYWT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gz2duZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gz2tuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gz29uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gz3NuZEei1-oCUCrJoUg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gz3duZEei1-oCUCrJoUg" name="MVYXT1" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gz3tuZEei1-oCUCrJoUg" value="MVYXT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gz39uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gz4NuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gz4duZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gz4tuZEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gz49uZEei1-oCUCrJoUg" name="MVY2T1" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gz5NuZEei1-oCUCrJoUg" value="MVY2T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gz5duZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gz5tuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gz59uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gz6NuZEei1-oCUCrJoUg" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gz6duZEei1-oCUCrJoUg" name="MVY3T1" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gz6tuZEei1-oCUCrJoUg" value="MVY3T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gz69uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gz7NuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gz7duZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gz7tuZEei1-oCUCrJoUg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gz79uZEei1-oCUCrJoUg" name="MVY4T1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gz8NuZEei1-oCUCrJoUg" value="MVY4T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gz8duZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gz8tuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gz89uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gz9NuZEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gz9duZEei1-oCUCrJoUg" name="MVY5T1" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gz9tuZEei1-oCUCrJoUg" value="MVY5T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gz99uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gz-NuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gz-duZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21gz-tuZEei1-oCUCrJoUg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21gz-9uZEei1-oCUCrJoUg" name="MVY6T1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_21gz_NuZEei1-oCUCrJoUg" value="MVY6T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21gz_duZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21gz_tuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21gz_9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0ANuZEei1-oCUCrJoUg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0AduZEei1-oCUCrJoUg" name="MVYYT1" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0AtuZEei1-oCUCrJoUg" value="MVYYT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0A9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0BNuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0BduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0BtuZEei1-oCUCrJoUg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0B9uZEei1-oCUCrJoUg" name="MVYZT1" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0CNuZEei1-oCUCrJoUg" value="MVYZT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0CduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0CtuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0C9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0DNuZEei1-oCUCrJoUg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0DduZEei1-oCUCrJoUg" name="MVY0T1" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0DtuZEei1-oCUCrJoUg" value="MVY0T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0D9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0ENuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0EduZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0EtuZEei1-oCUCrJoUg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0E9uZEei1-oCUCrJoUg" name="MVY1T1" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0FNuZEei1-oCUCrJoUg" value="MVY1T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0FduZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0FtuZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0F9uZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0GNuZEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0GduZEei1-oCUCrJoUg" name="MVEHN6" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0GtuZEei1-oCUCrJoUg" value="MVEHN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0G9uZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_21g0HNuZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0HduZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0HtuZEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0H9uZEei1-oCUCrJoUg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0INuZEei1-oCUCrJoUg" name="MVNPC1" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0IduZEei1-oCUCrJoUg" value="MVNPC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0ItuZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0I9uZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0JNuZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0JduZEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0JtuZEei1-oCUCrJoUg" name="MVNQC1" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0J9uZEei1-oCUCrJoUg" value="MVNQC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0KNuZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0KduZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0KtuZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0K9uZEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0LNuZEei1-oCUCrJoUg" name="MVHHT2" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0LduZEei1-oCUCrJoUg" value="MVHHT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0LtuZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0L9uZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0MNuZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0MduZEei1-oCUCrJoUg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0MtuZEei1-oCUCrJoUg" name="MVHIT2" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0M9uZEei1-oCUCrJoUg" value="MVHIT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0NNuZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0NduZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0NtuZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0N9uZEei1-oCUCrJoUg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_21g0ONuZEei1-oCUCrJoUg" name="MVHGT2" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_21g0OduZEei1-oCUCrJoUg" value="MVHGT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_21g0OtuZEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_21g0O9uZEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_21g0PNuZEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_21g0PduZEei1-oCUCrJoUg" value="76"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_k-IqUdwqEei1-oCUCrJoUg" name="ORBBREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_k-IqUtwqEei1-oCUCrJoUg" value="ORBBREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_k-IqU9wqEei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_k-R0QNwqEei1-oCUCrJoUg" value="ORBBREP   "/>
      <node defType="com.stambia.rdbms.column" id="_k_FskNwqEei1-oCUCrJoUg" name="BBAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_k_FskdwqEei1-oCUCrJoUg" value="BBAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_k_FsktwqEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_k_Fsk9wqEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_k_FslNwqEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_k_FsldwqEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_k_FsltwqEei1-oCUCrJoUg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_k_Fsl9wqEei1-oCUCrJoUg" name="BBA4CD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_k_FsmNwqEei1-oCUCrJoUg" value="BBA4CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_k_FsmdwqEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_k_FsmtwqEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_k_Fsm9wqEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_k_FsnNwqEei1-oCUCrJoUg" value="2"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_BQSRUdwrEei1-oCUCrJoUg" name="ORBDREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_BQSRUtwrEei1-oCUCrJoUg" value="ORBDREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_BQSRU9wrEei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_BQcCUNwrEei1-oCUCrJoUg" value="ORBDREP   "/>
      <node defType="com.stambia.rdbms.column" id="_BSM84NwrEei1-oCUCrJoUg" name="BDBCCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM84dwrEei1-oCUCrJoUg" value="BDBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM84twrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSM849wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM85NwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM85dwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM85twrEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM859wrEei1-oCUCrJoUg" name="BDSTST" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM86NwrEei1-oCUCrJoUg" value="BDSTST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM86dwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM86twrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM869wrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM87NwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM87dwrEei1-oCUCrJoUg" name="BDJXNA" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM87twrEei1-oCUCrJoUg" value="BDJXNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM879wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM88NwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM88dwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM88twrEei1-oCUCrJoUg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM889wrEei1-oCUCrJoUg" name="BDNPCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM89NwrEei1-oCUCrJoUg" value="BDNPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM89dwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM89twrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM899wrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM8-NwrEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM8-dwrEei1-oCUCrJoUg" name="BDNQCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM8-twrEei1-oCUCrJoUg" value="BDNQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM8-9wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM8_NwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM8_dwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM8_twrEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM8_9wrEei1-oCUCrJoUg" name="BDBSNA" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM9ANwrEei1-oCUCrJoUg" value="BDBSNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM9AdwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM9AtwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM9A9wrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM9BNwrEei1-oCUCrJoUg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM9BdwrEei1-oCUCrJoUg" name="BDBTNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM9BtwrEei1-oCUCrJoUg" value="BDBTNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM9B9wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM9CNwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM9CdwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM9CtwrEei1-oCUCrJoUg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM9C9wrEei1-oCUCrJoUg" name="BDA9TX" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM9DNwrEei1-oCUCrJoUg" value="BDA9TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM9DdwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM9DtwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM9D9wrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM9ENwrEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM9EdwrEei1-oCUCrJoUg" name="BDFZTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM9EtwrEei1-oCUCrJoUg" value="BDFZTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM9E9wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM9FNwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM9FdwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM9FtwrEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM9F9wrEei1-oCUCrJoUg" name="BDL2CD" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSM9GNwrEei1-oCUCrJoUg" value="BDL2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSM9GdwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSM9GtwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSM9G9wrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSM9HNwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSM9HdwrEei1-oCUCrJoUg" name="BDBHCD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSWG0NwrEei1-oCUCrJoUg" value="BDBHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSWG0dwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSWG0twrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSWG09wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSWG1NwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSWG1dwrEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSWG1twrEei1-oCUCrJoUg" name="BDAMCD" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSWG19wrEei1-oCUCrJoUg" value="BDAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSWG2NwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSWG2dwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSWG2twrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSWG29wrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSWG3NwrEei1-oCUCrJoUg" name="BDGHCD" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSWG3dwrEei1-oCUCrJoUg" value="BDGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSWG3twrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSWG39wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSWG4NwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSWG4dwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSWG4twrEei1-oCUCrJoUg" name="BDAQCD" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSWG49wrEei1-oCUCrJoUg" value="BDAQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSWG5NwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSWG5dwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSWG5twrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSWG59wrEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSWG6NwrEei1-oCUCrJoUg" name="BDAZCD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSWG6dwrEei1-oCUCrJoUg" value="BDAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSWG6twrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSWG69wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSWG7NwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSWG7dwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSWG7twrEei1-oCUCrJoUg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSWG79wrEei1-oCUCrJoUg" name="BDDNTX" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSWG8NwrEei1-oCUCrJoUg" value="BDDNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSWG8dwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSWG8twrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSWG89wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSWG9NwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSWG9dwrEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSWG9twrEei1-oCUCrJoUg" name="BDBJST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSWG99wrEei1-oCUCrJoUg" value="BDBJST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSWG-NwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYQNwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYQdwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYQtwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYQ9wrEei1-oCUCrJoUg" name="BDARDT" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYRNwrEei1-oCUCrJoUg" value="BDARDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYRdwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYRtwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYR9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYSNwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYSdwrEei1-oCUCrJoUg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYStwrEei1-oCUCrJoUg" name="BDASDT" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYS9wrEei1-oCUCrJoUg" value="BDASDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYTNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYTdwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYTtwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYT9wrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYUNwrEei1-oCUCrJoUg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYUdwrEei1-oCUCrJoUg" name="BDXCST" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYUtwrEei1-oCUCrJoUg" value="BDXCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYU9wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYVNwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYVdwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYVtwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYV9wrEei1-oCUCrJoUg" name="BDJBST" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYWNwrEei1-oCUCrJoUg" value="BDJBST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYWdwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYWtwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYW9wrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYXNwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYXdwrEei1-oCUCrJoUg" name="BDF0TX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYXtwrEei1-oCUCrJoUg" value="BDF0TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYX9wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYYNwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYYdwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYYtwrEei1-oCUCrJoUg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYY9wrEei1-oCUCrJoUg" name="BDHTNB" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYZNwrEei1-oCUCrJoUg" value="BDHTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYZdwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYZtwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYZ9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYaNwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYadwrEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYatwrEei1-oCUCrJoUg" name="BDAVDT" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYa9wrEei1-oCUCrJoUg" value="BDAVDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYbNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYbdwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYbtwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYb9wrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYcNwrEei1-oCUCrJoUg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYcdwrEei1-oCUCrJoUg" name="BDAWDT" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYctwrEei1-oCUCrJoUg" value="BDAWDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYc9wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYdNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYddwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYdtwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYd9wrEei1-oCUCrJoUg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYeNwrEei1-oCUCrJoUg" name="BDJCST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYedwrEei1-oCUCrJoUg" value="BDJCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYetwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYe9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYfNwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYfdwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYftwrEei1-oCUCrJoUg" name="BDJDST" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYf9wrEei1-oCUCrJoUg" value="BDJDST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYgNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYgdwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYgtwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYg9wrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYhNwrEei1-oCUCrJoUg" name="BDJEST" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYhdwrEei1-oCUCrJoUg" value="BDJEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYhtwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYh9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYiNwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYidwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYitwrEei1-oCUCrJoUg" name="BDDOPR" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYi9wrEei1-oCUCrJoUg" value="BDDOPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYjNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYjdwrEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYjtwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYj9wrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYkNwrEei1-oCUCrJoUg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYkdwrEei1-oCUCrJoUg" name="BDDPPR" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYktwrEei1-oCUCrJoUg" value="BDDPPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYk9wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYlNwrEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYldwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYltwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYl9wrEei1-oCUCrJoUg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYmNwrEei1-oCUCrJoUg" name="BDDQPR" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYmdwrEei1-oCUCrJoUg" value="BDDQPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYmtwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYm9wrEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYnNwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYndwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYntwrEei1-oCUCrJoUg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYn9wrEei1-oCUCrJoUg" name="BDF2NB" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYoNwrEei1-oCUCrJoUg" value="BDF2NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYodwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYotwrEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYo9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYpNwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYpdwrEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYptwrEei1-oCUCrJoUg" name="BDF3NB" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYp9wrEei1-oCUCrJoUg" value="BDF3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYqNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYqdwrEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYqtwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYq9wrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYrNwrEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYrdwrEei1-oCUCrJoUg" name="BDF4NB" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYrtwrEei1-oCUCrJoUg" value="BDF4NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYr9wrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_BSaYsNwrEei1-oCUCrJoUg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYsdwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYstwrEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYs9wrEei1-oCUCrJoUg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYtNwrEei1-oCUCrJoUg" name="BDTSST" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYtdwrEei1-oCUCrJoUg" value="BDTSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYttwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYt9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYuNwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYudwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYutwrEei1-oCUCrJoUg" name="BDC6N3" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYu9wrEei1-oCUCrJoUg" value="BDC6N3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYvNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYvdwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYvtwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYv9wrEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYwNwrEei1-oCUCrJoUg" name="BDC7N3" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYwdwrEei1-oCUCrJoUg" value="BDC7N3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYwtwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYw9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYxNwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYxdwrEei1-oCUCrJoUg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYxtwrEei1-oCUCrJoUg" name="BDKSTX" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYx9wrEei1-oCUCrJoUg" value="BDKSTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYyNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYydwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaYytwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaYy9wrEei1-oCUCrJoUg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaYzNwrEei1-oCUCrJoUg" name="BDKTTX" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaYzdwrEei1-oCUCrJoUg" value="BDKTTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaYztwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaYz9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaY0NwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaY0dwrEei1-oCUCrJoUg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaY0twrEei1-oCUCrJoUg" name="BDAKCD" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaY09wrEei1-oCUCrJoUg" value="BDAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaY1NwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaY1dwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaY1twrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaY19wrEei1-oCUCrJoUg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaY2NwrEei1-oCUCrJoUg" name="BDKVTX" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaY2dwrEei1-oCUCrJoUg" value="BDKVTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaY2twrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaY29wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaY3NwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaY3dwrEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaY3twrEei1-oCUCrJoUg" name="BDKWTX" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaY39wrEei1-oCUCrJoUg" value="BDKWTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaY4NwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaY4dwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaY4twrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaY49wrEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaY5NwrEei1-oCUCrJoUg" name="BDZUST" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaY5dwrEei1-oCUCrJoUg" value="BDZUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaY5twrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaY59wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaY6NwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaY6dwrEei1-oCUCrJoUg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSaY6twrEei1-oCUCrJoUg" name="BDWNTX" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSaY69wrEei1-oCUCrJoUg" value="BDWNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSaY7NwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSaY7dwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSaY7twrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSaY79wrEei1-oCUCrJoUg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSjiMNwrEei1-oCUCrJoUg" name="BDWOTX" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSjiMdwrEei1-oCUCrJoUg" value="BDWOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSjiMtwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSjiM9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSjiNNwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSjiNdwrEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSjiNtwrEei1-oCUCrJoUg" name="BDWPTX" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSjiN9wrEei1-oCUCrJoUg" value="BDWPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSjiONwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSjiOdwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSjiOtwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSjiO9wrEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSjiPNwrEei1-oCUCrJoUg" name="BDWQTX" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSjiPdwrEei1-oCUCrJoUg" value="BDWQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSjiPtwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSjiP9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSjiQNwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSjiQdwrEei1-oCUCrJoUg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSjiQtwrEei1-oCUCrJoUg" name="BDWRTX" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSjiQ9wrEei1-oCUCrJoUg" value="BDWRTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSjiRNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSjiRdwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSjiRtwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSjiR9wrEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSjiSNwrEei1-oCUCrJoUg" name="BDWSTX" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSjiSdwrEei1-oCUCrJoUg" value="BDWSTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSjiStwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSjiS9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSjiTNwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSjiTdwrEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSjiTtwrEei1-oCUCrJoUg" name="BDWTTX" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSjiT9wrEei1-oCUCrJoUg" value="BDWTTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSjiUNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSjiUdwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSjiUtwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSjiU9wrEei1-oCUCrJoUg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSjiVNwrEei1-oCUCrJoUg" name="BDWUTX" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSjiVdwrEei1-oCUCrJoUg" value="BDWUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSjiVtwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSjiV9wrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSjiWNwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSjiWdwrEei1-oCUCrJoUg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_BSjiWtwrEei1-oCUCrJoUg" name="BDWVTX" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_BSjiW9wrEei1-oCUCrJoUg" value="BDWVTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_BSjiXNwrEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_BSjiXdwrEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_BSjiXtwrEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_BSjiX9wrEei1-oCUCrJoUg" value="25"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_d2ez4dxmEei1-oCUCrJoUg" name="OSRGREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_d2ez4txmEei1-oCUCrJoUg" value="OSRGREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_d2ez49xmEei1-oCUCrJoUg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_d2n90NxmEei1-oCUCrJoUg" value="OSRGREP   "/>
      <node defType="com.stambia.rdbms.column" id="_d2xu0NxmEei1-oCUCrJoUg" name="RGBCCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_d2xu0dxmEei1-oCUCrJoUg" value="RGBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_d2xu0txmEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_d2xu09xmEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_d2xu1NxmEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_d2xu1dxmEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_d2xu1txmEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_d2xu19xmEei1-oCUCrJoUg" name="RGTYDT" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_d2xu2NxmEei1-oCUCrJoUg" value="RGTYDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_d2xu2dxmEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_d2xu2txmEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_d2xu29xmEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_d2xu3NxmEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_d2xu3dxmEei1-oCUCrJoUg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_d2xu3txmEei1-oCUCrJoUg" name="RGTZDT" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_d2xu39xmEei1-oCUCrJoUg" value="RGTZDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_d2xu4NxmEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_d2xu4dxmEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_d2xu4txmEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_d2xu49xmEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_d2xu5NxmEei1-oCUCrJoUg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_d2xu5dxmEei1-oCUCrJoUg" name="RGF7N6" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_d2xu5txmEei1-oCUCrJoUg" value="RGF7N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_d2xu59xmEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_d2xu6NxmEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_d2xu6dxmEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_d2xu6txmEei1-oCUCrJoUg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_d2xu69xmEei1-oCUCrJoUg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_d2xu7NxmEei1-oCUCrJoUg" name="RGMDN3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_d2xu7dxmEei1-oCUCrJoUg" value="RGMDN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_d2xu7txmEei1-oCUCrJoUg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_d2xu79xmEei1-oCUCrJoUg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_d2xu8NxmEei1-oCUCrJoUg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_d2xu8dxmEei1-oCUCrJoUg" value="35"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_XoS00edSEeicCszix7eWDA" name="ORGQREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_XoS00udSEeicCszix7eWDA" value="ORGQREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_XoS00-dSEeicCszix7eWDA" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_XoS01OdSEeicCszix7eWDA" value="ORGQREP   "/>
      <node defType="com.stambia.rdbms.column" id="_XomW0OdSEeicCszix7eWDA" name="GQFWNB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomW0edSEeicCszix7eWDA" value="GQFWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomW0udSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XomW0-dSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomW1OdSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomW1edSEeicCszix7eWDA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomW1udSEeicCszix7eWDA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomW1-dSEeicCszix7eWDA" name="GQAKCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomW2OdSEeicCszix7eWDA" value="GQAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomW2edSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomW2udSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomW2-dSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomW3OdSEeicCszix7eWDA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomW3edSEeicCszix7eWDA" name="GQBDCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomW3udSEeicCszix7eWDA" value="GQBDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomW3-dSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomW4OdSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomW4edSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomW4udSEeicCszix7eWDA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomW4-dSEeicCszix7eWDA" name="GQFOTX" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomW5OdSEeicCszix7eWDA" value="GQFOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomW5edSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomW5udSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomW5-dSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomW6OdSEeicCszix7eWDA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomW6edSEeicCszix7eWDA" name="GQFQTX" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomW6udSEeicCszix7eWDA" value="GQFQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomW6-dSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomW7OdSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomW7edSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomW7udSEeicCszix7eWDA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomW7-dSEeicCszix7eWDA" name="GQFRTX" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomW8OdSEeicCszix7eWDA" value="GQFRTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomW8edSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomW8udSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomW8-dSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomW9OdSEeicCszix7eWDA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomW9edSEeicCszix7eWDA" name="GQHUNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomW9udSEeicCszix7eWDA" value="GQHUNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomW9-dSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomW-OdSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomW-edSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomW-udSEeicCszix7eWDA" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomW--dSEeicCszix7eWDA" name="GQOVST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomW_OdSEeicCszix7eWDA" value="GQOVST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomW_edSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomW_udSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomW_-dSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXAOdSEeicCszix7eWDA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXAedSEeicCszix7eWDA" name="GQDQN3" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXAudSEeicCszix7eWDA" value="GQDQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXA-dSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXBOdSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXBedSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXBudSEeicCszix7eWDA" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXB-dSEeicCszix7eWDA" name="GQDRN3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXCOdSEeicCszix7eWDA" value="GQDRN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXCedSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXCudSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXC-dSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXDOdSEeicCszix7eWDA" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXDedSEeicCszix7eWDA" name="GQKINA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXDudSEeicCszix7eWDA" value="GQKINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXD-dSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXEOdSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXEedSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXEudSEeicCszix7eWDA" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXE-dSEeicCszix7eWDA" name="GQKJNA" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXFOdSEeicCszix7eWDA" value="GQKJNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXFedSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXFudSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXF-dSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXGOdSEeicCszix7eWDA" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXGedSEeicCszix7eWDA" name="GQMKTX" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXGudSEeicCszix7eWDA" value="GQMKTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXG-dSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXHOdSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXHedSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXHudSEeicCszix7eWDA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXH-dSEeicCszix7eWDA" name="GQMLTX" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXIOdSEeicCszix7eWDA" value="GQMLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXIedSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXIudSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXI-dSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXJOdSEeicCszix7eWDA" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXJedSEeicCszix7eWDA" name="GQMMTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXJudSEeicCszix7eWDA" value="GQMMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXJ-dSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXKOdSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXKedSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXKudSEeicCszix7eWDA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXK-dSEeicCszix7eWDA" name="GQMNTX" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXLOdSEeicCszix7eWDA" value="GQMNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXLedSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXLudSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXL-dSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXMOdSEeicCszix7eWDA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXMedSEeicCszix7eWDA" name="GQN8CD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXMudSEeicCszix7eWDA" value="GQN8CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXM-dSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXNOdSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXNedSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXNudSEeicCszix7eWDA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XomXN-dSEeicCszix7eWDA" name="GQN9CD" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_XomXOOdSEeicCszix7eWDA" value="GQN9CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XomXOedSEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XomXOudSEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XomXO-dSEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XomXPOdSEeicCszix7eWDA" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_dhm8EegXEeicCszix7eWDA" name="ORBQREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_dhm8EugXEeicCszix7eWDA" value="ORBQREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_dhm8E-gXEeicCszix7eWDA" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_dhm8FOgXEeicCszix7eWDA" value="ORBQREP   "/>
      <node defType="com.stambia.rdbms.column" id="_dhwGAOgXEeicCszix7eWDA" name="BQBOCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGAegXEeicCszix7eWDA" value="BQBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGAugXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGA-gXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGBOgXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGBegXEeicCszix7eWDA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGBugXEeicCszix7eWDA" name="BQHRST" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGB-gXEeicCszix7eWDA" value="BQHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGCOgXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGCegXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGCugXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGC-gXEeicCszix7eWDA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGDOgXEeicCszix7eWDA" name="BQAFCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGDegXEeicCszix7eWDA" value="BQAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGDugXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGD-gXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGEOgXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGEegXEeicCszix7eWDA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGEugXEeicCszix7eWDA" name="BQAMCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGE-gXEeicCszix7eWDA" value="BQAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGFOgXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGFegXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGFugXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGF-gXEeicCszix7eWDA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGGOgXEeicCszix7eWDA" name="BQGHCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGGegXEeicCszix7eWDA" value="BQGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGGugXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGG-gXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGHOgXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGHegXEeicCszix7eWDA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGHugXEeicCszix7eWDA" name="BQJ4CD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGH-gXEeicCszix7eWDA" value="BQJ4CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGIOgXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGIegXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGIugXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGI-gXEeicCszix7eWDA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGJOgXEeicCszix7eWDA" name="BQBYNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGJegXEeicCszix7eWDA" value="BQBYNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGJugXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGJ-gXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGKOgXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGKegXEeicCszix7eWDA" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGKugXEeicCszix7eWDA" name="BQAZDT" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGK-gXEeicCszix7eWDA" value="BQAZDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGLOgXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_dhwGLegXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGLugXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGL-gXEeicCszix7eWDA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGMOgXEeicCszix7eWDA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGMegXEeicCszix7eWDA" name="BQA0DT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGMugXEeicCszix7eWDA" value="BQA0DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGM-gXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_dhwGNOgXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGNegXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGNugXEeicCszix7eWDA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGN-gXEeicCszix7eWDA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGOOgXEeicCszix7eWDA" name="BQBQST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGOegXEeicCszix7eWDA" value="BQBQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGOugXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGO-gXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGPOgXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGPegXEeicCszix7eWDA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGPugXEeicCszix7eWDA" name="BQBRST" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGP-gXEeicCszix7eWDA" value="BQBRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGQOgXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGQegXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGQugXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGQ-gXEeicCszix7eWDA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGROgXEeicCszix7eWDA" name="BQAEQT" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGRegXEeicCszix7eWDA" value="BQAEQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGRugXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_dhwGR-gXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGSOgXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGSegXEeicCszix7eWDA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGSugXEeicCszix7eWDA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dhwGS-gXEeicCszix7eWDA" name="BQDQDT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_dhwGTOgXEeicCszix7eWDA" value="BQDQDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dhwGTegXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_dhwGTugXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dhwGT-gXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dhwGUOgXEeicCszix7eWDA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dhwGUegXEeicCszix7eWDA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dh53AOgXEeicCszix7eWDA" name="BQDRDT" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_dh53AegXEeicCszix7eWDA" value="BQDRDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dh53AugXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_dh53A-gXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dh53BOgXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dh53BegXEeicCszix7eWDA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dh53BugXEeicCszix7eWDA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dh53B-gXEeicCszix7eWDA" name="BQO1ST" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_dh53COgXEeicCszix7eWDA" value="BQO1ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dh53CegXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dh53CugXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dh53C-gXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dh53DOgXEeicCszix7eWDA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dh53DegXEeicCszix7eWDA" name="BQD3C1" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_dh53DugXEeicCszix7eWDA" value="BQD3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dh53D-gXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dh53EOgXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dh53EegXEeicCszix7eWDA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dh53EugXEeicCszix7eWDA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_dh53E-gXEeicCszix7eWDA" name="BQBCCD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_dh53FOgXEeicCszix7eWDA" value="BQBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_dh53FegXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_dh53FugXEeicCszix7eWDA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_dh53F-gXEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_dh53GOgXEeicCszix7eWDA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_dh53GegXEeicCszix7eWDA" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_Evzy8O2UEeihWNvRK7oOPw" name="ORAGREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_Evzy8e2UEeihWNvRK7oOPw" value="ORAGREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Evzy8u2UEeihWNvRK7oOPw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_Ev3dUO2UEeihWNvRK7oOPw" value="ORAGREP   "/>
      <node defType="com.stambia.rdbms.column" id="_Ev_ZIO2UEeihWNvRK7oOPw" name="AGAGCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ev_ZIe2UEeihWNvRK7oOPw" value="AGAGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ev_ZIu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ev_ZI-2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ev_ZJO2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ev_ZJe2UEeihWNvRK7oOPw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ev_ZJu2UEeihWNvRK7oOPw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwAAMO2UEeihWNvRK7oOPw" name="AGAKCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwAAMe2UEeihWNvRK7oOPw" value="AGAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwAAMu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwAAM-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwAANO2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwAANe2UEeihWNvRK7oOPw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwAANu2UEeihWNvRK7oOPw" name="AGAMCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwAAN-2UEeihWNvRK7oOPw" value="AGAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwAAOO2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwAAOe2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwAAOu2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwAAO-2UEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwAAPO2UEeihWNvRK7oOPw" name="AGF2CD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwAnQO2UEeihWNvRK7oOPw" value="AGF2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwAnQe2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_EwAnQu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwAnQ-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwAnRO2UEeihWNvRK7oOPw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwAnRe2UEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwAnRu2UEeihWNvRK7oOPw" name="AGACTX" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwAnR-2UEeihWNvRK7oOPw" value="AGACTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwAnSO2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwAnSe2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwAnSu2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwAnS-2UEeihWNvRK7oOPw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwAnTO2UEeihWNvRK7oOPw" name="AGABST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwAnTe2UEeihWNvRK7oOPw" value="AGABST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwAnTu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwAnT-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwAnUO2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwAnUe2UEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwBOUO2UEeihWNvRK7oOPw" name="AGEJTX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwBOUe2UEeihWNvRK7oOPw" value="AGEJTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwBOUu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwBOU-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwBOVO2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwBOVe2UEeihWNvRK7oOPw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwBOVu2UEeihWNvRK7oOPw" name="AGLLST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwBOV-2UEeihWNvRK7oOPw" value="AGLLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwBOWO2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwBOWe2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwBOWu2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwBOW-2UEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwBOXO2UEeihWNvRK7oOPw" name="AGBUN3" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwBOXe2UEeihWNvRK7oOPw" value="AGBUN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwBOXu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwBOX-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwBOYO2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwBOYe2UEeihWNvRK7oOPw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwB1YO2UEeihWNvRK7oOPw" name="AGBVN3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwB1Ye2UEeihWNvRK7oOPw" value="AGBVN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwB1Yu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwB1Y-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwB1ZO2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwB1Ze2UEeihWNvRK7oOPw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwB1Zu2UEeihWNvRK7oOPw" name="AGIUNA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwB1Z-2UEeihWNvRK7oOPw" value="AGIUNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwB1aO2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwB1ae2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwB1au2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwB1a-2UEeihWNvRK7oOPw" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwB1bO2UEeihWNvRK7oOPw" name="AGHVTX" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwB1be2UEeihWNvRK7oOPw" value="AGHVTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwB1bu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwB1b-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwB1cO2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwB1ce2UEeihWNvRK7oOPw" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwB1cu2UEeihWNvRK7oOPw" name="AGHWTX" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwB1c-2UEeihWNvRK7oOPw" value="AGHWTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwB1dO2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwB1de2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwB1du2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwB1d-2UEeihWNvRK7oOPw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwCccO2UEeihWNvRK7oOPw" name="AGHXTX" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwCcce2UEeihWNvRK7oOPw" value="AGHXTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwCccu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwCcc-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwCcdO2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwCcde2UEeihWNvRK7oOPw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwCcdu2UEeihWNvRK7oOPw" name="AGHYTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwCcd-2UEeihWNvRK7oOPw" value="AGHYTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwCceO2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwCcee2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwCceu2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwCce-2UEeihWNvRK7oOPw" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwCcfO2UEeihWNvRK7oOPw" name="AGNZCD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwCcfe2UEeihWNvRK7oOPw" value="AGNZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwCcfu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwCcf-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwCcgO2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwCcge2UEeihWNvRK7oOPw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_EwDDgO2UEeihWNvRK7oOPw" name="AGN0CD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_EwDDge2UEeihWNvRK7oOPw" value="AGN0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_EwDDgu2UEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_EwDDg-2UEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_EwDDhO2UEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_EwDDhe2UEeihWNvRK7oOPw" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_orwnce2VEeihWNvRK7oOPw" name="ORGPREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_orwncu2VEeihWNvRK7oOPw" value="ORGPREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_orwnc-2VEeihWNvRK7oOPw" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_orycoO2VEeihWNvRK7oOPw" value="ORGPREP   "/>
      <node defType="com.stambia.rdbms.column" id="_or6YcO2VEeihWNvRK7oOPw" name="GPFTNB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_or6Yce2VEeihWNvRK7oOPw" value="GPFTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or6Ycu2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_or6Yc-2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or6YdO2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or6Yde2VEeihWNvRK7oOPw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or6Ydu2VEeihWNvRK7oOPw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or6Yd-2VEeihWNvRK7oOPw" name="GPAFCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_or6YeO2VEeihWNvRK7oOPw" value="GPAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or6Yee2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or6Yeu2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or6Ye-2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or6YfO2VEeihWNvRK7oOPw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or6_gO2VEeihWNvRK7oOPw" name="GPAGCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_or6_ge2VEeihWNvRK7oOPw" value="GPAGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or6_gu2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_or6_g-2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or6_hO2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or6_he2VEeihWNvRK7oOPw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or6_hu2VEeihWNvRK7oOPw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or6_h-2VEeihWNvRK7oOPw" name="GPAKCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_or6_iO2VEeihWNvRK7oOPw" value="GPAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or6_ie2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or6_iu2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or6_i-2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or6_jO2VEeihWNvRK7oOPw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or6_je2VEeihWNvRK7oOPw" name="GPAMCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_or6_ju2VEeihWNvRK7oOPw" value="GPAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or6_j-2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or6_kO2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or6_ke2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or6_ku2VEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or7mkO2VEeihWNvRK7oOPw" name="GPBCCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_or7mke2VEeihWNvRK7oOPw" value="GPBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or7mku2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_or7mk-2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or7mlO2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or7mle2VEeihWNvRK7oOPw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or7mlu2VEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or7ml-2VEeihWNvRK7oOPw" name="GPBOCD" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_or7mmO2VEeihWNvRK7oOPw" value="GPBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or7mme2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or7mmu2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or7mm-2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or7mnO2VEeihWNvRK7oOPw" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or7mne2VEeihWNvRK7oOPw" name="GPHRST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_or7mnu2VEeihWNvRK7oOPw" value="GPHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or7mn-2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or7moO2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or7moe2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or7mou2VEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or8NoO2VEeihWNvRK7oOPw" name="GPFJTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_or8Noe2VEeihWNvRK7oOPw" value="GPFJTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or8Nou2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or8No-2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or8NpO2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or8Npe2VEeihWNvRK7oOPw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or8Npu2VEeihWNvRK7oOPw" name="GPFKTX" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_or8Np-2VEeihWNvRK7oOPw" value="GPFKTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or8NqO2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or8Nqe2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or8Nqu2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or8Nq-2VEeihWNvRK7oOPw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or8NrO2VEeihWNvRK7oOPw" name="GPBLPC" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_or8Nre2VEeihWNvRK7oOPw" value="GPBLPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or8Nru2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_or8Nr-2VEeihWNvRK7oOPw" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or8NsO2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or8Nse2VEeihWNvRK7oOPw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or8Nsu2VEeihWNvRK7oOPw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or80sO2VEeihWNvRK7oOPw" name="GPBMPC" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_or80se2VEeihWNvRK7oOPw" value="GPBMPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or80su2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_or80s-2VEeihWNvRK7oOPw" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or80tO2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or80te2VEeihWNvRK7oOPw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or80tu2VEeihWNvRK7oOPw" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or80t-2VEeihWNvRK7oOPw" name="GPITST" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_or80uO2VEeihWNvRK7oOPw" value="GPITST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or80ue2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or80uu2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or80u-2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or80vO2VEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or80ve2VEeihWNvRK7oOPw" name="GPIUST" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_or80vu2VEeihWNvRK7oOPw" value="GPIUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or80v-2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or80wO2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or80we2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or80wu2VEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or9bwO2VEeihWNvRK7oOPw" name="GPIVST" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_or9bwe2VEeihWNvRK7oOPw" value="GPIVST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or9bwu2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or9bw-2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or9bxO2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or9bxe2VEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or9bxu2VEeihWNvRK7oOPw" name="GPEIDT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_or9bx-2VEeihWNvRK7oOPw" value="GPEIDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or9byO2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_or9bye2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or9byu2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or9by-2VEeihWNvRK7oOPw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or9bzO2VEeihWNvRK7oOPw" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or9bze2VEeihWNvRK7oOPw" name="GPLKST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_or9bzu2VEeihWNvRK7oOPw" value="GPLKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or9bz-2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or9b0O2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or9b0e2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or9b0u2VEeihWNvRK7oOPw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or-C0O2VEeihWNvRK7oOPw" name="GPBSN3" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_or-C0e2VEeihWNvRK7oOPw" value="GPBSN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or-C0u2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or-C0-2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or-C1O2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or-C1e2VEeihWNvRK7oOPw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or-C1u2VEeihWNvRK7oOPw" name="GPBTN3" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_or-C1-2VEeihWNvRK7oOPw" value="GPBTN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or-C2O2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or-C2e2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or-C2u2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or-C2-2VEeihWNvRK7oOPw" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or-C3O2VEeihWNvRK7oOPw" name="GPHRTX" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_or-C3e2VEeihWNvRK7oOPw" value="GPHRTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or-C3u2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or-C3-2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or-C4O2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or-C4e2VEeihWNvRK7oOPw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or-p4O2VEeihWNvRK7oOPw" name="GPHSTX" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_or-p4e2VEeihWNvRK7oOPw" value="GPHSTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or-p4u2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or-p4-2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or-p5O2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or-p5e2VEeihWNvRK7oOPw" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or-p5u2VEeihWNvRK7oOPw" name="GPITNA" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_or-p5-2VEeihWNvRK7oOPw" value="GPITNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or-p6O2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or-p6e2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or-p6u2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or-p6-2VEeihWNvRK7oOPw" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or-p7O2VEeihWNvRK7oOPw" name="GPHTTX" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_or-p7e2VEeihWNvRK7oOPw" value="GPHTTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or-p7u2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or_Q8O2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or_Q8e2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or_Q8u2VEeihWNvRK7oOPw" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or_Q8-2VEeihWNvRK7oOPw" name="GPHUTX" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_or_Q9O2VEeihWNvRK7oOPw" value="GPHUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or_Q9e2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or_Q9u2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or_Q9-2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or_Q-O2VEeihWNvRK7oOPw" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or_Q-e2VEeihWNvRK7oOPw" name="GPNXCD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_or_Q-u2VEeihWNvRK7oOPw" value="GPNXCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or_Q--2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or_Q_O2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or_Q_e2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or_Q_u2VEeihWNvRK7oOPw" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or_4AO2VEeihWNvRK7oOPw" name="GPNYCD" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_or_4Ae2VEeihWNvRK7oOPw" value="GPNYCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or_4Au2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or_4A-2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or_4BO2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or_4Be2VEeihWNvRK7oOPw" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or_4Bu2VEeihWNvRK7oOPw" name="GPAADT" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_or_4B-2VEeihWNvRK7oOPw" value="GPAADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or_4CO2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_or_4Ce2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or_4Cu2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or_4C-2VEeihWNvRK7oOPw" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or_4DO2VEeihWNvRK7oOPw" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_or_4De2VEeihWNvRK7oOPw" name="GPAATX" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_or_4Du2VEeihWNvRK7oOPw" value="GPAATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_or_4D-2VEeihWNvRK7oOPw" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_or_4EO2VEeihWNvRK7oOPw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_or_4Ee2VEeihWNvRK7oOPw" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_or_4Eu2VEeihWNvRK7oOPw" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_0VHpUfJNEeiaFfxi6Zg4Ug" name="PIX_CLIENT_1ERE_COMMANDE">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_0VHpUvJNEeiaFfxi6Zg4Ug" value="PIX_CLIENT_1ERE_COMMANDE"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_0VHpU_JNEeiaFfxi6Zg4Ug" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_0VHpVPJNEeiaFfxi6Zg4Ug" value="PXCL1CDP  "/>
      <node defType="com.stambia.rdbms.column" id="_0a3E8PJNEeiaFfxi6Zg4Ug" name="P1CCLT" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_0a3E8fJNEeiaFfxi6Zg4Ug" value="P1CCLT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0a3E8vJNEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_0a3E8_JNEeiaFfxi6Zg4Ug" value="Code client"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0bAO4PJNEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0bAO4fJNEeiaFfxi6Zg4Ug" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0bAO4vJNEeiaFfxi6Zg4Ug" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0bAO4_JNEeiaFfxi6Zg4Ug" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0bAO5PJNEeiaFfxi6Zg4Ug" name="P1CDTE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_0bAO5fJNEeiaFfxi6Zg4Ug" value="P1CDTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0bAO5vJNEeiaFfxi6Zg4Ug" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_0bAO5_JNEeiaFfxi6Zg4Ug" value="Date 1ere commande"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0bAO6PJNEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0bAO6fJNEeiaFfxi6Zg4Ug" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0bAO6vJNEeiaFfxi6Zg4Ug" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0bAO6_JNEeiaFfxi6Zg4Ug" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0bAO7PJNEeiaFfxi6Zg4Ug" name="P1CCANAL" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_0bAO7fJNEeiaFfxi6Zg4Ug" value="P1CCANAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0bAO7vJNEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_0bAO7_JNEeiaFfxi6Zg4Ug" value="Mode de Passation 1ere Commande"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0bAO8PJNEeiaFfxi6Zg4Ug" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0bAO8fJNEeiaFfxi6Zg4Ug" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0bAO8vJNEeiaFfxi6Zg4Ug" value="15"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_7i8qgfJkEeiaFfxi6Zg4Ug" name="OTG9CPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_7i8qgvJkEeiaFfxi6Zg4Ug" value="OTG9CPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_7i8qg_JkEeiaFfxi6Zg4Ug" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_7i8qhPJkEeiaFfxi6Zg4Ug" value="OTG9CPP   "/>
      <node defType="com.stambia.rdbms.column" id="_7jF0cPJkEeiaFfxi6Zg4Ug" name="G9F2CD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jF0cfJkEeiaFfxi6Zg4Ug" value="G9F2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jF0cvJkEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jF0c_JkEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jF0dPJkEeiaFfxi6Zg4Ug" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jF0dfJkEeiaFfxi6Zg4Ug" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jF0dvJkEeiaFfxi6Zg4Ug" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jF0d_JkEeiaFfxi6Zg4Ug" name="G9AGCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jF0ePJkEeiaFfxi6Zg4Ug" value="G9AGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jF0efJkEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jF0evJkEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jF0e_JkEeiaFfxi6Zg4Ug" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jF0fPJkEeiaFfxi6Zg4Ug" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jF0ffJkEeiaFfxi6Zg4Ug" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jF0fvJkEeiaFfxi6Zg4Ug" name="G9FTNB" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jF0f_JkEeiaFfxi6Zg4Ug" value="G9FTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jF0gPJkEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jF0gfJkEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jF0gvJkEeiaFfxi6Zg4Ug" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jF0g_JkEeiaFfxi6Zg4Ug" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jF0hPJkEeiaFfxi6Zg4Ug" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_7jF0hfJkEeiaFfxi6Zg4Ug" name="G9AZCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_7jF0hvJkEeiaFfxi6Zg4Ug" value="G9AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_7jF0h_JkEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_7jF0iPJkEeiaFfxi6Zg4Ug" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_7jF0ifJkEeiaFfxi6Zg4Ug" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_7jF0ivJkEeiaFfxi6Zg4Ug" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_7jF0i_JkEeiaFfxi6Zg4Ug" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_TD1kgfoREeiL3uw9NeuELg" name="OSLZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_TD1kgvoREeiL3uw9NeuELg" value="OSLZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_TD1kg_oREeiL3uw9NeuELg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_TD1khPoREeiL3uw9NeuELg" value="OSLZREP   "/>
      <node defType="com.stambia.rdbms.column" id="_TD1khfoREeiL3uw9NeuELg" name="LZJKC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_TD1khvoREeiL3uw9NeuELg" value="LZJKC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TD1kh_oREeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TD1kiPoREeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TD1kifoREeiL3uw9NeuELg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TD1kivoREeiL3uw9NeuELg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TD_VgPoREeiL3uw9NeuELg" name="LZJLC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_TD_VgfoREeiL3uw9NeuELg" value="LZJLC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TD_VgvoREeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TD_Vg_oREeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TD_VhPoREeiL3uw9NeuELg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TD_VhfoREeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TD_VhvoREeiL3uw9NeuELg" name="LZXGN5" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_TD_Vh_oREeiL3uw9NeuELg" value="LZXGN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TD_ViPoREeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_TD_VifoREeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TD_VivoREeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TD_Vi_oREeiL3uw9NeuELg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TD_VjPoREeiL3uw9NeuELg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TD_VjfoREeiL3uw9NeuELg" name="LZWUT1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_TD_VjvoREeiL3uw9NeuELg" value="LZWUT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TD_Vj_oREeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TD_VkPoREeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TD_VkfoREeiL3uw9NeuELg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TD_VkvoREeiL3uw9NeuELg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_TD_Vk_oREeiL3uw9NeuELg" name="LZWVT1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_TD_VlPoREeiL3uw9NeuELg" value="LZWVT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_TD_VlfoREeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_TD_VlvoREeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_TD_Vl_oREeiL3uw9NeuELg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_TD_VmPoREeiL3uw9NeuELg" value="70"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_9UpEMf0xEeihcqubBYu_Xg" name="WEBCLI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_9UpEMv0xEeihcqubBYu_Xg" value="WEBCLI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_9UpEM_0xEeihcqubBYu_Xg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_9Uy1MP0xEeihcqubBYu_Xg" value="WEBCLI    "/>
      <node defType="com.stambia.rdbms.column" id="_9VFwIP0xEeihcqubBYu_Xg" name="IDWEBC" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VFwIf0xEeihcqubBYu_Xg" value="IDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VFwIv0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VFwI_0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VFwJP0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VFwJf0xEeihcqubBYu_Xg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6EP0xEeihcqubBYu_Xg" name="CPTCLI" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6Ef0xEeihcqubBYu_Xg" value="CPTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6Ev0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO6E_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6FP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6Ff0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6Fv0xEeihcqubBYu_Xg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6F_0xEeihcqubBYu_Xg" name="NOMCLI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6GP0xEeihcqubBYu_Xg" value="NOMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6Gf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6Gv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6G_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6HP0xEeihcqubBYu_Xg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6Hf0xEeihcqubBYu_Xg" name="PRECLI" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6Hv0xEeihcqubBYu_Xg" value="PRECLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6H_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6IP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6If0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6Iv0xEeihcqubBYu_Xg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6I_0xEeihcqubBYu_Xg" name="PWDCLI" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6JP0xEeihcqubBYu_Xg" value="PWDCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6Jf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6Jv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6J_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6KP0xEeihcqubBYu_Xg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6Kf0xEeihcqubBYu_Xg" name="MELADR" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6Kv0xEeihcqubBYu_Xg" value="MELADR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6K_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6LP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6Lf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6Lv0xEeihcqubBYu_Xg" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6L_0xEeihcqubBYu_Xg" name="MELSIT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6MP0xEeihcqubBYu_Xg" value="MELSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6Mf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6Mv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6M_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6NP0xEeihcqubBYu_Xg" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6Nf0xEeihcqubBYu_Xg" name="NIVCLI" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6Nv0xEeihcqubBYu_Xg" value="NIVCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6N_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6OP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6Of0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6Ov0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6O_0xEeihcqubBYu_Xg" name="ACTCLI" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6PP0xEeihcqubBYu_Xg" value="ACTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6Pf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6Pv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6P_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6QP0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6Qf0xEeihcqubBYu_Xg" name="TYPTAR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6Qv0xEeihcqubBYu_Xg" value="TYPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6Q_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6RP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6Rf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6Rv0xEeihcqubBYu_Xg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6R_0xEeihcqubBYu_Xg" name="TXREMI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6SP0xEeihcqubBYu_Xg" value="TXREMI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6Sf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO6Sv0xEeihcqubBYu_Xg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6S_0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6TP0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6Tf0xEeihcqubBYu_Xg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6Tv0xEeihcqubBYu_Xg" name="FRANCO" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6T_0xEeihcqubBYu_Xg" value="FRANCO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6UP0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO6Uf0xEeihcqubBYu_Xg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6Uv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6U_0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6VP0xEeihcqubBYu_Xg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6Vf0xEeihcqubBYu_Xg" name="FRPORT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6Vv0xEeihcqubBYu_Xg" value="FRPORT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6V_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO6WP0xEeihcqubBYu_Xg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6Wf0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6Wv0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6W_0xEeihcqubBYu_Xg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6XP0xEeihcqubBYu_Xg" name="MINCDE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6Xf0xEeihcqubBYu_Xg" value="MINCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6Xv0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO6X_0xEeihcqubBYu_Xg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6YP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6Yf0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6Yv0xEeihcqubBYu_Xg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6Y_0xEeihcqubBYu_Xg" name="MERCUR" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6ZP0xEeihcqubBYu_Xg" value="MERCUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6Zf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6Zv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6Z_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6aP0xEeihcqubBYu_Xg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6af0xEeihcqubBYu_Xg" name="CDECAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6av0xEeihcqubBYu_Xg" value="CDECAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6a_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6bP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6bf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6bv0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6b_0xEeihcqubBYu_Xg" name="CDELST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6cP0xEeihcqubBYu_Xg" value="CDELST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6cf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6cv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6c_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6dP0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6df0xEeihcqubBYu_Xg" name="CDEMER" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6dv0xEeihcqubBYu_Xg" value="CDEMER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6d_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6eP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6ef0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6ev0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6e_0xEeihcqubBYu_Xg" name="CDEFAV" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6fP0xEeihcqubBYu_Xg" value="CDEFAV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6ff0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6fv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6f_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6gP0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6gf0xEeihcqubBYu_Xg" name="TARCLI" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6gv0xEeihcqubBYu_Xg" value="TARCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6g_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6hP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6hf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6hv0xEeihcqubBYu_Xg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6h_0xEeihcqubBYu_Xg" name="UNIVCL" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6iP0xEeihcqubBYu_Xg" value="UNIVCL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6if0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6iv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6i_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6jP0xEeihcqubBYu_Xg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6jf0xEeihcqubBYu_Xg" name="REMISE" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6jv0xEeihcqubBYu_Xg" value="REMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6j_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO6kP0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6kf0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6kv0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6k_0xEeihcqubBYu_Xg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6lP0xEeihcqubBYu_Xg" name="IMPTAR" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6lf0xEeihcqubBYu_Xg" value="IMPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6lv0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6l_0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6mP0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6mf0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6mv0xEeihcqubBYu_Xg" name="CENTRA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6m_0xEeihcqubBYu_Xg" value="CENTRA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6nP0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO6nf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6nv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6n_0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6oP0xEeihcqubBYu_Xg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6of0xEeihcqubBYu_Xg" name="MAILVRP" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6ov0xEeihcqubBYu_Xg" value="MAILVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6o_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6pP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6pf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6pv0xEeihcqubBYu_Xg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6p_0xEeihcqubBYu_Xg" name="CLIMAIL" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6qP0xEeihcqubBYu_Xg" value="CLIMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6qf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6qv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6q_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6rP0xEeihcqubBYu_Xg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6rf0xEeihcqubBYu_Xg" name="SPECIAL" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6rv0xEeihcqubBYu_Xg" value="SPECIAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6r_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6sP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6sf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6sv0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6s_0xEeihcqubBYu_Xg" name="CODSOC" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6tP0xEeihcqubBYu_Xg" value="CODSOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6tf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6tv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6t_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6uP0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6uf0xEeihcqubBYu_Xg" name="TELVRP" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6uv0xEeihcqubBYu_Xg" value="TELVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6u_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6vP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6vf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6vv0xEeihcqubBYu_Xg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6v_0xEeihcqubBYu_Xg" name="NOMVRP" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6wP0xEeihcqubBYu_Xg" value="NOMVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6wf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6wv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6w_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6xP0xEeihcqubBYu_Xg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6xf0xEeihcqubBYu_Xg" name="LNGCLI" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6xv0xEeihcqubBYu_Xg" value="LNGCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6x_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6yP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6yf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6yv0xEeihcqubBYu_Xg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6y_0xEeihcqubBYu_Xg" name="PWDMD5" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6zP0xEeihcqubBYu_Xg" value="PWDMD5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6zf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6zv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6z_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO60P0xEeihcqubBYu_Xg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO60f0xEeihcqubBYu_Xg" name="NOMCOM" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO60v0xEeihcqubBYu_Xg" value="NOMCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO60_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO61P0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO61f0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO61v0xEeihcqubBYu_Xg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO61_0xEeihcqubBYu_Xg" name="MELCOM" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO62P0xEeihcqubBYu_Xg" value="MELCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO62f0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO62v0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO62_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO63P0xEeihcqubBYu_Xg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO63f0xEeihcqubBYu_Xg" name="TOPCRE" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO63v0xEeihcqubBYu_Xg" value="TOPCRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO63_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO64P0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO64f0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO64v0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO64_0xEeihcqubBYu_Xg" name="TOPPROMO" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO65P0xEeihcqubBYu_Xg" value="TOPPROMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO65f0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO65v0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO65_0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO66P0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO66f0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO66v0xEeihcqubBYu_Xg" name="TOPMERCURIALE" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO66_0xEeihcqubBYu_Xg" value="TOPMERCURIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO67P0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO67f0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO67v0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO67_0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO68P0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO68f0xEeihcqubBYu_Xg" name="TOPKIT" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO68v0xEeihcqubBYu_Xg" value="TOPKIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO68_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO69P0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO69f0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO69v0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO69_0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6-P0xEeihcqubBYu_Xg" name="TOPPRINCIPAL" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO6-f0xEeihcqubBYu_Xg" value="TOPPRINCIPAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO6-v0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO6-_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO6_P0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO6_f0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO6_v0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO6__0xEeihcqubBYu_Xg" name="TOPSUPERVISION" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO7AP0xEeihcqubBYu_Xg" value="TOPSUPERVISION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO7Af0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO7Av0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO7A_0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO7BP0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO7Bf0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO7Bv0xEeihcqubBYu_Xg" name="MODRGLMT" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO7B_0xEeihcqubBYu_Xg" value="MODRGLMT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO7CP0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VO7Cf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VO7Cv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VO7C_0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VO7DP0xEeihcqubBYu_Xg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VO7Df0xEeihcqubBYu_Xg" name="TOPFA" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VO7Dv0xEeihcqubBYu_Xg" value="TOPFA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VO7D_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VYrEP0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrEf0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrEv0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrE_0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrFP0xEeihcqubBYu_Xg" name="TOPLE" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrFf0xEeihcqubBYu_Xg" value="TOPLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrFv0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VYrF_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrGP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrGf0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrGv0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrG_0xEeihcqubBYu_Xg" name="FACTURATION" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrHP0xEeihcqubBYu_Xg" value="FACTURATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrHf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrHv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrH_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrIP0xEeihcqubBYu_Xg" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrIf0xEeihcqubBYu_Xg" name="ADRESSE" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrIv0xEeihcqubBYu_Xg" value="ADRESSE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrI_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrJP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrJf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrJv0xEeihcqubBYu_Xg" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrJ_0xEeihcqubBYu_Xg" name="TXREMISE" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrKP0xEeihcqubBYu_Xg" value="TXREMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrKf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrKv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrK_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrLP0xEeihcqubBYu_Xg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrLf0xEeihcqubBYu_Xg" name="TOPCHR" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrLv0xEeihcqubBYu_Xg" value="TOPCHR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrL_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VYrMP0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrMf0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrMv0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrM_0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrNP0xEeihcqubBYu_Xg" name="MAXCDE" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrNf0xEeihcqubBYu_Xg" value="MAXCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrNv0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VYrN_0xEeihcqubBYu_Xg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrOP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrOf0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrOv0xEeihcqubBYu_Xg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrO_0xEeihcqubBYu_Xg" name="MERCUGRP" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrPP0xEeihcqubBYu_Xg" value="MERCUGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrPf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrPv0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrP_0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrQP0xEeihcqubBYu_Xg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrQf0xEeihcqubBYu_Xg" name="CDEMERC" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrQv0xEeihcqubBYu_Xg" value="CDEMERC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrQ_0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrRP0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrRf0xEeihcqubBYu_Xg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrRv0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrR_0xEeihcqubBYu_Xg" name="TOPEFAC" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrSP0xEeihcqubBYu_Xg" value="TOPEFAC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrSf0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_9VYrSv0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrS_0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrTP0xEeihcqubBYu_Xg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrTf0xEeihcqubBYu_Xg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_9VYrTv0xEeihcqubBYu_Xg" name="AUTOLOGKEY" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_9VYrT_0xEeihcqubBYu_Xg" value="AUTOLOGKEY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_9VYrUP0xEeihcqubBYu_Xg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_9VYrUf0xEeihcqubBYu_Xg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_9VYrUv0xEeihcqubBYu_Xg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_9VYrU_0xEeihcqubBYu_Xg" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_wFBncBNaEemsgZOghPO5Pg" name="CLIENTCD">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_wFBncRNaEemsgZOghPO5Pg" value="CLIENTCD"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_wFBnchNaEemsgZOghPO5Pg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_wFVJcBNaEemsgZOghPO5Pg" value="CLIENTCD  "/>
      <node defType="com.stambia.rdbms.column" id="_wSkUMBNaEemsgZOghPO5Pg" name="IDCWEB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUMRNaEemsgZOghPO5Pg" value="IDCWEB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUMhNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUMxNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUNBNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUNRNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUNhNaEemsgZOghPO5Pg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUNxNaEemsgZOghPO5Pg" name="MFERME" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUOBNaEemsgZOghPO5Pg" value="MFERME"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUORNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUOhNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUOxNaEemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUPBNaEemsgZOghPO5Pg" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUPRNaEemsgZOghPO5Pg" name="PRENOM" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUPhNaEemsgZOghPO5Pg" value="PRENOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUPxNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUQBNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUQRNaEemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUQhNaEemsgZOghPO5Pg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUQxNaEemsgZOghPO5Pg" name="ANNIV" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkURBNaEemsgZOghPO5Pg" value="ANNIV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkURRNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkURhNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkURxNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUSBNaEemsgZOghPO5Pg" value="DATE"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUSRNaEemsgZOghPO5Pg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUShNaEemsgZOghPO5Pg" name="NEWSCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUSxNaEemsgZOghPO5Pg" value="NEWSCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUTBNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUTRNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUThNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUTxNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUUBNaEemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUURNaEemsgZOghPO5Pg" name="NEWSPA" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUUhNaEemsgZOghPO5Pg" value="NEWSPA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUUxNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUVBNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUVRNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUVhNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUVxNaEemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUWBNaEemsgZOghPO5Pg" name="NSIRET" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUWRNaEemsgZOghPO5Pg" value="NSIRET"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUWhNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUWxNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUXBNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUXRNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUXhNaEemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUXxNaEemsgZOghPO5Pg" name="BGROUP" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUYBNaEemsgZOghPO5Pg" value="BGROUP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUYRNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUYhNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUYxNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUZBNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUZRNaEemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUZhNaEemsgZOghPO5Pg" name="TYPGRP" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUZxNaEemsgZOghPO5Pg" value="TYPGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUaBNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUaRNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUahNaEemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUaxNaEemsgZOghPO5Pg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUbBNaEemsgZOghPO5Pg" name="IDMARC" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUbRNaEemsgZOghPO5Pg" value="IDMARC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUbhNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUbxNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUcBNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUcRNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUchNaEemsgZOghPO5Pg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUcxNaEemsgZOghPO5Pg" name="IDACTI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUdBNaEemsgZOghPO5Pg" value="IDACTI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUdRNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUdhNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUdxNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUeBNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUeRNaEemsgZOghPO5Pg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUehNaEemsgZOghPO5Pg" name="IDDETA" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUexNaEemsgZOghPO5Pg" value="IDDETA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUfBNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUfRNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUfhNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUfxNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUgBNaEemsgZOghPO5Pg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUgRNaEemsgZOghPO5Pg" name="TOPMOD" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUghNaEemsgZOghPO5Pg" value="TOPMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUgxNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUhBNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUhRNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUhhNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUhxNaEemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUiBNaEemsgZOghPO5Pg" name="NUMCLI" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUiRNaEemsgZOghPO5Pg" value="NUMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUihNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUixNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUjBNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUjRNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUjhNaEemsgZOghPO5Pg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUjxNaEemsgZOghPO5Pg" name="TOPCWB" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUkBNaEemsgZOghPO5Pg" value="TOPCWB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUkRNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUkhNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUkxNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUlBNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUlRNaEemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUlhNaEemsgZOghPO5Pg" name="TOPMWB" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUlxNaEemsgZOghPO5Pg" value="TOPMWB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUmBNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wSkUmRNaEemsgZOghPO5Pg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUmhNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUmxNaEemsgZOghPO5Pg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUnBNaEemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUnRNaEemsgZOghPO5Pg" name="TOPVWB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUnhNaEemsgZOghPO5Pg" value="TOPVWB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUnxNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUoBNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUoRNaEemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUohNaEemsgZOghPO5Pg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wSkUoxNaEemsgZOghPO5Pg" name="TOPCDI" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_wSkUpBNaEemsgZOghPO5Pg" value="TOPCDI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wSkUpRNaEemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wSkUphNaEemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wSkUpxNaEemsgZOghPO5Pg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wSkUqBNaEemsgZOghPO5Pg" value="1"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_pDWtQP7zEemVAft-X6rPVg" name="STAMBIA">
    <attribute defType="com.stambia.rdbms.schema.name" id="_pELMoP7zEemVAft-X6rPVg" value="STAMBIA"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_pELMof7zEemVAft-X6rPVg" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_pELMov7zEemVAft-X6rPVg" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_pELzsP7zEemVAft-X6rPVg" value="I_[targetName]"/>
  </node>
</md:node>