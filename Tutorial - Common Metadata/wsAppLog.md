<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.wsdl.wsdl" id="_5JlL8MF7EeilU77n104Zew" name="AppLoger" md:ref="platform:/plugin/com.indy.environment/.tech/web/wsdl.tech#UUID_TECH_WSDL1?fileId=UUID_TECH_WSDL1$type=tech$name=wsdl?">
  <attribute defType="com.stambia.wsdl.wsdl.xsdReverseVersion" id="_5JvkAcF7EeilU77n104Zew" value="1"/>
  <attribute defType="com.stambia.wsdl.wsdl.url" id="_5St50MF7EeilU77n104Zew" value="http://serverdev01/WEBSERVICELOGAPP_WEB/awws/WebServicelogApp.awws?wsdl"/>
  <attribute defType="com.stambia.wsdl.wsdl.prefixForElement" id="_9ugvsMF7EeilU77n104Zew" value="unqualified"/>
  <attribute defType="com.stambia.wsdl.wsdl.prefixForAttribute" id="_9ugvscF7EeilU77n104Zew" value="unqualified"/>
  <attribute defType="com.stambia.wsdl.wsdl.targetNamespace" id="_9uhWwMF7EeilU77n104Zew" value="urn:WebServiceLogApp"/>
  <node defType="com.stambia.xml.namespace" id="_9uVJgMF7EeilU77n104Zew" name="http://schemas.xmlsoap.org/wsdl/">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_9uVJgcF7EeilU77n104Zew" value="ns"/>
  </node>
  <node defType="com.stambia.xml.namespace" id="_9uVJgsF7EeilU77n104Zew" name="http://www.w3.org/2001/XMLSchema">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_9uVJg8F7EeilU77n104Zew" value="xsd"/>
  </node>
  <node defType="com.stambia.xml.namespace" id="_9uVJhMF7EeilU77n104Zew" name="http://schemas.xmlsoap.org/wsdl/http/">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_9uVJhcF7EeilU77n104Zew" value="http"/>
  </node>
  <node defType="com.stambia.xml.namespace" id="_9uVJhsF7EeilU77n104Zew" name="urn:WebServiceLogApp">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_9uVJh8F7EeilU77n104Zew" value="s0"/>
  </node>
  <node defType="com.stambia.xml.namespace" id="_9uVJiMF7EeilU77n104Zew" name="http://schemas.xmlsoap.org/wsdl/soap/">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_9uVJicF7EeilU77n104Zew" value="soap"/>
  </node>
  <node defType="com.stambia.wsdl.service" id="_9uWXoMF7EeilU77n104Zew" name="WebServiceLogApp">
    <node defType="com.stambia.wsdl.port" id="_9uW-sMF7EeilU77n104Zew" name="WebServiceLogAppSOAPPort">
      <attribute defType="com.stambia.wsdl.port.address" id="_9uXlwMF7EeilU77n104Zew" value="http://serverdev01/WEBSERVICELOGAPP_WEB/awws/WebServiceLogApp.awws"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_9uXlwcF7EeilU77n104Zew" value="SOAP"/>
      <attribute defType="com.stambia.wsdl.port.transportURI" id="_9uXlwsF7EeilU77n104Zew" value="http://schemas.xmlsoap.org/soap/http"/>
      <attribute defType="com.stambia.wsdl.port.style" id="_9uXlw8F7EeilU77n104Zew" value="document"/>
      <node defType="com.stambia.wsdl.operation" id="_9uYM0MF7EeilU77n104Zew" name="AjoutMessage">
        <attribute defType="com.stambia.wsdl.operation.style" id="_9uapEMF7EeilU77n104Zew" value="document"/>
        <attribute defType="com.stambia.wsdl.operation.actionURI" id="_9uapEcF7EeilU77n104Zew" value="urn:WebServiceLogApp/AjoutMessage"/>
        <node defType="com.stambia.wsdl.input" id="_9uYM0cF7EeilU77n104Zew">
          <node defType="com.stambia.wsdl.part" id="_9uYM0sF7EeilU77n104Zew" name="parameters">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_9uZbDMF7EeilU77n104Zew" value="soap:body"/>
            <attribute defType="com.stambia.wsdl.part.namespaceURI" id="_9uZbDcF7EeilU77n104Zew"/>
            <attribute defType="com.stambia.wsdl.part.use" id="_9uZbDsF7EeilU77n104Zew" value="literal"/>
            <node defType="com.stambia.xml.element" id="_9uYM08F7EeilU77n104Zew" name="AjoutMessage" position="0">
              <attribute defType="com.stambia.xml.element.originalType" id="_9uYM1MF7EeilU77n104Zew" value="s0:tAjoutMessage"/>
              <node defType="com.stambia.xml.sequence" id="_9uZa8MF7EeilU77n104Zew" position="3">
                <attribute defType="com.stambia.xml.sequence.minOccurs" id="_9uZa8cF7EeilU77n104Zew" value="1"/>
                <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_9uZa8sF7EeilU77n104Zew" value="1"/>
                <node defType="com.stambia.xml.element" id="_9uZa88F7EeilU77n104Zew" name="sNomApp" position="0">
                  <attribute defType="com.stambia.xml.element.minOccurs" id="_9uZa9MF7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.maxOccurs" id="_9uZa9cF7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.type" id="_9uZa9sF7EeilU77n104Zew" value="string"/>
                  <attribute defType="com.stambia.xml.element.originalType" id="_9uZa98F7EeilU77n104Zew" value="xsd:string"/>
                </node>
                <node defType="com.stambia.xml.element" id="_9uZa-MF7EeilU77n104Zew" name="iGravité" position="1">
                  <attribute defType="com.stambia.xml.element.minOccurs" id="_9uZa-cF7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.maxOccurs" id="_9uZa-sF7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.type" id="_9uZa-8F7EeilU77n104Zew" value="int"/>
                  <attribute defType="com.stambia.xml.element.originalType" id="_9uZa_MF7EeilU77n104Zew" value="xsd:int"/>
                </node>
                <node defType="com.stambia.xml.element" id="_9uZa_cF7EeilU77n104Zew" name="sObjet" position="2">
                  <attribute defType="com.stambia.xml.element.minOccurs" id="_9uZa_sF7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.maxOccurs" id="_9uZa_8F7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.type" id="_9uZbAMF7EeilU77n104Zew" value="string"/>
                  <attribute defType="com.stambia.xml.element.originalType" id="_9uZbAcF7EeilU77n104Zew" value="xsd:string"/>
                </node>
                <node defType="com.stambia.xml.element" id="_9uZbAsF7EeilU77n104Zew" name="sdetail" position="3">
                  <attribute defType="com.stambia.xml.element.minOccurs" id="_9uZbA8F7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.maxOccurs" id="_9uZbBMF7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.type" id="_9uZbBcF7EeilU77n104Zew" value="string"/>
                  <attribute defType="com.stambia.xml.element.originalType" id="_9uZbBsF7EeilU77n104Zew" value="xsd:string"/>
                </node>
                <node defType="com.stambia.xml.element" id="_9uZbB8F7EeilU77n104Zew" name="bTest" position="4">
                  <attribute defType="com.stambia.xml.element.minOccurs" id="_9uZbCMF7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.maxOccurs" id="_9uZbCcF7EeilU77n104Zew" value="1"/>
                  <attribute defType="com.stambia.xml.element.type" id="_9uZbCsF7EeilU77n104Zew" value="boolean"/>
                  <attribute defType="com.stambia.xml.element.originalType" id="_9uZbC8F7EeilU77n104Zew" value="xsd:boolean"/>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="_9uaCAMF7EeilU77n104Zew">
          <node defType="com.stambia.wsdl.part" id="_9uaCAcF7EeilU77n104Zew" name="parameters">
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_9uaCBMF7EeilU77n104Zew" value="soap:body"/>
            <attribute defType="com.stambia.wsdl.part.namespaceURI" id="_9uaCBcF7EeilU77n104Zew"/>
            <attribute defType="com.stambia.wsdl.part.use" id="_9uaCBsF7EeilU77n104Zew" value="literal"/>
            <node defType="com.stambia.xml.element" id="_9uaCAsF7EeilU77n104Zew" name="AjoutMessageResponse" position="0">
              <attribute defType="com.stambia.xml.element.originalType" id="_9uaCA8F7EeilU77n104Zew" value="s0:tAjoutMessageResponse"/>
            </node>
          </node>
        </node>
        <node defType="com.stambia.wsdl.fault" id="_9uapEsF7EeilU77n104Zew" name="StandardFault">
          <node defType="com.stambia.wsdl.part" id="_9uapE8F7EeilU77n104Zew" name="fault">
            <node defType="com.stambia.xml.element" id="_9uapFMF7EeilU77n104Zew" name="faultcode">
              <attribute defType="com.stambia.xml.element.type" id="_9uapFcF7EeilU77n104Zew" value="string"/>
            </node>
            <node defType="com.stambia.xml.element" id="_9uapFsF7EeilU77n104Zew" name="faultstring">
              <attribute defType="com.stambia.xml.element.type" id="_9uapF8F7EeilU77n104Zew" value="string"/>
            </node>
            <node defType="com.stambia.xml.element" id="_9uapGMF7EeilU77n104Zew" name="faultactor">
              <attribute defType="com.stambia.xml.element.type" id="_9uapGcF7EeilU77n104Zew" value="string"/>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>