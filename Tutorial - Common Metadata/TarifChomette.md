<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_oJoQUIwlEeisgcp1PNVE7g" name="TarifChomette" md:ref="../Stambia_chomette_com/ibm_db2_400.rdbms.md#UUID_MD_RDBMS_DB2_400?fileId=UUID_MD_RDBMS_DB2_400$type=md$name=IBM%20DB2/400?">
  <attribute defType="com.stambia.rdbms.server.url" id="_6zg8kIwlEeisgcp1PNVE7g" value="jdbc:as400://chomette"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_6znqQIwlEeisgcp1PNVE7g" value="com.ibm.as400.access.AS400JDBCDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_6zw0MIwlEeisgcp1PNVE7g" value="rhanem"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_6zyCUIwlEeisgcp1PNVE7g" value="CA4F95F2F01C6BD38E131D3FDB473790"/>
  <node defType="com.stambia.rdbms.schema" id="_oSPZwIwlEeisgcp1PNVE7g" name="ORIONBD">
    <attribute defType="com.stambia.rdbms.schema.name" id="_oSdcMIwlEeisgcp1PNVE7g" value="ORIONBD"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_oSeDQIwlEeisgcp1PNVE7g" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_oSeDQYwlEeisgcp1PNVE7g" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_oSeqUIwlEeisgcp1PNVE7g" value="I_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.work" id="_qwOcwP8JEemVAft-X6rPVg" ref="#_lgKRUP8JEemVAft-X6rPVg?fileId=_oJoQUIwlEeisgcp1PNVE7g$type=md$name=STAMBIA?"/>
    <node defType="com.stambia.rdbms.datastore" id="_6tDvQIwlEeisgcp1PNVE7g" name="OTB8CPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_6tDvQYwlEeisgcp1PNVE7g" value="OTB8CPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_6tDvQowlEeisgcp1PNVE7g" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_6tMSIIwlEeisgcp1PNVE7g" value="OTB8CPP   "/>
      <node defType="com.stambia.rdbms.column" id="_6tksoIwlEeisgcp1PNVE7g" name="B8B3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tksoYwlEeisgcp1PNVE7g" value="B8B3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tksoowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6tkso4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tkspIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tkspYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tkspowlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tlTsIwlEeisgcp1PNVE7g" name="B8IMS3" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tlTsYwlEeisgcp1PNVE7g" value="B8IMS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tlTsowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tlTs4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tlTtIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tlTtYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tl6wIwlEeisgcp1PNVE7g" name="B8INS3" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tl6wYwlEeisgcp1PNVE7g" value="B8INS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tl6wowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tl6w4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tl6xIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tl6xYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tl6xowlEeisgcp1PNVE7g" name="B8IOS3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tl6x4wlEeisgcp1PNVE7g" value="B8IOS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tl6yIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tl6yYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tl6yowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tl6y4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tmh0IwlEeisgcp1PNVE7g" name="B8IPS3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tmh0YwlEeisgcp1PNVE7g" value="B8IPS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tmh0owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tmh04wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tmh1IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tmh1YwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tnI4IwlEeisgcp1PNVE7g" name="B8IQS3" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tnI4YwlEeisgcp1PNVE7g" value="B8IQS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tnI4owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tnI44wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tnI5IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tnI5YwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tnI5owlEeisgcp1PNVE7g" name="B8IRS3" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tnI54wlEeisgcp1PNVE7g" value="B8IRS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tnI6IwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tnI6YwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tnI6owlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tnI64wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tnv8IwlEeisgcp1PNVE7g" name="B8ITS3" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tnv8YwlEeisgcp1PNVE7g" value="B8ITS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tnv8owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tnv84wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tnv9IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tnv9YwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tnv9owlEeisgcp1PNVE7g" name="B8IUS3" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tnv94wlEeisgcp1PNVE7g" value="B8IUS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tnv-IwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tnv-YwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tnv-owlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tnv-4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6toXAIwlEeisgcp1PNVE7g" name="B8ISS3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_6toXAYwlEeisgcp1PNVE7g" value="B8ISS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6toXAowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6toXA4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6toXBIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6toXBYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6to-EIwlEeisgcp1PNVE7g" name="B8IVS3" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_6to-EYwlEeisgcp1PNVE7g" value="B8IVS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6to-EowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6to-E4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6to-FIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6to-FYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6to-FowlEeisgcp1PNVE7g" name="B8IWS3" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_6to-F4wlEeisgcp1PNVE7g" value="B8IWS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6to-GIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6to-GYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6to-GowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6to-G4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tplIIwlEeisgcp1PNVE7g" name="B8IXS3" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tplIYwlEeisgcp1PNVE7g" value="B8IXS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tplIowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tplI4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tplJIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tplJYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tqMMIwlEeisgcp1PNVE7g" name="B8IYS3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tqMMYwlEeisgcp1PNVE7g" value="B8IYS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tqMMowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tqMM4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tqMNIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tqMNYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tqzQIwlEeisgcp1PNVE7g" name="B8JCS3" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tqzQYwlEeisgcp1PNVE7g" value="B8JCS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tqzQowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tqzQ4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tqzRIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tqzRYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6traUIwlEeisgcp1PNVE7g" name="B8IZS3" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_6traUYwlEeisgcp1PNVE7g" value="B8IZS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6traUowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6traU4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6traVIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6traVYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tsBYIwlEeisgcp1PNVE7g" name="B8I0S3" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tsBYYwlEeisgcp1PNVE7g" value="B8I0S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tsBYowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tsBY4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tsBZIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tsBZYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tsocIwlEeisgcp1PNVE7g" name="B8I1S3" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tsocYwlEeisgcp1PNVE7g" value="B8I1S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tsocowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tsoc4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tsodIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tsodYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6ttPgIwlEeisgcp1PNVE7g" name="B8I2S3" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_6ttPgYwlEeisgcp1PNVE7g" value="B8I2S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6ttPgowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6ttPg4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6ttPhIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6ttPhYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tt2kIwlEeisgcp1PNVE7g" name="B8I3S3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tt2kYwlEeisgcp1PNVE7g" value="B8I3S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tt2kowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tt2k4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tt2lIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tt2lYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tudoIwlEeisgcp1PNVE7g" name="B8I4S3" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tudoYwlEeisgcp1PNVE7g" value="B8I4S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tudoowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tudo4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tudpIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tudpYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tvEsIwlEeisgcp1PNVE7g" name="B8R1C1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tvEsYwlEeisgcp1PNVE7g" value="B8R1C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tvEsowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tvEs4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tvEtIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tvEtYwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tvrwIwlEeisgcp1PNVE7g" name="B8R2C1" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tvrwYwlEeisgcp1PNVE7g" value="B8R2C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tvrwowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tvrw4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tvrxIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tvrxYwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6twS0IwlEeisgcp1PNVE7g" name="B8R3C1" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_6twS0YwlEeisgcp1PNVE7g" value="B8R3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6twS0owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6twS04wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6twS1IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6twS1YwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tw54IwlEeisgcp1PNVE7g" name="B8R4C1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tw54YwlEeisgcp1PNVE7g" value="B8R4C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tw54owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tw544wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tw55IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tw55YwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6txg8IwlEeisgcp1PNVE7g" name="B8R5C1" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_6txg8YwlEeisgcp1PNVE7g" value="B8R5C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6txg8owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6txg84wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6txg9IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6txg9YwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tyIAIwlEeisgcp1PNVE7g" name="B8I5S3" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tyIAYwlEeisgcp1PNVE7g" value="B8I5S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tyIAowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tyIA4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tyIBIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tyIBYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tyvEIwlEeisgcp1PNVE7g" name="B8R6C1" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tyvEYwlEeisgcp1PNVE7g" value="B8R6C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tyvEowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tyvE4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tyvFIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tyvFYwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tyvFowlEeisgcp1PNVE7g" name="B8R7C1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tyvF4wlEeisgcp1PNVE7g" value="B8R7C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tyvGIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tyvGYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tyvGowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tyvG4wlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tzWIIwlEeisgcp1PNVE7g" name="B8R8C1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tz9MIwlEeisgcp1PNVE7g" value="B8R8C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tz9MYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tz9MowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tz9M4wlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tz9NIwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6tz9NYwlEeisgcp1PNVE7g" name="B8R9C1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_6tz9NowlEeisgcp1PNVE7g" value="B8R9C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6tz9N4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6tz9OIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6tz9OYwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6tz9OowlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t0kQIwlEeisgcp1PNVE7g" name="B8SAC1" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t0kQYwlEeisgcp1PNVE7g" value="B8SAC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t0kQowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t0kQ4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t0kRIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t0kRYwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t0kRowlEeisgcp1PNVE7g" name="B8I6S3" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t0kR4wlEeisgcp1PNVE7g" value="B8I6S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t0kSIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t0kSYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t0kSowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t0kS4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t1LUIwlEeisgcp1PNVE7g" name="B8SBC1" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t1LUYwlEeisgcp1PNVE7g" value="B8SBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t1LUowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t1LU4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t1LVIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t1LVYwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t1yYIwlEeisgcp1PNVE7g" name="B8SCC1" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t1yYYwlEeisgcp1PNVE7g" value="B8SCC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t1yYowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t1yY4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t1yZIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t1yZYwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t1yZowlEeisgcp1PNVE7g" name="B8SDC1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t1yZ4wlEeisgcp1PNVE7g" value="B8SDC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t1yaIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t1yaYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t1yaowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t1ya4wlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t2ZcIwlEeisgcp1PNVE7g" name="B8SEC1" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t2ZcYwlEeisgcp1PNVE7g" value="B8SEC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t2ZcowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t2Zc4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t2ZdIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t2ZdYwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t2ZdowlEeisgcp1PNVE7g" name="B8SFC1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t2Zd4wlEeisgcp1PNVE7g" value="B8SFC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t2ZeIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t2ZeYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t2ZeowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t2Ze4wlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t3AgIwlEeisgcp1PNVE7g" name="B8I7S3" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t3AgYwlEeisgcp1PNVE7g" value="B8I7S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t3AgowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t3Ag4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t3AhIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t3AhYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t3AhowlEeisgcp1PNVE7g" name="B8I8S3" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t3Ah4wlEeisgcp1PNVE7g" value="B8I8S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t3AiIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t3AiYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t3AiowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t3Ai4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t3nkIwlEeisgcp1PNVE7g" name="B8I9S3" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t3nkYwlEeisgcp1PNVE7g" value="B8I9S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t3nkowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t3nk4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t3nlIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t3nlYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t3nlowlEeisgcp1PNVE7g" name="B8JAS3" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t3nl4wlEeisgcp1PNVE7g" value="B8JAS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t3nmIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t3nmYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t3nmowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t3nm4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t4OoIwlEeisgcp1PNVE7g" name="B8JBS3" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t4OoYwlEeisgcp1PNVE7g" value="B8JBS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t4OoowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t4Oo4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t4OpIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t4OpYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t41sIwlEeisgcp1PNVE7g" name="B8NMT2" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t41sYwlEeisgcp1PNVE7g" value="B8NMT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t41sowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t41s4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t41tIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t41tYwlEeisgcp1PNVE7g" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6t41towlEeisgcp1PNVE7g" name="B8NNT2" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_6t41t4wlEeisgcp1PNVE7g" value="B8NNT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6t41uIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6t41uYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6t41uowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6t41u4wlEeisgcp1PNVE7g" value="80"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_6pKtwIwlEeisgcp1PNVE7g" name="ORI1REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_6pLU0IwlEeisgcp1PNVE7g" value="ORI1REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_6pLU0YwlEeisgcp1PNVE7g" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_6pVs4IwlEeisgcp1PNVE7g" value="ORI1REP   "/>
      <node defType="com.stambia.rdbms.column" id="_6r21YIwlEeisgcp1PNVE7g" name="I1B3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r21YYwlEeisgcp1PNVE7g" value="I1B3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r21YowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r21Y4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r21ZIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r21ZYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r21ZowlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r3ccIwlEeisgcp1PNVE7g" name="I1AZCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r3ccYwlEeisgcp1PNVE7g" value="I1AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r3ccowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r3cc4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r3cdIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r3cdYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r3cdowlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r3cd4wlEeisgcp1PNVE7g" name="I1GHCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r3ceIwlEeisgcp1PNVE7g" value="I1GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r3ceYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r3ceowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r3ce4wlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r3cfIwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r4DgIwlEeisgcp1PNVE7g" name="I1J2CD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r4DgYwlEeisgcp1PNVE7g" value="I1J2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r4DgowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r4Dg4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r4DhIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r4DhYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r4qkIwlEeisgcp1PNVE7g" name="I1J3CD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r4qkYwlEeisgcp1PNVE7g" value="I1J3CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r4qkowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r4qk4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r4qlIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r4qlYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r4qlowlEeisgcp1PNVE7g" name="I1HMCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r4ql4wlEeisgcp1PNVE7g" value="I1HMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r4qmIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r4qmYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r4qmowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r4qm4wlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r4qnIwlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r5RoIwlEeisgcp1PNVE7g" name="I1JCCD" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r5RoYwlEeisgcp1PNVE7g" value="I1JCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r5RoowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r5Ro4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r5RpIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r5RpYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r5RpowlEeisgcp1PNVE7g" name="I1CPCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r5Rp4wlEeisgcp1PNVE7g" value="I1CPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r5RqIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r5RqYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r5RqowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r5Rq4wlEeisgcp1PNVE7g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r54sIwlEeisgcp1PNVE7g" name="I1ISTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r54sYwlEeisgcp1PNVE7g" value="I1ISTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r54sowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r54s4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r54tIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r54tYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r6fwIwlEeisgcp1PNVE7g" name="I1JVCD" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r6fwYwlEeisgcp1PNVE7g" value="I1JVCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r6fwowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r6fw4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r6fxIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r6fxYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r6fxowlEeisgcp1PNVE7g" name="I1JWCD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r6fx4wlEeisgcp1PNVE7g" value="I1JWCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r6fyIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r6fyYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r7G0IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r7G0YwlEeisgcp1PNVE7g" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r7G0owlEeisgcp1PNVE7g" name="I1MINB" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r7G04wlEeisgcp1PNVE7g" value="I1MINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r7G1IwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r7G1YwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r7G1owlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r7G14wlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r7G2IwlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r7t4IwlEeisgcp1PNVE7g" name="I1MJNB" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r7t4YwlEeisgcp1PNVE7g" value="I1MJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r7t4owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r7t44wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r7t5IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r7t5YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r7t5owlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r8U8IwlEeisgcp1PNVE7g" name="I1MKNB" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r8U8YwlEeisgcp1PNVE7g" value="I1MKNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r8U8owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r8U84wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r8U9IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r8U9YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r8U9owlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r8U94wlEeisgcp1PNVE7g" name="I1MLNB" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r8U-IwlEeisgcp1PNVE7g" value="I1MLNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r8U-YwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r88AIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r88AYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r88AowlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r88A4wlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r88BIwlEeisgcp1PNVE7g" name="I1MMNB" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r88BYwlEeisgcp1PNVE7g" value="I1MMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r88BowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r88B4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r88CIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r88CYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r88CowlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r9jEIwlEeisgcp1PNVE7g" name="I1MNNB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r9jEYwlEeisgcp1PNVE7g" value="I1MNNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r9jEowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r9jE4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r9jFIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r9jFYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r9jFowlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r9jF4wlEeisgcp1PNVE7g" name="I1NMNB" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r9jGIwlEeisgcp1PNVE7g" value="I1NMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r9jGYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r9jGowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r9jG4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r9jHIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r9jHYwlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r-KIIwlEeisgcp1PNVE7g" name="I1KYNB" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r-KIYwlEeisgcp1PNVE7g" value="I1KYNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r-KIowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6r-KI4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r-KJIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r-KJYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r-KJowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r-xMIwlEeisgcp1PNVE7g" name="I1CNN3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r-xMYwlEeisgcp1PNVE7g" value="I1CNN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r-xMowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r-xM4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r-xNIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r-xNYwlEeisgcp1PNVE7g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r-xNowlEeisgcp1PNVE7g" name="I1CON3" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r-xN4wlEeisgcp1PNVE7g" value="I1CON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r-xOIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r-xOYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r-xOowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r-xO4wlEeisgcp1PNVE7g" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r_YQIwlEeisgcp1PNVE7g" name="I1I4TX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r_YQYwlEeisgcp1PNVE7g" value="I1I4TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r_YQowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r_YQ4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r_YRIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r_YRYwlEeisgcp1PNVE7g" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r__UIwlEeisgcp1PNVE7g" name="I1QQST" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r__UYwlEeisgcp1PNVE7g" value="I1QQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r__UowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r__U4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r__VIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r__VYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6r__VowlEeisgcp1PNVE7g" name="I1QPST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_6r__V4wlEeisgcp1PNVE7g" value="I1QPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6r__WIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6r__WYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6r__WowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6r__W4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sAmYIwlEeisgcp1PNVE7g" name="I1CXS1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sAmYYwlEeisgcp1PNVE7g" value="I1CXS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sAmYowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sAmY4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sAmZIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sAmZYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sBNcIwlEeisgcp1PNVE7g" name="I1KZNB" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sBNcYwlEeisgcp1PNVE7g" value="I1KZNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sBNcowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sBNc4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sBNdIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sBNdYwlEeisgcp1PNVE7g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sBNdowlEeisgcp1PNVE7g" name="I1NCNB" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sBNd4wlEeisgcp1PNVE7g" value="I1NCNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sBNeIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sBNeYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sBNeowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sBNe4wlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sBNfIwlEeisgcp1PNVE7g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sCbkIwlEeisgcp1PNVE7g" name="I1K0NB" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sCbkYwlEeisgcp1PNVE7g" value="I1K0NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sCbkowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sCbk4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sCblIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sCblYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sCblowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sEQwIwlEeisgcp1PNVE7g" name="I1HPN1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sEQwYwlEeisgcp1PNVE7g" value="I1HPN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sEQwowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sEQw4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sEQxIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sEQxYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sEQxowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sEQx4wlEeisgcp1PNVE7g" name="I1QRST" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sEQyIwlEeisgcp1PNVE7g" value="I1QRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sEQyYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sEQyowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sEQy4wlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sEQzIwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sE30IwlEeisgcp1PNVE7g" name="I1CYS1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sE30YwlEeisgcp1PNVE7g" value="I1CYS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sE30owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sE304wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sE31IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sE31YwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sFe4IwlEeisgcp1PNVE7g" name="I1QSST" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sFe4YwlEeisgcp1PNVE7g" value="I1QSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sFe4owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sFe44wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sFe5IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sFe5YwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sGF8IwlEeisgcp1PNVE7g" name="I1QTST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sGF8YwlEeisgcp1PNVE7g" value="I1QTST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sGF8owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sGF84wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sGF9IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sGF9YwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sGF9owlEeisgcp1PNVE7g" name="I1QVST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sGF94wlEeisgcp1PNVE7g" value="I1QVST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sGF-IwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sGF-YwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sGF-owlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sGF-4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sHUEIwlEeisgcp1PNVE7g" name="I1QUST" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sHUEYwlEeisgcp1PNVE7g" value="I1QUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sHUEowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sHUE4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sHUFIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sHUFYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sHUFowlEeisgcp1PNVE7g" name="I1FKS2" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sHUF4wlEeisgcp1PNVE7g" value="I1FKS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sHUGIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sHUGYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sHUGowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sHUG4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sH7IIwlEeisgcp1PNVE7g" name="I1FLS2" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sH7IYwlEeisgcp1PNVE7g" value="I1FLS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sH7IowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sH7I4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sH7JIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sH7JYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sIiMIwlEeisgcp1PNVE7g" name="I1FMS2" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sIiMYwlEeisgcp1PNVE7g" value="I1FMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sIiMowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sIiM4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sIiNIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sIiNYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sIiNowlEeisgcp1PNVE7g" name="I1QXST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sIiN4wlEeisgcp1PNVE7g" value="I1QXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sIiOIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sIiOYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sIiOowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sIiO4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sJJQIwlEeisgcp1PNVE7g" name="I1QYST" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sJJQYwlEeisgcp1PNVE7g" value="I1QYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sJJQowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sJJQ4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sJJRIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sJJRYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sJwUIwlEeisgcp1PNVE7g" name="I1QZST" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sJwUYwlEeisgcp1PNVE7g" value="I1QZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sJwUowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sJwU4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sJwVIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sJwVYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sKXYIwlEeisgcp1PNVE7g" name="I1Q0ST" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sKXYYwlEeisgcp1PNVE7g" value="I1Q0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sKXYowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sKXY4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sKXZIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sKXZYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sK-cIwlEeisgcp1PNVE7g" name="I1K1NB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sK-cYwlEeisgcp1PNVE7g" value="I1K1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sK-cowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sK-c4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sK-dIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sK-dYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sK-dowlEeisgcp1PNVE7g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sMzoIwlEeisgcp1PNVE7g" name="I1K2NB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sMzoYwlEeisgcp1PNVE7g" value="I1K2NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sMzoowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sMzo4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sMzpIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sMzpYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sNasIwlEeisgcp1PNVE7g" name="I1K3NB" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sNasYwlEeisgcp1PNVE7g" value="I1K3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sNasowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sNas4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sNatIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sNatYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sOo0IwlEeisgcp1PNVE7g" name="I1S6ST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sOo0YwlEeisgcp1PNVE7g" value="I1S6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sOo0owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sOo04wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sOo1IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sOo1YwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sPP4IwlEeisgcp1PNVE7g" name="I1XEST" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sPP4YwlEeisgcp1PNVE7g" value="I1XEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sPP4owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sPP44wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sPP5IwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sPP5YwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sP28IwlEeisgcp1PNVE7g" name="I1DHQT" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sP28YwlEeisgcp1PNVE7g" value="I1DHQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sP28owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sP284wlEeisgcp1PNVE7g" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sP29IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sP29YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sP29owlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sQeAIwlEeisgcp1PNVE7g" name="I1AFNB" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sQeAYwlEeisgcp1PNVE7g" value="I1AFNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sQeAowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sQeA4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sQeBIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sQeBYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sQeBowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sQeB4wlEeisgcp1PNVE7g" name="I1L9QT" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sQeCIwlEeisgcp1PNVE7g" value="I1L9QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sQeCYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sQeCowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sQeC4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sQeDIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sQeDYwlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sRFEIwlEeisgcp1PNVE7g" name="I1L9N1" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sRFEYwlEeisgcp1PNVE7g" value="I1L9N1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sRFEowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sRFE4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sRFFIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sRFFYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sRFFowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sRFF4wlEeisgcp1PNVE7g" name="I1DAT1" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sRFGIwlEeisgcp1PNVE7g" value="I1DAT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sRFGYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sRFGowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sRFG4wlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sRFHIwlEeisgcp1PNVE7g" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sRsIIwlEeisgcp1PNVE7g" name="I1CZS1" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sRsIYwlEeisgcp1PNVE7g" value="I1CZS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sRsIowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sRsI4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sRsJIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sRsJYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sRsJowlEeisgcp1PNVE7g" name="I1C9T1" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sRsJ4wlEeisgcp1PNVE7g" value="I1C9T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sRsKIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sRsKYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sRsKowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sRsK4wlEeisgcp1PNVE7g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sSTMIwlEeisgcp1PNVE7g" name="I1TPTX" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sSTMYwlEeisgcp1PNVE7g" value="I1TPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sSTMowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sSTM4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sSTNIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sSTNYwlEeisgcp1PNVE7g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sS6QIwlEeisgcp1PNVE7g" name="I1TQTX" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sS6QYwlEeisgcp1PNVE7g" value="I1TQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sS6QowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sS6Q4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sS6RIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sS6RYwlEeisgcp1PNVE7g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sS6RowlEeisgcp1PNVE7g" name="I1GVPR" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sS6R4wlEeisgcp1PNVE7g" value="I1GVPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sS6SIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sS6SYwlEeisgcp1PNVE7g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sS6SowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sS6S4wlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sS6TIwlEeisgcp1PNVE7g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sThUIwlEeisgcp1PNVE7g" name="I1CJPC" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sThUYwlEeisgcp1PNVE7g" value="I1CJPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sThUowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sThU4wlEeisgcp1PNVE7g" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sThVIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sThVYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sThVowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sThV4wlEeisgcp1PNVE7g" name="I1C0S1" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sThWIwlEeisgcp1PNVE7g" value="I1C0S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sThWYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sThWowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sThW4wlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sThXIwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sUIYIwlEeisgcp1PNVE7g" name="I1DYQT" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sUIYYwlEeisgcp1PNVE7g" value="I1DYQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sUIYowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sUIY4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sUIZIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sUIZYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sUIZowlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sUIZ4wlEeisgcp1PNVE7g" name="I1DXQT" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sUIaIwlEeisgcp1PNVE7g" value="I1DXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sUIaYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sUIaowlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sUIa4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sUIbIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sUIbYwlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sUvcIwlEeisgcp1PNVE7g" name="I1GQQT" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sUvcYwlEeisgcp1PNVE7g" value="I1GQQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sUvcowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sUvc4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sUvdIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sUvdYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sUvdowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sVWgIwlEeisgcp1PNVE7g" name="I1GRQT" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sVWgYwlEeisgcp1PNVE7g" value="I1GRQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sVWgowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sVWg4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sVWhIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sVWhYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sVWhowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sVWh4wlEeisgcp1PNVE7g" name="I1GSQT" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sVWiIwlEeisgcp1PNVE7g" value="I1GSQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sVWiYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sVWiowlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sVWi4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sVWjIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sVWjYwlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sV9kIwlEeisgcp1PNVE7g" name="I1GTQT" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sV9kYwlEeisgcp1PNVE7g" value="I1GTQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sV9kowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sV9k4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sV9lIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sV9lYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sV9lowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sV9l4wlEeisgcp1PNVE7g" name="I1GUQT" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sV9mIwlEeisgcp1PNVE7g" value="I1GUQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sV9mYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sV9mowlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sV9m4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sWkoIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sWkoYwlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sWkoowlEeisgcp1PNVE7g" name="I1GVQT" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sWko4wlEeisgcp1PNVE7g" value="I1GVQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sWkpIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sWkpYwlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sWkpowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sWkp4wlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sWkqIwlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sXLsIwlEeisgcp1PNVE7g" name="I1GWQT" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sXLsYwlEeisgcp1PNVE7g" value="I1GWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sXLsowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sXLs4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sXLtIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sXLtYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sXLtowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sXLt4wlEeisgcp1PNVE7g" name="I1GXQT" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sXLuIwlEeisgcp1PNVE7g" value="I1GXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sXLuYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sXLuowlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sXLu4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sXLvIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sXLvYwlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sXywIwlEeisgcp1PNVE7g" name="I1GYQT" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sXywYwlEeisgcp1PNVE7g" value="I1GYQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sXywowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sXyw4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sXyxIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sXyxYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sXyxowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sXyx4wlEeisgcp1PNVE7g" name="I1GZQT" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sXyyIwlEeisgcp1PNVE7g" value="I1GZQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sXyyYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sXyyowlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sXyy4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sXyzIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sXyzYwlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sYZ0IwlEeisgcp1PNVE7g" name="I1G0QT" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sYZ0YwlEeisgcp1PNVE7g" value="I1G0QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sYZ0owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sYZ04wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sYZ1IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sYZ1YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sYZ1owlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sZA4IwlEeisgcp1PNVE7g" name="I1G1QT" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sZA4YwlEeisgcp1PNVE7g" value="I1G1QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sZA4owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sZA44wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sZA5IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sZA5YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sZA5owlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sZA54wlEeisgcp1PNVE7g" name="I1DWQT" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sZA6IwlEeisgcp1PNVE7g" value="I1DWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sZA6YwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sZA6owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sZA64wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sZA7IwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sZA7YwlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sZn8IwlEeisgcp1PNVE7g" name="I1HUDT" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sZn8YwlEeisgcp1PNVE7g" value="I1HUDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sZn8owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sZn84wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sZn9IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sZn9YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sZn9owlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6saPAIwlEeisgcp1PNVE7g" name="I1YVNB" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_6saPAYwlEeisgcp1PNVE7g" value="I1YVNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6saPAowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6saPA4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6saPBIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6saPBYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6saPBowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6saPB4wlEeisgcp1PNVE7g" name="I1YWNB" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_6saPCIwlEeisgcp1PNVE7g" value="I1YWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6saPCYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6saPCowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6saPC4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6saPDIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6saPDYwlEeisgcp1PNVE7g" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sa2EIwlEeisgcp1PNVE7g" name="I1GOQT" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sa2EYwlEeisgcp1PNVE7g" value="I1GOQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sa2EowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sa2E4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sa2FIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sa2FYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sa2FowlEeisgcp1PNVE7g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sbdIIwlEeisgcp1PNVE7g" name="I1FNS2" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sbdIYwlEeisgcp1PNVE7g" value="I1FNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sbdIowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sbdI4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sbdJIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sbdJYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sbdJowlEeisgcp1PNVE7g" name="I1FOS2" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sbdJ4wlEeisgcp1PNVE7g" value="I1FOS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sbdKIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sbdKYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sbdKowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sbdK4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6scEMIwlEeisgcp1PNVE7g" name="I1FPS2" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_6scEMYwlEeisgcp1PNVE7g" value="I1FPS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6scEMowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6scEM4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6scENIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6scENYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6scENowlEeisgcp1PNVE7g" name="I1FQS2" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_6scEN4wlEeisgcp1PNVE7g" value="I1FQS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6scEOIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6scEOYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6scEOowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6scEO4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6scrQIwlEeisgcp1PNVE7g" name="I1FRS2" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_6scrQYwlEeisgcp1PNVE7g" value="I1FRS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6scrQowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6scrQ4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6scrRIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6scrRYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sdSUIwlEeisgcp1PNVE7g" name="I1FSS2" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sdSUYwlEeisgcp1PNVE7g" value="I1FSS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sdSUowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sdSU4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sdSVIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sdSVYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sdSVowlEeisgcp1PNVE7g" name="I1FTS2" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sdSV4wlEeisgcp1PNVE7g" value="I1FTS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sdSWIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sdSWYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sdSWowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sdSW4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sd5YIwlEeisgcp1PNVE7g" name="I1FUS2" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sd5YYwlEeisgcp1PNVE7g" value="I1FUS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sd5YowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sd5Y4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sd5ZIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sd5ZYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sd5ZowlEeisgcp1PNVE7g" name="I1FVS2" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sd5Z4wlEeisgcp1PNVE7g" value="I1FVS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sd5aIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sd5aYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sd5aowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sd5a4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6segcIwlEeisgcp1PNVE7g" name="I1FWS2" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_6segcYwlEeisgcp1PNVE7g" value="I1FWS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6segcowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6segc4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6segdIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6segdYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sfHgIwlEeisgcp1PNVE7g" name="I1FXS2" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sfHgYwlEeisgcp1PNVE7g" value="I1FXS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sfHgowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sfHg4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sfHhIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sfHhYwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sfHhowlEeisgcp1PNVE7g" name="I1FYS2" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sfHh4wlEeisgcp1PNVE7g" value="I1FYS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sfHiIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sfHiYwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sfHiowlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sfHi4wlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sfukIwlEeisgcp1PNVE7g" name="I1YSNB" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sfukYwlEeisgcp1PNVE7g" value="I1YSNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sfukowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sfuk4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sfulIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sfulYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sfulowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sgVoIwlEeisgcp1PNVE7g" name="I1YTNB" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sgVoYwlEeisgcp1PNVE7g" value="I1YTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sgVoowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sgVo4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sgVpIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sgVpYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sgVpowlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sgVp4wlEeisgcp1PNVE7g" name="I1YUNB" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sgVqIwlEeisgcp1PNVE7g" value="I1YUNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sgVqYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sgVqowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sgVq4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sgVrIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sgVrYwlEeisgcp1PNVE7g" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sg8sIwlEeisgcp1PNVE7g" name="I1GLQT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sg8sYwlEeisgcp1PNVE7g" value="I1GLQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sg8sowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sg8s4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sg8tIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6sg8tYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6sg8towlEeisgcp1PNVE7g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6sg8t4wlEeisgcp1PNVE7g" name="I1GMQT" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_6sg8uIwlEeisgcp1PNVE7g" value="I1GMQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6sg8uYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6sg8uowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6sg8u4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6shjwIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6shjwYwlEeisgcp1PNVE7g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6shjwowlEeisgcp1PNVE7g" name="I1GNQT" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_6shjw4wlEeisgcp1PNVE7g" value="I1GNQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6shjxIwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6shjxYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6shjxowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6shjx4wlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6shjyIwlEeisgcp1PNVE7g" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6siK0IwlEeisgcp1PNVE7g" name="I1JLDT" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_6siK0YwlEeisgcp1PNVE7g" value="I1JLDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6siK0owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6siK04wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6siK1IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6siK1YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6siK1owlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6six4IwlEeisgcp1PNVE7g" name="I1AADT" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_6six4YwlEeisgcp1PNVE7g" value="I1AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6six4owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6six44wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6six5IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6six5YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6six5owlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6six54wlEeisgcp1PNVE7g" name="I1AATX" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_6six6IwlEeisgcp1PNVE7g" value="I1AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6six6YwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6six6owlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6six64wlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6six7IwlEeisgcp1PNVE7g" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_6bA9AIwlEeisgcp1PNVE7g" name="OTIMCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_6bBkEIwlEeisgcp1PNVE7g" value="OTIMCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_6bBkEYwlEeisgcp1PNVE7g" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_6bznMIwlEeisgcp1PNVE7g" value="OTIMCPP   "/>
      <node defType="com.stambia.rdbms.column" id="_6lRFMIwlEeisgcp1PNVE7g" name="IMB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lRFMYwlEeisgcp1PNVE7g" value="IMB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lRFMowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lRFM4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lRFNIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lRFNYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lRFNowlEeisgcp1PNVE7g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lSTUIwlEeisgcp1PNVE7g" name="IMVBT2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lSTUYwlEeisgcp1PNVE7g" value="IMVBT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lSTUowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lSTU4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lSTVIwlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lSTVYwlEeisgcp1PNVE7g" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lS6YIwlEeisgcp1PNVE7g" name="IMYYDT" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lS6YYwlEeisgcp1PNVE7g" value="IMYYDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lS6YowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lS6Y4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lS6ZIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lS6ZYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lS6ZowlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lS6Z4wlEeisgcp1PNVE7g" name="IMONS3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lS6aIwlEeisgcp1PNVE7g" value="IMONS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lS6aYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lS6aowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lS6a4wlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lS6bIwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lThcIwlEeisgcp1PNVE7g" name="IMX2P3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lThcYwlEeisgcp1PNVE7g" value="IMX2P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lThcowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lThc4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lThdIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lThdYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lThdowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lUIgIwlEeisgcp1PNVE7g" name="IMX3P3" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lUIgYwlEeisgcp1PNVE7g" value="IMX3P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lUIgowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lUIg4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lUIhIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lUIhYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lUIhowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lUvkIwlEeisgcp1PNVE7g" name="IMX4P3" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lUvkYwlEeisgcp1PNVE7g" value="IMX4P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lUvkowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lUvk4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lUvlIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lUvlYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lUvlowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lUvl4wlEeisgcp1PNVE7g" name="IMX5P3" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lUvmIwlEeisgcp1PNVE7g" value="IMX5P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lUvmYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lUvmowlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lUvm4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lUvnIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lUvnYwlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lVWoIwlEeisgcp1PNVE7g" name="IMX6P3" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lVWoYwlEeisgcp1PNVE7g" value="IMX6P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lVWoowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lVWo4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lVWpIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lVWpYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lVWpowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lV9sIwlEeisgcp1PNVE7g" name="IMX7P3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lV9sYwlEeisgcp1PNVE7g" value="IMX7P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lV9sowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lV9s4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lV9tIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lV9tYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lV9towlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lWkwIwlEeisgcp1PNVE7g" name="IMX8P3" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lWkwYwlEeisgcp1PNVE7g" value="IMX8P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lWkwowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lWkw4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lWkxIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lWkxYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lWkxowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lWkx4wlEeisgcp1PNVE7g" name="IMX9P3" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lXL0IwlEeisgcp1PNVE7g" value="IMX9P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lXL0YwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lXL0owlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lXL04wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lXL1IwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lXL1YwlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lXL1owlEeisgcp1PNVE7g" name="IMYAP3" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lXL14wlEeisgcp1PNVE7g" value="IMYAP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lXL2IwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lXL2YwlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lXL2owlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lXL24wlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lXL3IwlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lXy4IwlEeisgcp1PNVE7g" name="IMYBP3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lXy4YwlEeisgcp1PNVE7g" value="IMYBP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lXy4owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lXy44wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lXy5IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lYZ8IwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lYZ8YwlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lZBAIwlEeisgcp1PNVE7g" name="IMYCP3" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lZBAYwlEeisgcp1PNVE7g" value="IMYCP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lZBAowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lZBA4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lZBBIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lZBBYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lZBBowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lZoEIwlEeisgcp1PNVE7g" name="IMYDP3" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lZoEYwlEeisgcp1PNVE7g" value="IMYDP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lZoEowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lZoE4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lZoFIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lZoFYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lZoFowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6laPIIwlEeisgcp1PNVE7g" name="IMYZDT" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_6laPIYwlEeisgcp1PNVE7g" value="IMYZDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6laPIowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6laPI4wlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6laPJIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6laPJYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6laPJowlEeisgcp1PNVE7g" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6laPJ4wlEeisgcp1PNVE7g" name="IMOOS3" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_6laPKIwlEeisgcp1PNVE7g" value="IMOOS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6laPKYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6laPKowlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6laPK4wlEeisgcp1PNVE7g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6laPLIwlEeisgcp1PNVE7g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lbdQIwlEeisgcp1PNVE7g" name="IMYEP3" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lbdQYwlEeisgcp1PNVE7g" value="IMYEP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lbdQowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lbdQ4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lbdRIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lbdRYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lbdRowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lcEUIwlEeisgcp1PNVE7g" name="IMYFP3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lcEUYwlEeisgcp1PNVE7g" value="IMYFP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lcEUowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lcEU4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lcEVIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lcEVYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lcEVowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lcrYIwlEeisgcp1PNVE7g" name="IMYGP3" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lcrYYwlEeisgcp1PNVE7g" value="IMYGP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lcrYowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lcrY4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lcrZIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lcrZYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lcrZowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6ldScIwlEeisgcp1PNVE7g" name="IMYHP3" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_6ldScYwlEeisgcp1PNVE7g" value="IMYHP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6ldScowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6ldSc4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6ldSdIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6ldSdYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6ldSdowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6ld5gIwlEeisgcp1PNVE7g" name="IMYIP3" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_6ld5gYwlEeisgcp1PNVE7g" value="IMYIP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6ld5gowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6ld5g4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6ld5hIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6ld5hYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6ld5howlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6legkIwlEeisgcp1PNVE7g" name="IMYJP3" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_6legkYwlEeisgcp1PNVE7g" value="IMYJP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6legkowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6legk4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6leglIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6leglYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6leglowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lfHoIwlEeisgcp1PNVE7g" name="IMYKP3" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lfHoYwlEeisgcp1PNVE7g" value="IMYKP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lfHoowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lfHo4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lfHpIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lfHpYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lfHpowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lfusIwlEeisgcp1PNVE7g" name="IMYLP3" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lfusYwlEeisgcp1PNVE7g" value="IMYLP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lfusowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lfus4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lfutIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lfutYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lfutowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lgVwIwlEeisgcp1PNVE7g" name="IMYMP3" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lgVwYwlEeisgcp1PNVE7g" value="IMYMP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lgVwowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lgVw4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lgVxIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lgVxYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lgVxowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lg80IwlEeisgcp1PNVE7g" name="IMYNP3" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lg80YwlEeisgcp1PNVE7g" value="IMYNP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lg80owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lg804wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lg81IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lg81YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lg81owlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6liK8IwlEeisgcp1PNVE7g" name="IMYOP3" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_6liK8YwlEeisgcp1PNVE7g" value="IMYOP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6liK8owlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6liK84wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6liK9IwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6liK9YwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6liK9owlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6liyAIwlEeisgcp1PNVE7g" name="IMYPP3" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_6liyAYwlEeisgcp1PNVE7g" value="IMYPP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6liyAowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6liyA4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6liyBIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6liyBYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6liyBowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6liyB4wlEeisgcp1PNVE7g" name="IMYQP3" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_6liyCIwlEeisgcp1PNVE7g" value="IMYQP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6liyCYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6liyCowlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6liyC4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6liyDIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6liyDYwlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6ljZEIwlEeisgcp1PNVE7g" name="IMYRP3" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_6ljZEYwlEeisgcp1PNVE7g" value="IMYRP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6ljZEowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6ljZE4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6ljZFIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6ljZFYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6ljZFowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lkAIIwlEeisgcp1PNVE7g" name="IMYSP3" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lkAIYwlEeisgcp1PNVE7g" value="IMYSP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lkAIowlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lkAI4wlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lkAJIwlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lkAJYwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lkAJowlEeisgcp1PNVE7g" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_6lkAJ4wlEeisgcp1PNVE7g" name="IMYTP3" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_6lkAKIwlEeisgcp1PNVE7g" value="IMYTP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_6lkAKYwlEeisgcp1PNVE7g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_6lkAKowlEeisgcp1PNVE7g" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_6lkAK4wlEeisgcp1PNVE7g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_6lkALIwlEeisgcp1PNVE7g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_6lkALYwlEeisgcp1PNVE7g" value="11"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.queryFolder" id="_ubcxQY8mEeiPuKKuoQuq0g" name="BaseArticleAverifier">
    <node defType="com.stambia.rdbms.query" id="_GReyQI8nEeiPuKKuoQuq0g" name="testSqlSelecttarif">
      <attribute defType="com.stambia.rdbms.query.expression" id="_MH_SoI8nEeiPuKKuoQuq0g" value="Select i1b3nb, i1qqst, i1qpst, i1i4tx&#xD;&#xA;from orionbd.ori1rep as i1 &#xD;&#xA;inner join orionbd.otb8cpp on i1b3nb = b8b3nb and b8iqs3 &lt;> 'N'&#xD;&#xA;where trim(i1qqst) in ('V','F','C' ) and i1b3nb &lt; 999000  and trim(i1qpst) in ('1','2','Z') &#xD;&#xA;&#xD;&#xA;union&#xD;&#xA;&#xD;&#xA;Select i1b3nb, i1qqst, i1qpst, i1i4tx&#xD;&#xA;from orionbd.ori1rep as i1 &#xD;&#xA;exception join orionbd.otb8cpp on i1b3nb = b8b3nb&#xD;&#xA;where trim(i1qqst) in ('V','F','C' ) and i1b3nb &lt; 999000  and trim(i1qpst) in ('1','2','Z')"/>
      <node defType="com.stambia.rdbms.column" id="_RMqGYI8nEeiPuKKuoQuq0g" name="I1B3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_RMqGYY8nEeiPuKKuoQuq0g" value="I1B3NB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_RMqtcI8nEeiPuKKuoQuq0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_RMqtcY8nEeiPuKKuoQuq0g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_RMqtco8nEeiPuKKuoQuq0g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_RMqtc48nEeiPuKKuoQuq0g" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_RMqtdI8nEeiPuKKuoQuq0g" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_RMsioI8nEeiPuKKuoQuq0g" name="I1QQST" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_RMsioY8nEeiPuKKuoQuq0g" value="I1QQST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_RMsioo8nEeiPuKKuoQuq0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_RMsio48nEeiPuKKuoQuq0g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_RMsipI8nEeiPuKKuoQuq0g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_RMsipY8nEeiPuKKuoQuq0g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_RMtJsI8nEeiPuKKuoQuq0g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_RMtwwI8nEeiPuKKuoQuq0g" name="I1QPST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_RMtwwY8nEeiPuKKuoQuq0g" value="I1QPST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_RMtwwo8nEeiPuKKuoQuq0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_RMtww48nEeiPuKKuoQuq0g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_RMtwxI8nEeiPuKKuoQuq0g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_RMtwxY8nEeiPuKKuoQuq0g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_RMtwxo8nEeiPuKKuoQuq0g" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_RMuX0I8nEeiPuKKuoQuq0g" name="I1I4TX" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_RMuX0Y8nEeiPuKKuoQuq0g" value="I1I4TX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_RMuX0o8nEeiPuKKuoQuq0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_RMuX048nEeiPuKKuoQuq0g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_RMuX1I8nEeiPuKKuoQuq0g" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_RMuX1Y8nEeiPuKKuoQuq0g" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_RMuX1o8nEeiPuKKuoQuq0g" value="20"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_lgKRUP8JEemVAft-X6rPVg" name="STAMBIA">
    <attribute defType="com.stambia.rdbms.schema.name" id="_lhBM8P8JEemVAft-X6rPVg" value="STAMBIA"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_lhBM8f8JEemVAft-X6rPVg" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_lhBM8v8JEemVAft-X6rPVg" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_lhBM8_8JEemVAft-X6rPVg" value="I_[targetName]"/>
  </node>
</md:node>