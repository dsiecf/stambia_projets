<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.wsdl.wsdl" id="_aiaPYAd5EemyyJxuIKe_6A" name="IntegraCmd" md:ref="platform:/plugin/com.indy.environment/.tech/web/wsdl.tech#UUID_TECH_WSDL1?fileId=UUID_TECH_WSDL1$type=tech$name=wsdl?">
  <attribute defType="com.stambia.wsdl.wsdl.xsdReverseVersion" id="_aijZrQd5EemyyJxuIKe_6A" value="1"/>
  <attribute defType="com.stambia.wsdl.wsdl.url" id="_apegsAd5EemyyJxuIKe_6A" value="http://172.16.128.4:10080/wsprod/_gen/wsintegrationcommande_new/integrationcommande.php?wsdl"/>
  <attribute defType="com.stambia.wsdl.wsdl.prefixForElement" id="_O5ZIYAd6EemyyJxuIKe_6A" value="unqualified"/>
  <attribute defType="com.stambia.wsdl.wsdl.prefixForAttribute" id="_O5ZIYQd6EemyyJxuIKe_6A" value="unqualified"/>
  <attribute defType="com.stambia.wsdl.wsdl.targetNamespace" id="_O5ZIYgd6EemyyJxuIKe_6A" value="http://172.16.128.4:10080/wsprod/_gen/wsintegrationcommande_new/integrationcommande.php"/>
  <node defType="com.stambia.xml.namespace" id="_O5P-cAd6EemyyJxuIKe_6A" name="http://schemas.xmlsoap.org/wsdl/">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_O5P-cQd6EemyyJxuIKe_6A" value="ns"/>
  </node>
  <node defType="com.stambia.xml.namespace" id="_O5P-cgd6EemyyJxuIKe_6A" name="http://172.16.128.4:10080/wsprod/_gen/wsintegrationcommande_new/integrationcommande.php">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_O5P-cwd6EemyyJxuIKe_6A" value="tns"/>
  </node>
  <node defType="com.stambia.xml.namespace" id="_O5P-dAd6EemyyJxuIKe_6A" name="http://www.w3.org/2001/XMLSchema">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_O5P-dQd6EemyyJxuIKe_6A" value="xsd"/>
  </node>
  <node defType="com.stambia.xml.namespace" id="_O5P-dgd6EemyyJxuIKe_6A" name="http://schemas.xmlsoap.org/soap/encoding/">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_O5P-dwd6EemyyJxuIKe_6A" value="soap-enc"/>
  </node>
  <node defType="com.stambia.xml.namespace" id="_O5P-eAd6EemyyJxuIKe_6A" name="http://schemas.xmlsoap.org/wsdl/soap/">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_O5P-eQd6EemyyJxuIKe_6A" value="soap"/>
  </node>
  <node defType="com.stambia.wsdl.service" id="_O5P-fQd6EemyyJxuIKe_6A" name="integrationcommandeService">
    <node defType="com.stambia.wsdl.port" id="_O5P-fgd6EemyyJxuIKe_6A" name="integrationcommandePort">
      <attribute defType="com.stambia.wsdl.port.address" id="_O5P-fwd6EemyyJxuIKe_6A" value="http://172.16.128.4:10080/wsprod/_gen/wsintegrationcommande_new/integrationcommande.php"/>
      <attribute defType="com.stambia.wsdl.port.protocol" id="_O5P-gAd6EemyyJxuIKe_6A" value="SOAP"/>
      <attribute defType="com.stambia.wsdl.port.transportURI" id="_O5P-gQd6EemyyJxuIKe_6A" value="http://schemas.xmlsoap.org/soap/http"/>
      <attribute defType="com.stambia.wsdl.port.style" id="_O5P-ggd6EemyyJxuIKe_6A" value="rpc"/>
      <node defType="com.stambia.wsdl.operation" id="_O5P-gwd6EemyyJxuIKe_6A" name="TrtTest">
        <attribute defType="com.stambia.wsdl.operation.style" id="_O5P-lAd6EemyyJxuIKe_6A"/>
        <attribute defType="com.stambia.wsdl.operation.actionURI" id="_O5P-lQd6EemyyJxuIKe_6A" value="http://172.16.128.4:10080/wsprod/_gen/wsintegrationcommande_new/integrationcommande.php#TrtTest"/>
        <node defType="com.stambia.wsdl.input" id="_O5P-hAd6EemyyJxuIKe_6A">
          <node defType="com.stambia.wsdl.part" id="_O5P-hQd6EemyyJxuIKe_6A" name="strxml">
            <attribute defType="com.stambia.wsdl.part.originalType" id="_O5P-hgd6EemyyJxuIKe_6A" value="xsd:string"/>
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_O5P-hwd6EemyyJxuIKe_6A" value="soap:body"/>
            <attribute defType="com.stambia.wsdl.part.namespaceURI" id="_O5P-iAd6EemyyJxuIKe_6A" value="http://172.16.128.4:10080/wsprod/_gen/wsintegrationcommande_new/integrationcommande.php"/>
            <attribute defType="com.stambia.wsdl.part.encodingStyle" id="_O5P-iQd6EemyyJxuIKe_6A">
              <values>http://schemas.xmlsoap.org/soap/encoding/</values>
            </attribute>
            <attribute defType="com.stambia.wsdl.part.use" id="_O5P-igd6EemyyJxuIKe_6A" value="encoded"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_O5P-iwd6EemyyJxuIKe_6A" value="string"/>
          </node>
        </node>
        <node defType="com.stambia.wsdl.output" id="_O5P-jAd6EemyyJxuIKe_6A">
          <node defType="com.stambia.wsdl.part" id="_O5P-jQd6EemyyJxuIKe_6A" name="return">
            <attribute defType="com.stambia.wsdl.part.originalType" id="_O5P-jgd6EemyyJxuIKe_6A" value="xsd:string"/>
            <attribute defType="com.stambia.wsdl.part.bindingType" id="_O5P-jwd6EemyyJxuIKe_6A" value="soap:body"/>
            <attribute defType="com.stambia.wsdl.part.namespaceURI" id="_O5P-kAd6EemyyJxuIKe_6A" value="http://172.16.128.4:10080/wsprod/_gen/wsintegrationcommande_new/integrationcommande.php"/>
            <attribute defType="com.stambia.wsdl.part.encodingStyle" id="_O5P-kQd6EemyyJxuIKe_6A">
              <values>http://schemas.xmlsoap.org/soap/encoding/</values>
            </attribute>
            <attribute defType="com.stambia.wsdl.part.use" id="_O5P-kgd6EemyyJxuIKe_6A" value="encoded"/>
            <attribute defType="com.stambia.wsdl.part.type" id="_O5P-kwd6EemyyJxuIKe_6A" value="string"/>
          </node>
        </node>
        <node defType="com.stambia.wsdl.fault" id="_O5P-lgd6EemyyJxuIKe_6A" name="StandardFault">
          <node defType="com.stambia.wsdl.part" id="_O5P-lwd6EemyyJxuIKe_6A" name="fault">
            <node defType="com.stambia.xml.element" id="_O5P-mAd6EemyyJxuIKe_6A" name="faultcode">
              <attribute defType="com.stambia.xml.element.type" id="_O5P-mQd6EemyyJxuIKe_6A" value="string"/>
            </node>
            <node defType="com.stambia.xml.element" id="_O5P-mgd6EemyyJxuIKe_6A" name="faultstring">
              <attribute defType="com.stambia.xml.element.type" id="_O5P-mwd6EemyyJxuIKe_6A" value="string"/>
            </node>
            <node defType="com.stambia.xml.element" id="_O5P-nAd6EemyyJxuIKe_6A" name="faultactor">
              <attribute defType="com.stambia.xml.element.type" id="_O5P-nQd6EemyyJxuIKe_6A" value="string"/>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>