<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_icPloIv3EeiNxp5TL-ow8A" name="ExcelTarifs" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/excel/excel.rdbms.md#UUID_MD_RDBMS_MICROSOFT_EXCEL?fileId=UUID_MD_RDBMS_MICROSOFT_EXCEL$type=md$name=Microsoft%20Excel?">
  <attribute defType="com.stambia.rdbms.server.url" id="_sEAKEIv3EeiNxp5TL-ow8A" value="jdbc:stambia:excel://c:/app_dev/RecapVerif.xlsx?forceDatatypeAsString=true&amp;columnNameStyle=PRESERVE"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_sEBYMIv3EeiNxp5TL-ow8A" value="com.stambia.jdbc.driver.excel.XLSXDriver"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_sECmUIv3EeiNxp5TL-ow8A" value="3951C0D79B227B95C1DC348DD0BCE8F1"/>
  <node defType="com.stambia.rdbms.schema" id="_ikEr8Iv3EeiNxp5TL-ow8A" name="RecapVerif">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_ikLZoIv3EeiNxp5TL-ow8A" value="RecapVerif"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_ikMAsIv3EeiNxp5TL-ow8A" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_ikMAsYv3EeiNxp5TL-ow8A" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_ikMnwIv3EeiNxp5TL-ow8A" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_sDiQAYv3EeiNxp5TL-ow8A" name="Feuil1$">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_sDi3EIv3EeiNxp5TL-ow8A" value="Feuil1$"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_sDi3EYv3EeiNxp5TL-ow8A" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_sDwScIv3EeiNxp5TL-ow8A" name="Article" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwScYv3EeiNxp5TL-ow8A" value="Article"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwScov3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSc4v3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSdIv3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSdYv3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSdov3EeiNxp5TL-ow8A" name="Status" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSd4v3EeiNxp5TL-ow8A" value="Status"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwSeIv3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSeYv3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSeov3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSe4v3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSfIv3EeiNxp5TL-ow8A" name="Type" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSfYv3EeiNxp5TL-ow8A" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwSfov3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSf4v3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSgIv3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSgYv3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSgov3EeiNxp5TL-ow8A" name="Désignation" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSg4v3EeiNxp5TL-ow8A" value="Désignation"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwShIv3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwShYv3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwShov3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSh4v3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSiIv3EeiNxp5TL-ow8A" name="Libellé Erreur" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSiYv3EeiNxp5TL-ow8A" value="Libellé Erreur"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwSiov3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSi4v3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSjIv3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSjYv3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSjov3EeiNxp5TL-ow8A" name="Code erreur" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSj4v3EeiNxp5TL-ow8A" value="Code erreur"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwSkIv3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSkYv3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSkov3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSk4v3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSlIv3EeiNxp5TL-ow8A" name="T0" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSlYv3EeiNxp5TL-ow8A" value="T0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwSlov3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSl4v3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSmIv3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSmYv3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSmov3EeiNxp5TL-ow8A" name="T1" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSm4v3EeiNxp5TL-ow8A" value="T1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwSnIv3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSnYv3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSnov3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSn4v3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSoIv3EeiNxp5TL-ow8A" name="T2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSoYv3EeiNxp5TL-ow8A" value="T2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwSoov3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSo4v3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSpIv3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSpYv3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSpov3EeiNxp5TL-ow8A" name="T3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSp4v3EeiNxp5TL-ow8A" value="T3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwSqIv3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSqYv3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSqov3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSq4v3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSrIv3EeiNxp5TL-ow8A" name="T4" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSrYv3EeiNxp5TL-ow8A" value="T4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwSrov3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwSr4v3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwSsIv3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDwSsYv3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDwSsov3EeiNxp5TL-ow8A" name="T5" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDwSs4v3EeiNxp5TL-ow8A" value="T5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDwStIv3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDwStYv3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDwStov3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDw5gIv3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDw5gYv3EeiNxp5TL-ow8A" name="T6" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDw5gov3EeiNxp5TL-ow8A" value="T6"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDw5g4v3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDw5hIv3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDw5hYv3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDw5hov3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDw5h4v3EeiNxp5TL-ow8A" name="T7" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDw5iIv3EeiNxp5TL-ow8A" value="T7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDw5iYv3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDw5iov3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDw5i4v3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDw5jIv3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_sDw5jYv3EeiNxp5TL-ow8A" name="T8" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_sDw5jov3EeiNxp5TL-ow8A" value="T8"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_sDw5j4v3EeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_sDw5kIv3EeiNxp5TL-ow8A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_sDw5kYv3EeiNxp5TL-ow8A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_sDw5kov3EeiNxp5TL-ow8A" value="VARCHAR"/>
      </node>
    </node>
  </node>
</md:node>