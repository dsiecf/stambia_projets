<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.file.server" id="_-zjIwOMzEeigXob0_5VVUQ" md:ref="platform:/plugin/com.indy.environment/.tech/file/standard.file.md#UUID_TECH_FILE_MD?fileId=UUID_TECH_FILE_MD$type=md$name=File?">
  <node defType="com.stambia.file.directory" id="_-5pJsOMzEeigXob0_5VVUQ" name="Clients CSV">
    <attribute defType="com.stambia.file.directory.path" id="_-58EoOMzEeigXob0_5VVUQ" value="C:\app_dev"/>
    <node defType="com.stambia.file.file" id="_-58EoeMzEeigXob0_5VVUQ" name="Retour_CHD">
      <attribute defType="com.stambia.file.file.type" id="_-6PmoeMzEeigXob0_5VVUQ" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_-6PmouMzEeigXob0_5VVUQ"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_-6Pmo-MzEeigXob0_5VVUQ" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_-6PmpOMzEeigXob0_5VVUQ" value="3B"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_-6PmpeMzEeigXob0_5VVUQ"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_-6PmpuMzEeigXob0_5VVUQ" value="2C"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_-6Pmp-MzEeigXob0_5VVUQ" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_-6PmqOMzEeigXob0_5VVUQ" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_-6PmqeMzEeigXob0_5VVUQ" value="1"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_9IPoEOM0EeigXob0_5VVUQ" value="Retour_CHD.csv"/>
      <node defType="com.stambia.file.field" id="_bHC_EOM1EeigXob0_5VVUQ" name="mardi_ouvre2" position="61">
        <attribute defType="com.stambia.file.field.size" id="_bHC_EeM1EeigXob0_5VVUQ" value="65"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_EuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_E-M1EeigXob0_5VVUQ" value="MARDI_OUVRE2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-aOM1EeigXob0_5VVUQ" name="Complement Adresse" position="19">
        <attribute defType="com.stambia.file.field.size" id="_bHC-aeM1EeigXob0_5VVUQ" value="94"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-auM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-a-M1EeigXob0_5VVUQ" value="COMPLEMENT_ADRESSE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-tOM1EeigXob0_5VVUQ" name="Nb employes" position="38">
        <attribute defType="com.stambia.file.field.size" id="_bHC-teM1EeigXob0_5VVUQ" value="72"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-tuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-t-M1EeigXob0_5VVUQ" value="NB_EMPLOYES"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-dOM1EeigXob0_5VVUQ" name="Ville" position="22">
        <attribute defType="com.stambia.file.field.size" id="_bHC-deM1EeigXob0_5VVUQ" value="79"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-duM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-d-M1EeigXob0_5VVUQ" value="VILLE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_UOM1EeigXob0_5VVUQ" name="samedi_ouvre2" position="77">
        <attribute defType="com.stambia.file.field.size" id="_bHC_UeM1EeigXob0_5VVUQ" value="68"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_UuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_U-M1EeigXob0_5VVUQ" value="SAMEDI_OUVRE2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-POM1EeigXob0_5VVUQ" name="Dep" position="8">
        <attribute defType="com.stambia.file.field.size" id="_bHC-PeM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-PuM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-P-M1EeigXob0_5VVUQ" value="DEP"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-QOM1EeigXob0_5VVUQ" name="Phone" position="9">
        <attribute defType="com.stambia.file.field.size" id="_bHC-QeM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-QuM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-Q-M1EeigXob0_5VVUQ" value="PHONE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-ROM1EeigXob0_5VVUQ" name="SIREN" position="10">
        <attribute defType="com.stambia.file.field.size" id="_bHC-ReM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-RuM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-R-M1EeigXob0_5VVUQ" value="SIREN"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-qOM1EeigXob0_5VVUQ" name="Nb de chambres" position="35">
        <attribute defType="com.stambia.file.field.size" id="_bHC-qeM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-quM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-q-M1EeigXob0_5VVUQ" value="NB_DE_CHAMBRES"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-vOM1EeigXob0_5VVUQ" name="Ticket moyen repas" position="40">
        <attribute defType="com.stambia.file.field.size" id="_bHC-veM1EeigXob0_5VVUQ" value="68"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-vuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-v-M1EeigXob0_5VVUQ" value="TICKET_MOYEN_REPAS"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-0OM1EeigXob0_5VVUQ" name="Nb de lits" position="45">
        <attribute defType="com.stambia.file.field.size" id="_bHC-0eM1EeigXob0_5VVUQ" value="71"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-0uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-0-M1EeigXob0_5VVUQ" value="NB_DE_LITS"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-3OM1EeigXob0_5VVUQ" name="Date fermeture" position="48">
        <attribute defType="com.stambia.file.field.size" id="_bHC-3eM1EeigXob0_5VVUQ" value="60"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-3uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-3-M1EeigXob0_5VVUQ" value="DATE_FERMETURE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-UOM1EeigXob0_5VVUQ" name="Annee derniere commande" position="13">
        <attribute defType="com.stambia.file.field.size" id="_bHC-UeM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-UuM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-U-M1EeigXob0_5VVUQ" value="ANNEE_DERNIERE_COMMANDE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_LOM1EeigXob0_5VVUQ" name="jeudi_ferme1" position="68">
        <attribute defType="com.stambia.file.field.size" id="_bHC_LeM1EeigXob0_5VVUQ" value="95"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_LuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_L-M1EeigXob0_5VVUQ" value="JEUDI_FERME1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-TOM1EeigXob0_5VVUQ" name="Country" position="12">
        <attribute defType="com.stambia.file.field.size" id="_bHC-TeM1EeigXob0_5VVUQ" value="56"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-TuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-T-M1EeigXob0_5VVUQ" value="COUNTRY"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_VOM1EeigXob0_5VVUQ" name="samedi_ferme2" position="78">
        <attribute defType="com.stambia.file.field.size" id="_bHC_VeM1EeigXob0_5VVUQ" value="55"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_VuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_V-M1EeigXob0_5VVUQ" value="SAMEDI_FERME2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-VOM1EeigXob0_5VVUQ" name="Matche" position="14">
        <attribute defType="com.stambia.file.field.size" id="_bHC-VeM1EeigXob0_5VVUQ" value="53"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-VuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-V-M1EeigXob0_5VVUQ" value="MATCHE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-cOM1EeigXob0_5VVUQ" name="Code postal" position="21">
        <attribute defType="com.stambia.file.field.size" id="_bHC-ceM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-cuM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-c-M1EeigXob0_5VVUQ" value="CODE_POSTAL"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-fOM1EeigXob0_5VVUQ" name="Fax" position="24">
        <attribute defType="com.stambia.file.field.size" id="_bHC-feM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-fuM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-f-M1EeigXob0_5VVUQ" value="FAX"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-rOM1EeigXob0_5VVUQ" name="Chiffre Affaires" position="36">
        <attribute defType="com.stambia.file.field.size" id="_bHC-reM1EeigXob0_5VVUQ" value="78"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-ruM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-r-M1EeigXob0_5VVUQ" value="CHIFFRE_AFFAIRES"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-yOM1EeigXob0_5VVUQ" name="Type operation" position="43">
        <attribute defType="com.stambia.file.field.size" id="_bHC-yeM1EeigXob0_5VVUQ" value="61"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-yuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-y-M1EeigXob0_5VVUQ" value="TYPE_OPERATION"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-5OM1EeigXob0_5VVUQ" name="Interlocuteur" position="50">
        <attribute defType="com.stambia.file.field.size" id="_bHC-5eM1EeigXob0_5VVUQ" value="89"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-5uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-5-M1EeigXob0_5VVUQ" value="INTERLOCUTEUR"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-OOM1EeigXob0_5VVUQ" name="Statut de normalisation" position="7">
        <attribute defType="com.stambia.file.field.size" id="_bHC-OeM1EeigXob0_5VVUQ" value="52"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-OuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-O-M1EeigXob0_5VVUQ" value="STATUT_DE_NORMALISATION"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-kOM1EeigXob0_5VVUQ" name="Type" position="29">
        <attribute defType="com.stambia.file.field.size" id="_bHC-keM1EeigXob0_5VVUQ" value="105"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-kuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-k-M1EeigXob0_5VVUQ" value="TYPE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_IOM1EeigXob0_5VVUQ" name="mercredi_ouvre2" position="65">
        <attribute defType="com.stambia.file.field.size" id="_bHC_IeM1EeigXob0_5VVUQ" value="65"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_IuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_I-M1EeigXob0_5VVUQ" value="MERCREDI_OUVRE2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_MOM1EeigXob0_5VVUQ" name="jeudi_ouvre2" position="69">
        <attribute defType="com.stambia.file.field.size" id="_bHC_MeM1EeigXob0_5VVUQ" value="65"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_MuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_M-M1EeigXob0_5VVUQ" value="JEUDI_OUVRE2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_WOM1EeigXob0_5VVUQ" name="dimanche_ouvre1" position="79">
        <attribute defType="com.stambia.file.field.size" id="_bHC_WeM1EeigXob0_5VVUQ" value="63"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_WuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_W-M1EeigXob0_5VVUQ" value="DIMANCHE_OUVRE1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-IOM1EeigXob0_5VVUQ" name="CodCli" position="1">
        <attribute defType="com.stambia.file.field.size" id="_bHC-IeM1EeigXob0_5VVUQ" value="94"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-IuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-I-M1EeigXob0_5VVUQ" value="CODCLI"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-sOM1EeigXob0_5VVUQ" name="Annees Existence" position="37">
        <attribute defType="com.stambia.file.field.size" id="_bHC-seM1EeigXob0_5VVUQ" value="73"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-suM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-s-M1EeigXob0_5VVUQ" value="ANNEES_EXISTENCE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_POM1EeigXob0_5VVUQ" name="vendredi_ferme1" position="72">
        <attribute defType="com.stambia.file.field.size" id="_bHC_PeM1EeigXob0_5VVUQ" value="95"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_PuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_P-M1EeigXob0_5VVUQ" value="VENDREDI_FERME1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-SOM1EeigXob0_5VVUQ" name="Nic" position="11">
        <attribute defType="com.stambia.file.field.size" id="_bHC-SeM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-SuM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-S-M1EeigXob0_5VVUQ" value="NIC"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-eOM1EeigXob0_5VVUQ" name="Telephone" position="23">
        <attribute defType="com.stambia.file.field.size" id="_bHC-eeM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-euM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-e-M1EeigXob0_5VVUQ" value="TELEPHONE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_HOM1EeigXob0_5VVUQ" name="mercredi_ferme1" position="64">
        <attribute defType="com.stambia.file.field.size" id="_bHC_HeM1EeigXob0_5VVUQ" value="95"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_HuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_H-M1EeigXob0_5VVUQ" value="MERCREDI_FERME1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-KOM1EeigXob0_5VVUQ" name="Enseigne" position="3">
        <attribute defType="com.stambia.file.field.size" id="_bHC-KeM1EeigXob0_5VVUQ" value="85"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-KuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-K-M1EeigXob0_5VVUQ" value="ENSEIGNE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-6OM1EeigXob0_5VVUQ" name="Latitude" position="51">
        <attribute defType="com.stambia.file.field.size" id="_bHC-6eM1EeigXob0_5VVUQ" value="74"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-6uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-6-M1EeigXob0_5VVUQ" value="LATITUDE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_NOM1EeigXob0_5VVUQ" name="jeudi_ferme2" position="70">
        <attribute defType="com.stambia.file.field.size" id="_bHC_NeM1EeigXob0_5VVUQ" value="55"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_NuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_N-M1EeigXob0_5VVUQ" value="JEUDI_FERME2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_TOM1EeigXob0_5VVUQ" name="samedi_ferme1" position="76">
        <attribute defType="com.stambia.file.field.size" id="_bHC_TeM1EeigXob0_5VVUQ" value="95"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_TuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_T-M1EeigXob0_5VVUQ" value="SAMEDI_FERME1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_COM1EeigXob0_5VVUQ" name="mardi_ouvre1" position="59">
        <attribute defType="com.stambia.file.field.size" id="_bHC_CeM1EeigXob0_5VVUQ" value="80"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_CuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_C-M1EeigXob0_5VVUQ" value="MARDI_OUVRE1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_KOM1EeigXob0_5VVUQ" name="jeudi_ouvre1" position="67">
        <attribute defType="com.stambia.file.field.size" id="_bHC_KeM1EeigXob0_5VVUQ" value="63"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_KuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_K-M1EeigXob0_5VVUQ" value="JEUDI_OUVRE1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-MOM1EeigXob0_5VVUQ" name="Code Postal normalisee" position="5">
        <attribute defType="com.stambia.file.field.size" id="_bHC-MeM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-MuM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-M-M1EeigXob0_5VVUQ" value="CODE_POSTAL_NORMALISEE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-mOM1EeigXob0_5VVUQ" name="Segment" position="31">
        <attribute defType="com.stambia.file.field.size" id="_bHC-meM1EeigXob0_5VVUQ" value="102"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-muM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-m-M1EeigXob0_5VVUQ" value="SEGMENT"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_DOM1EeigXob0_5VVUQ" name="mardi_ferme1" position="60">
        <attribute defType="com.stambia.file.field.size" id="_bHC_DeM1EeigXob0_5VVUQ" value="95"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_DuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_D-M1EeigXob0_5VVUQ" value="MARDI_FERME1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_YOM1EeigXob0_5VVUQ" name="dimanche_ouvre2" position="81">
        <attribute defType="com.stambia.file.field.size" id="_bHC_YeM1EeigXob0_5VVUQ" value="64"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_YuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_Y-M1EeigXob0_5VVUQ" value="DIMANCHE_OUVRE2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_OOM1EeigXob0_5VVUQ" name="vendredi_ouvre1" position="71">
        <attribute defType="com.stambia.file.field.size" id="_bHC_OeM1EeigXob0_5VVUQ" value="63"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_OuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_O-M1EeigXob0_5VVUQ" value="VENDREDI_OUVRE1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-9OM1EeigXob0_5VVUQ" name="Horaires" position="54">
        <attribute defType="com.stambia.file.field.size" id="_bHC-9eM1EeigXob0_5VVUQ" value="290"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-9uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-9-M1EeigXob0_5VVUQ" value="HORAIRES"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-jOM1EeigXob0_5VVUQ" name="Nom chaine" position="28">
        <attribute defType="com.stambia.file.field.size" id="_bHC-jeM1EeigXob0_5VVUQ" value="78"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-juM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-j-M1EeigXob0_5VVUQ" value="NOM_CHAINE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_QOM1EeigXob0_5VVUQ" name="vendredi_ouvre2" position="73">
        <attribute defType="com.stambia.file.field.size" id="_bHC_QeM1EeigXob0_5VVUQ" value="68"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_QuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_Q-M1EeigXob0_5VVUQ" value="VENDREDI_OUVRE2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_ZOM1EeigXob0_5VVUQ" name="dimanche_ferme2" position="82">
        <attribute defType="com.stambia.file.field.size" id="_bHC_ZeM1EeigXob0_5VVUQ" value="56"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_ZuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_Z-M1EeigXob0_5VVUQ" value="DIMANCHE_FERME2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_BOM1EeigXob0_5VVUQ" name="lundi_ferme2" position="58">
        <attribute defType="com.stambia.file.field.size" id="_bHC_BeM1EeigXob0_5VVUQ" value="80"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_BuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_B-M1EeigXob0_5VVUQ" value="LUNDI_FERME2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-nOM1EeigXob0_5VVUQ" name="Activite detaillee" position="32">
        <attribute defType="com.stambia.file.field.size" id="_bHC-neM1EeigXob0_5VVUQ" value="126"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-nuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-n-M1EeigXob0_5VVUQ" value="ACTIVITE_DETAILLEE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-2OM1EeigXob0_5VVUQ" name="Niveau de confiance" position="47">
        <attribute defType="com.stambia.file.field.size" id="_bHC-2eM1EeigXob0_5VVUQ" value="59"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-2uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-2-M1EeigXob0_5VVUQ" value="NIVEAU_DE_CONFIANCE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-wOM1EeigXob0_5VVUQ" name="Nb pieces de pains/jour" position="41">
        <attribute defType="com.stambia.file.field.size" id="_bHC-weM1EeigXob0_5VVUQ" value="50"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-wuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-w-M1EeigXob0_5VVUQ" value="NB_PIECES_DE_PAINS_JOUR"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-4OM1EeigXob0_5VVUQ" name="Date ouverture" position="49">
        <attribute defType="com.stambia.file.field.size" id="_bHC-4eM1EeigXob0_5VVUQ" value="60"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-4uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-4-M1EeigXob0_5VVUQ" value="DATE_OUVERTURE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-uOM1EeigXob0_5VVUQ" name="Nb de couverts/jour" position="39">
        <attribute defType="com.stambia.file.field.size" id="_bHC-ueM1EeigXob0_5VVUQ" value="80"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-uuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-u-M1EeigXob0_5VVUQ" value="NB_DE_COUVERTS_JOUR"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-WOM1EeigXob0_5VVUQ" name="CHD ID" position="15">
        <attribute defType="com.stambia.file.field.size" id="_bHC-WeM1EeigXob0_5VVUQ" value="68"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-WuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-W-M1EeigXob0_5VVUQ" value="CHD_ID"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-YOM1EeigXob0_5VVUQ" name="Denomination sociale" position="17">
        <attribute defType="com.stambia.file.field.size" id="_bHC-YeM1EeigXob0_5VVUQ" value="102"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-YuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-Y-M1EeigXob0_5VVUQ" value="DENOMINATION_SOCIALE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-_OM1EeigXob0_5VVUQ" name="lundi_ferme1" position="56">
        <attribute defType="com.stambia.file.field.size" id="_bHC-_eM1EeigXob0_5VVUQ" value="95"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-_uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-_-M1EeigXob0_5VVUQ" value="LUNDI_FERME1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-7OM1EeigXob0_5VVUQ" name="Longitude" position="52">
        <attribute defType="com.stambia.file.field.size" id="_bHC-7eM1EeigXob0_5VVUQ" value="74"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-7uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-7-M1EeigXob0_5VVUQ" value="LONGITUDE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-JOM1EeigXob0_5VVUQ" name="RS" position="2">
        <attribute defType="com.stambia.file.field.size" id="_bHC-JeM1EeigXob0_5VVUQ" value="90"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-JuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-J-M1EeigXob0_5VVUQ" value="RS"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_XOM1EeigXob0_5VVUQ" name="dimanche_ferme1" position="80">
        <attribute defType="com.stambia.file.field.size" id="_bHC_XeM1EeigXob0_5VVUQ" value="95"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_XuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_X-M1EeigXob0_5VVUQ" value="DIMANCHE_FERME1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-8OM1EeigXob0_5VVUQ" name="Presence terrasse" position="53">
        <attribute defType="com.stambia.file.field.size" id="_bHC-8eM1EeigXob0_5VVUQ" value="53"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-8uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-8-M1EeigXob0_5VVUQ" value="PRESENCE_TERRASSE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_SOM1EeigXob0_5VVUQ" name="samedi_ouvre1" position="75">
        <attribute defType="com.stambia.file.field.size" id="_bHC_SeM1EeigXob0_5VVUQ" value="63"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_SuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_S-M1EeigXob0_5VVUQ" value="SAMEDI_OUVRE1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-iOM1EeigXob0_5VVUQ" name="Site internet" position="27">
        <attribute defType="com.stambia.file.field.size" id="_bHC-ieM1EeigXob0_5VVUQ" value="305"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-iuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-i-M1EeigXob0_5VVUQ" value="SITE_INTERNET"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-oOM1EeigXob0_5VVUQ" name="Type de menu" position="33">
        <attribute defType="com.stambia.file.field.size" id="_bHC-oeM1EeigXob0_5VVUQ" value="98"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-ouM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-o-M1EeigXob0_5VVUQ" value="TYPE_DE_MENU"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_FOM1EeigXob0_5VVUQ" name="mardi_ferme2" position="62">
        <attribute defType="com.stambia.file.field.size" id="_bHC_FeM1EeigXob0_5VVUQ" value="55"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_FuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_F-M1EeigXob0_5VVUQ" value="MARDI_FERME2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-zOM1EeigXob0_5VVUQ" name="Nb repas par jours" position="44">
        <attribute defType="com.stambia.file.field.size" id="_bHC-zeM1EeigXob0_5VVUQ" value="80"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-zuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-z-M1EeigXob0_5VVUQ" value="NB_REPAS_PAR_JOURS"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_AOM1EeigXob0_5VVUQ" name="lundi_ouvre2" position="57">
        <attribute defType="com.stambia.file.field.size" id="_bHC_AeM1EeigXob0_5VVUQ" value="80"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_AuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_A-M1EeigXob0_5VVUQ" value="LUNDI_OUVRE2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-gOM1EeigXob0_5VVUQ" name="SIRET" position="25">
        <attribute defType="com.stambia.file.field.size" id="_bHC-geM1EeigXob0_5VVUQ" value="14"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-guM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-g-M1EeigXob0_5VVUQ" value="SIRET"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-xOM1EeigXob0_5VVUQ" name="Propriete" position="42">
        <attribute defType="com.stambia.file.field.size" id="_bHC-xeM1EeigXob0_5VVUQ" value="61"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-xuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-x-M1EeigXob0_5VVUQ" value="PROPRIETE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-NOM1EeigXob0_5VVUQ" name="Ville normalisee" position="6">
        <attribute defType="com.stambia.file.field.size" id="_bHC-NeM1EeigXob0_5VVUQ" value="76"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-NuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-N-M1EeigXob0_5VVUQ" value="VILLE_NORMALISEE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_JOM1EeigXob0_5VVUQ" name="mercredi_ferme2" position="66">
        <attribute defType="com.stambia.file.field.size" id="_bHC_JeM1EeigXob0_5VVUQ" value="55"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_JuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_J-M1EeigXob0_5VVUQ" value="MERCREDI_FERME2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-pOM1EeigXob0_5VVUQ" name="Nb de chambres tranche" position="34">
        <attribute defType="com.stambia.file.field.size" id="_bHC-peM1EeigXob0_5VVUQ" value="68"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-puM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-p-M1EeigXob0_5VVUQ" value="NB_DE_CHAMBRES_TRANCHE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_GOM1EeigXob0_5VVUQ" name="mercredi_ouvre1" position="63">
        <attribute defType="com.stambia.file.field.size" id="_bHC_GeM1EeigXob0_5VVUQ" value="63"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_GuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_G-M1EeigXob0_5VVUQ" value="MERCREDI_OUVRE1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-ZOM1EeigXob0_5VVUQ" name="Adresse" position="18">
        <attribute defType="com.stambia.file.field.size" id="_bHC-ZeM1EeigXob0_5VVUQ" value="100"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-ZuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-Z-M1EeigXob0_5VVUQ" value="ADRESSE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC--OM1EeigXob0_5VVUQ" name="lundi_ouvre1" position="55">
        <attribute defType="com.stambia.file.field.size" id="_bHC--eM1EeigXob0_5VVUQ" value="94"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC--uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC---M1EeigXob0_5VVUQ" value="LUNDI_OUVRE1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-XOM1EeigXob0_5VVUQ" name="Nom commercial" position="16">
        <attribute defType="com.stambia.file.field.size" id="_bHC-XeM1EeigXob0_5VVUQ" value="122"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-XuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-X-M1EeigXob0_5VVUQ" value="NOM_COMMERCIAL"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-lOM1EeigXob0_5VVUQ" name="GFC Code" position="30">
        <attribute defType="com.stambia.file.field.size" id="_bHC-leM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-luM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-l-M1EeigXob0_5VVUQ" value="GFC_CODE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-hOM1EeigXob0_5VVUQ" name="SIREN1" position="26">
        <attribute defType="com.stambia.file.field.size" id="_bHC-heM1EeigXob0_5VVUQ" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-huM1EeigXob0_5VVUQ" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-h-M1EeigXob0_5VVUQ" value="SIREN1"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC_ROM1EeigXob0_5VVUQ" name="vendredi_ferme2" position="74">
        <attribute defType="com.stambia.file.field.size" id="_bHC_ReM1EeigXob0_5VVUQ" value="55"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC_RuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC_R-M1EeigXob0_5VVUQ" value="VENDREDI_FERME2"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-1OM1EeigXob0_5VVUQ" name="Nb eleves" position="46">
        <attribute defType="com.stambia.file.field.size" id="_bHC-1eM1EeigXob0_5VVUQ" value="74"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-1uM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-1-M1EeigXob0_5VVUQ" value="NB_ELEVES"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-LOM1EeigXob0_5VVUQ" name="Adresse normalisee" position="4">
        <attribute defType="com.stambia.file.field.size" id="_bHC-LeM1EeigXob0_5VVUQ" value="108"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-LuM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-L-M1EeigXob0_5VVUQ" value="ADRESSE_NORMALISEE"/>
      </node>
      <node defType="com.stambia.file.field" id="_bHC-bOM1EeigXob0_5VVUQ" name="Lieu-dit, boite postale" position="20">
        <attribute defType="com.stambia.file.field.size" id="_bHC-beM1EeigXob0_5VVUQ" value="67"/>
        <attribute defType="com.stambia.file.field.type" id="_bHC-buM1EeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_bHC-b-M1EeigXob0_5VVUQ" value="LIEU_DIT__BOITE_POSTALE"/>
      </node>
      <node defType="com.stambia.file.field" id="_t9-tzONIEeigXob0_5VVUQ" name="Chiffre affaires" position="36">
        <attribute defType="com.stambia.file.field.size" id="_t9-tzeNIEeigXob0_5VVUQ" value="78"/>
        <attribute defType="com.stambia.file.field.type" id="_t9-tzuNIEeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_t9-tz-NIEeigXob0_5VVUQ" value="CHIFFRE_AFFAIRES"/>
      </node>
      <node defType="com.stambia.file.field" id="_t9-t0ONIEeigXob0_5VVUQ" name="Annees existence" position="37">
        <attribute defType="com.stambia.file.field.size" id="_t9-t0eNIEeigXob0_5VVUQ" value="73"/>
        <attribute defType="com.stambia.file.field.type" id="_t9-t0uNIEeigXob0_5VVUQ" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_t9-t0-NIEeigXob0_5VVUQ" value="ANNEES_EXISTENCE"/>
      </node>
    </node>
  </node>
</md:node>