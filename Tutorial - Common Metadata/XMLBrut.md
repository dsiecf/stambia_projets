<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.file.server" id="_N9U7AAd1Eem4BaSelO6HHA" md:ref="platform:/plugin/com.indy.environment/.tech/file/standard.file.md#UUID_TECH_FILE_MD?fileId=UUID_TECH_FILE_MD$type=md$name=File?">
  <node defType="com.stambia.file.directory" id="_ODvFAAd1Eem4BaSelO6HHA" name="OrderXml">
    <attribute defType="com.stambia.file.directory.path" id="_OEB_8Ad1Eem4BaSelO6HHA" value="C:\app_dev\Used_Files\Out_Files\Xml"/>
    <node defType="com.stambia.file.file" id="_OEB_8Qd1Eem4BaSelO6HHA" name="temporder_1545652922531">
      <attribute defType="com.stambia.file.file.type" id="_OEn10Qd1Eem4BaSelO6HHA" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_OEn10gd1Eem4BaSelO6HHA"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_OEn10wd1Eem4BaSelO6HHA" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_OEn11Ad1Eem4BaSelO6HHA" value="3B"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_OEn11Qd1Eem4BaSelO6HHA"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_OEn11gd1Eem4BaSelO6HHA" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_OEn11wd1Eem4BaSelO6HHA" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_OEn12Ad1Eem4BaSelO6HHA" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_OEn12Qd1Eem4BaSelO6HHA" value="0"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_gqtnMAd1Eem4BaSelO6HHA" value="temporder_1545652922531.xml"/>
      <node defType="com.stambia.file.field" id="_kKQQMAd1Eem4BaSelO6HHA" name="F1" position="1">
        <attribute defType="com.stambia.file.field.size" id="_kKQQMQd1Eem4BaSelO6HHA" value="20000"/>
        <attribute defType="com.stambia.file.field.type" id="_kKQQMgd1Eem4BaSelO6HHA" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_kKQQMwd1Eem4BaSelO6HHA" value="F1"/>
      </node>
      <node defType="com.stambia.file.propertyField" id="_N_UZ5AeHEemyyJxuIKe_6A" name="string_content"/>
    </node>
  </node>
</md:node>