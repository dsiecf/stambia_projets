<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.file.server" id="_ekw4UA6_EeuGaoQ5wj6kWA" md:ref="platform:/plugin/com.indy.environment/technology/file/standard.file.md#UUID_TECH_FILE_MD?fileId=UUID_TECH_FILE_MD$type=md$name=File?">
  <node defType="com.stambia.file.directory" id="_ezTqoA6_EeuGaoQ5wj6kWA" name="export_commande.csv">
    <attribute defType="com.stambia.file.directory.path" id="_e04_AA6_EeuGaoQ5wj6kWA" value="C:\app_dev\Used_Files\In_Files\Reference_Files"/>
    <node defType="com.stambia.file.file" id="_e093gA6_EeuGaoQ5wj6kWA" name="export_commande">
      <attribute defType="com.stambia.file.file.type" id="_e2rHsA6_EeuGaoQ5wj6kWA" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_e2uyEA6_EeuGaoQ5wj6kWA" value="Cp1250"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_e23U8A6_EeuGaoQ5wj6kWA" value="0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_e23U8Q6_EeuGaoQ5wj6kWA" value="2C"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_e238AA6_EeuGaoQ5wj6kWA"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_e24jEA6_EeuGaoQ5wj6kWA" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_e24jEQ6_EeuGaoQ5wj6kWA" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_e25KIA6_EeuGaoQ5wj6kWA" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_e25KIQ6_EeuGaoQ5wj6kWA" value="0"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_6dLJoA6_EeuGaoQ5wj6kWA" value="export_commande.csv"/>
      <node defType="com.stambia.file.field" id="_7GBOKA7GEeuGaoQ5wj6kWA" name="id_magento_order" position="3">
        <attribute defType="com.stambia.file.field.size" id="_7GBOKQ7GEeuGaoQ5wj6kWA" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_7GBOKg7GEeuGaoQ5wj6kWA" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_7GBOKw7GEeuGaoQ5wj6kWA" value="F3"/>
      </node>
      <node defType="com.stambia.file.field" id="_7GBOIA7GEeuGaoQ5wj6kWA" name="id_societe_magento" position="1">
        <attribute defType="com.stambia.file.field.size" id="_7GBOIQ7GEeuGaoQ5wj6kWA" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_7GBOIg7GEeuGaoQ5wj6kWA" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_7GBOIw7GEeuGaoQ5wj6kWA" value="F1"/>
      </node>
      <node defType="com.stambia.file.field" id="_7GBOJA7GEeuGaoQ5wj6kWA" name="id_societe_chomette" position="2">
        <attribute defType="com.stambia.file.field.size" id="_7GBOJQ7GEeuGaoQ5wj6kWA" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_7GBOJg7GEeuGaoQ5wj6kWA" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_7GBOJw7GEeuGaoQ5wj6kWA" value="F2"/>
      </node>
      <node defType="com.stambia.file.field" id="_7GBOLA7GEeuGaoQ5wj6kWA" name="id_chomette_order" position="4">
        <attribute defType="com.stambia.file.field.size" id="_7GBOLQ7GEeuGaoQ5wj6kWA" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_7GBOLg7GEeuGaoQ5wj6kWA" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_7GBOLw7GEeuGaoQ5wj6kWA" value="F4"/>
      </node>
    </node>
  </node>
</md:node>