<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_O7EdkIteEeiNxp5TL-ow8A" name="TarifBackup" md:ref="../Stambia_chomette_com/ibm_db2_400.rdbms.md#UUID_MD_RDBMS_DB2_400?fileId=UUID_MD_RDBMS_DB2_400$type=md$name=IBM%20DB2/400?">
  <attribute defType="com.stambia.rdbms.server.url" id="_iMz0AIteEeiNxp5TL-ow8A" value="jdbc:as400://backup"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_iM7IwIteEeiNxp5TL-ow8A" value="com.ibm.as400.access.AS400JDBCDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_iM_aMIteEeiNxp5TL-ow8A" value="rhanem"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_iNFg0IteEeiNxp5TL-ow8A" value="6AED38D619FC2EDB3243FBF92069A87A"/>
  <node defType="com.stambia.rdbms.schema" id="_PC15gIteEeiNxp5TL-ow8A" name="ORIONBD">
    <attribute defType="com.stambia.rdbms.schema.name" id="_PDKpoIteEeiNxp5TL-ow8A" value="ORIONBD"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_PDL3wIteEeiNxp5TL-ow8A" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_PDMe0IteEeiNxp5TL-ow8A" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_PDNF4IteEeiNxp5TL-ow8A" value="I_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.work" id="_jjctAP8JEemVAft-X6rPVg" ref="#_gg8lUP8JEemVAft-X6rPVg?fileId=_O7EdkIteEeiNxp5TL-ow8A$type=md$name=STAMBIA?"/>
    <node defType="com.stambia.rdbms.datastore" id="_hzJW0IteEeiNxp5TL-ow8A" name="OTB8CPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_hzJ94IteEeiNxp5TL-ow8A" value="OTB8CPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_hzJ94YteEeiNxp5TL-ow8A" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_hzPdcIteEeiNxp5TL-ow8A" value="OTB8CPP   "/>
      <node defType="com.stambia.rdbms.column" id="_hzptIIteEeiNxp5TL-ow8A" name="B8B3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzptIYteEeiNxp5TL-ow8A" value="B8B3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzptIoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hzptI4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzptJIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzptJYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzptJoteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzq7QIteEeiNxp5TL-ow8A" name="B8IMS3" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzq7QYteEeiNxp5TL-ow8A" value="B8IMS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzq7QoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzq7Q4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzq7RIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzq7RYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzriUIteEeiNxp5TL-ow8A" name="B8INS3" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzriUYteEeiNxp5TL-ow8A" value="B8INS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzriUoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzriU4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzriVIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzriVYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzsJYIteEeiNxp5TL-ow8A" name="B8IOS3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzsJYYteEeiNxp5TL-ow8A" value="B8IOS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzsJYoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzsJY4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzsJZIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzsJZYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hztXgIteEeiNxp5TL-ow8A" name="B8IPS3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_hztXgYteEeiNxp5TL-ow8A" value="B8IPS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hztXgoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hztXg4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hztXhIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hztXhYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzt-kIteEeiNxp5TL-ow8A" name="B8IQS3" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzt-kYteEeiNxp5TL-ow8A" value="B8IQS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzt-koteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzt-k4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzt-lIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzt-lYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzuloIteEeiNxp5TL-ow8A" name="B8IRS3" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzuloYteEeiNxp5TL-ow8A" value="B8IRS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzulooteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzulo4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzulpIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzulpYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzvMsIteEeiNxp5TL-ow8A" name="B8ITS3" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzvMsYteEeiNxp5TL-ow8A" value="B8ITS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzvMsoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzvMs4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzvMtIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzvMtYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzwa0IteEeiNxp5TL-ow8A" name="B8IUS3" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzwa0YteEeiNxp5TL-ow8A" value="B8IUS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzwa0oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzwa04teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzwa1IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzwa1YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzxB4IteEeiNxp5TL-ow8A" name="B8ISS3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzxB4YteEeiNxp5TL-ow8A" value="B8ISS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzxB4oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzxB44teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzxB5IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzxB5YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzyQAIteEeiNxp5TL-ow8A" name="B8IVS3" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzyQAYteEeiNxp5TL-ow8A" value="B8IVS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzyQAoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzyQA4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzyQBIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzyQBYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzy3EIteEeiNxp5TL-ow8A" name="B8IWS3" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzy3EYteEeiNxp5TL-ow8A" value="B8IWS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzy3EoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzy3E4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzy3FIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzy3FYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hzzeIIteEeiNxp5TL-ow8A" name="B8IXS3" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_hzzeIYteEeiNxp5TL-ow8A" value="B8IXS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hzzeIoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hzzeI4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hzzeJIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hzzeJYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz0FMIteEeiNxp5TL-ow8A" name="B8IYS3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz0FMYteEeiNxp5TL-ow8A" value="B8IYS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz0FMoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz0FM4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz0FNIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz0FNYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz1TUIteEeiNxp5TL-ow8A" name="B8JCS3" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz1TUYteEeiNxp5TL-ow8A" value="B8JCS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz1TUoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz1TU4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz1TVIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz1TVYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz16YIteEeiNxp5TL-ow8A" name="B8IZS3" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz16YYteEeiNxp5TL-ow8A" value="B8IZS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz16YoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz16Y4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz16ZIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz16ZYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz2hcIteEeiNxp5TL-ow8A" name="B8I0S3" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz2hcYteEeiNxp5TL-ow8A" value="B8I0S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz2hcoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz2hc4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz2hdIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz2hdYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz3IgIteEeiNxp5TL-ow8A" name="B8I1S3" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz3IgYteEeiNxp5TL-ow8A" value="B8I1S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz3IgoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz3Ig4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz3IhIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz3IhYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz4WoIteEeiNxp5TL-ow8A" name="B8I2S3" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz4WoYteEeiNxp5TL-ow8A" value="B8I2S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz4WooteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz4Wo4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz4WpIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz4WpYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz49sIteEeiNxp5TL-ow8A" name="B8I3S3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz49sYteEeiNxp5TL-ow8A" value="B8I3S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz49soteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz49s4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz49tIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz49tYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz5kwIteEeiNxp5TL-ow8A" name="B8I4S3" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz5kwYteEeiNxp5TL-ow8A" value="B8I4S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz5kwoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz5kw4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz5kxIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz5kxYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz6y4IteEeiNxp5TL-ow8A" name="B8R1C1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz6y4YteEeiNxp5TL-ow8A" value="B8R1C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz6y4oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz6y44teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz6y5IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz6y5YteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz7Z8IteEeiNxp5TL-ow8A" name="B8R2C1" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz7Z8YteEeiNxp5TL-ow8A" value="B8R2C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz7Z8oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz7Z84teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz7Z9IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz7Z9YteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz8BAIteEeiNxp5TL-ow8A" name="B8R3C1" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz8BAYteEeiNxp5TL-ow8A" value="B8R3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz8BAoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz8BA4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz8BBIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz8BBYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz9PIIteEeiNxp5TL-ow8A" name="B8R4C1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz9PIYteEeiNxp5TL-ow8A" value="B8R4C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz9PIoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz9PI4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz9PJIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz9PJYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz92MIteEeiNxp5TL-ow8A" name="B8R5C1" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz92MYteEeiNxp5TL-ow8A" value="B8R5C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz92MoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz92M4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz92NIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz92NYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz_EUIteEeiNxp5TL-ow8A" name="B8I5S3" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz_EUYteEeiNxp5TL-ow8A" value="B8I5S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz_EUoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz_EU4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz_EVIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz_EVYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hz_rYIteEeiNxp5TL-ow8A" name="B8R6C1" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_hz_rYYteEeiNxp5TL-ow8A" value="B8R6C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hz_rYoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hz_rY4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hz_rZIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hz_rZYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0AScIteEeiNxp5TL-ow8A" name="B8R7C1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0AScYteEeiNxp5TL-ow8A" value="B8R7C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0AScoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0ASc4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0ASdIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0ASdYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0BgkIteEeiNxp5TL-ow8A" name="B8R8C1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0BgkYteEeiNxp5TL-ow8A" value="B8R8C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0BgkoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0Bgk4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0BglIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0BglYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0CHoIteEeiNxp5TL-ow8A" name="B8R9C1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0CHoYteEeiNxp5TL-ow8A" value="B8R9C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0CHooteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0CHo4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0CHpIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0CHpYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0CusIteEeiNxp5TL-ow8A" name="B8SAC1" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0CusYteEeiNxp5TL-ow8A" value="B8SAC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0CusoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0Cus4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0CutIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0CutYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0D80IteEeiNxp5TL-ow8A" name="B8I6S3" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0D80YteEeiNxp5TL-ow8A" value="B8I6S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0D80oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0D804teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0D81IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0D81YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0Ej4IteEeiNxp5TL-ow8A" name="B8SBC1" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0Ej4YteEeiNxp5TL-ow8A" value="B8SBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0Ej4oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0Ej44teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0Ej5IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0Ej5YteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0FK8IteEeiNxp5TL-ow8A" name="B8SCC1" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0FK8YteEeiNxp5TL-ow8A" value="B8SCC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0FK8oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0FK84teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0FK9IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0FK9YteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0GZEIteEeiNxp5TL-ow8A" name="B8SDC1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0GZEYteEeiNxp5TL-ow8A" value="B8SDC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0GZEoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0GZE4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0GZFIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0GZFYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0HAIIteEeiNxp5TL-ow8A" name="B8SEC1" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0HAIYteEeiNxp5TL-ow8A" value="B8SEC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0HAIoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0HAI4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0HAJIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0HAJYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0HAJoteEeiNxp5TL-ow8A" name="B8SFC1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0HAJ4teEeiNxp5TL-ow8A" value="B8SFC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0HAKIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0HAKYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0HAKoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0HAK4teEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0HnMIteEeiNxp5TL-ow8A" name="B8I7S3" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0HnMYteEeiNxp5TL-ow8A" value="B8I7S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0HnMoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0HnM4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0HnNIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0HnNYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0IOQIteEeiNxp5TL-ow8A" name="B8I8S3" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0IOQYteEeiNxp5TL-ow8A" value="B8I8S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0IOQoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0IOQ4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0IORIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0IORYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0I1UIteEeiNxp5TL-ow8A" name="B8I9S3" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0I1UYteEeiNxp5TL-ow8A" value="B8I9S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0I1UoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0I1U4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0I1VIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0I1VYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0JcYIteEeiNxp5TL-ow8A" name="B8JAS3" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0JcYYteEeiNxp5TL-ow8A" value="B8JAS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0JcYoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0JcY4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0JcZIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0JcZYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0KDcIteEeiNxp5TL-ow8A" name="B8JBS3" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0KDcYteEeiNxp5TL-ow8A" value="B8JBS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0KDcoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0KDc4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0KDdIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0KDdYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0KDdoteEeiNxp5TL-ow8A" name="B8NMT2" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0KDd4teEeiNxp5TL-ow8A" value="B8NMT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0KDeIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0KDeYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0KDeoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0KDe4teEeiNxp5TL-ow8A" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_h0KqgIteEeiNxp5TL-ow8A" name="B8NNT2" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_h0KqgYteEeiNxp5TL-ow8A" value="B8NNT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_h0KqgoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_h0Kqg4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_h0KqhIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_h0KqhYteEeiNxp5TL-ow8A" value="80"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_hRwhAIteEeiNxp5TL-ow8A" name="OTIMCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_hRwhAYteEeiNxp5TL-ow8A" value="OTIMCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_hRxIEIteEeiNxp5TL-ow8A" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_hSm1kIteEeiNxp5TL-ow8A" value="OTIMCPP   "/>
      <node defType="com.stambia.rdbms.column" id="_hiedkIteEeiNxp5TL-ow8A" name="IMB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_hiedkYteEeiNxp5TL-ow8A" value="IMB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hiedkoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hiedk4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hiedlIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hiedlYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hiedloteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hifrsIteEeiNxp5TL-ow8A" name="IMVBT2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_hifrsYteEeiNxp5TL-ow8A" value="IMVBT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hifrsoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hifrs4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hifrtIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hifrtYteEeiNxp5TL-ow8A" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_higSwIteEeiNxp5TL-ow8A" name="IMYYDT" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_higSwYteEeiNxp5TL-ow8A" value="IMYYDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_higSwoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_higSw4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_higSxIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_higSxYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_higSxoteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hig50IteEeiNxp5TL-ow8A" name="IMONS3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_hig50YteEeiNxp5TL-ow8A" value="IMONS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hig50oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hig504teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hig51IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hig51YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hihg4IteEeiNxp5TL-ow8A" name="IMX2P3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_hihg4YteEeiNxp5TL-ow8A" value="IMX2P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hihg4oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hihg44teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hihg5IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hihg5YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hihg5oteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hiiH8IteEeiNxp5TL-ow8A" name="IMX3P3" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_hiiH8YteEeiNxp5TL-ow8A" value="IMX3P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hiiH8oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hiiH84teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hiiH9IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hiiH9YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hiiH9oteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hiiH94teEeiNxp5TL-ow8A" name="IMX4P3" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_hiiH-IteEeiNxp5TL-ow8A" value="IMX4P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hiiH-YteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hiiH-oteEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hiiH-4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hiiH_IteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hiiH_YteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hiivAIteEeiNxp5TL-ow8A" name="IMX5P3" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_hiivAYteEeiNxp5TL-ow8A" value="IMX5P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hiivAoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hiivA4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hiivBIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hiivBYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hiivBoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hijWEIteEeiNxp5TL-ow8A" name="IMX6P3" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_hijWEYteEeiNxp5TL-ow8A" value="IMX6P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hijWEoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hijWE4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hijWFIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hijWFYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hijWFoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hij9IIteEeiNxp5TL-ow8A" name="IMX7P3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_hij9IYteEeiNxp5TL-ow8A" value="IMX7P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hij9IoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hij9I4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hij9JIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hij9JYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hij9JoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hikkMIteEeiNxp5TL-ow8A" name="IMX8P3" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_hikkMYteEeiNxp5TL-ow8A" value="IMX8P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hikkMoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hikkM4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hikkNIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hikkNYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hikkNoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hilLQIteEeiNxp5TL-ow8A" name="IMX9P3" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_hilLQYteEeiNxp5TL-ow8A" value="IMX9P3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hilLQoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hilLQ4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hilLRIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hilLRYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hilLRoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hilyUIteEeiNxp5TL-ow8A" name="IMYAP3" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_hilyUYteEeiNxp5TL-ow8A" value="IMYAP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hilyUoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hilyU4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hilyVIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hilyVYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hilyVoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_himZYIteEeiNxp5TL-ow8A" name="IMYBP3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_himZYYteEeiNxp5TL-ow8A" value="IMYBP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_himZYoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_himZY4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_himZZIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_himZZYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_himZZoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hinAcIteEeiNxp5TL-ow8A" name="IMYCP3" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_hinAcYteEeiNxp5TL-ow8A" value="IMYCP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hinAcoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hinAc4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hinAdIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hinAdYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hinAdoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hinngIteEeiNxp5TL-ow8A" name="IMYDP3" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_hinngYteEeiNxp5TL-ow8A" value="IMYDP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hinngoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hinng4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hinnhIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hinnhYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hinnhoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hio1oIteEeiNxp5TL-ow8A" name="IMYZDT" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_hio1oYteEeiNxp5TL-ow8A" value="IMYZDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hipcsIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hipcsYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hipcsoteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hipcs4teEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hipctIteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hiqDwIteEeiNxp5TL-ow8A" name="IMOOS3" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_hiqDwYteEeiNxp5TL-ow8A" value="IMOOS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hiqDwoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hiqDw4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hiqDxIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hiqDxYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hiqq0IteEeiNxp5TL-ow8A" name="IMYEP3" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_hituIIteEeiNxp5TL-ow8A" value="IMYEP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hituIYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hituIoteEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hituI4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hituJIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hituJYteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hiuVMIteEeiNxp5TL-ow8A" name="IMYFP3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_hiuVMYteEeiNxp5TL-ow8A" value="IMYFP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hiuVMoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hiuVM4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hiuVNIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hiuVNYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hiuVNoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hiwxcIteEeiNxp5TL-ow8A" name="IMYGP3" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_hiwxcYteEeiNxp5TL-ow8A" value="IMYGP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hiwxcoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hiwxc4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hiwxdIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hiwxdYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hiwxdoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hixYgIteEeiNxp5TL-ow8A" name="IMYHP3" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_hixYgYteEeiNxp5TL-ow8A" value="IMYHP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hixYgoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hixYg4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hixYhIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hixYhYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hixYhoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hix_kIteEeiNxp5TL-ow8A" name="IMYIP3" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_hix_kYteEeiNxp5TL-ow8A" value="IMYIP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hix_koteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hix_k4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hix_lIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hix_lYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hix_loteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hiymoIteEeiNxp5TL-ow8A" name="IMYJP3" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_hiymoYteEeiNxp5TL-ow8A" value="IMYJP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hiymooteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hiymo4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hiympIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hiympYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hiympoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hizNsIteEeiNxp5TL-ow8A" name="IMYKP3" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_hizNsYteEeiNxp5TL-ow8A" value="IMYKP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hizNsoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hizNs4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hizNtIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hizNtYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hizNtoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hizNt4teEeiNxp5TL-ow8A" name="IMYLP3" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_hizNuIteEeiNxp5TL-ow8A" value="IMYLP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hizNuYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hizNuoteEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hizNu4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hizNvIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hizNvYteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hiz0wIteEeiNxp5TL-ow8A" name="IMYMP3" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_hiz0wYteEeiNxp5TL-ow8A" value="IMYMP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hiz0woteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hiz0w4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hiz0xIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hiz0xYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hiz0xoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hi0b0IteEeiNxp5TL-ow8A" name="IMYNP3" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_hi0b0YteEeiNxp5TL-ow8A" value="IMYNP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hi0b0oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hi0b04teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hi0b1IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hi0b1YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hi0b1oteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hi1C4IteEeiNxp5TL-ow8A" name="IMYOP3" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_hi1C4YteEeiNxp5TL-ow8A" value="IMYOP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hi1C4oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hi1C44teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hi1C5IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hi1C5YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hi1C5oteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hi1p8IteEeiNxp5TL-ow8A" name="IMYPP3" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_hi1p8YteEeiNxp5TL-ow8A" value="IMYPP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hi1p8oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hi1p84teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hi1p9IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hi1p9YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hi1p9oteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hi1p94teEeiNxp5TL-ow8A" name="IMYQP3" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_hi1p-IteEeiNxp5TL-ow8A" value="IMYQP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hi1p-YteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hi1p-oteEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hi1p-4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hi1p_IteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hi1p_YteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hi2RAIteEeiNxp5TL-ow8A" name="IMYRP3" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_hi2RAYteEeiNxp5TL-ow8A" value="IMYRP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hi2RAoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hi2RA4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hi2RBIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hi2RBYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hi2RBoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hi24EIteEeiNxp5TL-ow8A" name="IMYSP3" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_hi24EYteEeiNxp5TL-ow8A" value="IMYSP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hi24EoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hi24E4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hi24FIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hi24FYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hi24FoteEeiNxp5TL-ow8A" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hi3fIIteEeiNxp5TL-ow8A" name="IMYTP3" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_hi3fIYteEeiNxp5TL-ow8A" value="IMYTP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hi3fIoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hi3fI4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hi3fJIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hi3fJYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hi3fJoteEeiNxp5TL-ow8A" value="11"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_hwS3IIteEeiNxp5TL-ow8A" name="ORI1REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_hwTeMIteEeiNxp5TL-ow8A" value="ORI1REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_hwTeMYteEeiNxp5TL-ow8A" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_hwedUIteEeiNxp5TL-ow8A" value="ORI1REP   "/>
      <node defType="com.stambia.rdbms.column" id="_hxyE4IteEeiNxp5TL-ow8A" name="I1B3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_hxyE4YteEeiNxp5TL-ow8A" value="I1B3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hxyE4oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hxyE44teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hxyE5IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hxyE5YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hxyE5oteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hxyr8IteEeiNxp5TL-ow8A" name="I1AZCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_hxyr8YteEeiNxp5TL-ow8A" value="I1AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hxyr8oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hxyr84teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hxyr9IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hxyr9YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hxyr9oteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hxyr94teEeiNxp5TL-ow8A" name="I1GHCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_hxyr-IteEeiNxp5TL-ow8A" value="I1GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hxyr-YteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hxyr-oteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hxyr-4teEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hxyr_IteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hxzTAIteEeiNxp5TL-ow8A" name="I1J2CD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_hxzTAYteEeiNxp5TL-ow8A" value="I1J2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hxzTAoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hxzTA4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hxzTBIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hxzTBYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hxz6EIteEeiNxp5TL-ow8A" name="I1J3CD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_hxz6EYteEeiNxp5TL-ow8A" value="I1J3CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hxz6EoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hxz6E4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hxz6FIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hxz6FYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx0hIIteEeiNxp5TL-ow8A" name="I1HMCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx0hIYteEeiNxp5TL-ow8A" value="I1HMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx0hIoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hx0hI4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx0hJIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx0hJYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx0hJoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx0hJ4teEeiNxp5TL-ow8A" name="I1JCCD" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx0hKIteEeiNxp5TL-ow8A" value="I1JCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx0hKYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx1IMIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx1IMYteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx1IMoteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx1IM4teEeiNxp5TL-ow8A" name="I1CPCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx1INIteEeiNxp5TL-ow8A" value="I1CPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx1INYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx1INoteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx1IN4teEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx1IOIteEeiNxp5TL-ow8A" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx1vQIteEeiNxp5TL-ow8A" name="I1ISTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx1vQYteEeiNxp5TL-ow8A" value="I1ISTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx1vQoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx1vQ4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx1vRIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx1vRYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx2WUIteEeiNxp5TL-ow8A" name="I1JVCD" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx2WUYteEeiNxp5TL-ow8A" value="I1JVCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx2WUoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx2WU4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx2WVIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx2WVYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx3kcIteEeiNxp5TL-ow8A" name="I1JWCD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx3kcYteEeiNxp5TL-ow8A" value="I1JWCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx3kcoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx3kc4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx3kdIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx3kdYteEeiNxp5TL-ow8A" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx4LgIteEeiNxp5TL-ow8A" name="I1MINB" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx4LgYteEeiNxp5TL-ow8A" value="I1MINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx4LgoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hx4Lg4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx4LhIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx4LhYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx4LhoteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx4Lh4teEeiNxp5TL-ow8A" name="I1MJNB" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx4LiIteEeiNxp5TL-ow8A" value="I1MJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx4LiYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hx4LioteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx4Li4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx4LjIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx4LjYteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx4ykIteEeiNxp5TL-ow8A" name="I1MKNB" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx4ykYteEeiNxp5TL-ow8A" value="I1MKNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx4ykoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hx4yk4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx4ylIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx4ylYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx4yloteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx5ZoIteEeiNxp5TL-ow8A" name="I1MLNB" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx5ZoYteEeiNxp5TL-ow8A" value="I1MLNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx5ZooteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hx5Zo4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx5ZpIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx5ZpYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx5ZpoteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx6AsIteEeiNxp5TL-ow8A" name="I1MMNB" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx6AsYteEeiNxp5TL-ow8A" value="I1MMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx6AsoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hx6As4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx6AtIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx6AtYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx6AtoteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx6nwIteEeiNxp5TL-ow8A" name="I1MNNB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx6nwYteEeiNxp5TL-ow8A" value="I1MNNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx6nwoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hx6nw4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx6nxIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx6nxYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx6nxoteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx8c8IteEeiNxp5TL-ow8A" name="I1NMNB" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx8c8YteEeiNxp5TL-ow8A" value="I1NMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx8c8oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hx8c84teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx8c9IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx8c9YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx8c9oteEeiNxp5TL-ow8A" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx8c94teEeiNxp5TL-ow8A" name="I1KYNB" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx8c-IteEeiNxp5TL-ow8A" value="I1KYNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx8c-YteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hx8c-oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx8c-4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx8c_IteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx8c_YteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx9EAIteEeiNxp5TL-ow8A" name="I1CNN3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx9EAYteEeiNxp5TL-ow8A" value="I1CNN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx9EAoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx9EA4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx9EBIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx9EBYteEeiNxp5TL-ow8A" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx9rEIteEeiNxp5TL-ow8A" name="I1CON3" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx9rEYteEeiNxp5TL-ow8A" value="I1CON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx9rEoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx9rE4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx9rFIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx9rFYteEeiNxp5TL-ow8A" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx9rFoteEeiNxp5TL-ow8A" name="I1I4TX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx9rF4teEeiNxp5TL-ow8A" value="I1I4TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx9rGIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx9rGYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx9rGoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx9rG4teEeiNxp5TL-ow8A" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx-SIIteEeiNxp5TL-ow8A" name="I1QQST" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx-SIYteEeiNxp5TL-ow8A" value="I1QQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx-SIoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx-SI4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx-SJIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx-SJYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx-5MIteEeiNxp5TL-ow8A" name="I1QPST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx-5MYteEeiNxp5TL-ow8A" value="I1QPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx-5MoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx-5M4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx-5NIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx-5NYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx-5NoteEeiNxp5TL-ow8A" name="I1CXS1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx-5N4teEeiNxp5TL-ow8A" value="I1CXS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx-5OIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx-5OYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx-5OoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx-5O4teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hx_gQIteEeiNxp5TL-ow8A" name="I1KZNB" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_hx_gQYteEeiNxp5TL-ow8A" value="I1KZNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hx_gQoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hx_gQ4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hx_gRIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hx_gRYteEeiNxp5TL-ow8A" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyAHUIteEeiNxp5TL-ow8A" name="I1NCNB" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyAHUYteEeiNxp5TL-ow8A" value="I1NCNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyAHUoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyAHU4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyAHVIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyAHVYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyAHVoteEeiNxp5TL-ow8A" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyAHV4teEeiNxp5TL-ow8A" name="I1K0NB" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyAHWIteEeiNxp5TL-ow8A" value="I1K0NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyAHWYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyAHWoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyAHW4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyAHXIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyAHXYteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyAuYIteEeiNxp5TL-ow8A" name="I1HPN1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyAuYYteEeiNxp5TL-ow8A" value="I1HPN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyAuYoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyAuY4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyAuZIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyAuZYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyAuZoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyAuZ4teEeiNxp5TL-ow8A" name="I1QRST" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyAuaIteEeiNxp5TL-ow8A" value="I1QRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyAuaYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyAuaoteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyAua4teEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyAubIteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyBVcIteEeiNxp5TL-ow8A" name="I1CYS1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyBVcYteEeiNxp5TL-ow8A" value="I1CYS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyBVcoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyBVc4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyBVdIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyBVdYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyB8gIteEeiNxp5TL-ow8A" name="I1QSST" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyB8gYteEeiNxp5TL-ow8A" value="I1QSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyB8goteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyB8g4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyB8hIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyB8hYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyB8hoteEeiNxp5TL-ow8A" name="I1QTST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyB8h4teEeiNxp5TL-ow8A" value="I1QTST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyB8iIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyB8iYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyB8ioteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyB8i4teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyCjkIteEeiNxp5TL-ow8A" name="I1QVST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyCjkYteEeiNxp5TL-ow8A" value="I1QVST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyCjkoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyCjk4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyCjlIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyCjlYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyDKoIteEeiNxp5TL-ow8A" name="I1QUST" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyDKoYteEeiNxp5TL-ow8A" value="I1QUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyDKooteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyDKo4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyDKpIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyDKpYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyDxsIteEeiNxp5TL-ow8A" name="I1FKS2" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyDxsYteEeiNxp5TL-ow8A" value="I1FKS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyDxsoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyDxs4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyDxtIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyDxtYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyEYwIteEeiNxp5TL-ow8A" name="I1FLS2" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyEYwYteEeiNxp5TL-ow8A" value="I1FLS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyEYwoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyEYw4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyEYxIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyEYxYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyEYxoteEeiNxp5TL-ow8A" name="I1FMS2" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyEYx4teEeiNxp5TL-ow8A" value="I1FMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyEYyIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyEYyYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyEYyoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyEYy4teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyE_0IteEeiNxp5TL-ow8A" name="I1QXST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyE_0YteEeiNxp5TL-ow8A" value="I1QXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyE_0oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyE_04teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyE_1IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyE_1YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyFm4IteEeiNxp5TL-ow8A" name="I1QYST" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyFm4YteEeiNxp5TL-ow8A" value="I1QYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyFm4oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyFm44teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyFm5IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyFm5YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyFm5oteEeiNxp5TL-ow8A" name="I1QZST" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyFm54teEeiNxp5TL-ow8A" value="I1QZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyFm6IteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyFm6YteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyFm6oteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyFm64teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyGN8IteEeiNxp5TL-ow8A" name="I1Q0ST" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyGN8YteEeiNxp5TL-ow8A" value="I1Q0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyGN8oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyGN84teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyGN9IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyGN9YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyG1AIteEeiNxp5TL-ow8A" name="I1K1NB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyG1AYteEeiNxp5TL-ow8A" value="I1K1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyG1AoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyG1A4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyG1BIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyG1BYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyG1BoteEeiNxp5TL-ow8A" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyG1B4teEeiNxp5TL-ow8A" name="I1K2NB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyG1CIteEeiNxp5TL-ow8A" value="I1K2NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyG1CYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyG1CoteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyG1C4teEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyG1DIteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyHcEIteEeiNxp5TL-ow8A" name="I1K3NB" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyHcEYteEeiNxp5TL-ow8A" value="I1K3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyHcEoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyHcE4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyHcFIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyHcFYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyIDIIteEeiNxp5TL-ow8A" name="I1S6ST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyIDIYteEeiNxp5TL-ow8A" value="I1S6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyIDIoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyIDI4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyIDJIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyIDJYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyIDJoteEeiNxp5TL-ow8A" name="I1XEST" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyIDJ4teEeiNxp5TL-ow8A" value="I1XEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyIDKIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyIDKYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyIDKoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyIDK4teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyIqMIteEeiNxp5TL-ow8A" name="I1DHQT" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyIqMYteEeiNxp5TL-ow8A" value="I1DHQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyIqMoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyIqM4teEeiNxp5TL-ow8A" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyIqNIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyIqNYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyIqNoteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyJRQIteEeiNxp5TL-ow8A" name="I1AFNB" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyJRQYteEeiNxp5TL-ow8A" value="I1AFNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyJRQoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyJRQ4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyJRRIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyJRRYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyJRRoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyJRR4teEeiNxp5TL-ow8A" name="I1L9QT" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyJRSIteEeiNxp5TL-ow8A" value="I1L9QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyJRSYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyJRSoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyJRS4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyJRTIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyJRTYteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyJ4UIteEeiNxp5TL-ow8A" name="I1L9N1" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyJ4UYteEeiNxp5TL-ow8A" value="I1L9N1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyJ4UoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyJ4U4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyJ4VIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyJ4VYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyJ4VoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyKfYIteEeiNxp5TL-ow8A" name="I1DAT1" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyKfYYteEeiNxp5TL-ow8A" value="I1DAT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyKfYoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyKfY4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyKfZIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyKfZYteEeiNxp5TL-ow8A" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyKfZoteEeiNxp5TL-ow8A" name="I1CZS1" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyKfZ4teEeiNxp5TL-ow8A" value="I1CZS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyKfaIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyKfaYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyKfaoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyKfa4teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyLGcIteEeiNxp5TL-ow8A" name="I1C9T1" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyLGcYteEeiNxp5TL-ow8A" value="I1C9T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyLGcoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyLGc4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyLGdIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyLGdYteEeiNxp5TL-ow8A" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyLGdoteEeiNxp5TL-ow8A" name="I1TPTX" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyLGd4teEeiNxp5TL-ow8A" value="I1TPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyLGeIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyLGeYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyLGeoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyLGe4teEeiNxp5TL-ow8A" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyLtgIteEeiNxp5TL-ow8A" name="I1TQTX" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyLtgYteEeiNxp5TL-ow8A" value="I1TQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyLtgoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyLtg4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyLthIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyLthYteEeiNxp5TL-ow8A" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyMUkIteEeiNxp5TL-ow8A" name="I1GVPR" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyMUkYteEeiNxp5TL-ow8A" value="I1GVPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyMUkoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyMUk4teEeiNxp5TL-ow8A" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyMUlIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyMUlYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyMUloteEeiNxp5TL-ow8A" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyMUl4teEeiNxp5TL-ow8A" name="I1CJPC" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyMUmIteEeiNxp5TL-ow8A" value="I1CJPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyMUmYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyMUmoteEeiNxp5TL-ow8A" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyMUm4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyMUnIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyMUnYteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyNisIteEeiNxp5TL-ow8A" name="I1C0S1" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyNisYteEeiNxp5TL-ow8A" value="I1C0S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyNisoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyNis4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyNitIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyNitYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyOJwIteEeiNxp5TL-ow8A" name="I1DYQT" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyOJwYteEeiNxp5TL-ow8A" value="I1DYQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyOJwoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyOJw4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyOJxIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyOJxYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyOJxoteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyOw0IteEeiNxp5TL-ow8A" name="I1DXQT" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyOw0YteEeiNxp5TL-ow8A" value="I1DXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyOw0oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyOw04teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyOw1IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyOw1YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyOw1oteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyPX4IteEeiNxp5TL-ow8A" name="I1GQQT" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyPX4YteEeiNxp5TL-ow8A" value="I1GQQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyPX4oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyPX44teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyPX5IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyPX5YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyPX5oteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyP-8IteEeiNxp5TL-ow8A" name="I1GRQT" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyP-8YteEeiNxp5TL-ow8A" value="I1GRQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyP-8oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyP-84teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyP-9IteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyP-9YteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyP-9oteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyQmAIteEeiNxp5TL-ow8A" name="I1GSQT" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyQmAYteEeiNxp5TL-ow8A" value="I1GSQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyQmAoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyQmA4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyQmBIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyQmBYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyQmBoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyRNEIteEeiNxp5TL-ow8A" name="I1GTQT" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyRNEYteEeiNxp5TL-ow8A" value="I1GTQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyRNEoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyRNE4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyRNFIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyRNFYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyRNFoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyRNF4teEeiNxp5TL-ow8A" name="I1GUQT" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyRNGIteEeiNxp5TL-ow8A" value="I1GUQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyRNGYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyRNGoteEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyRNG4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyRNHIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyRNHYteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyR0IIteEeiNxp5TL-ow8A" name="I1GVQT" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyR0IYteEeiNxp5TL-ow8A" value="I1GVQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyR0IoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyR0I4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyR0JIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyR0JYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyR0JoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hySbMIteEeiNxp5TL-ow8A" name="I1GWQT" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyTCQIteEeiNxp5TL-ow8A" value="I1GWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyTCQYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyTCQoteEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyTCQ4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyTCRIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyTCRYteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyTCRoteEeiNxp5TL-ow8A" name="I1GXQT" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyTCR4teEeiNxp5TL-ow8A" value="I1GXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyTCSIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyTCSYteEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyTCSoteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyTCS4teEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyTCTIteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyTpUIteEeiNxp5TL-ow8A" name="I1GYQT" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyTpUYteEeiNxp5TL-ow8A" value="I1GYQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyTpUoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyTpU4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyTpVIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyTpVYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyTpVoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyUQYIteEeiNxp5TL-ow8A" name="I1GZQT" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyUQYYteEeiNxp5TL-ow8A" value="I1GZQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyUQYoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyUQY4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyUQZIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyUQZYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyUQZoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyU3cIteEeiNxp5TL-ow8A" name="I1G0QT" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyU3cYteEeiNxp5TL-ow8A" value="I1G0QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyU3coteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyU3c4teEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyU3dIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyU3dYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyU3doteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyU3d4teEeiNxp5TL-ow8A" name="I1G1QT" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyU3eIteEeiNxp5TL-ow8A" value="I1G1QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyU3eYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyU3eoteEeiNxp5TL-ow8A" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyU3e4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyU3fIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyU3fYteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyVegIteEeiNxp5TL-ow8A" name="I1DWQT" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyVegYteEeiNxp5TL-ow8A" value="I1DWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyVegoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyVeg4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyVehIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyVehYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyVehoteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyWFkIteEeiNxp5TL-ow8A" name="I1HUDT" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyWFkYteEeiNxp5TL-ow8A" value="I1HUDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyWFkoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyWFk4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyWFlIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyWFlYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyWFloteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyWFl4teEeiNxp5TL-ow8A" name="I1YVNB" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyWFmIteEeiNxp5TL-ow8A" value="I1YVNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyWFmYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyWFmoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyWFm4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyWFnIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyWFnYteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyWsoIteEeiNxp5TL-ow8A" name="I1YWNB" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyWsoYteEeiNxp5TL-ow8A" value="I1YWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyWsooteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyWso4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyWspIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyWspYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyWspoteEeiNxp5TL-ow8A" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyWsp4teEeiNxp5TL-ow8A" name="I1GOQT" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyWsqIteEeiNxp5TL-ow8A" value="I1GOQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyWsqYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyWsqoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyWsq4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyWsrIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyWsrYteEeiNxp5TL-ow8A" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyXTsIteEeiNxp5TL-ow8A" name="I1FNS2" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyXTsYteEeiNxp5TL-ow8A" value="I1FNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyXTsoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyXTs4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyXTtIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyXTtYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyX6wIteEeiNxp5TL-ow8A" name="I1FOS2" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyX6wYteEeiNxp5TL-ow8A" value="I1FOS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyX6woteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyX6w4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyX6xIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyX6xYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyX6xoteEeiNxp5TL-ow8A" name="I1FPS2" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyX6x4teEeiNxp5TL-ow8A" value="I1FPS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyX6yIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyX6yYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyX6yoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyX6y4teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyYh0IteEeiNxp5TL-ow8A" name="I1FQS2" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyYh0YteEeiNxp5TL-ow8A" value="I1FQS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyYh0oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyYh04teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyYh1IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyYh1YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyZI4IteEeiNxp5TL-ow8A" name="I1FRS2" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyZI4YteEeiNxp5TL-ow8A" value="I1FRS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyZI4oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyZI44teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyZI5IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyZI5YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyZv8IteEeiNxp5TL-ow8A" name="I1FSS2" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyZv8YteEeiNxp5TL-ow8A" value="I1FSS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyZv8oteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyZv84teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyZv9IteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyZv9YteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyZv9oteEeiNxp5TL-ow8A" name="I1FTS2" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyZv94teEeiNxp5TL-ow8A" value="I1FTS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyZv-IteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyZv-YteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyZv-oteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyZv-4teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyaXAIteEeiNxp5TL-ow8A" name="I1FUS2" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyaXAYteEeiNxp5TL-ow8A" value="I1FUS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyaXAoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyaXA4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyaXBIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyaXBYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyaXBoteEeiNxp5TL-ow8A" name="I1FVS2" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyaXB4teEeiNxp5TL-ow8A" value="I1FVS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyaXCIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyaXCYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyaXCoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyaXC4teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hya-EIteEeiNxp5TL-ow8A" name="I1FWS2" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_hya-EYteEeiNxp5TL-ow8A" value="I1FWS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hya-EoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hya-E4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hya-FIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hya-FYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyblIIteEeiNxp5TL-ow8A" name="I1FXS2" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyblIYteEeiNxp5TL-ow8A" value="I1FXS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyblIoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyblI4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyblJIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyblJYteEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyblJoteEeiNxp5TL-ow8A" name="I1FYS2" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyblJ4teEeiNxp5TL-ow8A" value="I1FYS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyblKIteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyblKYteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyblKoteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyblK4teEeiNxp5TL-ow8A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hycMMIteEeiNxp5TL-ow8A" name="I1YSNB" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_hycMMYteEeiNxp5TL-ow8A" value="I1YSNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hycMMoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hycMM4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hycMNIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hycMNYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hycMNoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyczQIteEeiNxp5TL-ow8A" name="I1YTNB" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyczQYteEeiNxp5TL-ow8A" value="I1YTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyczQoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyczQ4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyczRIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyczRYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyczRoteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyczR4teEeiNxp5TL-ow8A" name="I1YUNB" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyczSIteEeiNxp5TL-ow8A" value="I1YUNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyczSYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyczSoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyczS4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyczTIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyczTYteEeiNxp5TL-ow8A" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hydaUIteEeiNxp5TL-ow8A" name="I1GLQT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_hydaUYteEeiNxp5TL-ow8A" value="I1GLQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hydaUoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hydaU4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hydaVIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hydaVYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hydaVoteEeiNxp5TL-ow8A" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyeBYIteEeiNxp5TL-ow8A" name="I1GMQT" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyeBYYteEeiNxp5TL-ow8A" value="I1GMQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyeBYoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyeBY4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyeBZIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyeBZYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyeBZoteEeiNxp5TL-ow8A" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyeBZ4teEeiNxp5TL-ow8A" name="I1GNQT" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyeBaIteEeiNxp5TL-ow8A" value="I1GNQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyeBaYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyeBaoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyeBa4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyeBbIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyeBbYteEeiNxp5TL-ow8A" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyeocIteEeiNxp5TL-ow8A" name="I1JLDT" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyeocYteEeiNxp5TL-ow8A" value="I1JLDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyeocoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyeoc4teEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyeodIteEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyeodYteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyeodoteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyeod4teEeiNxp5TL-ow8A" name="I1AADT" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyeoeIteEeiNxp5TL-ow8A" value="I1AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyeoeYteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_hyeoeoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyeoe4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyeofIteEeiNxp5TL-ow8A" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyeofYteEeiNxp5TL-ow8A" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_hyfPgIteEeiNxp5TL-ow8A" name="I1AATX" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_hyfPgYteEeiNxp5TL-ow8A" value="I1AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_hyfPgoteEeiNxp5TL-ow8A" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_hyfPg4teEeiNxp5TL-ow8A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_hyfPhIteEeiNxp5TL-ow8A" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_hyfPhYteEeiNxp5TL-ow8A" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_WsMp0P4YEeiVrKKMla1zGg" name="ORAZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_WsMp0f4YEeiVrKKMla1zGg" value="ORAZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_WsMp0v4YEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_WucFgP4YEeiVrKKMla1zGg" value="ORAZREP   "/>
      <node defType="com.stambia.rdbms.column" id="_XWPsYP4YEeiVrKKMla1zGg" name="AZAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWPsYf4YEeiVrKKMla1zGg" value="AZAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWPsYv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWPsY_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWPsZP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWPsZf4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWPsZv4YEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2UP4YEeiVrKKMla1zGg" name="AZAMCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2Uf4YEeiVrKKMla1zGg" value="AZAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2Uv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2U_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2VP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2Vf4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2Vv4YEeiVrKKMla1zGg" name="AZC0CD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2V_4YEeiVrKKMla1zGg" value="AZC0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2WP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWY2Wf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2Wv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2W_4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2XP4YEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2Xf4YEeiVrKKMla1zGg" name="AZF6PR" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2Xv4YEeiVrKKMla1zGg" value="AZF6PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2X_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWY2YP4YEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2Yf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2Yv4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2Y_4YEeiVrKKMla1zGg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2ZP4YEeiVrKKMla1zGg" name="AZTEST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2Zf4YEeiVrKKMla1zGg" value="AZTEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2Zv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2Z_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2aP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2af4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2av4YEeiVrKKMla1zGg" name="AZTKST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2a_4YEeiVrKKMla1zGg" value="AZTKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2bP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2bf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2bv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2b_4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2cP4YEeiVrKKMla1zGg" name="AZTLST" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2cf4YEeiVrKKMla1zGg" value="AZTLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2cv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2c_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2dP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2df4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2dv4YEeiVrKKMla1zGg" name="AZNDCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2d_4YEeiVrKKMla1zGg" value="AZNDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2eP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2ef4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2ev4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2e_4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2fP4YEeiVrKKMla1zGg" name="AZNECD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2ff4YEeiVrKKMla1zGg" value="AZNECD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2fv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2f_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2gP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2gf4YEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2gv4YEeiVrKKMla1zGg" name="AZAQST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2g_4YEeiVrKKMla1zGg" value="AZAQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2hP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2hf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2hv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2h_4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2iP4YEeiVrKKMla1zGg" name="AZARST" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2if4YEeiVrKKMla1zGg" value="AZARST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2iv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2i_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2jP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2jf4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2jv4YEeiVrKKMla1zGg" name="AZASST" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2j_4YEeiVrKKMla1zGg" value="AZASST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2kP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2kf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2kv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2k_4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2lP4YEeiVrKKMla1zGg" name="AZBHS2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2lf4YEeiVrKKMla1zGg" value="AZBHS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2lv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2l_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2mP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2mf4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2mv4YEeiVrKKMla1zGg" name="AZAWNA" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2m_4YEeiVrKKMla1zGg" value="AZAWNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2nP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2nf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2nv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2n_4YEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWY2oP4YEeiVrKKMla1zGg" name="AZAUTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWY2of4YEeiVrKKMla1zGg" value="AZAUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWY2ov4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWY2o_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWY2pP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWY2pf4YEeiVrKKMla1zGg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinUP4YEeiVrKKMla1zGg" name="AZA0CD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinUf4YEeiVrKKMla1zGg" value="AZA0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinUv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWinU_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinVP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinVf4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinVv4YEeiVrKKMla1zGg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinV_4YEeiVrKKMla1zGg" name="AZATTX" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinWP4YEeiVrKKMla1zGg" value="AZATTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinWf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinWv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinW_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinXP4YEeiVrKKMla1zGg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinXf4YEeiVrKKMla1zGg" name="AZOEST" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinXv4YEeiVrKKMla1zGg" value="AZOEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinX_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinYP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinYf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinYv4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinY_4YEeiVrKKMla1zGg" name="AZA8ST" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinZP4YEeiVrKKMla1zGg" value="AZA8ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinZf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinZv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinZ_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinaP4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinaf4YEeiVrKKMla1zGg" name="AZAOST" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinav4YEeiVrKKMla1zGg" value="AZAOST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWina_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinbP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinbf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinbv4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinb_4YEeiVrKKMla1zGg" name="AZFNTX" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWincP4YEeiVrKKMla1zGg" value="AZFNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWincf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWincv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinc_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWindP4YEeiVrKKMla1zGg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWindf4YEeiVrKKMla1zGg" name="AZZLTX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWindv4YEeiVrKKMla1zGg" value="AZZLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWind_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWineP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinef4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinev4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWine_4YEeiVrKKMla1zGg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinfP4YEeiVrKKMla1zGg" name="AZFMTX" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinff4YEeiVrKKMla1zGg" value="AZFMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinfv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWinf_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWingP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWingf4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWingv4YEeiVrKKMla1zGg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWing_4YEeiVrKKMla1zGg" name="AZIWST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinhP4YEeiVrKKMla1zGg" value="AZIWST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinhf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinhv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinh_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWiniP4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinif4YEeiVrKKMla1zGg" name="AZIXST" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWiniv4YEeiVrKKMla1zGg" value="AZIXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWini_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinjP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinjf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinjv4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinj_4YEeiVrKKMla1zGg" name="AZIYST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinkP4YEeiVrKKMla1zGg" value="AZIYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinkf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinkv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWink_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinlP4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinlf4YEeiVrKKMla1zGg" name="AZIZST" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinlv4YEeiVrKKMla1zGg" value="AZIZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinl_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinmP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinmf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinmv4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinm_4YEeiVrKKMla1zGg" name="AZI0ST" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinnP4YEeiVrKKMla1zGg" value="AZI0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinnf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinnv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinn_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinoP4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinof4YEeiVrKKMla1zGg" name="AZI2ST" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinov4YEeiVrKKMla1zGg" value="AZI2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWino_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinpP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinpf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinpv4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinp_4YEeiVrKKMla1zGg" name="AZHKCD" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinqP4YEeiVrKKMla1zGg" value="AZHKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinqf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinqv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinq_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinrP4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinrf4YEeiVrKKMla1zGg" name="AZCKTX" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinrv4YEeiVrKKMla1zGg" value="AZCKTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinr_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWinsP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinsf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinsv4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWins_4YEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWintP4YEeiVrKKMla1zGg" name="AZAACD" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWintf4YEeiVrKKMla1zGg" value="AZAACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWintv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWint_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinuP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinuf4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinuv4YEeiVrKKMla1zGg" name="AZSEST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinu_4YEeiVrKKMla1zGg" value="AZSEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinvP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinvf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinvv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinv_4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinwP4YEeiVrKKMla1zGg" name="AZSFST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinwf4YEeiVrKKMla1zGg" value="AZSFST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinwv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinw_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinxP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWinxf4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinxv4YEeiVrKKMla1zGg" name="AZAFCD" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinx_4YEeiVrKKMla1zGg" value="AZAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinyP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinyf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWinyv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWiny_4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWinzP4YEeiVrKKMla1zGg" name="AZABCD" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWinzf4YEeiVrKKMla1zGg" value="AZABCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWinzv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWinz_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWin0P4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWin0f4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWin0v4YEeiVrKKMla1zGg" name="AZACCD" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWin0_4YEeiVrKKMla1zGg" value="AZACCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWin1P4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWin1f4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWin1v4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWin1_4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWin2P4YEeiVrKKMla1zGg" name="AZAHCD" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWin2f4YEeiVrKKMla1zGg" value="AZAHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWin2v4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWin2_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWin3P4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWin3f4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWin3v4YEeiVrKKMla1zGg" name="AZE3ST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWin3_4YEeiVrKKMla1zGg" value="AZE3ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWin4P4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWin4f4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWin4v4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWin4_4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWin5P4YEeiVrKKMla1zGg" name="AZCGCD" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWin5f4YEeiVrKKMla1zGg" value="AZCGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWin5v4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWin5_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWin6P4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWin6f4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWin6v4YEeiVrKKMla1zGg" name="AZJ4CD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWin6_4YEeiVrKKMla1zGg" value="AZJ4CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWin7P4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWin7f4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWin7v4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWin7_4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYUP4YEeiVrKKMla1zGg" name="AZBCCD" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYUf4YEeiVrKKMla1zGg" value="AZBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYUv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWsYU_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYVP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYVf4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYVv4YEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYV_4YEeiVrKKMla1zGg" name="AZF3CD" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYWP4YEeiVrKKMla1zGg" value="AZF3CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYWf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYWv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYW_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYXP4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYXf4YEeiVrKKMla1zGg" name="AZADNB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYXv4YEeiVrKKMla1zGg" value="AZADNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYX_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWsYYP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYYf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYYv4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYY_4YEeiVrKKMla1zGg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYZP4YEeiVrKKMla1zGg" name="AZAENB" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYZf4YEeiVrKKMla1zGg" value="AZAENB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYZv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWsYZ_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYaP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYaf4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYav4YEeiVrKKMla1zGg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYa_4YEeiVrKKMla1zGg" name="AZFTNB" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYbP4YEeiVrKKMla1zGg" value="AZFTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYbf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWsYbv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYb_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYcP4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYcf4YEeiVrKKMla1zGg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYcv4YEeiVrKKMla1zGg" name="AZBOCD" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYc_4YEeiVrKKMla1zGg" value="AZBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYdP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYdf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYdv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYd_4YEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYeP4YEeiVrKKMla1zGg" name="AZHRST" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYef4YEeiVrKKMla1zGg" value="AZHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYev4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYe_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYfP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYff4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYfv4YEeiVrKKMla1zGg" name="AZA4ST" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYf_4YEeiVrKKMla1zGg" value="AZA4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYgP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYgf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYgv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYg_4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYhP4YEeiVrKKMla1zGg" name="AZFWNB" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYhf4YEeiVrKKMla1zGg" value="AZFWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYhv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWsYh_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYiP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYif4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYiv4YEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYi_4YEeiVrKKMla1zGg" name="AZA0ST" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYjP4YEeiVrKKMla1zGg" value="AZA0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYjf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYjv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYj_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYkP4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYkf4YEeiVrKKMla1zGg" name="AZA3ST" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYkv4YEeiVrKKMla1zGg" value="AZA3ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYk_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYlP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYlf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYlv4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYl_4YEeiVrKKMla1zGg" name="AZBDCD" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYmP4YEeiVrKKMla1zGg" value="AZBDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYmf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYmv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYm_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYnP4YEeiVrKKMla1zGg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYnf4YEeiVrKKMla1zGg" name="AZA1ST" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYnv4YEeiVrKKMla1zGg" value="AZA1ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYn_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYoP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYof4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYov4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYo_4YEeiVrKKMla1zGg" name="AZAJNB" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYpP4YEeiVrKKMla1zGg" value="AZAJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYpf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYpv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYp_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYqP4YEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYqf4YEeiVrKKMla1zGg" name="AZA6NA" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYqv4YEeiVrKKMla1zGg" value="AZA6NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYq_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYrP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYrf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYrv4YEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYr_4YEeiVrKKMla1zGg" name="AZBACD" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYsP4YEeiVrKKMla1zGg" value="AZBACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYsf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWsYsv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYs_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYtP4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYtf4YEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYtv4YEeiVrKKMla1zGg" name="AZBBCD" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYt_4YEeiVrKKMla1zGg" value="AZBBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYuP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XWsYuf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYuv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYu_4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYvP4YEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYvf4YEeiVrKKMla1zGg" name="AZA2NA" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYvv4YEeiVrKKMla1zGg" value="AZA2NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYv_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYwP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYwf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYwv4YEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYw_4YEeiVrKKMla1zGg" name="AZA3NA" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYxP4YEeiVrKKMla1zGg" value="AZA3NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYxf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYxv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYx_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYyP4YEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYyf4YEeiVrKKMla1zGg" name="AZCFCD" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsYyv4YEeiVrKKMla1zGg" value="AZCFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsYy_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsYzP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsYzf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsYzv4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsYz_4YEeiVrKKMla1zGg" name="AZI4ST" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsY0P4YEeiVrKKMla1zGg" value="AZI4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsY0f4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsY0v4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsY0_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsY1P4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsY1f4YEeiVrKKMla1zGg" name="AZB7S1" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsY1v4YEeiVrKKMla1zGg" value="AZB7S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsY1_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsY2P4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsY2f4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsY2v4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsY2_4YEeiVrKKMla1zGg" name="AZNHST" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsY3P4YEeiVrKKMla1zGg" value="AZNHST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsY3f4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsY3v4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsY3_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsY4P4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsY4f4YEeiVrKKMla1zGg" name="AZD3C1" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsY4v4YEeiVrKKMla1zGg" value="AZD3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsY4_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsY5P4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsY5f4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsY5v4YEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XWsY5_4YEeiVrKKMla1zGg" name="AZAUST" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_XWsY6P4YEeiVrKKMla1zGg" value="AZAUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XWsY6f4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XWsY6v4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XWsY6_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XWsY7P4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iQP4YEeiVrKKMla1zGg" name="AZI5ST" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iQf4YEeiVrKKMla1zGg" value="AZI5ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iQv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1iQ_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iRP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iRf4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iRv4YEeiVrKKMla1zGg" name="AZADVA" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iR_4YEeiVrKKMla1zGg" value="AZADVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iSP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XW1iSf4YEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1iSv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iS_4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iTP4YEeiVrKKMla1zGg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iTf4YEeiVrKKMla1zGg" name="AZCBPR" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iTv4YEeiVrKKMla1zGg" value="AZCBPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iT_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XW1iUP4YEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1iUf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iUv4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iU_4YEeiVrKKMla1zGg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iVP4YEeiVrKKMla1zGg" name="AZBCNA" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iVf4YEeiVrKKMla1zGg" value="AZBCNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iVv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1iV_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iWP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iWf4YEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iWv4YEeiVrKKMla1zGg" name="AZALNB" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iW_4YEeiVrKKMla1zGg" value="AZALNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iXP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1iXf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iXv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iX_4YEeiVrKKMla1zGg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iYP4YEeiVrKKMla1zGg" name="AZAMNB" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iYf4YEeiVrKKMla1zGg" value="AZAMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iYv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1iY_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iZP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iZf4YEeiVrKKMla1zGg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iZv4YEeiVrKKMla1zGg" name="AZANNB" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iZ_4YEeiVrKKMla1zGg" value="AZANNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iaP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1iaf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iav4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1ia_4YEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1ibP4YEeiVrKKMla1zGg" name="AZAONB" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1ibf4YEeiVrKKMla1zGg" value="AZAONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1ibv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1ib_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1icP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1icf4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1icv4YEeiVrKKMla1zGg" name="AZBGNA" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1ic_4YEeiVrKKMla1zGg" value="AZBGNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1idP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1idf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1idv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1id_4YEeiVrKKMla1zGg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1ieP4YEeiVrKKMla1zGg" name="AZBHNA" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1ief4YEeiVrKKMla1zGg" value="AZBHNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iev4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1ie_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1ifP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iff4YEeiVrKKMla1zGg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1ifv4YEeiVrKKMla1zGg" name="AZE2ST" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1if_4YEeiVrKKMla1zGg" value="AZE2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1igP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1igf4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1igv4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1ig_4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1ihP4YEeiVrKKMla1zGg" name="AZCHCD" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1ihf4YEeiVrKKMla1zGg" value="AZCHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1ihv4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1ih_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iiP4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iif4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iiv4YEeiVrKKMla1zGg" name="AZAJCD" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1ii_4YEeiVrKKMla1zGg" value="AZAJCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1ijP4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XW1ijf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1ijv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1ij_4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1ikP4YEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1ikf4YEeiVrKKMla1zGg" name="AZBINA" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1ikv4YEeiVrKKMla1zGg" value="AZBINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1ik_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1ilP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1ilf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1ilv4YEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1il_4YEeiVrKKMla1zGg" name="AZCQTX" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1imP4YEeiVrKKMla1zGg" value="AZCQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1imf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1imv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1im_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1inP4YEeiVrKKMla1zGg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1inf4YEeiVrKKMla1zGg" name="AZI6ST" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1inv4YEeiVrKKMla1zGg" value="AZI6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1in_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1ioP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iof4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iov4YEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1io_4YEeiVrKKMla1zGg" name="AZBJN3" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1ipP4YEeiVrKKMla1zGg" value="AZBJN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1ipf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1ipv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1ip_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iqP4YEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iqf4YEeiVrKKMla1zGg" name="AZBON3" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iqv4YEeiVrKKMla1zGg" value="AZBON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iq_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1irP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1irf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1irv4YEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1ir_4YEeiVrKKMla1zGg" name="AZBPN3" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1isP4YEeiVrKKMla1zGg" value="AZBPN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1isf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1isv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1is_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1itP4YEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1itf4YEeiVrKKMla1zGg" name="AZHNTX" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1itv4YEeiVrKKMla1zGg" value="AZHNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1it_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1iuP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iuf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iuv4YEeiVrKKMla1zGg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iu_4YEeiVrKKMla1zGg" name="AZHOTX" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1ivP4YEeiVrKKMla1zGg" value="AZHOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1ivf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1ivv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iv_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1iwP4YEeiVrKKMla1zGg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1iwf4YEeiVrKKMla1zGg" name="AZHPTX" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iwv4YEeiVrKKMla1zGg" value="AZHPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iw_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1ixP4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1ixf4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1ixv4YEeiVrKKMla1zGg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1ix_4YEeiVrKKMla1zGg" name="AZHQTX" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1iyP4YEeiVrKKMla1zGg" value="AZHQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iyf4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1iyv4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1iy_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1izP4YEeiVrKKMla1zGg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1izf4YEeiVrKKMla1zGg" name="AZBQN3" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1izv4YEeiVrKKMla1zGg" value="AZBQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1iz_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1i0P4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1i0f4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1i0v4YEeiVrKKMla1zGg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1i0_4YEeiVrKKMla1zGg" name="AZAKCD" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1i1P4YEeiVrKKMla1zGg" value="AZAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1i1f4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1i1v4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1i1_4YEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1i2P4YEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1i2f4YEeiVrKKMla1zGg" name="AZFVPR" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1i2v4YEeiVrKKMla1zGg" value="AZFVPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1i2_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XW1i3P4YEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1i3f4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1i3v4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1i3_4YEeiVrKKMla1zGg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1i4P4YEeiVrKKMla1zGg" name="AZGHDT" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1i4f4YEeiVrKKMla1zGg" value="AZGHDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1i4v4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XW1i4_4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1i5P4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1i5f4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1i5v4YEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XW1i5_4YEeiVrKKMla1zGg" name="AZGIDT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_XW1i6P4YEeiVrKKMla1zGg" value="AZGIDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XW1i6f4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XW1i6v4YEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XW1i6_4YEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XW1i7P4YEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XW1i7f4YEeiVrKKMla1zGg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_0Sf0wP4iEeiVrKKMla1zGg" name="ORQ3REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_0Sf0wf4iEeiVrKKMla1zGg" value="ORQ3REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_0Sf0wv4iEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_0SplwP4iEeiVrKKMla1zGg" value="ORQ3REP   "/>
      <node defType="com.stambia.rdbms.column" id="_0TQCsP4iEeiVrKKMla1zGg" name="Q3AZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TQCsf4iEeiVrKKMla1zGg" value="Q3AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TQCsv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TQCs_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TQCtP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TQCtf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TQCtv4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TQCt_4iEeiVrKKMla1zGg" name="Q3VTN2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TQCuP4iEeiVrKKMla1zGg" value="Q3VTN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TQCuf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TQCuv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TQCu_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TQCvP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TQCvf4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZzsP4iEeiVrKKMla1zGg" name="Q3VUN2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZzsf4iEeiVrKKMla1zGg" value="Q3VUN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZzsv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZzs_4iEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZztP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZztf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZztv4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZzt_4iEeiVrKKMla1zGg" name="Q3VVN2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZzuP4iEeiVrKKMla1zGg" value="Q3VVN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZzuf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZzuv4iEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZzu_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZzvP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZzvf4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZzvv4iEeiVrKKMla1zGg" name="Q3VWN2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZzv_4iEeiVrKKMla1zGg" value="Q3VWN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZzwP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZzwf4iEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZzwv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZzw_4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZzxP4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZzxf4iEeiVrKKMla1zGg" name="Q3LMS2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZzxv4iEeiVrKKMla1zGg" value="Q3LMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZzx_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZzyP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZzyf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZzyv4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZzy_4iEeiVrKKMla1zGg" name="Q3KODT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZzzP4iEeiVrKKMla1zGg" value="Q3KODT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZzzf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZzzv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZzz_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZz0P4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZz0f4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZz0v4iEeiVrKKMla1zGg" name="Q3VXN2" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZz0_4iEeiVrKKMla1zGg" value="Q3VXN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZz1P4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZz1f4iEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZz1v4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZz1_4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZz2P4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZz2f4iEeiVrKKMla1zGg" name="Q3VYN2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZz2v4iEeiVrKKMla1zGg" value="Q3VYN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZz2_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZz3P4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZz3f4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZz3v4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZz3_4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZz4P4iEeiVrKKMla1zGg" name="Q3VZN2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZz4f4iEeiVrKKMla1zGg" value="Q3VZN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZz4v4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZz4_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZz5P4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZz5f4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZz5v4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZz5_4iEeiVrKKMla1zGg" name="Q3V0N2" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZz6P4iEeiVrKKMla1zGg" value="Q3V0N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZz6f4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZz6v4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZz6_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZz7P4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZz7f4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZz7v4iEeiVrKKMla1zGg" name="Q3V1N2" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZz7_4iEeiVrKKMla1zGg" value="Q3V1N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZz8P4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZz8f4iEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZz8v4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZz8_4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZz9P4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZz9f4iEeiVrKKMla1zGg" name="Q3V2N2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZz9v4iEeiVrKKMla1zGg" value="Q3V2N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZz9_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZz-P4iEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZz-f4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZz-v4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZz-_4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZz_P4iEeiVrKKMla1zGg" name="Q3V3N2" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZz_f4iEeiVrKKMla1zGg" value="Q3V3N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZz_v4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZz__4iEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0AP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0Af4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0Av4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0A_4iEeiVrKKMla1zGg" name="Q3V4N2" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0BP4iEeiVrKKMla1zGg" value="Q3V4N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0Bf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZ0Bv4iEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0B_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0CP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0Cf4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0Cv4iEeiVrKKMla1zGg" name="Q3V5N2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0C_4iEeiVrKKMla1zGg" value="Q3V5N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0DP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZ0Df4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0Dv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0D_4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0EP4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0Ef4iEeiVrKKMla1zGg" name="Q3V6N2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0Ev4iEeiVrKKMla1zGg" value="Q3V6N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0E_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZ0FP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0Ff4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0Fv4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0F_4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0GP4iEeiVrKKMla1zGg" name="Q3V7N2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0Gf4iEeiVrKKMla1zGg" value="Q3V7N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0Gv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZ0G_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0HP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0Hf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0Hv4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0H_4iEeiVrKKMla1zGg" name="Q3V8N2" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0IP4iEeiVrKKMla1zGg" value="Q3V8N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0If4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZ0Iv4iEeiVrKKMla1zGg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0I_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0JP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0Jf4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0Jv4iEeiVrKKMla1zGg" name="Q3KPDT" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0J_4iEeiVrKKMla1zGg" value="Q3KPDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0KP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZ0Kf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0Kv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0K_4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0LP4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0Lf4iEeiVrKKMla1zGg" name="Q3LNS2" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0Lv4iEeiVrKKMla1zGg" value="Q3LNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0L_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0MP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0Mf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0Mv4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0M_4iEeiVrKKMla1zGg" name="Q3V9N2" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0NP4iEeiVrKKMla1zGg" value="Q3V9N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0Nf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZ0Nv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0N_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0OP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0Of4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0Ov4iEeiVrKKMla1zGg" name="Q3WAN2" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0O_4iEeiVrKKMla1zGg" value="Q3WAN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0PP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZ0Pf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0Pv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0P_4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0QP4iEeiVrKKMla1zGg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0Qf4iEeiVrKKMla1zGg" name="Q3KQDT" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0Qv4iEeiVrKKMla1zGg" value="Q3KQDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0Q_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0TZ0RP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0Rf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0Rv4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0R_4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0TZ0SP4iEeiVrKKMla1zGg" name="Q3QHNA" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_0TZ0Sf4iEeiVrKKMla1zGg" value="Q3QHNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0TZ0Sv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0TZ0S_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0TZ0TP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0TZ0Tf4iEeiVrKKMla1zGg" value="60"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_zKd1sf4iEeiVrKKMla1zGg" name="OSNOCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_zKd1sv4iEeiVrKKMla1zGg" value="OSNOCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_zKd1s_4iEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_zLNckP4iEeiVrKKMla1zGg" value="OSNOCPP   "/>
      <node defType="com.stambia.rdbms.column" id="_zVDVEP4iEeiVrKKMla1zGg" name="NOAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_zVDVEf4iEeiVrKKMla1zGg" value="NOAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zVDVEv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_zVDVE_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zVDVFP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zVDVFf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zVDVFv4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zVDVF_4iEeiVrKKMla1zGg" name="NOR0DT" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_zVDVGP4iEeiVrKKMla1zGg" value="NOR0DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zVDVGf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_zVDVGv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zVDVG_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zVDVHP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zVDVHf4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zVDVHv4iEeiVrKKMla1zGg" name="NOYNS2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_zVDVH_4iEeiVrKKMla1zGg" value="NOYNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zVDVIP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zVDVIf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zVDVIv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zVDVI_4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zVDVJP4iEeiVrKKMla1zGg" name="NOYPS2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_zVDVJf4iEeiVrKKMla1zGg" value="NOYPS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zVDVJv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zVDVJ_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zVDVKP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zVDVKf4iEeiVrKKMla1zGg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_0RPQgf4iEeiVrKKMla1zGg" name="OSLZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_0RPQgv4iEeiVrKKMla1zGg" value="OSLZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_0RPQg_4iEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_0RPQhP4iEeiVrKKMla1zGg" value="OSLZREP   "/>
      <node defType="com.stambia.rdbms.column" id="_0R1tcP4iEeiVrKKMla1zGg" name="LZJKC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_0R1tcf4iEeiVrKKMla1zGg" value="LZJKC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0R1tcv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0R1tc_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0R1tdP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0R1tdf4iEeiVrKKMla1zGg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0R1tdv4iEeiVrKKMla1zGg" name="LZJLC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_0R1td_4iEeiVrKKMla1zGg" value="LZJLC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0R1teP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0R1tef4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0R1tev4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0R1te_4iEeiVrKKMla1zGg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0R1tfP4iEeiVrKKMla1zGg" name="LZXGN5" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_0R1tff4iEeiVrKKMla1zGg" value="LZXGN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0R1tfv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0R1tf_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0R1tgP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0R1tgf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0R1tgv4iEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0R1tg_4iEeiVrKKMla1zGg" name="LZWUT1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_0R1thP4iEeiVrKKMla1zGg" value="LZWUT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0R1thf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0R1thv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0R1th_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0R1tiP4iEeiVrKKMla1zGg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0R1tif4iEeiVrKKMla1zGg" name="LZWVT1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_0R1tiv4iEeiVrKKMla1zGg" value="LZWVT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0R1ti_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0R1tjP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0R1tjf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0R1tjv4iEeiVrKKMla1zGg" value="70"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_zXlEoP4iEeiVrKKMla1zGg" name="ORGVREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_zXlEof4iEeiVrKKMla1zGg" value="ORGVREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_zXlEov4iEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_zX4moP4iEeiVrKKMla1zGg" value="ORGVREP   "/>
      <node defType="com.stambia.rdbms.column" id="_zZX0YP4iEeiVrKKMla1zGg" name="GVAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_zZX0Yf4iEeiVrKKMla1zGg" value="GVAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zZX0Yv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_zZX0Y_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zZX0ZP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zZX0Zf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zZX0Zv4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zZX0Z_4iEeiVrKKMla1zGg" name="GVFSCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_zZX0aP4iEeiVrKKMla1zGg" value="GVFSCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zZX0af4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_zZX0av4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zZX0a_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zZX0bP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zZX0bf4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zZX0bv4iEeiVrKKMla1zGg" name="GVFRCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_zZX0b_4iEeiVrKKMla1zGg" value="GVFRCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zZX0cP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zZX0cf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zZX0cv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zZX0c_4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zZX0dP4iEeiVrKKMla1zGg" name="GVFZNB" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_zZX0df4iEeiVrKKMla1zGg" value="GVFZNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zZX0dv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_zZX0d_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zZX0eP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zZX0ef4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zZX0ev4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zZX0e_4iEeiVrKKMla1zGg" name="GVJAST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_zZX0fP4iEeiVrKKMla1zGg" value="GVJAST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zZX0ff4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zZX0fv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zZX0f_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zZX0gP4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_zZX0gf4iEeiVrKKMla1zGg" name="GVF1NB" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_zZX0gv4iEeiVrKKMla1zGg" value="GVF1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_zZX0g_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_zZX0hP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_zZX0hf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_zZX0hv4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_zZX0h_4iEeiVrKKMla1zGg" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_z_gnUP4iEeiVrKKMla1zGg" name="OSK7REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_z_gnUf4iEeiVrKKMla1zGg" value="OSK7REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_z_gnUv4iEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_z_5B0P4iEeiVrKKMla1zGg" value="OSK7REP   "/>
      <node defType="com.stambia.rdbms.column" id="_0BUlMP4iEeiVrKKMla1zGg" name="K7AZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_0BUlMf4iEeiVrKKMla1zGg" value="K7AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0BUlMv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0BUlM_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0BUlNP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0BUlNf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0BUlNv4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0BUlN_4iEeiVrKKMla1zGg" name="K7S7CD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_0BUlOP4iEeiVrKKMla1zGg" value="K7S7CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0BUlOf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0BUlOv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0BUlO_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0BUlPP4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0BUlPf4iEeiVrKKMla1zGg" name="K7MFC1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_0BUlPv4iEeiVrKKMla1zGg" value="K7MFC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0BUlP_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0BUlQP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0BUlQf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0BUlQv4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0BUlQ_4iEeiVrKKMla1zGg" name="K7V7S2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_0BUlRP4iEeiVrKKMla1zGg" value="K7V7S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0BUlRf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0BUlRv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0BUlR_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0BUlSP4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0BUlSf4iEeiVrKKMla1zGg" name="K7V8S2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_0BUlSv4iEeiVrKKMla1zGg" value="K7V8S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0BUlS_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0BUlTP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0BUlTf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0BUlTv4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0BUlT_4iEeiVrKKMla1zGg" name="K7V9S2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_0BUlUP4iEeiVrKKMla1zGg" value="K7V9S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0BUlUf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0BUlUv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0BUlU_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0BUlVP4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0BUlVf4iEeiVrKKMla1zGg" name="K7V8T1" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_0BUlVv4iEeiVrKKMla1zGg" value="K7V8T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0BUlV_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0BUlWP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0BUlWf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0BUlWv4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0BUlW_4iEeiVrKKMla1zGg" name="K7A7N6" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_0BUlXP4iEeiVrKKMla1zGg" value="K7A7N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0BUlXf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0BUlXv4iEeiVrKKMla1zGg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0BUlX_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0BUlYP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0BUlYf4iEeiVrKKMla1zGg" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_0CNWAP4iEeiVrKKMla1zGg" name="OSMVCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_0CNWAf4iEeiVrKKMla1zGg" value="OSMVCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_0CNWAv4iEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_0CSOgP4iEeiVrKKMla1zGg" value="OSMVCPP   "/>
      <node defType="com.stambia.rdbms.column" id="_0DUJQP4iEeiVrKKMla1zGg" name="MVAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DUJQf4iEeiVrKKMla1zGg" value="MVAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DUJQv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0DUJQ_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DUJRP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DUJRf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DUJRv4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DUwUP4iEeiVrKKMla1zGg" name="MVGHCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DUwUf4iEeiVrKKMla1zGg" value="MVGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DUwUv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DUwU_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DUwVP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DUwVf4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DUwVv4iEeiVrKKMla1zGg" name="MVJ6C1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DUwV_4iEeiVrKKMla1zGg" value="MVJ6C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DUwWP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DUwWf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DUwWv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DUwW_4iEeiVrKKMla1zGg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DVXYP4iEeiVrKKMla1zGg" name="MVLQN3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DVXYf4iEeiVrKKMla1zGg" value="MVLQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DVXYv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DVXY_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DVXZP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DVXZf4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DVXZv4iEeiVrKKMla1zGg" name="MVJZC1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DVXZ_4iEeiVrKKMla1zGg" value="MVJZC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DVXaP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DVXaf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DVXav4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DVXa_4iEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DVXbP4iEeiVrKKMla1zGg" name="MVJ0C1" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DVXbf4iEeiVrKKMla1zGg" value="MVJ0C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DVXbv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DVXb_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DVXcP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DVXcf4iEeiVrKKMla1zGg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DV-cP4iEeiVrKKMla1zGg" name="MVYAT1" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DV-cf4iEeiVrKKMla1zGg" value="MVYAT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DV-cv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DV-c_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DV-dP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DV-df4iEeiVrKKMla1zGg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DV-dv4iEeiVrKKMla1zGg" name="MVYBT1" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DV-d_4iEeiVrKKMla1zGg" value="MVYBT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DV-eP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DV-ef4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DV-ev4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DV-e_4iEeiVrKKMla1zGg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DV-fP4iEeiVrKKMla1zGg" name="MVYCT1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DV-ff4iEeiVrKKMla1zGg" value="MVYCT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DWlgP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DWlgf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DWlgv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DWlg_4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DWlhP4iEeiVrKKMla1zGg" name="MVYDT1" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DWlhf4iEeiVrKKMla1zGg" value="MVYDT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DWlhv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DWlh_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DWliP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DWlif4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DWliv4iEeiVrKKMla1zGg" name="MVYET1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DWli_4iEeiVrKKMla1zGg" value="MVYET1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DWljP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DWljf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DWljv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DWlj_4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DXMkP4iEeiVrKKMla1zGg" name="MVYFT1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DXMkf4iEeiVrKKMla1zGg" value="MVYFT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DXMkv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DXMk_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DXMlP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DXMlf4iEeiVrKKMla1zGg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DXMlv4iEeiVrKKMla1zGg" name="MVYGT1" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DXMl_4iEeiVrKKMla1zGg" value="MVYGT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DXMmP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DXMmf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DXMmv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DXMm_4iEeiVrKKMla1zGg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DXMnP4iEeiVrKKMla1zGg" name="MVZ1N5" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DXMnf4iEeiVrKKMla1zGg" value="MVZ1N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DXMnv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0DXMn_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DXMoP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DXMof4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DXMov4iEeiVrKKMla1zGg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DXzoP4iEeiVrKKMla1zGg" name="MVZ2N5" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DXzof4iEeiVrKKMla1zGg" value="MVZ2N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DXzov4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0DXzo_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DXzpP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DXzpf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DXzpv4iEeiVrKKMla1zGg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DXzp_4iEeiVrKKMla1zGg" name="MVYAS2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DXzqP4iEeiVrKKMla1zGg" value="MVYAS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DXzqf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DXzqv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DXzq_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DXzrP4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DXzrf4iEeiVrKKMla1zGg" name="MVYHT1" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DXzrv4iEeiVrKKMla1zGg" value="MVYHT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DXzr_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DXzsP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DXzsf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DXzsv4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DYasP4iEeiVrKKMla1zGg" name="MVYIT1" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DYasf4iEeiVrKKMla1zGg" value="MVYIT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DYasv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DYas_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DYatP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DYatf4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DYatv4iEeiVrKKMla1zGg" name="MVYJT1" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DYat_4iEeiVrKKMla1zGg" value="MVYJT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DYauP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DYauf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DYauv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DYau_4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DYavP4iEeiVrKKMla1zGg" name="MVYKT1" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DYavf4iEeiVrKKMla1zGg" value="MVYKT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DYavv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZBwP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZBwf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZBwv4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZBw_4iEeiVrKKMla1zGg" name="MVYLT1" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZBxP4iEeiVrKKMla1zGg" value="MVYLT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZBxf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZBxv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZBx_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZByP4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZByf4iEeiVrKKMla1zGg" name="MVYMT1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZByv4iEeiVrKKMla1zGg" value="MVYMT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZBy_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZBzP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZBzf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZBzv4iEeiVrKKMla1zGg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZBz_4iEeiVrKKMla1zGg" name="MVYNT1" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZB0P4iEeiVrKKMla1zGg" value="MVYNT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZB0f4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZB0v4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZB0_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZB1P4iEeiVrKKMla1zGg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZB1f4iEeiVrKKMla1zGg" name="MVYOT1" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZB1v4iEeiVrKKMla1zGg" value="MVYOT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZB1_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZB2P4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZB2f4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZB2v4iEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZB2_4iEeiVrKKMla1zGg" name="MVYPT1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZB3P4iEeiVrKKMla1zGg" value="MVYPT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZB3f4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZB3v4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZB3_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZB4P4iEeiVrKKMla1zGg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZB4f4iEeiVrKKMla1zGg" name="MVYQT1" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZB4v4iEeiVrKKMla1zGg" value="MVYQT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZB4_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZB5P4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZB5f4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZB5v4iEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZB5_4iEeiVrKKMla1zGg" name="MVYRT1" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZB6P4iEeiVrKKMla1zGg" value="MVYRT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZB6f4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZB6v4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZB6_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZB7P4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZB7f4iEeiVrKKMla1zGg" name="MVYST1" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZB7v4iEeiVrKKMla1zGg" value="MVYST1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZB7_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZB8P4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZB8f4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZB8v4iEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZB8_4iEeiVrKKMla1zGg" name="MVYTT1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZB9P4iEeiVrKKMla1zGg" value="MVYTT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZB9f4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZB9v4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZB9_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZB-P4iEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZB-f4iEeiVrKKMla1zGg" name="MVYUT1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZB-v4iEeiVrKKMla1zGg" value="MVYUT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZB-_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZB_P4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZB_f4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZB_v4iEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZB__4iEeiVrKKMla1zGg" name="MVYVT1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCAP4iEeiVrKKMla1zGg" value="MVYVT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCAf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCAv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCA_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCBP4iEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCBf4iEeiVrKKMla1zGg" name="MVYWT1" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCBv4iEeiVrKKMla1zGg" value="MVYWT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCB_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCCP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCCf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCCv4iEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCC_4iEeiVrKKMla1zGg" name="MVYXT1" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCDP4iEeiVrKKMla1zGg" value="MVYXT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCDf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCDv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCD_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCEP4iEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCEf4iEeiVrKKMla1zGg" name="MVY2T1" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCEv4iEeiVrKKMla1zGg" value="MVY2T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCE_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCFP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCFf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCFv4iEeiVrKKMla1zGg" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCF_4iEeiVrKKMla1zGg" name="MVY3T1" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCGP4iEeiVrKKMla1zGg" value="MVY3T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCGf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCGv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCG_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCHP4iEeiVrKKMla1zGg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCHf4iEeiVrKKMla1zGg" name="MVY4T1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCHv4iEeiVrKKMla1zGg" value="MVY4T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCH_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCIP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCIf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCIv4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCI_4iEeiVrKKMla1zGg" name="MVY5T1" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCJP4iEeiVrKKMla1zGg" value="MVY5T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCJf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCJv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCJ_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCKP4iEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCKf4iEeiVrKKMla1zGg" name="MVY6T1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCKv4iEeiVrKKMla1zGg" value="MVY6T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCK_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCLP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCLf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCLv4iEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCL_4iEeiVrKKMla1zGg" name="MVYYT1" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCMP4iEeiVrKKMla1zGg" value="MVYYT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCMf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCMv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCM_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCNP4iEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCNf4iEeiVrKKMla1zGg" name="MVYZT1" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCNv4iEeiVrKKMla1zGg" value="MVYZT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCN_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCOP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCOf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCOv4iEeiVrKKMla1zGg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCO_4iEeiVrKKMla1zGg" name="MVY0T1" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCPP4iEeiVrKKMla1zGg" value="MVY0T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCPf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCPv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCP_4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCQP4iEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCQf4iEeiVrKKMla1zGg" name="MVY1T1" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCQv4iEeiVrKKMla1zGg" value="MVY1T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCQ_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCRP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCRf4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCRv4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCR_4iEeiVrKKMla1zGg" name="MVEHN6" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCSP4iEeiVrKKMla1zGg" value="MVEHN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCSf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0DZCSv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCS_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCTP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCTf4iEeiVrKKMla1zGg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCTv4iEeiVrKKMla1zGg" name="MVNPC1" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCT_4iEeiVrKKMla1zGg" value="MVNPC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCUP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCUf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCUv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCU_4iEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCVP4iEeiVrKKMla1zGg" name="MVNQC1" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCVf4iEeiVrKKMla1zGg" value="MVNQC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCVv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCV_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCWP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCWf4iEeiVrKKMla1zGg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCWv4iEeiVrKKMla1zGg" name="MVHHT2" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCW_4iEeiVrKKMla1zGg" value="MVHHT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCXP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCXf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCXv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCX_4iEeiVrKKMla1zGg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCYP4iEeiVrKKMla1zGg" name="MVHIT2" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCYf4iEeiVrKKMla1zGg" value="MVHIT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCYv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCY_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCZP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCZf4iEeiVrKKMla1zGg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0DZCZv4iEeiVrKKMla1zGg" name="MVHGT2" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_0DZCZ_4iEeiVrKKMla1zGg" value="MVHGT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0DZCaP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0DZCaf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0DZCav4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0DZCa_4iEeiVrKKMla1zGg" value="76"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_0P7B4P4iEeiVrKKMla1zGg" name="OTIJCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_0P7B4f4iEeiVrKKMla1zGg" value="OTIJCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_0P7B4v4iEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_0QAhcP4iEeiVrKKMla1zGg" value="OTIJCPP   "/>
      <node defType="com.stambia.rdbms.column" id="_0Qr24P4iEeiVrKKMla1zGg" name="IJAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_0Qr24f4iEeiVrKKMla1zGg" value="IJAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0Qr24v4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_0Qr24_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0Qr25P4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0Qr25f4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0Qr25v4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0Qsd8P4iEeiVrKKMla1zGg" name="IJOFS3" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_0Qsd8f4iEeiVrKKMla1zGg" value="IJOFS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0Qsd8v4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0Qsd8_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0Qsd9P4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0Qsd9f4iEeiVrKKMla1zGg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0QtFAP4iEeiVrKKMla1zGg" name="IJXAC1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_0QtFAf4iEeiVrKKMla1zGg" value="IJXAC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0QtFAv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0QtFA_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0QtFBP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0QtFBf4iEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0QtFBv4iEeiVrKKMla1zGg" name="IJXBC1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_0QtFB_4iEeiVrKKMla1zGg" value="IJXBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0QtFCP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0QtFCf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0QtFCv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0QtFC_4iEeiVrKKMla1zGg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0QtsEP4iEeiVrKKMla1zGg" name="IJOGS3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_0QtsEf4iEeiVrKKMla1zGg" value="IJOGS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0QtsEv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0QtsE_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0QtsFP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0QtsFf4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0QuTIP4iEeiVrKKMla1zGg" name="IJOHS3" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_0QuTIf4iEeiVrKKMla1zGg" value="IJOHS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0QuTIv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0QuTI_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0QuTJP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0QuTJf4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0QuTJv4iEeiVrKKMla1zGg" name="IJOIS3" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_0QuTJ_4iEeiVrKKMla1zGg" value="IJOIS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0QuTKP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0QuTKf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0QuTKv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0QuTK_4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0Qu6MP4iEeiVrKKMla1zGg" name="IJOJS3" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_0Qu6Mf4iEeiVrKKMla1zGg" value="IJOJS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0Qu6Mv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0Qu6M_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0Qu6NP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0Qu6Nf4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0Qu6Nv4iEeiVrKKMla1zGg" name="IJBOCD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_0Qu6N_4iEeiVrKKMla1zGg" value="IJBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0Qu6OP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0Qu6Of4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0Qu6Ov4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0Qu6O_4iEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0Qu6PP4iEeiVrKKMla1zGg" name="IJHRST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_0Qu6Pf4iEeiVrKKMla1zGg" value="IJHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0Qu6Pv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0Qu6P_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0Qu6QP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0Qu6Qf4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0QvhQP4iEeiVrKKMla1zGg" name="IJOKS3" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_0QvhQf4iEeiVrKKMla1zGg" value="IJOKS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0QvhQv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0QvhQ_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0QvhRP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0QvhRf4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0QvhRv4iEeiVrKKMla1zGg" name="IJOLS3" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_0QvhR_4iEeiVrKKMla1zGg" value="IJOLS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0QvhSP4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0QvhSf4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0QvhSv4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0QvhS_4iEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_0QvhTP4iEeiVrKKMla1zGg" name="IJQYS3" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_0QvhTf4iEeiVrKKMla1zGg" value="IJQYS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_0QvhTv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_0QvhT_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_0QvhUP4iEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_0QvhUf4iEeiVrKKMla1zGg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_zf7IUP4iEeiVrKKMla1zGg" name="CLIENT_SEG">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_zf7IUf4iEeiVrKKMla1zGg" value="CLIENT_SEG"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_zf7IUv4iEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_zgODQP4iEeiVrKKMla1zGg" value="CLIENT_SEG"/>
      <node defType="com.stambia.rdbms.column" id="_z-LKkP4iEeiVrKKMla1zGg" name="AZAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_z-LKkf4iEeiVrKKMla1zGg" value="AZAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z-LKkv4iEeiVrKKMla1zGg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_z-LKk_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z-LKlP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z-LKlf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z-LKlv4iEeiVrKKMla1zGg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_z-LxoP4iEeiVrKKMla1zGg" name="IDMARC" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_z-Lxof4iEeiVrKKMla1zGg" value="IDMARC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z-Lxov4iEeiVrKKMla1zGg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_z-Lxo_4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z-LxpP4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z-Lxpf4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z-Lxpv4iEeiVrKKMla1zGg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_z-Lxp_4iEeiVrKKMla1zGg" name="IDACTI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_z-LxqP4iEeiVrKKMla1zGg" value="IDACTI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z-Lxqf4iEeiVrKKMla1zGg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_z-Lxqv4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z-Lxq_4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z-LxrP4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z-Lxrf4iEeiVrKKMla1zGg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_z-Lxrv4iEeiVrKKMla1zGg" name="IDDETA" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_z-Lxr_4iEeiVrKKMla1zGg" value="IDDETA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_z-LxsP4iEeiVrKKMla1zGg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_z-Lxsf4iEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_z-Lxsv4iEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_z-Lxs_4iEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_z-LxtP4iEeiVrKKMla1zGg" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_T5Blcf8GEeiVrKKMla1zGg" name="ORJGCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_T5Blcv8GEeiVrKKMla1zGg" value="ORJGCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_T5Blc_8GEeiVrKKMla1zGg" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_T5UgYP8GEeiVrKKMla1zGg" value="ORJGCPP   "/>
      <node defType="com.stambia.rdbms.column" id="_T6EHQP8GEeiVrKKMla1zGg" name="JGKBCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_T6EHQf8GEeiVrKKMla1zGg" value="JGKBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_T6EHQv8GEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_T6EHQ_8GEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_T6EHRP8GEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_T6EHRf8GEeiVrKKMla1zGg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_T6EHRv8GEeiVrKKMla1zGg" name="JGAMCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_T6EHR_8GEeiVrKKMla1zGg" value="JGAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_T6EHSP8GEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_T6EHSf8GEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_T6EHSv8GEeiVrKKMla1zGg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_T6EHS_8GEeiVrKKMla1zGg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_T6EHTP8GEeiVrKKMla1zGg" name="JGNINB" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_T6EHTf8GEeiVrKKMla1zGg" value="JGNINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_T6EHTv8GEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_T6EHT_8GEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_T6EHUP8GEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_T6EHUf8GEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_T6EHUv8GEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_T6EHU_8GEeiVrKKMla1zGg" name="JGPONB" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_T6EHVP8GEeiVrKKMla1zGg" value="JGPONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_T6EHVf8GEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_T6EHVv8GEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_T6EHV_8GEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_T6EHWP8GEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_T6EHWf8GEeiVrKKMla1zGg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_T6EHWv8GEeiVrKKMla1zGg" name="JGTTNB" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_T6EHW_8GEeiVrKKMla1zGg" value="JGTTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_T6EHXP8GEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_T6EHXf8GEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_T6EHXv8GEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_T6EHX_8GEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_T6EHYP8GEeiVrKKMla1zGg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_XqiHkQQ9EemZAt8vLIIhpA" name="OSNCREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_XqrRgAQ9EemZAt8vLIIhpA" value="OSNCREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_XqrRgQQ9EemZAt8vLIIhpA" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_Xq1CgAQ9EemZAt8vLIIhpA" value="OSNCREP   "/>
      <node defType="com.stambia.rdbms.column" id="_XsBVUAQ9EemZAt8vLIIhpA" name="NCAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVUQQ9EemZAt8vLIIhpA" value="NCAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVUgQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XsBVUwQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVVAQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVVQQ9EemZAt8vLIIhpA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVVgQ9EemZAt8vLIIhpA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBVVwQ9EemZAt8vLIIhpA" name="NCYCS2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVWAQ9EemZAt8vLIIhpA" value="NCYCS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVWQQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVWgQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVWwQ9EemZAt8vLIIhpA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVXAQ9EemZAt8vLIIhpA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBVXQQ9EemZAt8vLIIhpA" name="NCYMS2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVXgQ9EemZAt8vLIIhpA" value="NCYMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVXwQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVYAQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVYQQ9EemZAt8vLIIhpA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVYgQ9EemZAt8vLIIhpA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBVYwQ9EemZAt8vLIIhpA" name="NCYDS2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVZAQ9EemZAt8vLIIhpA" value="NCYDS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVZQQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVZgQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVZwQ9EemZAt8vLIIhpA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVaAQ9EemZAt8vLIIhpA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBVaQQ9EemZAt8vLIIhpA" name="NCRSDT" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVagQ9EemZAt8vLIIhpA" value="NCRSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVawQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XsBVbAQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVbQQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVbgQ9EemZAt8vLIIhpA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVbwQ9EemZAt8vLIIhpA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBVcAQ9EemZAt8vLIIhpA" name="NCYES2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVcQQ9EemZAt8vLIIhpA" value="NCYES2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVcgQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVcwQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVdAQ9EemZAt8vLIIhpA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVdQQ9EemZAt8vLIIhpA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBVdgQ9EemZAt8vLIIhpA" name="NCRTDT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVdwQ9EemZAt8vLIIhpA" value="NCRTDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVeAQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XsBVeQQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVegQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVewQ9EemZAt8vLIIhpA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVfAQ9EemZAt8vLIIhpA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBVfQQ9EemZAt8vLIIhpA" name="NCZ5N5" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVfgQ9EemZAt8vLIIhpA" value="NCZ5N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVfwQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XsBVgAQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVgQQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVggQ9EemZAt8vLIIhpA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVgwQ9EemZAt8vLIIhpA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBVhAQ9EemZAt8vLIIhpA" name="NCRUDT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVhQQ9EemZAt8vLIIhpA" value="NCRUDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVhgQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XsBVhwQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBViAQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBViQQ9EemZAt8vLIIhpA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVigQ9EemZAt8vLIIhpA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBViwQ9EemZAt8vLIIhpA" name="NCZ6N5" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVjAQ9EemZAt8vLIIhpA" value="NCZ6N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVjQQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XsBVjgQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVjwQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVkAQ9EemZAt8vLIIhpA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVkQQ9EemZAt8vLIIhpA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_XsBVkgQ9EemZAt8vLIIhpA" name="NCRVDT" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_XsBVkwQ9EemZAt8vLIIhpA" value="NCRVDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_XsBVlAQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_XsBVlQQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_XsBVlgQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_XsBVlwQ9EemZAt8vLIIhpA" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_XsBVmAQ9EemZAt8vLIIhpA" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_wM0KkAQ9EemZAt8vLIIhpA" name="OSNEREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_wM0KkQQ9EemZAt8vLIIhpA" value="OSNEREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_wM0KkgQ9EemZAt8vLIIhpA" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_wNHFgAQ9EemZAt8vLIIhpA" value="OSNEREP   "/>
      <node defType="com.stambia.rdbms.column" id="_wOJnUAQ9EemZAt8vLIIhpA" name="NEZ7N5" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_wOJnUQQ9EemZAt8vLIIhpA" value="NEZ7N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wOJnUgQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_wOJnUwQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wOJnVAQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wOJnVQQ9EemZAt8vLIIhpA" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wOJnVgQ9EemZAt8vLIIhpA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wOJnVwQ9EemZAt8vLIIhpA" name="NEYFS2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_wOJnWAQ9EemZAt8vLIIhpA" value="NEYFS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wOJnWQQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wOJnWgQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wOJnWwQ9EemZAt8vLIIhpA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wOJnXAQ9EemZAt8vLIIhpA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wOJnXQQ9EemZAt8vLIIhpA" name="NEYGS2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_wOJnXgQ9EemZAt8vLIIhpA" value="NEYGS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wOJnXwQ9EemZAt8vLIIhpA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wOJnYAQ9EemZAt8vLIIhpA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wOJnYQQ9EemZAt8vLIIhpA" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_wOJnYgQ9EemZAt8vLIIhpA" value="1"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_qO9fYMF-EeilU77n104Zew" name="PATRICK">
    <attribute defType="com.stambia.rdbms.schema.name" id="_qP1CEMF-EeilU77n104Zew" value="PATRICK"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_qP1CEcF-EeilU77n104Zew" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_qP1pIMF-EeilU77n104Zew" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_qP1pIcF-EeilU77n104Zew" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_uA0ncMF-EeilU77n104Zew" name="WS_LOG_RESULT">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_uA0nccF-EeilU77n104Zew" value="WS_LOG_RESULT"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_uA1OgMF-EeilU77n104Zew" value="TABLE"/>
      <attribute defType="com.stambia.rdbms.datastore.fileName" id="_uA9xYMF-EeilU77n104Zew" value="WS_LO00001"/>
      <node defType="com.stambia.rdbms.column" id="_uE7EUMF-EeilU77n104Zew" name="SESSION_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_uE7EUcF-EeilU77n104Zew" value="SESSION_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_uE7EUsF-EeilU77n104Zew" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_uE7EU8F-EeilU77n104Zew" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_uE7EVMF-EeilU77n104Zew" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_uE7EVcF-EeilU77n104Zew" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_uE85gMF-EeilU77n104Zew" name="RESPONSE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_uE85gcF-EeilU77n104Zew" value="RESPONSE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_uE85gsF-EeilU77n104Zew" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_uE85g8F-EeilU77n104Zew" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_uE85hMF-EeilU77n104Zew" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_uE85hcF-EeilU77n104Zew" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_uE-usMF-EeilU77n104Zew" name="FAULT_ACTOR" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_uE-uscF-EeilU77n104Zew" value="FAULT_ACTOR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_uE-ussF-EeilU77n104Zew" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_uE-us8F-EeilU77n104Zew" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_uE-utMF-EeilU77n104Zew" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_uE-utcF-EeilU77n104Zew" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_uE_80MF-EeilU77n104Zew" name="FAULT_CODE" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_uE_80cF-EeilU77n104Zew" value="FAULT_CODE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_uE_80sF-EeilU77n104Zew" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_uE_808F-EeilU77n104Zew" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_uE_81MF-EeilU77n104Zew" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_uE_81cF-EeilU77n104Zew" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_uFBK8MF-EeilU77n104Zew" name="FAULT_STRING" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_uFBK8cF-EeilU77n104Zew" value="FAULT_STRING"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_uFBK8sF-EeilU77n104Zew" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_uFBK88F-EeilU77n104Zew" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_uFBK9MF-EeilU77n104Zew" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_uFBK9cF-EeilU77n104Zew" value="1000"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.queryFolder" id="_eUqUYf42EeiVrKKMla1zGg" name="CustomerNumber">
    <node defType="com.stambia.rdbms.query" id="_gLn2AP42EeiVrKKMla1zGg" name="GetCustomerNumber">
      <attribute defType="com.stambia.rdbms.query.expression" id="_Ox24YP43EeiVrKKMla1zGg" value="SELECT JGTTNB FROM orionbd.orjgcpp WHERE JGKBCD='ORAZREP'"/>
      <node defType="com.stambia.rdbms.column" id="_269lkP8DEeiVrKKMla1zGg" name="JGTTNB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_269lkf8DEeiVrKKMla1zGg" value="JGTTNB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_269lkv8DEeiVrKKMla1zGg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_269lk_8DEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_269llP8DEeiVrKKMla1zGg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_269llf8DEeiVrKKMla1zGg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_269llv8DEeiVrKKMla1zGg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_Pezd8P43EeiVrKKMla1zGg" name="UpdateCustomerCounter">
      <attribute defType="com.stambia.rdbms.query.expression" id="_bOBeIP43EeiVrKKMla1zGg" value="update orionbd.orjgcpp set JGTTNB = JGTTNB + 1 WHERE JGKBCD='ORAZREP'"/>
    </node>
  </node>
  <node defType="com.stambia.rdbms.schema" id="_gg8lUP8JEemVAft-X6rPVg" name="STAMBIA">
    <attribute defType="com.stambia.rdbms.schema.name" id="_ghbtgP8JEemVAft-X6rPVg" value="STAMBIA"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_ghcUkP8JEemVAft-X6rPVg" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_ghcUkf8JEemVAft-X6rPVg" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_ghcUkv8JEemVAft-X6rPVg" value="I_[targetName]"/>
  </node>
</md:node>