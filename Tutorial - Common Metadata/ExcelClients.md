<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_ijUakN0jEeiUprvGXSQ59A" name="ExcelClients" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/excel/excel.rdbms.md#UUID_MD_RDBMS_MICROSOFT_EXCEL?fileId=UUID_MD_RDBMS_MICROSOFT_EXCEL$type=md$name=Microsoft%20Excel?">
  <attribute defType="com.stambia.rdbms.server.url" id="_wHslkN0jEeiUprvGXSQ59A" value="jdbc:stambia:excel://c:/app_dev/Retour_CHD.xlsx?forceDatatypeAsString=true&amp;columnNameStyle=PRESERVE"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_wHslkd0jEeiUprvGXSQ59A" value="com.stambia.jdbc.driver.excel.XLSXDriver"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_wHtzsN0jEeiUprvGXSQ59A" value="3951C0D79B227B95C1DC348DD0BCE8F1"/>
  <node defType="com.stambia.rdbms.schema" id="_iqhOwN0jEeiUprvGXSQ59A" name="Retour_CHD">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_irHEoN0jEeiUprvGXSQ59A" value="Retour_CHD"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_irHEod0jEeiUprvGXSQ59A" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_irHEot0jEeiUprvGXSQ59A" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_irHEo90jEeiUprvGXSQ59A" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_vzURAd0jEeiUprvGXSQ59A" name="DATA$">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vzURAt0jEeiUprvGXSQ59A" value="DATA$"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vzURA90jEeiUprvGXSQ59A" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_wHhmcN0jEeiUprvGXSQ59A" name="CodCli" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmcd0jEeiUprvGXSQ59A" value="CodCli"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmct0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmc90jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmdN0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmdd0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmdt0jEeiUprvGXSQ59A" name="RS" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmd90jEeiUprvGXSQ59A" value="RS"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmeN0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmed0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmet0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhme90jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmfN0jEeiUprvGXSQ59A" name="Enseigne" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmfd0jEeiUprvGXSQ59A" value="Enseigne"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmft0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmf90jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmgN0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmgd0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmgt0jEeiUprvGXSQ59A" name="Adresse normalisée" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmg90jEeiUprvGXSQ59A" value="Adresse normalisée"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmhN0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmhd0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmht0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmh90jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmiN0jEeiUprvGXSQ59A" name="Code Postal normalisée" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmid0jEeiUprvGXSQ59A" value="Code Postal normalisée"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmit0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmi90jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmjN0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmjd0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmjt0jEeiUprvGXSQ59A" name="Ville normalisée" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmj90jEeiUprvGXSQ59A" value="Ville normalisée"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmkN0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmkd0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmkt0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmk90jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmlN0jEeiUprvGXSQ59A" name="Statut de normalisation" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmld0jEeiUprvGXSQ59A" value="Statut de normalisation"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmlt0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhml90jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmmN0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmmd0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmmt0jEeiUprvGXSQ59A" name="Dep" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmm90jEeiUprvGXSQ59A" value="Dep"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmnN0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmnd0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmnt0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmn90jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmoN0jEeiUprvGXSQ59A" name="Phone" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmod0jEeiUprvGXSQ59A" value="Phone"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmot0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmo90jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmpN0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmpd0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmpt0jEeiUprvGXSQ59A" name="SIREN" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmp90jEeiUprvGXSQ59A" value="SIREN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmqN0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmqd0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmqt0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmq90jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmrN0jEeiUprvGXSQ59A" name="Nic" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmrd0jEeiUprvGXSQ59A" value="Nic"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmrt0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmr90jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmsN0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmsd0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmst0jEeiUprvGXSQ59A" name="Country" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhms90jEeiUprvGXSQ59A" value="Country"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmtN0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmtd0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmtt0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmt90jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmuN0jEeiUprvGXSQ59A" name="Année dernière commande" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmud0jEeiUprvGXSQ59A" value="Année dernière commande"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmut0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmu90jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmvN0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmvd0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmvt0jEeiUprvGXSQ59A" name="Matché" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmv90jEeiUprvGXSQ59A" value="Matché"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmwN0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmwd0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmwt0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmw90jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmxN0jEeiUprvGXSQ59A" name="CHD ID" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmxd0jEeiUprvGXSQ59A" value="CHD ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmxt0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmx90jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmyN0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmyd0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhmyt0jEeiUprvGXSQ59A" name="Nom commercial" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhmy90jEeiUprvGXSQ59A" value="Nom commercial"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhmzN0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhmzd0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhmzt0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhmz90jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhm0N0jEeiUprvGXSQ59A" name="Dénomination sociale" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhm0d0jEeiUprvGXSQ59A" value="Dénomination sociale"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhm0t0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhm090jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhm1N0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhm1d0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhm1t0jEeiUprvGXSQ59A" name="Adresse" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhm190jEeiUprvGXSQ59A" value="Adresse"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhm2N0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHhm2d0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHhm2t0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHhm290jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHhm4t0jEeiUprvGXSQ59A" name="Lieu-dit, boîte postale" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHhm490jEeiUprvGXSQ59A" value="Lieu-dit, boîte postale"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHhm5N0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHme8N0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHme8d0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHme8t0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHme890jEeiUprvGXSQ59A" name="Code postal" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHme9N0jEeiUprvGXSQ59A" value="Code postal"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHme9d0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHme9t0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHme990jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHme-N0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHme-d0jEeiUprvGXSQ59A" name="Ville" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHme-t0jEeiUprvGXSQ59A" value="Ville"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHme-90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHme_N0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHme_d0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHme_t0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHme_90jEeiUprvGXSQ59A" name="Téléphone" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfAN0jEeiUprvGXSQ59A" value="Téléphone"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfAd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfAt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfA90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfBN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfBd0jEeiUprvGXSQ59A" name="Fax" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfBt0jEeiUprvGXSQ59A" value="Fax"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfB90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfCN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfCd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfCt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfC90jEeiUprvGXSQ59A" name="SIRET" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfDN0jEeiUprvGXSQ59A" value="SIRET"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfDd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfDt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfD90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfEN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfEd0jEeiUprvGXSQ59A" name="SIREN1" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfEt0jEeiUprvGXSQ59A" value="SIREN1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfE90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfFN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfFd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfFt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfF90jEeiUprvGXSQ59A" name="Site internet" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfGN0jEeiUprvGXSQ59A" value="Site internet"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfGd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfGt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfG90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfHN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfHd0jEeiUprvGXSQ59A" name="Nom chaîne" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfHt0jEeiUprvGXSQ59A" value="Nom chaîne"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfH90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfIN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfId0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfIt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfI90jEeiUprvGXSQ59A" name="Type" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfJN0jEeiUprvGXSQ59A" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfJd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfJt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfJ90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfKN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfKd0jEeiUprvGXSQ59A" name="GFC Code" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfKt0jEeiUprvGXSQ59A" value="GFC Code"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfK90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfLN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfLd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfLt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfL90jEeiUprvGXSQ59A" name="Segment" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfMN0jEeiUprvGXSQ59A" value="Segment"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfMd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfMt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfM90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfNN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfNd0jEeiUprvGXSQ59A" name="Activité détaillée" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfNt0jEeiUprvGXSQ59A" value="Activité détaillée"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfN90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfON0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfOd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfOt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfO90jEeiUprvGXSQ59A" name="Type de menu" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfPN0jEeiUprvGXSQ59A" value="Type de menu"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfPd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfPt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfP90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfQN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfQd0jEeiUprvGXSQ59A" name="Nb de chambres tranche" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfQt0jEeiUprvGXSQ59A" value="Nb de chambres tranche"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfQ90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfRN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfRd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfRt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfR90jEeiUprvGXSQ59A" name="Nb de chambres" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfSN0jEeiUprvGXSQ59A" value="Nb de chambres"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfSd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfSt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfS90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfTN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfX90jEeiUprvGXSQ59A" name="Nb de couverts/jour" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfYN0jEeiUprvGXSQ59A" value="Nb de couverts/jour"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfYd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfYt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfY90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfZN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfZd0jEeiUprvGXSQ59A" name="Ticket moyen repas" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfZt0jEeiUprvGXSQ59A" value="Ticket moyen repas"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfZ90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfaN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfad0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfat0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfa90jEeiUprvGXSQ59A" name="Nb pièces de pains/jour" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfbN0jEeiUprvGXSQ59A" value="Nb pièces de pains/jour"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfbd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfbt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfb90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfcN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfcd0jEeiUprvGXSQ59A" name="Propriété" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfct0jEeiUprvGXSQ59A" value="Propriété"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfc90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfdN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfdd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfdt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmffd0jEeiUprvGXSQ59A" name="Nb repas par jours" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfft0jEeiUprvGXSQ59A" value="Nb repas par jours"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmff90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfgN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfgd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfgt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfg90jEeiUprvGXSQ59A" name="Nb de lits" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfhN0jEeiUprvGXSQ59A" value="Nb de lits"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfhd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfht0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfh90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfiN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfj90jEeiUprvGXSQ59A" name="Niveau de confiance" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfkN0jEeiUprvGXSQ59A" value="Niveau de confiance"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfkd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfkt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfk90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmflN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfld0jEeiUprvGXSQ59A" name="Interlocuteur" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmflt0jEeiUprvGXSQ59A" value="Interlocuteur"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfl90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfmN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfmd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfmt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfm90jEeiUprvGXSQ59A" name="Latitude" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfnN0jEeiUprvGXSQ59A" value="Latitude"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfnd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfnt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfn90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfoN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfod0jEeiUprvGXSQ59A" name="Longitude" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfot0jEeiUprvGXSQ59A" value="Longitude"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfo90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfpN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfpd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfpt0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfp90jEeiUprvGXSQ59A" name="Présence terrasse" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfqN0jEeiUprvGXSQ59A" value="Présence terrasse"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfqd0jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfqt0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfq90jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfrN0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_wHmfrd0jEeiUprvGXSQ59A" name="Horaires" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_wHmfrt0jEeiUprvGXSQ59A" value="Horaires"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_wHmfr90jEeiUprvGXSQ59A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_wHmfsN0jEeiUprvGXSQ59A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_wHmfsd0jEeiUprvGXSQ59A" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_wHmfst0jEeiUprvGXSQ59A" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_1piHTt6_Eei89e-w6JO_mw" name="Nb élèves" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_1piHT96_Eei89e-w6JO_mw" value="Nb élèves"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_1piHUN6_Eei89e-w6JO_mw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_1piHUd6_Eei89e-w6JO_mw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_1piHUt6_Eei89e-w6JO_mw" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_1piHU96_Eei89e-w6JO_mw" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5iLW6t7AEei89e-w6JO_mw" name="Nb employés" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_5iLW697AEei89e-w6JO_mw" value="Nb employés"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5iLW7N7AEei89e-w6JO_mw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5iLW7d7AEei89e-w6JO_mw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_5iLW7t7AEei89e-w6JO_mw" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5iLW797AEei89e-w6JO_mw" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5iLXCN7AEei89e-w6JO_mw" name="Type opération" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_5iLXCd7AEei89e-w6JO_mw" value="Type opération"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5iLXCt7AEei89e-w6JO_mw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5iLXC97AEei89e-w6JO_mw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_5iLXDN7AEei89e-w6JO_mw" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5iLXDd7AEei89e-w6JO_mw" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJRu-HtEeiFj_ce9nNTCg" name="Complément Adresse" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJRvOHtEeiFj_ce9nNTCg" value="Complément Adresse"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJRveHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJRvuHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJRv-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJRwOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSaeHtEeiFj_ce9nNTCg" name="Date fermeture" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSauHtEeiFj_ce9nNTCg" value="Date fermeture"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSa-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSbOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSbeHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSbuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSb-HtEeiFj_ce9nNTCg" name="Date ouverture" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJScOHtEeiFj_ce9nNTCg" value="Date ouverture"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSceHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJScuHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSc-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSdOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSk-HtEeiFj_ce9nNTCg" name="lundi_ouvre1" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSlOHtEeiFj_ce9nNTCg" value="lundi_ouvre1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSleHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSluHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSl-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSmOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSmeHtEeiFj_ce9nNTCg" name="lundi_ferme1" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSmuHtEeiFj_ce9nNTCg" value="lundi_ferme1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSm-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSnOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSneHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSnuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSn-HtEeiFj_ce9nNTCg" name="lundi_ouvre2" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSoOHtEeiFj_ce9nNTCg" value="lundi_ouvre2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSoeHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSouHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSo-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSpOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSpeHtEeiFj_ce9nNTCg" name="lundi_ferme2" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSpuHtEeiFj_ce9nNTCg" value="lundi_ferme2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSp-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSqOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSqeHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSquHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSq-HtEeiFj_ce9nNTCg" name="mardi_ouvre1" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSrOHtEeiFj_ce9nNTCg" value="mardi_ouvre1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSreHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSruHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSr-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSsOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSseHtEeiFj_ce9nNTCg" name="mardi_ferme1" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSsuHtEeiFj_ce9nNTCg" value="mardi_ferme1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSs-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJStOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSteHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJStuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSt-HtEeiFj_ce9nNTCg" name="mardi_ouvre2" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSuOHtEeiFj_ce9nNTCg" value="mardi_ouvre2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSueHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSuuHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSu-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSvOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSveHtEeiFj_ce9nNTCg" name="mardi_ferme2" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSvuHtEeiFj_ce9nNTCg" value="mardi_ferme2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSv-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSwOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSweHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSwuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSw-HtEeiFj_ce9nNTCg" name="mercredi_ouvre1" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSxOHtEeiFj_ce9nNTCg" value="mercredi_ouvre1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSxeHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSxuHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSx-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSyOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSyeHtEeiFj_ce9nNTCg" name="mercredi_ferme1" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJSyuHtEeiFj_ce9nNTCg" value="mercredi_ferme1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJSy-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJSzOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJSzeHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJSzuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJSz-HtEeiFj_ce9nNTCg" name="mercredi_ouvre2" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJS0OHtEeiFj_ce9nNTCg" value="mercredi_ouvre2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJS0eHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJS0uHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJS0-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJS1OHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJS1eHtEeiFj_ce9nNTCg" name="mercredi_ferme2" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJS1uHtEeiFj_ce9nNTCg" value="mercredi_ferme2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJS1-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJS2OHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJS2eHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJS2uHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJS2-HtEeiFj_ce9nNTCg" name="jeudi_ouvre1" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJS3OHtEeiFj_ce9nNTCg" value="jeudi_ouvre1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJS3eHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJS3uHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJS3-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJS4OHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJS4eHtEeiFj_ce9nNTCg" name="jeudi_ferme1" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJS4uHtEeiFj_ce9nNTCg" value="jeudi_ferme1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJS4-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJS5OHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJS5eHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJS5uHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJS5-HtEeiFj_ce9nNTCg" name="jeudi_ouvre2" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJS6OHtEeiFj_ce9nNTCg" value="jeudi_ouvre2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJS6eHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJS6uHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJS6-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJS7OHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJS7eHtEeiFj_ce9nNTCg" name="jeudi_ferme2" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJS7uHtEeiFj_ce9nNTCg" value="jeudi_ferme2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJS7-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJS8OHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJS8eHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJS8uHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJS8-HtEeiFj_ce9nNTCg" name="vendredi_ouvre1" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJS9OHtEeiFj_ce9nNTCg" value="vendredi_ouvre1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJS9eHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJS9uHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJS9-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJS-OHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJS-eHtEeiFj_ce9nNTCg" name="vendredi_ferme1" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJS-uHtEeiFj_ce9nNTCg" value="vendredi_ferme1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJS--HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJS_OHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJS_eHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJS_uHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJS_-HtEeiFj_ce9nNTCg" name="vendredi_ouvre2" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTAOHtEeiFj_ce9nNTCg" value="vendredi_ouvre2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTAeHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTAuHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTA-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTBOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJTBeHtEeiFj_ce9nNTCg" name="vendredi_ferme2" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTBuHtEeiFj_ce9nNTCg" value="vendredi_ferme2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTB-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTCOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTCeHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTCuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJTC-HtEeiFj_ce9nNTCg" name="samedi_ouvre1" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTDOHtEeiFj_ce9nNTCg" value="samedi_ouvre1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTDeHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTDuHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTD-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTEOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJTEeHtEeiFj_ce9nNTCg" name="samedi_ferme1" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTEuHtEeiFj_ce9nNTCg" value="samedi_ferme1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTE-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTFOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTFeHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTFuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJTF-HtEeiFj_ce9nNTCg" name="samedi_ouvre2" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTGOHtEeiFj_ce9nNTCg" value="samedi_ouvre2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTGeHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTGuHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTG-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTHOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJTHeHtEeiFj_ce9nNTCg" name="samedi_ferme2" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTHuHtEeiFj_ce9nNTCg" value="samedi_ferme2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTH-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTIOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTIeHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTIuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJTI-HtEeiFj_ce9nNTCg" name="dimanche_ouvre1" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTJOHtEeiFj_ce9nNTCg" value="dimanche_ouvre1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTJeHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTJuHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTJ-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTKOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJTKeHtEeiFj_ce9nNTCg" name="dimanche_ferme1" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTKuHtEeiFj_ce9nNTCg" value="dimanche_ferme1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTK-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTLOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTLeHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTLuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJTL-HtEeiFj_ce9nNTCg" name="dimanche_ouvre2" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTMOHtEeiFj_ce9nNTCg" value="dimanche_ouvre2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTMeHtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTMuHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTM-HtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTNOHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_kOJTNeHtEeiFj_ce9nNTCg" name="dimanche_ferme2" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_kOJTNuHtEeiFj_ce9nNTCg" value="dimanche_ferme2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_kOJTN-HtEeiFj_ce9nNTCg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_kOJTOOHtEeiFj_ce9nNTCg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_kOJTOeHtEeiFj_ce9nNTCg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_kOJTOuHtEeiFj_ce9nNTCg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_JIEYKOcrEeicCszix7eWDA" name="Années existence" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_JIEYKecrEeicCszix7eWDA" value="Années existence"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_JIEYKucrEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_JIEYK-crEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_JIEYLOcrEeicCszix7eWDA" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_JIEYLecrEeicCszix7eWDA" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_r3WvwuctEeicCszix7eWDA" name="Chiffre Affaires" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_r3Wvw-ctEeicCszix7eWDA" value="Chiffre Affaires"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_r3WvxOctEeicCszix7eWDA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_r3WvxectEeicCszix7eWDA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_r3WvxuctEeicCszix7eWDA" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_r3Wvx-ctEeicCszix7eWDA" value="VARCHAR"/>
      </node>
    </node>
  </node>
</md:node>