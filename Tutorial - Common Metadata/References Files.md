<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.file.server" id="_7hkc4IqaEeiTZYPWrCo9ig" md:ref="platform:/plugin/com.indy.environment/.tech/file/standard.file.md#UUID_TECH_FILE_MD?fileId=UUID_TECH_FILE_MD$type=md$name=File?">
  <node defType="com.stambia.file.directory" id="_7mPhgIqaEeiTZYPWrCo9ig" name="Reference Files Folder">
    <attribute defType="com.stambia.file.directory.path" id="_7mjDgIqaEeiTZYPWrCo9ig" value="C:\stambia\stambiaRuntime\samples\files"/>
    <node defType="com.stambia.file.file" id="_7mkRoIqaEeiTZYPWrCo9ig" name="DiscountRanges">
      <attribute defType="com.stambia.file.file.type" id="_7m3MkIqaEeiTZYPWrCo9ig" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_7m3zoIqaEeiTZYPWrCo9ig"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_7m4asIqaEeiTZYPWrCo9ig" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_7m4asYqaEeiTZYPWrCo9ig" value="2C"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_7m5BwIqaEeiTZYPWrCo9ig"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_7m5BwYqaEeiTZYPWrCo9ig" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_7m5o0IqaEeiTZYPWrCo9ig" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_7m5o0YqaEeiTZYPWrCo9ig" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_7m6P4IqaEeiTZYPWrCo9ig" value="1"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_y1x8AIqbEeiTZYPWrCo9ig" value="DiscountRanges.txt"/>
      <node defType="com.stambia.file.field" id="_CHjngIqdEeiTZYPWrCo9ig" name="min" position="1">
        <attribute defType="com.stambia.file.field.size" id="_CHjngYqdEeiTZYPWrCo9ig" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_CHjngoqdEeiTZYPWrCo9ig" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_CHjng4qdEeiTZYPWrCo9ig" value="MIN"/>
      </node>
      <node defType="com.stambia.file.field" id="_CHjniIqdEeiTZYPWrCo9ig" name="range" position="3">
        <attribute defType="com.stambia.file.field.size" id="_CHjniYqdEeiTZYPWrCo9ig" value="62"/>
        <attribute defType="com.stambia.file.field.type" id="_CHjnioqdEeiTZYPWrCo9ig" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_CHkOkIqdEeiTZYPWrCo9ig" value="RANGE"/>
      </node>
      <node defType="com.stambia.file.field" id="_CHjnhIqdEeiTZYPWrCo9ig" name="max" position="2">
        <attribute defType="com.stambia.file.field.size" id="_CHjnhYqdEeiTZYPWrCo9ig" value="12"/>
        <attribute defType="com.stambia.file.field.type" id="_CHjnhoqdEeiTZYPWrCo9ig" value="Numeric"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_CHjnh4qdEeiTZYPWrCo9ig" value="MAX"/>
      </node>
    </node>
    <node defType="com.stambia.file.file" id="_QPwpkIqjEeiTZYPWrCo9ig" name="Time">
      <attribute defType="com.stambia.file.file.type" id="_QQfCUIqjEeiTZYPWrCo9ig" value="DELIMITED"/>
      <attribute defType="com.stambia.file.file.charsetName" id="_QQfpYIqjEeiTZYPWrCo9ig"/>
      <attribute defType="com.stambia.file.file.lineSeparator" id="_QQgQcIqjEeiTZYPWrCo9ig" value="0D0A"/>
      <attribute defType="com.stambia.file.file.fieldSeparator" id="_QQgQcYqjEeiTZYPWrCo9ig" value="2C"/>
      <attribute defType="com.stambia.file.file.stringDelimiter" id="_QQgQcoqjEeiTZYPWrCo9ig"/>
      <attribute defType="com.stambia.file.file.decimalSeparator" id="_QQg3gIqjEeiTZYPWrCo9ig" value="2E"/>
      <attribute defType="com.stambia.file.file.lineToSkip" id="_QQg3gYqjEeiTZYPWrCo9ig" value="0"/>
      <attribute defType="com.stambia.file.file.lastLineToSkip" id="_QQg3goqjEeiTZYPWrCo9ig" value="0"/>
      <attribute defType="com.stambia.file.file.header" id="_QQhekIqjEeiTZYPWrCo9ig" value="1"/>
      <attribute defType="com.stambia.file.file.physicalName" id="_hT6HEIqjEeiTZYPWrCo9ig" value="Time.csv"/>
      <node defType="com.stambia.file.field" id="_wUE6oIqjEeiTZYPWrCo9ig" name="DAY_DATE" position="1">
        <attribute defType="com.stambia.file.field.size" id="_wUE6oYqjEeiTZYPWrCo9ig" value="66"/>
        <attribute defType="com.stambia.file.field.type" id="_wUE6ooqjEeiTZYPWrCo9ig" value="String"/>
        <attribute defType="com.stambia.file.field.physicalName" id="_wUE6o4qjEeiTZYPWrCo9ig" value="F1"/>
      </node>
    </node>
  </node>
</md:node>