<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_kCLKEIslEei8pYut2Xo_Tg" name="Excel" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/excel/excel.rdbms.md#UUID_MD_RDBMS_MICROSOFT_EXCEL?fileId=UUID_MD_RDBMS_MICROSOFT_EXCEL$type=md$name=Microsoft%20Excel?">
  <attribute defType="com.stambia.rdbms.server.url" id="_LfzLAIsmEei8pYut2Xo_Tg" value="jdbc:stambia:excel://c:/app_dev/customers.xlsx?forceDatatypeAsString=true&amp;columnNameStyle=PRESERVE"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_Lf4DgIsmEei8pYut2Xo_Tg" value="com.stambia.jdbc.driver.excel.XLSXDriver"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_Lf7t4IsmEei8pYut2Xo_Tg" value="3951C0D79B227B95C1DC348DD0BCE8F1"/>
  <node defType="com.stambia.rdbms.schema" id="_kQzb8IslEei8pYut2Xo_Tg" name="customers">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_kRKBQIslEei8pYut2Xo_Tg" value="customers"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_kRLPYIslEei8pYut2Xo_Tg" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_kRMdgIslEei8pYut2Xo_Tg" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_kRNroIslEei8pYut2Xo_Tg" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_LfaJcIsmEei8pYut2Xo_Tg" name="Feuil1$">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_LfaJcYsmEei8pYut2Xo_Tg" value="Feuil1$"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_Lfb-oIsmEei8pYut2Xo_Tg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_LfkhgIsmEei8pYut2Xo_Tg" name="CUS_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_LfkhgYsmEei8pYut2Xo_Tg" value="CUS_ID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LfkhgosmEei8pYut2Xo_Tg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Lfkhg4smEei8pYut2Xo_Tg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_LfkhhIsmEei8pYut2Xo_Tg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LfkhhYsmEei8pYut2Xo_Tg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LfkhhosmEei8pYut2Xo_Tg" name="TIT_NAME" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_LflIkIsmEei8pYut2Xo_Tg" value="TIT_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LflIkYsmEei8pYut2Xo_Tg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LflIkosmEei8pYut2Xo_Tg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_LflIk4smEei8pYut2Xo_Tg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LflIlIsmEei8pYut2Xo_Tg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LflIlYsmEei8pYut2Xo_Tg" name="CUS_LAST_NAME" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_LflIlosmEei8pYut2Xo_Tg" value="CUS_LAST_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LflIl4smEei8pYut2Xo_Tg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LflImIsmEei8pYut2Xo_Tg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_LflImYsmEei8pYut2Xo_Tg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LflImosmEei8pYut2Xo_Tg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LflIm4smEei8pYut2Xo_Tg" name="CUS_FIRST_NAME" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_LflInIsmEei8pYut2Xo_Tg" value="CUS_FIRST_NAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LflInYsmEei8pYut2Xo_Tg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LflInosmEei8pYut2Xo_Tg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_LflIn4smEei8pYut2Xo_Tg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LflIoIsmEei8pYut2Xo_Tg" value="VARCHAR"/>
      </node>
    </node>
  </node>
</md:node>