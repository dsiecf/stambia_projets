<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_14k8gPelEeiL3uw9NeuELg" name="ECFDB" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/mssql/mssql.rdbms.md#UUID_MD_RDBMS_MSSQL?fileId=UUID_MD_RDBMS_MSSQL$type=md$name=Microsoft%20SQL%20Server?">
  <attribute defType="com.stambia.rdbms.server.url" id="_rvRUwPemEeiL3uw9NeuELg" value="jdbc:sqlserver://172.16.128.35"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_rvRUwfemEeiL3uw9NeuELg" value="com.microsoft.sqlserver.jdbc.SQLServerDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_rvRUwvemEeiL3uw9NeuELg" value="etude"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_rvRUw_emEeiL3uw9NeuELg" value="356C965F9E8F8893F3B9C9982987DD1F"/>
  <node defType="com.stambia.rdbms.schema" id="_1-iakPelEeiL3uw9NeuELg" name="ECFDBprod.dbo">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_1-_GgPelEeiL3uw9NeuELg" value="ECFDBprod"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_1-_GgfelEeiL3uw9NeuELg" value="dbo"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_1-_GgvelEeiL3uw9NeuELg" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_1-_Gg_elEeiL3uw9NeuELg" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_1-_GhPelEeiL3uw9NeuELg" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_rqNOlPemEeiL3uw9NeuELg" name="ref_Distinctions">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rqNOlfemEeiL3uw9NeuELg" value="ref_Distinctions"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rqNOlvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rqWYgPemEeiL3uw9NeuELg" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqWYgfemEeiL3uw9NeuELg" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqWYgvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqWYg_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqWYhPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqWYhfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqWYhvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqWYh_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqWYiPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqWYifemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqWYivemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqWYi_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqWYjPemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqWYjfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqWYjvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqWYj_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqWYkPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqWYkfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqWYkvemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rqWYk_emEeiL3uw9NeuELg" name="PK_ref_Distinctions">
        <node defType="com.stambia.rdbms.colref" id="_rqWYlPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rqWYlfemEeiL3uw9NeuELg" ref="#_rqWYgPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_reSSdPemEeiL3uw9NeuELg" name="ref_NiveauStanding">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_reSSdfemEeiL3uw9NeuELg" value="ref_NiveauStanding"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_reSSdvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_reSSd_emEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_reSSePemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_reSSefemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_reSSevemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_reSSe_emEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_reSSfPemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_reSSffemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_reSSfvemEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_reSSf_emEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_reSSgPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_reSSgfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_reSSgvemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_reSSg_emEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_recDcPemEeiL3uw9NeuELg" name="PK_NiveauStanding">
        <node defType="com.stambia.rdbms.colref" id="_recDcfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_recDcvemEeiL3uw9NeuELg" ref="#_reSSd_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rZhHNPemEeiL3uw9NeuELg" name="Client_HoraireOuv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rZhHNfemEeiL3uw9NeuELg" value="Client_HoraireOuv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rZhHNvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rZq4MPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZq4MfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZq4MvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZq4M_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZq4NPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZq4NfemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZq4NvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZq4N_emEeiL3uw9NeuELg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZq4OPemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZq4OfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZq4OvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZq4O_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZq4PPemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZq4PfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZq4PvemEeiL3uw9NeuELg" name="H_OuvertureLundi" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZq4P_emEeiL3uw9NeuELg" value="H_OuvertureLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZq4QPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZq4QfemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZq4QvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZq4Q_emEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZq4RPemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZq4RfemEeiL3uw9NeuELg" name="H_FermeLundi" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZq4RvemEeiL3uw9NeuELg" value="H_FermeLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZq4R_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZq4SPemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZq4SfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZq4SvemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZq4S_emEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZq4TPemEeiL3uw9NeuELg" name="H_OuvertureLundi2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZq4TfemEeiL3uw9NeuELg" value="H_OuvertureLundi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZq4TvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZq4T_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZq4UPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZq4UfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZq4UvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZq4U_emEeiL3uw9NeuELg" name="H_FermeLundi2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZq4VPemEeiL3uw9NeuELg" value="H_FermeLundi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZq4VfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZq4VvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZq4V_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZq4WPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZq4WfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZq4WvemEeiL3uw9NeuELg" name="H_OuvertureMardi" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZq4W_emEeiL3uw9NeuELg" value="H_OuvertureMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZq4XPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZq4XfemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZq4XvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZq4X_emEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZq4YPemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZq4YfemEeiL3uw9NeuELg" name="H_FermeMardi" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZq4YvemEeiL3uw9NeuELg" value="H_FermeMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZq4Y_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZq4ZPemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZq4ZfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZq4ZvemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZq4Z_emEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CIPemEeiL3uw9NeuELg" name="H_OuvertureMardi2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CIfemEeiL3uw9NeuELg" value="H_OuvertureMardi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CIvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0CI_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0CJPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0CJfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CJvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CJ_emEeiL3uw9NeuELg" name="H_FermeMardi2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CKPemEeiL3uw9NeuELg" value="H_FermeMardi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CKfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0CKvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0CK_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0CLPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CLfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CLvemEeiL3uw9NeuELg" name="H_OuvertureMercredi" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CL_emEeiL3uw9NeuELg" value="H_OuvertureMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CMPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0CMfemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0CMvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0CM_emEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CNPemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CNfemEeiL3uw9NeuELg" name="H_FermeMercredi" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CNvemEeiL3uw9NeuELg" value="H_FermeMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CN_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0COPemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0COfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0COvemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CO_emEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CPPemEeiL3uw9NeuELg" name="H_OuvertureMercredi2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CPfemEeiL3uw9NeuELg" value="H_OuvertureMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CPvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0CP_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0CQPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0CQfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CQvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CQ_emEeiL3uw9NeuELg" name="H_FermeMercredi2" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CRPemEeiL3uw9NeuELg" value="H_FermeMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CRfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0CRvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0CR_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0CSPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CSfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CSvemEeiL3uw9NeuELg" name="H_OuvertureJeudi" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CS_emEeiL3uw9NeuELg" value="H_OuvertureJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CTPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0CTfemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0CTvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0CT_emEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CUPemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CUfemEeiL3uw9NeuELg" name="H_FermeJeudi" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CUvemEeiL3uw9NeuELg" value="H_FermeJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CU_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0CVPemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0CVfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0CVvemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CV_emEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CWPemEeiL3uw9NeuELg" name="H_OuvertureJeudi2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CWfemEeiL3uw9NeuELg" value="H_OuvertureJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CWvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0CW_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0CXPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0CXfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CXvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ0CX_emEeiL3uw9NeuELg" name="H_FermeJeudi2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ0CYPemEeiL3uw9NeuELg" value="H_FermeJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ0CYfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ0CYvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ0CY_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ0CZPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ0CZfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zIPemEeiL3uw9NeuELg" name="H_OuvertureVendredi" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zIfemEeiL3uw9NeuELg" value="H_OuvertureVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zIvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zI_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zJPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zJfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zJvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zJ_emEeiL3uw9NeuELg" name="H_FermeVendredi" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zKPemEeiL3uw9NeuELg" value="H_FermeVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zKfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zKvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zK_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zLPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zLfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zLvemEeiL3uw9NeuELg" name="H_OuvertureVendredi2" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zL_emEeiL3uw9NeuELg" value="H_OuvertureVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zMPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zMfemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zMvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zM_emEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zNPemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zNfemEeiL3uw9NeuELg" name="H_FermeVendredi2" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zNvemEeiL3uw9NeuELg" value="H_FermeVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zN_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zOPemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zOfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zOvemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zO_emEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zPPemEeiL3uw9NeuELg" name="H_OuvertureSamedi" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zPfemEeiL3uw9NeuELg" value="H_OuvertureSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zPvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zP_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zQPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zQfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zQvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zQ_emEeiL3uw9NeuELg" name="H_FermeSamedi" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zRPemEeiL3uw9NeuELg" value="H_FermeSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zRfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zRvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zR_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zSPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zSfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zSvemEeiL3uw9NeuELg" name="H_OuvertureSamedi2" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zS_emEeiL3uw9NeuELg" value="H_OuvertureSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zTPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zTfemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zTvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zT_emEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zUPemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zUfemEeiL3uw9NeuELg" name="H_FermeSamedi2" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zUvemEeiL3uw9NeuELg" value="H_FermeSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zU_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zVPemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zVfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zVvemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zV_emEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zWPemEeiL3uw9NeuELg" name="H_OuvertureDimanche" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zWfemEeiL3uw9NeuELg" value="H_OuvertureDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zWvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zW_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zXPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zXfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zXvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZ9zX_emEeiL3uw9NeuELg" name="H_FermeDimanche" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZ9zYPemEeiL3uw9NeuELg" value="H_FermeDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZ9zYfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZ9zYvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZ9zY_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZ9zZPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZ9zZfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_raHkIPemEeiL3uw9NeuELg" name="H_OuvertureDimanche2" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_raHkIfemEeiL3uw9NeuELg" value="H_OuvertureDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raHkIvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_raHkI_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raHkJPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raHkJfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raHkJvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_raHkJ_emEeiL3uw9NeuELg" name="H_FermeDimanche2" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_raHkKPemEeiL3uw9NeuELg" value="H_FermeDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raHkKfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_raHkKvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raHkK_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raHkLPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raHkLfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_raHkLvemEeiL3uw9NeuELg" name="PK_Horaire_Ouverture">
        <node defType="com.stambia.rdbms.colref" id="_raHkL_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_raHkMPemEeiL3uw9NeuELg" ref="#_rZq4N_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr_XQPemEeiL3uw9NeuELg" name="FK_Client_HoraireOuv_Client">
        <node defType="com.stambia.rdbms.relation" id="_rr_XQfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr_XQvemEeiL3uw9NeuELg" ref="#_rZq4N_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr_XQ_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rjMnofemEeiL3uw9NeuELg" name="ref_TypeEtabNiv3">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rjMnovemEeiL3uw9NeuELg" value="ref_TypeEtabNiv3"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rjMno_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rjMnpPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjMnpfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjMnpvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rjMnp_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjMnqPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjMnqfemEeiL3uw9NeuELg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjMnqvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rjMnq_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjMnrPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjMnrfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjMnrvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjMnr_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjMnsPemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rjMnsfemEeiL3uw9NeuELg" name="IdTypeEtabN2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjMnsvemEeiL3uw9NeuELg" value="IdTypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjMns_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rjMntPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjMntfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjMntvemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjMnt_emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rjWYoPemEeiL3uw9NeuELg" name="PK_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.colref" id="_rjWYofemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rjWYovemEeiL3uw9NeuELg" ref="#_rjMnpPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rkF_hPemEeiL3uw9NeuELg" name="Client_SpeCulinaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rkF_hfemEeiL3uw9NeuELg" value="Client_SpeCulinaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rkF_hvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rkPwgPemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rkPwgfemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rkPwgvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rkPwg_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rkPwhPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rkPwhfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rkPwhvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rkPwh_emEeiL3uw9NeuELg" name="IdSpeCulinaire" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rkPwiPemEeiL3uw9NeuELg" value="IdSpeCulinaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rkPwifemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rkPwivemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rkPwi_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rkPwjPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rkPwjfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rkZhgPemEeiL3uw9NeuELg" name="PK_Client_SpeCulinaire">
        <node defType="com.stambia.rdbms.colref" id="_rkZhgfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rkZhgvemEeiL3uw9NeuELg" ref="#_rkPwgPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rkZhg_emEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rkZhhPemEeiL3uw9NeuELg" ref="#_rkPwh_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdSpeCulinaire?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtLqEPemEeiL3uw9NeuELg" name="FK_Client_SpeCulinaire_Client">
        <node defType="com.stambia.rdbms.relation" id="_rtLqEfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtLqEvemEeiL3uw9NeuELg" ref="#_rkPwgPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtLqE_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtLqFPemEeiL3uw9NeuELg" name="FK_Client_SpeCulinaire_ref_SpecialiteCulinaire1">
        <node defType="com.stambia.rdbms.relation" id="_rtLqFfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtLqFvemEeiL3uw9NeuELg" ref="#_rkPwh_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdSpeCulinaire?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtLqF_emEeiL3uw9NeuELg" ref="#_rjDdsPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rW-wmvemEeiL3uw9NeuELg" name="ref_NiveauFidelite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rW-wm_emEeiL3uw9NeuELg" value="ref_NiveauFidelite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rW-wnPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rXIhkPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXIhkfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXIhkvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rXIhk_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXIhlPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXIhlfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXIhlvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rXIhl_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXIhmPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXIhmfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXIhmvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXIhm_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXIhnPemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rXIhnfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXIhnvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXIhn_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXIhoPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXIhofemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXIhovemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rXIho_emEeiL3uw9NeuELg" name="PK_ref_NiveauFidelite">
        <node defType="com.stambia.rdbms.colref" id="_rXIhpPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rXIhpfemEeiL3uw9NeuELg" ref="#_rXIhkPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rimxwfemEeiL3uw9NeuELg" name="Client_Concurrent">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rimxwvemEeiL3uw9NeuELg" value="Client_Concurrent"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rimxw_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rimxxPemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rimxxfemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rimxxvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rimxx_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rimxyPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rimxyfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rimxyvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rimxy_emEeiL3uw9NeuELg" name="IdConcurrent" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rimxzPemEeiL3uw9NeuELg" value="IdConcurrent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rimxzfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rimxzvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rimxz_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rimx0PemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rimx0femEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_riwiwPemEeiL3uw9NeuELg" name="PK_Client_Concurrent">
        <node defType="com.stambia.rdbms.colref" id="_riwiwfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_riwiwvemEeiL3uw9NeuELg" ref="#_rimxxPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_riwiw_emEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_riwixPemEeiL3uw9NeuELg" ref="#_rimxy_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdConcurrent?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rs4vIPemEeiL3uw9NeuELg" name="FK_Client_Concurrent_Client">
        <node defType="com.stambia.rdbms.relation" id="_rs4vIfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rs4vIvemEeiL3uw9NeuELg" ref="#_rimxxPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rs4vI_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rs4vJPemEeiL3uw9NeuELg" name="FK_Client_Concurrent_ref_Concurrent1">
        <node defType="com.stambia.rdbms.relation" id="_rs4vJfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rs4vJvemEeiL3uw9NeuELg" ref="#_rimxy_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdConcurrent?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rs4vJ_emEeiL3uw9NeuELg" ref="#_rn9y4PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rYB5dPemEeiL3uw9NeuELg" name="ref_Mois">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rYB5dfemEeiL3uw9NeuELg" value="ref_Mois"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rYB5dvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rYB5d_emEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYB5ePemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYB5efemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYB5evemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYB5e_emEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYB5fPemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYB5ffemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYLDYPemEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYLDYfemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYLDYvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYLDY_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYLDZPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYLDZfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rYLDZvemEeiL3uw9NeuELg" name="PK_ref_Mois">
        <node defType="com.stambia.rdbms.colref" id="_rYLDZ_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rYLDaPemEeiL3uw9NeuELg" ref="#_rYB5d_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rhHkAfemEeiL3uw9NeuELg" name="ref_FormeJuridique">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rhHkAvemEeiL3uw9NeuELg" value="ref_FormeJuridique"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rhHkA_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rhHkBPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhHkBfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhHkBvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhHkB_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhHkCPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhHkCfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhHkCvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhHkC_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhHkDPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhHkDfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhHkDvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhHkD_emEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhHkEPemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhHkEfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhHkEvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhHkE_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhHkFPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhHkFfemEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhHkFvemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rhQt8PemEeiL3uw9NeuELg" name="PK_FormeJuridique">
        <node defType="com.stambia.rdbms.colref" id="_rhQt8femEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rhQt8vemEeiL3uw9NeuELg" ref="#_rhHkBPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rbAVB_emEeiL3uw9NeuELg" name="ref_NbLits">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rbAVCPemEeiL3uw9NeuELg" value="ref_NbLits"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rbAVCfemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rbKF8PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbKF8femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbKF8vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbKF8_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbKF9PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbKF9femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbKF9vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbKF9_emEeiL3uw9NeuELg" name="Nblits" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbKF-PemEeiL3uw9NeuELg" value="Nblits"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbKF-femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbKF-vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbKF-_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbKF_PemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbKF_femEeiL3uw9NeuELg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbKF_vemEeiL3uw9NeuELg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbKF__emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbKGAPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbKGAfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbKGAvemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbKGA_emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbKGBPemEeiL3uw9NeuELg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbKGBfemEeiL3uw9NeuELg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbKGBvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbKGB_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbKGCPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbKGCfemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbKGCvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rbKGC_emEeiL3uw9NeuELg" name="PK_ref_NbLits">
        <node defType="com.stambia.rdbms.colref" id="_rbKGDPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rbKGDfemEeiL3uw9NeuELg" ref="#_rbKF8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rokP0femEeiL3uw9NeuELg" name="ref_TypeComSouhaite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rokP0vemEeiL3uw9NeuELg" value="ref_TypeComSouhaite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rokP0_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rokP1PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rokP1femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rokP1vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rokP1_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rokP2PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rokP2femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rokP2vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rokP2_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rokP3PemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rokP3femEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rokP3vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rokP3_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rokP4PemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rokP4femEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rokP4vemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rokP4_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rokP5PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rokP5femEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rokP5vemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rotZwPemEeiL3uw9NeuELg" name="PK_ref_TypeComSouhaite">
        <node defType="com.stambia.rdbms.colref" id="_rotZwfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rotZwvemEeiL3uw9NeuELg" ref="#_rokP1PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rcfizfemEeiL3uw9NeuELg" name="ref_NbRepasJour">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rcfizvemEeiL3uw9NeuELg" value="ref_NbRepasJour"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rcfiz_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rcpTsPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcpTsfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcpTsvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rcpTs_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcpTtPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcpTtfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcpTtvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rcpTt_emEeiL3uw9NeuELg" name="NbRepasJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcpTuPemEeiL3uw9NeuELg" value="NbRepasJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcpTufemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcpTuvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcpTu_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcpTvPemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rcpTvfemEeiL3uw9NeuELg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcpTvvemEeiL3uw9NeuELg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcpTv_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rcpTwPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcpTwfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcpTwvemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcpTw_emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rcpTxPemEeiL3uw9NeuELg" name="nbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcpTxfemEeiL3uw9NeuELg" value="nbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcpTxvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rcpTx_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcpTyPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcpTyfemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcpTyvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rcpTy_emEeiL3uw9NeuELg" name="PK_ref_NbRepasJour">
        <node defType="com.stambia.rdbms.colref" id="_rcpTzPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rcpTzfemEeiL3uw9NeuELg" ref="#_rcpTsPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rUAVEfemEeiL3uw9NeuELg" name="ref_TypeMenu">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rUAVEvemEeiL3uw9NeuELg" value="ref_TypeMenu"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rUAVE_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rVDd8PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rVDd8femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rVDd8vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rVDd8_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rVDd9PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rVDd9femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rVDd9vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rVMn4PemEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rVMn4femEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rVMn4vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rVMn4_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rVMn5PemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rVMn5femEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rVMn5vemEeiL3uw9NeuELg" name="IdSpecCulinaire" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rVMn5_emEeiL3uw9NeuELg" value="IdSpecCulinaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rVMn6PemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rVMn6femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rVMn6vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rVMn6_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rVMn7PemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rVpT0PemEeiL3uw9NeuELg" name="PK_ref_TypeMenu">
        <node defType="com.stambia.rdbms.colref" id="_rVpT0femEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rVpT0vemEeiL3uw9NeuELg" ref="#_rVDd8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_re4IWvemEeiL3uw9NeuELg" name="ref_ActualiteProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_re4IW_emEeiL3uw9NeuELg" value="ref_ActualiteProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_re4IXPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rfB5UPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfB5UfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfB5UvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rfB5U_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfB5VPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfB5VfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfB5VvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rfB5V_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfB5WPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfB5WfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfB5WvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfB5W_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfB5XPemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rfB5XfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfB5XvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfB5X_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfB5YPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfB5YfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfB5YvemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rfB5Y_emEeiL3uw9NeuELg" name="PK_ref_ActualiteProjet">
        <node defType="com.stambia.rdbms.colref" id="_rfB5ZPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rfB5ZfemEeiL3uw9NeuELg" ref="#_rfB5UPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_riT21PemEeiL3uw9NeuELg" name="Infos_CHD">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_riT21femEeiL3uw9NeuELg" value="Infos_CHD"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_riT21vemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_ridAwPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_ridAwfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ridAwvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ridAw_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ridAxPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ridAxfemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ridAxvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ridAx_emEeiL3uw9NeuELg" name="ParamRequest" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ridAyPemEeiL3uw9NeuELg" value="ParamRequest"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ridAyfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ridAyvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ridAy_emEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ridAzPemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ridAzfemEeiL3uw9NeuELg" name="TextResponse" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_ridAzvemEeiL3uw9NeuELg" value="TextResponse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ridAz_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ridA0PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ridA0femEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ridA0vemEeiL3uw9NeuELg" value="max"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ridA0_emEeiL3uw9NeuELg" name="CreateDate" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_ridA1PemEeiL3uw9NeuELg" value="CreateDate"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ridA1femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ridA1vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ridA1_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ridA2PemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ridA2femEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_ridA2vemEeiL3uw9NeuELg" name="PK_Infos_CHD">
        <node defType="com.stambia.rdbms.colref" id="_ridA2_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_ridA3PemEeiL3uw9NeuELg" ref="#_ridAwPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rnq39PemEeiL3uw9NeuELg" name="ref_ComportementMultiCanal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rnq39femEeiL3uw9NeuELg" value="ref_ComportementMultiCanal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rnq39vemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rn0o8PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rn0o8femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rn0o8vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rn0o8_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rn0o9PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rn0o9femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rn0o9vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rn0o9_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rn0o-PemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rn0o-femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rn0o-vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rn0o-_emEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rn0o_PemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rn0o_femEeiL3uw9NeuELg" name="PK_ref_ComportementMultiCanal">
        <node defType="com.stambia.rdbms.colref" id="_rn0o_vemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rn0o__emEeiL3uw9NeuELg" ref="#_rn0o8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rZEbUfemEeiL3uw9NeuELg" name="ref_Pays">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rZEbUvemEeiL3uw9NeuELg" value="ref_Pays"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rZEbU_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rZOMQPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZOMQfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZOMQvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZOMQ_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZOMRPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZOMRfemEeiL3uw9NeuELg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZOMRvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZOMR_emEeiL3uw9NeuELg" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZOMSPemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZOMSfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZOMSvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZOMS_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZOMTPemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZOMTfemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZXWMPemEeiL3uw9NeuELg" name="Alpha2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZXWMfemEeiL3uw9NeuELg" value="Alpha2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZXWMvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZXWM_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZXWNPemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZXWNfemEeiL3uw9NeuELg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZXWNvemEeiL3uw9NeuELg" name="Alpha3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZXWN_emEeiL3uw9NeuELg" value="Alpha3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZXWOPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZXWOfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZXWOvemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZXWO_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZXWPPemEeiL3uw9NeuELg" name="Nom_en_gb" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZXWPfemEeiL3uw9NeuELg" value="Nom_en_gb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZXWPvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZXWP_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZXWQPemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZXWQfemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZXWQvemEeiL3uw9NeuELg" name="Nom_fr_fr" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZXWQ_emEeiL3uw9NeuELg" value="Nom_fr_fr"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZXWRPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZXWRfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZXWRvemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZXWR_emEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rZhHMPemEeiL3uw9NeuELg" name="PK_ref_Pays">
        <node defType="com.stambia.rdbms.colref" id="_rZhHMfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rZhHMvemEeiL3uw9NeuELg" ref="#_rZOMQPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rllNSvemEeiL3uw9NeuELg" name="ref_NbCouvertJour">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rllNS_emEeiL3uw9NeuELg" value="ref_NbCouvertJour"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rllNTPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rlu-QPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlu-QfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlu-QvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rlu-Q_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlu-RPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlu-RfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlu-RvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rlu-R_emEeiL3uw9NeuELg" name="NbCouvertsJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlu-SPemEeiL3uw9NeuELg" value="NbCouvertsJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlu-SfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlu-SvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlu-S_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlu-TPemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rlu-TfemEeiL3uw9NeuELg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlu-TvemEeiL3uw9NeuELg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlu-T_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rlu-UPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlu-UfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlu-UvemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlu-U_emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rlu-VPemEeiL3uw9NeuELg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlu-VfemEeiL3uw9NeuELg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlu-VvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rlu-V_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlu-WPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlu-WfemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlu-WvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rlu-W_emEeiL3uw9NeuELg" name="PK_ref_NbCouvertJour">
        <node defType="com.stambia.rdbms.colref" id="_rlu-XPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rlu-XfemEeiL3uw9NeuELg" ref="#_rlu-QPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_roHj4femEeiL3uw9NeuELg" name="Client_BI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_roHj4vemEeiL3uw9NeuELg" value="Client_BI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_roHj4_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_roHj5PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_roHj5femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roHj5vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_roHj5_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roHj6PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roHj6femEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roHj6vemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roHj6_emEeiL3uw9NeuELg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_roHj7PemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roHj7femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_roHj7vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roHj7_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roHj8PemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roHj8femEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roHj8vemEeiL3uw9NeuELg" name="NbLigneConsomme" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_roHj8_emEeiL3uw9NeuELg" value="NbLigneConsomme"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roHj9PemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_roHj9femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roHj9vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roHj9_emEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roHj-PemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roHj-femEeiL3uw9NeuELg" name="FrequenceMoyCmd" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_roHj-vemEeiL3uw9NeuELg" value="FrequenceMoyCmd"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roHj-_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roHj_PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roHj_femEeiL3uw9NeuELg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roHj_vemEeiL3uw9NeuELg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roHj__emEeiL3uw9NeuELg" name="ProfilSaisonnier" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_roHkAPemEeiL3uw9NeuELg" value="ProfilSaisonnier"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roHkAfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roHkAvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roHkA_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roHkBPemEeiL3uw9NeuELg" value="150"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roRU4PemEeiL3uw9NeuELg" name="PanierMoyen" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_roRU4femEeiL3uw9NeuELg" value="PanierMoyen"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roRU4vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roRU4_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roRU5PemEeiL3uw9NeuELg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roRU5femEeiL3uw9NeuELg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roRU5vemEeiL3uw9NeuELg" name="NbLignesMoy" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_roRU5_emEeiL3uw9NeuELg" value="NbLignesMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roRU6PemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roRU6femEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roRU6vemEeiL3uw9NeuELg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roRU6_emEeiL3uw9NeuELg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roRU7PemEeiL3uw9NeuELg" name="FreqVisiteWeb" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_roRU7femEeiL3uw9NeuELg" value="FreqVisiteWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roRU7vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roRU7_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roRU8PemEeiL3uw9NeuELg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roRU8femEeiL3uw9NeuELg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roRU8vemEeiL3uw9NeuELg" name="FreqVisiteCcial" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_roRU8_emEeiL3uw9NeuELg" value="FreqVisiteCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roRU9PemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roRU9femEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roRU9vemEeiL3uw9NeuELg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roRU9_emEeiL3uw9NeuELg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roRU-PemEeiL3uw9NeuELg" name="FreqAppel" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_roRU-femEeiL3uw9NeuELg" value="FreqAppel"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roRU-vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roRU-_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roRU_PemEeiL3uw9NeuELg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roRU_femEeiL3uw9NeuELg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roRU_vemEeiL3uw9NeuELg" name="FreqEmailing" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_roRU__emEeiL3uw9NeuELg" value="FreqEmailing"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roRVAPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roRVAfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roRVAvemEeiL3uw9NeuELg" value="float"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roRVA_emEeiL3uw9NeuELg" value="53"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roRVBPemEeiL3uw9NeuELg" name="SecteurVacant" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_roRVBfemEeiL3uw9NeuELg" value="SecteurVacant"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roRVBvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roRVB_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roRVCPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roRVCfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roRVCvemEeiL3uw9NeuELg" name="ClientPrete" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_roRVC_emEeiL3uw9NeuELg" value="ClientPrete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roRVDPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roRVDfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roRVDvemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roRVD_emEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roae0PemEeiL3uw9NeuELg" name="CcialConge" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_roae0femEeiL3uw9NeuELg" value="CcialConge"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roae0vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roae0_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roae1PemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roae1femEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roae1vemEeiL3uw9NeuELg" name="CcialEnCharge" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_roae1_emEeiL3uw9NeuELg" value="CcialEnCharge"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roae2PemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roae2femEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roae2vemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roae2_emEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_roae3PemEeiL3uw9NeuELg" name="CcialEnPret" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_roae3femEeiL3uw9NeuELg" value="CcialEnPret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_roae3vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_roae3_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_roae4PemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_roae4femEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_roae4vemEeiL3uw9NeuELg" name="PK_Client_BI">
        <node defType="com.stambia.rdbms.colref" id="_roae4_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_roae5PemEeiL3uw9NeuELg" ref="#_roHj5PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtoWAPemEeiL3uw9NeuELg" name="FK_Client_BI_Client">
        <node defType="com.stambia.rdbms.relation" id="_rtoWAfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtoWAvemEeiL3uw9NeuELg" ref="#_roHj6_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtoWA_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rYLDavemEeiL3uw9NeuELg" name="ref_Commune">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rYLDa_emEeiL3uw9NeuELg" value="ref_Commune"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rYLDbPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rYU0YPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYU0YfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYU0YvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYU0Y_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYU0ZPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYU0ZfemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYU0ZvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYU0Z_emEeiL3uw9NeuELg" name="IdDepartement" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYU0aPemEeiL3uw9NeuELg" value="IdDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYU0afemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYU0avemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYU0a_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYU0bPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYU0bfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYU0bvemEeiL3uw9NeuELg" name="NumINSEECommune" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYU0b_emEeiL3uw9NeuELg" value="NumINSEECommune"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYU0cPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYU0cfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYU0cvemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYU0c_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYU0dPemEeiL3uw9NeuELg" name="PrtculariteCom" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYU0dfemEeiL3uw9NeuELg" value="PrtculariteCom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYU0dvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYU0d_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYU0ePemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYU0efemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYU0evemEeiL3uw9NeuELg" name="ComOuLieudit" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYU0e_emEeiL3uw9NeuELg" value="ComOuLieudit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYU0fPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYU0ffemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYU0fvemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYU0f_emEeiL3uw9NeuELg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYU0gPemEeiL3uw9NeuELg" name="CodePostal" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYU0gfemEeiL3uw9NeuELg" value="CodePostal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYU0gvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYU0g_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYU0hPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYU0hfemEeiL3uw9NeuELg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYU0hvemEeiL3uw9NeuELg" name="PrtculariteBDist" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYU0h_emEeiL3uw9NeuELg" value="PrtculariteBDist"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYU0iPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYU0ifemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYU0ivemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYU0i_emEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYU0jPemEeiL3uw9NeuELg" name="LigneAcheminement" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYU0jfemEeiL3uw9NeuELg" value="LigneAcheminement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYU0jvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYU0j_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYU0kPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYU0kfemEeiL3uw9NeuELg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYU0kvemEeiL3uw9NeuELg" name="CodeComptaPTT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYU0k_emEeiL3uw9NeuELg" value="CodeComptaPTT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYU0lPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYU0lfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYU0lvemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYU0l_emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYelYPemEeiL3uw9NeuELg" name="Com" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYelYfemEeiL3uw9NeuELg" value="Com"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYelYvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYelY_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYelZPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYelZfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYelZvemEeiL3uw9NeuELg" name="INSEEComRatache" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYelZ_emEeiL3uw9NeuELg" value="INSEEComRatache"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYelaPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYelafemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYelavemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYela_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rYelbPemEeiL3uw9NeuELg" name="PK_ref_Commune">
        <node defType="com.stambia.rdbms.colref" id="_rYelbfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rYelbvemEeiL3uw9NeuELg" ref="#_rYU0YPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rkscdPemEeiL3uw9NeuELg" name="ref_NiveauUser">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rkscdfemEeiL3uw9NeuELg" value="ref_NiveauUser"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rkscdvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rk2NcPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rk2NcfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rk2NcvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rk2Nc_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rk2NdPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rk2NdfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rk2NdvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rk2Nd_emEeiL3uw9NeuELg" name="LibNiveau" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rk2NePemEeiL3uw9NeuELg" value="LibNiveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rk2NefemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rk2NevemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rk2Ne_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rk2NfPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rk2NffemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rk2NfvemEeiL3uw9NeuELg" name="PK_ref_NiveauUser">
        <node defType="com.stambia.rdbms.colref" id="_rk2Nf_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rk2NgPemEeiL3uw9NeuELg" ref="#_rk2NcPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rcMnxPemEeiL3uw9NeuELg" name="ref_Jours">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rcMnxfemEeiL3uw9NeuELg" value="ref_Jours"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rcMnxvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rcMnx_emEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcMnyPemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcMnyfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rcMnyvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcMny_emEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcMnzPemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcMnzfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rcWYwPemEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcWYwfemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcWYwvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcWYw_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcWYxPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcWYxfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rcWYxvemEeiL3uw9NeuELg" name="PK_ref_Jours">
        <node defType="com.stambia.rdbms.colref" id="_rcWYx_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rcWYyPemEeiL3uw9NeuELg" ref="#_rcMnx_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_riwixvemEeiL3uw9NeuELg" name="ref_ClientType">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_riwix_emEeiL3uw9NeuELg" value="ref_ClientType"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_riwiyPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_ri5ssPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_ri5ssfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ri5ssvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ri5ss_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ri5stPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ri5stfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ri5stvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ri5st_emEeiL3uw9NeuELg" name="LibTypeClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ri5suPemEeiL3uw9NeuELg" value="LibTypeClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ri5sufemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ri5suvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ri5su_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ri5svPemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_ri5svfemEeiL3uw9NeuELg" name="PK_ref_ClientType">
        <node defType="com.stambia.rdbms.colref" id="_ri5svvemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_ri5sv_emEeiL3uw9NeuELg" ref="#_ri5ssPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_raafEfemEeiL3uw9NeuELg" name="Client_Adresse">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_raafEvemEeiL3uw9NeuELg" value="Client_Adresse"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_raafE_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_raafFPemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_raafFfemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raafFvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_raafF_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raafGPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raafGfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raafGvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_raafG_emEeiL3uw9NeuELg" name="Id" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_raafHPemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raafHfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_raafHvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raafH_emEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raafIPemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raafIfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_raafIvemEeiL3uw9NeuELg" name="Rue" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_raafI_emEeiL3uw9NeuELg" value="Rue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raafJPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raafJfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raafJvemEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raafJ_emEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_raafKPemEeiL3uw9NeuELg" name="Complement" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_raafKfemEeiL3uw9NeuELg" value="Complement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raafKvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raafK_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raafLPemEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raafLfemEeiL3uw9NeuELg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_raafLvemEeiL3uw9NeuELg" name="CodePostale" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_raafL_emEeiL3uw9NeuELg" value="CodePostale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raafMPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raafMfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raafMvemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raafM_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_raafNPemEeiL3uw9NeuELg" name="BureauDistributeur" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_raafNfemEeiL3uw9NeuELg" value="BureauDistributeur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raafNvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raafN_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raafOPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raafOfemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rajpAPemEeiL3uw9NeuELg" name="CodeDepartement" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rajpAfemEeiL3uw9NeuELg" value="CodeDepartement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rajpAvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rajpA_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rajpBPemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rajpBfemEeiL3uw9NeuELg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rajpBvemEeiL3uw9NeuELg" name="CodeCommune" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rajpB_emEeiL3uw9NeuELg" value="CodeCommune"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rajpCPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rajpCfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rajpCvemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rajpC_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rajpDPemEeiL3uw9NeuELg" name="Ville" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rajpDfemEeiL3uw9NeuELg" value="Ville"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rajpDvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rajpD_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rajpEPemEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rajpEfemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rajpEvemEeiL3uw9NeuELg" name="IdPays" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rajpE_emEeiL3uw9NeuELg" value="IdPays"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rajpFPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rajpFfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rajpFvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rajpF_emEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rajpGPemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rajpGfemEeiL3uw9NeuELg" name="TypeAdresse" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rajpGvemEeiL3uw9NeuELg" value="TypeAdresse"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rajpG_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rajpHPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rajpHfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rajpHvemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rajpH_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rajpIPemEeiL3uw9NeuELg" name="CNT4" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_rajpIfemEeiL3uw9NeuELg" value="CNT4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rajpIvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rajpI_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rajpJPemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rajpJfemEeiL3uw9NeuELg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rajpJvemEeiL3uw9NeuELg" name="IRIS" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rajpJ_emEeiL3uw9NeuELg" value="IRIS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rajpKPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rajpKfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rajpKvemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rajpK_emEeiL3uw9NeuELg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rajpLPemEeiL3uw9NeuELg" name="TypeZone" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rajpLfemEeiL3uw9NeuELg" value="TypeZone"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rajpLvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rajpL_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rajpMPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rajpMfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rajpMvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rajpM_emEeiL3uw9NeuELg" name="EnvTouristique" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rajpNPemEeiL3uw9NeuELg" value="EnvTouristique"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rajpNfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rajpNvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rajpN_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rajpOPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rajpOfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rajpOvemEeiL3uw9NeuELg" name="PK_Adresse">
        <node defType="com.stambia.rdbms.colref" id="_rajpO_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rajpPPemEeiL3uw9NeuELg" ref="#_raafG_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr_XTPemEeiL3uw9NeuELg" name="FK_Client_Adresse_Client">
        <node defType="com.stambia.rdbms.relation" id="_rr_XTfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr_XTvemEeiL3uw9NeuELg" ref="#_raafFPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr_XT_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr_XUPemEeiL3uw9NeuELg" name="FK_Client_Adresse_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.relation" id="_rr_XUfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr_XUvemEeiL3uw9NeuELg" ref="#_rajpM_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=EnvTouristique?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr_XU_emEeiL3uw9NeuELg" ref="#_rataBPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr_XVPemEeiL3uw9NeuELg" name="FK_Adresse_ref_Pays">
        <node defType="com.stambia.rdbms.relation" id="_rr_XVfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr_XVvemEeiL3uw9NeuELg" ref="#_rajpEvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdPays?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr_XV_emEeiL3uw9NeuELg" ref="#_rZOMQPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr_XWPemEeiL3uw9NeuELg" name="FK_Adresse_ref_TypeAdresse">
        <node defType="com.stambia.rdbms.relation" id="_rr_XWfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr_XWvemEeiL3uw9NeuELg" ref="#_rajpGfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeAdresse?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr_XW_emEeiL3uw9NeuELg" ref="#_rb5s0PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr_XXPemEeiL3uw9NeuELg" name="FK_Client_Adresse_ref_TypeZone">
        <node defType="com.stambia.rdbms.relation" id="_rr_XXfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr_XXvemEeiL3uw9NeuELg" ref="#_rajpLPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeZone?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr_XX_emEeiL3uw9NeuELg" ref="#_rk_XZPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rn0pAfemEeiL3uw9NeuELg" name="ref_Concurrent">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rn0pAvemEeiL3uw9NeuELg" value="ref_Concurrent"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rn0pA_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rn9y4PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rn9y4femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rn9y4vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rn9y4_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rn9y5PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rn9y5femEeiL3uw9NeuELg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rn9y5vemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rn9y5_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rn9y6PemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rn9y6femEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rn9y6vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rn9y6_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rn9y7PemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rn9y7femEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rn9y7vemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rn9y7_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rn9y8PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rn9y8femEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rn9y8vemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rn9y8_emEeiL3uw9NeuELg" name="PK_ref_Concurrent">
        <node defType="com.stambia.rdbms.colref" id="_rn9y9PemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rn9y9femEeiL3uw9NeuELg" ref="#_rn9y4PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rX4IcfemEeiL3uw9NeuELg" name="ref_StatutCOLL">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rX4IcvemEeiL3uw9NeuELg" value="ref_StatutCOLL"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rX4Ic_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rX4IdPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rX4IdfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rX4IdvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rX4Id_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rX4IePemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rX4IefemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rX4IevemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rX4Ie_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rX4IfPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rX4IffemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rX4IfvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rX4If_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rX4IgPemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rX4IgfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rX4IgvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rX4Ig_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rX4IhPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rX4IhfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rX4IhvemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rYB5cPemEeiL3uw9NeuELg" name="PK_StatutCOLL">
        <node defType="com.stambia.rdbms.colref" id="_rYB5cfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rYB5cvemEeiL3uw9NeuELg" ref="#_rX4IdPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_reIhcfemEeiL3uw9NeuELg" name="ref_TypeClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_reIhcvemEeiL3uw9NeuELg" value="ref_TypeClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_reIhc_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_reIhdPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_reIhdfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_reIhdvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_reIhd_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_reIhePemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_reIhefemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_reIhevemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_reIhe_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_reIhfPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_reIhffemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_reIhfvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_reIhf_emEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_reIhgPemEeiL3uw9NeuELg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_reSScPemEeiL3uw9NeuELg" name="PK_ref_TypeClient">
        <node defType="com.stambia.rdbms.colref" id="_reSScfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_reSScvemEeiL3uw9NeuELg" ref="#_reIhdPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rfoWTfemEeiL3uw9NeuELg" name="Client_UniversConso">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rfoWTvemEeiL3uw9NeuELg" value="Client_UniversConso"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rfoWT_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rfxgMPemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfxgMfemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfxgMvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rfxgM_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfxgNPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfxgNfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfxgNvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rfxgN_emEeiL3uw9NeuELg" name="IdUniversProduit" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfxgOPemEeiL3uw9NeuELg" value="IdUniversProduit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfxgOfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rfxgOvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfxgO_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfxgPPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfxgPfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rfxgPvemEeiL3uw9NeuELg" name="PK_Client_UniversConso">
        <node defType="com.stambia.rdbms.colref" id="_rfxgP_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rfxgQPemEeiL3uw9NeuELg" ref="#_rfxgMPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rfxgQfemEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rfxgQvemEeiL3uw9NeuELg" ref="#_rfxgN_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdUniversProduit?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0N_emEeiL3uw9NeuELg" name="FK_Client_UniversConso_Client">
        <node defType="com.stambia.rdbms.relation" id="_rsl0OPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0OfemEeiL3uw9NeuELg" ref="#_rfxgMPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0OvemEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0O_emEeiL3uw9NeuELg" name="FK_Client_UniversConso_ref_UniversConso1">
        <node defType="com.stambia.rdbms.relation" id="_rsl0PPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0PfemEeiL3uw9NeuELg" ref="#_rfxgN_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdUniversProduit?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0PvemEeiL3uw9NeuELg" ref="#_rW1mp_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_raQuEPemEeiL3uw9NeuELg" name="Client_RepasServis">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_raQuEfemEeiL3uw9NeuELg" value="Client_RepasServis"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_raQuEvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_raQuE_emEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_raQuFPemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raQuFfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_raQuFvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raQuF_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raQuGPemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raQuGfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_raQuGvemEeiL3uw9NeuELg" name="IdRepas" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_raQuG_emEeiL3uw9NeuELg" value="IdRepas"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_raQuHPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_raQuHfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_raQuHvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_raQuH_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_raQuIPemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_raQuIfemEeiL3uw9NeuELg" name="PK_Client_RepasServis">
        <node defType="com.stambia.rdbms.colref" id="_raQuIvemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_raQuI_emEeiL3uw9NeuELg" ref="#_raQuE_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_raQuJPemEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_raQuJfemEeiL3uw9NeuELg" ref="#_raQuGvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdRepas?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr_XRPemEeiL3uw9NeuELg" name="FK_Client_RepasServis_Client">
        <node defType="com.stambia.rdbms.relation" id="_rr_XRfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr_XRvemEeiL3uw9NeuELg" ref="#_raQuE_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr_XR_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr_XSPemEeiL3uw9NeuELg" name="FK_Client_RepasServis_ref_RepasServis1">
        <node defType="com.stambia.rdbms.relation" id="_rr_XSfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr_XSvemEeiL3uw9NeuELg" ref="#_raQuGvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdRepas?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr_XS_emEeiL3uw9NeuELg" ref="#_riKF1PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rk_XYfemEeiL3uw9NeuELg" name="ref_TypeZone">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rk_XYvemEeiL3uw9NeuELg" value="ref_TypeZone"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rk_XY_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rk_XZPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rk_XZfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rk_XZvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rk_XZ_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rk_XaPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rk_XafemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rk_XavemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rk_Xa_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rk_XbPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rk_XbfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rk_XbvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rk_Xb_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rk_XcPemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rk_XcfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rk_XcvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rk_Xc_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rk_XdPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rk_XdfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rk_XdvemEeiL3uw9NeuELg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rk_Xd_emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rlJIYPemEeiL3uw9NeuELg" name="PK_TypeZone">
        <node defType="com.stambia.rdbms.colref" id="_rlJIYfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rlJIYvemEeiL3uw9NeuELg" ref="#_rk_XZPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rXSSkfemEeiL3uw9NeuELg" name="ref_Departement">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rXSSkvemEeiL3uw9NeuELg" value="ref_Departement"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rXSSk_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rXSSlPemEeiL3uw9NeuELg" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXSSlfemEeiL3uw9NeuELg" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXSSlvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rXSSl_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXSSmPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXSSmfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXSSmvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rXSSm_emEeiL3uw9NeuELg" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXSSnPemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXSSnfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXSSnvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXSSn_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXSSoPemEeiL3uw9NeuELg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rXSSofemEeiL3uw9NeuELg" name="Libelle" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXSSovemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXSSo_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXSSpPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXSSpfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXSSpvemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rXbcgPemEeiL3uw9NeuELg" name="PK_ref_Departement">
        <node defType="com.stambia.rdbms.colref" id="_rXbcgfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rXbcgvemEeiL3uw9NeuELg" ref="#_rXSSlPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rlJIZPemEeiL3uw9NeuELg" name="ref_AnneesExistence">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rlJIZfemEeiL3uw9NeuELg" value="ref_AnneesExistence"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rlJIZvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rlSSUPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlSSUfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlSSUvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rlSSU_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlSSVPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlSSVfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlSSVvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rlSSV_emEeiL3uw9NeuELg" name="libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlSSWPemEeiL3uw9NeuELg" value="libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlSSWfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlSSWvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlSSW_emEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlSSXPemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rlSSXfemEeiL3uw9NeuELg" name="AnneeMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlSSXvemEeiL3uw9NeuELg" value="AnneeMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlSSX_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rlSSYPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlSSYfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlSSYvemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlSSY_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rlSSZPemEeiL3uw9NeuELg" name="AnneeMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlSSZfemEeiL3uw9NeuELg" value="AnneeMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlSSZvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rlSSZ_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlSSaPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlSSafemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlSSavemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rlSSa_emEeiL3uw9NeuELg" name="PK_ref_AnneesExistence">
        <node defType="com.stambia.rdbms.colref" id="_rlSSbPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rlSSbfemEeiL3uw9NeuELg" ref="#_rlSSUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rcDd0femEeiL3uw9NeuELg" name="ref_Canal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rcDd0vemEeiL3uw9NeuELg" value="ref_Canal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rcDd0_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rcDd1PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcDd1femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcDd1vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rcDd1_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcDd2PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcDd2femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcDd2vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rcDd2_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcDd3PemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcDd3femEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcDd3vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcDd3_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcDd4PemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rcDd4femEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcDd4vemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcDd4_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcDd5PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcDd5femEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcDd5vemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rcMnwPemEeiL3uw9NeuELg" name="PK_ref_Canal">
        <node defType="com.stambia.rdbms.colref" id="_rcMnwfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rcMnwvemEeiL3uw9NeuELg" ref="#_rcDd1PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rpwipPemEeiL3uw9NeuELg" name="ref_PerimetreProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rpwipfemEeiL3uw9NeuELg" value="ref_PerimetreProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rpwipvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rp5skPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rp5skfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rp5skvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rp5sk_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rp5slPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rp5slfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rp5slvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rp5sl_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rp5smPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rp5smfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rp5smvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rp5sm_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rp5snPemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rp5snfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rp5snvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rp5sn_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rp5soPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rp5sofemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rp5sovemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rp5so_emEeiL3uw9NeuELg" name="PK_ref_PerimetreDernierProjet">
        <node defType="com.stambia.rdbms.colref" id="_rp5spPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rp5spfemEeiL3uw9NeuELg" ref="#_rp5skPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rWPJsfemEeiL3uw9NeuELg" name="Client_JourLiv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rWPJsvemEeiL3uw9NeuELg" value="Client_JourLiv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rWPJs_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rWPJtPemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWPJtfemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWPJtvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rWPJt_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWPJuPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWPJufemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWPJuvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWPJu_emEeiL3uw9NeuELg" name="IdJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWPJvPemEeiL3uw9NeuELg" value="IdJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWPJvfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rWPJvvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWPJv_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWPJwPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWPJwfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rWY6sPemEeiL3uw9NeuELg" name="PK_Client_JourLiv">
        <node defType="com.stambia.rdbms.colref" id="_rWY6sfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rWY6svemEeiL3uw9NeuELg" ref="#_rWPJtPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rWY6s_emEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rWY6tPemEeiL3uw9NeuELg" ref="#_rWPJu_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdJour?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rrirUPemEeiL3uw9NeuELg" name="FK_Client_JourLiv_Client">
        <node defType="com.stambia.rdbms.relation" id="_rrirUfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rrirUvemEeiL3uw9NeuELg" ref="#_rWPJtPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rrirU_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rrirVPemEeiL3uw9NeuELg" name="FK_Client_JourLiv_ref_Jours1">
        <node defType="com.stambia.rdbms.relation" id="_rrirVfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rrirVvemEeiL3uw9NeuELg" ref="#_rWPJu_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rrirV_emEeiL3uw9NeuELg" ref="#_rcMnx_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rh3K6_emEeiL3uw9NeuELg" name="ref_ScoringCA">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rh3K7PemEeiL3uw9NeuELg" value="ref_ScoringCA"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rh3K7femEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_riAU0PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_riAU0femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_riAU0vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_riAU0_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_riAU1PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_riAU1femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_riAU1vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_riAU1_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_riAU2PemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_riAU2femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_riAU2vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_riAU2_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_riAU3PemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_riAU3femEeiL3uw9NeuELg" name="ValMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_riAU3vemEeiL3uw9NeuELg" value="ValMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_riAU3_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_riAU4PemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_riAU4femEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_riAU4vemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_riAU4_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_riAU5PemEeiL3uw9NeuELg" name="ValMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_riAU5femEeiL3uw9NeuELg" value="ValMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_riAU5vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_riAU5_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_riAU6PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_riAU6femEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_riAU6vemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_riAU6_emEeiL3uw9NeuELg" name="PK_ref_ScoringCA">
        <node defType="com.stambia.rdbms.colref" id="_riAU7PemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_riAU7femEeiL3uw9NeuELg" ref="#_riAU0PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rpA7wfemEeiL3uw9NeuELg" name="ref_Genre">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rpA7wvemEeiL3uw9NeuELg" value="ref_Genre"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rpA7w_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rpA7xPemEeiL3uw9NeuELg" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpA7xfemEeiL3uw9NeuELg" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpA7xvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rpA7x_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpA7yPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpA7yfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpA7yvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpA7y_emEeiL3uw9NeuELg" name="Genre" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpA7zPemEeiL3uw9NeuELg" value="Genre"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpA7zfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpA7zvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpA7z_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpA70PemEeiL3uw9NeuELg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpKFsPemEeiL3uw9NeuELg" name="Civilite" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpKFsfemEeiL3uw9NeuELg" value="Civilite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpKFsvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpKFs_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpKFtPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpKFtfemEeiL3uw9NeuELg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpKFtvemEeiL3uw9NeuELg" name="Abreviation" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpKFt_emEeiL3uw9NeuELg" value="Abreviation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpKFuPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpKFufemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpKFuvemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpKFu_emEeiL3uw9NeuELg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rpKFvPemEeiL3uw9NeuELg" name="PK_ref_Genre">
        <node defType="com.stambia.rdbms.colref" id="_rpKFvfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rpKFvvemEeiL3uw9NeuELg" ref="#_rpA7xPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rmelJvemEeiL3uw9NeuELg" name="ref_Segment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rmelJ_emEeiL3uw9NeuELg" value="ref_Segment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rmelKPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rmoWIPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rmoWIfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rmoWIvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rmoWI_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rmoWJPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rmoWJfemEeiL3uw9NeuELg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rmoWJvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rmoWJ_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rmoWKPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rmoWKfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rmoWKvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rmoWK_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rmoWLPemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rmoWLfemEeiL3uw9NeuELg" name="PK_ref_Segment">
        <node defType="com.stambia.rdbms.colref" id="_rmoWLvemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rmoWL_emEeiL3uw9NeuELg" ref="#_rmoWIPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rbv76femEeiL3uw9NeuELg" name="ref_TypeAdresse">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rbv76vemEeiL3uw9NeuELg" value="ref_TypeAdresse"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rbv76_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rb5s0PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rb5s0femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rb5s0vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rb5s0_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rb5s1PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rb5s1femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rb5s1vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rb5s1_emEeiL3uw9NeuELg" name="Type" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rb5s2PemEeiL3uw9NeuELg" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rb5s2femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rb5s2vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rb5s2_emEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rb5s3PemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rb5s3femEeiL3uw9NeuELg" name="PK_ref_TypeAdresse">
        <node defType="com.stambia.rdbms.colref" id="_rb5s3vemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rb5s3_emEeiL3uw9NeuELg" ref="#_rb5s0PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rgq4EfemEeiL3uw9NeuELg" name="Client_UserWeb_News">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rgq4EvemEeiL3uw9NeuELg" value="Client_UserWeb_News"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rgq4E_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rgq4FPemEeiL3uw9NeuELg" name="IdUserWeb" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgq4FfemEeiL3uw9NeuELg" value="IdUserWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgq4FvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgq4F_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgq4GPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgq4GfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgq4GvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgq4G_emEeiL3uw9NeuELg" name="IdNews" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgq4HPemEeiL3uw9NeuELg" value="IdNews"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgq4HfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgq4HvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgq4H_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgq4IPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgq4IfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rg0pEPemEeiL3uw9NeuELg" name="PK_Client_Contact_News">
        <node defType="com.stambia.rdbms.colref" id="_rg0pEfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rg0pEvemEeiL3uw9NeuELg" ref="#_rgq4FPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdUserWeb?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rg0pE_emEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rg0pFPemEeiL3uw9NeuELg" ref="#_rgq4G_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdNews?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0e_emEeiL3uw9NeuELg" name="FK_Client_UserWeb_News_Client_UserWeb">
        <node defType="com.stambia.rdbms.relation" id="_rsl0fPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0ffemEeiL3uw9NeuELg" ref="#_rgq4FPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdUserWeb?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0fvemEeiL3uw9NeuELg" ref="#_rm7REPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0f_emEeiL3uw9NeuELg" name="FK_Client_UserWeb_News_ref_NewsletterType">
        <node defType="com.stambia.rdbms.relation" id="_rsl0gPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0gfemEeiL3uw9NeuELg" ref="#_rgq4G_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdNews?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0gvemEeiL3uw9NeuELg" ref="#_rmxgEPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rfLqUfemEeiL3uw9NeuELg" name="ref_NbChambres">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rfLqUvemEeiL3uw9NeuELg" value="ref_NbChambres"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rfLqU_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rfLqVPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfLqVfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfLqVvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rfLqV_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfLqWPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfLqWfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfLqWvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rfLqW_emEeiL3uw9NeuELg" name="NbChambres" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfLqXPemEeiL3uw9NeuELg" value="NbChambres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfLqXfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfLqXvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfLqX_emEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfLqYPemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rfU0QPemEeiL3uw9NeuELg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfU0QfemEeiL3uw9NeuELg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfU0QvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rfU0Q_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfU0RPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfU0RfemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfU0RvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rfU0R_emEeiL3uw9NeuELg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfU0SPemEeiL3uw9NeuELg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfU0SfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rfU0SvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfU0S_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfU0TPemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfU0TfemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rfU0TvemEeiL3uw9NeuELg" name="PK_ref_NbChambres">
        <node defType="com.stambia.rdbms.colref" id="_rfU0T_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rfU0UPemEeiL3uw9NeuELg" ref="#_rfLqVPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rfelQfemEeiL3uw9NeuELg" name="Client_JourOuvert">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rfelQvemEeiL3uw9NeuELg" value="Client_JourOuvert"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rfelQ_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rfelRPemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfelRfemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfelRvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rfelR_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfelSPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfelSfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfelSvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rfoWQPemEeiL3uw9NeuELg" name="IdJour" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rfoWQfemEeiL3uw9NeuELg" value="IdJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rfoWQvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rfoWQ_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rfoWRPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rfoWRfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rfoWRvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rfoWR_emEeiL3uw9NeuELg" name="PK_Client_JourOuvert">
        <node defType="com.stambia.rdbms.colref" id="_rfoWSPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rfoWSfemEeiL3uw9NeuELg" ref="#_rfelRPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rfoWSvemEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rfoWS_emEeiL3uw9NeuELg" ref="#_rfoWQPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdJour?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rscDMPemEeiL3uw9NeuELg" name="FK_Client_JourOuvert_Client">
        <node defType="com.stambia.rdbms.relation" id="_rsl0MPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0MfemEeiL3uw9NeuELg" ref="#_rfelRPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0MvemEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0M_emEeiL3uw9NeuELg" name="FK_Client_JourOuvert_ref_Jours1">
        <node defType="com.stambia.rdbms.relation" id="_rsl0NPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0NfemEeiL3uw9NeuELg" ref="#_rfoWQPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0NvemEeiL3uw9NeuELg" ref="#_rcMnx_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rW1mpPemEeiL3uw9NeuELg" name="ref_UniversConso">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rW1mpfemEeiL3uw9NeuELg" value="ref_UniversConso"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rW1mpvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rW1mp_emEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rW1mqPemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rW1mqfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rW1mqvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rW1mq_emEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rW1mrPemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rW1mrfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rW-wkPemEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rW-wkfemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rW-wkvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rW-wk_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rW-wlPemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rW-wlfemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rW-wlvemEeiL3uw9NeuELg" name="PK_ref_UniversConso">
        <node defType="com.stambia.rdbms.colref" id="_rW-wl_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rW-wmPemEeiL3uw9NeuELg" ref="#_rW1mp_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rmxgJ_emEeiL3uw9NeuELg" name="Client_UserWeb">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rmxgKPemEeiL3uw9NeuELg" value="Client_UserWeb"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rmxgKfemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rm7REPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rm7REfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rm7REvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rm7RE_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rm7RFPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rm7RFfemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rm7RFvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rm7RF_emEeiL3uw9NeuELg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rm7RGPemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rm7RGfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rm7RGvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rm7RG_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rm7RHPemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rm7RHfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rm7RHvemEeiL3uw9NeuELg" name="IdGenre" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rm7RH_emEeiL3uw9NeuELg" value="IdGenre"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rm7RIPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rm7RIfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rm7RIvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rm7RI_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rm7RJPemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rm7RJfemEeiL3uw9NeuELg" name="Nom" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rm7RJvemEeiL3uw9NeuELg" value="Nom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rm7RJ_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rm7RKPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rm7RKfemEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rm7RKvemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rm7RK_emEeiL3uw9NeuELg" name="Prenom" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rm7RLPemEeiL3uw9NeuELg" value="Prenom"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rm7RLfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rm7RLvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rm7RL_emEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rm7RMPemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rm7RMfemEeiL3uw9NeuELg" name="IntitulePoste" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rm7RMvemEeiL3uw9NeuELg" value="IntitulePoste"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rm7RM_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rm7RNPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rm7RNfemEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rm7RNvemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rm7RN_emEeiL3uw9NeuELg" name="Mail" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rm7ROPemEeiL3uw9NeuELg" value="Mail"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rm7ROfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rm7ROvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rm7RO_emEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rm7RPPemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rm7RPfemEeiL3uw9NeuELg" name="IdWeb" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rm7RPvemEeiL3uw9NeuELg" value="IdWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rm7RP_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rm7RQPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rm7RQfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rm7RQvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnFCEPemEeiL3uw9NeuELg" name="IdTypeComSouhait" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnFCEfemEeiL3uw9NeuELg" value="IdTypeComSouhait"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnFCEvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rnFCE_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnFCFPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnFCFfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnFCFvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnFCF_emEeiL3uw9NeuELg" name="OptinEmail" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnFCGPemEeiL3uw9NeuELg" value="OptinEmail"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnFCGfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnFCGvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnFCG_emEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnFCHPemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnFCHfemEeiL3uw9NeuELg" name="DateCrt" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnFCHvemEeiL3uw9NeuELg" value="DateCrt"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnFCH_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rnFCIPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnFCIfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnFCIvemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnFCI_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnFCJPemEeiL3uw9NeuELg" name="DateSup" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnFCJfemEeiL3uw9NeuELg" value="DateSup"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnFCJvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rnFCJ_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnFCKPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnFCKfemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnFCKvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnFCK_emEeiL3uw9NeuELg" name="TopSup" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnFCLPemEeiL3uw9NeuELg" value="TopSup"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnFCLfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnFCLvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnFCL_emEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnFCMPemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnFCMfemEeiL3uw9NeuELg" name="IdNiveau" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnFCMvemEeiL3uw9NeuELg" value="IdNiveau"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnFCM_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rnFCNPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnFCNfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnFCNvemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnFCN_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnFCOPemEeiL3uw9NeuELg" name="IdUserMagento" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnFCOfemEeiL3uw9NeuELg" value="IdUserMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnFCOvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnFCO_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnFCPPemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnFCPfemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnFCPvemEeiL3uw9NeuELg" name="OptinSMS" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnFCP_emEeiL3uw9NeuELg" value="OptinSMS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnFCQPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnFCQfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnFCQvemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnFCQ_emEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rnOMAPemEeiL3uw9NeuELg" name="PK_Contact">
        <node defType="com.stambia.rdbms.colref" id="_rnOMAfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rnOMAvemEeiL3uw9NeuELg" ref="#_rm7REPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtelAPemEeiL3uw9NeuELg" name="FK_Client_UserWeb_Client">
        <node defType="com.stambia.rdbms.relation" id="_rtelAfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtelAvemEeiL3uw9NeuELg" ref="#_rm7RF_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtelA_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtelBPemEeiL3uw9NeuELg" name="FK_Client_UserWeb_ref_Genre">
        <node defType="com.stambia.rdbms.relation" id="_rtelBfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtelBvemEeiL3uw9NeuELg" ref="#_rm7RHvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdGenre?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtelB_emEeiL3uw9NeuELg" ref="#_rpA7xPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtelCPemEeiL3uw9NeuELg" name="FK_Client_UserWeb_ref_NiveauUser">
        <node defType="com.stambia.rdbms.relation" id="_rtelCfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtelCvemEeiL3uw9NeuELg" ref="#_rnFCMfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdNiveau?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtelC_emEeiL3uw9NeuELg" ref="#_rk2NcPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtelDPemEeiL3uw9NeuELg" name="FK_Client_UserWeb_ref_TypeComSouhaite">
        <node defType="com.stambia.rdbms.relation" id="_rtelDfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtelDvemEeiL3uw9NeuELg" ref="#_rnFCEPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdTypeComSouhait?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtelD_emEeiL3uw9NeuELg" ref="#_rokP1PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_recDdPemEeiL3uw9NeuELg" name="ref_PrestationsOffertes">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_recDdfemEeiL3uw9NeuELg" value="ref_PrestationsOffertes"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_recDdvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_relNYPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_relNYfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_relNYvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_relNY_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_relNZPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_relNZfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_relNZvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_relNZ_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_relNaPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_relNafemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_relNavemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_relNa_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_relNbPemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_relNbfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_relNbvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_relNb_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_relNcPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_relNcfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_relNcvemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_relNc_emEeiL3uw9NeuELg" name="PK_ref_PrestationsOffertes">
        <node defType="com.stambia.rdbms.colref" id="_relNdPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_relNdfemEeiL3uw9NeuELg" ref="#_relNYPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rjWYpPemEeiL3uw9NeuELg" name="Client_PerimetreProchainProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rjWYpfemEeiL3uw9NeuELg" value="Client_PerimetreProchainProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rjWYpvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rjgJoPemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjgJofemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjgJovemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rjgJo_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjgJpPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjgJpfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjgJpvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rjgJp_emEeiL3uw9NeuELg" name="IdUnivers" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjgJqPemEeiL3uw9NeuELg" value="IdUnivers"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjgJqfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rjgJqvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjgJq_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjgJrPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjgJrfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rjp6oPemEeiL3uw9NeuELg" name="PK_Client_PerimetreProchainProjet">
        <node defType="com.stambia.rdbms.colref" id="_rjp6ofemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rjp6ovemEeiL3uw9NeuELg" ref="#_rjgJoPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rjp6o_emEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rjp6pPemEeiL3uw9NeuELg" ref="#_rjgJp_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdUnivers?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtB5EPemEeiL3uw9NeuELg" name="FK_Client_PerimetreProchainProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_rtB5EfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtB5EvemEeiL3uw9NeuELg" ref="#_rjgJoPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtB5E_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtB5FPemEeiL3uw9NeuELg" name="FK_Client_PerimetreProchainProjet_ref_UniversConso">
        <node defType="com.stambia.rdbms.relation" id="_rtB5FfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtB5FvemEeiL3uw9NeuELg" ref="#_rjgJp_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdUnivers?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtB5F_emEeiL3uw9NeuELg" ref="#_rW1mp_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rj81kfemEeiL3uw9NeuELg" name="ref_TypeEtabNiv1">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rj81kvemEeiL3uw9NeuELg" value="ref_TypeEtabNiv1"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rj81k_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rj81lPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rj81lfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rj81lvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rj81l_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rj81mPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rj81mfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rj81mvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rj81m_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rj81nPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rj81nfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rj81nvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rj81n_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rj81oPemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rkF_gPemEeiL3uw9NeuELg" name="PK_TypeEtablissement">
        <node defType="com.stambia.rdbms.colref" id="_rkF_gfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rkF_gvemEeiL3uw9NeuELg" ref="#_rj81lPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rg0pFvemEeiL3uw9NeuELg" name="Client_ActualiteProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rg0pF_emEeiL3uw9NeuELg" value="Client_ActualiteProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rg0pGPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rg9zAPemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rg9zAfemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rg9zAvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rg9zA_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rg9zBPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rg9zBfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rg9zBvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rg9zB_emEeiL3uw9NeuELg" name="IdProjet" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rg9zCPemEeiL3uw9NeuELg" value="IdProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rg9zCfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rg9zCvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rg9zC_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rg9zDPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rg9zDfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rg9zDvemEeiL3uw9NeuELg" name="PK_Client_ActualiteProjet">
        <node defType="com.stambia.rdbms.colref" id="_rg9zD_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rg9zEPemEeiL3uw9NeuELg" ref="#_rg9zAPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rg9zEfemEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rg9zEvemEeiL3uw9NeuELg" ref="#_rg9zB_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdProjet?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0g_emEeiL3uw9NeuELg" name="FK_Client_ActualiteProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_rsl0hPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0hfemEeiL3uw9NeuELg" ref="#_rg9zAPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0hvemEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0h_emEeiL3uw9NeuELg" name="FK_Client_ActualiteProjet_ref_ActualiteProjet1">
        <node defType="com.stambia.rdbms.relation" id="_rsl0iPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0ifemEeiL3uw9NeuELg" ref="#_rg9zB_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdProjet?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0ivemEeiL3uw9NeuELg" ref="#_rfB5UPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rotZxPemEeiL3uw9NeuELg" name="ref_TypeOperation">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rotZxfemEeiL3uw9NeuELg" value="ref_TypeOperation"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rotZxvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_ro3KwPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_ro3KwfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ro3KwvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ro3Kw_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ro3KxPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ro3KxfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ro3KxvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ro3Kx_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ro3KyPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ro3KyfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ro3KyvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ro3Ky_emEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ro3KzPemEeiL3uw9NeuELg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_ro3KzfemEeiL3uw9NeuELg" name="PK_ref_TypeOperation">
        <node defType="com.stambia.rdbms.colref" id="_ro3KzvemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_ro3Kz_emEeiL3uw9NeuELg" ref="#_ro3KwPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rjp6pvemEeiL3uw9NeuELg" name="ref_TypeEtabNiv2">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rjp6p_emEeiL3uw9NeuELg" value="ref_TypeEtabNiv2"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rjp6qPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rjp6qfemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjp6qvemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjp6q_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rjp6rPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjp6rfemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjp6rvemEeiL3uw9NeuELg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjp6r_emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rjzEkPemEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjzEkfemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjzEkvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjzEk_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjzElPemEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjzElfemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rjzElvemEeiL3uw9NeuELg" name="IdTypeEtabN1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjzEl_emEeiL3uw9NeuELg" value="IdTypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjzEmPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rjzEmfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjzEmvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjzEm_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjzEnPemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rjzEnfemEeiL3uw9NeuELg" name="PK_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.colref" id="_rjzEnvemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rjzEn_emEeiL3uw9NeuELg" ref="#_rjp6qfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtB5GPemEeiL3uw9NeuELg" name="FK_ref_TypeEtabNiv2_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_rtB5GfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtB5GvemEeiL3uw9NeuELg" ref="#_rjzElvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdTypeEtabN1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtB5G_emEeiL3uw9NeuELg" ref="#_rj81lPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rmB5NPemEeiL3uw9NeuELg" name="Client_Distinction">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rmB5NfemEeiL3uw9NeuELg" value="Client_Distinction"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rmB5NvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rmLqMPemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rmLqMfemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rmLqMvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rmLqM_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rmLqNPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rmLqNfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rmLqNvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rmLqN_emEeiL3uw9NeuELg" name="IdDistinction" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rmLqOPemEeiL3uw9NeuELg" value="IdDistinction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rmLqOfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rmLqOvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rmLqO_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rmLqPPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rmLqPfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rmU0IPemEeiL3uw9NeuELg" name="PK_Client_Distinction">
        <node defType="com.stambia.rdbms.colref" id="_rmU0IfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rmU0IvemEeiL3uw9NeuELg" ref="#_rmLqMPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rmU0I_emEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rmU0JPemEeiL3uw9NeuELg" ref="#_rmLqN_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdDistinction?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtVbEPemEeiL3uw9NeuELg" name="FK_Client_Distinction_Client">
        <node defType="com.stambia.rdbms.relation" id="_rtVbEfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtVbEvemEeiL3uw9NeuELg" ref="#_rmLqMPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtVbE_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtVbFPemEeiL3uw9NeuELg" name="FK_Client_Distinction_ref_Distinctions1">
        <node defType="com.stambia.rdbms.relation" id="_rtVbFfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtVbFvemEeiL3uw9NeuELg" ref="#_rmLqN_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdDistinction?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtVbF_emEeiL3uw9NeuELg" ref="#_rqWYgPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_ri5swfemEeiL3uw9NeuELg" name="ref_SpecialiteCulinaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_ri5swvemEeiL3uw9NeuELg" value="ref_SpecialiteCulinaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_ri5sw_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rjDdsPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjDdsfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjDdsvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rjDds_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjDdtPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjDdtfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjDdtvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rjDdt_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rjDduPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rjDdufemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rjDduvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rjDdu_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rjDdvPemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rjDdvfemEeiL3uw9NeuELg" name="PK_ref_SpecialiteCulinaire">
        <node defType="com.stambia.rdbms.colref" id="_rjDdvvemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rjDdv_emEeiL3uw9NeuELg" ref="#_rjDdsPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rc8OofemEeiL3uw9NeuELg" name="Client_Organisation">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rc8OovemEeiL3uw9NeuELg" value="Client_Organisation"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rc8Oo_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rc8OpPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rc8OpfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rc8OpvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rc8Op_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rc8OqPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rc8OqfemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rc8OqvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rc8Oq_emEeiL3uw9NeuELg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rc8OrPemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rc8OrfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rc8OrvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rc8Or_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rc8OsPemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rc8OsfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdF_oPemEeiL3uw9NeuELg" name="Secteur" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdF_ofemEeiL3uw9NeuELg" value="Secteur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdF_ovemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdF_o_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdF_pPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdF_pfemEeiL3uw9NeuELg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdF_pvemEeiL3uw9NeuELg" name="Dic" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdF_p_emEeiL3uw9NeuELg" value="Dic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdF_qPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdF_qfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdF_qvemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdF_q_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdF_rPemEeiL3uw9NeuELg" name="Dec" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdF_rfemEeiL3uw9NeuELg" value="Dec"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdF_rvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdF_r_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdF_sPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdF_sfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdF_svemEeiL3uw9NeuELg" name="MagasinFavori" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdF_s_emEeiL3uw9NeuELg" value="MagasinFavori"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdF_tPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdF_tfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdF_tvemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdF_t_emEeiL3uw9NeuELg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdF_uPemEeiL3uw9NeuELg" name="CentraleAchat" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdF_ufemEeiL3uw9NeuELg" value="CentraleAchat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdF_uvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rdF_u_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdF_vPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdF_vfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdF_vvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdF_v_emEeiL3uw9NeuELg" name="AppelOffres" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdF_wPemEeiL3uw9NeuELg" value="AppelOffres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdF_wfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdF_wvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdF_w_emEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdF_xPemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdF_xfemEeiL3uw9NeuELg" name="MarchePublic" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdF_xvemEeiL3uw9NeuELg" value="MarchePublic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdF_x_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdF_yPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdF_yfemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdF_yvemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdPwoPemEeiL3uw9NeuELg" name="DateFinContrat" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdPwofemEeiL3uw9NeuELg" value="DateFinContrat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdPwovemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rdPwo_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdPwpPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdPwpfemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdPwpvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdPwp_emEeiL3uw9NeuELg" name="TaciteReconduction" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdPwqPemEeiL3uw9NeuELg" value="TaciteReconduction"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdPwqfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdPwqvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdPwq_emEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdPwrPemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdPwrfemEeiL3uw9NeuELg" name="MercuFerme" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdPwrvemEeiL3uw9NeuELg" value="MercuFerme"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdPwr_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdPwsPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdPwsfemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdPwsvemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdPws_emEeiL3uw9NeuELg" name="Mercuriale" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdPwtPemEeiL3uw9NeuELg" value="Mercuriale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdPwtfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdPwtvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdPwt_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdPwuPemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdPwufemEeiL3uw9NeuELg" name="FctParBudget" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdPwuvemEeiL3uw9NeuELg" value="FctParBudget"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdPwu_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdPwvPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdPwvfemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdPwvvemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdPwv_emEeiL3uw9NeuELg" name="VisiteCcial" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdPwwPemEeiL3uw9NeuELg" value="VisiteCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdPwwfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdPwwvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdPww_emEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdPwxPemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdPwxfemEeiL3uw9NeuELg" name="AppelOutcall" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdPwxvemEeiL3uw9NeuELg" value="AppelOutcall"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdPwx_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdPwyPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdPwyfemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdPwyvemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdPwy_emEeiL3uw9NeuELg" name="PassageMag" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdPwzPemEeiL3uw9NeuELg" value="PassageMag"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdPwzfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdPwzvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdPwz_emEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdPw0PemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdY6kPemEeiL3uw9NeuELg" name="PrefOutcall" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdY6kfemEeiL3uw9NeuELg" value="PrefOutcall"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdY6kvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdY6k_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdY6lPemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdY6lfemEeiL3uw9NeuELg" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdY6lvemEeiL3uw9NeuELg" name="PrefVisites" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdY6l_emEeiL3uw9NeuELg" value="PrefVisites"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdY6mPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdY6mfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdY6mvemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdY6m_emEeiL3uw9NeuELg" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdY6nPemEeiL3uw9NeuELg" name="AdherentCtraleAch" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdY6nfemEeiL3uw9NeuELg" value="AdherentCtraleAch"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdY6nvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdY6n_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdY6oPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdY6ofemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdY6ovemEeiL3uw9NeuELg" name="NumAdherent" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdY6o_emEeiL3uw9NeuELg" value="NumAdherent"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdY6pPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdY6pfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdY6pvemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdY6p_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdY6qPemEeiL3uw9NeuELg" name="HoraireLivSouhait" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdY6qfemEeiL3uw9NeuELg" value="HoraireLivSouhait"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdY6qvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdY6q_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdY6rPemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdY6rfemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdY6rvemEeiL3uw9NeuELg" name="NumCtraleAch" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdY6r_emEeiL3uw9NeuELg" value="NumCtraleAch"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdY6sPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdY6sfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdY6svemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdY6s_emEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdY6tPemEeiL3uw9NeuELg" name="TypeOp" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdY6tfemEeiL3uw9NeuELg" value="TypeOp"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdY6tvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rdY6t_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdY6uPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdY6ufemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdY6uvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdirkPemEeiL3uw9NeuELg" name="MercuContrat" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdirkfemEeiL3uw9NeuELg" value="MercuContrat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdirkvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdirk_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdirlPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdirlfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdirlvemEeiL3uw9NeuELg" name="RespMercu" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdirl_emEeiL3uw9NeuELg" value="RespMercu"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdirmPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdirmfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdirmvemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdirm_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdirnPemEeiL3uw9NeuELg" name="FacturesDemat" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdirnfemEeiL3uw9NeuELg" value="FacturesDemat"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdirnvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdirn_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdiroPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdirofemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdirovemEeiL3uw9NeuELg" name="MailFacture" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdiro_emEeiL3uw9NeuELg" value="MailFacture"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdirpPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdirpfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdirpvemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdirp_emEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdirqPemEeiL3uw9NeuELg" name="multicompte" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdirqfemEeiL3uw9NeuELg" value="multicompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdirqvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdirq_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdirrPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdirrfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdirrvemEeiL3uw9NeuELg" name="Chaine" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdirr_emEeiL3uw9NeuELg" value="Chaine"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdirsPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdirsfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdirsvemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdirs_emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdirtPemEeiL3uw9NeuELg" name="Centralisateur" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdirtfemEeiL3uw9NeuELg" value="Centralisateur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdirtvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdirt_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdiruPemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdirufemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rdr1gPemEeiL3uw9NeuELg" name="HabitudeCmd" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_rdr1gfemEeiL3uw9NeuELg" value="HabitudeCmd"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rdr1gvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rdr1g_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rdr1hPemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rdr1hfemEeiL3uw9NeuELg" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rdr1hvemEeiL3uw9NeuELg" name="PK_Client_Organisation">
        <node defType="com.stambia.rdbms.colref" id="_rdr1h_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rdr1iPemEeiL3uw9NeuELg" ref="#_rc8OpPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsSSMPemEeiL3uw9NeuELg" name="FK_Client_Organisation_Client">
        <node defType="com.stambia.rdbms.relation" id="_rsSSMfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsSSMvemEeiL3uw9NeuELg" ref="#_rc8Oq_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsSSM_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsSSNPemEeiL3uw9NeuELg" name="FK_Client_Organisation_ref_TypeOperation">
        <node defType="com.stambia.rdbms.relation" id="_rsSSNfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsSSNvemEeiL3uw9NeuELg" ref="#_rdY6tPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeOp?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsSSN_emEeiL3uw9NeuELg" ref="#_ro3KwPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rYelcPemEeiL3uw9NeuELg" name="Client">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rYelcfemEeiL3uw9NeuELg" value="Client"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rYelcvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rYnvUPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvUfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvUvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYnvU_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvVPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvVfemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvVvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvV_emEeiL3uw9NeuELg" name="Code" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvWPemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvWfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYnvWvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvW_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvXPemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvXfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvXvemEeiL3uw9NeuELg" name="IdSociete" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvX_emEeiL3uw9NeuELg" value="IdSociete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvYPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYnvYfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvYvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvY_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvZPemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvZfemEeiL3uw9NeuELg" name="CodeSocMagento" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvZvemEeiL3uw9NeuELg" value="CodeSocMagento"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvZ_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvaPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvafemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvavemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnva_emEeiL3uw9NeuELg" name="RaisonSociale" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvbPemEeiL3uw9NeuELg" value="RaisonSociale"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvbfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvbvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvb_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvcPemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvcfemEeiL3uw9NeuELg" name="Enseigne" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvcvemEeiL3uw9NeuELg" value="Enseigne"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvc_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvdPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvdfemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvdvemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvd_emEeiL3uw9NeuELg" name="Siren" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvePemEeiL3uw9NeuELg" value="Siren"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvefemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvevemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnve_emEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvfPemEeiL3uw9NeuELg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvffemEeiL3uw9NeuELg" name="Siret" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvfvemEeiL3uw9NeuELg" value="Siret"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvf_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvgPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvgfemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvgvemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvg_emEeiL3uw9NeuELg" name="Nic" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvhPemEeiL3uw9NeuELg" value="Nic"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvhfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvhvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvh_emEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnviPemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvifemEeiL3uw9NeuELg" name="Tva" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvivemEeiL3uw9NeuELg" value="Tva"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvi_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvjPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvjfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvjvemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvj_emEeiL3uw9NeuELg" name="Naf" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvkPemEeiL3uw9NeuELg" value="Naf"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvkfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvkvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvk_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvlPemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYnvlfemEeiL3uw9NeuELg" name="TypeEtab1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYnvlvemEeiL3uw9NeuELg" value="TypeEtab1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYnvl_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYnvmPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYnvmfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYnvmvemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYnvm_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxgUPemEeiL3uw9NeuELg" name="TypeEtab2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxgUfemEeiL3uw9NeuELg" value="TypeEtab2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxgUvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYxgU_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxgVPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgVfemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgVvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxgV_emEeiL3uw9NeuELg" name="TypeEtab3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxgWPemEeiL3uw9NeuELg" value="TypeEtab3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxgWfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYxgWvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxgW_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgXPemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgXfemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxgXvemEeiL3uw9NeuELg" name="DateCreation" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxgX_emEeiL3uw9NeuELg" value="DateCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxgYPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYxgYfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxgYvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgY_emEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgZPemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxgZfemEeiL3uw9NeuELg" name="SiteWeb" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxgZvemEeiL3uw9NeuELg" value="SiteWeb"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxgZ_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxgaPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgafemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgavemEeiL3uw9NeuELg" value="350"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxga_emEeiL3uw9NeuELg" name="Langue" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxgbPemEeiL3uw9NeuELg" value="Langue"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxgbfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYxgbvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxgb_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgcPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgcfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxgcvemEeiL3uw9NeuELg" name="TypeCompte" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxgc_emEeiL3uw9NeuELg" value="TypeCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxgdPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYxgdfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxgdvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgd_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgePemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxgefemEeiL3uw9NeuELg" name="Groupe" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxgevemEeiL3uw9NeuELg" value="Groupe"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxge_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxgfPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgffemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgfvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxgf_emEeiL3uw9NeuELg" name="International" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxggPemEeiL3uw9NeuELg" value="International"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxggfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxggvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgg_emEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxghPemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxghfemEeiL3uw9NeuELg" name="Directif" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxghvemEeiL3uw9NeuELg" value="Directif"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxgh_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxgiPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgifemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgivemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxgi_emEeiL3uw9NeuELg" name="DateCrtCompte" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxgjPemEeiL3uw9NeuELg" value="DateCrtCompte"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxgjfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYxgjvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxgj_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgkPemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgkfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rYxgkvemEeiL3uw9NeuELg" name="Statut" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_rYxgk_emEeiL3uw9NeuELg" value="Statut"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rYxglPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rYxglfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rYxglvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rYxgl_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rYxgmPemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7RUPemEeiL3uw9NeuELg" name="CanalAcquisition" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7RUfemEeiL3uw9NeuELg" value="CanalAcquisition"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7RUvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rY7RU_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7RVPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7RVfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7RVvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7RV_emEeiL3uw9NeuELg" name="DateMaj_CHD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7RWPemEeiL3uw9NeuELg" value="DateMaj_CHD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7RWfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rY7RWvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7RW_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7RXPemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7RXfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7RXvemEeiL3uw9NeuELg" name="DateMaj_Orion" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7RX_emEeiL3uw9NeuELg" value="DateMaj_Orion"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7RYPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rY7RYfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7RYvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7RY_emEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7RZPemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7RZfemEeiL3uw9NeuELg" name="DateMaj_Ccial" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7RZvemEeiL3uw9NeuELg" value="DateMaj_Ccial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7RZ_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rY7RaPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7RafemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7RavemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7Ra_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7RbPemEeiL3uw9NeuELg" name="CompteChomette" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7RbfemEeiL3uw9NeuELg" value="CompteChomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7RbvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7Rb_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7RcPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7RcfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7RcvemEeiL3uw9NeuELg" name="DateSuppression" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7Rc_emEeiL3uw9NeuELg" value="DateSuppression"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7RdPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rY7RdfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7RdvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7Rd_emEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7RePemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7RefemEeiL3uw9NeuELg" name="TopSuppression" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7RevemEeiL3uw9NeuELg" value="TopSuppression"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7Re_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7RfPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7RffemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7RfvemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7Rf_emEeiL3uw9NeuELg" name="CHDID" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7RgPemEeiL3uw9NeuELg" value="CHDID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7RgfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7RgvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7Rg_emEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7RhPemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7RhfemEeiL3uw9NeuELg" name="Latitude" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7RhvemEeiL3uw9NeuELg" value="Latitude"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7Rh_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7RiPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7RifemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7RivemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rY7Ri_emEeiL3uw9NeuELg" name="Longitude" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_rY7RjPemEeiL3uw9NeuELg" value="Longitude"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rY7RjfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rY7RjvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rY7Rj_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rY7RkPemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZEbQPemEeiL3uw9NeuELg" name="Propriete" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZEbQfemEeiL3uw9NeuELg" value="Propriete"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZEbQvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rZEbQ_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZEbRPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZEbRfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZEbRvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rZEbR_emEeiL3uw9NeuELg" name="Nature" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_rZEbSPemEeiL3uw9NeuELg" value="Nature"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rZEbSfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rZEbSvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rZEbS_emEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rZEbTPemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rZEbTfemEeiL3uw9NeuELg" name="PK_Client">
        <node defType="com.stambia.rdbms.colref" id="_rZEbTvemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rZEbT_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr1mQPemEeiL3uw9NeuELg" name="FK_Client_ref_Canal">
        <node defType="com.stambia.rdbms.relation" id="_rr1mQfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr1mQvemEeiL3uw9NeuELg" ref="#_rY7RUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=CanalAcquisition?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr1mQ_emEeiL3uw9NeuELg" ref="#_rcDd1PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr1mRPemEeiL3uw9NeuELg" name="FK_Client_ref_Langue">
        <node defType="com.stambia.rdbms.relation" id="_rr1mRfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr1mRvemEeiL3uw9NeuELg" ref="#_rYxga_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Langue?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr1mR_emEeiL3uw9NeuELg" ref="#_rlcDVPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr1mSPemEeiL3uw9NeuELg" name="FK_Client_ref_Propriété">
        <node defType="com.stambia.rdbms.relation" id="_rr1mSfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr1mSvemEeiL3uw9NeuELg" ref="#_rZEbQPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Propriete?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr1mS_emEeiL3uw9NeuELg" ref="#_rl4vRPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr1mTPemEeiL3uw9NeuELg" name="FK_Client_ref_Societe">
        <node defType="com.stambia.rdbms.relation" id="_rr1mTfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr1mTvemEeiL3uw9NeuELg" ref="#_rYnvXvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdSociete?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr1mT_emEeiL3uw9NeuELg" ref="#_rXu-gPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr1mUPemEeiL3uw9NeuELg" name="FK_Client_ref_StatutClient">
        <node defType="com.stambia.rdbms.relation" id="_rr1mUfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr1mUvemEeiL3uw9NeuELg" ref="#_rYxgkvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Statut?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr1mU_emEeiL3uw9NeuELg" ref="#_rczEtPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr1mVPemEeiL3uw9NeuELg" name="FK_Client_ref_TypeCompte">
        <node defType="com.stambia.rdbms.relation" id="_rr1mVfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr1mVvemEeiL3uw9NeuELg" ref="#_rYxgcvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeCompte?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr1mV_emEeiL3uw9NeuELg" ref="#_rnhG9PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr1mWPemEeiL3uw9NeuELg" name="FK_Client_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_rr1mWfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr1mWvemEeiL3uw9NeuELg" ref="#_rYnvlfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeEtab1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr1mW_emEeiL3uw9NeuELg" ref="#_rj81lPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr1mXPemEeiL3uw9NeuELg" name="FK_Client_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.relation" id="_rr1mXfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr1mXvemEeiL3uw9NeuELg" ref="#_rYxgUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeEtab2?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr1mX_emEeiL3uw9NeuELg" ref="#_rjp6qfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rr1mYPemEeiL3uw9NeuELg" name="FK_Client_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.relation" id="_rr1mYfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rr1mYvemEeiL3uw9NeuELg" ref="#_rYxgV_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeEtab3?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rr1mY_emEeiL3uw9NeuELg" ref="#_rjMnpPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.column" id="_t_udkOnfEemXsr6XuHVYhQ" name="DateMaj_Web" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_t_udkenfEemXsr6XuHVYhQ" value="DateMaj_Web"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_t_udkunfEemXsr6XuHVYhQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_t_udk-nfEemXsr6XuHVYhQ" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_t_udlOnfEemXsr6XuHVYhQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_t_udlenfEemXsr6XuHVYhQ" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_t_udlunfEemXsr6XuHVYhQ" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_t_wSwOnfEemXsr6XuHVYhQ" name="MailPrincipal" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_t_wSwenfEemXsr6XuHVYhQ" value="MailPrincipal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_t_wSwunfEemXsr6XuHVYhQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_t_wSw-nfEemXsr6XuHVYhQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_t_wSxOnfEemXsr6XuHVYhQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_t_wSxenfEemXsr6XuHVYhQ" value="150"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rd1mgfemEeiL3uw9NeuELg" name="Client_Telephone">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rd1mgvemEeiL3uw9NeuELg" value="Client_Telephone"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rd1mg_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rd1mhPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rd1mhfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rd1mhvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rd1mh_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rd1miPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rd1mifemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rd1mivemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rd1mi_emEeiL3uw9NeuELg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rd1mjPemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rd1mjfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rd1mjvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rd1mj_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rd1mkPemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rd1mkfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rd1mkvemEeiL3uw9NeuELg" name="Numero" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rd1mk_emEeiL3uw9NeuELg" value="Numero"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rd1mlPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rd1mlfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rd1mlvemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rd1ml_emEeiL3uw9NeuELg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rd1mmPemEeiL3uw9NeuELg" name="IdType" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rd1mmfemEeiL3uw9NeuELg" value="IdType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rd1mmvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rd1mm_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rd1mnPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rd1mnfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rd1mnvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rd_XgPemEeiL3uw9NeuELg" name="PK_Telephone">
        <node defType="com.stambia.rdbms.colref" id="_rd_XgfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rd_XgvemEeiL3uw9NeuELg" ref="#_rd1mhPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsSSOPemEeiL3uw9NeuELg" name="FK_Client_Telephone_Client">
        <node defType="com.stambia.rdbms.relation" id="_rsSSOfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsSSOvemEeiL3uw9NeuELg" ref="#_rd1mi_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsSSO_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsSSPPemEeiL3uw9NeuELg" name="FK_Telephone_ref_TypeTel">
        <node defType="com.stambia.rdbms.relation" id="_rsSSPfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsSSPvemEeiL3uw9NeuELg" ref="#_rd1mmPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdType?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsSSP_emEeiL3uw9NeuELg" ref="#_reu-ZPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rbT28femEeiL3uw9NeuELg" name="Client_ActiviteSegment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rbT28vemEeiL3uw9NeuELg" value="Client_ActiviteSegment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rbT28_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rbT29PemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbT29femEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbT29vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbT29_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbT2-PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbT2-femEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbT2-vemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbT2-_emEeiL3uw9NeuELg" name="IdActivite" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbT2_PemEeiL3uw9NeuELg" value="IdActivite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbT2_femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbT2_vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbT2__emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbT3APemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbT3AfemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rbdA4PemEeiL3uw9NeuELg" name="PK_Client_ActiviteSegment">
        <node defType="com.stambia.rdbms.colref" id="_rbdA4femEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rbdA4vemEeiL3uw9NeuELg" ref="#_rbT29PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rbdA4_emEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rbdA5PemEeiL3uw9NeuELg" ref="#_rbT2-_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdActivite?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsJIQPemEeiL3uw9NeuELg" name="FK_Client_ActiviteSegment_Client">
        <node defType="com.stambia.rdbms.relation" id="_rsJIQfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsJIQvemEeiL3uw9NeuELg" ref="#_rbT29PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsJIQ_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsJIRPemEeiL3uw9NeuELg" name="FK_Client_ActiviteSegment_ref_ActiviteSegment1">
        <node defType="com.stambia.rdbms.relation" id="_rsJIRfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsJIRvemEeiL3uw9NeuELg" ref="#_rbT2-_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdActivite?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsJIR_emEeiL3uw9NeuELg" ref="#_rkircPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rbdA5vemEeiL3uw9NeuELg" name="Client_HoraireLiv">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rbdA5_emEeiL3uw9NeuELg" value="Client_HoraireLiv"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rbdA6PemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rbdA6femEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbdA6vemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbdA6_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbdA7PemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbdA7femEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbdA7vemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbdA7_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbdA8PemEeiL3uw9NeuELg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbdA8femEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbdA8vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbdA8_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbdA9PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbdA9femEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbdA9vemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbdA9_emEeiL3uw9NeuELg" name="H_DebutLivLundi" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbdA-PemEeiL3uw9NeuELg" value="H_DebutLivLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbdA-femEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbdA-vemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbdA-_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbdA_PemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbdA_femEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmx4PemEeiL3uw9NeuELg" name="H_DebutLivMardi" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmx4femEeiL3uw9NeuELg" value="H_DebutLivMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmx4vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmx4_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmx5PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmx5femEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmx5vemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmx5_emEeiL3uw9NeuELg" name="H_DebutLivMercredi" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmx6PemEeiL3uw9NeuELg" value="H_DebutLivMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmx6femEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmx6vemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmx6_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmx7PemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmx7femEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmx7vemEeiL3uw9NeuELg" name="H_DebutLivJeudi" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmx7_emEeiL3uw9NeuELg" value="H_DebutLivJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmx8PemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmx8femEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmx8vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmx8_emEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmx9PemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmx9femEeiL3uw9NeuELg" name="H_DebutLivVendredi" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmx9vemEeiL3uw9NeuELg" value="H_DebutLivVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmx9_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmx-PemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmx-femEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmx-vemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmx-_emEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmx_PemEeiL3uw9NeuELg" name="H_DebutLivSamedi" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmx_femEeiL3uw9NeuELg" value="H_DebutLivSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmx_vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmx__emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmyAPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmyAfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmyAvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmyA_emEeiL3uw9NeuELg" name="H_DebutLivDimanche" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmyBPemEeiL3uw9NeuELg" value="H_DebutLivDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmyBfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmyBvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmyB_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmyCPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmyCfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmyCvemEeiL3uw9NeuELg" name="H_FinLivLundi" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmyC_emEeiL3uw9NeuELg" value="H_FinLivLundi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmyDPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmyDfemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmyDvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmyD_emEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmyEPemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmyEfemEeiL3uw9NeuELg" name="H_FinLivMardi" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmyEvemEeiL3uw9NeuELg" value="H_FinLivMardi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmyE_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmyFPemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmyFfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmyFvemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmyF_emEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmyGPemEeiL3uw9NeuELg" name="H_FinLivMercredi" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmyGfemEeiL3uw9NeuELg" value="H_FinLivMercredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmyGvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmyG_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmyHPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmyHfemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmyHvemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbmyH_emEeiL3uw9NeuELg" name="H_FinLivJeudi" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbmyIPemEeiL3uw9NeuELg" value="H_FinLivJeudi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbmyIfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbmyIvemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbmyI_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbmyJPemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbmyJfemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbv70PemEeiL3uw9NeuELg" name="H_FinLivVendredi" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbv70femEeiL3uw9NeuELg" value="H_FinLivVendredi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbv70vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbv70_emEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbv71PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbv71femEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbv71vemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbv71_emEeiL3uw9NeuELg" name="H_FinLivSamedi" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbv72PemEeiL3uw9NeuELg" value="H_FinLivSamedi"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbv72femEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbv72vemEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbv72_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbv73PemEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbv73femEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbv73vemEeiL3uw9NeuELg" name="H_FinLivDimanche" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbv73_emEeiL3uw9NeuELg" value="H_FinLivDimanche"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbv74PemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbv74femEeiL3uw9NeuELg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbv74vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbv74_emEeiL3uw9NeuELg" value="time"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbv75PemEeiL3uw9NeuELg" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rbv75femEeiL3uw9NeuELg" name="PK_Horaire_Livraison">
        <node defType="com.stambia.rdbms.colref" id="_rbv75vemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rbv75_emEeiL3uw9NeuELg" ref="#_rbdA8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsJISPemEeiL3uw9NeuELg" name="FK_Client_HoraireLiv_Client">
        <node defType="com.stambia.rdbms.relation" id="_rsJISfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsJISvemEeiL3uw9NeuELg" ref="#_rbdA8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsJIS_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rV8OxvemEeiL3uw9NeuELg" name="Client_ActiviteParCanal">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rV8Ox_emEeiL3uw9NeuELg" value="Client_ActiviteParCanal"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rV8OyPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rWF_wPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWF_wfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWF_wvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rWF_w_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWF_xPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWF_xfemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWF_xvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWF_x_emEeiL3uw9NeuELg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWF_yPemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWF_yfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rWF_yvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWF_y_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWF_zPemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWF_zfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWF_zvemEeiL3uw9NeuELg" name="IdCanal" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWF_z_emEeiL3uw9NeuELg" value="IdCanal"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWF_0PemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rWF_0femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWF_0vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWF_0_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWF_1PemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWF_1femEeiL3uw9NeuELg" name="Actif12dm" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWF_1vemEeiL3uw9NeuELg" value="Actif12dm"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWF_1_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWF_2PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWF_2femEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWF_2vemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rWF_2_emEeiL3uw9NeuELg" name="PK_ActiviteParCanal">
        <node defType="com.stambia.rdbms.colref" id="_rWF_3PemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rWF_3femEeiL3uw9NeuELg" ref="#_rWF_wPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rrZhaPemEeiL3uw9NeuELg" name="FK_ActiviteParCanal_Client">
        <node defType="com.stambia.rdbms.relation" id="_rrZhafemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rrZhavemEeiL3uw9NeuELg" ref="#_rWF_x_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rrZha_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rrZhbPemEeiL3uw9NeuELg" name="FK_ActiviteParCanal_ref_Canal1">
        <node defType="com.stambia.rdbms.relation" id="_rrZhbfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rrZhbvemEeiL3uw9NeuELg" ref="#_rWF_zvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdCanal?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rrZhb_emEeiL3uw9NeuELg" ref="#_rcDd1PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rqDdkfemEeiL3uw9NeuELg" name="ref_NbEleves">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rqDdkvemEeiL3uw9NeuELg" value="ref_NbEleves"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rqDdk_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rqDdlPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqDdlfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqDdlvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqDdl_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqDdmPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqDdmfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqDdmvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqDdm_emEeiL3uw9NeuELg" name="NbEleves" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqDdnPemEeiL3uw9NeuELg" value="NbEleves"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqDdnfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqDdnvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqDdn_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqDdoPemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqDdofemEeiL3uw9NeuELg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqDdovemEeiL3uw9NeuELg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqDdo_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqDdpPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqDdpfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqDdpvemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqDdp_emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rqDdqPemEeiL3uw9NeuELg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rqDdqfemEeiL3uw9NeuELg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rqDdqvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rqDdq_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rqDdrPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rqDdrfemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rqDdrvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rqNOkPemEeiL3uw9NeuELg" name="PK_ref_NbEleves">
        <node defType="com.stambia.rdbms.colref" id="_rqNOkfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rqNOkvemEeiL3uw9NeuELg" ref="#_rqDdlPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rhQt9PemEeiL3uw9NeuELg" name="ref_TypeEtabClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rhQt9femEeiL3uw9NeuELg" value="ref_TypeEtabClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rhQt9vemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rhQt9_emEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhQt-PemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhQt-femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhQt-vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhQt-_emEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhQt_PemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhQt_femEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhae8PemEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhae8femEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhae8vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhae8_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhae9PemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhae9femEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhae9vemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhae9_emEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhae-PemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhae-femEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhae-vemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhae-_emEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rhae_PemEeiL3uw9NeuELg" name="PK_ref_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_rhae_femEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rhae_vemEeiL3uw9NeuELg" ref="#_rhQt9_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rXlNivemEeiL3uw9NeuELg" name="ref_Societe">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rXlNi_emEeiL3uw9NeuELg" value="ref_Societe"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rXlNjPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rXu-gPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXu-gfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXu-gvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rXu-g_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXu-hPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXu-hfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXu-hvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rXu-h_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXu-iPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXu-ifemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXu-ivemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXu-i_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXu-jPemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rXu-jfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXu-jvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXu-j_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXu-kPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXu-kfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXu-kvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rXu-k_emEeiL3uw9NeuELg" name="PK_ref_Societe">
        <node defType="com.stambia.rdbms.colref" id="_rXu-lPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rXu-lfemEeiL3uw9NeuELg" ref="#_rXu-gPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rczEsfemEeiL3uw9NeuELg" name="ref_StatutClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rczEsvemEeiL3uw9NeuELg" value="ref_StatutClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rczEs_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rczEtPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rczEtfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rczEtvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rczEt_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rczEuPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rczEufemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rczEuvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rczEu_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rczEvPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rczEvfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rczEvvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rczEv_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rczEwPemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rczEwfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rczEwvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rczEw_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rczExPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rczExfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rczExvemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rczEx_emEeiL3uw9NeuELg" name="PK_ref_StatutClient">
        <node defType="com.stambia.rdbms.colref" id="_rczEyPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rczEyfemEeiL3uw9NeuELg" ref="#_rczEtPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rpT2sfemEeiL3uw9NeuELg" name="ref_NbEmployes">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rpT2svemEeiL3uw9NeuELg" value="ref_NbEmployes"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rpT2s_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rpT2tPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpT2tfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpT2tvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rpT2t_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpT2uPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpT2ufemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpT2uvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpT2u_emEeiL3uw9NeuELg" name="NbEmployes" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpT2vPemEeiL3uw9NeuELg" value="NbEmployes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpT2vfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpT2vvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpT2v_emEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpT2wPemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpdnsPemEeiL3uw9NeuELg" name="NbMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpdnsfemEeiL3uw9NeuELg" value="NbMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpdnsvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rpdns_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpdntPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpdntfemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpdntvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpdnt_emEeiL3uw9NeuELg" name="NbMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpdnuPemEeiL3uw9NeuELg" value="NbMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpdnufemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rpdnuvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpdnu_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpdnvPemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpdnvfemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rpdnvvemEeiL3uw9NeuELg" name="PK_ref_NbEmployes">
        <node defType="com.stambia.rdbms.colref" id="_rpdnv_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rpdnwPemEeiL3uw9NeuELg" ref="#_rpT2tPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rfxgRPemEeiL3uw9NeuELg" name="Client_InfosCciales">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rfxgRfemEeiL3uw9NeuELg" value="Client_InfosCciales"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rfxgRvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rf7RMPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rf7RMfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rf7RMvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rf7RM_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rf7RNPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rf7RNfemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rf7RNvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rf7RN_emEeiL3uw9NeuELg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rf7ROPemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rf7ROfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rf7ROvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rf7RO_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rf7RPPemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rf7RPfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rf7RPvemEeiL3uw9NeuELg" name="DateDebAffluence" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rf7RP_emEeiL3uw9NeuELg" value="DateDebAffluence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rf7RQPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rf7RQfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rf7RQvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rf7RQ_emEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rf7RRPemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rf7RRfemEeiL3uw9NeuELg" name="DateFinAffluence" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rf7RRvemEeiL3uw9NeuELg" value="DateFinAffluence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rf7RR_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rf7RSPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rf7RSfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rf7RSvemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rf7RS_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rf7RTPemEeiL3uw9NeuELg" name="StatutCOLL" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rf7RTfemEeiL3uw9NeuELg" value="StatutCOLL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rf7RTvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rf7RT_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rf7RUPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rf7RUfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rf7RUvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgEbIPemEeiL3uw9NeuELg" name="NiveauStanding" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgEbIfemEeiL3uw9NeuELg" value="NiveauStanding"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgEbIvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgEbI_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgEbJPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgEbJfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgEbJvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgEbJ_emEeiL3uw9NeuELg" name="OuvertTouteAnnee" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgEbKPemEeiL3uw9NeuELg" value="OuvertTouteAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgEbKfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgEbKvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgEbK_emEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgEbLPemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgEbLfemEeiL3uw9NeuELg" name="DigitalFriendly" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgEbLvemEeiL3uw9NeuELg" value="DigitalFriendly"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgEbL_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgEbMPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgEbMfemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgEbMvemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgEbM_emEeiL3uw9NeuELg" name="NbLits" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgEbNPemEeiL3uw9NeuELg" value="NbLits"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgEbNfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgEbNvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgEbN_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgEbOPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgEbOfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgEbOvemEeiL3uw9NeuELg" name="NbRepasJour" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgEbO_emEeiL3uw9NeuELg" value="NbRepasJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgEbPPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgEbPfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgEbPvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgEbP_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgEbQPemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgEbQfemEeiL3uw9NeuELg" name="NbCouvJour" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgEbQvemEeiL3uw9NeuELg" value="NbCouvJour"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgEbQ_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgEbRPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgEbRfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgEbRvemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgEbR_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgEbSPemEeiL3uw9NeuELg" name="NbChambres" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgEbSfemEeiL3uw9NeuELg" value="NbChambres"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgEbSvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgEbS_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgEbTPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgEbTfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgEbTvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgEbT_emEeiL3uw9NeuELg" name="NbEleves" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgEbUPemEeiL3uw9NeuELg" value="NbEleves"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgEbUfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgEbUvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgEbU_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgEbVPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgEbVfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgEbVvemEeiL3uw9NeuELg" name="NbEmployes" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgEbV_emEeiL3uw9NeuELg" value="NbEmployes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgEbWPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgEbWfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgEbWvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgEbW_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgEbXPemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMIPemEeiL3uw9NeuELg" name="TrancheCA" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOMIfemEeiL3uw9NeuELg" value="TrancheCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMIvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgOMI_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMJPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMJfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMJvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMJ_emEeiL3uw9NeuELg" name="TicketMoy" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOMKPemEeiL3uw9NeuELg" value="TicketMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMKfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgOMKvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMK_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMLPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMLfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMLvemEeiL3uw9NeuELg" name="PrxMoyNuit" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOML_emEeiL3uw9NeuELg" value="PrxMoyNuit"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMMPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgOMMfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMMvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMM_emEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMNPemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMNfemEeiL3uw9NeuELg" name="PotentielCArecur" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOMNvemEeiL3uw9NeuELg" value="PotentielCArecur"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMN_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgOMOPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMOfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMOvemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMO_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMPPemEeiL3uw9NeuELg" name="PotentielCAInvest" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOMPfemEeiL3uw9NeuELg" value="PotentielCAInvest"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMPvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgOMP_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMQPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMQfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMQvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMQ_emEeiL3uw9NeuELg" name="ScoringCA" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOMRPemEeiL3uw9NeuELg" value="ScoringCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMRfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgOMRvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMR_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMSPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMSfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMSvemEeiL3uw9NeuELg" name="FamilleFermes" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOMS_emEeiL3uw9NeuELg" value="FamilleFermes"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMTPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMTfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMTvemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMT_emEeiL3uw9NeuELg" value="512"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMUPemEeiL3uw9NeuELg" name="CondConcession" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOMUfemEeiL3uw9NeuELg" value="CondConcession"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMUvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgOMU_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMVPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMVfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMVvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMV_emEeiL3uw9NeuELg" name="ScoringRelation" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOMWPemEeiL3uw9NeuELg" value="ScoringRelation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMWfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgOMWvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMW_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMXPemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMXfemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgOMXvemEeiL3uw9NeuELg" name="Fidelite" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgOMX_emEeiL3uw9NeuELg" value="Fidelite"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgOMYPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgOMYfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgOMYvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgOMY_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgOMZPemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9IPemEeiL3uw9NeuELg" name="Comportement" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9IfemEeiL3uw9NeuELg" value="Comportement"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9IvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgX9I_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9JPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9JfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9JvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9J_emEeiL3uw9NeuELg" name="EcheanceProjet" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9KPemEeiL3uw9NeuELg" value="EcheanceProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9KfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgX9KvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9K_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9LPemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9LfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9LvemEeiL3uw9NeuELg" name="DernierProjet" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9L_emEeiL3uw9NeuELg" value="DernierProjet"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9MPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgX9MfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9MvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9M_emEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9NPemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9NfemEeiL3uw9NeuELg" name="CmdFinAnnee" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9NvemEeiL3uw9NeuELg" value="CmdFinAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9N_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9OPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9OfemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9OvemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9O_emEeiL3uw9NeuELg" name="PrchCmdFinAnnee" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9PPemEeiL3uw9NeuELg" value="PrchCmdFinAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9PfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgX9PvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9P_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9QPemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9QfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9QvemEeiL3uw9NeuELg" name="CompteChomette" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9Q_emEeiL3uw9NeuELg" value="CompteChomette"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9RPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9RfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9RvemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9R_emEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9SPemEeiL3uw9NeuELg" name="InvestCetteAnnee" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9SfemEeiL3uw9NeuELg" value="InvestCetteAnnee"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9SvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9S_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9TPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9TfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9TvemEeiL3uw9NeuELg" name="CcurentRegion" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9T_emEeiL3uw9NeuELg" value="CcurentRegion"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9UPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9UfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9UvemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9U_emEeiL3uw9NeuELg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9VPemEeiL3uw9NeuELg" name="DteCreation" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9VfemEeiL3uw9NeuELg" value="DteCreation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9VvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgX9V_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9WPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9WfemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9WvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rgX9W_emEeiL3uw9NeuELg" name="DteMAJCcial" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_rgX9XPemEeiL3uw9NeuELg" value="DteMAJCcial"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rgX9XfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rgX9XvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rgX9X_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rgX9YPemEeiL3uw9NeuELg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rgX9YfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rghHEPemEeiL3uw9NeuELg" name="AnneeExistence" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_rghHEfemEeiL3uw9NeuELg" value="AnneeExistence"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rghHEvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rghHE_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rghHFPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rghHFfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rghHFvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rghHF_emEeiL3uw9NeuELg" name="Remise" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_rghHGPemEeiL3uw9NeuELg" value="Remise"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rghHGfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rghHGvemEeiL3uw9NeuELg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rghHG_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rghHHPemEeiL3uw9NeuELg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rghHHfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rghHHvemEeiL3uw9NeuELg" name="ColTarifaire" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_rghHH_emEeiL3uw9NeuELg" value="ColTarifaire"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rghHIPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rghHIfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rghHIvemEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rghHI_emEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rghHJPemEeiL3uw9NeuELg" name="PK_Client_InfosCciales">
        <node defType="com.stambia.rdbms.colref" id="_rghHJfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rghHJvemEeiL3uw9NeuELg" ref="#_rf7RMPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0P_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_Client">
        <node defType="com.stambia.rdbms.relation" id="_rsl0QPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0QfemEeiL3uw9NeuELg" ref="#_rf7RN_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0QvemEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0Q_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_AnneesExistence">
        <node defType="com.stambia.rdbms.relation" id="_rsl0RPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0RfemEeiL3uw9NeuELg" ref="#_rghHEPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=AnneeExistence?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0RvemEeiL3uw9NeuELg" ref="#_rlSSUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0R_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_ComportementMultiCanal">
        <node defType="com.stambia.rdbms.relation" id="_rsl0SPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0SfemEeiL3uw9NeuELg" ref="#_rgX9IPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Comportement?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0SvemEeiL3uw9NeuELg" ref="#_rn0o8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0S_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_NbChambres">
        <node defType="com.stambia.rdbms.relation" id="_rsl0TPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0TfemEeiL3uw9NeuELg" ref="#_rgEbSPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=NbChambres?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0TvemEeiL3uw9NeuELg" ref="#_rfLqVPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0T_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_NbCouvertJour">
        <node defType="com.stambia.rdbms.relation" id="_rsl0UPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0UfemEeiL3uw9NeuELg" ref="#_rgEbQfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=NbCouvJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0UvemEeiL3uw9NeuELg" ref="#_rlu-QPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0U_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_NbEleves">
        <node defType="com.stambia.rdbms.relation" id="_rsl0VPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0VfemEeiL3uw9NeuELg" ref="#_rgEbT_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=NbEleves?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0VvemEeiL3uw9NeuELg" ref="#_rqDdlPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0V_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_NbEmployes">
        <node defType="com.stambia.rdbms.relation" id="_rsl0WPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0WfemEeiL3uw9NeuELg" ref="#_rgEbVvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=NbEmployes?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0WvemEeiL3uw9NeuELg" ref="#_rpT2tPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0W_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_NbLits">
        <node defType="com.stambia.rdbms.relation" id="_rsl0XPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0XfemEeiL3uw9NeuELg" ref="#_rgEbM_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=NbLits?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0XvemEeiL3uw9NeuELg" ref="#_rbKF8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0X_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_NbRepasJour">
        <node defType="com.stambia.rdbms.relation" id="_rsl0YPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0YfemEeiL3uw9NeuELg" ref="#_rgEbOvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=NbRepasJour?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0YvemEeiL3uw9NeuELg" ref="#_rcpTsPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0Y_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_NiveauFidelite">
        <node defType="com.stambia.rdbms.relation" id="_rsl0ZPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0ZfemEeiL3uw9NeuELg" ref="#_rgOMXvemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Fidelite?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0ZvemEeiL3uw9NeuELg" ref="#_rXIhkPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0Z_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_NiveauStanding">
        <node defType="com.stambia.rdbms.relation" id="_rsl0aPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0afemEeiL3uw9NeuELg" ref="#_rgEbIPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=NiveauStanding?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0avemEeiL3uw9NeuELg" ref="#_reSSd_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0a_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_ScoringCA">
        <node defType="com.stambia.rdbms.relation" id="_rsl0bPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0bfemEeiL3uw9NeuELg" ref="#_rgOMQ_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=ScoringCA?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0bvemEeiL3uw9NeuELg" ref="#_riAU0PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0b_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_StatutCOLL">
        <node defType="com.stambia.rdbms.relation" id="_rsl0cPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0cfemEeiL3uw9NeuELg" ref="#_rf7RTPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=StatutCOLL?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0cvemEeiL3uw9NeuELg" ref="#_rX4IdPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0c_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_TicketRepasMoyen">
        <node defType="com.stambia.rdbms.relation" id="_rsl0dPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0dfemEeiL3uw9NeuELg" ref="#_rgOMJ_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TicketMoy?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0dvemEeiL3uw9NeuELg" ref="#_rcWYzfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsl0d_emEeiL3uw9NeuELg" name="FK_Client_InfosCciales_ref_TrancheCA">
        <node defType="com.stambia.rdbms.relation" id="_rsl0ePemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsl0efemEeiL3uw9NeuELg" ref="#_rgOMIPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TrancheCA?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsl0evemEeiL3uw9NeuELg" ref="#_rpmxpPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_reu-YfemEeiL3uw9NeuELg" name="ref_TypeTel">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_reu-YvemEeiL3uw9NeuELg" value="ref_TypeTel"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_reu-Y_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_reu-ZPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_reu-ZfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_reu-ZvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_reu-Z_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_reu-aPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_reu-afemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_reu-avemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_re4IUPemEeiL3uw9NeuELg" name="Type" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_re4IUfemEeiL3uw9NeuELg" value="Type"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_re4IUvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_re4IU_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_re4IVPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_re4IVfemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_re4IVvemEeiL3uw9NeuELg" name="PK_ref_TypeTel">
        <node defType="com.stambia.rdbms.colref" id="_re4IV_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_re4IWPemEeiL3uw9NeuELg" ref="#_reu-ZPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rlcDUfemEeiL3uw9NeuELg" name="ref_Langue">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rlcDUvemEeiL3uw9NeuELg" value="ref_Langue"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rlcDU_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rlcDVPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlcDVfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlcDVvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rlcDV_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlcDWPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlcDWfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlcDWvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rlcDW_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rlcDXPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rlcDXfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rlcDXvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rlcDX_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rlcDYPemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rllNQPemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rllNQfemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rllNQvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rllNQ_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rllNRPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rllNRfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rllNRvemEeiL3uw9NeuELg" name="PK_Langue">
        <node defType="com.stambia.rdbms.colref" id="_rllNR_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rllNSPemEeiL3uw9NeuELg" ref="#_rlcDVPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rVpT1PemEeiL3uw9NeuELg" name="Client_PrestationOfferte">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rVpT1femEeiL3uw9NeuELg" value="Client_PrestationOfferte"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rVpT1vemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rVzE0PemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rVzE0femEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rVzE0vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rVzE0_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rVzE1PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rVzE1femEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rVzE1vemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rVzE1_emEeiL3uw9NeuELg" name="IdPrestation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rVzE2PemEeiL3uw9NeuELg" value="IdPrestation"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rVzE2femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rVzE2vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rVzE2_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rVzE3PemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rVzE3femEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rV8OwPemEeiL3uw9NeuELg" name="PK_Client_PrestationOfferte">
        <node defType="com.stambia.rdbms.colref" id="_rV8OwfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rV8OwvemEeiL3uw9NeuELg" ref="#_rVzE0PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rV8Ow_emEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rV8OxPemEeiL3uw9NeuELg" ref="#_rVzE1_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdPrestation?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rrZhYPemEeiL3uw9NeuELg" name="FK_Client_PrestationOfferte_Client">
        <node defType="com.stambia.rdbms.relation" id="_rrZhYfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rrZhYvemEeiL3uw9NeuELg" ref="#_rVzE0PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rrZhY_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rrZhZPemEeiL3uw9NeuELg" name="FK_Client_PrestationOfferte_ref_PrestationsOffertes1">
        <node defType="com.stambia.rdbms.relation" id="_rrZhZfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rrZhZvemEeiL3uw9NeuELg" ref="#_rVzE1_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdPrestation?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rrZhZ_emEeiL3uw9NeuELg" ref="#_relNYPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rhafAPemEeiL3uw9NeuELg" name="Client_PerimetreDernierProjet">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rhafAfemEeiL3uw9NeuELg" value="Client_PerimetreDernierProjet"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rhafAvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rhkP8PemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhkP8femEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhkP8vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhkP8_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhkP9PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhkP9femEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhkP9vemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhkP9_emEeiL3uw9NeuELg" name="IdUnivers" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhkP-PemEeiL3uw9NeuELg" value="IdUnivers"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhkP-femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhkP-vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhkP-_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhkP_PemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhkP_femEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rhkP_vemEeiL3uw9NeuELg" name="PK_Client_PerimetreDernierProjet">
        <node defType="com.stambia.rdbms.colref" id="_rhkP__emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rhkQAPemEeiL3uw9NeuELg" ref="#_rhkP8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rhkQAfemEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rhkQAvemEeiL3uw9NeuELg" ref="#_rhkP9_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdUnivers?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsu-IPemEeiL3uw9NeuELg" name="FK_Client_PerimetreDernierProjet_Client">
        <node defType="com.stambia.rdbms.relation" id="_rsu-IfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsu-IvemEeiL3uw9NeuELg" ref="#_rhkP8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsu-I_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsu-JPemEeiL3uw9NeuELg" name="FK_Client_PerimetreDernierProjet_ref_UniversConso">
        <node defType="com.stambia.rdbms.relation" id="_rsu-JfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsu-JvemEeiL3uw9NeuELg" ref="#_rhkP9_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdUnivers?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsu-J_emEeiL3uw9NeuELg" ref="#_rW1mp_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rnOMBPemEeiL3uw9NeuELg" name="Client_TypeEtabClient">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rnOMBfemEeiL3uw9NeuELg" value="Client_TypeEtabClient"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rnOMBvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rnX9APemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnX9AfemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnX9AvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rnX9A_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnX9BPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnX9BfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnX9BvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnX9B_emEeiL3uw9NeuELg" name="IdClientType" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnX9CPemEeiL3uw9NeuELg" value="IdClientType"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnX9CfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rnX9CvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnX9C_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnX9DPemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnX9DfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rnX9DvemEeiL3uw9NeuELg" name="PK_Client_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_rnX9D_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rnX9EPemEeiL3uw9NeuELg" ref="#_rnX9APemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rnX9EfemEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rnX9EvemEeiL3uw9NeuELg" ref="#_rnX9B_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClientType?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtelEPemEeiL3uw9NeuELg" name="FK_Client_TypeEtabClient_Client">
        <node defType="com.stambia.rdbms.relation" id="_rtelEfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtelEvemEeiL3uw9NeuELg" ref="#_rnX9APemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtelE_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtelFPemEeiL3uw9NeuELg" name="FK_Client_TypeEtabClient_ref_TypeEtabClient1">
        <node defType="com.stambia.rdbms.relation" id="_rtelFfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtelFvemEeiL3uw9NeuELg" ref="#_rnX9B_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClientType?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtelF_emEeiL3uw9NeuELg" ref="#_rhQt9_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rataAfemEeiL3uw9NeuELg" name="ref_EnvTouristique">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rataAvemEeiL3uw9NeuELg" value="ref_EnvTouristique"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rataA_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rataBPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rataBfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rataBvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rataB_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rataCPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rataCfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rataCvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ra3LAPemEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ra3LAfemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ra3LAvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ra3LA_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ra3LBPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ra3LBfemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ra3LBvemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_ra3LB_emEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ra3LCPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ra3LCfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ra3LCvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ra3LC_emEeiL3uw9NeuELg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ra3LDPemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_ra3LDfemEeiL3uw9NeuELg" name="PK_ref_EnvTouristique">
        <node defType="com.stambia.rdbms.colref" id="_ra3LDvemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_ra3LD_emEeiL3uw9NeuELg" ref="#_rataBPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rpmxofemEeiL3uw9NeuELg" name="ref_TrancheCA">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rpmxovemEeiL3uw9NeuELg" value="ref_TrancheCA"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rpmxo_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rpmxpPemEeiL3uw9NeuELg" name="id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpmxpfemEeiL3uw9NeuELg" value="id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpmxpvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rpmxp_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpmxqPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpmxqfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpmxqvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpmxq_emEeiL3uw9NeuELg" name="TrancheCA" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpmxrPemEeiL3uw9NeuELg" value="TrancheCA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpmxrfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpmxrvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpmxr_emEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpmxsPemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpmxsfemEeiL3uw9NeuELg" name="TrancheCAMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpmxsvemEeiL3uw9NeuELg" value="TrancheCAMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpmxs_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rpmxtPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpmxtfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpmxtvemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpmxt_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rpmxuPemEeiL3uw9NeuELg" name="TrancheCAMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rpmxufemEeiL3uw9NeuELg" value="TrancheCAMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rpmxuvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rpmxu_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rpmxvPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rpmxvfemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rpmxvvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rpwioPemEeiL3uw9NeuELg" name="PK_ref_TrancheCA">
        <node defType="com.stambia.rdbms.colref" id="_rpwiofemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rpwiovemEeiL3uw9NeuELg" ref="#_rpmxpPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rhtZ4femEeiL3uw9NeuELg" name="ref_CHD_TypeEtab">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rhtZ4vemEeiL3uw9NeuELg" value="ref_CHD_TypeEtab"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rhtZ4_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rhtZ5PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhtZ5femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhtZ5vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhtZ5_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhtZ6PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhtZ6femEeiL3uw9NeuELg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhtZ6vemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhtZ6_emEeiL3uw9NeuELg" name="GFCCode" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhtZ7PemEeiL3uw9NeuELg" value="GFCCode"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhtZ7femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhtZ7vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhtZ7_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhtZ8PemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhtZ8femEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhtZ8vemEeiL3uw9NeuELg" name="TypeEtabN1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhtZ8_emEeiL3uw9NeuELg" value="TypeEtabN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhtZ9PemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhtZ9femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhtZ9vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhtZ9_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhtZ-PemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rhtZ-femEeiL3uw9NeuELg" name="TypeEtabN2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rhtZ-vemEeiL3uw9NeuELg" value="TypeEtabN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rhtZ-_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rhtZ_PemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rhtZ_femEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rhtZ_vemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rhtZ__emEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rh3K4PemEeiL3uw9NeuELg" name="TypeEtabN3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rh3K4femEeiL3uw9NeuELg" value="TypeEtabN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rh3K4vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rh3K4_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rh3K5PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rh3K5femEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rh3K5vemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rh3K5_emEeiL3uw9NeuELg" name="PK_ref_CHD_TypeEtabClient">
        <node defType="com.stambia.rdbms.colref" id="_rh3K6PemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rh3K6femEeiL3uw9NeuELg" ref="#_rhtZ5PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsu-KPemEeiL3uw9NeuELg" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv1">
        <node defType="com.stambia.rdbms.relation" id="_rsu-KfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsu-KvemEeiL3uw9NeuELg" ref="#_rhtZ8vemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeEtabN1?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsu-K_emEeiL3uw9NeuELg" ref="#_rj81lPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsu-LPemEeiL3uw9NeuELg" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv2">
        <node defType="com.stambia.rdbms.relation" id="_rsu-LfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsu-LvemEeiL3uw9NeuELg" ref="#_rhtZ-femEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeEtabN2?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsu-L_emEeiL3uw9NeuELg" ref="#_rjp6qfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rsu-MPemEeiL3uw9NeuELg" name="FK_ref_CHD_TypeEtab_ref_TypeEtabNiv3">
        <node defType="com.stambia.rdbms.relation" id="_rsu-MfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rsu-MvemEeiL3uw9NeuELg" ref="#_rh3K4PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=TypeEtabN3?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rsu-M_emEeiL3uw9NeuELg" ref="#_rjMnpPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rXbchPemEeiL3uw9NeuELg" name="ref_ColonneTarifaire">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rXbchfemEeiL3uw9NeuELg" value="ref_ColonneTarifaire"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rXbchvemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rXbch_emEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXbciPemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXbcifemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rXbcivemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXbci_emEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXbcjPemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXbcjfemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rXbcjvemEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXbcj_emEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXbckPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXbckfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXbckvemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXbck_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rXlNgPemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rXlNgfemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rXlNgvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rXlNg_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rXlNhPemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rXlNhfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rXlNhvemEeiL3uw9NeuELg" name="PK_ref_ColonneTarifaire">
        <node defType="com.stambia.rdbms.colref" id="_rXlNh_emEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rXlNiPemEeiL3uw9NeuELg" ref="#_rXbch_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rmoWMfemEeiL3uw9NeuELg" name="ref_NewsletterType">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rmoWMvemEeiL3uw9NeuELg" value="ref_NewsletterType"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rmoWM_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rmxgEPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rmxgEfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rmxgEvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rmxgE_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rmxgFPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rmxgFfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rmxgFvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rmxgF_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rmxgGPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rmxgGfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rmxgGvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rmxgG_emEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rmxgHPemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rmxgHfemEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rmxgHvemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rmxgH_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rmxgIPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rmxgIfemEeiL3uw9NeuELg" value="nvarchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rmxgIvemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rmxgI_emEeiL3uw9NeuELg" name="PK_ref_NewsletterType">
        <node defType="com.stambia.rdbms.colref" id="_rmxgJPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rmxgJfemEeiL3uw9NeuELg" ref="#_rmxgEPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_ra3LEfemEeiL3uw9NeuELg" name="ref_ScoringFinance">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_ra3LEvemEeiL3uw9NeuELg" value="ref_ScoringFinance"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_ra3LE_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rbAU8PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbAU8femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbAU8vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rbAU8_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbAU9PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbAU9femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbAU9vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbAU9_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbAU-PemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbAU-femEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbAU-vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbAU-_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbAU_PemEeiL3uw9NeuELg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rbAU_femEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rbAU_vemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rbAU__emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rbAVAPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rbAVAfemEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rbAVAvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rbAVA_emEeiL3uw9NeuELg" name="PK_ref_ScoringFinance">
        <node defType="com.stambia.rdbms.colref" id="_rbAVBPemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rbAVBfemEeiL3uw9NeuELg" ref="#_rbAU8PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rl4vQfemEeiL3uw9NeuELg" name="ref_Propriété">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rl4vQvemEeiL3uw9NeuELg" value="ref_Propriété"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rl4vQ_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rl4vRPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rl4vRfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rl4vRvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rl4vR_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rl4vSPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rl4vSfemEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rl4vSvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rl4vS_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rl4vTPemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rl4vTfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rl4vTvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rl4vT_emEeiL3uw9NeuELg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rl4vUPemEeiL3uw9NeuELg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rmB5MPemEeiL3uw9NeuELg" name="PK_ref_Propriété">
        <node defType="com.stambia.rdbms.colref" id="_rmB5MfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rmB5MvemEeiL3uw9NeuELg" ref="#_rl4vRPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rmU0JvemEeiL3uw9NeuELg" name="Client_MoisOuvert">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rmU0J_emEeiL3uw9NeuELg" value="Client_MoisOuvert"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rmU0KPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rmU0KfemEeiL3uw9NeuELg" name="IdClient" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rmU0KvemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rmU0K_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rmU0LPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rmU0LfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rmU0LvemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rmU0L_emEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rmU0MPemEeiL3uw9NeuELg" name="IdMois" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rmU0MfemEeiL3uw9NeuELg" value="IdMois"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rmU0MvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rmU0M_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rmU0NPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rmU0NfemEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rmU0NvemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rmelIPemEeiL3uw9NeuELg" name="PK_Client_MoisOuvert">
        <node defType="com.stambia.rdbms.colref" id="_rmelIfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rmelIvemEeiL3uw9NeuELg" ref="#_rmU0KfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
        </node>
        <node defType="com.stambia.rdbms.colref" id="_rmelI_emEeiL3uw9NeuELg" position="2">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rmelJPemEeiL3uw9NeuELg" ref="#_rmU0MPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdMois?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtVbGPemEeiL3uw9NeuELg" name="FK_Client_MoisOuvert_Client">
        <node defType="com.stambia.rdbms.relation" id="_rtVbGfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtVbGvemEeiL3uw9NeuELg" ref="#_rmU0KfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtVbG_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtVbHPemEeiL3uw9NeuELg" name="FK_Client_MoisOuvert_ref_Mois1">
        <node defType="com.stambia.rdbms.relation" id="_rtVbHfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtVbHvemEeiL3uw9NeuELg" ref="#_rmU0MPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdMois?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtVbH_emEeiL3uw9NeuELg" ref="#_rYB5d_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rWY6tvemEeiL3uw9NeuELg" name="Client_Activite">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rWY6t_emEeiL3uw9NeuELg" value="Client_Activite"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rWY6uPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rWirsPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWirsfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWirsvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rWirs_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWirtPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWirtfemEeiL3uw9NeuELg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWirtvemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWirt_emEeiL3uw9NeuELg" name="IdClient" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWiruPemEeiL3uw9NeuELg" value="IdClient"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWirufemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rWiruvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWiru_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWirvPemEeiL3uw9NeuELg" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWirvfemEeiL3uw9NeuELg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWirvvemEeiL3uw9NeuELg" name="ActifMoisClosN" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWirv_emEeiL3uw9NeuELg" value="ActifMoisClosN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWirwPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWirwfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWirwvemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWirw_emEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWirxPemEeiL3uw9NeuELg" name="ActifMoisClosN_1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWirxfemEeiL3uw9NeuELg" value="ActifMoisClosN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWirxvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWirx_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWiryPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWiryfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWiryvemEeiL3uw9NeuELg" name="Actif12DM" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWiry_emEeiL3uw9NeuELg" value="Actif12DM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWirzPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWirzfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWirzvemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWirz_emEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWir0PemEeiL3uw9NeuELg" name="Actif13_24DM" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWir0femEeiL3uw9NeuELg" value="Actif13_24DM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWir0vemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWir0_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWir1PemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWir1femEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWr1oPemEeiL3uw9NeuELg" name="ActifN" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWr1ofemEeiL3uw9NeuELg" value="ActifN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWr1ovemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWr1o_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWr1pPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWr1pfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWr1pvemEeiL3uw9NeuELg" name="ActifPeriodeN_1" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWr1p_emEeiL3uw9NeuELg" value="ActifPeriodeN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWr1qPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWr1qfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWr1qvemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWr1q_emEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWr1rPemEeiL3uw9NeuELg" name="ActifN_1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWr1rfemEeiL3uw9NeuELg" value="ActifN_1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWr1rvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWr1r_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWr1sPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWr1sfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWr1svemEeiL3uw9NeuELg" name="ActifN_2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWr1s_emEeiL3uw9NeuELg" value="ActifN_2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWr1tPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWr1tfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWr1tvemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWr1t_emEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rWr1uPemEeiL3uw9NeuELg" name="ActifN_3" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_rWr1ufemEeiL3uw9NeuELg" value="ActifN_3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rWr1uvemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rWr1u_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rWr1vPemEeiL3uw9NeuELg" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rWr1vfemEeiL3uw9NeuELg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rW1moPemEeiL3uw9NeuELg" name="PK_Client_Activite">
        <node defType="com.stambia.rdbms.colref" id="_rW1mofemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rW1movemEeiL3uw9NeuELg" ref="#_rWirsPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rrirWPemEeiL3uw9NeuELg" name="FK_Client_Activite_Client">
        <node defType="com.stambia.rdbms.relation" id="_rrirWfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rrirWvemEeiL3uw9NeuELg" ref="#_rWirt_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdClient?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rrirW_emEeiL3uw9NeuELg" ref="#_rYnvUPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rcWYyvemEeiL3uw9NeuELg" name="ref_TicketRepasMoyen">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rcWYy_emEeiL3uw9NeuELg" value="ref_TicketRepasMoyen"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rcWYzPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rcWYzfemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcWYzvemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcfisPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rcfisfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcfisvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcfis_emEeiL3uw9NeuELg" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcfitPemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rcfitfemEeiL3uw9NeuELg" name="TicketMoy" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcfitvemEeiL3uw9NeuELg" value="TicketMoy"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcfit_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcfiuPemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcfiufemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcfiuvemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rcfiu_emEeiL3uw9NeuELg" name="MntMin" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcfivPemEeiL3uw9NeuELg" value="MntMin"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcfivfemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rcfivvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcfiv_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcfiwPemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcfiwfemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rcfiwvemEeiL3uw9NeuELg" name="MntMax" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_rcfiw_emEeiL3uw9NeuELg" value="MntMax"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rcfixPemEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rcfixfemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rcfixvemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rcfix_emEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rcfiyPemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rcfiyfemEeiL3uw9NeuELg" name="PK_ref_TicketRepasMoyen">
        <node defType="com.stambia.rdbms.colref" id="_rcfiyvemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rcfiy_emEeiL3uw9NeuELg" ref="#_rcWYzfemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_riKF0femEeiL3uw9NeuELg" name="ref_RepasServis">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_riKF0vemEeiL3uw9NeuELg" value="ref_RepasServis"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_riKF0_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_riKF1PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_riKF1femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_riKF1vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_riKF1_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_riKF2PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_riKF2femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_riKF2vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_riKF2_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_riKF3PemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_riKF3femEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_riKF3vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_riKF3_emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_riKF4PemEeiL3uw9NeuELg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_riKF4femEeiL3uw9NeuELg" name="Code" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_riKF4vemEeiL3uw9NeuELg" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_riKF4_emEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_riKF5PemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_riKF5femEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_riKF5vemEeiL3uw9NeuELg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_riT20PemEeiL3uw9NeuELg" name="PK_ref_RepasServi">
        <node defType="com.stambia.rdbms.colref" id="_riT20femEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_riT20vemEeiL3uw9NeuELg" ref="#_riKF1PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rkZhhvemEeiL3uw9NeuELg" name="ref_ActiviteSegment">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rkZhh_emEeiL3uw9NeuELg" value="ref_ActiviteSegment"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rkZhiPemEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rkircPemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rkircfemEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rkircvemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rkirc_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rkirdPemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rkirdfemEeiL3uw9NeuELg" value="smallint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rkirdvemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rkird_emEeiL3uw9NeuELg" name="IdSegment" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rkirePemEeiL3uw9NeuELg" value="IdSegment"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rkirefemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rkirevemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rkire_emEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rkirfPemEeiL3uw9NeuELg" value="smallint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rkirffemEeiL3uw9NeuELg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rkirfvemEeiL3uw9NeuELg" name="Libelle" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_rkirf_emEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rkirgPemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rkirgfemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rkirgvemEeiL3uw9NeuELg" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rkirg_emEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rksccPemEeiL3uw9NeuELg" name="PK_ref_ActiviteSegment">
        <node defType="com.stambia.rdbms.colref" id="_rksccfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rksccvemEeiL3uw9NeuELg" ref="#_rkircPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
      <node defType="com.stambia.rdbms.fk" id="_rtLqGPemEeiL3uw9NeuELg" name="FK_ref_ActiviteSegment_ref_Segment">
        <node defType="com.stambia.rdbms.relation" id="_rtLqGfemEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.relation.fk" id="_rtLqGvemEeiL3uw9NeuELg" ref="#_rkird_emEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=IdSegment?"/>
          <attribute defType="com.stambia.rdbms.relation.pk" id="_rtLqG_emEeiL3uw9NeuELg" ref="#_rmoWIPemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_rnhG8femEeiL3uw9NeuELg" name="ref_TypeCompte">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_rnhG8vemEeiL3uw9NeuELg" value="ref_TypeCompte"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_rnhG8_emEeiL3uw9NeuELg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_rnhG9PemEeiL3uw9NeuELg" name="Id" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnhG9femEeiL3uw9NeuELg" value="Id"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnhG9vemEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_rnhG9_emEeiL3uw9NeuELg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnhG-PemEeiL3uw9NeuELg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnhG-femEeiL3uw9NeuELg" value="tinyint identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnhG-vemEeiL3uw9NeuELg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_rnhG-_emEeiL3uw9NeuELg" name="Libelle" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_rnhG_PemEeiL3uw9NeuELg" value="Libelle"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_rnhG_femEeiL3uw9NeuELg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_rnhG_vemEeiL3uw9NeuELg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_rnhG__emEeiL3uw9NeuELg" value="nchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_rnhHAPemEeiL3uw9NeuELg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_rnq38PemEeiL3uw9NeuELg" name="PK_ref_TypeCompte">
        <node defType="com.stambia.rdbms.colref" id="_rnq38femEeiL3uw9NeuELg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_rnq38vemEeiL3uw9NeuELg" ref="#_rnhG9PemEeiL3uw9NeuELg?fileId=_14k8gPelEeiL3uw9NeuELg$type=md$name=Id?"/>
        </node>
      </node>
    </node>
  </node>
</md:node>