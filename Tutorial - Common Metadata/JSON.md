<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_if-PYP1TEeirUNs87YKmbQ" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_ilLfgP1TEeirUNs87YKmbQ" name="Societe">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_ioAxEP1TEeirUNs87YKmbQ"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_ioAxEf1TEeirUNs87YKmbQ" value="C:\app_dev\Used_Files\In_Files\Json\Societe.json"/>
    <attribute defType="com.stambia.json.rootObject.filePath" id="_NFL-Mv4WEeiVrKKMla1zGg" value="\\servernt46\Stambia_Test\Chomette.com\Export\Sociétés\Encours\*.json"/>
    <node defType="com.stambia.json.value" id="_9ywrKf1TEeirUNs87YKmbQ" name="rue1" position="5">
      <attribute defType="com.stambia.json.value.type" id="_9ywrKv1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrL_1TEeirUNs87YKmbQ" name="ville" position="8">
      <attribute defType="com.stambia.json.value.type" id="_9ywrMP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrLf1TEeirUNs87YKmbQ" name="code_postal" position="7">
      <attribute defType="com.stambia.json.value.type" id="_9ywrLv1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.object" id="_9ywrYP1TEeirUNs87YKmbQ" name="activite" position="25">
      <node defType="com.stambia.json.value" id="_PScb__7hEeiVrKKMla1zGg" name="specialite_culinaire" position="6">
        <attribute defType="com.stambia.json.value.type" id="_PSccAP7hEeiVrKKMla1zGg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmkQQ2EemZAt8vLIIhpA" name="code_client" position="1">
        <attribute defType="com.stambia.json.value.type" id="_GEXmkgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmkwQ2EemZAt8vLIIhpA" name="tva_intracomm" position="2">
        <attribute defType="com.stambia.json.value.type" id="_GEXmlAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmlQQ2EemZAt8vLIIhpA" name="type_etab_1" position="3">
        <attribute defType="com.stambia.json.value.type" id="_GEXmlgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmlwQ2EemZAt8vLIIhpA" name="type_etab_2" position="4">
        <attribute defType="com.stambia.json.value.type" id="_GEXmmAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmmQQ2EemZAt8vLIIhpA" name="type_etab_3" position="5">
        <attribute defType="com.stambia.json.value.type" id="_GEXmmgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmnQQ2EemZAt8vLIIhpA" name="niveau_standing" position="7">
        <attribute defType="com.stambia.json.value.type" id="_GEXmngQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmnwQ2EemZAt8vLIIhpA" name="repas_servis" position="8">
        <attribute defType="com.stambia.json.value.type" id="_GEXmoAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmoQQ2EemZAt8vLIIhpA" name="prestations" position="9">
        <attribute defType="com.stambia.json.value.type" id="_GEXmogQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmowQ2EemZAt8vLIIhpA" name="langue_principale" position="10">
        <attribute defType="com.stambia.json.value.type" id="_GEXmpAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmpQQ2EemZAt8vLIIhpA" name="nb_couverts_jour" position="11">
        <attribute defType="com.stambia.json.value.type" id="_GEXmpgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmpwQ2EemZAt8vLIIhpA" name="nb_repas_jour" position="12">
        <attribute defType="com.stambia.json.value.type" id="_GEXmqAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmqQQ2EemZAt8vLIIhpA" name="nb_couchages" position="13">
        <attribute defType="com.stambia.json.value.type" id="_GEXmqgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmqwQ2EemZAt8vLIIhpA" name="nb_lits" position="14">
        <attribute defType="com.stambia.json.value.type" id="_GEXmrAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmrQQ2EemZAt8vLIIhpA" name="nb_eleves" position="15">
        <attribute defType="com.stambia.json.value.type" id="_GEXmrgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmrwQ2EemZAt8vLIIhpA" name="type_compte" position="16">
        <attribute defType="com.stambia.json.value.type" id="_GEXmsAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmsQQ2EemZAt8vLIIhpA" name="num_adherent" position="17">
        <attribute defType="com.stambia.json.value.type" id="_GEXmsgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmswQ2EemZAt8vLIIhpA" name="env_tourist" position="18">
        <attribute defType="com.stambia.json.value.type" id="_GEXmtAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmtQQ2EemZAt8vLIIhpA" name="annees_existence" position="19">
        <attribute defType="com.stambia.json.value.type" id="_GEXmtgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmtwQ2EemZAt8vLIIhpA" name="scoring_ca" position="20">
        <attribute defType="com.stambia.json.value.type" id="_GEXmuAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmuQQ2EemZAt8vLIIhpA" name="statut_client" position="21">
        <attribute defType="com.stambia.json.value.type" id="_GEXmugQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmuwQ2EemZAt8vLIIhpA" name="niveau_fidelite" position="22">
        <attribute defType="com.stambia.json.value.type" id="_GEXmvAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmvQQ2EemZAt8vLIIhpA" name="comportement" position="23">
        <attribute defType="com.stambia.json.value.type" id="_GEXmvgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmvwQ2EemZAt8vLIIhpA" name="demat_factures" position="24">
        <attribute defType="com.stambia.json.value.type" id="_GEXmwAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmwQQ2EemZAt8vLIIhpA" name="email_fact_demat" position="25">
        <attribute defType="com.stambia.json.value.type" id="_GEXmwgQ2EemZAt8vLIIhpA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_GEXmwwQ2EemZAt8vLIIhpA" name="jours_d_ouverture" position="26">
        <attribute defType="com.stambia.json.value.type" id="_GEXmxAQ2EemZAt8vLIIhpA" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrN_1TEeirUNs87YKmbQ" name="siret" position="12">
      <attribute defType="com.stambia.json.value.type" id="_9ywrOP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrRf1TEeirUNs87YKmbQ" name="minimum_commande" position="18">
      <attribute defType="com.stambia.json.value.type" id="_9ywrRv1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrQf1TEeirUNs87YKmbQ" name="mode_paiement" position="16">
      <attribute defType="com.stambia.json.value.type" id="_9ywrQv1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrOf1TEeirUNs87YKmbQ" name="mercuriale" position="13">
      <attribute defType="com.stambia.json.value.type" id="_9ywrOv1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrR_1TEeirUNs87YKmbQ" name="commercial" position="19">
      <attribute defType="com.stambia.json.value.type" id="_9ywrSP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrM_1TEeirUNs87YKmbQ" name="tel" position="10">
      <attribute defType="com.stambia.json.value.type" id="_9ywrNP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrO_1TEeirUNs87YKmbQ" name="tarif" position="14">
      <attribute defType="com.stambia.json.value.type" id="_9ywrPP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrI_1TEeirUNs87YKmbQ" name="id_client_chomette" position="2">
      <attribute defType="com.stambia.json.value.type" id="_9ywrJP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrNf1TEeirUNs87YKmbQ" name="fax" position="11">
      <attribute defType="com.stambia.json.value.type" id="_9ywrNv1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrQ_1TEeirUNs87YKmbQ" name="frais_de_port" position="17">
      <attribute defType="com.stambia.json.value.type" id="_9ywrRP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrP_1TEeirUNs87YKmbQ" name="couleur" position="15">
      <attribute defType="com.stambia.json.value.type" id="_9ywrQP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrJf1TEeirUNs87YKmbQ" name="raison_sociale" position="3">
      <attribute defType="com.stambia.json.value.type" id="_9ywrJv1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrIf1TEeirUNs87YKmbQ" name="id_societe_magento" position="1">
      <attribute defType="com.stambia.json.value.type" id="_9ywrIv1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrJ_1TEeirUNs87YKmbQ" name="enseigne" position="4">
      <attribute defType="com.stambia.json.value.type" id="_9ywrKP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrK_1TEeirUNs87YKmbQ" name="rue2" position="6">
      <attribute defType="com.stambia.json.value.type" id="_9ywrLP1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.json.object" id="_9ywrS_1TEeirUNs87YKmbQ" name="livraison" position="24">
      <node defType="com.stambia.json.value" id="_9ywrTP1TEeirUNs87YKmbQ" name="raison_sociale" position="1">
        <attribute defType="com.stambia.json.value.type" id="_9ywrTf1TEeirUNs87YKmbQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9ywrTv1TEeirUNs87YKmbQ" name="enseigne" position="2">
        <attribute defType="com.stambia.json.value.type" id="_9ywrT_1TEeirUNs87YKmbQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9ywrUP1TEeirUNs87YKmbQ" name="rue1" position="3">
        <attribute defType="com.stambia.json.value.type" id="_9ywrUf1TEeirUNs87YKmbQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9ywrUv1TEeirUNs87YKmbQ" name="rue2" position="4">
        <attribute defType="com.stambia.json.value.type" id="_9ywrU_1TEeirUNs87YKmbQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9ywrVP1TEeirUNs87YKmbQ" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_9ywrVf1TEeirUNs87YKmbQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9ywrVv1TEeirUNs87YKmbQ" name="ville" position="6">
        <attribute defType="com.stambia.json.value.type" id="_9ywrV_1TEeirUNs87YKmbQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9ywrWP1TEeirUNs87YKmbQ" name="pays" position="7">
        <attribute defType="com.stambia.json.value.type" id="_9ywrWf1TEeirUNs87YKmbQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9ywrWv1TEeirUNs87YKmbQ" name="jours_d_ouverture" position="8">
        <attribute defType="com.stambia.json.value.type" id="_9ywrW_1TEeirUNs87YKmbQ" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.value" id="_9ywrMf1TEeirUNs87YKmbQ" name="pays" position="9">
      <attribute defType="com.stambia.json.value.type" id="_9ywrMv1TEeirUNs87YKmbQ" value="string"/>
    </node>
    <node defType="com.stambia.xml.propertyField" id="_9_-vM_79EeiVrKKMla1zGg" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_AoebcP7-EeiVrKKMla1zGg" value="fileName"/>
    </node>
    <node defType="com.stambia.json.value" id="_GEXmeQQ2EemZAt8vLIIhpA" name="Email_admin" position="21">
      <attribute defType="com.stambia.json.value.type" id="_GEXmegQ2EemZAt8vLIIhpA" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_GEXmdwQ2EemZAt8vLIIhpA" name="Type_livraison" position="20">
      <attribute defType="com.stambia.json.value.type" id="_GEXmeAQ2EemZAt8vLIIhpA" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_GEXmfQQ2EemZAt8vLIIhpA" name="Lastname" position="23">
      <attribute defType="com.stambia.json.value.type" id="_GEXmfgQ2EemZAt8vLIIhpA" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_GEXmewQ2EemZAt8vLIIhpA" name="Firstname" position="22">
      <attribute defType="com.stambia.json.value.type" id="_GEXmfAQ2EemZAt8vLIIhpA" value="string"/>
    </node>
  </node>
</md:node>