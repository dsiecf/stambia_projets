<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.variable.set" id="_Ae1owA7OEeuGaoQ5wj6kWA" md:ref="platform:/plugin/com.indy.environment/technology/others/variable.tech#UUID_TECH_VARIABLE?fileId=UUID_TECH_VARIABLE$type=tech$name=Variable?">
  <node defType="com.stambia.variable.variable" id="_A_rE8A7OEeuGaoQ5wj6kWA" name="VarFileDate">
    <attribute defType="com.stambia.variable.variable.defaultValue" id="_H30BcA7OEeuGaoQ5wj6kWA" value="%x{md:formatDate('yyyyMMdd-HHmmss.SSS')}x%"/>
    <attribute defType="com.stambia.variable.variable.nativeXpathExpression" id="_H4nSsA7OEeuGaoQ5wj6kWA" value="true"/>
  </node>
</md:node>