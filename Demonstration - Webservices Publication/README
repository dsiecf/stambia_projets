-------------------------------------
Stambia WebServices Publication Demonstration Project
-------------------------------------

Welcome to the Stambia's Web Services Publication demonstration project.
This project contains several processes that you will be able to publish as webservices.

Prerequisites
-------------
    - Stambia DI Designer S18.3.0 or higher
	- Stambia DI Runtime S17.4.0 or higher

Setup
-------------
Simply import into the global project the Generic templates.
	  

Execution
-------------
1-	Start your Runtime and connect it to the Designer.
2-	Start demo Databases.
3-	Go into the '/02 - Examples/' folder and execute the "00 - Prepare Demonstration/Create Sample files" Process.
	This will create all the required files that will be used by the samples.
4-  Test the examples in any order.


Testing a Web Service Example
-------------
Each example has the following structure

XX - Example Name
	01-	Web Service to publish
		-> Folder containing the examples of Web Services 
	02-	Web Service Resources
		-> Optional folder containing the resources that the Web Services might use, such as Mappings, if any.
		
To test a Web Service:
1-	Publication of the Web Service
	You first have to publish the Web Service you want to test:
		a-	Lead to the "01- Web Service to publish" folder of the Example you want to test
		b-	Right click on the Web Service Process and choose 'Publish As Webservice'
		->	The Web Service is now published on the Runtime and can be invoked through REST and SOAP

2-	Invocation of the Web Service
	a-  Invocation through the REST API
		Simply open your favorite browser or a software such as POSTMAN that will allow to perform HTTP requests
		Use the following URL, by adapting it to the Example:
		http://<hostname>:42200/rest/StambiaDeliveryService/2/<process name>?param1=value1&param2=value2
	
	b-	Invocation through the SOAP API
		Use a software such as SoapUI to perform the invocation.
		The URL to the WSDL descriptor containing the Web Services can be accessed from the Runtime console:
		http://<yourhostname>:42200/wsi/DeliverableService?WSDL
		Use this URL in your software to reverse the available Web Services.

The process "00 - Prepare Demonstration/Create Sample files" will create XML, JSON and Text files in your Stambia Runtime Temporary folder.
Those files contain samples that can be used to test the Web Services.

Most of the examples contain notes to explain their purposes.
