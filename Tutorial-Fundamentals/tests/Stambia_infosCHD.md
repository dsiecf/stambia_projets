<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_1tI3EAYsEeq4kNhKU9q4kQ" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootArray" id="_1taj4AYsEeq4kNhKU9q4kQ" name="chdinfos">
    <attribute defType="com.stambia.json.rootArray.encoding" id="_1tbK8AYsEeq4kNhKU9q4kQ" value="UTF-8"/>
    <attribute defType="com.stambia.json.rootArray.reverseURLPath" id="_1tbK8QYsEeq4kNhKU9q4kQ"/>
    <attribute defType="com.stambia.json.rootArray.reverseFilePath" id="_1tbK8gYsEeq4kNhKU9q4kQ" value="C:\app_dev\Used_Files\In_Files\Json\chdinfos.json"/>
    <attribute defType="com.stambia.json.rootArray.filePath" id="_1tbK8wYsEeq4kNhKU9q4kQ" value="C:\Users\amatysiak\Downloads\CHDtemp_file_20191113_151152.json"/>
    <node defType="com.stambia.json.object" id="_1tbK9AYsEeq4kNhKU9q4kQ" name="item" position="1">
      <node defType="com.stambia.json.value" id="_1tbK9QYsEeq4kNhKU9q4kQ" name="chd_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_1tbK9gYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tbyAAYsEeq4kNhKU9q4kQ" name="dba_name" position="2">
        <attribute defType="com.stambia.json.value.type" id="_1tbyAQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tbyAgYsEeq4kNhKU9q4kQ" name="legal_name" position="3">
        <attribute defType="com.stambia.json.value.type" id="_1tbyAwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tbyBAYsEeq4kNhKU9q4kQ" name="address" position="4">
        <attribute defType="com.stambia.json.value.type" id="_1tbyBQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tbyBgYsEeq4kNhKU9q4kQ" name="address2" position="5">
        <attribute defType="com.stambia.json.value.type" id="_1tbyBwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tbyCAYsEeq4kNhKU9q4kQ" name="postal_code" position="6">
        <attribute defType="com.stambia.json.value.type" id="_1tbyCQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tbyCgYsEeq4kNhKU9q4kQ" name="city" position="7">
        <attribute defType="com.stambia.json.value.type" id="_1tbyCwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tbyDAYsEeq4kNhKU9q4kQ" name="phone" position="8">
        <attribute defType="com.stambia.json.value.type" id="_1tbyDQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZEAYsEeq4kNhKU9q4kQ" name="fax" position="9">
        <attribute defType="com.stambia.json.value.type" id="_1tcZEQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZEgYsEeq4kNhKU9q4kQ" name="legal_number1" position="10">
        <attribute defType="com.stambia.json.value.type" id="_1tcZEwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZFAYsEeq4kNhKU9q4kQ" name="website" position="11">
        <attribute defType="com.stambia.json.value.type" id="_1tcZFQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZFgYsEeq4kNhKU9q4kQ" name="chain_name" position="12">
        <attribute defType="com.stambia.json.value.type" id="_1tcZFwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZGAYsEeq4kNhKU9q4kQ" name="organisation_type" position="13">
        <attribute defType="com.stambia.json.value.type" id="_1tcZGQYsEeq4kNhKU9q4kQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZGgYsEeq4kNhKU9q4kQ" name="operation_type" position="14">
        <attribute defType="com.stambia.json.value.type" id="_1tcZGwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZHAYsEeq4kNhKU9q4kQ" name="market_segment" position="15">
        <attribute defType="com.stambia.json.value.type" id="_1tcZHQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZHgYsEeq4kNhKU9q4kQ" name="menu_type" position="16">
        <attribute defType="com.stambia.json.value.type" id="_1tcZHwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZIAYsEeq4kNhKU9q4kQ" name="number_of_rooms" position="17">
        <attribute defType="com.stambia.json.value.type" id="_1tcZIQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZIgYsEeq4kNhKU9q4kQ" name="number_of_rooms_range" position="18">
        <attribute defType="com.stambia.json.value.type" id="_1tcZIwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZJAYsEeq4kNhKU9q4kQ" name="annual_sales_range" position="19">
        <attribute defType="com.stambia.json.value.type" id="_1tcZJQYsEeq4kNhKU9q4kQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZJgYsEeq4kNhKU9q4kQ" name="confidence_level" position="20">
        <attribute defType="com.stambia.json.value.type" id="_1tcZJwYsEeq4kNhKU9q4kQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZKAYsEeq4kNhKU9q4kQ" name="average_check_range" position="21">
        <attribute defType="com.stambia.json.value.type" id="_1tcZKQYsEeq4kNhKU9q4kQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tcZKgYsEeq4kNhKU9q4kQ" name="years_in_business_range" position="22">
        <attribute defType="com.stambia.json.value.type" id="_1tcZKwYsEeq4kNhKU9q4kQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdAIAYsEeq4kNhKU9q4kQ" name="number_of_employees_range" position="23">
        <attribute defType="com.stambia.json.value.type" id="_1tdAIQYsEeq4kNhKU9q4kQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdAIgYsEeq4kNhKU9q4kQ" name="number_of_pieces_of_bred_range" position="24">
        <attribute defType="com.stambia.json.value.type" id="_1tdAIwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdAJAYsEeq4kNhKU9q4kQ" name="ownership" position="25">
        <attribute defType="com.stambia.json.value.type" id="_1tdAJQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdAJgYsEeq4kNhKU9q4kQ" name="number_of_meals_range" position="26">
        <attribute defType="com.stambia.json.value.type" id="_1tdAJwYsEeq4kNhKU9q4kQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdAKAYsEeq4kNhKU9q4kQ" name="number_of_beds_range" position="27">
        <attribute defType="com.stambia.json.value.type" id="_1tdAKQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdAKgYsEeq4kNhKU9q4kQ" name="number_of_students_range" position="28">
        <attribute defType="com.stambia.json.value.type" id="_1tdAKwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdALAYsEeq4kNhKU9q4kQ" name="closed_date" position="29">
        <attribute defType="com.stambia.json.value.type" id="_1tdALQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdALgYsEeq4kNhKU9q4kQ" name="open_date" position="30">
        <attribute defType="com.stambia.json.value.type" id="_1tdALwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdAMAYsEeq4kNhKU9q4kQ" name="contact_name" position="31">
        <attribute defType="com.stambia.json.value.type" id="_1tdAMQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdAMgYsEeq4kNhKU9q4kQ" name="latitude" position="32">
        <attribute defType="com.stambia.json.value.type" id="_1tdAMwYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdANAYsEeq4kNhKU9q4kQ" name="longitude" position="33">
        <attribute defType="com.stambia.json.value.type" id="_1tdANQYsEeq4kNhKU9q4kQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1tdANgYsEeq4kNhKU9q4kQ" name="terrace" position="34">
        <attribute defType="com.stambia.json.value.type" id="_1tdANwYsEeq4kNhKU9q4kQ" value="boolean"/>
      </node>
      <node defType="com.stambia.json.object" id="_1tdAOAYsEeq4kNhKU9q4kQ" name="hours" position="35">
        <node defType="com.stambia.json.array" id="_1tdAOQYsEeq4kNhKU9q4kQ" name="fri" position="1">
          <node defType="com.stambia.json.object" id="_1tdAOgYsEeq4kNhKU9q4kQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_1tdAOwYsEeq4kNhKU9q4kQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_1tdnMAYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1tdnMQYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1tdnMgYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1tdnMwYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_1tdnNAYsEeq4kNhKU9q4kQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_1tdnNQYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1tdnNgYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1tdnNwYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1tdnOAYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.xml.propertyField" id="_1tdnOQYsEeq4kNhKU9q4kQ" name="OccurenceF">
              <attribute defType="com.stambia.xml.propertyField.property" id="_1tdnOgYsEeq4kNhKU9q4kQ" value="nodeLocalPosition"/>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_1tdnOwYsEeq4kNhKU9q4kQ" name="mon" position="2">
          <node defType="com.stambia.json.object" id="_1tdnPAYsEeq4kNhKU9q4kQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_1tdnPQYsEeq4kNhKU9q4kQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_1tdnPgYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1tdnPwYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1tdnQAYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1tdnQQYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_1tdnQgYsEeq4kNhKU9q4kQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_1teOQAYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1teOQQYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1teOQgYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1teOQwYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_1teORAYsEeq4kNhKU9q4kQ" name="sat" position="3">
          <node defType="com.stambia.json.object" id="_1teORQYsEeq4kNhKU9q4kQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_1teORgYsEeq4kNhKU9q4kQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_1teORwYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1teOSAYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1teOSQYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1teOSgYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_1teOSwYsEeq4kNhKU9q4kQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_1teOTAYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1teOTQYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1teOTgYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1teOTwYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_1teOUAYsEeq4kNhKU9q4kQ" name="sun" position="4">
          <node defType="com.stambia.json.object" id="_1te1UAYsEeq4kNhKU9q4kQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_1te1UQYsEeq4kNhKU9q4kQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_1te1UgYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1te1UwYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1te1VAYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1te1VQYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_1te1VgYsEeq4kNhKU9q4kQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_1te1VwYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1te1WAYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1te1WQYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1te1WgYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_1te1WwYsEeq4kNhKU9q4kQ" name="thu" position="5">
          <node defType="com.stambia.json.object" id="_1te1XAYsEeq4kNhKU9q4kQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_1te1XQYsEeq4kNhKU9q4kQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_1te1XgYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1te1XwYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1te1YAYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1te1YQYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_1te1YgYsEeq4kNhKU9q4kQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_1te1YwYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1te1ZAYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1te1ZQYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1tfcYAYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.xml.propertyField" id="_1tfcYQYsEeq4kNhKU9q4kQ" name="OccurenceTh">
              <attribute defType="com.stambia.xml.propertyField.property" id="_1tfcYgYsEeq4kNhKU9q4kQ" value="nodeLocalPosition"/>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_1tfcYwYsEeq4kNhKU9q4kQ" name="tue" position="6">
          <node defType="com.stambia.json.object" id="_1tfcZAYsEeq4kNhKU9q4kQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_1tfcZQYsEeq4kNhKU9q4kQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_1tfcZgYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1tfcZwYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1tfcaAYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1tfcaQYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_1tfcagYsEeq4kNhKU9q4kQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_1tfcawYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1tfcbAYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1tfcbQYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1tfcbgYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_1tfcbwYsEeq4kNhKU9q4kQ" name="wed" position="7">
          <node defType="com.stambia.json.object" id="_1tfccAYsEeq4kNhKU9q4kQ" name="item" position="1">
            <node defType="com.stambia.json.object" id="_1tfccQYsEeq4kNhKU9q4kQ" name="open" position="1">
              <node defType="com.stambia.json.value" id="_1tfccgYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1tfccwYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1tfcdAYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1tfcdQYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
            <node defType="com.stambia.json.object" id="_1tfcdgYsEeq4kNhKU9q4kQ" name="close" position="2">
              <node defType="com.stambia.json.value" id="_1tfcdwYsEeq4kNhKU9q4kQ" name="hour" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1tfceAYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
              <node defType="com.stambia.json.value" id="_1tfceQYsEeq4kNhKU9q4kQ" name="minute" position="2">
                <attribute defType="com.stambia.json.value.type" id="_1tfcegYsEeq4kNhKU9q4kQ" value="number"/>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node defType="com.stambia.xml.propertyField" id="_1tfcewYsEeq4kNhKU9q4kQ" name="file_path"/>
    <node defType="com.stambia.xml.propertyField" id="_1tfcfAYsEeq4kNhKU9q4kQ" name="string_content"/>
    <node defType="com.stambia.xml.propertyField" id="_1tfcfQYsEeq4kNhKU9q4kQ" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_1tgDcAYsEeq4kNhKU9q4kQ" value="fileName"/>
    </node>
  </node>
</md:node>