<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_Jm8UEAYiEeq4kNhKU9q4kQ" name="testHSQLDemo" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/hsql/hsql.rdbms.md#UUID_MD_RDBMS_HYPERSONIC_SQL?fileId=UUID_MD_RDBMS_HYPERSONIC_SQL$type=md$name=Hypersonic%20SQL?">
  <attribute defType="com.stambia.rdbms.server.url" id="_L6RBkAYiEeq4kNhKU9q4kQ" value="jdbc:hsqldb:hsql://localhost:62210"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_L6cAsAYiEeq4kNhKU9q4kQ" value="org.hsqldb.jdbcDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_L6cnwAYiEeq4kNhKU9q4kQ" value="sa"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_L6cnwQYiEeq4kNhKU9q4kQ" value="3951C0D79B227B95C1DC348DD0BCE8F1"/>
  <node defType="com.stambia.rdbms.schema" id="_J14vAAYiEeq4kNhKU9q4kQ" name="HOTEL_MANAGEMENT">
    <attribute defType="com.stambia.rdbms.schema.name" id="_J2-UIAYiEeq4kNhKU9q4kQ" value="HOTEL_MANAGEMENT"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_J2-7MAYiEeq4kNhKU9q4kQ" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_J2_iQAYiEeq4kNhKU9q4kQ" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_J2_iQQYiEeq4kNhKU9q4kQ" value="I_[targetName]"/>
  </node>
</md:node>