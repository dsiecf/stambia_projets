<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_gsJ5kKdEEeuR0e9NvyHj0g" name="Bdd_export" md:ref="platform:/plugin/com.indy.environment/technology/rdbms/excel/excel.rdbms.md#UUID_MD_RDBMS_MICROSOFT_EXCEL?fileId=UUID_MD_RDBMS_MICROSOFT_EXCEL$type=md$name=Microsoft%20Excel?">
  <attribute defType="com.stambia.rdbms.server.url" id="_vJ2SMKdEEeuR0e9NvyHj0g" value="jdbc:stambia:excel://\\servernt46\stambia\Bdd\Bdd_Extract.xlsx?forceDatatypeAsString=true&amp;columnNameStyle=PRESERVE"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_vJ4HYKdEEeuR0e9NvyHj0g" value="com.stambia.jdbc.driver.excel.XLSXDriver"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_vJ5VgKdEEeuR0e9NvyHj0g" value="3951C0D79B227B95C1DC348DD0BCE8F1"/>
  <node defType="com.stambia.rdbms.schema" id="_g6jh8KdEEeuR0e9NvyHj0g" name="Bdd_Extract">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_g6yygKdEEeuR0e9NvyHj0g" value="Bdd_Extract"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_g6zZkKdEEeuR0e9NvyHj0g" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_g6zZkadEEeuR0e9NvyHj0g" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_g6zZkqdEEeuR0e9NvyHj0g" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_vI7sMadEEeuR0e9NvyHj0g" name="Bdd_Extract$">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_vI8TQKdEEeuR0e9NvyHj0g" value="Bdd_Extract$"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_vI86UKdEEeuR0e9NvyHj0g" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_vJMx8KdEEeuR0e9NvyHj0g" name="Code" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJMx8adEEeuR0e9NvyHj0g" value="Code"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJMx8qdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJNZAKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJNZAadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRDYKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRDYadEEeuR0e9NvyHj0g" name="IdSociete" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRDYqdEEeuR0e9NvyHj0g" value="IdSociete"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRDY6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRDZKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRDZadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRDZqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRDZ6dEEeuR0e9NvyHj0g" name="CodeSocMagento" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRDaKdEEeuR0e9NvyHj0g" value="CodeSocMagento"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRDaadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRDaqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRDa6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRDbKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRDbadEEeuR0e9NvyHj0g" name="RaisonSociale" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRDbqdEEeuR0e9NvyHj0g" value="RaisonSociale"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRDb6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRDcKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRDcadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRDcqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRDc6dEEeuR0e9NvyHj0g" name="Enseigne" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRDdKdEEeuR0e9NvyHj0g" value="Enseigne"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRDdadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRDdqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRDd6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRDeKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRDeadEEeuR0e9NvyHj0g" name="Siren" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRDeqdEEeuR0e9NvyHj0g" value="Siren"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRDe6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRDfKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqcKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqcadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqcqdEEeuR0e9NvyHj0g" name="Siret" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqc6dEEeuR0e9NvyHj0g" value="Siret"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqdKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqdadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqdqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqd6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqeKdEEeuR0e9NvyHj0g" name="Nic" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqeadEEeuR0e9NvyHj0g" value="Nic"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqeqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqe6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqfKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqfadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqfqdEEeuR0e9NvyHj0g" name="Tva" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqf6dEEeuR0e9NvyHj0g" value="Tva"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqgKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqgadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqgqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqg6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqhKdEEeuR0e9NvyHj0g" name="Naf" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqhadEEeuR0e9NvyHj0g" value="Naf"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqhqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqh6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqiKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqiadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqiqdEEeuR0e9NvyHj0g" name="TypeEtab1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqi6dEEeuR0e9NvyHj0g" value="TypeEtab1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqjKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqjadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqjqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqj6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqkKdEEeuR0e9NvyHj0g" name="TypeEtab2" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqkadEEeuR0e9NvyHj0g" value="TypeEtab2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqkqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqk6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqlKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqladEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqlqdEEeuR0e9NvyHj0g" name="TypeEtab3" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRql6dEEeuR0e9NvyHj0g" value="TypeEtab3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqmKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqmadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqmqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqm6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqnKdEEeuR0e9NvyHj0g" name="DateCreation" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqnadEEeuR0e9NvyHj0g" value="DateCreation"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqnqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqn6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqoKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqoadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqoqdEEeuR0e9NvyHj0g" name="SiteWeb" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqo6dEEeuR0e9NvyHj0g" value="SiteWeb"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqpKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqpadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqpqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqp6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqqKdEEeuR0e9NvyHj0g" name="Langue" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqqadEEeuR0e9NvyHj0g" value="Langue"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqqqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqq6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqrKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqradEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqrqdEEeuR0e9NvyHj0g" name="TypeCompte" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqr6dEEeuR0e9NvyHj0g" value="TypeCompte"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqsKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqsadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqsqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqs6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqtKdEEeuR0e9NvyHj0g" name="Groupe" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqtadEEeuR0e9NvyHj0g" value="Groupe"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqtqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqt6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRquKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRquadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRquqdEEeuR0e9NvyHj0g" name="International" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqu6dEEeuR0e9NvyHj0g" value="International"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqvKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqvadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqvqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqv6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqwKdEEeuR0e9NvyHj0g" name="Directif" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqwadEEeuR0e9NvyHj0g" value="Directif"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqwqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqw6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqxKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJRqxadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJRqxqdEEeuR0e9NvyHj0g" name="DateCrtCompte" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJRqx6dEEeuR0e9NvyHj0g" value="DateCrtCompte"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJRqyKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJRqyadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJRqyqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRgKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRgadEEeuR0e9NvyHj0g" name="CanalAcquisition" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRgqdEEeuR0e9NvyHj0g" value="CanalAcquisition"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRg6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRhKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRhadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRhqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRh6dEEeuR0e9NvyHj0g" name="DateMaj_CHD" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRiKdEEeuR0e9NvyHj0g" value="DateMaj_CHD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRiadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRiqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRi6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRjKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRjadEEeuR0e9NvyHj0g" name="DateMaj_Orion" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRjqdEEeuR0e9NvyHj0g" value="DateMaj_Orion"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRj6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRkKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRkadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRkqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRk6dEEeuR0e9NvyHj0g" name="DateMaj_Ccial" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRlKdEEeuR0e9NvyHj0g" value="DateMaj_Ccial"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRladEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRlqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRl6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRmKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRmadEEeuR0e9NvyHj0g" name="CompteChomette" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRmqdEEeuR0e9NvyHj0g" value="CompteChomette"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRm6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRnKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRnadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRnqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRn6dEEeuR0e9NvyHj0g" name="DateSuppression" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRoKdEEeuR0e9NvyHj0g" value="DateSuppression"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRoadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRoqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRo6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRpKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRpadEEeuR0e9NvyHj0g" name="TopSuppression" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRpqdEEeuR0e9NvyHj0g" value="TopSuppression"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRp6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRqKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRqadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRqqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRq6dEEeuR0e9NvyHj0g" name="CHDID" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRrKdEEeuR0e9NvyHj0g" value="CHDID"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRradEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRrqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRr6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRsKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRsadEEeuR0e9NvyHj0g" name="Latitude" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRsqdEEeuR0e9NvyHj0g" value="Latitude"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRs6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRtKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRtadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRtqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRt6dEEeuR0e9NvyHj0g" name="Longitude" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRuKdEEeuR0e9NvyHj0g" value="Longitude"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRuadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRuqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRu6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRvKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRvadEEeuR0e9NvyHj0g" name="Propriete" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJSRvqdEEeuR0e9NvyHj0g" value="Propriete"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJSRv6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJSRwKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJSRwadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJSRwqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJSRw6dEEeuR0e9NvyHj0g" name="Nature" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4kKdEEeuR0e9NvyHj0g" value="Nature"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4kadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4kqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4k6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4lKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS4ladEEeuR0e9NvyHj0g" name="DateMaj_Web" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4lqdEEeuR0e9NvyHj0g" value="DateMaj_Web"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4l6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4mKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4madEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4mqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS4m6dEEeuR0e9NvyHj0g" name="MailPrincipal" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4nKdEEeuR0e9NvyHj0g" value="MailPrincipal"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4nadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4nqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4n6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4oKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS4oadEEeuR0e9NvyHj0g" name="IdMois" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4oqdEEeuR0e9NvyHj0g" value="IdMois"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4o6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4pKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4padEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4pqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS4p6dEEeuR0e9NvyHj0g" name="IdPrestation" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4qKdEEeuR0e9NvyHj0g" value="IdPrestation"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4qadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4qqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4q6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4rKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS4radEEeuR0e9NvyHj0g" name="IdNews" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4rqdEEeuR0e9NvyHj0g" value="IdNews"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4r6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4sKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4sadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4sqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS4s6dEEeuR0e9NvyHj0g" name="IdClientType" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4tKdEEeuR0e9NvyHj0g" value="IdClientType"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4tadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4tqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4t6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4uKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS4uadEEeuR0e9NvyHj0g" name="IdGenre" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4uqdEEeuR0e9NvyHj0g" value="IdGenre"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4u6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4vKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4vadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4vqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS4v6dEEeuR0e9NvyHj0g" name="Nom" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4wKdEEeuR0e9NvyHj0g" value="Nom"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4wadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4wqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4w6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4xKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJS4xadEEeuR0e9NvyHj0g" name="Prenom" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJS4xqdEEeuR0e9NvyHj0g" value="Prenom"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJS4x6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJS4yKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJS4yadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJS4yqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTfoKdEEeuR0e9NvyHj0g" name="IntitulePoste" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTfoadEEeuR0e9NvyHj0g" value="IntitulePoste"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJTfoqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJTfo6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJTfpKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJTfpadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTfpqdEEeuR0e9NvyHj0g" name="Mail" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTfp6dEEeuR0e9NvyHj0g" value="Mail"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJTfqKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJTfqadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJTfqqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJTfq6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTfrKdEEeuR0e9NvyHj0g" name="IdWeb" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTfradEEeuR0e9NvyHj0g" value="IdWeb"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJTfrqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJTfr6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJTfsKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJTfsadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTfsqdEEeuR0e9NvyHj0g" name="DateCrt" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTfs6dEEeuR0e9NvyHj0g" value="DateCrt"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJTftKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJTftadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJTftqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJTft6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTfuKdEEeuR0e9NvyHj0g" name="DateSup" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTfuadEEeuR0e9NvyHj0g" value="DateSup"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJTfuqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJTfu6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJTfvKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJTfvadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTfvqdEEeuR0e9NvyHj0g" name="TopSup" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTfv6dEEeuR0e9NvyHj0g" value="TopSup"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJTfwKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJTfwadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJTfwqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJTfw6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTfxKdEEeuR0e9NvyHj0g" name="IdNiveau" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTfxadEEeuR0e9NvyHj0g" value="IdNiveau"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJTfxqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJTfx6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJTfyKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJTfyadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTfyqdEEeuR0e9NvyHj0g" name="IdUserMagento" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTfy6dEEeuR0e9NvyHj0g" value="IdUserMagento"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJTfzKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJTfzadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJTfzqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJTfz6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTf0KdEEeuR0e9NvyHj0g" name="OptinSMS" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTf0adEEeuR0e9NvyHj0g" value="OptinSMS"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJTf0qdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJTf06dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJTf1KdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJTf1adEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJTf1qdEEeuR0e9NvyHj0g" name="IdJour" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJTf16dEEeuR0e9NvyHj0g" value="IdJour"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUGsKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUGsadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUGsqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUGs6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUGtKdEEeuR0e9NvyHj0g" name="IdType" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUGtadEEeuR0e9NvyHj0g" value="IdType"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUGtqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUGt6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUGuKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUGuadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUGuqdEEeuR0e9NvyHj0g" name="Numero" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUGu6dEEeuR0e9NvyHj0g" value="Numero"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUGvKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUGvadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUGvqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUGv6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUGwKdEEeuR0e9NvyHj0g" name="Numero_2" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUGwadEEeuR0e9NvyHj0g" value="Numero_2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUGwqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUGw6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUGxKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUGxadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUGxqdEEeuR0e9NvyHj0g" name="Idtype_Tel" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUGx6dEEeuR0e9NvyHj0g" value="Idtype_Tel"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUGyKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUGyadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUGyqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUGy6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUGzKdEEeuR0e9NvyHj0g" name="IdActivite" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUGzadEEeuR0e9NvyHj0g" value="IdActivite"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUGzqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUGz6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUG0KdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUG0adEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUG0qdEEeuR0e9NvyHj0g" name="NiveauStanding" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUG06dEEeuR0e9NvyHj0g" value="NiveauStanding"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUG1KdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUG1adEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUG1qdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUG16dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUG2KdEEeuR0e9NvyHj0g" name="DigitalFriendly" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUG2adEEeuR0e9NvyHj0g" value="DigitalFriendly"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUG2qdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUG26dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUG3KdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUG3adEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUG3qdEEeuR0e9NvyHj0g" name="NbEmployes" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUG36dEEeuR0e9NvyHj0g" value="NbEmployes"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUG4KdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUG4adEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUG4qdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUtwKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUtwadEEeuR0e9NvyHj0g" name="TrancheCA" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUtwqdEEeuR0e9NvyHj0g" value="TrancheCA"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUtw6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUtxKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUtxadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUtxqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUtx6dEEeuR0e9NvyHj0g" name="TicketMoy" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUtyKdEEeuR0e9NvyHj0g" value="TicketMoy"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUtyadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUtyqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUty6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUtzKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUtzadEEeuR0e9NvyHj0g" name="DteCreation" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUtzqdEEeuR0e9NvyHj0g" value="DteCreation"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUtz6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUt0KdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUt0adEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUt0qdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUt06dEEeuR0e9NvyHj0g" name="DteMAJCcial" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUt1KdEEeuR0e9NvyHj0g" value="DteMAJCcial"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUt1adEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUt1qdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUt16dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUt2KdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUt2adEEeuR0e9NvyHj0g" name="ColTarifaire" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUt2qdEEeuR0e9NvyHj0g" value="ColTarifaire"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUt26dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUt3KdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUt3adEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUt3qdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUt36dEEeuR0e9NvyHj0g" name="RepasServis" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUt4KdEEeuR0e9NvyHj0g" value="RepasServis"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUt4adEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUt4qdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUt46dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUt5KdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUt5adEEeuR0e9NvyHj0g" name="IdSpeCulinaire" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUt5qdEEeuR0e9NvyHj0g" value="IdSpeCulinaire"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUt56dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUt6KdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUt6adEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUt6qdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUt66dEEeuR0e9NvyHj0g" name="H_OuvertureLundi" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUt7KdEEeuR0e9NvyHj0g" value="H_OuvertureLundi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUt7adEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUt7qdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUt76dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUt8KdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUt8adEEeuR0e9NvyHj0g" name="H_FermeLundi" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUt8qdEEeuR0e9NvyHj0g" value="H_FermeLundi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUt86dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUt9KdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUt9adEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUt9qdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUt96dEEeuR0e9NvyHj0g" name="H_OuvertureLundi2" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUt-KdEEeuR0e9NvyHj0g" value="H_OuvertureLundi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUt-adEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUt-qdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUt-6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUt_KdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUt_adEEeuR0e9NvyHj0g" name="H_FermeLundi2" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUt_qdEEeuR0e9NvyHj0g" value="H_FermeLundi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUt_6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUuAKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUuAadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUuAqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUuA6dEEeuR0e9NvyHj0g" name="H_OuvertureMardi" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUuBKdEEeuR0e9NvyHj0g" value="H_OuvertureMardi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUuBadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUuBqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUuB6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUuCKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUuCadEEeuR0e9NvyHj0g" name="H_FermeMardi" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUuCqdEEeuR0e9NvyHj0g" value="H_FermeMardi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUuC6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUuDKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUuDadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUuDqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUuD6dEEeuR0e9NvyHj0g" name="H_OuvertureMardi2" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUuEKdEEeuR0e9NvyHj0g" value="H_OuvertureMardi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUuEadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUuEqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUuE6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUuFKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUuFadEEeuR0e9NvyHj0g" name="H_FermeMardi2" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUuFqdEEeuR0e9NvyHj0g" value="H_FermeMardi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUuF6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUuGKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUuGadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUuGqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUuG6dEEeuR0e9NvyHj0g" name="H_OuvertureMercredi" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUuHKdEEeuR0e9NvyHj0g" value="H_OuvertureMercredi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUuHadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUuHqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUuH6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUuIKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUuIadEEeuR0e9NvyHj0g" name="H_FermeMercredi" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUuIqdEEeuR0e9NvyHj0g" value="H_FermeMercredi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUuI6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUuJKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUuJadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUuJqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJUuJ6dEEeuR0e9NvyHj0g" name="H_OuvertureMercredi2" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJUuKKdEEeuR0e9NvyHj0g" value="H_OuvertureMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJUuKadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJUuKqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJUuK6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJUuLKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVU0KdEEeuR0e9NvyHj0g" name="H_FermeMercredi2" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVU0adEEeuR0e9NvyHj0g" value="H_FermeMercredi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVU0qdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVU06dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVU1KdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVU1adEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVU1qdEEeuR0e9NvyHj0g" name="H_OuvertureJeudi" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVU16dEEeuR0e9NvyHj0g" value="H_OuvertureJeudi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVU2KdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVU2adEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVU2qdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVU26dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVU3KdEEeuR0e9NvyHj0g" name="H_FermeJeudi" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVU3adEEeuR0e9NvyHj0g" value="H_FermeJeudi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVU3qdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVU36dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVU4KdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVU4adEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVU4qdEEeuR0e9NvyHj0g" name="H_OuvertureJeudi2" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVU46dEEeuR0e9NvyHj0g" value="H_OuvertureJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVU5KdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVU5adEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVU5qdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVU56dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVU6KdEEeuR0e9NvyHj0g" name="H_FermeJeudi2" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVU6adEEeuR0e9NvyHj0g" value="H_FermeJeudi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVU6qdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVU66dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVU7KdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVU7adEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVU7qdEEeuR0e9NvyHj0g" name="H_OuvertureVendredi" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVU76dEEeuR0e9NvyHj0g" value="H_OuvertureVendredi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVU8KdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVU8adEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVU8qdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVU86dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVU9KdEEeuR0e9NvyHj0g" name="H_FermeVendredi" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVU9adEEeuR0e9NvyHj0g" value="H_FermeVendredi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVU9qdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVU96dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVU-KdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVU-adEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVU-qdEEeuR0e9NvyHj0g" name="H_OuvertureVendredi2" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVU-6dEEeuR0e9NvyHj0g" value="H_OuvertureVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVU_KdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVU_adEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVU_qdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVU_6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVVAKdEEeuR0e9NvyHj0g" name="H_FermeVendredi2" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVVAadEEeuR0e9NvyHj0g" value="H_FermeVendredi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVVAqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVVA6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVVBKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVVBadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVVBqdEEeuR0e9NvyHj0g" name="H_OuvertureSamedi" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVVB6dEEeuR0e9NvyHj0g" value="H_OuvertureSamedi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVVCKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVVCadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVVCqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVVC6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVVDKdEEeuR0e9NvyHj0g" name="H_FermeSamedi" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVVDadEEeuR0e9NvyHj0g" value="H_FermeSamedi"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVVDqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVVD6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVVEKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVVEadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVVEqdEEeuR0e9NvyHj0g" name="H_OuvertureSamedi2" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVVE6dEEeuR0e9NvyHj0g" value="H_OuvertureSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVVFKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVVFadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVVFqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVVF6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVVGKdEEeuR0e9NvyHj0g" name="H_FermeSamedi2" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVVGadEEeuR0e9NvyHj0g" value="H_FermeSamedi2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVVGqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVVG6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVVHKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJVVHadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJVVHqdEEeuR0e9NvyHj0g" name="H_OuvertureDimanche" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJVVH6dEEeuR0e9NvyHj0g" value="H_OuvertureDimanche"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJVVIKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJVVIadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJVVIqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV74KdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV74adEEeuR0e9NvyHj0g" name="H_FermeDimanche" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV74qdEEeuR0e9NvyHj0g" value="H_FermeDimanche"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV746dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV75KdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV75adEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV75qdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV756dEEeuR0e9NvyHj0g" name="H_OuvertureDimanche2" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV76KdEEeuR0e9NvyHj0g" value="H_OuvertureDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV76adEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV76qdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV766dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV77KdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV77adEEeuR0e9NvyHj0g" name="H_FermeDimanche2" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV77qdEEeuR0e9NvyHj0g" value="H_FermeDimanche2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV776dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV78KdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV78adEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV78qdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV786dEEeuR0e9NvyHj0g" name="Rue" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV79KdEEeuR0e9NvyHj0g" value="Rue"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV79adEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV79qdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV796dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV7-KdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV7-adEEeuR0e9NvyHj0g" name="Complement" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV7-qdEEeuR0e9NvyHj0g" value="Complement"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV7-6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV7_KdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV7_adEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV7_qdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV7_6dEEeuR0e9NvyHj0g" name="CodePostale" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV8AKdEEeuR0e9NvyHj0g" value="CodePostale"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV8AadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV8AqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV8A6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV8BKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV8BadEEeuR0e9NvyHj0g" name="BureauDistributeur" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV8BqdEEeuR0e9NvyHj0g" value="BureauDistributeur"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV8B6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV8CKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV8CadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV8CqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV8C6dEEeuR0e9NvyHj0g" name="CodeDepartement" position="100">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV8DKdEEeuR0e9NvyHj0g" value="CodeDepartement"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV8DadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV8DqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV8D6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV8EKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV8EadEEeuR0e9NvyHj0g" name="CodeCommune" position="101">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV8EqdEEeuR0e9NvyHj0g" value="CodeCommune"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV8E6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV8FKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV8FadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV8FqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV8F6dEEeuR0e9NvyHj0g" name="IdPays" position="102">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV8GKdEEeuR0e9NvyHj0g" value="IdPays"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV8GadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV8GqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV8G6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJV8HKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJV8HadEEeuR0e9NvyHj0g" name="TypeAdresse" position="103">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJV8HqdEEeuR0e9NvyHj0g" value="TypeAdresse"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJV8H6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJV8IKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJV8IadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWi8KdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWi8adEEeuR0e9NvyHj0g" name="CNT4" position="104">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWi8qdEEeuR0e9NvyHj0g" value="CNT4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWi86dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWi9KdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWi9adEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWi9qdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWi96dEEeuR0e9NvyHj0g" name="IRIS" position="105">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWi-KdEEeuR0e9NvyHj0g" value="IRIS"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWi-adEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWi-qdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWi-6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWi_KdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWi_adEEeuR0e9NvyHj0g" name="TypeZone" position="106">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWi_qdEEeuR0e9NvyHj0g" value="TypeZone"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWi_6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjAKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjAadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjAqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjA6dEEeuR0e9NvyHj0g" name="EnvTouristique" position="107">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjBKdEEeuR0e9NvyHj0g" value="EnvTouristique"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjBadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjBqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjB6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjCKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjCadEEeuR0e9NvyHj0g" name="IdUniversProduit" position="108">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjCqdEEeuR0e9NvyHj0g" value="IdUniversProduit"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjC6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjDKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjDadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjDqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjD6dEEeuR0e9NvyHj0g" name="Secteur" position="109">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjEKdEEeuR0e9NvyHj0g" value="Secteur"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjEadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjEqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjE6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjFKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjFadEEeuR0e9NvyHj0g" name="Dic" position="110">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjFqdEEeuR0e9NvyHj0g" value="Dic"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjF6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjGKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjGadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjGqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjG6dEEeuR0e9NvyHj0g" name="Dec" position="111">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjHKdEEeuR0e9NvyHj0g" value="Dec"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjHadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjHqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjH6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjIKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjIadEEeuR0e9NvyHj0g" name="MercuFerme" position="112">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjIqdEEeuR0e9NvyHj0g" value="MercuFerme"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjI6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjJKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjJadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjJqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjJ6dEEeuR0e9NvyHj0g" name="Mercuriale" position="113">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjKKdEEeuR0e9NvyHj0g" value="Mercuriale"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjKadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjKqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjK6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjLKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjLadEEeuR0e9NvyHj0g" name="AppelOutcall" position="114">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjLqdEEeuR0e9NvyHj0g" value="AppelOutcall"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjL6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjMKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjMadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjMqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjM6dEEeuR0e9NvyHj0g" name="TypeOp" position="115">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjNKdEEeuR0e9NvyHj0g" value="TypeOp"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjNadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjNqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjN6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjOKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjOadEEeuR0e9NvyHj0g" name="MercuContrat" position="116">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjOqdEEeuR0e9NvyHj0g" value="MercuContrat"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjO6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjPKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjPadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjPqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjP6dEEeuR0e9NvyHj0g" name="RespMercu" position="117">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjQKdEEeuR0e9NvyHj0g" value="RespMercu"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjQadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjQqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjQ6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjRKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjRadEEeuR0e9NvyHj0g" name="multicompte" position="118">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjRqdEEeuR0e9NvyHj0g" value="multicompte"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjR6dEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjSKdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjSadEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjSqdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjS6dEEeuR0e9NvyHj0g" name="Chaine" position="119">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjTKdEEeuR0e9NvyHj0g" value="Chaine"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJWjTadEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJWjTqdEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJWjT6dEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJWjUKdEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJWjUadEEeuR0e9NvyHj0g" name="Centralisateur" position="120">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJWjUqdEEeuR0e9NvyHj0g" value="Centralisateur"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJXKAKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJXKAadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJXKAqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJXKA6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJXKBKdEEeuR0e9NvyHj0g" name="SecteurVacant" position="121">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJXKBadEEeuR0e9NvyHj0g" value="SecteurVacant"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJXKBqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJXKB6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJXKCKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJXKCadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJXKCqdEEeuR0e9NvyHj0g" name="CcialConge" position="122">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJXKC6dEEeuR0e9NvyHj0g" value="CcialConge"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJXKDKdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJXKDadEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJXKDqdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJXKD6dEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_vJXKEKdEEeuR0e9NvyHj0g" name="CcialEnCharge" position="123">
        <attribute defType="com.stambia.rdbms.column.name" id="_vJXKEadEEeuR0e9NvyHj0g" value="CcialEnCharge"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_vJXKEqdEEeuR0e9NvyHj0g" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_vJXKE6dEEeuR0e9NvyHj0g" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_vJXKFKdEEeuR0e9NvyHj0g" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_vJXKFadEEeuR0e9NvyHj0g" value="VARCHAR"/>
      </node>
    </node>
  </node>
</md:node>