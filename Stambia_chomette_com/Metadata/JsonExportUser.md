<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_Pw0AkBNXEemsgZOghPO5Pg" md:ref="platform:/plugin/com.indy.environment/technology/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootArray" id="_P4Cp8BNXEemsgZOghPO5Pg" name="Clients">
    <attribute defType="com.stambia.json.rootArray.reverseURLPath" id="_P4wboBNXEemsgZOghPO5Pg"/>
    <attribute defType="com.stambia.json.rootArray.reverseFilePath" id="_P4wboRNXEemsgZOghPO5Pg" value="C:\app_dev\Used_Files\In_Files\Json\Clients.json"/>
    <node defType="com.stambia.json.object" id="_YfA30S7XEemi8scpwjq3_w" name="item" position="1">
      <node defType="com.stambia.json.object" id="_DZYwclqeEemVO-iMxXwWEg" name="identification" position="1">
        <node defType="com.stambia.json.value" id="_DZYwc1qeEemVO-iMxXwWEg" name="website_id" position="1">
          <attribute defType="com.stambia.json.value.type" id="_DZYwdFqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwdVqeEemVO-iMxXwWEg" name="id_client_magento" position="3">
          <attribute defType="com.stambia.json.value.type" id="_DZYwdlqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwd1qeEemVO-iMxXwWEg" name="id_societe_magento" position="4">
          <attribute defType="com.stambia.json.value.type" id="_DZYweFqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYweVqeEemVO-iMxXwWEg" name="id_client_chomette" position="5">
          <attribute defType="com.stambia.json.value.type" id="_DZYwelqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwe1qeEemVO-iMxXwWEg" name="id_societe_chomette" position="6">
          <attribute defType="com.stambia.json.value.type" id="_DZYwfFqeEemVO-iMxXwWEg" value="number"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwfVqeEemVO-iMxXwWEg" name="type" position="7">
          <attribute defType="com.stambia.json.value.type" id="_DZYwflqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwf1qeEemVO-iMxXwWEg" name="max_order" position="9">
          <attribute defType="com.stambia.json.value.type" id="_DZYwgFqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwgVqeEemVO-iMxXwWEg" name="autorisation_order" position="10">
          <attribute defType="com.stambia.json.value.type" id="_DZYwglqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwg1qeEemVO-iMxXwWEg" name="civilite" position="11">
          <attribute defType="com.stambia.json.value.type" id="_DZYwhFqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwhVqeEemVO-iMxXwWEg" name="nom" position="12">
          <attribute defType="com.stambia.json.value.type" id="_DZYwhlqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwh1qeEemVO-iMxXwWEg" name="prenom" position="13">
          <attribute defType="com.stambia.json.value.type" id="_DZYwiFqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwiVqeEemVO-iMxXwWEg" name="email" position="14">
          <attribute defType="com.stambia.json.value.type" id="_DZYwilqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwi1qeEemVO-iMxXwWEg" name="telephone" position="16">
          <attribute defType="com.stambia.json.value.type" id="_DZYwjFqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwjVqeEemVO-iMxXwWEg" name="mobile" position="17">
          <attribute defType="com.stambia.json.value.type" id="_DZYwjlqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_DZYwj1qeEemVO-iMxXwWEg" name="intitule_poste" position="18">
          <attribute defType="com.stambia.json.value.type" id="_DZYwkFqeEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_-vY0lVqfEemVO-iMxXwWEg" name="is_shipping_change" position="19">
          <attribute defType="com.stambia.json.value.type" id="_-vY0llqfEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_-vY0l1qfEemVO-iMxXwWEg" name="is_billing_change" position="20">
          <attribute defType="com.stambia.json.value.type" id="_-vY0mFqfEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rE8NwFqgEemVO-iMxXwWEg" name="action" position="8">
          <attribute defType="com.stambia.json.value.type" id="_rE8NwVqgEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rE8NzlqgEemVO-iMxXwWEg" name="password" position="15">
          <attribute defType="com.stambia.json.value.type" id="_rE8Nz1qgEemVO-iMxXwWEg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_NOwclXZJEema_LGoZACb8w" name="store_id" position="2">
          <attribute defType="com.stambia.json.value.type" id="_NOwclnZJEema_LGoZACb8w" value="string"/>
        </node>
      </node>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_eaF4MDLrEeqF3cPVFXUsDw" name="wsClients">
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_ihsmwDLrEeqF3cPVFXUsDw" value="C:\app_dev\Used_Files\In_Files\Json\wsClients.json"/>
    <node defType="com.stambia.json.object" id="_kcrswTLrEeqF3cPVFXUsDw" name="information" position="1">
      <node defType="com.stambia.json.array" id="_kcrswjLrEeqF3cPVFXUsDw" name="data" position="1">
        <node defType="com.stambia.json.object" id="_kcrswzLrEeqF3cPVFXUsDw" name="item" position="1">
          <node defType="com.stambia.json.object" id="_kcrsxDLrEeqF3cPVFXUsDw" name="identification" position="1">
            <node defType="com.stambia.json.value" id="_kcrsxTLrEeqF3cPVFXUsDw" name="website_id" position="1">
              <attribute defType="com.stambia.json.value.type" id="_kcrsxjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrsxzLrEeqF3cPVFXUsDw" name="store_id" position="2">
              <attribute defType="com.stambia.json.value.type" id="_kcrsyDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrsyTLrEeqF3cPVFXUsDw" name="id_client_magento" position="3">
              <attribute defType="com.stambia.json.value.type" id="_kcrsyjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrsyzLrEeqF3cPVFXUsDw" name="id_societe_magento" position="4">
              <attribute defType="com.stambia.json.value.type" id="_kcrszDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrszTLrEeqF3cPVFXUsDw" name="id_client_chomette" position="5">
              <attribute defType="com.stambia.json.value.type" id="_kcrszjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrszzLrEeqF3cPVFXUsDw" name="id_societe_chomette" position="6">
              <attribute defType="com.stambia.json.value.type" id="_kcrs0DLrEeqF3cPVFXUsDw" value="number"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs0TLrEeqF3cPVFXUsDw" name="type" position="7">
              <attribute defType="com.stambia.json.value.type" id="_kcrs0jLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs0zLrEeqF3cPVFXUsDw" name="action" position="8">
              <attribute defType="com.stambia.json.value.type" id="_kcrs1DLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs1TLrEeqF3cPVFXUsDw" name="max_order" position="9">
              <attribute defType="com.stambia.json.value.type" id="_kcrs1jLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs1zLrEeqF3cPVFXUsDw" name="autorisation_order" position="10">
              <attribute defType="com.stambia.json.value.type" id="_kcrs2DLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs2TLrEeqF3cPVFXUsDw" name="civilite" position="11">
              <attribute defType="com.stambia.json.value.type" id="_kcrs2jLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs2zLrEeqF3cPVFXUsDw" name="nom" position="12">
              <attribute defType="com.stambia.json.value.type" id="_kcrs3DLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs3TLrEeqF3cPVFXUsDw" name="prenom" position="13">
              <attribute defType="com.stambia.json.value.type" id="_kcrs3jLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs3zLrEeqF3cPVFXUsDw" name="email" position="14">
              <attribute defType="com.stambia.json.value.type" id="_kcrs4DLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs4TLrEeqF3cPVFXUsDw" name="password" position="15">
              <attribute defType="com.stambia.json.value.type" id="_kcrs4jLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs4zLrEeqF3cPVFXUsDw" name="telephone" position="16">
              <attribute defType="com.stambia.json.value.type" id="_kcrs5DLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs5TLrEeqF3cPVFXUsDw" name="mobile" position="17">
              <attribute defType="com.stambia.json.value.type" id="_kcrs5jLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs5zLrEeqF3cPVFXUsDw" name="intitule_poste" position="18">
              <attribute defType="com.stambia.json.value.type" id="_kcrs6DLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs6TLrEeqF3cPVFXUsDw" name="is_shipping_change" position="19">
              <attribute defType="com.stambia.json.value.type" id="_kcrs6jLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_kcrs6zLrEeqF3cPVFXUsDw" name="is_billing_change" position="20">
              <attribute defType="com.stambia.json.value.type" id="_kcrs7DLrEeqF3cPVFXUsDw" value="string"/>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>