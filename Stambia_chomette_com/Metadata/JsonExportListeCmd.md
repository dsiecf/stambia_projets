<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_cxZ_IAk9Eemca7hN01bE4g" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_c30JIAk9Eemca7hN01bE4g" name="CmdExport">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_c4amEAk9Eemca7hN01bE4g"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_c4amEQk9Eemca7hN01bE4g" value="C:\app_dev\Used_Files\In_Files\Json\CmdExport.json"/>
    <node defType="com.stambia.json.array" id="_fdnGsQk9Eemca7hN01bE4g" name="Order_List" position="1">
      <node defType="com.stambia.json.object" id="_fdnGsgk9Eemca7hN01bE4g" name="item" position="1">
        <node defType="com.stambia.json.value" id="_fdnGswk9Eemca7hN01bE4g" name="type" position="1">
          <attribute defType="com.stambia.json.value.type" id="_fdnGtAk9Eemca7hN01bE4g" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_fdnGtQk9Eemca7hN01bE4g" name="id_usermagento" position="2">
          <attribute defType="com.stambia.json.value.type" id="_fdnGtgk9Eemca7hN01bE4g" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_fdnGtwk9Eemca7hN01bE4g" name="id_societemagento" position="3">
          <attribute defType="com.stambia.json.value.type" id="_fdnGuAk9Eemca7hN01bE4g" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_fdnGuQk9Eemca7hN01bE4g" name="id_userCHOMETTE" position="4">
          <attribute defType="com.stambia.json.value.type" id="_fdnGugk9Eemca7hN01bE4g" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_fdnGuwk9Eemca7hN01bE4g" name="id_societeCHOMETTE" position="5">
          <attribute defType="com.stambia.json.value.type" id="_fdnGvAk9Eemca7hN01bE4g" value="string"/>
        </node>
        <node defType="com.stambia.json.object" id="_fdnGvQk9Eemca7hN01bE4g" name="entete" position="6">
          <node defType="com.stambia.json.value" id="_fdnGvgk9Eemca7hN01bE4g" name="id_commande_magento" position="1">
            <attribute defType="com.stambia.json.value.type" id="_fdnGvwk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnGwAk9Eemca7hN01bE4g" name="id_commande_web" position="2">
            <attribute defType="com.stambia.json.value.type" id="_fdnGwQk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnGwgk9Eemca7hN01bE4g" name="mode_reglement" position="3">
            <attribute defType="com.stambia.json.value.type" id="_fdnGwwk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnGxAk9Eemca7hN01bE4g" name="reference_client" position="4">
            <attribute defType="com.stambia.json.value.type" id="_fdnGxQk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnGxgk9Eemca7hN01bE4g" name="date_creation" position="5">
            <attribute defType="com.stambia.json.value.type" id="_fdnGxwk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnGyAk9Eemca7hN01bE4g" name="heure_creation" position="6">
            <attribute defType="com.stambia.json.value.type" id="_fdnGyQk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnGygk9Eemca7hN01bE4g" name="date_liv_souhaite" position="7">
            <attribute defType="com.stambia.json.value.type" id="_fdnGywk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnGzAk9Eemca7hN01bE4g" name="mnt_tva55" position="8">
            <attribute defType="com.stambia.json.value.type" id="_fdnGzQk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnGzgk9Eemca7hN01bE4g" name="mnt_tva196" position="9">
            <attribute defType="com.stambia.json.value.type" id="_fdnGzwk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG0Ak9Eemca7hN01bE4g" name="mnt_tva_autre" position="10">
            <attribute defType="com.stambia.json.value.type" id="_fdnG0Qk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG0gk9Eemca7hN01bE4g" name="mnt_ht" position="11">
            <attribute defType="com.stambia.json.value.type" id="_fdnG0wk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG1Ak9Eemca7hN01bE4g" name="mnt_ttc" position="12">
            <attribute defType="com.stambia.json.value.type" id="_fdnG1Qk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG1gk9Eemca7hN01bE4g" name="code_promo" position="13">
            <attribute defType="com.stambia.json.value.type" id="_fdnG1wk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG2Ak9Eemca7hN01bE4g" name="nb_lignes" position="14">
            <attribute defType="com.stambia.json.value.type" id="_fdnG2Qk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG3Ak9Eemca7hN01bE4g" name="remise_mnt_ht" position="16">
            <attribute defType="com.stambia.json.value.type" id="_fdnG3Qk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG3gk9Eemca7hN01bE4g" name="mnt_fdp" position="17">
            <attribute defType="com.stambia.json.value.type" id="_fdnG3wk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG4Ak9Eemca7hN01bE4g" name="instruction_livraison" position="18">
            <attribute defType="com.stambia.json.value.type" id="_fdnG4Qk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG4gk9Eemca7hN01bE4g" name="commentaire" position="19">
            <attribute defType="com.stambia.json.value.type" id="_fdnG4wk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_fdnG5Ak9Eemca7hN01bE4g" name="mnt_non_remise" position="20">
            <attribute defType="com.stambia.json.value.type" id="_fdnG5Qk9Eemca7hN01bE4g" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_RFTajnriEem-bPviwv5ZLw" name="remise_prc" position="15">
            <attribute defType="com.stambia.json.value.type" id="_RFUBkHriEem-bPviwv5ZLw" value="string"/>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_fdnG6Ak9Eemca7hN01bE4g" name="lignes_articles" position="7">
          <node defType="com.stambia.json.object" id="_fdnG6Qk9Eemca7hN01bE4g" name="item" position="1">
            <node defType="com.stambia.json.value" id="_fdnG6gk9Eemca7hN01bE4g" name="code_article" position="1">
              <attribute defType="com.stambia.json.value.type" id="_fdnG6wk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG7Ak9Eemca7hN01bE4g" name="designation" position="2">
              <attribute defType="com.stambia.json.value.type" id="_fdnG7Qk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG7gk9Eemca7hN01bE4g" name="conditionnement" position="3">
              <attribute defType="com.stambia.json.value.type" id="_fdnG7wk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG8Ak9Eemca7hN01bE4g" name="quantite" position="4">
              <attribute defType="com.stambia.json.value.type" id="_fdnG8Qk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG8gk9Eemca7hN01bE4g" name="prix_lot" position="5">
              <attribute defType="com.stambia.json.value.type" id="_fdnG8wk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG9Ak9Eemca7hN01bE4g" name="prix_unitaire" position="6">
              <attribute defType="com.stambia.json.value.type" id="_fdnG9Qk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG9gk9Eemca7hN01bE4g" name="prix_unitaire_remise" position="7">
              <attribute defType="com.stambia.json.value.type" id="_fdnG9wk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG-Ak9Eemca7hN01bE4g" name="taxe_deee" position="8">
              <attribute defType="com.stambia.json.value.type" id="_fdnG-Qk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG-gk9Eemca7hN01bE4g" name="taxe_tgap" position="9">
              <attribute defType="com.stambia.json.value.type" id="_fdnG-wk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG_Ak9Eemca7hN01bE4g" name="taxe_ecopart" position="10">
              <attribute defType="com.stambia.json.value.type" id="_fdnG_Qk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnG_gk9Eemca7hN01bE4g" name="qmv" position="11">
              <attribute defType="com.stambia.json.value.type" id="_fdnG_wk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnHAAk9Eemca7hN01bE4g" name="tva" position="12">
              <attribute defType="com.stambia.json.value.type" id="_fdnHAQk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnHAgk9Eemca7hN01bE4g" name="mnt_ht" position="13">
              <attribute defType="com.stambia.json.value.type" id="_fdnHAwk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnHBAk9Eemca7hN01bE4g" name="mnt_ttc" position="14">
              <attribute defType="com.stambia.json.value.type" id="_fdnHBQk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_fdnHBgk9Eemca7hN01bE4g" name="mnt_ht_remise" position="15">
              <attribute defType="com.stambia.json.value.type" id="_fdnHBwk9Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_UfCkcwn6Eemca7hN01bE4g" name="mnt_ttc_remise" position="17">
              <attribute defType="com.stambia.json.value.type" id="_UfCkdAn6Eemca7hN01bE4g" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_RFV2wHriEem-bPviwv5ZLw" name="mnt_ht_non_remise" position="16">
              <attribute defType="com.stambia.json.value.type" id="_RFV2wXriEem-bPviwv5ZLw" value="string"/>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>