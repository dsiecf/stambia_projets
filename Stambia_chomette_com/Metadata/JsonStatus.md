<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_PVkDoA6kEem8rKIEbYLbhQ" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_PcJMwA6kEem8rKIEbYLbhQ" name="Status">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_Pcl4sA6kEem8rKIEbYLbhQ"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_Pcl4sQ6kEem8rKIEbYLbhQ" value="C:\app_dev\Used_Files\In_Files\Json\Status.json"/>
    <node defType="com.stambia.json.array" id="_UY1JMQ6kEem8rKIEbYLbhQ" name="Status_commandes" position="1">
      <node defType="com.stambia.json.object" id="_UY1JMg6kEem8rKIEbYLbhQ" name="item" position="1">
        <node defType="com.stambia.json.value" id="_UY1JMw6kEem8rKIEbYLbhQ" name="id_commande_magento" position="1">
          <attribute defType="com.stambia.json.value.type" id="_UY1JNA6kEem8rKIEbYLbhQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_UY1JNQ6kEem8rKIEbYLbhQ" name="id_commande_chomette" position="2">
          <attribute defType="com.stambia.json.value.type" id="_UY1JNg6kEem8rKIEbYLbhQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_UY1JNw6kEem8rKIEbYLbhQ" name="status" position="3">
          <attribute defType="com.stambia.json.value.type" id="_UY1JOA6kEem8rKIEbYLbhQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_UY1JOQ6kEem8rKIEbYLbhQ" name="facture" position="4">
          <attribute defType="com.stambia.json.value.type" id="_UY1JOg6kEem8rKIEbYLbhQ" value="string"/>
        </node>
        <node defType="com.stambia.json.array" id="_TKYiyzkaEemaHK9T15k6XQ" name="tracking_url" position="5">
          <node defType="com.stambia.json.value" id="_TKYizDkaEemaHK9T15k6XQ" name="item" position="1">
            <attribute defType="com.stambia.json.value.type" id="_TKYizTkaEemaHK9T15k6XQ" value="string"/>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_TKhssDkaEemaHK9T15k6XQ" name="partial_shipped" position="6">
          <node defType="com.stambia.json.object" id="_TKhssTkaEemaHK9T15k6XQ" name="item" position="1">
            <node defType="com.stambia.json.value" id="_TKhssjkaEemaHK9T15k6XQ" name="item" position="1">
              <attribute defType="com.stambia.json.value.type" id="_TKhsszkaEemaHK9T15k6XQ" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_TKhstDkaEemaHK9T15k6XQ" name="delivered_qty" position="2">
              <attribute defType="com.stambia.json.value.type" id="_TKhstTkaEemaHK9T15k6XQ" value="number"/>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>