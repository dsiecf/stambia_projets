<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_nQD5oCS4EemYGNC7g14LyQ" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_nVpkQCS4EemYGNC7g14LyQ" name="kits">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_nWJ6kCS4EemYGNC7g14LyQ"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_nWJ6kSS4EemYGNC7g14LyQ" value="C:\app_dev\Used_Files\In_Files\Json\kits.json"/>
    <node defType="com.stambia.json.array" id="_3QcOwSVBEemYGNC7g14LyQ" name="id_societe_chomette" position="1">
      <node defType="com.stambia.json.object" id="_3QcOwiVBEemYGNC7g14LyQ" name="item" position="1">
        <node defType="com.stambia.json.object" id="_1_mkAzmwEemaHK9T15k6XQ" name="nom" position="1">
          <node defType="com.stambia.json.value" id="_1_mkBDmwEemaHK9T15k6XQ" name="libelle" position="1">
            <attribute defType="com.stambia.json.value.type" id="_1_mkBTmwEemaHK9T15k6XQ" value="string"/>
          </node>
          <node defType="com.stambia.json.array" id="_1_mkBjmwEemaHK9T15k6XQ" name="contenu" position="2">
            <node defType="com.stambia.json.object" id="_1_mkBzmwEemaHK9T15k6XQ" name="item" position="1">
              <node defType="com.stambia.json.value" id="_1_mkCDmwEemaHK9T15k6XQ" name="article" position="1">
                <attribute defType="com.stambia.json.value.type" id="_1_nLEDmwEemaHK9T15k6XQ" value="string"/>
                <node defType="com.stambia.xml.propertyField" id="_VB1JIzm1EemaHK9T15k6XQ" name="CodeArt">
                  <attribute defType="com.stambia.xml.propertyField.property" id="_WaYQcDm1EemaHK9T15k6XQ" value="nodeName"/>
                </node>
              </node>
            </node>
          </node>
          <node defType="com.stambia.xml.propertyField" id="_0ZKYbDm1EemaHK9T15k6XQ" name="kitNom">
            <attribute defType="com.stambia.xml.propertyField.property" id="_2Ko9EDm1EemaHK9T15k6XQ" value="nodeName"/>
          </node>
        </node>
      </node>
      <node defType="com.stambia.xml.propertyField" id="_wYpB8yVsEemYGNC7g14LyQ" name="CodeCli">
        <attribute defType="com.stambia.xml.propertyField.property" id="_x6vt4CVsEemYGNC7g14LyQ" value="nodeName"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_yos9MU_3EempI9psh7GMCw" name="kits_">
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_8MavIE_3EempI9psh7GMCw" value="C:\app_dev\Used_Files\In_Files\Json\kits_.json"/>
    <node defType="com.stambia.json.array" id="_98dwcU_3EempI9psh7GMCw" name="id_societe_chomette" position="1">
      <node defType="com.stambia.json.object" id="_98dwck_3EempI9psh7GMCw" name="item" position="1">
        <node defType="com.stambia.json.value" id="_98dwc0_3EempI9psh7GMCw" name="nom" position="1">
          <attribute defType="com.stambia.json.value.type" id="_98dwdE_3EempI9psh7GMCw" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_98dwdU_3EempI9psh7GMCw" name="libelle" position="2">
          <attribute defType="com.stambia.json.value.type" id="_98dwdk_3EempI9psh7GMCw" value="string"/>
        </node>
        <node defType="com.stambia.json.array" id="_98dwd0_3EempI9psh7GMCw" name="contenu" position="3">
          <node defType="com.stambia.json.object" id="_98dweE_3EempI9psh7GMCw" name="item" position="1">
            <node defType="com.stambia.json.value" id="_98dweU_3EempI9psh7GMCw" name="article" position="1">
              <attribute defType="com.stambia.json.value.type" id="_98dwek_3EempI9psh7GMCw" value="string"/>
              <node defType="com.stambia.xml.propertyField" id="_23mDQ1BqEempI9psh7GMCw" name="CodeArt">
                <attribute defType="com.stambia.xml.propertyField.property" id="_3z56gFBqEempI9psh7GMCw" value="nodeName"/>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node defType="com.stambia.xml.propertyField" id="_a9hvE0_8EempI9psh7GMCw" name="CodeCli">
        <attribute defType="com.stambia.xml.propertyField.property" id="_fIxAcE_8EempI9psh7GMCw" value="nodeName"/>
      </node>
    </node>
  </node>
</md:node>