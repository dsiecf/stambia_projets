<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_N22KIBKaEemsgZOghPO5Pg" md:ref="platform:/plugin/com.indy.environment/technology/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootArray" id="_N8SDwBKaEemsgZOghPO5Pg" name="Societes">
    <attribute defType="com.stambia.json.rootArray.reverseURLPath" id="_N8k-sBKaEemsgZOghPO5Pg"/>
    <attribute defType="com.stambia.json.rootArray.reverseFilePath" id="_N8k-sRKaEemsgZOghPO5Pg" value="C:\app_dev\Used_Files\In_Files\Json\Societes.json"/>
    <node defType="com.stambia.json.object" id="_xD3KoS6rEemi8scpwjq3_w" name="item" position="1">
      <node defType="com.stambia.json.object" id="_xD3Koi6rEemi8scpwjq3_w" name="identification" position="1">
        <node defType="com.stambia.json.value" id="_xD3Koy6rEemi8scpwjq3_w" name="id_societe_magento" position="4">
          <attribute defType="com.stambia.json.value.type" id="_xD3KpC6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3KpS6rEemi8scpwjq3_w" name="id_client_chomette" position="3">
          <attribute defType="com.stambia.json.value.type" id="_xD3Kpi6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3Kpy6rEemi8scpwjq3_w" name="raison_sociale" position="10">
          <attribute defType="com.stambia.json.value.type" id="_xD3KqC6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3KqS6rEemi8scpwjq3_w" name="enseigne" position="11">
          <attribute defType="com.stambia.json.value.type" id="_xD3Kqi6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3Kqy6rEemi8scpwjq3_w" name="rue1" position="14">
          <attribute defType="com.stambia.json.value.type" id="_xD3KrC6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3KrS6rEemi8scpwjq3_w" name="rue2" position="15">
          <attribute defType="com.stambia.json.value.type" id="_xD3Kri6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3Kry6rEemi8scpwjq3_w" name="code_postal" position="18">
          <attribute defType="com.stambia.json.value.type" id="_xD3KsC6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3KsS6rEemi8scpwjq3_w" name="ville" position="16">
          <attribute defType="com.stambia.json.value.type" id="_xD3Ksi6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3Ksy6rEemi8scpwjq3_w" name="pays" position="17">
          <attribute defType="com.stambia.json.value.type" id="_xD3KtC6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3KtS6rEemi8scpwjq3_w" name="tel" position="19">
          <attribute defType="com.stambia.json.value.type" id="_xD3Kti6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3KvS6rEemi8scpwjq3_w" name="tarif" position="9">
          <attribute defType="com.stambia.json.value.type" id="_xD3Kvi6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3Kxy6rEemi8scpwjq3_w" name="commercial" position="8">
          <attribute defType="com.stambia.json.value.type" id="_xD3KyC6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_Y5BGezOEEemjULX4DWixSg" name="email_admin" position="5">
          <attribute defType="com.stambia.json.value.type" id="_Y5BGfDOEEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_EWWocTQlEemjULX4DWixSg" name="nom" position="12">
          <attribute defType="com.stambia.json.value.type" id="_EWWocjQlEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_EWWoczQlEemjULX4DWixSg" name="prenom" position="13">
          <attribute defType="com.stambia.json.value.type" id="_EWWodDQlEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_i_t50zs1EemaHK9T15k6XQ" name="website_id" position="1">
          <attribute defType="com.stambia.json.value.type" id="_i_t51Ds1EemaHK9T15k6XQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_nq55Q3ItEemIMIaOcxSI7A" name="store_id" position="2">
          <attribute defType="com.stambia.json.value.type" id="_nq55RHItEemIMIaOcxSI7A" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_tuaXfZ2pEem6jcmdEfXK_g" name="email_societe" position="6">
          <attribute defType="com.stambia.json.value.type" id="_tuaXfp2pEem6jcmdEfXK_g" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_tuaXf52pEem6jcmdEfXK_g" name="email_bis" position="7">
          <attribute defType="com.stambia.json.value.type" id="_tuaXgJ2pEem6jcmdEfXK_g" value="string"/>
        </node>
      </node>
      <node defType="com.stambia.json.object" id="_xD3K0S6rEemi8scpwjq3_w" name="livraison" position="2">
        <node defType="com.stambia.json.value" id="_xD3K1i6rEemi8scpwjq3_w" name="rue1" position="1">
          <attribute defType="com.stambia.json.value.type" id="_xD3K1y6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K2C6rEemi8scpwjq3_w" name="rue2" position="2">
          <attribute defType="com.stambia.json.value.type" id="_xD3K2S6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K2i6rEemi8scpwjq3_w" name="code_postal" position="5">
          <attribute defType="com.stambia.json.value.type" id="_xD3K2y6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K3C6rEemi8scpwjq3_w" name="ville" position="3">
          <attribute defType="com.stambia.json.value.type" id="_xD3K3S6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K3i6rEemi8scpwjq3_w" name="pays" position="4">
          <attribute defType="com.stambia.json.value.type" id="_xD3K3y6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMaIjQiEemjULX4DWixSg" name="tel" position="6">
          <attribute defType="com.stambia.json.value.type" id="_pVMaIzQiEemjULX4DWixSg" value="string"/>
        </node>
      </node>
      <node defType="com.stambia.json.object" id="_xD3K4i6rEemi8scpwjq3_w" name="facturation" position="3">
        <node defType="com.stambia.json.value" id="_xD3K4y6rEemi8scpwjq3_w" name="rue1" position="1">
          <attribute defType="com.stambia.json.value.type" id="_xD3K5C6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K5S6rEemi8scpwjq3_w" name="rue2" position="2">
          <attribute defType="com.stambia.json.value.type" id="_xD3K5i6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K5y6rEemi8scpwjq3_w" name="ville" position="3">
          <attribute defType="com.stambia.json.value.type" id="_xD3K6C6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K6S6rEemi8scpwjq3_w" name="pays" position="4">
          <attribute defType="com.stambia.json.value.type" id="_xD3K6i6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K6y6rEemi8scpwjq3_w" name="code_postal" position="5">
          <attribute defType="com.stambia.json.value.type" id="_xD3K7C6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K7S6rEemi8scpwjq3_w" name="tel" position="6">
          <attribute defType="com.stambia.json.value.type" id="_xD3K7i6rEemi8scpwjq3_w" value="string"/>
        </node>
      </node>
      <node defType="com.stambia.json.object" id="_xD3K7y6rEemi8scpwjq3_w" name="activite" position="4">
        <node defType="com.stambia.json.value" id="_xD3K8C6rEemi8scpwjq3_w" name="code_client" position="2">
          <attribute defType="com.stambia.json.value.type" id="_xD3K8S6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K8i6rEemi8scpwjq3_w" name="tva_intracomm" position="8">
          <attribute defType="com.stambia.json.value.type" id="_xD3K8y6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K9C6rEemi8scpwjq3_w" name="type_etab_1" position="9">
          <attribute defType="com.stambia.json.value.type" id="_xD3K9S6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K9i6rEemi8scpwjq3_w" name="type_etab_2" position="10">
          <attribute defType="com.stambia.json.value.type" id="_xD3K9y6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K-C6rEemi8scpwjq3_w" name="type_etab_3" position="11">
          <attribute defType="com.stambia.json.value.type" id="_xD3K-S6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K-i6rEemi8scpwjq3_w" name="specialites_culinaires" position="12">
          <attribute defType="com.stambia.json.value.type" id="_xD3K-y6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K_C6rEemi8scpwjq3_w" name="niveau_standing" position="13">
          <attribute defType="com.stambia.json.value.type" id="_xD3K_S6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3K_i6rEemi8scpwjq3_w" name="repas_servis" position="14">
          <attribute defType="com.stambia.json.value.type" id="_xD3K_y6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LAC6rEemi8scpwjq3_w" name="prestations" position="15">
          <attribute defType="com.stambia.json.value.type" id="_xD3LAS6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LAi6rEemi8scpwjq3_w" name="langue_principale" position="16">
          <attribute defType="com.stambia.json.value.type" id="_xD3LAy6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LBC6rEemi8scpwjq3_w" name="nb_couverts_jour" position="17">
          <attribute defType="com.stambia.json.value.type" id="_xD3LBS6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LBi6rEemi8scpwjq3_w" name="nb_repas_jour" position="18">
          <attribute defType="com.stambia.json.value.type" id="_xD3LBy6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LCC6rEemi8scpwjq3_w" name="nb_couchages" position="19">
          <attribute defType="com.stambia.json.value.type" id="_xD3LCS6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LCi6rEemi8scpwjq3_w" name="nb_lits" position="20">
          <attribute defType="com.stambia.json.value.type" id="_xD3LCy6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LDC6rEemi8scpwjq3_w" name="nb_eleves" position="21">
          <attribute defType="com.stambia.json.value.type" id="_xD3LDS6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LDi6rEemi8scpwjq3_w" name="type_compte" position="22">
          <attribute defType="com.stambia.json.value.type" id="_xD3LDy6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LEC6rEemi8scpwjq3_w" name="num_adherent" position="23">
          <attribute defType="com.stambia.json.value.type" id="_xD3LES6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LEi6rEemi8scpwjq3_w" name="env_tourist" position="24">
          <attribute defType="com.stambia.json.value.type" id="_xD3LEy6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LFC6rEemi8scpwjq3_w" name="annees_existence" position="25">
          <attribute defType="com.stambia.json.value.type" id="_xD3LFS6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LFi6rEemi8scpwjq3_w" name="scoring_ca" position="26">
          <attribute defType="com.stambia.json.value.type" id="_xD3LFy6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LGC6rEemi8scpwjq3_w" name="statut_client" position="27">
          <attribute defType="com.stambia.json.value.type" id="_xD3LGS6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LGi6rEemi8scpwjq3_w" name="niveau_fidelite" position="28">
          <attribute defType="com.stambia.json.value.type" id="_xD3LGy6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LHC6rEemi8scpwjq3_w" name="comportement" position="29">
          <attribute defType="com.stambia.json.value.type" id="_xD3LHS6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LHi6rEemi8scpwjq3_w" name="demat_factures" position="30">
          <attribute defType="com.stambia.json.value.type" id="_xD3LHy6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LIC6rEemi8scpwjq3_w" name="email_fact_demat" position="31">
          <attribute defType="com.stambia.json.value.type" id="_xD3LIS6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_xD3LIi6rEemi8scpwjq3_w" name="jours_d_ouverture" position="32">
          <attribute defType="com.stambia.json.value.type" id="_xD3LIy6rEemi8scpwjq3_w" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMaMjQiEemjULX4DWixSg" name="siret" position="1">
          <attribute defType="com.stambia.json.value.type" id="_pVMaMzQiEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMaNjQiEemjULX4DWixSg" name="mercuriale" position="3">
          <attribute defType="com.stambia.json.value.type" id="_pVMaNzQiEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMaODQiEemjULX4DWixSg" name="fax" position="7">
          <attribute defType="com.stambia.json.value.type" id="_pVMaOTQiEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMabDQiEemjULX4DWixSg" name="couleur" position="33">
          <attribute defType="com.stambia.json.value.type" id="_pVMabTQiEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMabjQiEemjULX4DWixSg" name="mode_paiement" position="34">
          <attribute defType="com.stambia.json.value.type" id="_pVMabzQiEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMacDQiEemjULX4DWixSg" name="frais_de_port" position="35">
          <attribute defType="com.stambia.json.value.type" id="_pVMacTQiEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMacjQiEemjULX4DWixSg" name="free_shipping" position="36">
          <attribute defType="com.stambia.json.value.type" id="_pVMaczQiEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMadDQiEemjULX4DWixSg" name="minimum_commande" position="37">
          <attribute defType="com.stambia.json.value.type" id="_pVMadTQiEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pVMaeDQiEemjULX4DWixSg" name="instruction_livraison" position="39">
          <attribute defType="com.stambia.json.value.type" id="_pVMaeTQiEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_chO4UjQlEemjULX4DWixSg" name="type_livraison" position="38">
          <attribute defType="com.stambia.json.value.type" id="_chO4UzQlEemjULX4DWixSg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_SGxEBkmKEemlXJJXLd6IGA" name="reference_commande_obligatoire" position="40">
          <attribute defType="com.stambia.json.value.type" id="_SGxEB0mKEemlXJJXLd6IGA" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_CsNw6tq_Eem5ebMPg-wyFg" name="centralisateur_gescom" position="4">
          <attribute defType="com.stambia.json.value.type" id="_CsNw69q_Eem5ebMPg-wyFg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_CsNw7Nq_Eem5ebMPg-wyFg" name="chaine_gescom" position="5">
          <attribute defType="com.stambia.json.value.type" id="_CsNw7dq_Eem5ebMPg-wyFg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_CsNw7tq_Eem5ebMPg-wyFg" name="groupe_gescom" position="6">
          <attribute defType="com.stambia.json.value.type" id="_CsNw79q_Eem5ebMPg-wyFg" value="string"/>
        </node>
      </node>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_3TnvIBs8EemFNuqeh5x00Q" name="Societe">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_3UOMEBs8EemFNuqeh5x00Q"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_3UOMERs8EemFNuqeh5x00Q" value="C:\app_dev\Used_Files\In_Files\Json\Societe.json"/>
    <attribute defType="com.stambia.json.rootObject.filePath" id="_S8m_sDTsEemjULX4DWixSg" value="\\servernt46\Stambia_Test\Chomette.com\Import\Sociétés\Encours\${~/FileName}$"/>
    <node defType="com.stambia.xml.propertyField" id="_k1AdTDTvEemjULX4DWixSg" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_mY4q0DTvEemjULX4DWixSg" value="fileName"/>
    </node>
    <node defType="com.stambia.json.object" id="_PU0qVVfCEemGruvsZ9L3ag" name="livraison" position="2">
      <node defType="com.stambia.json.value" id="_PU0qVlfCEemGruvsZ9L3ag" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_PU0qV1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_G0DFIOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0qWFfCEemGruvsZ9L3ag" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_PU0qWVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_HLWx4OamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0qWlfCEemGruvsZ9L3ag" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_PU0qW1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_HlkBoOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0qXFfCEemGruvsZ9L3ag" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_PU0qXVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_IE0wgOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0qXlfCEemGruvsZ9L3ag" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_PU0qX1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_IiqjEOamEemt9aU7LTgIyg" value="10"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0qYFfCEemGruvsZ9L3ag" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_PU0qYVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_JZsRsOamEemt9aU7LTgIyg" value="250"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_PU1RZlfCEemGruvsZ9L3ag" name="activite" position="4">
      <node defType="com.stambia.json.value" id="_PU1RZ1fCEemGruvsZ9L3ag" name="siret" position="1">
        <attribute defType="com.stambia.json.value.type" id="_PU1RaFfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_N8YZwOamEemt9aU7LTgIyg" value="20"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1RaVfCEemGruvsZ9L3ag" name="code_client" position="2">
        <attribute defType="com.stambia.json.value.type" id="_PU1RalfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_OeMGUOamEemt9aU7LTgIyg" value="7"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1Ra1fCEemGruvsZ9L3ag" name="fax" position="3">
        <attribute defType="com.stambia.json.value.type" id="_PU1RbFfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_P0aSYOamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1RbVfCEemGruvsZ9L3ag" name="mercuriale" position="4">
        <attribute defType="com.stambia.json.value.type" id="_PU1RblfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_QVlFwOamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1Rb1fCEemGruvsZ9L3ag" name="tva_intracomm" position="8">
        <attribute defType="com.stambia.json.value.type" id="_PU1RcFfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_SDCtIOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1RcVfCEemGruvsZ9L3ag" name="type_etab_1" position="9">
        <attribute defType="com.stambia.json.value.type" id="_PU1RclfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1Rc1fCEemGruvsZ9L3ag" name="type_etab_2" position="10">
        <attribute defType="com.stambia.json.value.type" id="_PU1RdFfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1RdVfCEemGruvsZ9L3ag" name="type_etab_3" position="11">
        <attribute defType="com.stambia.json.value.type" id="_PU1RdlfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1Rd1fCEemGruvsZ9L3ag" name="specialites_culinaires" position="12">
        <attribute defType="com.stambia.json.value.type" id="_PU1ReFfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1ReVfCEemGruvsZ9L3ag" name="niveau_standing" position="13">
        <attribute defType="com.stambia.json.value.type" id="_PU1RelfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1Re1fCEemGruvsZ9L3ag" name="repas_servis" position="14">
        <attribute defType="com.stambia.json.value.type" id="_PU14cFfCEemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14cVfCEemGruvsZ9L3ag" name="prestations" position="15">
        <attribute defType="com.stambia.json.value.type" id="_PU14clfCEemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14c1fCEemGruvsZ9L3ag" name="langue_principale" position="16">
        <attribute defType="com.stambia.json.value.type" id="_PU14dFfCEemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14dVfCEemGruvsZ9L3ag" name="nb_couverts_jour" position="17">
        <attribute defType="com.stambia.json.value.type" id="_PU14dlfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14d1fCEemGruvsZ9L3ag" name="nb_repas_jour" position="18">
        <attribute defType="com.stambia.json.value.type" id="_PU14eFfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14eVfCEemGruvsZ9L3ag" name="nb_couchages" position="19">
        <attribute defType="com.stambia.json.value.type" id="_PU14elfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14e1fCEemGruvsZ9L3ag" name="nb_lits" position="20">
        <attribute defType="com.stambia.json.value.type" id="_PU14fFfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14fVfCEemGruvsZ9L3ag" name="nb_eleves" position="21">
        <attribute defType="com.stambia.json.value.type" id="_PU14flfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14f1fCEemGruvsZ9L3ag" name="type_compte" position="22">
        <attribute defType="com.stambia.json.value.type" id="_PU14gFfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14gVfCEemGruvsZ9L3ag" name="num_adherent" position="23">
        <attribute defType="com.stambia.json.value.type" id="_PU14glfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14g1fCEemGruvsZ9L3ag" name="env_tourist" position="24">
        <attribute defType="com.stambia.json.value.type" id="_PU14hFfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14hVfCEemGruvsZ9L3ag" name="annees_existence" position="25">
        <attribute defType="com.stambia.json.value.type" id="_PU14hlfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14h1fCEemGruvsZ9L3ag" name="scoring_ca" position="26">
        <attribute defType="com.stambia.json.value.type" id="_PU14iFfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU14iVfCEemGruvsZ9L3ag" name="statut_client" position="27">
        <attribute defType="com.stambia.json.value.type" id="_PU14ilfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fgFfCEemGruvsZ9L3ag" name="niveau_fidelite" position="28">
        <attribute defType="com.stambia.json.value.type" id="_PU2fgVfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fglfCEemGruvsZ9L3ag" name="comportement" position="29">
        <attribute defType="com.stambia.json.value.type" id="_PU2fg1fCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fhFfCEemGruvsZ9L3ag" name="demat_factures" position="30">
        <attribute defType="com.stambia.json.value.type" id="_PU2fhVfCEemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fhlfCEemGruvsZ9L3ag" name="email_fact_demat" position="31">
        <attribute defType="com.stambia.json.value.type" id="_PU2fh1fCEemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fiFfCEemGruvsZ9L3ag" name="jours_d_ouverture" position="32">
        <attribute defType="com.stambia.json.value.type" id="_PU2fiVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_Ys25wOamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2filfCEemGruvsZ9L3ag" name="couleur" position="33">
        <attribute defType="com.stambia.json.value.type" id="_PU2fi1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_sCQoMOamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fjFfCEemGruvsZ9L3ag" name="mode_paiement" position="34">
        <attribute defType="com.stambia.json.value.type" id="_PU2fjVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_rRy0kOamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fjlfCEemGruvsZ9L3ag" name="frais_de_port" position="35">
        <attribute defType="com.stambia.json.value.type" id="_PU2fj1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_qwcbAOamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fkFfCEemGruvsZ9L3ag" name="free_shipping" position="36">
        <attribute defType="com.stambia.json.value.type" id="_PU2fkVfCEemGruvsZ9L3ag" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fklfCEemGruvsZ9L3ag" name="minimum_commande" position="37">
        <attribute defType="com.stambia.json.value.type" id="_PU2fk1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_qEDPEOamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2flFfCEemGruvsZ9L3ag" name="type_livraison" position="38">
        <attribute defType="com.stambia.json.value.type" id="_PU2flVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_pdJnIOamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fllfCEemGruvsZ9L3ag" name="instruction_livraison" position="39">
        <attribute defType="com.stambia.json.value.type" id="_PU2fl1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_agL4kOamEemt9aU7LTgIyg" value="500"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU2fmFfCEemGruvsZ9L3ag" name="reference_commande_obligatoire" position="40">
        <attribute defType="com.stambia.json.value.type" id="_PU2fmVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_bBo_0OamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_OFHFDNuPEemWA5xRaTOtjQ" name="centralisateur_gescom" position="5">
        <attribute defType="com.stambia.json.value.type" id="_OFHFDduPEemWA5xRaTOtjQ" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_Q1cRkOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_OFHFDtuPEemWA5xRaTOtjQ" name="chaine_gescom" position="6">
        <attribute defType="com.stambia.json.value.type" id="_OFHFD9uPEemWA5xRaTOtjQ" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_RPRG0OamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_OFHFENuPEemWA5xRaTOtjQ" name="groupe_gescom" position="7">
        <attribute defType="com.stambia.json.value.type" id="_OFHFEduPEemWA5xRaTOtjQ" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_RngnsOamEemt9aU7LTgIyg" value="250"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_PUzcMVfCEemGruvsZ9L3ag" name="Identification" position="1">
      <node defType="com.stambia.json.value" id="_PU0DQFfCEemGruvsZ9L3ag" name="website_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_PU0DQVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_4W7UQOalEemt9aU7LTgIyg" value="1"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DQlfCEemGruvsZ9L3ag" name="id_client_chomette" position="3">
        <attribute defType="com.stambia.json.value.type" id="_PU0DQ1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_53Th4OalEemt9aU7LTgIyg" value="7"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DRFfCEemGruvsZ9L3ag" name="id_societe_magento" position="4">
        <attribute defType="com.stambia.json.value.type" id="_PU0DRVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_9M4WkOalEemt9aU7LTgIyg" value="7"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DRlfCEemGruvsZ9L3ag" name="email_admin" position="5">
        <attribute defType="com.stambia.json.value.type" id="_PU0DR1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_90S74OalEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DSFfCEemGruvsZ9L3ag" name="commercial" position="8">
        <attribute defType="com.stambia.json.value.type" id="_PU0DSVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_AHMLkOamEemt9aU7LTgIyg" value="7"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DSlfCEemGruvsZ9L3ag" name="tarif" position="9">
        <attribute defType="com.stambia.json.value.type" id="_PU0DS1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_AuzlMOamEemt9aU7LTgIyg" value="50"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DTFfCEemGruvsZ9L3ag" name="raison_sociale" position="10">
        <attribute defType="com.stambia.json.value.type" id="_PU0DTVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_BQnRwOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DTlfCEemGruvsZ9L3ag" name="enseigne" position="11">
        <attribute defType="com.stambia.json.value.type" id="_PU0DT1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_BrsEMOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DUFfCEemGruvsZ9L3ag" name="nom" position="12">
        <attribute defType="com.stambia.json.value.type" id="_PU0DUVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_CHIqEOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DUlfCEemGruvsZ9L3ag" name="prenom" position="13">
        <attribute defType="com.stambia.json.value.type" id="_PU0DU1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_Cg2xoOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DVFfCEemGruvsZ9L3ag" name="rue1" position="14">
        <attribute defType="com.stambia.json.value.type" id="_PU0DVVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_C5Ay8OamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DVlfCEemGruvsZ9L3ag" name="rue2" position="15">
        <attribute defType="com.stambia.json.value.type" id="_PU0DV1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_DV2f8OamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DWFfCEemGruvsZ9L3ag" name="ville" position="16">
        <attribute defType="com.stambia.json.value.type" id="_PU0DWVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_DvYaQOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0DWlfCEemGruvsZ9L3ag" name="pays" position="17">
        <attribute defType="com.stambia.json.value.type" id="_PU0qUFfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_EQqiYOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0qUVfCEemGruvsZ9L3ag" name="code_postal" position="18">
        <attribute defType="com.stambia.json.value.type" id="_PU0qUlfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_FHCJsOamEemt9aU7LTgIyg" value="10"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0qU1fCEemGruvsZ9L3ag" name="tel" position="19">
        <attribute defType="com.stambia.json.value.type" id="_PU0qVFfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_GDo74OamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_BGzE0nIuEemIMIaOcxSI7A" name="store_id" position="2">
        <attribute defType="com.stambia.json.value.type" id="_BGzE03IuEemIMIaOcxSI7A" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_49tAYOalEemt9aU7LTgIyg" value="1"/>
      </node>
      <node defType="com.stambia.json.value" id="_OFFP3NuPEemWA5xRaTOtjQ" name="email_societe" position="6">
        <attribute defType="com.stambia.json.value.type" id="_OFFP3duPEemWA5xRaTOtjQ" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_-TCGUOalEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_OFFP3tuPEemWA5xRaTOtjQ" name="email_bis" position="7">
        <attribute defType="com.stambia.json.value.type" id="_OFFP39uPEemWA5xRaTOtjQ" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_-u5i8OalEemt9aU7LTgIyg" value="250"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_PU0qYlfCEemGruvsZ9L3ag" name="facturation" position="3">
      <node defType="com.stambia.json.value" id="_PU0qY1fCEemGruvsZ9L3ag" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_PU0qZFfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_KdQngOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0qZVfCEemGruvsZ9L3ag" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_PU0qZlfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_K7Nu0OamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU0qZ1fCEemGruvsZ9L3ag" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_PU0qaFfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_LV1OQOamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1RYFfCEemGruvsZ9L3ag" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_PU1RYVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_LuBr0OamEemt9aU7LTgIyg" value="250"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1RYlfCEemGruvsZ9L3ag" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_PU1RY1fCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_MJvXcOamEemt9aU7LTgIyg" value="10"/>
      </node>
      <node defType="com.stambia.json.value" id="_PU1RZFfCEemGruvsZ9L3ag" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_PU1RZVfCEemGruvsZ9L3ag" value="string"/>
        <attribute defType="com.stambia.json.value.size" id="_MtaFMOamEemt9aU7LTgIyg" value="250"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_-NI4AVe-EemGruvsZ9L3ag" name="Soc">
    <attribute defType="com.stambia.json.rootObject.filePath" id="_Any8kFe_EemGruvsZ9L3ag" value="\\servernt46\Stambia_Test\Chomette.com\Import\Sociétés\Encours\Societe.json"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_BZYKgFe_EemGruvsZ9L3ag" value="C:\app_dev\Used_Files\In_Files\Json\TestSoc.json"/>
    <node defType="com.stambia.json.object" id="_EtTZj1e_EemGruvsZ9L3ag" name="facturation" position="3">
      <node defType="com.stambia.json.value" id="_EtTZkFe_EemGruvsZ9L3ag" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_EtTZkVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZkle_EemGruvsZ9L3ag" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_EtTZk1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZlFe_EemGruvsZ9L3ag" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_EtTZlVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZlle_EemGruvsZ9L3ag" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_EtTZl1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZmFe_EemGruvsZ9L3ag" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_EtTZmVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZmle_EemGruvsZ9L3ag" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_EtTZm1e_EemGruvsZ9L3ag" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_EtTZgle_EemGruvsZ9L3ag" name="livraison" position="2">
      <node defType="com.stambia.json.value" id="_EtTZg1e_EemGruvsZ9L3ag" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_EtTZhFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZhVe_EemGruvsZ9L3ag" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_EtTZhle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZh1e_EemGruvsZ9L3ag" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_EtTZiFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZiVe_EemGruvsZ9L3ag" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_EtTZile_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZi1e_EemGruvsZ9L3ag" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_EtTZjFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZjVe_EemGruvsZ9L3ag" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_EtTZjle_EemGruvsZ9L3ag" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_EtTZYVe_EemGruvsZ9L3ag" name="Identification" position="1">
      <node defType="com.stambia.json.value" id="_EtTZYle_EemGruvsZ9L3ag" name="website_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_EtTZY1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZZFe_EemGruvsZ9L3ag" name="id_client_chomette" position="2">
        <attribute defType="com.stambia.json.value.type" id="_EtTZZVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZZle_EemGruvsZ9L3ag" name="id_societe_magento" position="3">
        <attribute defType="com.stambia.json.value.type" id="_EtTZZ1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZaFe_EemGruvsZ9L3ag" name="email_admin" position="4">
        <attribute defType="com.stambia.json.value.type" id="_EtTZaVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZale_EemGruvsZ9L3ag" name="commercial" position="5">
        <attribute defType="com.stambia.json.value.type" id="_EtTZa1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZbFe_EemGruvsZ9L3ag" name="tarif" position="6">
        <attribute defType="com.stambia.json.value.type" id="_EtTZbVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZble_EemGruvsZ9L3ag" name="raison_sociale" position="7">
        <attribute defType="com.stambia.json.value.type" id="_EtTZb1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZcFe_EemGruvsZ9L3ag" name="enseigne" position="8">
        <attribute defType="com.stambia.json.value.type" id="_EtTZcVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZcle_EemGruvsZ9L3ag" name="nom" position="9">
        <attribute defType="com.stambia.json.value.type" id="_EtTZc1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZdFe_EemGruvsZ9L3ag" name="prenom" position="10">
        <attribute defType="com.stambia.json.value.type" id="_EtTZdVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZdle_EemGruvsZ9L3ag" name="rue1" position="11">
        <attribute defType="com.stambia.json.value.type" id="_EtTZd1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZeFe_EemGruvsZ9L3ag" name="rue2" position="12">
        <attribute defType="com.stambia.json.value.type" id="_EtTZeVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZele_EemGruvsZ9L3ag" name="ville" position="13">
        <attribute defType="com.stambia.json.value.type" id="_EtTZe1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZfFe_EemGruvsZ9L3ag" name="pays" position="14">
        <attribute defType="com.stambia.json.value.type" id="_EtTZfVe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZfle_EemGruvsZ9L3ag" name="code_postal" position="15">
        <attribute defType="com.stambia.json.value.type" id="_EtTZf1e_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZgFe_EemGruvsZ9L3ag" name="tel" position="16">
        <attribute defType="com.stambia.json.value.type" id="_EtTZgVe_EemGruvsZ9L3ag" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_EtTZnFe_EemGruvsZ9L3ag" name="activite" position="4">
      <node defType="com.stambia.json.value" id="_EtTZnVe_EemGruvsZ9L3ag" name="siret" position="1">
        <attribute defType="com.stambia.json.value.type" id="_EtTZnle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZn1e_EemGruvsZ9L3ag" name="code_client" position="2">
        <attribute defType="com.stambia.json.value.type" id="_EtTZoFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZoVe_EemGruvsZ9L3ag" name="fax" position="3">
        <attribute defType="com.stambia.json.value.type" id="_EtTZole_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZo1e_EemGruvsZ9L3ag" name="mercuriale" position="4">
        <attribute defType="com.stambia.json.value.type" id="_EtTZpFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZpVe_EemGruvsZ9L3ag" name="tva_intracomm" position="5">
        <attribute defType="com.stambia.json.value.type" id="_EtTZple_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZp1e_EemGruvsZ9L3ag" name="type_etab_1" position="6">
        <attribute defType="com.stambia.json.value.type" id="_EtTZqFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZqVe_EemGruvsZ9L3ag" name="type_etab_2" position="7">
        <attribute defType="com.stambia.json.value.type" id="_EtTZqle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZq1e_EemGruvsZ9L3ag" name="type_etab_3" position="8">
        <attribute defType="com.stambia.json.value.type" id="_EtTZrFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZrVe_EemGruvsZ9L3ag" name="specialites_culinaires" position="9">
        <attribute defType="com.stambia.json.value.type" id="_EtTZrle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZr1e_EemGruvsZ9L3ag" name="niveau_standing" position="10">
        <attribute defType="com.stambia.json.value.type" id="_EtTZsFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZsVe_EemGruvsZ9L3ag" name="repas_servis" position="11">
        <attribute defType="com.stambia.json.value.type" id="_EtTZsle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZs1e_EemGruvsZ9L3ag" name="prestations" position="12">
        <attribute defType="com.stambia.json.value.type" id="_EtTZtFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZtVe_EemGruvsZ9L3ag" name="langue_principale" position="13">
        <attribute defType="com.stambia.json.value.type" id="_EtTZtle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZt1e_EemGruvsZ9L3ag" name="nb_couverts_jour" position="14">
        <attribute defType="com.stambia.json.value.type" id="_EtTZuFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZuVe_EemGruvsZ9L3ag" name="nb_repas_jour" position="15">
        <attribute defType="com.stambia.json.value.type" id="_EtTZule_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZu1e_EemGruvsZ9L3ag" name="nb_couchages" position="16">
        <attribute defType="com.stambia.json.value.type" id="_EtTZvFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZvVe_EemGruvsZ9L3ag" name="nb_lits" position="17">
        <attribute defType="com.stambia.json.value.type" id="_EtTZvle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZv1e_EemGruvsZ9L3ag" name="nb_eleves" position="18">
        <attribute defType="com.stambia.json.value.type" id="_EtTZwFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZwVe_EemGruvsZ9L3ag" name="type_compte" position="19">
        <attribute defType="com.stambia.json.value.type" id="_EtTZwle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZw1e_EemGruvsZ9L3ag" name="num_adherent" position="20">
        <attribute defType="com.stambia.json.value.type" id="_EtTZxFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZxVe_EemGruvsZ9L3ag" name="env_tourist" position="21">
        <attribute defType="com.stambia.json.value.type" id="_EtTZxle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZx1e_EemGruvsZ9L3ag" name="annees_existence" position="22">
        <attribute defType="com.stambia.json.value.type" id="_EtTZyFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZyVe_EemGruvsZ9L3ag" name="scoring_ca" position="23">
        <attribute defType="com.stambia.json.value.type" id="_EtTZyle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZy1e_EemGruvsZ9L3ag" name="statut_client" position="24">
        <attribute defType="com.stambia.json.value.type" id="_EtTZzFe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZzVe_EemGruvsZ9L3ag" name="niveau_fidelite" position="25">
        <attribute defType="com.stambia.json.value.type" id="_EtTZzle_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZz1e_EemGruvsZ9L3ag" name="comportement" position="26">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ0Fe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ0Ve_EemGruvsZ9L3ag" name="demat_factures" position="27">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ0le_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ01e_EemGruvsZ9L3ag" name="email_fact_demat" position="28">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ1Fe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ1Ve_EemGruvsZ9L3ag" name="jours_d_ouverture" position="29">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ1le_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ11e_EemGruvsZ9L3ag" name="couleur" position="30">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ2Fe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ2Ve_EemGruvsZ9L3ag" name="mode_paiement" position="31">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ2le_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ21e_EemGruvsZ9L3ag" name="frais_de_port" position="32">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ3Fe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ3Ve_EemGruvsZ9L3ag" name="free_shipping" position="33">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ3le_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ31e_EemGruvsZ9L3ag" name="minimum_commande" position="34">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ4Fe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ4Ve_EemGruvsZ9L3ag" name="type_livraison" position="35">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ4le_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ41e_EemGruvsZ9L3ag" name="instruction_livraison" position="36">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ5Fe_EemGruvsZ9L3ag" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_EtTZ5Ve_EemGruvsZ9L3ag" name="reference_commande_obligatoire" position="37">
        <attribute defType="com.stambia.json.value.type" id="_EtTZ5le_EemGruvsZ9L3ag" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.xml.propertyField" id="_sr8cs1e_EemGruvsZ9L3ag" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_vgUo0Fe_EemGruvsZ9L3ag" value="fileName"/>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_7FghoRtvEeqaN66_aen_tw" name="wsSociete_IN">
    <attribute defType="com.stambia.json.rootObject.encoding" id="_AXcVUBtwEeqaN66_aen_tw" value="UTF-8"/>
    <attribute defType="com.stambia.json.rootObject.filePath" id="_DBm1gBtwEeqaN66_aen_tw" value="${~/IN_FILE_NAME}$"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_IyLtABtwEeqaN66_aen_tw" value="C:\app_dev\Used_Files\In_Files\Json\Societe_IN.json"/>
    <node defType="com.stambia.json.object" id="_JzW2SBtwEeqaN66_aen_tw" name="livraison" position="2">
      <node defType="com.stambia.json.value" id="_JzW2SRtwEeqaN66_aen_tw" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ShtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2SxtwEeqaN66_aen_tw" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_JzW2TBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2TRtwEeqaN66_aen_tw" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ThtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2TxtwEeqaN66_aen_tw" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_JzW2UBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2URtwEeqaN66_aen_tw" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_JzW2UhtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2UxtwEeqaN66_aen_tw" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_JzW2VBtwEeqaN66_aen_tw" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_JzW2YhtwEeqaN66_aen_tw" name="activite" position="4">
      <node defType="com.stambia.json.value" id="_JzW2YxtwEeqaN66_aen_tw" name="siret" position="1">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ZBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2ZRtwEeqaN66_aen_tw" name="code_client" position="2">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ZhtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2ZxtwEeqaN66_aen_tw" name="fax" position="3">
        <attribute defType="com.stambia.json.value.type" id="_JzW2aBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2aRtwEeqaN66_aen_tw" name="mercuriale" position="4">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ahtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2axtwEeqaN66_aen_tw" name="centralisateur_gescom" position="5">
        <attribute defType="com.stambia.json.value.type" id="_JzW2bBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2bRtwEeqaN66_aen_tw" name="chaine_gescom" position="6">
        <attribute defType="com.stambia.json.value.type" id="_JzW2bhtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2bxtwEeqaN66_aen_tw" name="groupe_gescom" position="7">
        <attribute defType="com.stambia.json.value.type" id="_JzW2cBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2cRtwEeqaN66_aen_tw" name="tva_intracomm" position="8">
        <attribute defType="com.stambia.json.value.type" id="_JzW2chtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2cxtwEeqaN66_aen_tw" name="type_etab_1" position="9">
        <attribute defType="com.stambia.json.value.type" id="_JzW2dBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2dRtwEeqaN66_aen_tw" name="type_etab_2" position="10">
        <attribute defType="com.stambia.json.value.type" id="_JzW2dhtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2dxtwEeqaN66_aen_tw" name="type_etab_3" position="11">
        <attribute defType="com.stambia.json.value.type" id="_JzW2eBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2eRtwEeqaN66_aen_tw" name="specialites_culinaires" position="12">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ehtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2extwEeqaN66_aen_tw" name="niveau_standing" position="13">
        <attribute defType="com.stambia.json.value.type" id="_JzW2fBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2fRtwEeqaN66_aen_tw" name="repas_servis" position="14">
        <attribute defType="com.stambia.json.value.type" id="_JzW2fhtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2fxtwEeqaN66_aen_tw" name="prestations" position="15">
        <attribute defType="com.stambia.json.value.type" id="_JzW2gBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2gRtwEeqaN66_aen_tw" name="langue_principale" position="16">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ghtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2gxtwEeqaN66_aen_tw" name="nb_couverts_jour" position="17">
        <attribute defType="com.stambia.json.value.type" id="_JzW2hBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2hRtwEeqaN66_aen_tw" name="nb_repas_jour" position="18">
        <attribute defType="com.stambia.json.value.type" id="_JzW2hhtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2hxtwEeqaN66_aen_tw" name="nb_couchages" position="19">
        <attribute defType="com.stambia.json.value.type" id="_JzW2iBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2iRtwEeqaN66_aen_tw" name="nb_lits" position="20">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ihtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2ixtwEeqaN66_aen_tw" name="nb_eleves" position="21">
        <attribute defType="com.stambia.json.value.type" id="_JzW2jBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2jRtwEeqaN66_aen_tw" name="type_compte" position="22">
        <attribute defType="com.stambia.json.value.type" id="_JzW2jhtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2jxtwEeqaN66_aen_tw" name="num_adherent" position="23">
        <attribute defType="com.stambia.json.value.type" id="_JzW2kBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2kRtwEeqaN66_aen_tw" name="env_tourist" position="24">
        <attribute defType="com.stambia.json.value.type" id="_JzW2khtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2kxtwEeqaN66_aen_tw" name="annees_existence" position="25">
        <attribute defType="com.stambia.json.value.type" id="_JzW2lBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2lRtwEeqaN66_aen_tw" name="scoring_ca" position="26">
        <attribute defType="com.stambia.json.value.type" id="_JzW2lhtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2lxtwEeqaN66_aen_tw" name="statut_client" position="27">
        <attribute defType="com.stambia.json.value.type" id="_JzW2mBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2mRtwEeqaN66_aen_tw" name="niveau_fidelite" position="28">
        <attribute defType="com.stambia.json.value.type" id="_JzW2mhtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2mxtwEeqaN66_aen_tw" name="comportement" position="29">
        <attribute defType="com.stambia.json.value.type" id="_JzW2nBtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2nRtwEeqaN66_aen_tw" name="demat_factures" position="30">
        <attribute defType="com.stambia.json.value.type" id="_JzW2nhtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2nxtwEeqaN66_aen_tw" name="email_fact_demat" position="31">
        <attribute defType="com.stambia.json.value.type" id="_JzW2oBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2oRtwEeqaN66_aen_tw" name="jours_d_ouverture" position="32">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ohtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2oxtwEeqaN66_aen_tw" name="couleur" position="33">
        <attribute defType="com.stambia.json.value.type" id="_JzW2pBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2pRtwEeqaN66_aen_tw" name="mode_paiement" position="34">
        <attribute defType="com.stambia.json.value.type" id="_JzW2phtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2pxtwEeqaN66_aen_tw" name="frais_de_port" position="35">
        <attribute defType="com.stambia.json.value.type" id="_JzW2qBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2qRtwEeqaN66_aen_tw" name="free_shipping" position="36">
        <attribute defType="com.stambia.json.value.type" id="_JzW2qhtwEeqaN66_aen_tw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2qxtwEeqaN66_aen_tw" name="minimum_commande" position="37">
        <attribute defType="com.stambia.json.value.type" id="_JzW2rBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2rRtwEeqaN66_aen_tw" name="type_livraison" position="38">
        <attribute defType="com.stambia.json.value.type" id="_JzW2rhtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2rxtwEeqaN66_aen_tw" name="instruction_livraison" position="39">
        <attribute defType="com.stambia.json.value.type" id="_JzW2sBtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2sRtwEeqaN66_aen_tw" name="reference_commande_obligatoire" position="40">
        <attribute defType="com.stambia.json.value.type" id="_JzW2shtwEeqaN66_aen_tw" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_JzW2IRtwEeqaN66_aen_tw" name="Identification" position="1">
      <node defType="com.stambia.json.value" id="_JzW2IhtwEeqaN66_aen_tw" name="website_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_JzW2IxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2JBtwEeqaN66_aen_tw" name="store_id" position="2">
        <attribute defType="com.stambia.json.value.type" id="_JzW2JRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2JhtwEeqaN66_aen_tw" name="id_client_chomette" position="3">
        <attribute defType="com.stambia.json.value.type" id="_JzW2JxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2KBtwEeqaN66_aen_tw" name="id_societe_magento" position="4">
        <attribute defType="com.stambia.json.value.type" id="_JzW2KRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2KhtwEeqaN66_aen_tw" name="email_admin" position="5">
        <attribute defType="com.stambia.json.value.type" id="_JzW2KxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2LBtwEeqaN66_aen_tw" name="email_societe" position="6">
        <attribute defType="com.stambia.json.value.type" id="_JzW2LRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2LhtwEeqaN66_aen_tw" name="email_bis" position="7">
        <attribute defType="com.stambia.json.value.type" id="_JzW2LxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2MBtwEeqaN66_aen_tw" name="commercial" position="8">
        <attribute defType="com.stambia.json.value.type" id="_JzW2MRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2MhtwEeqaN66_aen_tw" name="tarif" position="9">
        <attribute defType="com.stambia.json.value.type" id="_JzW2MxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2NBtwEeqaN66_aen_tw" name="raison_sociale" position="10">
        <attribute defType="com.stambia.json.value.type" id="_JzW2NRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2NhtwEeqaN66_aen_tw" name="enseigne" position="11">
        <attribute defType="com.stambia.json.value.type" id="_JzW2NxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2OBtwEeqaN66_aen_tw" name="nom" position="12">
        <attribute defType="com.stambia.json.value.type" id="_JzW2ORtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2OhtwEeqaN66_aen_tw" name="prenom" position="13">
        <attribute defType="com.stambia.json.value.type" id="_JzW2OxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2PBtwEeqaN66_aen_tw" name="rue1" position="14">
        <attribute defType="com.stambia.json.value.type" id="_JzW2PRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2PhtwEeqaN66_aen_tw" name="rue2" position="15">
        <attribute defType="com.stambia.json.value.type" id="_JzW2PxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2QBtwEeqaN66_aen_tw" name="ville" position="16">
        <attribute defType="com.stambia.json.value.type" id="_JzW2QRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2QhtwEeqaN66_aen_tw" name="pays" position="17">
        <attribute defType="com.stambia.json.value.type" id="_JzW2QxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2RBtwEeqaN66_aen_tw" name="code_postal" position="18">
        <attribute defType="com.stambia.json.value.type" id="_JzW2RRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2RhtwEeqaN66_aen_tw" name="tel" position="19">
        <attribute defType="com.stambia.json.value.type" id="_JzW2RxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_DuEQJYDsEeuJS-kZ1MuueQ" name="sponsorship" position="20">
        <attribute defType="com.stambia.json.value.type" id="_DuEQJoDsEeuJS-kZ1MuueQ" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_JzW2VRtwEeqaN66_aen_tw" name="facturation" position="3">
      <node defType="com.stambia.json.value" id="_JzW2VhtwEeqaN66_aen_tw" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_JzW2VxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2WBtwEeqaN66_aen_tw" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_JzW2WRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2WhtwEeqaN66_aen_tw" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_JzW2WxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2XBtwEeqaN66_aen_tw" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_JzW2XRtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2XhtwEeqaN66_aen_tw" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_JzW2XxtwEeqaN66_aen_tw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_JzW2YBtwEeqaN66_aen_tw" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_JzW2YRtwEeqaN66_aen_tw" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.xml.propertyField" id="_OWJE0xtwEeqaN66_aen_tw" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_PguBkBtwEeqaN66_aen_tw" value="fileName"/>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_yW8toBxBEeqXWqeEPUsEYw" name="JSONSOCIETE_WS_IN">
    <attribute defType="com.stambia.json.rootObject.encoding" id="_yXWWQBxBEeqXWqeEPUsEYw" value="UTF-8"/>
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_yXkYsBxBEeqXWqeEPUsEYw"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_yXkYsRxBEeqXWqeEPUsEYw" value="C:\app_dev\Used_Files\In_Files\Json\Societe_IN.json"/>
    <attribute defType="com.stambia.json.rootObject.filePath" id="_82ufMBxBEeqXWqeEPUsEYw" value="//servernt46/Stambia/Chomette.com/Import/Societes/Encours/${~/NomFichierImport}$"/>
    <externalize defType="com.stambia.json.rootObject.regexpSelector" enable="false"/>
    <node defType="com.stambia.json.object" id="_9_CLQxxBEeqXWqeEPUsEYw" name="facturation" position="3">
      <node defType="com.stambia.json.value" id="_9_CLRBxBEeqXWqeEPUsEYw" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_9_CLRRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLRhxBEeqXWqeEPUsEYw" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_9_CLRxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLSBxBEeqXWqeEPUsEYw" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_9_CLSRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLShxBEeqXWqeEPUsEYw" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_9_CLSxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLTBxBEeqXWqeEPUsEYw" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_9_CLTRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLThxBEeqXWqeEPUsEYw" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_9_CLTxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_9_CLNhxBEeqXWqeEPUsEYw" name="livraison" position="2">
      <node defType="com.stambia.json.value" id="_9_CLNxxBEeqXWqeEPUsEYw" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_9_CLOBxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLORxBEeqXWqeEPUsEYw" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_9_CLOhxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLOxxBEeqXWqeEPUsEYw" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_9_CLPBxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLPRxBEeqXWqeEPUsEYw" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_9_CLPhxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLPxxBEeqXWqeEPUsEYw" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_9_CLQBxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLQRxBEeqXWqeEPUsEYw" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_9_CLQhxBEeqXWqeEPUsEYw" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_9_BkARxBEeqXWqeEPUsEYw" name="Identification" position="1">
      <node defType="com.stambia.json.value" id="_9_CLEBxBEeqXWqeEPUsEYw" name="website_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_9_CLERxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLEhxBEeqXWqeEPUsEYw" name="store_id" position="2">
        <attribute defType="com.stambia.json.value.type" id="_9_CLExxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLFBxBEeqXWqeEPUsEYw" name="id_client_chomette" position="3">
        <attribute defType="com.stambia.json.value.type" id="_9_CLFRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLFhxBEeqXWqeEPUsEYw" name="id_societe_magento" position="4">
        <attribute defType="com.stambia.json.value.type" id="_9_CLFxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLGBxBEeqXWqeEPUsEYw" name="email_admin" position="5">
        <attribute defType="com.stambia.json.value.type" id="_9_CLGRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLGhxBEeqXWqeEPUsEYw" name="email_societe" position="6">
        <attribute defType="com.stambia.json.value.type" id="_9_CLGxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLHBxBEeqXWqeEPUsEYw" name="email_bis" position="7">
        <attribute defType="com.stambia.json.value.type" id="_9_CLHRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLHhxBEeqXWqeEPUsEYw" name="commercial" position="8">
        <attribute defType="com.stambia.json.value.type" id="_9_CLHxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLIBxBEeqXWqeEPUsEYw" name="tarif" position="9">
        <attribute defType="com.stambia.json.value.type" id="_9_CLIRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLIhxBEeqXWqeEPUsEYw" name="raison_sociale" position="10">
        <attribute defType="com.stambia.json.value.type" id="_9_CLIxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLJBxBEeqXWqeEPUsEYw" name="enseigne" position="11">
        <attribute defType="com.stambia.json.value.type" id="_9_CLJRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLJhxBEeqXWqeEPUsEYw" name="nom" position="12">
        <attribute defType="com.stambia.json.value.type" id="_9_CLJxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLKBxBEeqXWqeEPUsEYw" name="prenom" position="13">
        <attribute defType="com.stambia.json.value.type" id="_9_CLKRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLKhxBEeqXWqeEPUsEYw" name="rue1" position="14">
        <attribute defType="com.stambia.json.value.type" id="_9_CLKxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLLBxBEeqXWqeEPUsEYw" name="rue2" position="15">
        <attribute defType="com.stambia.json.value.type" id="_9_CLLRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLLhxBEeqXWqeEPUsEYw" name="ville" position="16">
        <attribute defType="com.stambia.json.value.type" id="_9_CLLxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLMBxBEeqXWqeEPUsEYw" name="pays" position="17">
        <attribute defType="com.stambia.json.value.type" id="_9_CLMRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLMhxBEeqXWqeEPUsEYw" name="code_postal" position="18">
        <attribute defType="com.stambia.json.value.type" id="_9_CLMxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLNBxBEeqXWqeEPUsEYw" name="tel" position="19">
        <attribute defType="com.stambia.json.value.type" id="_9_CLNRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_-oZ1eoD5EeuX366VcVFClg" name="sponsorship" position="20">
        <attribute defType="com.stambia.json.value.type" id="_-oZ1e4D5EeuX366VcVFClg" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_9_CLUBxBEeqXWqeEPUsEYw" name="activite" position="4">
      <node defType="com.stambia.json.value" id="_9_CLURxBEeqXWqeEPUsEYw" name="siret" position="1">
        <attribute defType="com.stambia.json.value.type" id="_9_CLUhxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLUxxBEeqXWqeEPUsEYw" name="code_client" position="2">
        <attribute defType="com.stambia.json.value.type" id="_9_CLVBxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLVRxBEeqXWqeEPUsEYw" name="fax" position="3">
        <attribute defType="com.stambia.json.value.type" id="_9_CLVhxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLVxxBEeqXWqeEPUsEYw" name="mercuriale" position="4">
        <attribute defType="com.stambia.json.value.type" id="_9_CLWBxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLWRxBEeqXWqeEPUsEYw" name="centralisateur_gescom" position="5">
        <attribute defType="com.stambia.json.value.type" id="_9_CLWhxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLWxxBEeqXWqeEPUsEYw" name="chaine_gescom" position="6">
        <attribute defType="com.stambia.json.value.type" id="_9_CLXBxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLXRxBEeqXWqeEPUsEYw" name="groupe_gescom" position="7">
        <attribute defType="com.stambia.json.value.type" id="_9_CLXhxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CLXxxBEeqXWqeEPUsEYw" name="tva_intracomm" position="8">
        <attribute defType="com.stambia.json.value.type" id="_9_CLYBxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyIBxBEeqXWqeEPUsEYw" name="type_etab_1" position="9">
        <attribute defType="com.stambia.json.value.type" id="_9_CyIRxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyIhxBEeqXWqeEPUsEYw" name="type_etab_2" position="10">
        <attribute defType="com.stambia.json.value.type" id="_9_CyIxxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyJBxBEeqXWqeEPUsEYw" name="type_etab_3" position="11">
        <attribute defType="com.stambia.json.value.type" id="_9_CyJRxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyJhxBEeqXWqeEPUsEYw" name="specialites_culinaires" position="12">
        <attribute defType="com.stambia.json.value.type" id="_9_CyJxxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyKBxBEeqXWqeEPUsEYw" name="niveau_standing" position="13">
        <attribute defType="com.stambia.json.value.type" id="_9_CyKRxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyKhxBEeqXWqeEPUsEYw" name="repas_servis" position="14">
        <attribute defType="com.stambia.json.value.type" id="_9_CyKxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyLBxBEeqXWqeEPUsEYw" name="prestations" position="15">
        <attribute defType="com.stambia.json.value.type" id="_9_CyLRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyLhxBEeqXWqeEPUsEYw" name="langue_principale" position="16">
        <attribute defType="com.stambia.json.value.type" id="_9_CyLxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyMBxBEeqXWqeEPUsEYw" name="nb_couverts_jour" position="17">
        <attribute defType="com.stambia.json.value.type" id="_9_CyMRxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyMhxBEeqXWqeEPUsEYw" name="nb_repas_jour" position="18">
        <attribute defType="com.stambia.json.value.type" id="_9_CyMxxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyNBxBEeqXWqeEPUsEYw" name="nb_couchages" position="19">
        <attribute defType="com.stambia.json.value.type" id="_9_CyNRxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyNhxBEeqXWqeEPUsEYw" name="nb_lits" position="20">
        <attribute defType="com.stambia.json.value.type" id="_9_CyNxxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyOBxBEeqXWqeEPUsEYw" name="nb_eleves" position="21">
        <attribute defType="com.stambia.json.value.type" id="_9_CyORxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyOhxBEeqXWqeEPUsEYw" name="type_compte" position="22">
        <attribute defType="com.stambia.json.value.type" id="_9_CyOxxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyPBxBEeqXWqeEPUsEYw" name="num_adherent" position="23">
        <attribute defType="com.stambia.json.value.type" id="_9_CyPRxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyPhxBEeqXWqeEPUsEYw" name="env_tourist" position="24">
        <attribute defType="com.stambia.json.value.type" id="_9_CyPxxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyQBxBEeqXWqeEPUsEYw" name="annees_existence" position="25">
        <attribute defType="com.stambia.json.value.type" id="_9_CyQRxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyQhxBEeqXWqeEPUsEYw" name="scoring_ca" position="26">
        <attribute defType="com.stambia.json.value.type" id="_9_CyQxxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyRBxBEeqXWqeEPUsEYw" name="statut_client" position="27">
        <attribute defType="com.stambia.json.value.type" id="_9_CyRRxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyRhxBEeqXWqeEPUsEYw" name="niveau_fidelite" position="28">
        <attribute defType="com.stambia.json.value.type" id="_9_CyRxxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CySBxBEeqXWqeEPUsEYw" name="comportement" position="29">
        <attribute defType="com.stambia.json.value.type" id="_9_CySRxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyShxBEeqXWqeEPUsEYw" name="demat_factures" position="30">
        <attribute defType="com.stambia.json.value.type" id="_9_CySxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyTBxBEeqXWqeEPUsEYw" name="email_fact_demat" position="31">
        <attribute defType="com.stambia.json.value.type" id="_9_CyTRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyThxBEeqXWqeEPUsEYw" name="jours_d_ouverture" position="32">
        <attribute defType="com.stambia.json.value.type" id="_9_CyTxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyUBxBEeqXWqeEPUsEYw" name="couleur" position="33">
        <attribute defType="com.stambia.json.value.type" id="_9_CyURxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyUhxBEeqXWqeEPUsEYw" name="mode_paiement" position="34">
        <attribute defType="com.stambia.json.value.type" id="_9_CyUxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyVBxBEeqXWqeEPUsEYw" name="frais_de_port" position="35">
        <attribute defType="com.stambia.json.value.type" id="_9_CyVRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyVhxBEeqXWqeEPUsEYw" name="free_shipping" position="36">
        <attribute defType="com.stambia.json.value.type" id="_9_CyVxxBEeqXWqeEPUsEYw" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyWBxBEeqXWqeEPUsEYw" name="minimum_commande" position="37">
        <attribute defType="com.stambia.json.value.type" id="_9_CyWRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyWhxBEeqXWqeEPUsEYw" name="type_livraison" position="38">
        <attribute defType="com.stambia.json.value.type" id="_9_CyWxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyXBxBEeqXWqeEPUsEYw" name="instruction_livraison" position="39">
        <attribute defType="com.stambia.json.value.type" id="_9_CyXRxBEeqXWqeEPUsEYw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_9_CyXhxBEeqXWqeEPUsEYw" name="reference_commande_obligatoire" position="40">
        <attribute defType="com.stambia.json.value.type" id="_9_CyXxxBEeqXWqeEPUsEYw" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.xml.propertyField" id="_8SPSExxBEeqXWqeEPUsEYw" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_Bkt4UBxCEeqXWqeEPUsEYw" value="fileName"/>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_ThCD8DLrEeqF3cPVFXUsDw" name="wsSocietes">
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_YXFM4DLrEeqF3cPVFXUsDw" value="C:\app_dev\Used_Files\In_Files\Json\wsSocietes.json"/>
    <node defType="com.stambia.json.object" id="_aHBggTLrEeqF3cPVFXUsDw" name="information" position="1">
      <node defType="com.stambia.json.array" id="_aHBggjLrEeqF3cPVFXUsDw" name="data" position="1">
        <node defType="com.stambia.json.object" id="_aHBggzLrEeqF3cPVFXUsDw" name="item" position="1">
          <node defType="com.stambia.json.object" id="_aHBghDLrEeqF3cPVFXUsDw" name="identification" position="1">
            <node defType="com.stambia.json.value" id="_aHBghTLrEeqF3cPVFXUsDw" name="website_id" position="1">
              <attribute defType="com.stambia.json.value.type" id="_aHBghjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBghzLrEeqF3cPVFXUsDw" name="store_id" position="2">
              <attribute defType="com.stambia.json.value.type" id="_aHBgiDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgiTLrEeqF3cPVFXUsDw" name="id_client_chomette" position="3">
              <attribute defType="com.stambia.json.value.type" id="_aHBgijLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgizLrEeqF3cPVFXUsDw" name="id_societe_magento" position="4">
              <attribute defType="com.stambia.json.value.type" id="_aHBgjDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgjTLrEeqF3cPVFXUsDw" name="email_admin" position="5">
              <attribute defType="com.stambia.json.value.type" id="_aHBgjjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgjzLrEeqF3cPVFXUsDw" name="email_societe" position="6">
              <attribute defType="com.stambia.json.value.type" id="_aHBgkDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgkTLrEeqF3cPVFXUsDw" name="email_bis" position="7">
              <attribute defType="com.stambia.json.value.type" id="_aHBgkjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgkzLrEeqF3cPVFXUsDw" name="commercial" position="8">
              <attribute defType="com.stambia.json.value.type" id="_aHBglDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBglTLrEeqF3cPVFXUsDw" name="tarif" position="9">
              <attribute defType="com.stambia.json.value.type" id="_aHBgljLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBglzLrEeqF3cPVFXUsDw" name="raison_sociale" position="10">
              <attribute defType="com.stambia.json.value.type" id="_aHBgmDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgmTLrEeqF3cPVFXUsDw" name="enseigne" position="11">
              <attribute defType="com.stambia.json.value.type" id="_aHBgmjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgmzLrEeqF3cPVFXUsDw" name="nom" position="12">
              <attribute defType="com.stambia.json.value.type" id="_aHBgnDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgnTLrEeqF3cPVFXUsDw" name="prenom" position="13">
              <attribute defType="com.stambia.json.value.type" id="_aHBgnjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgnzLrEeqF3cPVFXUsDw" name="rue1" position="14">
              <attribute defType="com.stambia.json.value.type" id="_aHBgoDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgoTLrEeqF3cPVFXUsDw" name="rue2" position="15">
              <attribute defType="com.stambia.json.value.type" id="_aHBgojLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgozLrEeqF3cPVFXUsDw" name="ville" position="16">
              <attribute defType="com.stambia.json.value.type" id="_aHBgpDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgpTLrEeqF3cPVFXUsDw" name="pays" position="17">
              <attribute defType="com.stambia.json.value.type" id="_aHBgpjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgpzLrEeqF3cPVFXUsDw" name="code_postal" position="18">
              <attribute defType="com.stambia.json.value.type" id="_aHBgqDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgqTLrEeqF3cPVFXUsDw" name="tel" position="19">
              <attribute defType="com.stambia.json.value.type" id="_aHBgqjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
          </node>
          <node defType="com.stambia.json.object" id="_aHBgqzLrEeqF3cPVFXUsDw" name="livraison" position="2">
            <node defType="com.stambia.json.value" id="_aHBgrDLrEeqF3cPVFXUsDw" name="rue1" position="1">
              <attribute defType="com.stambia.json.value.type" id="_aHBgrTLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgrjLrEeqF3cPVFXUsDw" name="rue2" position="2">
              <attribute defType="com.stambia.json.value.type" id="_aHBgrzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgsDLrEeqF3cPVFXUsDw" name="ville" position="3">
              <attribute defType="com.stambia.json.value.type" id="_aHBgsTLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgsjLrEeqF3cPVFXUsDw" name="pays" position="4">
              <attribute defType="com.stambia.json.value.type" id="_aHBgszLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgtDLrEeqF3cPVFXUsDw" name="code_postal" position="5">
              <attribute defType="com.stambia.json.value.type" id="_aHBgtTLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgtjLrEeqF3cPVFXUsDw" name="tel" position="6">
              <attribute defType="com.stambia.json.value.type" id="_aHBgtzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
          </node>
          <node defType="com.stambia.json.object" id="_aHBguDLrEeqF3cPVFXUsDw" name="facturation" position="3">
            <node defType="com.stambia.json.value" id="_aHBguTLrEeqF3cPVFXUsDw" name="rue1" position="1">
              <attribute defType="com.stambia.json.value.type" id="_aHBgujLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBguzLrEeqF3cPVFXUsDw" name="rue2" position="2">
              <attribute defType="com.stambia.json.value.type" id="_aHBgvDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgvTLrEeqF3cPVFXUsDw" name="ville" position="3">
              <attribute defType="com.stambia.json.value.type" id="_aHBgvjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgvzLrEeqF3cPVFXUsDw" name="pays" position="4">
              <attribute defType="com.stambia.json.value.type" id="_aHBgwDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgwTLrEeqF3cPVFXUsDw" name="code_postal" position="5">
              <attribute defType="com.stambia.json.value.type" id="_aHBgwjLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgwzLrEeqF3cPVFXUsDw" name="tel" position="6">
              <attribute defType="com.stambia.json.value.type" id="_aHBgxDLrEeqF3cPVFXUsDw" value="string"/>
            </node>
          </node>
          <node defType="com.stambia.json.object" id="_aHBgxTLrEeqF3cPVFXUsDw" name="activite" position="4">
            <node defType="com.stambia.json.value" id="_aHBgxjLrEeqF3cPVFXUsDw" name="siret" position="1">
              <attribute defType="com.stambia.json.value.type" id="_aHBgxzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgyDLrEeqF3cPVFXUsDw" name="code_client" position="2">
              <attribute defType="com.stambia.json.value.type" id="_aHBgyTLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgyjLrEeqF3cPVFXUsDw" name="mercuriale" position="3">
              <attribute defType="com.stambia.json.value.type" id="_aHBgyzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgzDLrEeqF3cPVFXUsDw" name="centralisateur_gescom" position="4">
              <attribute defType="com.stambia.json.value.type" id="_aHBgzTLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBgzjLrEeqF3cPVFXUsDw" name="chaine_gescom" position="5">
              <attribute defType="com.stambia.json.value.type" id="_aHBgzzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg0DLrEeqF3cPVFXUsDw" name="groupe_gescom" position="6">
              <attribute defType="com.stambia.json.value.type" id="_aHBg0TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg0jLrEeqF3cPVFXUsDw" name="fax" position="7">
              <attribute defType="com.stambia.json.value.type" id="_aHBg0zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg1DLrEeqF3cPVFXUsDw" name="tva_intracomm" position="8">
              <attribute defType="com.stambia.json.value.type" id="_aHBg1TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg1jLrEeqF3cPVFXUsDw" name="type_etab_1" position="9">
              <attribute defType="com.stambia.json.value.type" id="_aHBg1zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg2DLrEeqF3cPVFXUsDw" name="type_etab_2" position="10">
              <attribute defType="com.stambia.json.value.type" id="_aHBg2TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg2jLrEeqF3cPVFXUsDw" name="type_etab_3" position="11">
              <attribute defType="com.stambia.json.value.type" id="_aHBg2zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg3DLrEeqF3cPVFXUsDw" name="specialites_culinaires" position="12">
              <attribute defType="com.stambia.json.value.type" id="_aHBg3TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg3jLrEeqF3cPVFXUsDw" name="niveau_standing" position="13">
              <attribute defType="com.stambia.json.value.type" id="_aHBg3zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg4DLrEeqF3cPVFXUsDw" name="repas_servis" position="14">
              <attribute defType="com.stambia.json.value.type" id="_aHBg4TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg4jLrEeqF3cPVFXUsDw" name="prestations" position="15">
              <attribute defType="com.stambia.json.value.type" id="_aHBg4zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg5DLrEeqF3cPVFXUsDw" name="langue_principale" position="16">
              <attribute defType="com.stambia.json.value.type" id="_aHBg5TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg5jLrEeqF3cPVFXUsDw" name="nb_couverts_jour" position="17">
              <attribute defType="com.stambia.json.value.type" id="_aHBg5zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg6DLrEeqF3cPVFXUsDw" name="nb_repas_jour" position="18">
              <attribute defType="com.stambia.json.value.type" id="_aHBg6TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg6jLrEeqF3cPVFXUsDw" name="nb_couchages" position="19">
              <attribute defType="com.stambia.json.value.type" id="_aHBg6zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg7DLrEeqF3cPVFXUsDw" name="nb_lits" position="20">
              <attribute defType="com.stambia.json.value.type" id="_aHBg7TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg7jLrEeqF3cPVFXUsDw" name="nb_eleves" position="21">
              <attribute defType="com.stambia.json.value.type" id="_aHBg7zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg8DLrEeqF3cPVFXUsDw" name="type_compte" position="22">
              <attribute defType="com.stambia.json.value.type" id="_aHBg8TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg8jLrEeqF3cPVFXUsDw" name="num_adherent" position="23">
              <attribute defType="com.stambia.json.value.type" id="_aHBg8zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg9DLrEeqF3cPVFXUsDw" name="env_tourist" position="24">
              <attribute defType="com.stambia.json.value.type" id="_aHBg9TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg9jLrEeqF3cPVFXUsDw" name="annees_existence" position="25">
              <attribute defType="com.stambia.json.value.type" id="_aHBg9zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg-DLrEeqF3cPVFXUsDw" name="scoring_ca" position="26">
              <attribute defType="com.stambia.json.value.type" id="_aHBg-TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg-jLrEeqF3cPVFXUsDw" name="statut_client" position="27">
              <attribute defType="com.stambia.json.value.type" id="_aHBg-zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg_DLrEeqF3cPVFXUsDw" name="niveau_fidelite" position="28">
              <attribute defType="com.stambia.json.value.type" id="_aHBg_TLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBg_jLrEeqF3cPVFXUsDw" name="comportement" position="29">
              <attribute defType="com.stambia.json.value.type" id="_aHBg_zLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhADLrEeqF3cPVFXUsDw" name="demat_factures" position="30">
              <attribute defType="com.stambia.json.value.type" id="_aHBhATLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhAjLrEeqF3cPVFXUsDw" name="email_fact_demat" position="31">
              <attribute defType="com.stambia.json.value.type" id="_aHBhAzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhBDLrEeqF3cPVFXUsDw" name="jours_d_ouverture" position="32">
              <attribute defType="com.stambia.json.value.type" id="_aHBhBTLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhBjLrEeqF3cPVFXUsDw" name="couleur" position="33">
              <attribute defType="com.stambia.json.value.type" id="_aHBhBzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhCDLrEeqF3cPVFXUsDw" name="mode_paiement" position="34">
              <attribute defType="com.stambia.json.value.type" id="_aHBhCTLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhCjLrEeqF3cPVFXUsDw" name="frais_de_port" position="35">
              <attribute defType="com.stambia.json.value.type" id="_aHBhCzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhDDLrEeqF3cPVFXUsDw" name="free_shipping" position="36">
              <attribute defType="com.stambia.json.value.type" id="_aHBhDTLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhDjLrEeqF3cPVFXUsDw" name="minimum_commande" position="37">
              <attribute defType="com.stambia.json.value.type" id="_aHBhDzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhEDLrEeqF3cPVFXUsDw" name="type_livraison" position="38">
              <attribute defType="com.stambia.json.value.type" id="_aHBhETLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhEjLrEeqF3cPVFXUsDw" name="instruction_livraison" position="39">
              <attribute defType="com.stambia.json.value.type" id="_aHBhEzLrEeqF3cPVFXUsDw" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_aHBhFDLrEeqF3cPVFXUsDw" name="reference_commande_obligatoire" position="40">
              <attribute defType="com.stambia.json.value.type" id="_aHBhFTLrEeqF3cPVFXUsDw" value="string"/>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node defType="com.stambia.json.rootArray" id="_APQssDYqEeqF3cPVFXUsDw" name="wsRetourSoc">
    <attribute defType="com.stambia.json.rootArray.encoding" id="_APumwDYqEeqF3cPVFXUsDw" value="UTF-8"/>
    <attribute defType="com.stambia.json.rootArray.reverseURLPath" id="_APyRIDYqEeqF3cPVFXUsDw"/>
    <attribute defType="com.stambia.json.rootArray.reverseFilePath" id="_APy4MDYqEeqF3cPVFXUsDw" value="C:\app_dev\Used_Files\In_Files\Json\wsretsoc.json"/>
    <node defType="com.stambia.json.object" id="_HRgncTYqEeqF3cPVFXUsDw" name="item" position="1">
      <node defType="com.stambia.json.value" id="_HRgncjYqEeqF3cPVFXUsDw" name="id_societe_chomette" position="1">
        <attribute defType="com.stambia.json.value.type" id="_HRgnczYqEeqF3cPVFXUsDw" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_HRgndDYqEeqF3cPVFXUsDw" name="id_societe_magento" position="2">
        <attribute defType="com.stambia.json.value.type" id="_HRgndTYqEeqF3cPVFXUsDw" value="string"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_Nd_sgH3MEeuJS-kZ1MuueQ" name="wsSociete_OUT">
    <attribute defType="com.stambia.json.rootObject.encoding" id="_Nd_sgX3MEeuJS-kZ1MuueQ" value="UTF-8"/>
    <attribute defType="com.stambia.json.rootObject.filePath" id="_Nd_sgn3MEeuJS-kZ1MuueQ" value="${~/IN_FILE_NAME}$"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_Nd_sg33MEeuJS-kZ1MuueQ" value="C:\app_dev\Used_Files\In_Files\Json\Societe.json"/>
    <node defType="com.stambia.json.object" id="_Nd_shH3MEeuJS-kZ1MuueQ" name="livraison" position="2">
      <node defType="com.stambia.json.value" id="_Nd_shX3MEeuJS-kZ1MuueQ" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_Nd_shn3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_Nd_sh33MEeuJS-kZ1MuueQ" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_Nd_siH3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_Nd_siX3MEeuJS-kZ1MuueQ" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_Nd_sin3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_Nd_si33MEeuJS-kZ1MuueQ" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_Nd_sjH3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_Nd_sjX3MEeuJS-kZ1MuueQ" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_Nd_sjn3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_Nd_sj33MEeuJS-kZ1MuueQ" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_Nd_skH3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_Nd_skX3MEeuJS-kZ1MuueQ" name="activite" position="4">
      <node defType="com.stambia.json.value" id="_Nd_skn3MEeuJS-kZ1MuueQ" name="siret" position="1">
        <attribute defType="com.stambia.json.value.type" id="_Nd_sk33MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_Nd_slH3MEeuJS-kZ1MuueQ" name="code_client" position="2">
        <attribute defType="com.stambia.json.value.type" id="_Nd_slX3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_Nd_sln3MEeuJS-kZ1MuueQ" name="fax" position="3">
        <attribute defType="com.stambia.json.value.type" id="_Nd_sl33MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeATkH3MEeuJS-kZ1MuueQ" name="mercuriale" position="4">
        <attribute defType="com.stambia.json.value.type" id="_NeATkX3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeATkn3MEeuJS-kZ1MuueQ" name="centralisateur_gescom" position="5">
        <attribute defType="com.stambia.json.value.type" id="_NeATk33MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeATlH3MEeuJS-kZ1MuueQ" name="chaine_gescom" position="6">
        <attribute defType="com.stambia.json.value.type" id="_NeATlX3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeATln3MEeuJS-kZ1MuueQ" name="groupe_gescom" position="7">
        <attribute defType="com.stambia.json.value.type" id="_NeATl33MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeATmH3MEeuJS-kZ1MuueQ" name="tva_intracomm" position="8">
        <attribute defType="com.stambia.json.value.type" id="_NeATmX3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeATmn3MEeuJS-kZ1MuueQ" name="type_etab_1" position="9">
        <attribute defType="com.stambia.json.value.type" id="_NeATm33MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeATnH3MEeuJS-kZ1MuueQ" name="type_etab_2" position="10">
        <attribute defType="com.stambia.json.value.type" id="_NeATnX3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeATnn3MEeuJS-kZ1MuueQ" name="type_etab_3" position="11">
        <attribute defType="com.stambia.json.value.type" id="_NeATn33MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeAToH3MEeuJS-kZ1MuueQ" name="specialites_culinaires" position="12">
        <attribute defType="com.stambia.json.value.type" id="_NeAToX3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeATon3MEeuJS-kZ1MuueQ" name="niveau_standing" position="13">
        <attribute defType="com.stambia.json.value.type" id="_NeATo33MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeA6oH3MEeuJS-kZ1MuueQ" name="repas_servis" position="14">
        <attribute defType="com.stambia.json.value.type" id="_NeA6oX3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeA6on3MEeuJS-kZ1MuueQ" name="prestations" position="15">
        <attribute defType="com.stambia.json.value.type" id="_NeA6o33MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeA6pH3MEeuJS-kZ1MuueQ" name="langue_principale" position="16">
        <attribute defType="com.stambia.json.value.type" id="_NeA6pX3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeA6pn3MEeuJS-kZ1MuueQ" name="nb_couverts_jour" position="17">
        <attribute defType="com.stambia.json.value.type" id="_NeA6p33MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeA6qH3MEeuJS-kZ1MuueQ" name="nb_repas_jour" position="18">
        <attribute defType="com.stambia.json.value.type" id="_NeA6qX3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeA6qn3MEeuJS-kZ1MuueQ" name="nb_couchages" position="19">
        <attribute defType="com.stambia.json.value.type" id="_NeA6q33MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeA6rH3MEeuJS-kZ1MuueQ" name="nb_lits" position="20">
        <attribute defType="com.stambia.json.value.type" id="_NeA6rX3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhsH3MEeuJS-kZ1MuueQ" name="nb_eleves" position="21">
        <attribute defType="com.stambia.json.value.type" id="_NeBhsX3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhsn3MEeuJS-kZ1MuueQ" name="type_compte" position="22">
        <attribute defType="com.stambia.json.value.type" id="_NeBhs33MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhtH3MEeuJS-kZ1MuueQ" name="num_adherent" position="23">
        <attribute defType="com.stambia.json.value.type" id="_NeBhtX3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhtn3MEeuJS-kZ1MuueQ" name="env_tourist" position="24">
        <attribute defType="com.stambia.json.value.type" id="_NeBht33MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhuH3MEeuJS-kZ1MuueQ" name="annees_existence" position="25">
        <attribute defType="com.stambia.json.value.type" id="_NeBhuX3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhun3MEeuJS-kZ1MuueQ" name="scoring_ca" position="26">
        <attribute defType="com.stambia.json.value.type" id="_NeBhu33MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhvH3MEeuJS-kZ1MuueQ" name="statut_client" position="27">
        <attribute defType="com.stambia.json.value.type" id="_NeBhvX3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhvn3MEeuJS-kZ1MuueQ" name="niveau_fidelite" position="28">
        <attribute defType="com.stambia.json.value.type" id="_NeBhv33MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhwH3MEeuJS-kZ1MuueQ" name="comportement" position="29">
        <attribute defType="com.stambia.json.value.type" id="_NeBhwX3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeBhwn3MEeuJS-kZ1MuueQ" name="demat_factures" position="30">
        <attribute defType="com.stambia.json.value.type" id="_NeCIwH3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCIwX3MEeuJS-kZ1MuueQ" name="email_fact_demat" position="31">
        <attribute defType="com.stambia.json.value.type" id="_NeCIwn3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCIw33MEeuJS-kZ1MuueQ" name="jours_d_ouverture" position="32">
        <attribute defType="com.stambia.json.value.type" id="_NeCIxH3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCIxX3MEeuJS-kZ1MuueQ" name="couleur" position="33">
        <attribute defType="com.stambia.json.value.type" id="_NeCIxn3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCIx33MEeuJS-kZ1MuueQ" name="mode_paiement" position="34">
        <attribute defType="com.stambia.json.value.type" id="_NeCIyH3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCIyX3MEeuJS-kZ1MuueQ" name="frais_de_port" position="35">
        <attribute defType="com.stambia.json.value.type" id="_NeCIyn3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCIy33MEeuJS-kZ1MuueQ" name="free_shipping" position="36">
        <attribute defType="com.stambia.json.value.type" id="_NeCIzH3MEeuJS-kZ1MuueQ" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCIzX3MEeuJS-kZ1MuueQ" name="minimum_commande" position="37">
        <attribute defType="com.stambia.json.value.type" id="_NeCIzn3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCIz33MEeuJS-kZ1MuueQ" name="type_livraison" position="38">
        <attribute defType="com.stambia.json.value.type" id="_NeCI0H3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCI0X3MEeuJS-kZ1MuueQ" name="instruction_livraison" position="39">
        <attribute defType="com.stambia.json.value.type" id="_NeCI0n3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCI033MEeuJS-kZ1MuueQ" name="reference_commande_obligatoire" position="40">
        <attribute defType="com.stambia.json.value.type" id="_NeCI1H3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_NeCI1X3MEeuJS-kZ1MuueQ" name="Identification" position="1">
      <node defType="com.stambia.json.value" id="_NeCI1n3MEeuJS-kZ1MuueQ" name="website_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_NeCI133MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCI2H3MEeuJS-kZ1MuueQ" name="store_id" position="2">
        <attribute defType="com.stambia.json.value.type" id="_NeCI2X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCI2n3MEeuJS-kZ1MuueQ" name="id_client_chomette" position="3">
        <attribute defType="com.stambia.json.value.type" id="_NeCI233MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCI3H3MEeuJS-kZ1MuueQ" name="id_societe_magento" position="4">
        <attribute defType="com.stambia.json.value.type" id="_NeCI3X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCI3n3MEeuJS-kZ1MuueQ" name="email_admin" position="5">
        <attribute defType="com.stambia.json.value.type" id="_NeCI333MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCI4H3MEeuJS-kZ1MuueQ" name="email_societe" position="6">
        <attribute defType="com.stambia.json.value.type" id="_NeCI4X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCv0H3MEeuJS-kZ1MuueQ" name="email_bis" position="7">
        <attribute defType="com.stambia.json.value.type" id="_NeCv0X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCv0n3MEeuJS-kZ1MuueQ" name="commercial" position="8">
        <attribute defType="com.stambia.json.value.type" id="_NeCv033MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCv1H3MEeuJS-kZ1MuueQ" name="tarif" position="9">
        <attribute defType="com.stambia.json.value.type" id="_NeCv1X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCv1n3MEeuJS-kZ1MuueQ" name="raison_sociale" position="10">
        <attribute defType="com.stambia.json.value.type" id="_NeCv133MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCv2H3MEeuJS-kZ1MuueQ" name="enseigne" position="11">
        <attribute defType="com.stambia.json.value.type" id="_NeCv2X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCv2n3MEeuJS-kZ1MuueQ" name="nom" position="12">
        <attribute defType="com.stambia.json.value.type" id="_NeCv233MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeCv3H3MEeuJS-kZ1MuueQ" name="prenom" position="13">
        <attribute defType="com.stambia.json.value.type" id="_NeCv3X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW4H3MEeuJS-kZ1MuueQ" name="rue1" position="14">
        <attribute defType="com.stambia.json.value.type" id="_NeDW4X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW4n3MEeuJS-kZ1MuueQ" name="rue2" position="15">
        <attribute defType="com.stambia.json.value.type" id="_NeDW433MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW5H3MEeuJS-kZ1MuueQ" name="ville" position="16">
        <attribute defType="com.stambia.json.value.type" id="_NeDW5X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW5n3MEeuJS-kZ1MuueQ" name="pays" position="17">
        <attribute defType="com.stambia.json.value.type" id="_NeDW533MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW6H3MEeuJS-kZ1MuueQ" name="code_postal" position="18">
        <attribute defType="com.stambia.json.value.type" id="_NeDW6X3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW6n3MEeuJS-kZ1MuueQ" name="tel" position="19">
        <attribute defType="com.stambia.json.value.type" id="_NeDW633MEeuJS-kZ1MuueQ" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.object" id="_NeDW7H3MEeuJS-kZ1MuueQ" name="facturation" position="3">
      <node defType="com.stambia.json.value" id="_NeDW7X3MEeuJS-kZ1MuueQ" name="rue1" position="1">
        <attribute defType="com.stambia.json.value.type" id="_NeDW7n3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW733MEeuJS-kZ1MuueQ" name="rue2" position="2">
        <attribute defType="com.stambia.json.value.type" id="_NeDW8H3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW8X3MEeuJS-kZ1MuueQ" name="ville" position="3">
        <attribute defType="com.stambia.json.value.type" id="_NeDW8n3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW833MEeuJS-kZ1MuueQ" name="pays" position="4">
        <attribute defType="com.stambia.json.value.type" id="_NeDW9H3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW9X3MEeuJS-kZ1MuueQ" name="code_postal" position="5">
        <attribute defType="com.stambia.json.value.type" id="_NeDW9n3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_NeDW933MEeuJS-kZ1MuueQ" name="tel" position="6">
        <attribute defType="com.stambia.json.value.type" id="_NeDW-H3MEeuJS-kZ1MuueQ" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.xml.propertyField" id="_NeDW-X3MEeuJS-kZ1MuueQ" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_NeDW-n3MEeuJS-kZ1MuueQ" value="fileName"/>
    </node>
  </node>
</md:node>