<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_oS-bECSrEemYGNC7g14LyQ" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_oYkFsCSrEemYGNC7g14LyQ" name="favoris">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_oZKioCSrEemYGNC7g14LyQ"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_oZKioSSrEemYGNC7g14LyQ" value="C:\app_dev\Used_Files\In_Files\Json\favoris.json"/>
    <node defType="com.stambia.json.array" id="_RlhEUSSuEemYGNC7g14LyQ" name="" position="1">
      <node defType="com.stambia.json.object" id="_RlhEUiSuEemYGNC7g14LyQ" name="item" position="1">
        <node defType="com.stambia.json.value" id="_RlhEUySuEemYGNC7g14LyQ" name="libelle" position="1">
          <attribute defType="com.stambia.json.value.type" id="_RlhEVCSuEemYGNC7g14LyQ" value="string"/>
        </node>
        <node defType="com.stambia.json.array" id="_RlhEVSSuEemYGNC7g14LyQ" name="contenu" position="2">
          <node defType="com.stambia.json.object" id="_RlhEViSuEemYGNC7g14LyQ" name="item" position="1">
            <node defType="com.stambia.json.value" id="_RlhEVySuEemYGNC7g14LyQ" name="" position="1">
              <attribute defType="com.stambia.json.value.type" id="_RlhEWCSuEemYGNC7g14LyQ" value="string"/>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>