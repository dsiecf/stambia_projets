<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_z0bqEAUEEemmM6PCleITdQ" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_z7bp8AUEEemmM6PCleITdQ" name="Societes">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_z7uk4AUEEemmM6PCleITdQ"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_z7uk4QUEEemmM6PCleITdQ" value="C:\app_dev\Used_Files\In_Files\Json\Societes.json"/>
    <node defType="com.stambia.json.array" id="_1NOB8QUEEemmM6PCleITdQ" name="Societe_List" position="1">
      <node defType="com.stambia.json.object" id="_1NOB8gUEEemmM6PCleITdQ" name="item" position="1">
        <node defType="com.stambia.json.value" id="_1NOB8wUEEemmM6PCleITdQ" name="id_societe_magento" position="1">
          <attribute defType="com.stambia.json.value.type" id="_1NOB9AUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOB9QUEEemmM6PCleITdQ" name="id_client_chomette" position="2">
          <attribute defType="com.stambia.json.value.type" id="_1NOB9gUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOB9wUEEemmM6PCleITdQ" name="raison_sociale" position="3">
          <attribute defType="com.stambia.json.value.type" id="_1NOB-AUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOB-QUEEemmM6PCleITdQ" name="enseigne" position="4">
          <attribute defType="com.stambia.json.value.type" id="_1NOB-gUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOB-wUEEemmM6PCleITdQ" name="rue1" position="5">
          <attribute defType="com.stambia.json.value.type" id="_1NOB_AUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOB_QUEEemmM6PCleITdQ" name="rue2" position="6">
          <attribute defType="com.stambia.json.value.type" id="_1NOB_gUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOB_wUEEemmM6PCleITdQ" name="code_postal" position="7">
          <attribute defType="com.stambia.json.value.type" id="_1NOCAAUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCAQUEEemmM6PCleITdQ" name="ville" position="8">
          <attribute defType="com.stambia.json.value.type" id="_1NOCAgUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCAwUEEemmM6PCleITdQ" name="pays" position="9">
          <attribute defType="com.stambia.json.value.type" id="_1NOCBAUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCBQUEEemmM6PCleITdQ" name="tel" position="10">
          <attribute defType="com.stambia.json.value.type" id="_1NOCBgUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCBwUEEemmM6PCleITdQ" name="fax" position="11">
          <attribute defType="com.stambia.json.value.type" id="_1NOCCAUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCCQUEEemmM6PCleITdQ" name="siret" position="12">
          <attribute defType="com.stambia.json.value.type" id="_1NOCCgUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCCwUEEemmM6PCleITdQ" name="mercuriale" position="13">
          <attribute defType="com.stambia.json.value.type" id="_1NOCDAUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCDQUEEemmM6PCleITdQ" name="tarif" position="14">
          <attribute defType="com.stambia.json.value.type" id="_1NOCDgUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCDwUEEemmM6PCleITdQ" name="couleur" position="15">
          <attribute defType="com.stambia.json.value.type" id="_1NOCEAUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCEQUEEemmM6PCleITdQ" name="mode_paiement" position="16">
          <attribute defType="com.stambia.json.value.type" id="_1NOCEgUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCEwUEEemmM6PCleITdQ" name="frais_de_port" position="17">
          <attribute defType="com.stambia.json.value.type" id="_1NOCFAUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCFQUEEemmM6PCleITdQ" name="minimum_commande" position="18">
          <attribute defType="com.stambia.json.value.type" id="_1NOCFgUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCFwUEEemmM6PCleITdQ" name="commercial" position="19">
          <attribute defType="com.stambia.json.value.type" id="_1NOCGAUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCGQUEEemmM6PCleITdQ" name="Type_livraison" position="20">
          <attribute defType="com.stambia.json.value.type" id="_1NOCGgUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCGwUEEemmM6PCleITdQ" name="Email_admin" position="21">
          <attribute defType="com.stambia.json.value.type" id="_1NOCHAUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCHQUEEemmM6PCleITdQ" name="Firstname" position="22">
          <attribute defType="com.stambia.json.value.type" id="_1NOCHgUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_1NOCHwUEEemmM6PCleITdQ" name="Lastname" position="23">
          <attribute defType="com.stambia.json.value.type" id="_1NOCIAUEEemmM6PCleITdQ" value="string"/>
        </node>
        <node defType="com.stambia.json.object" id="_1NOCIQUEEemmM6PCleITdQ" name="livraison" position="24">
          <node defType="com.stambia.json.value" id="_1NOCIgUEEemmM6PCleITdQ" name="raison_sociale" position="1">
            <attribute defType="com.stambia.json.value.type" id="_1NOCIwUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCJAUEEemmM6PCleITdQ" name="enseigne" position="2">
            <attribute defType="com.stambia.json.value.type" id="_1NOCJQUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCJgUEEemmM6PCleITdQ" name="rue1" position="3">
            <attribute defType="com.stambia.json.value.type" id="_1NOCJwUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCKAUEEemmM6PCleITdQ" name="rue2" position="4">
            <attribute defType="com.stambia.json.value.type" id="_1NOCKQUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCKgUEEemmM6PCleITdQ" name="code_postal" position="5">
            <attribute defType="com.stambia.json.value.type" id="_1NOCKwUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCLAUEEemmM6PCleITdQ" name="ville" position="6">
            <attribute defType="com.stambia.json.value.type" id="_1NOCLQUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCLgUEEemmM6PCleITdQ" name="pays" position="7">
            <attribute defType="com.stambia.json.value.type" id="_1NOCLwUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCMAUEEemmM6PCleITdQ" name="jours_d_ouverture" position="8">
            <attribute defType="com.stambia.json.value.type" id="_1NOCMQUEEemmM6PCleITdQ" value="string"/>
          </node>
        </node>
        <node defType="com.stambia.json.object" id="_1NOCMgUEEemmM6PCleITdQ" name="activite" position="25">
          <node defType="com.stambia.json.value" id="_1NOCMwUEEemmM6PCleITdQ" name="code_client" position="1">
            <attribute defType="com.stambia.json.value.type" id="_1NOCNAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCNQUEEemmM6PCleITdQ" name="tva_intracomm" position="2">
            <attribute defType="com.stambia.json.value.type" id="_1NOCNgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCNwUEEemmM6PCleITdQ" name="type_etab_1" position="3">
            <attribute defType="com.stambia.json.value.type" id="_1NOCOAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCOQUEEemmM6PCleITdQ" name="type_etab_2" position="4">
            <attribute defType="com.stambia.json.value.type" id="_1NOCOgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCOwUEEemmM6PCleITdQ" name="type_etab_3" position="5">
            <attribute defType="com.stambia.json.value.type" id="_1NOCPAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCPQUEEemmM6PCleITdQ" name="specialite_culinaire" position="6">
            <attribute defType="com.stambia.json.value.type" id="_1NOCPgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCPwUEEemmM6PCleITdQ" name="niveau_standing" position="7">
            <attribute defType="com.stambia.json.value.type" id="_1NOCQAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCQQUEEemmM6PCleITdQ" name="repas_servis" position="8">
            <attribute defType="com.stambia.json.value.type" id="_1NOCQgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCQwUEEemmM6PCleITdQ" name="prestations" position="9">
            <attribute defType="com.stambia.json.value.type" id="_1NOCRAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCRQUEEemmM6PCleITdQ" name="langue_principale" position="10">
            <attribute defType="com.stambia.json.value.type" id="_1NOCRgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCRwUEEemmM6PCleITdQ" name="nb_couverts_jour" position="11">
            <attribute defType="com.stambia.json.value.type" id="_1NOCSAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCSQUEEemmM6PCleITdQ" name="nb_repas_jour" position="12">
            <attribute defType="com.stambia.json.value.type" id="_1NOCSgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCSwUEEemmM6PCleITdQ" name="nb_couchages" position="13">
            <attribute defType="com.stambia.json.value.type" id="_1NOCTAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCTQUEEemmM6PCleITdQ" name="nb_lits" position="14">
            <attribute defType="com.stambia.json.value.type" id="_1NOCTgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCTwUEEemmM6PCleITdQ" name="nb_eleves" position="15">
            <attribute defType="com.stambia.json.value.type" id="_1NOCUAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCUQUEEemmM6PCleITdQ" name="type_compte" position="16">
            <attribute defType="com.stambia.json.value.type" id="_1NOCUgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCUwUEEemmM6PCleITdQ" name="num_adherent" position="17">
            <attribute defType="com.stambia.json.value.type" id="_1NOCVAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCVQUEEemmM6PCleITdQ" name="env_tourist" position="18">
            <attribute defType="com.stambia.json.value.type" id="_1NOCVgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCVwUEEemmM6PCleITdQ" name="annees_existence" position="19">
            <attribute defType="com.stambia.json.value.type" id="_1NOCWAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCWQUEEemmM6PCleITdQ" name="scoring_ca" position="20">
            <attribute defType="com.stambia.json.value.type" id="_1NOCWgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCWwUEEemmM6PCleITdQ" name="statut_client" position="21">
            <attribute defType="com.stambia.json.value.type" id="_1NOCXAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCXQUEEemmM6PCleITdQ" name="niveau_fidelite" position="22">
            <attribute defType="com.stambia.json.value.type" id="_1NOCXgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCXwUEEemmM6PCleITdQ" name="comportement" position="23">
            <attribute defType="com.stambia.json.value.type" id="_1NOCYAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCYQUEEemmM6PCleITdQ" name="demat_factures" position="24">
            <attribute defType="com.stambia.json.value.type" id="_1NOCYgUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCYwUEEemmM6PCleITdQ" name="email_fact_demat" position="25">
            <attribute defType="com.stambia.json.value.type" id="_1NOCZAUEEemmM6PCleITdQ" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_1NOCZQUEEemmM6PCleITdQ" name="jours_d_ouverture" position="26">
            <attribute defType="com.stambia.json.value.type" id="_1NOCZgUEEemmM6PCleITdQ" value="string"/>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>