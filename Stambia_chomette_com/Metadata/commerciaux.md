<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_nmi94EZgEem8hIAZ2622yA" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootArray" id="_ntcQEEZgEem8hIAZ2622yA" name="Commerciaux">
    <attribute defType="com.stambia.json.rootArray.reverseURLPath" id="_nuL28EZgEem8hIAZ2622yA"/>
    <attribute defType="com.stambia.json.rootArray.reverseFilePath" id="_nuL28UZgEem8hIAZ2622yA" value="C:\app_dev\Used_Files\In_Files\Json\Commerciaux.json"/>
    <node defType="com.stambia.json.object" id="_pk_nkUZgEem8hIAZ2622yA" name="item" position="1">
      <node defType="com.stambia.json.object" id="_pk_nkkZgEem8hIAZ2622yA" name="identification" position="1">
        <node defType="com.stambia.json.value" id="_pk_nk0ZgEem8hIAZ2622yA" name="username" position="1">
          <attribute defType="com.stambia.json.value.type" id="_pk_nlEZgEem8hIAZ2622yA" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pk_nlUZgEem8hIAZ2622yA" name="firstname" position="2">
          <attribute defType="com.stambia.json.value.type" id="_pk_nlkZgEem8hIAZ2622yA" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pk_nl0ZgEem8hIAZ2622yA" name="lastname" position="3">
          <attribute defType="com.stambia.json.value.type" id="_pk_nmEZgEem8hIAZ2622yA" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pk_nmUZgEem8hIAZ2622yA" name="email" position="4">
          <attribute defType="com.stambia.json.value.type" id="_pk_nmkZgEem8hIAZ2622yA" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pk_nm0ZgEem8hIAZ2622yA" name="image" position="5">
          <attribute defType="com.stambia.json.value.type" id="_pk_nnEZgEem8hIAZ2622yA" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pk_nnUZgEem8hIAZ2622yA" name="code" position="6">
          <attribute defType="com.stambia.json.value.type" id="_pk_nnkZgEem8hIAZ2622yA" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_pk_nn0ZgEem8hIAZ2622yA" name="password" position="7">
          <attribute defType="com.stambia.json.value.type" id="_pk_noEZgEem8hIAZ2622yA" value="string"/>
        </node>
      </node>
    </node>
  </node>
</md:node>