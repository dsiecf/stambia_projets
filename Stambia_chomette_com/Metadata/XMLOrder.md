<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.xml.xsd" id="_XJv5kAVMEem1quiE84ULVQ" name="order.xml" md:ref="platform:/plugin/com.indy.environment/.tech/file/default.xml.md#UUID_MD_XML_DEFAULT?fileId=UUID_MD_XML_DEFAULT$type=md$name=Xml?">
  <attribute defType="com.stambia.xml.xsd.xsdReverseVersion" id="_XJv5wQVMEem1quiE84ULVQ" value="1"/>
  <attribute defType="com.stambia.xml.xsd.xmlPath" id="_br6AsAVNEem1quiE84ULVQ" value="C:\app_dev\Used_Files\In_Files\Xml\Commande.xml"/>
  <attribute defType="com.stambia.xml.xsd.xsdPath" id="_cMrygAVNEem1quiE84ULVQ" value="C:\app_dev\Used_Files\In_Files\Xml\Commande.xsd"/>
  <attribute defType="com.stambia.xml.xsd.prefixForElement" id="_ekfXYAVNEem1quiE84ULVQ" value="qualified"/>
  <attribute defType="com.stambia.xml.xsd.prefixForAttribute" id="_ekfXYQVNEem1quiE84ULVQ" value="unqualified"/>
  <attribute defType="com.stambia.xml.xsd.targetNamespace" id="_ekfXYgVNEem1quiE84ULVQ"/>
  <node defType="com.stambia.xml.namespace" id="_ekVmZAVNEem1quiE84ULVQ" name="http://www.w3.org/2001/XMLSchema">
    <attribute defType="com.stambia.xml.namespace.prefix" id="_ekVmZQVNEem1quiE84ULVQ" value="xs"/>
  </node>
  <node defType="com.stambia.xml.root" id="_ekVmaQVNEem1quiE84ULVQ" name="Order" position="0">
    <node defType="com.stambia.xml.sequence" id="_ekVmagVNEem1quiE84ULVQ" position="3">
      <attribute defType="com.stambia.xml.sequence.minOccurs" id="_ekVmawVNEem1quiE84ULVQ" value="1"/>
      <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_ekVmbAVNEem1quiE84ULVQ" value="1"/>
      <node defType="com.stambia.xml.element" id="_ekVmbQVNEem1quiE84ULVQ" name="Header" position="0">
        <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmbgVNEem1quiE84ULVQ" value="1"/>
        <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmbwVNEem1quiE84ULVQ" value="1"/>
        <node defType="com.stambia.xml.sequence" id="_ekVmcAVNEem1quiE84ULVQ" position="3">
          <attribute defType="com.stambia.xml.sequence.minOccurs" id="_ekVmcQVNEem1quiE84ULVQ" value="1"/>
          <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_ekVmcgVNEem1quiE84ULVQ" value="1"/>
          <node defType="com.stambia.xml.element" id="_ekVmcwVNEem1quiE84ULVQ" name="OrderNb" position="0">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmdAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmdQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmdgVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmdwVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmeAVNEem1quiE84ULVQ" name="Company" position="1">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmeQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmegVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmewVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmfAVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmfQVNEem1quiE84ULVQ" name="ActionCode" position="2">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmfgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmfwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmgAVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmgQVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmggVNEem1quiE84ULVQ" name="EDI" position="3">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmgwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmhAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmhQVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmhgVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmhwVNEem1quiE84ULVQ" name="CustomerNb" position="4">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmiAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmiQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmigVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmiwVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmjAVNEem1quiE84ULVQ" name="OrderingDate" position="5">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmjQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmjgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmjwVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmkAVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmkQVNEem1quiE84ULVQ" name="OrderingTime" position="6">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmkgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmkwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmlAVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmlQVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmlgVNEem1quiE84ULVQ" name="HTAmount" position="7">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmlwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmmAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmmQVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmmgVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmmwVNEem1quiE84ULVQ" name="TTCAmount" position="8">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmnAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmnQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmngVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmnwVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmoAVNEem1quiE84ULVQ" name="CustomerRef" position="9">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmoQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmogVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuooAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuooQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmowVNEem1quiE84ULVQ" name="CreationDte" position="10">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmpAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmpQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmpgVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmpwVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmqAVNEem1quiE84ULVQ" name="CreationH" position="11">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmqQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmqgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmqwVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmrAVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmrQVNEem1quiE84ULVQ" name="Filler" position="12">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmrgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmrwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmsAVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmsQVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmsgVNEem1quiE84ULVQ" name="AdrDest" position="13">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmswVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmtAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuoogdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuoowdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmtQVNEem1quiE84ULVQ" name="OrderType" position="14">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmtgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmtwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmuAVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmuQVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmugVNEem1quiE84ULVQ" name="Canal" position="15">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmuwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmvAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmvQVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmvgVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmvwVNEem1quiE84ULVQ" name="Version" position="16">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmwAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmwQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVmwgVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVmwwVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmxAVNEem1quiE84ULVQ" name="PaymentMode" position="17">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmxQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmxgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuopAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuopQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmxwVNEem1quiE84ULVQ" name="EtsCode" position="18">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmyAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmyQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuopgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuopwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmygVNEem1quiE84ULVQ" name="DelivDate" position="19">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmywVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmzAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuoqAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuoqQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVmzQVNEem1quiE84ULVQ" name="AR" position="20">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVmzgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVmzwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuoqgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuoqwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm0AVNEem1quiE84ULVQ" name="SPCdt" position="21">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm0QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm0gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuorAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuorQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm0wVNEem1quiE84ULVQ" name="SndMode" position="22">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm1AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm1QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuorgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuorwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm1gVNEem1quiE84ULVQ" name="Quarantine" position="23">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm1wVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm2AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuosAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuosQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm2QVNEem1quiE84ULVQ" name="Charges" position="24">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm2gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm2wVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuosgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuoswdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm3AVNEem1quiE84ULVQ" name="Spinneret" position="25">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm3QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm3gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuotAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuotQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm3wVNEem1quiE84ULVQ" name="Currency" position="26">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm4AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm4QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuotgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuotwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm4gVNEem1quiE84ULVQ" name="OverallHT" position="27">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm4wVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm5AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuouAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuouQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm5QVNEem1quiE84ULVQ" name="Discount" position="28">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm5gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm5wVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuougdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuouwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm6AVNEem1quiE84ULVQ" name="TotalNbLn" position="29">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm6QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm6gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuovAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuovQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm6wVNEem1quiE84ULVQ" name="IntegrDte" position="30">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm7AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm7QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuovgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuovwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm7gVNEem1quiE84ULVQ" name="IntegrH" position="31">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm7wVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm8AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuowAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuowQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm8QVNEem1quiE84ULVQ" name="OrdOrion" position="32">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm8gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm8wVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuowgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuowwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm9AVNEem1quiE84ULVQ" name="OrdAquarel" position="33">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm9QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm9gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuoxAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuoxQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVm9wVNEem1quiE84ULVQ" name="VRPCode" position="34">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm-AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm-QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uNuoxgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uNuoxwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
        </node>
      </node>
      <node defType="com.stambia.xml.element" id="_ekVm-gVNEem1quiE84ULVQ" name="Articles" position="1">
        <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVm-wVNEem1quiE84ULVQ" value="1"/>
        <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVm_AVNEem1quiE84ULVQ" value="1"/>
        <node defType="com.stambia.xml.sequence" id="_ekVm_QVNEem1quiE84ULVQ" position="3">
          <attribute defType="com.stambia.xml.sequence.minOccurs" id="_ekVm_gVNEem1quiE84ULVQ" value="1"/>
          <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_ekVm_wVNEem1quiE84ULVQ" value="1"/>
          <node defType="com.stambia.xml.element" id="_ekVnAAVNEem1quiE84ULVQ" name="Article" position="0">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnAQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnAgVNEem1quiE84ULVQ" value="1"/>
            <node defType="com.stambia.xml.sequence" id="_ekVnAwVNEem1quiE84ULVQ" position="3">
              <attribute defType="com.stambia.xml.sequence.minOccurs" id="_ekVnBAVNEem1quiE84ULVQ" value="1"/>
              <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_ekVnBQVNEem1quiE84ULVQ" value="1"/>
              <node defType="com.stambia.xml.element" id="_ekVnBgVNEem1quiE84ULVQ" name="LnNb" position="0">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnBwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnCAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_ekVnCQVNEem1quiE84ULVQ" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_ekVnCgVNEem1quiE84ULVQ" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnCwVNEem1quiE84ULVQ" name="OrderNb" position="1">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnDAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnDQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_ekVnDgVNEem1quiE84ULVQ" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_ekVnDwVNEem1quiE84ULVQ" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnEAVNEem1quiE84ULVQ" name="Code" position="2">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnEQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnEgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_ekVnEwVNEem1quiE84ULVQ" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_ekVnFAVNEem1quiE84ULVQ" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnFQVNEem1quiE84ULVQ" name="Quantity" position="3">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnFgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnFwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_ekVnGAVNEem1quiE84ULVQ" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_ekVnGQVNEem1quiE84ULVQ" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnGgVNEem1quiE84ULVQ" name="HTUPrice" position="4">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnGwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnHAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_ekVnHQVNEem1quiE84ULVQ" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_ekVnHgVNEem1quiE84ULVQ" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnHwVNEem1quiE84ULVQ" name="CustomerNb" position="5">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnIAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnIQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_ekVnIgVNEem1quiE84ULVQ" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_ekVnIwVNEem1quiE84ULVQ" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnJAVNEem1quiE84ULVQ" name="CustomerRef" position="6">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnJQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnJgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uNuoyAdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uNuoyQdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnJwVNEem1quiE84ULVQ" name="VATRate" position="7">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnKAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnKQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_ekVnKgVNEem1quiE84ULVQ" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_ekVnKwVNEem1quiE84ULVQ" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnLAVNEem1quiE84ULVQ" name="ActionCode" position="8">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnLQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnLgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_ekVnLwVNEem1quiE84ULVQ" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_ekVnMAVNEem1quiE84ULVQ" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnMQVNEem1quiE84ULVQ" name="DiscountLn" position="9">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnMgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnMwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uNuoygdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uNuoywdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnNAVNEem1quiE84ULVQ" name="VATCode" position="10">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnNQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnNgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uNuozAdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uNuozQdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnNwVNEem1quiE84ULVQ" name="OrdAquarel" position="11">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnOAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnOQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uNuozgdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uNuozwdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnOgVNEem1quiE84ULVQ" name="ChargeCode" position="12">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnOwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnPAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uNuo0AdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uNuo0QdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnPQVNEem1quiE84ULVQ" name="FreeLn" position="13">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnPgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnPwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uNuo0gdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uNuo0wdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnQAVNEem1quiE84ULVQ" name="Rebate" position="14">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnQQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnQgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uNuo1AdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uNuo1QdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnQwVNEem1quiE84ULVQ" name="Mercurial" position="15">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnRAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnRQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uNuo1gdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uNuo1wdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnRgVNEem1quiE84ULVQ" name="FreeQte" position="16">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnRwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnSAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uN3ykAdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uN3ykQdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnSQVNEem1quiE84ULVQ" name="NetAmount" position="17">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnSgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnSwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uN3ykgdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uN3ykwdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnTAVNEem1quiE84ULVQ" name="TaxType" position="18">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnTQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnTgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uN3ylAdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uN3ylQdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnTwVNEem1quiE84ULVQ" name="DiscountCode" position="19">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnUAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnUQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uN3ylgdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uN3ylwdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnUgVNEem1quiE84ULVQ" name="Margin" position="20">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnUwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnVAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uN3ymAdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uN3ymQdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnVQVNEem1quiE84ULVQ" name="LnRate" position="21">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnVgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnVwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uN3ymgdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uN3ymwdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnWAVNEem1quiE84ULVQ" name="FreeVal" position="22">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnWQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnWgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uN3ynAdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uN3ynQdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnWwVNEem1quiE84ULVQ" name="Reason" position="23">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnXAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnXQVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uN3yngdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uN3ynwdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnXgVNEem1quiE84ULVQ" name="NetPrice" position="24">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnXwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnYAVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_uN3yoAdnEem4BaSelO6HHA" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_uN3yoQdnEem4BaSelO6HHA" value="xs:string"/>
              </node>
              <node defType="com.stambia.xml.element" id="_ekVnYQVNEem1quiE84ULVQ" name="Filler" position="25">
                <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnYgVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnYwVNEem1quiE84ULVQ" value="1"/>
                <attribute defType="com.stambia.xml.element.type" id="_ekVnZAVNEem1quiE84ULVQ" value="string"/>
                <attribute defType="com.stambia.xml.element.originalType" id="_ekVnZQVNEem1quiE84ULVQ" value="xs:string"/>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node defType="com.stambia.xml.element" id="_ekVnZgVNEem1quiE84ULVQ" name="Payment" position="2">
        <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnZwVNEem1quiE84ULVQ" value="1"/>
        <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnaAVNEem1quiE84ULVQ" value="1"/>
        <node defType="com.stambia.xml.sequence" id="_ekVnaQVNEem1quiE84ULVQ" position="3">
          <attribute defType="com.stambia.xml.sequence.minOccurs" id="_ekVnagVNEem1quiE84ULVQ" value="1"/>
          <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_ekVnawVNEem1quiE84ULVQ" value="1"/>
          <node defType="com.stambia.xml.element" id="_ekVnbAVNEem1quiE84ULVQ" name="PaymentType" position="0">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnbQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnbgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVnbwVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVncAVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVncQVNEem1quiE84ULVQ" name="MovementCode" position="1">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVncgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVncwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVndAVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVndQVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVndgVNEem1quiE84ULVQ" name="BurstingCode" position="2">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVndwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVneAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVneQVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVnegVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnewVNEem1quiE84ULVQ" name="PaymentDate" position="3">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnfAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnfQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVnfgVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVnfwVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVngAVNEem1quiE84ULVQ" name="Amount" position="4">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVngQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnggVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVngwVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVnhAVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnhQVNEem1quiE84ULVQ" name="OrderNb" position="5">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnhgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnhwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVniAVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVniQVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnigVNEem1quiE84ULVQ" name="CustomerNb" position="6">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVniwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnjAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVnjQVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVnjgVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnjwVNEem1quiE84ULVQ" name="Filler" position="7">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnkAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnkQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVnkgVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVnkwVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnlAVNEem1quiE84ULVQ" name="Company" position="8">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnlQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnlgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVnlwVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVnmAVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnmQVNEem1quiE84ULVQ" name="VRPCode" position="9">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnmgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnmwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yogdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yowdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnnAVNEem1quiE84ULVQ" name="OrdSolo" position="10">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnnQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnngVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3ypAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3ypQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnnwVNEem1quiE84ULVQ" name="LnNb" position="11">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnoAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnoQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVnogVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVnowVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
        </node>
      </node>
      <node defType="com.stambia.xml.element" id="_ekVnpAVNEem1quiE84ULVQ" name="Delivery" position="3">
        <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnpQVNEem1quiE84ULVQ" value="1"/>
        <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnpgVNEem1quiE84ULVQ" value="1"/>
        <node defType="com.stambia.xml.sequence" id="_ekVnpwVNEem1quiE84ULVQ" position="3">
          <attribute defType="com.stambia.xml.sequence.minOccurs" id="_ekVnqAVNEem1quiE84ULVQ" value="1"/>
          <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_ekVnqQVNEem1quiE84ULVQ" value="1"/>
          <node defType="com.stambia.xml.element" id="_ekVnqgVNEem1quiE84ULVQ" name="OrderNb" position="0">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnqwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnrAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVnrQVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVnrgVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnrwVNEem1quiE84ULVQ" name="CustomerRef" position="1">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnsAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnsQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3ypgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3ypwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnsgVNEem1quiE84ULVQ" name="Street1" position="2">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnswVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVntAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yqAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yqQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVntQVNEem1quiE84ULVQ" name="Street2" position="3">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVntgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVntwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yqgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yqwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnuAVNEem1quiE84ULVQ" name="ZipCode" position="4">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnuQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnugVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yrAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yrQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnuwVNEem1quiE84ULVQ" name="DeliveryOffice" position="5">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnvAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnvQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yrgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yrwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnvgVNEem1quiE84ULVQ" name="CountryCode" position="6">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnvwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnwAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3ysAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3ysQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnwQVNEem1quiE84ULVQ" name="CompanyName" position="7">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnwgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnwwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3ysgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yswdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVnxAVNEem1quiE84ULVQ" name="CustomerNb" position="8">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnxQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnxgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVnxwVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVnyAVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
        </node>
      </node>
      <node defType="com.stambia.xml.element" id="_ekVnyQVNEem1quiE84ULVQ" name="Comment" position="4">
        <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVnygVNEem1quiE84ULVQ" value="1"/>
        <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVnywVNEem1quiE84ULVQ" value="1"/>
        <node defType="com.stambia.xml.sequence" id="_ekVnzAVNEem1quiE84ULVQ" position="3">
          <attribute defType="com.stambia.xml.sequence.minOccurs" id="_ekVnzQVNEem1quiE84ULVQ" value="1"/>
          <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_ekVnzgVNEem1quiE84ULVQ" value="1"/>
          <node defType="com.stambia.xml.element" id="_ekVnzwVNEem1quiE84ULVQ" name="OrderNb" position="0">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn0AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn0QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVn0gVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVn0wVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVn1AVNEem1quiE84ULVQ" name="CustomerRef" position="1">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn1QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn1gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3ytAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3ytQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVn1wVNEem1quiE84ULVQ" name="Comment1" position="2">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn2AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn2QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3ytgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3ytwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVn2gVNEem1quiE84ULVQ" name="Comment2" position="3">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn2wVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn3AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yuAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yuQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVn3QVNEem1quiE84ULVQ" name="Comment3" position="4">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn3gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn3wVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yugdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yuwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVn4AVNEem1quiE84ULVQ" name="Comment4" position="5">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn4QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn4gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yvAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yvQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVn4wVNEem1quiE84ULVQ" name="ActionCode" position="6">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn5AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn5QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVn5gVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVn5wVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVn6AVNEem1quiE84ULVQ" name="AquarelCode" position="7">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn6QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn6gVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yvgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yvwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVn6wVNEem1quiE84ULVQ" name="CustomerNb" position="8">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn7AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn7QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVn7gVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVn7wVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
        </node>
      </node>
      <node defType="com.stambia.xml.element" id="_ekVn8AVNEem1quiE84ULVQ" name="Mark" position="5">
        <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn8QVNEem1quiE84ULVQ" value="1"/>
        <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn8gVNEem1quiE84ULVQ" value="1"/>
        <node defType="com.stambia.xml.sequence" id="_ekVn8wVNEem1quiE84ULVQ" position="3">
          <attribute defType="com.stambia.xml.sequence.minOccurs" id="_ekVn9AVNEem1quiE84ULVQ" value="1"/>
          <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_ekVn9QVNEem1quiE84ULVQ" value="1"/>
          <node defType="com.stambia.xml.element" id="_ekVn9gVNEem1quiE84ULVQ" name="OrderNb" position="0">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn9wVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn-AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVn-QVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVn-gVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVn-wVNEem1quiE84ULVQ" name="CustomerNb" position="1">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVn_AVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVn_QVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVn_gVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVn_wVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoAAVNEem1quiE84ULVQ" name="CustomerRef" position="2">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoAQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoAgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3ywAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3ywQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoAwVNEem1quiE84ULVQ" name="Mark1" position="3">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoBAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoBQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3ywgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3ywwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoBgVNEem1quiE84ULVQ" name="Mark2" position="4">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoBwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoCAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yxAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yxQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoCQVNEem1quiE84ULVQ" name="Mark3" position="5">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoCgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoCwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yxgdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yxwdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoDAVNEem1quiE84ULVQ" name="Mark4" position="6">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoDQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoDgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yyAdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uN3yyQdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoDwVNEem1quiE84ULVQ" name="Mark5" position="7">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoEAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoEQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uN3yygdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uOBjkAdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoEgVNEem1quiE84ULVQ" name="Mark6" position="8">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoEwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoFAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uOBjkQdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uOBjkgdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoFQVNEem1quiE84ULVQ" name="Mark7" position="9">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoFgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoFwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uOBjkwdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uOBjlAdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoGAVNEem1quiE84ULVQ" name="Mark8" position="10">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoGQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoGgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uOBjlQdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uOBjlgdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
        </node>
      </node>
      <node defType="com.stambia.xml.element" id="_ekVoGwVNEem1quiE84ULVQ" name="Chorus" position="6">
        <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoHAVNEem1quiE84ULVQ" value="1"/>
        <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoHQVNEem1quiE84ULVQ" value="1"/>
        <node defType="com.stambia.xml.sequence" id="_ekVoHgVNEem1quiE84ULVQ" position="3">
          <attribute defType="com.stambia.xml.sequence.minOccurs" id="_ekVoHwVNEem1quiE84ULVQ" value="1"/>
          <attribute defType="com.stambia.xml.sequence.maxOccurs" id="_ekVoIAVNEem1quiE84ULVQ" value="1"/>
          <node defType="com.stambia.xml.element" id="_ekVoIQVNEem1quiE84ULVQ" name="OrderNb" position="0">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoIgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoIwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVoJAVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVoJQVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoJgVNEem1quiE84ULVQ" name="CustomerNb" position="1">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoJwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoKAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_ekVoKQVNEem1quiE84ULVQ" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_ekVoKgVNEem1quiE84ULVQ" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoKwVNEem1quiE84ULVQ" name="CustomerRef" position="2">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoLAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoLQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uOBjlwdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uOBjmAdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoLgVNEem1quiE84ULVQ" name="CodeService" position="3">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoLwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoMAVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uOBjmQdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uOBjmgdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoMQVNEem1quiE84ULVQ" name="NumEngagement" position="4">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoMgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoMwVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uOBjmwdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uOBjnAdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
          <node defType="com.stambia.xml.element" id="_ekVoNAVNEem1quiE84ULVQ" name="CodeMarche" position="5">
            <attribute defType="com.stambia.xml.element.minOccurs" id="_ekVoNQVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.maxOccurs" id="_ekVoNgVNEem1quiE84ULVQ" value="1"/>
            <attribute defType="com.stambia.xml.element.type" id="_uOBjnQdnEem4BaSelO6HHA" value="string"/>
            <attribute defType="com.stambia.xml.element.originalType" id="_uOBjngdnEem4BaSelO6HHA" value="xs:string"/>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>