<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.function.folder" id="_OVd8oMBGEeq4q7DmXKq3zg" md:ref="platform:/plugin/com.indy.environment/technology/others/udf.md#UUID_MD_UDF?fileId=UUID_MD_UDF$type=md$name=User%20Defined%20Functions?">
  <attribute defType="com.stambia.function.folder.prefix" id="_VA554MBGEeq4q7DmXKq3zg" value="ChaineFormate"/>
  <node defType="com.stambia.function.function" id="_k5jNkMBGEeq4q7DmXKq3zg" name="Function01">
    <attribute defType="com.stambia.function.function.description" id="_qtzrMMBGEeq4q7DmXKq3zg" value="A test of string formating"/>
    <node defType="com.stambia.function.parameter" id="_lb660MBGEeq4q7DmXKq3zg" name="param01">
      <attribute defType="com.stambia.function.parameter.description" id="_01NZAMBGEeq4q7DmXKq3zg" value="String to Format"/>
    </node>
    <node defType="com.stambia.function.implementation" id="_aE_pYcBHEeq4q7DmXKq3zg" name="REPLACE">
      <attribute defType="com.stambia.function.implementation.expression" id="_jS15AMBHEeq4q7DmXKq3zg" value="replace(replace(replace(replace( rtrim(ltrim($param01)) ,'''',''),char(13),' '),char(10),' '),char(9),' ')"/>
    </node>
  </node>
</md:node>