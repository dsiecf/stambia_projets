<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_NE-68BPwEemsgZOghPO5Pg" md:ref="platform:/plugin/com.indy.environment/technology/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_NLKbcBPwEemsgZOghPO5Pg" name="Client">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_NLdWYBPwEemsgZOghPO5Pg"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_NLdWYRPwEemsgZOghPO5Pg" value="C:\app_dev\Used_Files\In_Files\Json\Client.json"/>
    <attribute defType="com.stambia.json.rootObject.filePath" id="_V3r58FqtEemVO-iMxXwWEg" value="\\servernt46\Stambia_Test\Chomette.com\Import\Clients\Encours\${~/FileName}$"/>
    <node defType="com.stambia.xml.propertyField" id="_OtfF41olEemVO-iMxXwWEg" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_RPfIkFolEemVO-iMxXwWEg" value="fileName"/>
    </node>
    <node defType="com.stambia.json.object" id="_orNSQXM2EemIMIaOcxSI7A" name="Identification" position="1">
      <node defType="com.stambia.json.value" id="_orNSQnM2EemIMIaOcxSI7A" name="website_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_orNSQ3M2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orNSRHM2EemIMIaOcxSI7A" name="id_client_magento" position="3">
        <attribute defType="com.stambia.json.value.type" id="_orNSRXM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orNSRnM2EemIMIaOcxSI7A" name="id_societe_magento" position="4">
        <attribute defType="com.stambia.json.value.type" id="_orNSR3M2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orNSSHM2EemIMIaOcxSI7A" name="id_client_chomette" position="5">
        <attribute defType="com.stambia.json.value.type" id="_orNSSXM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orNSSnM2EemIMIaOcxSI7A" name="id_societe_chomette" position="6">
        <attribute defType="com.stambia.json.value.type" id="_orNSS3M2EemIMIaOcxSI7A" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_orNSTHM2EemIMIaOcxSI7A" name="type" position="7">
        <attribute defType="com.stambia.json.value.type" id="_orN5UHM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orN5UXM2EemIMIaOcxSI7A" name="max_order" position="8">
        <attribute defType="com.stambia.json.value.type" id="_orN5UnM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orN5U3M2EemIMIaOcxSI7A" name="autorisation_order" position="9">
        <attribute defType="com.stambia.json.value.type" id="_orN5VHM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orN5VXM2EemIMIaOcxSI7A" name="civilite" position="10">
        <attribute defType="com.stambia.json.value.type" id="_orN5VnM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orN5V3M2EemIMIaOcxSI7A" name="nom" position="11">
        <attribute defType="com.stambia.json.value.type" id="_orN5WHM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orN5WXM2EemIMIaOcxSI7A" name="prenom" position="12">
        <attribute defType="com.stambia.json.value.type" id="_orN5WnM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orN5W3M2EemIMIaOcxSI7A" name="email" position="13">
        <attribute defType="com.stambia.json.value.type" id="_orN5XHM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orN5XXM2EemIMIaOcxSI7A" name="telephone" position="14">
        <attribute defType="com.stambia.json.value.type" id="_orN5XnM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orN5X3M2EemIMIaOcxSI7A" name="mobile" position="15">
        <attribute defType="com.stambia.json.value.type" id="_orN5YHM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_orN5YXM2EemIMIaOcxSI7A" name="intitule_poste" position="16">
        <attribute defType="com.stambia.json.value.type" id="_orN5YnM2EemIMIaOcxSI7A" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_dccw5HVREema_LGoZACb8w" name="store_id" position="2">
        <attribute defType="com.stambia.json.value.type" id="_dccw5XVREema_LGoZACb8w" value="string"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_rsJFISC4Eeqn0Jtj2MvWfA" name="JSONCustomer_IN">
    <attribute defType="com.stambia.json.rootObject.filePath" id="_BmL6kCC5Eeqn0Jtj2MvWfA" value="//servernt46/Stambia/Chomette.com/Import/Clients/Encours/${~/NomFichierImport}$"/>
    <attribute defType="com.stambia.json.rootObject.encoding" id="_hLgigCC5Eeqn0Jtj2MvWfA" value="UTF-8"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_jCcO8CC5Eeqn0Jtj2MvWfA" value="C:\app_dev\Used_Files\In_Files\Json\Client.json"/>
    <node defType="com.stambia.json.object" id="_nrbzUSC5Eeqn0Jtj2MvWfA" name="Identification" position="1">
      <node defType="com.stambia.json.value" id="_nrbzUiC5Eeqn0Jtj2MvWfA" name="website_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_nrbzUyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzVCC5Eeqn0Jtj2MvWfA" name="store_id" position="2">
        <attribute defType="com.stambia.json.value.type" id="_nrbzVSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzViC5Eeqn0Jtj2MvWfA" name="id_client_magento" position="3">
        <attribute defType="com.stambia.json.value.type" id="_nrbzVyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzWCC5Eeqn0Jtj2MvWfA" name="id_societe_magento" position="4">
        <attribute defType="com.stambia.json.value.type" id="_nrbzWSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzWiC5Eeqn0Jtj2MvWfA" name="id_client_chomette" position="5">
        <attribute defType="com.stambia.json.value.type" id="_nrbzWyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzXCC5Eeqn0Jtj2MvWfA" name="id_societe_chomette" position="6">
        <attribute defType="com.stambia.json.value.type" id="_nrbzXSC5Eeqn0Jtj2MvWfA" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzXiC5Eeqn0Jtj2MvWfA" name="type" position="7">
        <attribute defType="com.stambia.json.value.type" id="_nrbzXyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzYCC5Eeqn0Jtj2MvWfA" name="max_order" position="8">
        <attribute defType="com.stambia.json.value.type" id="_nrbzYSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzYiC5Eeqn0Jtj2MvWfA" name="autorisation_order" position="9">
        <attribute defType="com.stambia.json.value.type" id="_nrbzYyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzZCC5Eeqn0Jtj2MvWfA" name="civilite" position="10">
        <attribute defType="com.stambia.json.value.type" id="_nrbzZSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzZiC5Eeqn0Jtj2MvWfA" name="nom" position="11">
        <attribute defType="com.stambia.json.value.type" id="_nrbzZyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzaCC5Eeqn0Jtj2MvWfA" name="prenom" position="12">
        <attribute defType="com.stambia.json.value.type" id="_nrbzaSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzaiC5Eeqn0Jtj2MvWfA" name="email" position="13">
        <attribute defType="com.stambia.json.value.type" id="_nrbzayC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzbCC5Eeqn0Jtj2MvWfA" name="telephone" position="14">
        <attribute defType="com.stambia.json.value.type" id="_nrbzbSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzbiC5Eeqn0Jtj2MvWfA" name="mobile" position="15">
        <attribute defType="com.stambia.json.value.type" id="_nrbzbyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_nrbzcCC5Eeqn0Jtj2MvWfA" name="intitule_poste" position="16">
        <attribute defType="com.stambia.json.value.type" id="_nrbzcSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.json.rootObject" id="_poRTcSC5Eeqn0Jtj2MvWfA" name="wsClient">
    <attribute defType="com.stambia.json.rootObject.filePath" id="_u0034CC5Eeqn0Jtj2MvWfA" value="${~/IN_FILE_NAME}$"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_wDbZACC5Eeqn0Jtj2MvWfA" value="C:\app_dev\Used_Files\In_Files\Json\Client.json"/>
    <attribute defType="com.stambia.json.rootObject.encoding" id="_xVgE4CC5Eeqn0Jtj2MvWfA" value="UTF-8"/>
    <node defType="com.stambia.json.object" id="_1LPnYSC5Eeqn0Jtj2MvWfA" name="Identification" position="1">
      <node defType="com.stambia.json.value" id="_1LPnYiC5Eeqn0Jtj2MvWfA" name="website_id" position="1">
        <attribute defType="com.stambia.json.value.type" id="_1LPnYyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPnZCC5Eeqn0Jtj2MvWfA" name="store_id" position="2">
        <attribute defType="com.stambia.json.value.type" id="_1LPnZSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPnZiC5Eeqn0Jtj2MvWfA" name="id_client_magento" position="3">
        <attribute defType="com.stambia.json.value.type" id="_1LPnZyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPnaCC5Eeqn0Jtj2MvWfA" name="id_societe_magento" position="4">
        <attribute defType="com.stambia.json.value.type" id="_1LPnaSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPnaiC5Eeqn0Jtj2MvWfA" name="id_client_chomette" position="5">
        <attribute defType="com.stambia.json.value.type" id="_1LPnayC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPnbCC5Eeqn0Jtj2MvWfA" name="id_societe_chomette" position="6">
        <attribute defType="com.stambia.json.value.type" id="_1LPnbSC5Eeqn0Jtj2MvWfA" value="number"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPnbiC5Eeqn0Jtj2MvWfA" name="type" position="7">
        <attribute defType="com.stambia.json.value.type" id="_1LPnbyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPncCC5Eeqn0Jtj2MvWfA" name="max_order" position="8">
        <attribute defType="com.stambia.json.value.type" id="_1LPncSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPnciC5Eeqn0Jtj2MvWfA" name="autorisation_order" position="9">
        <attribute defType="com.stambia.json.value.type" id="_1LPncyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPndCC5Eeqn0Jtj2MvWfA" name="civilite" position="10">
        <attribute defType="com.stambia.json.value.type" id="_1LPndSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPndiC5Eeqn0Jtj2MvWfA" name="nom" position="11">
        <attribute defType="com.stambia.json.value.type" id="_1LPndyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPneCC5Eeqn0Jtj2MvWfA" name="prenom" position="12">
        <attribute defType="com.stambia.json.value.type" id="_1LPneSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPneiC5Eeqn0Jtj2MvWfA" name="email" position="13">
        <attribute defType="com.stambia.json.value.type" id="_1LPneyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPnfCC5Eeqn0Jtj2MvWfA" name="telephone" position="14">
        <attribute defType="com.stambia.json.value.type" id="_1LPnfSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPnfiC5Eeqn0Jtj2MvWfA" name="mobile" position="15">
        <attribute defType="com.stambia.json.value.type" id="_1LPnfyC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_1LPngCC5Eeqn0Jtj2MvWfA" name="intitule_poste" position="16">
        <attribute defType="com.stambia.json.value.type" id="_1LPngSC5Eeqn0Jtj2MvWfA" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.xml.propertyField" id="_47LeAyDTEeqn0Jtj2MvWfA" name="NomFichier">
      <attribute defType="com.stambia.xml.propertyField.property" id="_7QNbsCDTEeqn0Jtj2MvWfA" value="fileName"/>
    </node>
  </node>
</md:node>