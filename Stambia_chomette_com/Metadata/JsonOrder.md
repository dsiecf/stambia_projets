<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_pvqwAAVFEem1quiE84ULVQ" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_p1PzkAVFEem1quiE84ULVQ" name="Commande">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_p2JLcAVFEem1quiE84ULVQ"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_p2JLcQVFEem1quiE84ULVQ" value="C:\app_dev\Used_Files\In_Files\Json\Commande.json"/>
    <attribute defType="com.stambia.json.rootObject.filePath" id="_D7YggAdrEem4BaSelO6HHA" value="C:\app_dev\Used_Files\In_Files\Json\Commande.json"/>
    <node defType="com.stambia.json.object" id="_rEdYuwVFEem1quiE84ULVQ" name="entete" position="6">
      <node defType="com.stambia.json.value" id="_rEdYvAVFEem1quiE84ULVQ" name="id_commande_magento" position="1">
        <attribute defType="com.stambia.json.value.type" id="_rEdYvQVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdYvgVFEem1quiE84ULVQ" name="id_commande_web" position="2">
        <attribute defType="com.stambia.json.value.type" id="_rEdYvwVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdYwAVFEem1quiE84ULVQ" name="mode_reglement" position="3">
        <attribute defType="com.stambia.json.value.type" id="_rEdYwQVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdYwgVFEem1quiE84ULVQ" name="reference_client" position="4">
        <attribute defType="com.stambia.json.value.type" id="_rEdYwwVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdYxAVFEem1quiE84ULVQ" name="date_creation" position="5">
        <attribute defType="com.stambia.json.value.type" id="_rEdYxQVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdYxgVFEem1quiE84ULVQ" name="heure_creation" position="6">
        <attribute defType="com.stambia.json.value.type" id="_rEdYxwVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdYyAVFEem1quiE84ULVQ" name="date_liv_souhaite" position="7">
        <attribute defType="com.stambia.json.value.type" id="_rEdYyQVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdYygVFEem1quiE84ULVQ" name="mnt_tva55" position="8">
        <attribute defType="com.stambia.json.value.type" id="_rEdYywVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdYzAVFEem1quiE84ULVQ" name="mnt_tva196" position="9">
        <attribute defType="com.stambia.json.value.type" id="_rEdYzQVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdYzgVFEem1quiE84ULVQ" name="mnt_tva_autre" position="10">
        <attribute defType="com.stambia.json.value.type" id="_rEdYzwVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY0AVFEem1quiE84ULVQ" name="mnt_ht" position="11">
        <attribute defType="com.stambia.json.value.type" id="_rEdY0QVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY0gVFEem1quiE84ULVQ" name="mnt_ttc" position="12">
        <attribute defType="com.stambia.json.value.type" id="_rEdY0wVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY1AVFEem1quiE84ULVQ" name="code_promo" position="13">
        <attribute defType="com.stambia.json.value.type" id="_rEdY1QVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY1gVFEem1quiE84ULVQ" name="nb_lignes" position="14">
        <attribute defType="com.stambia.json.value.type" id="_rEdY1wVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY2AVFEem1quiE84ULVQ" name="remise_%" position="15">
        <attribute defType="com.stambia.json.value.type" id="_rEdY2QVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY2gVFEem1quiE84ULVQ" name="remise_mnt_ht" position="16">
        <attribute defType="com.stambia.json.value.type" id="_rEdY2wVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY3AVFEem1quiE84ULVQ" name="mnt_fdp" position="17">
        <attribute defType="com.stambia.json.value.type" id="_rEdY3QVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY3gVFEem1quiE84ULVQ" name="instruction_livraison" position="18">
        <attribute defType="com.stambia.json.value.type" id="_rEdY3wVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY4AVFEem1quiE84ULVQ" name="commentaire" position="19">
        <attribute defType="com.stambia.json.value.type" id="_rEdY4QVFEem1quiE84ULVQ" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_rEdY4gVFEem1quiE84ULVQ" name="mnt_non_remise" position="20">
        <attribute defType="com.stambia.json.value.type" id="_rEdY4wVFEem1quiE84ULVQ" value="string"/>
      </node>
    </node>
    <node defType="com.stambia.json.value" id="_rEdYsQVFEem1quiE84ULVQ" name="type" position="1">
      <attribute defType="com.stambia.json.value.type" id="_rEdYsgVFEem1quiE84ULVQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_rEdYswVFEem1quiE84ULVQ" name="id_usermagento" position="2">
      <attribute defType="com.stambia.json.value.type" id="_rEdYtAVFEem1quiE84ULVQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_rEdYtQVFEem1quiE84ULVQ" name="id_societemagento" position="3">
      <attribute defType="com.stambia.json.value.type" id="_rEdYtgVFEem1quiE84ULVQ" value="string"/>
    </node>
    <node defType="com.stambia.json.array" id="_rEdY5AVFEem1quiE84ULVQ" name="lignes_articles" position="7">
      <node defType="com.stambia.json.object" id="_rEdY5QVFEem1quiE84ULVQ" name="item" position="1">
        <node defType="com.stambia.json.value" id="_rEdY5gVFEem1quiE84ULVQ" name="code_article" position="1">
          <attribute defType="com.stambia.json.value.type" id="_rEdY5wVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY6AVFEem1quiE84ULVQ" name="designation" position="2">
          <attribute defType="com.stambia.json.value.type" id="_rEdY6QVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY6gVFEem1quiE84ULVQ" name="conditionnement" position="3">
          <attribute defType="com.stambia.json.value.type" id="_rEdY6wVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY7AVFEem1quiE84ULVQ" name="quantite" position="4">
          <attribute defType="com.stambia.json.value.type" id="_rEdY7QVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY7gVFEem1quiE84ULVQ" name="prix_lot" position="5">
          <attribute defType="com.stambia.json.value.type" id="_rEdY7wVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY8AVFEem1quiE84ULVQ" name="prix_unitaire" position="6">
          <attribute defType="com.stambia.json.value.type" id="_rEdY8QVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY8gVFEem1quiE84ULVQ" name="prix_unitaire_remise" position="7">
          <attribute defType="com.stambia.json.value.type" id="_rEdY8wVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY9AVFEem1quiE84ULVQ" name="taxe_deee" position="8">
          <attribute defType="com.stambia.json.value.type" id="_rEdY9QVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY9gVFEem1quiE84ULVQ" name="taxe_tgap" position="9">
          <attribute defType="com.stambia.json.value.type" id="_rEdY9wVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY-AVFEem1quiE84ULVQ" name="taxe_ecopart" position="10">
          <attribute defType="com.stambia.json.value.type" id="_rEdY-QVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY-gVFEem1quiE84ULVQ" name="qmv" position="11">
          <attribute defType="com.stambia.json.value.type" id="_rEdY-wVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY_AVFEem1quiE84ULVQ" name="tva" position="12">
          <attribute defType="com.stambia.json.value.type" id="_rEdY_QVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdY_gVFEem1quiE84ULVQ" name="mnt_ht" position="13">
          <attribute defType="com.stambia.json.value.type" id="_rEdY_wVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdZAAVFEem1quiE84ULVQ" name="mnt_ttc" position="14">
          <attribute defType="com.stambia.json.value.type" id="_rEdZAQVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdZAgVFEem1quiE84ULVQ" name="mnt_ht_remise" position="15">
          <attribute defType="com.stambia.json.value.type" id="_rEdZAwVFEem1quiE84ULVQ" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_rEdZBAVFEem1quiE84ULVQ" name="mnt_ht_non_remise" position="16">
          <attribute defType="com.stambia.json.value.type" id="_rEdZBQVFEem1quiE84ULVQ" value="string"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.json.value" id="_rEdYtwVFEem1quiE84ULVQ" name="id_userCHOMETTE" position="4">
      <attribute defType="com.stambia.json.value.type" id="_rEdYuAVFEem1quiE84ULVQ" value="string"/>
    </node>
    <node defType="com.stambia.json.value" id="_rEdYuQVFEem1quiE84ULVQ" name="id_societeCHOMETTE" position="5">
      <attribute defType="com.stambia.json.value.type" id="_rEdYugVFEem1quiE84ULVQ" value="string"/>
    </node>
  </node>
</md:node>