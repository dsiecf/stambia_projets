<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_2m59MGOREeWUhqKZpf18yA" name="H2Engine" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/h2/h2.rdbms.md#UUID_MD_RDBMS_H2?fileId=UUID_MD_RDBMS_H2$type=md$name=H2+Database?">
  <attribute defType="com.stambia.rdbms.server.url" id="_2m59MWOREeWUhqKZpf18yA" value="jdbc:h2:mem:${/CORE_SESSION_ID}$"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_2m59MmOREeWUhqKZpf18yA" value="org.h2.Driver"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_2m59M2OREeWUhqKZpf18yA" value="3951C0D79B227B95C1DC348DD0BCE8F1"/>
  <externalize defType="com.stambia.rdbms.server.physicalName" enable="false"/>
  <externalize defType="com.stambia.rdbms.server.url" enable="false"/>
  <externalize defType="com.stambia.rdbms.server.user" enable="false"/>
  <externalize defType="com.stambia.rdbms.server.password" enable="false"/>
  <node defType="com.stambia.rdbms.schema" id="_2m6kQGOREeWUhqKZpf18yA" name="PUBLIC">
    <attribute defType="com.stambia.rdbms.schema.name" id="_2m6kQWOREeWUhqKZpf18yA" value="PUBLIC"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_2m6kQmOREeWUhqKZpf18yA" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_2m6kQ2OREeWUhqKZpf18yA" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_2m6kRGOREeWUhqKZpf18yA" value="I_[targetName]"/>
    <externalize defType="com.stambia.rdbms.schema.catalog.name" enable="false"/>
    <externalize defType="com.stambia.rdbms.schema.name" enable="false"/>
  </node>
  <node defType="com.stambia.rdbms.queryFolder" id="_2m6kRWOREeWUhqKZpf18yA" name="queryFolder">
    <node defType="com.stambia.rdbms.query" id="_2m6kRmOREeWUhqKZpf18yA" name="oneline">
      <attribute defType="com.stambia.rdbms.query.expression" id="_2m7LUGOREeWUhqKZpf18yA" value="select 1 field from dual"/>
      <node defType="com.stambia.rdbms.column" id="_2m7LUWOREeWUhqKZpf18yA" name="FIELD" position="1">
        <attribute defType="com.stambia.rdbms.column.type" id="_2m7LUmOREeWUhqKZpf18yA" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_2m7LU2OREeWUhqKZpf18yA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.name" id="_2m7LVGOREeWUhqKZpf18yA" value="FIELD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_2m7LVWOREeWUhqKZpf18yA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_2m7LVmOREeWUhqKZpf18yA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_2m7LV2OREeWUhqKZpf18yA" value="10"/>
      </node>
    </node>
  </node>
</md:node>