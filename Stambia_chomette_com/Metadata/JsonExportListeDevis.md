<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_CQSi0Ak7Eemca7hN01bE4g" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_CWi70Ak7Eemca7hN01bE4g" name="Devis">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_CW12wAk7Eemca7hN01bE4g"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_CW12wQk7Eemca7hN01bE4g" value="C:\app_dev\Used_Files\In_Files\Json\DevisExport.json"/>
    <node defType="com.stambia.json.array" id="_f6qoQUyrEemn-PTqagJaAg" name="Order_List" position="1">
      <node defType="com.stambia.json.object" id="_f6qoQkyrEemn-PTqagJaAg" name="item" position="1">
        <node defType="com.stambia.json.value" id="_f6qoQ0yrEemn-PTqagJaAg" name="type" position="1">
          <attribute defType="com.stambia.json.value.type" id="_f6qoREyrEemn-PTqagJaAg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_f6qoRUyrEemn-PTqagJaAg" name="id_usermagento" position="2">
          <attribute defType="com.stambia.json.value.type" id="_f6qoRkyrEemn-PTqagJaAg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_f6qoR0yrEemn-PTqagJaAg" name="id_societemagento" position="3">
          <attribute defType="com.stambia.json.value.type" id="_f6qoSEyrEemn-PTqagJaAg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_f6qoSUyrEemn-PTqagJaAg" name="id_userCHOMETTE" position="4">
          <attribute defType="com.stambia.json.value.type" id="_f6qoSkyrEemn-PTqagJaAg" value="string"/>
        </node>
        <node defType="com.stambia.json.value" id="_f6qoS0yrEemn-PTqagJaAg" name="id_societeCHOMETTE" position="5">
          <attribute defType="com.stambia.json.value.type" id="_f6qoTEyrEemn-PTqagJaAg" value="string"/>
        </node>
        <node defType="com.stambia.json.object" id="_f6qoTUyrEemn-PTqagJaAg" name="entete" position="6">
          <node defType="com.stambia.json.value" id="_f6qoTkyrEemn-PTqagJaAg" name="id_commande_magento" position="1">
            <attribute defType="com.stambia.json.value.type" id="_f6qoT0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoUEyrEemn-PTqagJaAg" name="id_commande_web" position="2">
            <attribute defType="com.stambia.json.value.type" id="_f6qoUUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoUkyrEemn-PTqagJaAg" name="mode_reglement" position="3">
            <attribute defType="com.stambia.json.value.type" id="_f6qoU0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoVEyrEemn-PTqagJaAg" name="reference_client" position="4">
            <attribute defType="com.stambia.json.value.type" id="_f6qoVUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoVkyrEemn-PTqagJaAg" name="date_creation" position="5">
            <attribute defType="com.stambia.json.value.type" id="_f6qoV0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoWEyrEemn-PTqagJaAg" name="heure_creation" position="6">
            <attribute defType="com.stambia.json.value.type" id="_f6qoWUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoWkyrEemn-PTqagJaAg" name="date_liv_souhaite" position="7">
            <attribute defType="com.stambia.json.value.type" id="_f6qoW0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoXEyrEemn-PTqagJaAg" name="mnt_tva55" position="8">
            <attribute defType="com.stambia.json.value.type" id="_f6qoXUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoXkyrEemn-PTqagJaAg" name="mnt_tva196" position="9">
            <attribute defType="com.stambia.json.value.type" id="_f6qoX0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoYEyrEemn-PTqagJaAg" name="mnt_tva_autre" position="10">
            <attribute defType="com.stambia.json.value.type" id="_f6qoYUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoYkyrEemn-PTqagJaAg" name="mnt_ht" position="11">
            <attribute defType="com.stambia.json.value.type" id="_f6qoY0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoZEyrEemn-PTqagJaAg" name="mnt_ttc" position="12">
            <attribute defType="com.stambia.json.value.type" id="_f6qoZUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoZkyrEemn-PTqagJaAg" name="code_promo" position="13">
            <attribute defType="com.stambia.json.value.type" id="_f6qoZ0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qoaEyrEemn-PTqagJaAg" name="nb_lignes" position="14">
            <attribute defType="com.stambia.json.value.type" id="_f6qoaUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qobEyrEemn-PTqagJaAg" name="remise_mnt_ht" position="16">
            <attribute defType="com.stambia.json.value.type" id="_f6qobUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qobkyrEemn-PTqagJaAg" name="mnt_fdp" position="17">
            <attribute defType="com.stambia.json.value.type" id="_f6qob0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qocEyrEemn-PTqagJaAg" name="instruction_livraison" position="18">
            <attribute defType="com.stambia.json.value.type" id="_f6qocUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qockyrEemn-PTqagJaAg" name="commentaire" position="19">
            <attribute defType="com.stambia.json.value.type" id="_f6qoc0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qodEyrEemn-PTqagJaAg" name="mnt_non_remise" position="20">
            <attribute defType="com.stambia.json.value.type" id="_f6qodUyrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_f6qodkyrEemn-PTqagJaAg" name="date_fin_validite" position="21">
            <attribute defType="com.stambia.json.value.type" id="_f6qod0yrEemn-PTqagJaAg" value="string"/>
          </node>
          <node defType="com.stambia.json.value" id="_4kPfqnrhEem-bPviwv5ZLw" name="remise_prc" position="15">
            <attribute defType="com.stambia.json.value.type" id="_4kPfq3rhEem-bPviwv5ZLw" value="string"/>
          </node>
        </node>
        <node defType="com.stambia.json.array" id="_f6qoeEyrEemn-PTqagJaAg" name="lignes_articles" position="7">
          <node defType="com.stambia.json.object" id="_f6qoeUyrEemn-PTqagJaAg" name="item" position="1">
            <node defType="com.stambia.json.value" id="_f6qoekyrEemn-PTqagJaAg" name="code_article" position="1">
              <attribute defType="com.stambia.json.value.type" id="_f6qoe0yrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qofEyrEemn-PTqagJaAg" name="designation" position="2">
              <attribute defType="com.stambia.json.value.type" id="_f6qofUyrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qofkyrEemn-PTqagJaAg" name="conditionnement" position="3">
              <attribute defType="com.stambia.json.value.type" id="_f6qof0yrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qogEyrEemn-PTqagJaAg" name="quantite" position="4">
              <attribute defType="com.stambia.json.value.type" id="_f6qogUyrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qogkyrEemn-PTqagJaAg" name="prix_lot" position="5">
              <attribute defType="com.stambia.json.value.type" id="_f6qog0yrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qohEyrEemn-PTqagJaAg" name="prix_unitaire" position="6">
              <attribute defType="com.stambia.json.value.type" id="_f6qohUyrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qohkyrEemn-PTqagJaAg" name="prix_unitaire_remise" position="7">
              <attribute defType="com.stambia.json.value.type" id="_f6qoh0yrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qoiEyrEemn-PTqagJaAg" name="taxe_deee" position="8">
              <attribute defType="com.stambia.json.value.type" id="_f6qoiUyrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qoikyrEemn-PTqagJaAg" name="taxe_tgap" position="9">
              <attribute defType="com.stambia.json.value.type" id="_f6qoi0yrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qojEyrEemn-PTqagJaAg" name="taxe_ecopart" position="10">
              <attribute defType="com.stambia.json.value.type" id="_f6qojUyrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qojkyrEemn-PTqagJaAg" name="qmv" position="11">
              <attribute defType="com.stambia.json.value.type" id="_f6qoj0yrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qokEyrEemn-PTqagJaAg" name="tva" position="12">
              <attribute defType="com.stambia.json.value.type" id="_f6qokUyrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qokkyrEemn-PTqagJaAg" name="mnt_ht" position="13">
              <attribute defType="com.stambia.json.value.type" id="_f6qok0yrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qolEyrEemn-PTqagJaAg" name="mnt_ttc" position="14">
              <attribute defType="com.stambia.json.value.type" id="_f6qolUyrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qolkyrEemn-PTqagJaAg" name="mnt_ht_remise" position="15">
              <attribute defType="com.stambia.json.value.type" id="_f6qol0yrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qomEyrEemn-PTqagJaAg" name="mnt_ht_non_remise" position="16">
              <attribute defType="com.stambia.json.value.type" id="_f6qomUyrEemn-PTqagJaAg" value="string"/>
            </node>
            <node defType="com.stambia.json.value" id="_f6qoqkyrEemn-PTqagJaAg" name="mnt_ttc_remise" position="17">
              <attribute defType="com.stambia.json.value.type" id="_f6qoq0yrEemn-PTqagJaAg" value="string"/>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</md:node>