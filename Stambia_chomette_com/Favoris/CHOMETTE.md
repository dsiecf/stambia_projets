<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_l7wCEM7uEem5ebMPg-wyFg" name="CHOMETTE" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/db2_400/ibm_db2_400.rdbms.md#UUID_MD_RDBMS_DB2_400?fileId=UUID_MD_RDBMS_DB2_400$type=md$name=IBM%20DB2/400?">
  <attribute defType="com.stambia.rdbms.server.url" id="_l7wpIM7uEem5ebMPg-wyFg" value="jdbc:as400://chomette"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_l7wpIc7uEem5ebMPg-wyFg" value="com.ibm.as400.access.AS400JDBCDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_l7wpIs7uEem5ebMPg-wyFg" value="gescom"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_l7wpI87uEem5ebMPg-wyFg" value="86E98B5A4EEE31BDBD8DF145839CA9DF"/>
  <node defType="com.stambia.rdbms.schema" id="_l7wpJM7uEem5ebMPg-wyFg" name="ORIONBD">
    <attribute defType="com.stambia.rdbms.schema.name" id="_l7xQMM7uEem5ebMPg-wyFg" value="ORIONBD"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_l7xQMc7uEem5ebMPg-wyFg" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_l7xQMs7uEem5ebMPg-wyFg" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_l7xQM87uEem5ebMPg-wyFg" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_l7xQNM7uEem5ebMPg-wyFg" name="ORB0REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l7xQNc7uEem5ebMPg-wyFg" value="ORB0REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l7xQNs7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l7xQN87uEem5ebMPg-wyFg" name="B0BPCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7xQOM7uEem5ebMPg-wyFg" value="B0BPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7xQOc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7xQOs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7xQO87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7xQPM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7xQPc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7xQPs7uEem5ebMPg-wyFg" name="B0EONB" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7xQP87uEem5ebMPg-wyFg" value="B0EONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7xQQM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7xQQc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7xQQs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7xQQ87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7xQRM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7xQRc7uEem5ebMPg-wyFg" name="B0BTST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7xQRs7uEem5ebMPg-wyFg" value="B0BTST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7xQR87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7xQSM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7xQSc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7xQSs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7xQS87uEem5ebMPg-wyFg" name="B0BFTX" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7xQTM7uEem5ebMPg-wyFg" value="B0BFTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7xQTc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7xQTs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7xQT87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7xQUM7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7xQUc7uEem5ebMPg-wyFg" name="B0BUST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7xQUs7uEem5ebMPg-wyFg" value="B0BUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7xQU87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7xQVM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7xQVc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3QM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3Qc7uEem5ebMPg-wyFg" name="B0BWST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3Qs7uEem5ebMPg-wyFg" value="B0BWST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3Q87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3RM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3Rc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3Rs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3R87uEem5ebMPg-wyFg" name="B0CAS1" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3SM7uEem5ebMPg-wyFg" value="B0CAS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3Sc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3Ss7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3S87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3TM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3Tc7uEem5ebMPg-wyFg" name="B0BXST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3Ts7uEem5ebMPg-wyFg" value="B0BXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3T87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3UM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3Uc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3Us7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3U87uEem5ebMPg-wyFg" name="B0JGST" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3VM7uEem5ebMPg-wyFg" value="B0JGST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3Vc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3Vs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3V87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3WM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3Wc7uEem5ebMPg-wyFg" name="B0JHST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3Ws7uEem5ebMPg-wyFg" value="B0JHST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3W87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3XM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3Xc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3Xs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3X87uEem5ebMPg-wyFg" name="B0C0CD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3YM7uEem5ebMPg-wyFg" value="B0C0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3Yc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7x3Ys7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3Y87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3ZM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3Zc7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3Zs7uEem5ebMPg-wyFg" name="B0M5ST" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3Z87uEem5ebMPg-wyFg" value="B0M5ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3aM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3ac7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3as7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3a87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3bM7uEem5ebMPg-wyFg" name="B0JNST" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3bc7uEem5ebMPg-wyFg" value="B0JNST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3bs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3b87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3cM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3cc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3cs7uEem5ebMPg-wyFg" name="B0ARCD" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3c87uEem5ebMPg-wyFg" value="B0ARCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3dM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3dc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3ds7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3d87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3eM7uEem5ebMPg-wyFg" name="B0AMCD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3ec7uEem5ebMPg-wyFg" value="B0AMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3es7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3e87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3fM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3fc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3fs7uEem5ebMPg-wyFg" name="B0GHCD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3f87uEem5ebMPg-wyFg" value="B0GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3gM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3gc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3gs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3g87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3hM7uEem5ebMPg-wyFg" name="B0AQCD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3hc7uEem5ebMPg-wyFg" value="B0AQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3hs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3h87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3iM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3ic7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3is7uEem5ebMPg-wyFg" name="B0BHCD" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3i87uEem5ebMPg-wyFg" value="B0BHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3jM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7x3jc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3js7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3j87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3kM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3kc7uEem5ebMPg-wyFg" name="B0D1NB" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3ks7uEem5ebMPg-wyFg" value="B0D1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3k87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7x3lM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3lc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7x3ls7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7x3l87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7x3mM7uEem5ebMPg-wyFg" name="B0I1NB" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7x3mc7uEem5ebMPg-wyFg" value="B0I1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7x3ms7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7x3m87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7x3nM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yeUM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yeUc7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yeUs7uEem5ebMPg-wyFg" name="B0ADCD" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yeU87uEem5ebMPg-wyFg" value="B0ADCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yeVM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yeVc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yeVs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yeV87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yeWM7uEem5ebMPg-wyFg" name="B0D3C1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yeWc7uEem5ebMPg-wyFg" value="B0D3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yeWs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yeW87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yeXM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yeXc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yeXs7uEem5ebMPg-wyFg" name="B0GMST" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yeX87uEem5ebMPg-wyFg" value="B0GMST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yeYM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yeYc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yeYs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yeY87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yeZM7uEem5ebMPg-wyFg" name="B0JPST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yeZc7uEem5ebMPg-wyFg" value="B0JPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yeZs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yeZ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yeaM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yeac7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yeas7uEem5ebMPg-wyFg" name="B0FIPR" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yea87uEem5ebMPg-wyFg" value="B0FIPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yebM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yebc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yebs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yeb87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yecM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yecc7uEem5ebMPg-wyFg" name="B0GTP1" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yecs7uEem5ebMPg-wyFg" value="B0GTP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yec87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yedM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yedc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yeds7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yed87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yeeM7uEem5ebMPg-wyFg" name="B0DWPR" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yeec7uEem5ebMPg-wyFg" value="B0DWPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yees7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yee87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yefM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yefc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yefs7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yef87uEem5ebMPg-wyFg" name="B0DMVA" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yegM7uEem5ebMPg-wyFg" value="B0DMVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yegc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yegs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yeg87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yehM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yehc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yehs7uEem5ebMPg-wyFg" name="B0BTPR" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yeh87uEem5ebMPg-wyFg" value="B0BTPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yeiM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yeic7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yeis7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yei87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yejM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yejc7uEem5ebMPg-wyFg" name="B0GWP1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yejs7uEem5ebMPg-wyFg" value="B0GWP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yej87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yekM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yekc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yeks7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yek87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yelM7uEem5ebMPg-wyFg" name="B0GVP1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yelc7uEem5ebMPg-wyFg" value="B0GVP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yels7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yel87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yemM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yemc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yems7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yem87uEem5ebMPg-wyFg" name="B0GUP1" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yenM7uEem5ebMPg-wyFg" value="B0GUP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yenc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yens7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yen87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yeoM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yeoc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yeos7uEem5ebMPg-wyFg" name="B0HIP1" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yeo87uEem5ebMPg-wyFg" value="B0HIP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yepM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yepc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yeps7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yep87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yeqM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yeqc7uEem5ebMPg-wyFg" name="B0DKVA" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yeqs7uEem5ebMPg-wyFg" value="B0DKVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yeq87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yerM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yerc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yers7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yer87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yesM7uEem5ebMPg-wyFg" name="B0GXP2" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yesc7uEem5ebMPg-wyFg" value="B0GXP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yess7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yes87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yetM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yetc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7yets7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7yet87uEem5ebMPg-wyFg" name="B0BFDT" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7yeuM7uEem5ebMPg-wyFg" value="B0BFDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7yeuc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7yeus7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7yeu87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7yevM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFYM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFYc7uEem5ebMPg-wyFg" name="B0ASCD" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFYs7uEem5ebMPg-wyFg" value="B0ASCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFY87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFZM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFZc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFZs7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFZ87uEem5ebMPg-wyFg" name="B0JQST" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFaM7uEem5ebMPg-wyFg" value="B0JQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFac7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFas7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFa87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFbM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFbc7uEem5ebMPg-wyFg" name="B0BZST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFbs7uEem5ebMPg-wyFg" value="B0BZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFb87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFcM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFcc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFcs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFc87uEem5ebMPg-wyFg" name="B0E6DT" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFdM7uEem5ebMPg-wyFg" value="B0E6DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFdc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zFds7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFd87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFeM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFec7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFes7uEem5ebMPg-wyFg" name="B0BYST" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFe87uEem5ebMPg-wyFg" value="B0BYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFfM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFfc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFfs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFf87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFgM7uEem5ebMPg-wyFg" name="B0KMDT" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFgc7uEem5ebMPg-wyFg" value="B0KMDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFgs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zFg87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFhM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFhc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFhs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFh87uEem5ebMPg-wyFg" name="B0B0ST" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFiM7uEem5ebMPg-wyFg" value="B0B0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFic7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFis7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFi87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFjM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFjc7uEem5ebMPg-wyFg" name="B0A4DT" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFjs7uEem5ebMPg-wyFg" value="B0A4DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFj87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zFkM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFkc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFks7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFk87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFlM7uEem5ebMPg-wyFg" name="B0B1ST" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFlc7uEem5ebMPg-wyFg" value="B0B1ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFl87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFmM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFmc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFms7uEem5ebMPg-wyFg" name="B0JIST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFm87uEem5ebMPg-wyFg" value="B0JIST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFnM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFnc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFns7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFn87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFoM7uEem5ebMPg-wyFg" name="B0JJST" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFoc7uEem5ebMPg-wyFg" value="B0JJST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFos7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFo87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFpM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFpc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFps7uEem5ebMPg-wyFg" name="B0JKST" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFp87uEem5ebMPg-wyFg" value="B0JKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFqM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFqc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFqs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFq87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFrM7uEem5ebMPg-wyFg" name="B0JMST" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFrc7uEem5ebMPg-wyFg" value="B0JMST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFrs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFr87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFsM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFsc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFss7uEem5ebMPg-wyFg" name="B0HSST" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFs87uEem5ebMPg-wyFg" value="B0HSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFtM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFtc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFts7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFt87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFuM7uEem5ebMPg-wyFg" name="B0FRCD" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFuc7uEem5ebMPg-wyFg" value="B0FRCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFus7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFu87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFvM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFvc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFvs7uEem5ebMPg-wyFg" name="B0B4ST" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFv87uEem5ebMPg-wyFg" value="B0B4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFwM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFwc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFws7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFw87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zFxM7uEem5ebMPg-wyFg" name="B0B7ST" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zFxc7uEem5ebMPg-wyFg" value="B0B7ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zFxs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zFx87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zFyM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zFyc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zscM7uEem5ebMPg-wyFg" name="B0PJST" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zscc7uEem5ebMPg-wyFg" value="B0PJST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zscs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsc87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsdM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zsdc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsds7uEem5ebMPg-wyFg" name="B0PKST" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsd87uEem5ebMPg-wyFg" value="B0PKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zseM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsec7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zses7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zse87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsfM7uEem5ebMPg-wyFg" name="B0CECD" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsfc7uEem5ebMPg-wyFg" value="B0CECD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsfs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zsf87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsgM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsgc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zsgs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsg87uEem5ebMPg-wyFg" name="B0AZCD" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zshM7uEem5ebMPg-wyFg" value="B0AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zshc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zshs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsh87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsiM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zsic7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsis7uEem5ebMPg-wyFg" name="B0FTNB" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsi87uEem5ebMPg-wyFg" value="B0FTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsjM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zsjc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsjs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsj87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zskM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zskc7uEem5ebMPg-wyFg" name="B0AFPC" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsks7uEem5ebMPg-wyFg" value="B0AFPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsk87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zslM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zslc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zsls7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsl87uEem5ebMPg-wyFg" name="B0HRNB" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsmM7uEem5ebMPg-wyFg" value="B0HRNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsmc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zsms7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsm87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsnM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zsnc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsns7uEem5ebMPg-wyFg" name="B0AGPC" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsn87uEem5ebMPg-wyFg" value="B0AGPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsoM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsoc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsos7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zso87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zspM7uEem5ebMPg-wyFg" name="B0HSNB" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zspc7uEem5ebMPg-wyFg" value="B0HSNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsps7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zsp87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsqM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsqc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zsqs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsq87uEem5ebMPg-wyFg" name="B0AHPC" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsrM7uEem5ebMPg-wyFg" value="B0AHPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsrc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsrs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsr87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zssM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zssc7uEem5ebMPg-wyFg" name="B0B1CD" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsss7uEem5ebMPg-wyFg" value="B0B1CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zss87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zstM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zstc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsts7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zst87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsuM7uEem5ebMPg-wyFg" name="B0F7NB" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsuc7uEem5ebMPg-wyFg" value="B0F7NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsus7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zsu87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsvM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsvc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zsvs7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsv87uEem5ebMPg-wyFg" name="B0GLCD" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zswM7uEem5ebMPg-wyFg" value="B0GLCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zswc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zsws7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsw87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsxM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zsxc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zsxs7uEem5ebMPg-wyFg" name="B0F8NB" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zsx87uEem5ebMPg-wyFg" value="B0F8NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsyM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zsyc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zsys7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zsy87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zszM7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zszc7uEem5ebMPg-wyFg" name="B0BCCD" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zszs7uEem5ebMPg-wyFg" value="B0BCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zsz87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zs0M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zs0c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zs0s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zs087uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zs1M7uEem5ebMPg-wyFg" name="B0A5DT" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zs1c7uEem5ebMPg-wyFg" value="B0A5DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zs1s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zs187uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zs2M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7zs2c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7zs2s7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7zs287uEem5ebMPg-wyFg" name="B0A6DT" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7zs3M7uEem5ebMPg-wyFg" value="B0A6DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7zs3c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7zs3s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7zs387uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70TgM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tgc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70Tgs7uEem5ebMPg-wyFg" name="B0A7DT" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Tg87uEem5ebMPg-wyFg" value="B0A7DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70ThM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70Thc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Ths7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70Th87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70TiM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70Tic7uEem5ebMPg-wyFg" name="B0DVDT" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Tis7uEem5ebMPg-wyFg" value="B0DVDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70Ti87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70TjM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Tjc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70Tjs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tj87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70TkM7uEem5ebMPg-wyFg" name="B0B8S1" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Tkc7uEem5ebMPg-wyFg" value="B0B8S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70Tks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Tk87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70TlM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tlc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70Tls7uEem5ebMPg-wyFg" name="B0RPST" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Tl87uEem5ebMPg-wyFg" value="B0RPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70TmM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Tmc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70Tms7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tm87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70TnM7uEem5ebMPg-wyFg" name="B0GLDT" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Tnc7uEem5ebMPg-wyFg" value="B0GLDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70Tns7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70Tn87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70ToM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70Toc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tos7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70To87uEem5ebMPg-wyFg" name="B0GMDT" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70TpM7uEem5ebMPg-wyFg" value="B0GMDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70Tpc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70Tps7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Tp87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70TqM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tqc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70Tqs7uEem5ebMPg-wyFg" name="B0I2QT" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Tq87uEem5ebMPg-wyFg" value="B0I2QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70TrM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70Trc7uEem5ebMPg-wyFg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Trs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70Tr87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70TsM7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70Tsc7uEem5ebMPg-wyFg" name="B0I3QT" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Tss7uEem5ebMPg-wyFg" value="B0I3QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70Ts87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70TtM7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Ttc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70Tts7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tt87uEem5ebMPg-wyFg" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70TuM7uEem5ebMPg-wyFg" name="B0E9S1" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Tuc7uEem5ebMPg-wyFg" value="B0E9S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70Tus7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Tu87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70TvM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tvc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70Tvs7uEem5ebMPg-wyFg" name="B0FAS1" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Tv87uEem5ebMPg-wyFg" value="B0FAS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70TwM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Twc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70Tws7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tw87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70TxM7uEem5ebMPg-wyFg" name="B0C9CD" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Txc7uEem5ebMPg-wyFg" value="B0C9CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70Txs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Tx87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70TyM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tyc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70Tys7uEem5ebMPg-wyFg" name="B0FHS1" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70Ty87uEem5ebMPg-wyFg" value="B0FHS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70TzM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70Tzc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70Tzs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70Tz87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70T0M7uEem5ebMPg-wyFg" name="B0DRN1" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70T0c7uEem5ebMPg-wyFg" value="B0DRN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70T0s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70T087uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70T1M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70T1c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70T1s7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70T187uEem5ebMPg-wyFg" name="B0DSN1" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70T2M7uEem5ebMPg-wyFg" value="B0DSN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70T2c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70T2s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70T287uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70T3M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70T3c7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70T3s7uEem5ebMPg-wyFg" name="B0DTN1" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70T387uEem5ebMPg-wyFg" value="B0DTN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70T4M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70T4c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70T4s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70T487uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70T5M7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70T5c7uEem5ebMPg-wyFg" name="B0K9S2" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70T5s7uEem5ebMPg-wyFg" value="B0K9S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70T587uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70T6M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70T6c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70T6s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70T687uEem5ebMPg-wyFg" name="B0LAS2" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70T7M7uEem5ebMPg-wyFg" value="B0LAS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70T7c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70T7s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70T787uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70T8M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70T8c7uEem5ebMPg-wyFg" name="B0SIN2" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70T8s7uEem5ebMPg-wyFg" value="B0SIN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70T887uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70T9M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70T9c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70T9s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70T987uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70T-M7uEem5ebMPg-wyFg" name="B0SJN2" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70T-c7uEem5ebMPg-wyFg" value="B0SJN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70T-s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l70T-87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l70T_M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l70T_c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l70T_s7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l70T_87uEem5ebMPg-wyFg" name="B0F4N1" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_l70UAM7uEem5ebMPg-wyFg" value="B0F4N1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l70UAc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l706kM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706kc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706ks7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706k87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706lM7uEem5ebMPg-wyFg" name="B0QGDT" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706lc7uEem5ebMPg-wyFg" value="B0QGDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706ls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l706l87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706mM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706mc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706ms7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706m87uEem5ebMPg-wyFg" name="B0XES2" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706nM7uEem5ebMPg-wyFg" value="B0XES2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706nc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706ns7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706n87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706oM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706oc7uEem5ebMPg-wyFg" name="B0QHDT" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706os7uEem5ebMPg-wyFg" value="B0QHDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706o87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l706pM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706pc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706ps7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706p87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706qM7uEem5ebMPg-wyFg" name="B0QIDT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706qc7uEem5ebMPg-wyFg" value="B0QIDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706qs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l706q87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706rM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706rc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706rs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706r87uEem5ebMPg-wyFg" name="B0RNP2" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706sM7uEem5ebMPg-wyFg" value="B0RNP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706sc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l706ss7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706s87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706tM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706tc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706ts7uEem5ebMPg-wyFg" name="B0ROP2" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706t87uEem5ebMPg-wyFg" value="B0ROP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706uM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l706uc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706us7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706u87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706vM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706vc7uEem5ebMPg-wyFg" name="B0WWS2" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706vs7uEem5ebMPg-wyFg" value="B0WWS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706v87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706wM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706wc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706ws7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706w87uEem5ebMPg-wyFg" name="B0WXS2" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706xM7uEem5ebMPg-wyFg" value="B0WXS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706xc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706xs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706x87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706yM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706yc7uEem5ebMPg-wyFg" name="B0JHC1" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706ys7uEem5ebMPg-wyFg" value="B0JHC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706y87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706zM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706zc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706zs7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706z87uEem5ebMPg-wyFg" name="B0AADT" position="100">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7060M7uEem5ebMPg-wyFg" value="B0AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7060c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7060s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706087uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7061M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7061c7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7061s7uEem5ebMPg-wyFg" name="B0AATX" position="101">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706187uEem5ebMPg-wyFg" value="B0AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7062M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7062c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7062s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706287uEem5ebMPg-wyFg" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l7063M7uEem5ebMPg-wyFg" name="WEB_EXPORTCMDMAGENTO">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l7063c7uEem5ebMPg-wyFg" value="WEB_EXPORTCMDMAGENTO"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l7063s7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l706387uEem5ebMPg-wyFg" name="WEAMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7064M7uEem5ebMPg-wyFg" value="WEAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7064c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7064s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706487uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7065M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7065c7uEem5ebMPg-wyFg" name="WEBPCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7065s7uEem5ebMPg-wyFg" value="WEBPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706587uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7066M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7066c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7066s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706687uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7067M7uEem5ebMPg-wyFg" name="WENUMSEQ" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7067c7uEem5ebMPg-wyFg" value="WENUMSEQ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7067s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l706787uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7068M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7068c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7068s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l706887uEem5ebMPg-wyFg" name="WEDATE" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7069M7uEem5ebMPg-wyFg" value="WEDATE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7069c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7069s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l706987uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l706-M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l706-c7uEem5ebMPg-wyFg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l706-s7uEem5ebMPg-wyFg" name="ORB1REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l706-87uEem5ebMPg-wyFg" value="ORB1REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l706_M7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l706_c7uEem5ebMPg-wyFg" name="B1BPCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l706_s7uEem5ebMPg-wyFg" value="B1BPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l706_87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l707AM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l707Ac7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l707As7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l707A87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l707BM7uEem5ebMPg-wyFg" name="B1EONB" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l707Bc7uEem5ebMPg-wyFg" value="B1EONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l707Bs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l707B87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l707CM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l707Cc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l707Cs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l707C87uEem5ebMPg-wyFg" name="B1BQCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l707DM7uEem5ebMPg-wyFg" value="B1BQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l707Dc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l707Ds7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l707D87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l707EM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l707Ec7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l707Es7uEem5ebMPg-wyFg" name="B1RHNB" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l707E87uEem5ebMPg-wyFg" value="B1RHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l707FM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l707Fc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l707Fs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l707F87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l707GM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l707Gc7uEem5ebMPg-wyFg" name="B1RINB" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l707Gs7uEem5ebMPg-wyFg" value="B1RINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l707G87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71hoM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71hoc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71hos7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71ho87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71hpM7uEem5ebMPg-wyFg" name="B1DFN1" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71hpc7uEem5ebMPg-wyFg" value="B1DFN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71hps7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71hp87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71hqM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71hqc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71hqs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71hq87uEem5ebMPg-wyFg" name="B1BGTX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71hrM7uEem5ebMPg-wyFg" value="B1BGTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71hrc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71hrs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71hr87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71hsM7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71hsc7uEem5ebMPg-wyFg" name="B1XGST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71hss7uEem5ebMPg-wyFg" value="B1XGST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71hs87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71htM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71htc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71hts7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71ht87uEem5ebMPg-wyFg" name="B1JGCD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71huM7uEem5ebMPg-wyFg" value="B1JGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71huc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71hus7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71hu87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71hvM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71hvc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71hvs7uEem5ebMPg-wyFg" name="B1CEST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71hv87uEem5ebMPg-wyFg" value="B1CEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71hwM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71hwc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71hws7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71hw87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71hxM7uEem5ebMPg-wyFg" name="B1W6ST" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71hxc7uEem5ebMPg-wyFg" value="B1W6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71hxs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71hx87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71hyM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71hyc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71hys7uEem5ebMPg-wyFg" name="B1AZCD" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71hy87uEem5ebMPg-wyFg" value="B1AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71hzM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71hzc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71hzs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71hz87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71h0M7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71h0c7uEem5ebMPg-wyFg" name="B1AIQT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71h0s7uEem5ebMPg-wyFg" value="B1AIQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71h087uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71h1M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71h1c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71h1s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71h187uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71h2M7uEem5ebMPg-wyFg" name="B1EWQT" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71h2c7uEem5ebMPg-wyFg" value="B1EWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71h2s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71h287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71h3M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71h3c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71h3s7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71h387uEem5ebMPg-wyFg" name="B1EXQT" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71h4M7uEem5ebMPg-wyFg" value="B1EXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71h4c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71h4s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71h487uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71h5M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71h5c7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71h5s7uEem5ebMPg-wyFg" name="B1CAQT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71h587uEem5ebMPg-wyFg" value="B1CAQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71h6M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71h6c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71h6s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71h687uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71h7M7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71h7c7uEem5ebMPg-wyFg" name="B1AJQT" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71h7s7uEem5ebMPg-wyFg" value="B1AJQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71h787uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71h8M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71h8c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71h8s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71h887uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71h9M7uEem5ebMPg-wyFg" name="B1TGQT" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71h9c7uEem5ebMPg-wyFg" value="B1TGQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71h9s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71h987uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71h-M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71h-c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71h-s7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71h-87uEem5ebMPg-wyFg" name="B1I7QT" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71h_M7uEem5ebMPg-wyFg" value="B1I7QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71h_c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71h_s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71h_87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71iAM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71iAc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71iAs7uEem5ebMPg-wyFg" name="B1FZS1" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71iA87uEem5ebMPg-wyFg" value="B1FZS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71iBM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71iBc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71iBs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71iB87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71iCM7uEem5ebMPg-wyFg" name="B1AFPR" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71iCc7uEem5ebMPg-wyFg" value="B1AFPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71iCs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71iC87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71iDM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71iDc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71iDs7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l71iD87uEem5ebMPg-wyFg" name="B1AGPR" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l71iEM7uEem5ebMPg-wyFg" value="B1AGPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l71iEc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l71iEs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l71iE87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l71iFM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l71iFc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72IsM7uEem5ebMPg-wyFg" name="B1O1PR" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72Isc7uEem5ebMPg-wyFg" value="B1O1PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72Iss7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72Is87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72ItM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72Itc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72Its7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72It87uEem5ebMPg-wyFg" name="B1AHPR" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72IuM7uEem5ebMPg-wyFg" value="B1AHPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72Iuc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72Ius7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72Iu87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72IvM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72Ivc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72Ivs7uEem5ebMPg-wyFg" name="B1GXP1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72Iv87uEem5ebMPg-wyFg" value="B1GXP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72IwM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72Iwc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72Iws7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72Iw87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72IxM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72Ixc7uEem5ebMPg-wyFg" name="B1GYP2" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72Ixs7uEem5ebMPg-wyFg" value="B1GYP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72Ix87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72IyM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72Iyc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72Iys7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72Iy87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72IzM7uEem5ebMPg-wyFg" name="B1GZP2" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72Izc7uEem5ebMPg-wyFg" value="B1GZP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72Izs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72Iz87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72I0M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72I0c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72I0s7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72I087uEem5ebMPg-wyFg" name="B1HTPR" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72I1M7uEem5ebMPg-wyFg" value="B1HTPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72I1c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72I1s7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72I187uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72I2M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72I2c7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72I2s7uEem5ebMPg-wyFg" name="B1LAP2" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72I287uEem5ebMPg-wyFg" value="B1LAP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72I3M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72I3c7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72I3s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72I387uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72I4M7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72I4c7uEem5ebMPg-wyFg" name="B1GYP1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72I4s7uEem5ebMPg-wyFg" value="B1GYP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72I487uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72I5M7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72I5c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72I5s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72I587uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72I6M7uEem5ebMPg-wyFg" name="B1GZP1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72I6c7uEem5ebMPg-wyFg" value="B1GZP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72I6s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72I687uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72I7M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72I7c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72I7s7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72I787uEem5ebMPg-wyFg" name="B1AKPC" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72I8M7uEem5ebMPg-wyFg" value="B1AKPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72I8c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72I8s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72I887uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72I9M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72I9c7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72I9s7uEem5ebMPg-wyFg" name="B1CHST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72I987uEem5ebMPg-wyFg" value="B1CHST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72I-M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72I-c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72I-s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72I-87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72I_M7uEem5ebMPg-wyFg" name="B1XLST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72I_c7uEem5ebMPg-wyFg" value="B1XLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72I_s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72I_87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72JAM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72JAc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72JAs7uEem5ebMPg-wyFg" name="B1HGCD" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72JA87uEem5ebMPg-wyFg" value="B1HGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72JBM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72JBc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72JBs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72JB87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72JCM7uEem5ebMPg-wyFg" name="B1ERNB" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72JCc7uEem5ebMPg-wyFg" value="B1ERNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72JCs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72JC87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72JDM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72JDc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72JDs7uEem5ebMPg-wyFg" name="B1BFCD" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72JD87uEem5ebMPg-wyFg" value="B1BFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72JEM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72JEc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72JEs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72JE87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72JFM7uEem5ebMPg-wyFg" name="B1C0CD" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72JFc7uEem5ebMPg-wyFg" value="B1C0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72JFs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72JF87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72JGM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72JGc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72JGs7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72JG87uEem5ebMPg-wyFg" name="B1BOCD" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72JHM7uEem5ebMPg-wyFg" value="B1BOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72JHc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72vwM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72vwc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72vws7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72vw87uEem5ebMPg-wyFg" name="B1HRST" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72vxM7uEem5ebMPg-wyFg" value="B1HRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72vxc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72vxs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72vx87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72vyM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72vyc7uEem5ebMPg-wyFg" name="B1HLCD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72vys7uEem5ebMPg-wyFg" value="B1HLCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72vy87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72vzM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72vzc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72vzs7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72vz87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72v0M7uEem5ebMPg-wyFg" name="B1HMCD" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72v0c7uEem5ebMPg-wyFg" value="B1HMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72v0s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72v087uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72v1M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72v1c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72v1s7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72v187uEem5ebMPg-wyFg" name="B1I3NB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72v2M7uEem5ebMPg-wyFg" value="B1I3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72v2c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72v2s7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72v287uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72v3M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72v3c7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72v3s7uEem5ebMPg-wyFg" name="B1JGC1" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72v387uEem5ebMPg-wyFg" value="B1JGC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72v4M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72v4c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72v4s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72v487uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72v5M7uEem5ebMPg-wyFg" name="B1LUPC" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72v5c7uEem5ebMPg-wyFg" value="B1LUPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72v5s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72v587uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72v6M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72v6c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72v6s7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72v687uEem5ebMPg-wyFg" name="B1FRST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72v7M7uEem5ebMPg-wyFg" value="B1FRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72v7c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72v7s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72v787uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72v8M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72v8c7uEem5ebMPg-wyFg" name="B1JRST" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72v8s7uEem5ebMPg-wyFg" value="B1JRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72v887uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72v9M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72v9c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72v9s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72v987uEem5ebMPg-wyFg" name="B1JSST" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72v-M7uEem5ebMPg-wyFg" value="B1JSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72v-c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72v-s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72v-87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72v_M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72v_c7uEem5ebMPg-wyFg" name="B1PLST" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72v_s7uEem5ebMPg-wyFg" value="B1PLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72v_87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wAM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wAc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wAs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wA87uEem5ebMPg-wyFg" name="B1LBS2" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wBM7uEem5ebMPg-wyFg" value="B1LBS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wBc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wBs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wB87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wCM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wCc7uEem5ebMPg-wyFg" name="B1B9S1" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wCs7uEem5ebMPg-wyFg" value="B1B9S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wC87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wDM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wDc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wDs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wD87uEem5ebMPg-wyFg" name="B1RQST" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wEM7uEem5ebMPg-wyFg" value="B1RQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wEc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wEs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wE87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wFM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wFc7uEem5ebMPg-wyFg" name="B1QMDT" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wFs7uEem5ebMPg-wyFg" value="B1QMDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wF87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72wGM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wGc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wGs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wG87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wHM7uEem5ebMPg-wyFg" name="B1QNDT" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wHc7uEem5ebMPg-wyFg" value="B1QNDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wHs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72wH87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wIM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wIc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wIs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wI87uEem5ebMPg-wyFg" name="B1RRP2" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wJM7uEem5ebMPg-wyFg" value="B1RRP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wJc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72wJs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wJ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wKM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wKc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wKs7uEem5ebMPg-wyFg" name="B1RSP2" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wK87uEem5ebMPg-wyFg" value="B1RSP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wLM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72wLc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wLs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wL87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wMM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wMc7uEem5ebMPg-wyFg" name="B1W0S2" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wMs7uEem5ebMPg-wyFg" value="B1W0S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wM87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wNM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wNc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wNs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wN87uEem5ebMPg-wyFg" name="B1W1S2" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wOM7uEem5ebMPg-wyFg" value="B1W1S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wOc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l72wOs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l72wO87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l72wPM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l72wPc7uEem5ebMPg-wyFg" name="B1AADT" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_l72wPs7uEem5ebMPg-wyFg" value="B1AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l72wP87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l72wQM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73W0M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73W0c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73W0s7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73W087uEem5ebMPg-wyFg" name="B1AATX" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73W1M7uEem5ebMPg-wyFg" value="B1AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73W1c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73W1s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73W187uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73W2M7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73W2c7uEem5ebMPg-wyFg" name="B1OWST" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73W2s7uEem5ebMPg-wyFg" value="B1OWST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73W287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73W3M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73W3c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73W3s7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l73W387uEem5ebMPg-wyFg" name="ORI1REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l73W4M7uEem5ebMPg-wyFg" value="ORI1REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l73W4c7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l73W4s7uEem5ebMPg-wyFg" name="I1B3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73W487uEem5ebMPg-wyFg" value="I1B3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73W5M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73W5c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73W5s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73W587uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73W6M7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73W6c7uEem5ebMPg-wyFg" name="I1AZCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73W6s7uEem5ebMPg-wyFg" value="I1AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73W687uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73W7M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73W7c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73W7s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73W787uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73W8M7uEem5ebMPg-wyFg" name="I1GHCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73W8c7uEem5ebMPg-wyFg" value="I1GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73W8s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73W887uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73W9M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73W9c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73W9s7uEem5ebMPg-wyFg" name="I1J2CD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73W987uEem5ebMPg-wyFg" value="I1J2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73W-M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73W-c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73W-s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73W-87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73W_M7uEem5ebMPg-wyFg" name="I1J3CD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73W_c7uEem5ebMPg-wyFg" value="I1J3CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73W_s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73W_87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XAM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XAc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XAs7uEem5ebMPg-wyFg" name="I1HMCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XA87uEem5ebMPg-wyFg" value="I1HMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XBM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73XBc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XBs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XB87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XCM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XCc7uEem5ebMPg-wyFg" name="I1JCCD" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XCs7uEem5ebMPg-wyFg" value="I1JCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XC87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XDM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XDc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XDs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XD87uEem5ebMPg-wyFg" name="I1CPCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XEM7uEem5ebMPg-wyFg" value="I1CPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XEc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XEs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XE87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XFM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XFc7uEem5ebMPg-wyFg" name="I1ISTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XFs7uEem5ebMPg-wyFg" value="I1ISTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XF87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XGM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XGc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XGs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XG87uEem5ebMPg-wyFg" name="I1JVCD" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XHM7uEem5ebMPg-wyFg" value="I1JVCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XHc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XHs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XH87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XIM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XIc7uEem5ebMPg-wyFg" name="I1JWCD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XIs7uEem5ebMPg-wyFg" value="I1JWCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XI87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XJM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XJc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XJs7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XJ87uEem5ebMPg-wyFg" name="I1MINB" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XKM7uEem5ebMPg-wyFg" value="I1MINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XKc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73XKs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XK87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XLM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XLc7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XLs7uEem5ebMPg-wyFg" name="I1MJNB" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XL87uEem5ebMPg-wyFg" value="I1MJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XMM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73XMc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XMs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XM87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XNM7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XNc7uEem5ebMPg-wyFg" name="I1MKNB" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XNs7uEem5ebMPg-wyFg" value="I1MKNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XN87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73XOM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XOc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XOs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XO87uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XPM7uEem5ebMPg-wyFg" name="I1MLNB" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XPc7uEem5ebMPg-wyFg" value="I1MLNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XPs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73XP87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XQM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XQc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XQs7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XQ87uEem5ebMPg-wyFg" name="I1MMNB" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XRM7uEem5ebMPg-wyFg" value="I1MMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XRc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73XRs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XR87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XSM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XSc7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XSs7uEem5ebMPg-wyFg" name="I1MNNB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XS87uEem5ebMPg-wyFg" value="I1MNNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XTM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73XTc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XTs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XT87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XUM7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73XUc7uEem5ebMPg-wyFg" name="I1NMNB" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73XUs7uEem5ebMPg-wyFg" value="I1NMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73XU87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73XVM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73XVc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73XVs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73XV87uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7394M7uEem5ebMPg-wyFg" name="I1KYNB" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7394c7uEem5ebMPg-wyFg" value="I1KYNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7394s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l739487uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7395M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7395c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7395s7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l739587uEem5ebMPg-wyFg" name="I1CNN3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7396M7uEem5ebMPg-wyFg" value="I1CNN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7396c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7396s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l739687uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7397M7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7397c7uEem5ebMPg-wyFg" name="I1CON3" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7397s7uEem5ebMPg-wyFg" value="I1CON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l739787uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7398M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7398c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7398s7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l739887uEem5ebMPg-wyFg" name="I1I4TX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7399M7uEem5ebMPg-wyFg" value="I1I4TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7399c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7399s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l739987uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l739-M7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l739-c7uEem5ebMPg-wyFg" name="I1QQST" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l739-s7uEem5ebMPg-wyFg" value="I1QQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l739-87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l739_M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l739_c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l739_s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l739_87uEem5ebMPg-wyFg" name="I1QPST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-AM7uEem5ebMPg-wyFg" value="I1QPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-Ac7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-As7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-A87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-BM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-Bc7uEem5ebMPg-wyFg" name="I1CXS1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-Bs7uEem5ebMPg-wyFg" value="I1CXS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-B87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-CM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-Cc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-Cs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-C87uEem5ebMPg-wyFg" name="I1KZNB" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-DM7uEem5ebMPg-wyFg" value="I1KZNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-Dc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-Ds7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-D87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-EM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-Ec7uEem5ebMPg-wyFg" name="I1NCNB" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-Es7uEem5ebMPg-wyFg" value="I1NCNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-E87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73-FM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-Fc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-Fs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-F87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-GM7uEem5ebMPg-wyFg" name="I1K0NB" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-Gc7uEem5ebMPg-wyFg" value="I1K0NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-Gs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73-G87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-HM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-Hc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-Hs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-H87uEem5ebMPg-wyFg" name="I1HPN1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-IM7uEem5ebMPg-wyFg" value="I1HPN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-Ic7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l73-Is7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-I87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-JM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-Jc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-Js7uEem5ebMPg-wyFg" name="I1QRST" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-J87uEem5ebMPg-wyFg" value="I1QRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-KM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-Kc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-Ks7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-K87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-LM7uEem5ebMPg-wyFg" name="I1CYS1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-Lc7uEem5ebMPg-wyFg" value="I1CYS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-Ls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-L87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-MM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-Mc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-Ms7uEem5ebMPg-wyFg" name="I1QSST" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-M87uEem5ebMPg-wyFg" value="I1QSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-NM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-Nc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-Ns7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-N87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-OM7uEem5ebMPg-wyFg" name="I1QTST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-Oc7uEem5ebMPg-wyFg" value="I1QTST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-Os7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-O87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-PM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-Pc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-Ps7uEem5ebMPg-wyFg" name="I1QVST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-P87uEem5ebMPg-wyFg" value="I1QVST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-QM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-Qc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-Qs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-Q87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-RM7uEem5ebMPg-wyFg" name="I1QUST" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-Rc7uEem5ebMPg-wyFg" value="I1QUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-Rs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-R87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-SM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-Sc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-Ss7uEem5ebMPg-wyFg" name="I1FKS2" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-S87uEem5ebMPg-wyFg" value="I1FKS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-TM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-Tc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-Ts7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-T87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-UM7uEem5ebMPg-wyFg" name="I1FLS2" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-Uc7uEem5ebMPg-wyFg" value="I1FLS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-Us7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-U87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-VM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-Vc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-Vs7uEem5ebMPg-wyFg" name="I1FMS2" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-V87uEem5ebMPg-wyFg" value="I1FMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-WM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-Wc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-Ws7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-W87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-XM7uEem5ebMPg-wyFg" name="I1QXST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-Xc7uEem5ebMPg-wyFg" value="I1QXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-Xs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-X87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-YM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l73-Yc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l73-Ys7uEem5ebMPg-wyFg" name="I1QYST" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l73-Y87uEem5ebMPg-wyFg" value="I1QYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l73-ZM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l73-Zc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l73-Zs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74k8M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74k8c7uEem5ebMPg-wyFg" name="I1QZST" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74k8s7uEem5ebMPg-wyFg" value="I1QZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74k887uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74k9M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74k9c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74k9s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74k987uEem5ebMPg-wyFg" name="I1Q0ST" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74k-M7uEem5ebMPg-wyFg" value="I1Q0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74k-c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74k-s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74k-87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74k_M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74k_c7uEem5ebMPg-wyFg" name="I1K1NB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74k_s7uEem5ebMPg-wyFg" value="I1K1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74k_87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l74lAM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lAc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lAs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lA87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lBM7uEem5ebMPg-wyFg" name="I1K2NB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lBc7uEem5ebMPg-wyFg" value="I1K2NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lBs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lB87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lCM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lCc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lCs7uEem5ebMPg-wyFg" name="I1K3NB" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lC87uEem5ebMPg-wyFg" value="I1K3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lDM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lDc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lDs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lD87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lEM7uEem5ebMPg-wyFg" name="I1S6ST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lEc7uEem5ebMPg-wyFg" value="I1S6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lEs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lE87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lFM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lFc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lFs7uEem5ebMPg-wyFg" name="I1XEST" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lF87uEem5ebMPg-wyFg" value="I1XEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lGM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lGc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lGs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lG87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lHM7uEem5ebMPg-wyFg" name="I1DHQT" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lHc7uEem5ebMPg-wyFg" value="I1DHQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lHs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l74lH87uEem5ebMPg-wyFg" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lIM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lIc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lIs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lI87uEem5ebMPg-wyFg" name="I1AFNB" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lJM7uEem5ebMPg-wyFg" value="I1AFNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lJc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l74lJs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lJ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lKM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lKc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lKs7uEem5ebMPg-wyFg" name="I1L9QT" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lK87uEem5ebMPg-wyFg" value="I1L9QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lLM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l74lLc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lLs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lL87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lMM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lMc7uEem5ebMPg-wyFg" name="I1L9N1" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lMs7uEem5ebMPg-wyFg" value="I1L9N1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lM87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l74lNM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lNc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lNs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lN87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lOM7uEem5ebMPg-wyFg" name="I1DAT1" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lOc7uEem5ebMPg-wyFg" value="I1DAT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lOs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lO87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lPM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lPc7uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lPs7uEem5ebMPg-wyFg" name="I1CZS1" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lP87uEem5ebMPg-wyFg" value="I1CZS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lQM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lQc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lQs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lQ87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lRM7uEem5ebMPg-wyFg" name="I1C9T1" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lRc7uEem5ebMPg-wyFg" value="I1C9T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lRs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lR87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lSM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lSc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lSs7uEem5ebMPg-wyFg" name="I1TPTX" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lS87uEem5ebMPg-wyFg" value="I1TPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lTM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lTc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lTs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lT87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lUM7uEem5ebMPg-wyFg" name="I1TQTX" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lUc7uEem5ebMPg-wyFg" value="I1TQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lUs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lU87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lVM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lVc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lVs7uEem5ebMPg-wyFg" name="I1GVPR" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lV87uEem5ebMPg-wyFg" value="I1GVPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lWM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l74lWc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lWs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lW87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lXM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lXc7uEem5ebMPg-wyFg" name="I1CJPC" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lXs7uEem5ebMPg-wyFg" value="I1CJPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lX87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l74lYM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lYc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lYs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lY87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lZM7uEem5ebMPg-wyFg" name="I1C0S1" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lZc7uEem5ebMPg-wyFg" value="I1C0S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lZs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lZ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74laM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lac7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74las7uEem5ebMPg-wyFg" name="I1DYQT" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74la87uEem5ebMPg-wyFg" value="I1DYQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lbM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l74lbc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l74lbs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l74lb87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l74lcM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l74lcc7uEem5ebMPg-wyFg" name="I1DXQT" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_l74lcs7uEem5ebMPg-wyFg" value="I1DXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l74lc87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MAM7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MAc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MAs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MA87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MBM7uEem5ebMPg-wyFg" name="I1GQQT" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MBc7uEem5ebMPg-wyFg" value="I1GQQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MBs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MB87uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MCM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MCc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MCs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MC87uEem5ebMPg-wyFg" name="I1GRQT" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MDM7uEem5ebMPg-wyFg" value="I1GRQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MDc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MDs7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MD87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MEM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MEc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MEs7uEem5ebMPg-wyFg" name="I1GSQT" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75ME87uEem5ebMPg-wyFg" value="I1GSQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MFM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MFc7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MFs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MF87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MGM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MGc7uEem5ebMPg-wyFg" name="I1GTQT" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MGs7uEem5ebMPg-wyFg" value="I1GTQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MG87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MHM7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MHc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MHs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MH87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MIM7uEem5ebMPg-wyFg" name="I1GUQT" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MIc7uEem5ebMPg-wyFg" value="I1GUQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MIs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MI87uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MJM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MJc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MJs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MJ87uEem5ebMPg-wyFg" name="I1GVQT" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MKM7uEem5ebMPg-wyFg" value="I1GVQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MKc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MKs7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MK87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MLM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MLc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MLs7uEem5ebMPg-wyFg" name="I1GWQT" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75ML87uEem5ebMPg-wyFg" value="I1GWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MMM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MMc7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MMs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MM87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MNM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MNc7uEem5ebMPg-wyFg" name="I1GXQT" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MNs7uEem5ebMPg-wyFg" value="I1GXQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MN87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MOM7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MOc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MOs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MO87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MPM7uEem5ebMPg-wyFg" name="I1GYQT" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MPc7uEem5ebMPg-wyFg" value="I1GYQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MPs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MP87uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MQM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MQc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MQs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MQ87uEem5ebMPg-wyFg" name="I1GZQT" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MRM7uEem5ebMPg-wyFg" value="I1GZQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MRc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MRs7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MR87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MSM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MSc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MSs7uEem5ebMPg-wyFg" name="I1G0QT" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MS87uEem5ebMPg-wyFg" value="I1G0QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MTM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MTc7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MTs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MT87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MUM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MUc7uEem5ebMPg-wyFg" name="I1G1QT" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MUs7uEem5ebMPg-wyFg" value="I1G1QT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MU87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MVM7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MVc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MVs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MV87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MWM7uEem5ebMPg-wyFg" name="I1DWQT" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MWc7uEem5ebMPg-wyFg" value="I1DWQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MWs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MW87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MXM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MXc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MXs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MX87uEem5ebMPg-wyFg" name="I1HUDT" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MYM7uEem5ebMPg-wyFg" value="I1HUDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MYc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75MYs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MY87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75MZM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MZc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MZs7uEem5ebMPg-wyFg" name="I1YVNB" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75MZ87uEem5ebMPg-wyFg" value="I1YVNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75MaM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75Mac7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75Mas7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75Ma87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75MbM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75Mbc7uEem5ebMPg-wyFg" name="I1YWNB" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75Mbs7uEem5ebMPg-wyFg" value="I1YWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75Mb87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75McM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75Mcc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75Mcs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75Mc87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75MdM7uEem5ebMPg-wyFg" name="I1GOQT" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75Mdc7uEem5ebMPg-wyFg" value="I1GOQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75Mds7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75Md87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75MeM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75Mec7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75Mes7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75Me87uEem5ebMPg-wyFg" name="I1FNS2" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zEM7uEem5ebMPg-wyFg" value="I1FNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zEc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zEs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zE87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zFM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zFc7uEem5ebMPg-wyFg" name="I1FOS2" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zFs7uEem5ebMPg-wyFg" value="I1FOS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zF87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zGM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zGc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zGs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zG87uEem5ebMPg-wyFg" name="I1FPS2" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zHM7uEem5ebMPg-wyFg" value="I1FPS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zHc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zHs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zH87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zIM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zIc7uEem5ebMPg-wyFg" name="I1FQS2" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zIs7uEem5ebMPg-wyFg" value="I1FQS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zI87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zJM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zJc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zJs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zJ87uEem5ebMPg-wyFg" name="I1FRS2" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zKM7uEem5ebMPg-wyFg" value="I1FRS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zKc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zKs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zK87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zLM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zLc7uEem5ebMPg-wyFg" name="I1FSS2" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zLs7uEem5ebMPg-wyFg" value="I1FSS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zL87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zMM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zMc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zMs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zM87uEem5ebMPg-wyFg" name="I1FTS2" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zNM7uEem5ebMPg-wyFg" value="I1FTS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zNc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zNs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zN87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zOM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zOc7uEem5ebMPg-wyFg" name="I1FUS2" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zOs7uEem5ebMPg-wyFg" value="I1FUS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zO87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zPM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zPc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zPs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zP87uEem5ebMPg-wyFg" name="I1FVS2" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zQM7uEem5ebMPg-wyFg" value="I1FVS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zQc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zQs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zQ87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zRM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zRc7uEem5ebMPg-wyFg" name="I1FWS2" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zRs7uEem5ebMPg-wyFg" value="I1FWS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zR87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zSM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zSc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zSs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zS87uEem5ebMPg-wyFg" name="I1FXS2" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zTM7uEem5ebMPg-wyFg" value="I1FXS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zTc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zTs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zT87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zUM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zUc7uEem5ebMPg-wyFg" name="I1FYS2" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zUs7uEem5ebMPg-wyFg" value="I1FYS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zU87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zVM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zVc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zVs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zV87uEem5ebMPg-wyFg" name="I1YSNB" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zWM7uEem5ebMPg-wyFg" value="I1YSNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zWc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75zWs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zW87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zXM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zXc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zXs7uEem5ebMPg-wyFg" name="I1YTNB" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zX87uEem5ebMPg-wyFg" value="I1YTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zYM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75zYc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zYs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zY87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zZM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zZc7uEem5ebMPg-wyFg" name="I1YUNB" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zZs7uEem5ebMPg-wyFg" value="I1YUNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zZ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75zaM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zac7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zas7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75za87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zbM7uEem5ebMPg-wyFg" name="I1GLQT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zbc7uEem5ebMPg-wyFg" value="I1GLQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zbs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75zb87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zcM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zcc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zcs7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zc87uEem5ebMPg-wyFg" name="I1GMQT" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zdM7uEem5ebMPg-wyFg" value="I1GMQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zdc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75zds7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zd87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zeM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zec7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zes7uEem5ebMPg-wyFg" name="I1GNQT" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75ze87uEem5ebMPg-wyFg" value="I1GNQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zfM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75zfc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zfs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zf87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zgM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zgc7uEem5ebMPg-wyFg" name="I1JLDT" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zgs7uEem5ebMPg-wyFg" value="I1JLDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zg87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75zhM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zhc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zhs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zh87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75ziM7uEem5ebMPg-wyFg" name="I1AADT" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zic7uEem5ebMPg-wyFg" value="I1AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zis7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l75zi87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zjM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zjc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zjs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l75zj87uEem5ebMPg-wyFg" name="I1AATX" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_l75zkM7uEem5ebMPg-wyFg" value="I1AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l75zkc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l75zks7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l75zk87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l75zlM7uEem5ebMPg-wyFg" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l75zlc7uEem5ebMPg-wyFg" name="OTKYCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l75zls7uEem5ebMPg-wyFg" value="OTKYCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l76aIM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l76aIc7uEem5ebMPg-wyFg" name="KYB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aIs7uEem5ebMPg-wyFg" value="KYB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aI87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76aJM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aJc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aJs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aJ87uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aKM7uEem5ebMPg-wyFg" name="KYYHC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aKc7uEem5ebMPg-wyFg" value="KYYHC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aKs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aK87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aLM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aLc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aLs7uEem5ebMPg-wyFg" name="KYRKS3" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aL87uEem5ebMPg-wyFg" value="KYRKS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aMM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aMc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aMs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aM87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aNM7uEem5ebMPg-wyFg" name="KYRLS3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aNc7uEem5ebMPg-wyFg" value="KYRLS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aNs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aN87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aOM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aOc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aOs7uEem5ebMPg-wyFg" name="KYW9N6" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aO87uEem5ebMPg-wyFg" value="KYW9N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aPM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76aPc7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aPs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aP87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aQM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aQc7uEem5ebMPg-wyFg" name="KYDGP4" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aQs7uEem5ebMPg-wyFg" value="KYDGP4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aQ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76aRM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aRc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aRs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aR87uEem5ebMPg-wyFg" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l76aSM7uEem5ebMPg-wyFg" name="OTCACPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l76aSc7uEem5ebMPg-wyFg" value="OTCACPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l76aSs7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l76aS87uEem5ebMPg-wyFg" name="CAB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aTM7uEem5ebMPg-wyFg" value="CAB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aTc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76aTs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aT87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aUM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aUc7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aUs7uEem5ebMPg-wyFg" name="CAKWN6" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aU87uEem5ebMPg-wyFg" value="CAKWN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aVM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76aVc7uEem5ebMPg-wyFg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aVs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aV87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aWM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aWc7uEem5ebMPg-wyFg" name="CAM1N6" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aWs7uEem5ebMPg-wyFg" value="CAM1N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aW87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76aXM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aXc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aXs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aX87uEem5ebMPg-wyFg" value="5"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l76aYM7uEem5ebMPg-wyFg" name="OTBLCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l76aYc7uEem5ebMPg-wyFg" value="OTBLCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l76aYs7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l76aY87uEem5ebMPg-wyFg" name="BLB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aZM7uEem5ebMPg-wyFg" value="BLB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76aZc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76aZs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aZ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aaM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aac7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aas7uEem5ebMPg-wyFg" name="BLLQP3" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aa87uEem5ebMPg-wyFg" value="BLLQP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76abM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76abc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76abs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76ab87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76acM7uEem5ebMPg-wyFg" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l76acc7uEem5ebMPg-wyFg" name="ORAZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l76acs7uEem5ebMPg-wyFg" value="ORAZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l76ac87uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l76adM7uEem5ebMPg-wyFg" name="AZAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76adc7uEem5ebMPg-wyFg" value="AZAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76ads7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76ad87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aeM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76aec7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aes7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76ae87uEem5ebMPg-wyFg" name="AZAMCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76afM7uEem5ebMPg-wyFg" value="AZAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76afc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76afs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76af87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76agM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76agc7uEem5ebMPg-wyFg" name="AZC0CD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76ags7uEem5ebMPg-wyFg" value="AZC0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76ag87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76ahM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76ahc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76ahs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76ah87uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aiM7uEem5ebMPg-wyFg" name="AZF6PR" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aic7uEem5ebMPg-wyFg" value="AZF6PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76ais7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l76ai87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76ajM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76ajc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76ajs7uEem5ebMPg-wyFg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aj87uEem5ebMPg-wyFg" name="AZTEST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76akM7uEem5ebMPg-wyFg" value="AZTEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76akc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76aks7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76ak87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76alM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76alc7uEem5ebMPg-wyFg" name="AZTKST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76als7uEem5ebMPg-wyFg" value="AZTKST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76al87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76amM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76amc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76ams7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76am87uEem5ebMPg-wyFg" name="AZTLST" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76anM7uEem5ebMPg-wyFg" value="AZTLST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76anc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76ans7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76an87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aoM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76aoc7uEem5ebMPg-wyFg" name="AZNDCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l76aos7uEem5ebMPg-wyFg" value="AZNDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l76ao87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l76apM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l76apc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l76aps7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l76ap87uEem5ebMPg-wyFg" name="AZNECD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77BMM7uEem5ebMPg-wyFg" value="AZNECD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77BMc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77BMs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77BM87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77BNM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77BNc7uEem5ebMPg-wyFg" name="AZAQST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77BNs7uEem5ebMPg-wyFg" value="AZAQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77BN87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77BOM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77BOc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77BOs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77BO87uEem5ebMPg-wyFg" name="AZARST" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77BPM7uEem5ebMPg-wyFg" value="AZARST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77BPc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77BPs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77BP87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77BQM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77BQc7uEem5ebMPg-wyFg" name="AZASST" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77BQs7uEem5ebMPg-wyFg" value="AZASST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77BQ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77BRM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77BRc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77BRs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77BR87uEem5ebMPg-wyFg" name="AZBHS2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77BSM7uEem5ebMPg-wyFg" value="AZBHS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77BSc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77oQM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77oQc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77oQs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77oQ87uEem5ebMPg-wyFg" name="AZAWNA" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77oRM7uEem5ebMPg-wyFg" value="AZAWNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77oRc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77oRs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77oR87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77oSM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77oSc7uEem5ebMPg-wyFg" name="AZAUTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77oSs7uEem5ebMPg-wyFg" value="AZAUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77oS87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77oTM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77oTc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77oTs7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77oT87uEem5ebMPg-wyFg" name="AZA0CD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77oUM7uEem5ebMPg-wyFg" value="AZA0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77oUc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l77oUs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77oU87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77oVM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77oVc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77oVs7uEem5ebMPg-wyFg" name="AZATTX" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77oV87uEem5ebMPg-wyFg" value="AZATTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77oWM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77oWc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77oWs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77oW87uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77oXM7uEem5ebMPg-wyFg" name="AZOEST" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77oXc7uEem5ebMPg-wyFg" value="AZOEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77oXs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77oX87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77oYM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77oYc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77oYs7uEem5ebMPg-wyFg" name="AZA8ST" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77oY87uEem5ebMPg-wyFg" value="AZA8ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77oZM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77oZc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77oZs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77oZ87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77oaM7uEem5ebMPg-wyFg" name="AZAOST" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77oac7uEem5ebMPg-wyFg" value="AZAOST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77oas7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77oa87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77obM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77obc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77obs7uEem5ebMPg-wyFg" name="AZFNTX" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77ob87uEem5ebMPg-wyFg" value="AZFNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77ocM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77occ7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77ocs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77oc87uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77odM7uEem5ebMPg-wyFg" name="AZZLTX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77odc7uEem5ebMPg-wyFg" value="AZZLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77ods7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l77od87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l77oeM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l77oec7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l77oes7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l77oe87uEem5ebMPg-wyFg" name="AZFMTX" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l77ofM7uEem5ebMPg-wyFg" value="AZFMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l77ofc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l78PUM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78PUc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78PUs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PU87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78PVM7uEem5ebMPg-wyFg" name="AZIWST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78PVc7uEem5ebMPg-wyFg" value="AZIWST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78PVs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78PV87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78PWM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PWc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78PWs7uEem5ebMPg-wyFg" name="AZIXST" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78PW87uEem5ebMPg-wyFg" value="AZIXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78PXM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78PXc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78PXs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PX87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78PYM7uEem5ebMPg-wyFg" name="AZIYST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78PYc7uEem5ebMPg-wyFg" value="AZIYST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78PYs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78PY87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78PZM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PZc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78PZs7uEem5ebMPg-wyFg" name="AZIZST" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78PZ87uEem5ebMPg-wyFg" value="AZIZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78PaM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pac7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Pas7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Pa87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78PbM7uEem5ebMPg-wyFg" name="AZI0ST" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pbc7uEem5ebMPg-wyFg" value="AZI0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pbs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pb87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78PcM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Pcc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pcs7uEem5ebMPg-wyFg" name="AZI2ST" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pc87uEem5ebMPg-wyFg" value="AZI2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78PdM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pdc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Pds7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Pd87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78PeM7uEem5ebMPg-wyFg" name="AZHKCD" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pec7uEem5ebMPg-wyFg" value="AZHKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pes7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pe87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78PfM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Pfc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pfs7uEem5ebMPg-wyFg" name="AZCKTX" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pf87uEem5ebMPg-wyFg" value="AZCKTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78PgM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l78Pgc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pgs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Pg87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PhM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Phc7uEem5ebMPg-wyFg" name="AZAACD" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Phs7uEem5ebMPg-wyFg" value="AZAACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Ph87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78PiM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Pic7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Pis7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pi87uEem5ebMPg-wyFg" name="AZSEST" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78PjM7uEem5ebMPg-wyFg" value="AZSEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pjc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pjs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Pj87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PkM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pkc7uEem5ebMPg-wyFg" name="AZSFST" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pks7uEem5ebMPg-wyFg" value="AZSFST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pk87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78PlM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Plc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Pls7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pl87uEem5ebMPg-wyFg" name="AZAFCD" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78PmM7uEem5ebMPg-wyFg" value="AZAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pmc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pms7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Pm87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PnM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pnc7uEem5ebMPg-wyFg" name="AZABCD" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pns7uEem5ebMPg-wyFg" value="AZABCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pn87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78PoM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Poc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Pos7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Po87uEem5ebMPg-wyFg" name="AZACCD" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78PpM7uEem5ebMPg-wyFg" value="AZACCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Ppc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pps7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Pp87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PqM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pqc7uEem5ebMPg-wyFg" name="AZAHCD" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pqs7uEem5ebMPg-wyFg" value="AZAHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pq87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78PrM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Prc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Prs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pr87uEem5ebMPg-wyFg" name="AZE3ST" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78PsM7uEem5ebMPg-wyFg" value="AZE3ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Psc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pss7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Ps87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PtM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Ptc7uEem5ebMPg-wyFg" name="AZCGCD" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pts7uEem5ebMPg-wyFg" value="AZCGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pt87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78PuM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Puc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Pus7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pu87uEem5ebMPg-wyFg" name="AZJ4CD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78PvM7uEem5ebMPg-wyFg" value="AZJ4CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pvc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pvs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Pv87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78PwM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78Pwc7uEem5ebMPg-wyFg" name="AZBCCD" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pws7uEem5ebMPg-wyFg" value="AZBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pw87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l78PxM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Pxc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l78Pxs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l78Px87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l78PyM7uEem5ebMPg-wyFg" name="AZF3CD" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l78Pyc7uEem5ebMPg-wyFg" value="AZF3CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l78Pys7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l78Py87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782YM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782Yc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782Ys7uEem5ebMPg-wyFg" name="AZADNB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782Y87uEem5ebMPg-wyFg" value="AZADNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782ZM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l782Zc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782Zs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782Z87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782aM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782ac7uEem5ebMPg-wyFg" name="AZAENB" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782as7uEem5ebMPg-wyFg" value="AZAENB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782a87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l782bM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782bc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782bs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782b87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782cM7uEem5ebMPg-wyFg" name="AZFTNB" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782cc7uEem5ebMPg-wyFg" value="AZFTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782cs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l782c87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782dM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782dc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782ds7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782d87uEem5ebMPg-wyFg" name="AZBOCD" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782eM7uEem5ebMPg-wyFg" value="AZBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782ec7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782es7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782e87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782fM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782fc7uEem5ebMPg-wyFg" name="AZHRST" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782fs7uEem5ebMPg-wyFg" value="AZHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782f87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782gM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782gc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782gs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782g87uEem5ebMPg-wyFg" name="AZA4ST" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782hM7uEem5ebMPg-wyFg" value="AZA4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782hc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782hs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782h87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782iM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782ic7uEem5ebMPg-wyFg" name="AZFWNB" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782is7uEem5ebMPg-wyFg" value="AZFWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782i87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l782jM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782jc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782js7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782j87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782kM7uEem5ebMPg-wyFg" name="AZA0ST" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782kc7uEem5ebMPg-wyFg" value="AZA0ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782ks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782k87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782lM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782lc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782ls7uEem5ebMPg-wyFg" name="AZA3ST" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782l87uEem5ebMPg-wyFg" value="AZA3ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782mM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782mc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782ms7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782m87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782nM7uEem5ebMPg-wyFg" name="AZBDCD" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782nc7uEem5ebMPg-wyFg" value="AZBDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782ns7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782n87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782oM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782oc7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782os7uEem5ebMPg-wyFg" name="AZA1ST" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782o87uEem5ebMPg-wyFg" value="AZA1ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782pM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782pc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782ps7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782p87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782qM7uEem5ebMPg-wyFg" name="AZAJNB" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782qc7uEem5ebMPg-wyFg" value="AZAJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782qs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782q87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782rM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782rc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782rs7uEem5ebMPg-wyFg" name="AZA6NA" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782r87uEem5ebMPg-wyFg" value="AZA6NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782sM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782sc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782ss7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782s87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782tM7uEem5ebMPg-wyFg" name="AZBACD" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782tc7uEem5ebMPg-wyFg" value="AZBACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782ts7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l782t87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782uM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782uc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782us7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782u87uEem5ebMPg-wyFg" name="AZBBCD" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782vM7uEem5ebMPg-wyFg" value="AZBBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782vc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l782vs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782v87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782wM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782wc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782ws7uEem5ebMPg-wyFg" name="AZA2NA" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782w87uEem5ebMPg-wyFg" value="AZA2NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782xM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782xc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782xs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782x87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782yM7uEem5ebMPg-wyFg" name="AZA3NA" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782yc7uEem5ebMPg-wyFg" value="AZA3NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l782ys7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782y87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l782zM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782zc7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l782zs7uEem5ebMPg-wyFg" name="AZCFCD" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782z87uEem5ebMPg-wyFg" value="AZCFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7820M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7820c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7820s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782087uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7821M7uEem5ebMPg-wyFg" name="AZI4ST" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7821c7uEem5ebMPg-wyFg" value="AZI4ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7821s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782187uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7822M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7822c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7822s7uEem5ebMPg-wyFg" name="AZB7S1" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_l782287uEem5ebMPg-wyFg" value="AZB7S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7823M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7823c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7823s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l782387uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7824M7uEem5ebMPg-wyFg" name="AZNHST" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7824c7uEem5ebMPg-wyFg" value="AZNHST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7824s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l782487uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dcM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dcc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dcs7uEem5ebMPg-wyFg" name="AZD3C1" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dc87uEem5ebMPg-wyFg" value="AZD3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79ddM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79ddc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dds7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dd87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79deM7uEem5ebMPg-wyFg" name="AZAUST" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dec7uEem5ebMPg-wyFg" value="AZAUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79des7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79de87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dfM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dfc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dfs7uEem5ebMPg-wyFg" name="AZI5ST" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79df87uEem5ebMPg-wyFg" value="AZI5ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79dgM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79dgc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dgs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dg87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dhM7uEem5ebMPg-wyFg" name="AZADVA" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dhc7uEem5ebMPg-wyFg" value="AZADVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79dhs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l79dh87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79diM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dic7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dis7uEem5ebMPg-wyFg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79di87uEem5ebMPg-wyFg" name="AZCBPR" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79djM7uEem5ebMPg-wyFg" value="AZCBPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79djc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l79djs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79dj87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dkM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dkc7uEem5ebMPg-wyFg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dks7uEem5ebMPg-wyFg" name="AZBCNA" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dk87uEem5ebMPg-wyFg" value="AZBCNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79dlM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79dlc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dls7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dl87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dmM7uEem5ebMPg-wyFg" name="AZALNB" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dmc7uEem5ebMPg-wyFg" value="AZALNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79dms7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79dm87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dnM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dnc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dns7uEem5ebMPg-wyFg" name="AZAMNB" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dn87uEem5ebMPg-wyFg" value="AZAMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79doM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79doc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dos7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79do87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dpM7uEem5ebMPg-wyFg" name="AZANNB" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dpc7uEem5ebMPg-wyFg" value="AZANNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79dps7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79dp87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dqM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dqc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dqs7uEem5ebMPg-wyFg" name="AZAONB" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dq87uEem5ebMPg-wyFg" value="AZAONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79drM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79drc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79drs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dr87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dsM7uEem5ebMPg-wyFg" name="AZBGNA" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dsc7uEem5ebMPg-wyFg" value="AZBGNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79dss7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79ds87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dtM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dtc7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dts7uEem5ebMPg-wyFg" name="AZBHNA" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dt87uEem5ebMPg-wyFg" value="AZBHNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79duM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79duc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dus7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79du87uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dvM7uEem5ebMPg-wyFg" name="AZE2ST" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dvc7uEem5ebMPg-wyFg" value="AZE2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79dvs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79dv87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dwM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dwc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dws7uEem5ebMPg-wyFg" name="AZCHCD" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dw87uEem5ebMPg-wyFg" value="AZCHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79dxM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79dxc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dxs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dx87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dyM7uEem5ebMPg-wyFg" name="AZAJCD" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79dyc7uEem5ebMPg-wyFg" value="AZAJCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79dys7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l79dy87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79dzM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79dzc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79dzs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79dz87uEem5ebMPg-wyFg" name="AZBINA" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79d0M7uEem5ebMPg-wyFg" value="AZBINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79d0c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79d0s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79d087uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79d1M7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79d1c7uEem5ebMPg-wyFg" name="AZCQTX" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79d1s7uEem5ebMPg-wyFg" value="AZCQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79d187uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79d2M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79d2c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79d2s7uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79d287uEem5ebMPg-wyFg" name="AZI6ST" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79d3M7uEem5ebMPg-wyFg" value="AZI6ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79d3c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79d3s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79d387uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79d4M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79d4c7uEem5ebMPg-wyFg" name="AZBJN3" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79d4s7uEem5ebMPg-wyFg" value="AZBJN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l79d487uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l79d5M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l79d5c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l79d5s7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l79d587uEem5ebMPg-wyFg" name="AZBON3" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_l79d6M7uEem5ebMPg-wyFg" value="AZBON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-EgM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Egc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-Egs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Eg87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-EhM7uEem5ebMPg-wyFg" name="AZBPN3" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Ehc7uEem5ebMPg-wyFg" value="AZBPN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-Ehs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Eh87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-EiM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Eic7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-Eis7uEem5ebMPg-wyFg" name="AZHNTX" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Ei87uEem5ebMPg-wyFg" value="AZHNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-EjM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Ejc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-Ejs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Ej87uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-EkM7uEem5ebMPg-wyFg" name="AZHOTX" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Ekc7uEem5ebMPg-wyFg" value="AZHOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-Eks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Ek87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-ElM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Elc7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-Els7uEem5ebMPg-wyFg" name="AZHPTX" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-El87uEem5ebMPg-wyFg" value="AZHPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-EmM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Emc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-Ems7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Em87uEem5ebMPg-wyFg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-EnM7uEem5ebMPg-wyFg" name="AZHQTX" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Enc7uEem5ebMPg-wyFg" value="AZHQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-Ens7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-En87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-EoM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Eoc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-Eos7uEem5ebMPg-wyFg" name="AZBQN3" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Eo87uEem5ebMPg-wyFg" value="AZBQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-EpM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Epc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-Eps7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Ep87uEem5ebMPg-wyFg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-EqM7uEem5ebMPg-wyFg" name="AZAKCD" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Eqc7uEem5ebMPg-wyFg" value="AZAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-Eqs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Eq87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-ErM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Erc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-Ers7uEem5ebMPg-wyFg" name="AZFVPR" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Er87uEem5ebMPg-wyFg" value="AZFVPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-EsM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-Esc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Ess7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-Es87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-EtM7uEem5ebMPg-wyFg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-Etc7uEem5ebMPg-wyFg" name="AZGHDT" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Ets7uEem5ebMPg-wyFg" value="AZGHDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-Et87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-EuM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Euc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-Eus7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Eu87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-EvM7uEem5ebMPg-wyFg" name="AZGIDT" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Evc7uEem5ebMPg-wyFg" value="AZGIDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-Evs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-Ev87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-EwM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-Ewc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Ews7uEem5ebMPg-wyFg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l7-Ew87uEem5ebMPg-wyFg" name="ORAJREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l7-ExM7uEem5ebMPg-wyFg" value="ORAJREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l7-Exc7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l7-Exs7uEem5ebMPg-wyFg" name="AJAMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Ex87uEem5ebMPg-wyFg" value="AJAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-EyM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-Eyc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-Eys7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-Ey87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-EzM7uEem5ebMPg-wyFg" name="AJAJCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-Ezc7uEem5ebMPg-wyFg" value="AJAJCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-Ezs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-Ez87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-E0M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-E0c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-E0s7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-E087uEem5ebMPg-wyFg" name="AJAFTX" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-E1M7uEem5ebMPg-wyFg" value="AJAFTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-E1c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-E1s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-E187uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-E2M7uEem5ebMPg-wyFg" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-E2c7uEem5ebMPg-wyFg" name="AJAEST" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-E2s7uEem5ebMPg-wyFg" value="AJAEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-E287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-E3M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-E3c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-E3s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-E387uEem5ebMPg-wyFg" name="AJAFST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-E4M7uEem5ebMPg-wyFg" value="AJAFST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-E4c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-E4s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-E487uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-E5M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-E5c7uEem5ebMPg-wyFg" name="AJE9ST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-E5s7uEem5ebMPg-wyFg" value="AJE9ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-E587uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-E6M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-E6c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-E6s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-E687uEem5ebMPg-wyFg" name="AJLQST" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-E7M7uEem5ebMPg-wyFg" value="AJLQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-E7c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-E7s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-E787uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-E8M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-E8c7uEem5ebMPg-wyFg" name="AJNUST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-E8s7uEem5ebMPg-wyFg" value="AJNUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-E887uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-E9M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-E9c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-E9s7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l7-E987uEem5ebMPg-wyFg" name="ORJNREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l7-E-M7uEem5ebMPg-wyFg" value="ORJNREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l7-E-c7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l7-E-s7uEem5ebMPg-wyFg" name="JNB3NB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-E-87uEem5ebMPg-wyFg" value="JNB3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-E_M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-E_c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-E_s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-rkM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-rkc7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-rks7uEem5ebMPg-wyFg" name="JNAMCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-rk87uEem5ebMPg-wyFg" value="JNAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-rlM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-rlc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-rls7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-rl87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-rmM7uEem5ebMPg-wyFg" name="JNDMCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-rmc7uEem5ebMPg-wyFg" value="JNDMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-rms7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-rm87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-rnM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-rnc7uEem5ebMPg-wyFg" value="2"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l7-rns7uEem5ebMPg-wyFg" name="ORESREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l7-rn87uEem5ebMPg-wyFg" value="ORESREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l7-roM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l7-roc7uEem5ebMPg-wyFg" name="ESAMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-ros7uEem5ebMPg-wyFg" value="ESAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-ro87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-rpM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-rpc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-rps7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-rp87uEem5ebMPg-wyFg" name="ESDMCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-rqM7uEem5ebMPg-wyFg" value="ESDMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-rqc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-rqs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-rq87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-rrM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-rrc7uEem5ebMPg-wyFg" name="ESA3PC" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-rrs7uEem5ebMPg-wyFg" value="ESA3PC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-rr87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-rsM7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-rsc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-rss7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-rs87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-rtM7uEem5ebMPg-wyFg" name="ESBUVA" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-rtc7uEem5ebMPg-wyFg" value="ESBUVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-rts7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-rt87uEem5ebMPg-wyFg" value="8"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-ruM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-ruc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-rus7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-ru87uEem5ebMPg-wyFg" name="ESS8ST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-rvM7uEem5ebMPg-wyFg" value="ESS8ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-rvc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-rvs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-rv87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-rwM7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l7-rwc7uEem5ebMPg-wyFg" name="PIXEL_MAGENTO_CLIENTS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l7-rws7uEem5ebMPg-wyFg" value="PIXEL_MAGENTO_CLIENTS"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l7-rw87uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l7-rxM7uEem5ebMPg-wyFg" name="PMC_CLIENT_CODE" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-rxc7uEem5ebMPg-wyFg" value="PMC_CLIENT_CODE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-rxs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-rx87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-ryM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-ryc7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-rys7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-ry87uEem5ebMPg-wyFg" name="PMC_SOCIETE_CODE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-rzM7uEem5ebMPg-wyFg" value="PMC_SOCIETE_CODE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-rzc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-rzs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-rz87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-r0M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-r0c7uEem5ebMPg-wyFg" name="PMC_SOCIETE_ID_MAGENTO" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-r0s7uEem5ebMPg-wyFg" value="PMC_SOCIETE_ID_MAGENTO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-r087uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-r1M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-r1c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-r1s7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-r187uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-r2M7uEem5ebMPg-wyFg" name="PMC_DATE_CREATION" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-r2c7uEem5ebMPg-wyFg" value="PMC_DATE_CREATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-r2s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-r287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-r3M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-r3c7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-r3s7uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-r387uEem5ebMPg-wyFg" name="PMC_TOP_SUPPRESSION" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-r4M7uEem5ebMPg-wyFg" value="PMC_TOP_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-r4c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-r4s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-r487uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-r5M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-r5c7uEem5ebMPg-wyFg" name="PMC_DATE_SUPPRESSION" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-r5s7uEem5ebMPg-wyFg" value="PMC_DATE_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-r587uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-r6M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-r6c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-r6s7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-r687uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-r7M7uEem5ebMPg-wyFg" name="PMC_DATE_MODIFICATION" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-r7c7uEem5ebMPg-wyFg" value="PMC_DATE_MODIFICATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-r7s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-r787uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-r8M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-r8c7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-r8s7uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-r887uEem5ebMPg-wyFg" name="PMC_NOM" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-r9M7uEem5ebMPg-wyFg" value="PMC_NOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-r9c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-r9s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-r987uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-r-M7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-r-c7uEem5ebMPg-wyFg" name="PMC_PRENOM" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-r-s7uEem5ebMPg-wyFg" value="PMC_PRENOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-r-87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-r_M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-r_c7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-r_s7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-r_87uEem5ebMPg-wyFg" name="PMC_STORE_ID" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-sAM7uEem5ebMPg-wyFg" value="PMC_STORE_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-sAc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7-sAs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-sA87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-sBM7uEem5ebMPg-wyFg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-sBc7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-sBs7uEem5ebMPg-wyFg" name="PMC_MAIL_SOCIETE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-sB87uEem5ebMPg-wyFg" value="PMC_MAIL_SOCIETE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-sCM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-sCc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7-sCs7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7-sC87uEem5ebMPg-wyFg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7-sDM7uEem5ebMPg-wyFg" name="PMC_MAIL_SOCIETE_BIS" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7-sDc7uEem5ebMPg-wyFg" value="PMC_MAIL_SOCIETE_BIS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7-sDs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7-sD87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_SoM7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_Soc7uEem5ebMPg-wyFg" value="250"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l7_Sos7uEem5ebMPg-wyFg" name="WEBCLI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l7_So87uEem5ebMPg-wyFg" value="WEBCLI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l7_SpM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l7_Spc7uEem5ebMPg-wyFg" name="IDWEBC" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_Sps7uEem5ebMPg-wyFg" value="IDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_Sp87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_SqM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_Sqc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_Sqs7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_Sq87uEem5ebMPg-wyFg" name="CPTCLI" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_SrM7uEem5ebMPg-wyFg" value="CPTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_Src7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_Srs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_Sr87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_SsM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_Ssc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_Sss7uEem5ebMPg-wyFg" name="NOMCLI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_Ss87uEem5ebMPg-wyFg" value="NOMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_StM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_Stc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_Sts7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_St87uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_SuM7uEem5ebMPg-wyFg" name="PRECLI" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_Suc7uEem5ebMPg-wyFg" value="PRECLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_Sus7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_Su87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_SvM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_Svc7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_Svs7uEem5ebMPg-wyFg" name="PWDCLI" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_Sv87uEem5ebMPg-wyFg" value="PWDCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_SwM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_Swc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_Sws7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_Sw87uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_SxM7uEem5ebMPg-wyFg" name="MELADR" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_Sxc7uEem5ebMPg-wyFg" value="MELADR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_Sxs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_Sx87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_SyM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_Syc7uEem5ebMPg-wyFg" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_Sys7uEem5ebMPg-wyFg" name="MELSIT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_Sy87uEem5ebMPg-wyFg" value="MELSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_SzM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_Szc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_Szs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_Sz87uEem5ebMPg-wyFg" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_S0M7uEem5ebMPg-wyFg" name="NIVCLI" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_S0c7uEem5ebMPg-wyFg" value="NIVCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_S0s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_S087uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_S1M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_S1c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_S1s7uEem5ebMPg-wyFg" name="ACTCLI" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_S187uEem5ebMPg-wyFg" value="ACTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_S2M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_S2c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_S2s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_S287uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_S3M7uEem5ebMPg-wyFg" name="TYPTAR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_S3c7uEem5ebMPg-wyFg" value="TYPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_S3s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_S387uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_S4M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_S4c7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_S4s7uEem5ebMPg-wyFg" name="TXREMI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_S487uEem5ebMPg-wyFg" value="TXREMI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_S5M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_S5c7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_S5s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_S587uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_S6M7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_S6c7uEem5ebMPg-wyFg" name="FRANCO" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_S6s7uEem5ebMPg-wyFg" value="FRANCO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_S687uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_S7M7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_S7c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_S7s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_S787uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_S8M7uEem5ebMPg-wyFg" name="FRPORT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_S8c7uEem5ebMPg-wyFg" value="FRPORT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_S8s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_S887uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_S9M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_S9c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_S9s7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_S987uEem5ebMPg-wyFg" name="MINCDE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_S-M7uEem5ebMPg-wyFg" value="MINCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_S-c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_S-s7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_S-87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_S_M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_S_c7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_S_s7uEem5ebMPg-wyFg" name="MERCUR" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_S_87uEem5ebMPg-wyFg" value="MERCUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_TAM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_TAc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_TAs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_TA87uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_TBM7uEem5ebMPg-wyFg" name="CDECAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_TBc7uEem5ebMPg-wyFg" value="CDECAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_TBs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_TB87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_TCM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_TCc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_TCs7uEem5ebMPg-wyFg" name="CDELST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_TC87uEem5ebMPg-wyFg" value="CDELST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_TDM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_TDc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_TDs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_TD87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_TEM7uEem5ebMPg-wyFg" name="CDEMER" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_TEc7uEem5ebMPg-wyFg" value="CDEMER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_TEs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_TE87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_TFM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_TFc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_TFs7uEem5ebMPg-wyFg" name="CDEFAV" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_TF87uEem5ebMPg-wyFg" value="CDEFAV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_TGM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_TGc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_TGs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_TG87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_THM7uEem5ebMPg-wyFg" name="TARCLI" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_THc7uEem5ebMPg-wyFg" value="TARCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_THs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_TH87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_TIM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_TIc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_TIs7uEem5ebMPg-wyFg" name="UNIVCL" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_TI87uEem5ebMPg-wyFg" value="UNIVCL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_TJM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_5sM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_5sc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_5ss7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5s87uEem5ebMPg-wyFg" name="REMISE" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_5tM7uEem5ebMPg-wyFg" value="REMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5tc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_5ts7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_5t87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_5uM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_5uc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5us7uEem5ebMPg-wyFg" name="IMPTAR" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_5u87uEem5ebMPg-wyFg" value="IMPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5vM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_5vc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_5vs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_5v87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5wM7uEem5ebMPg-wyFg" name="CENTRA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_5wc7uEem5ebMPg-wyFg" value="CENTRA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5ws7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_5w87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_5xM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_5xc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_5xs7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5x87uEem5ebMPg-wyFg" name="MAILVRP" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_5yM7uEem5ebMPg-wyFg" value="MAILVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5yc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_5ys7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_5y87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_5zM7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5zc7uEem5ebMPg-wyFg" name="CLIMAIL" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_5zs7uEem5ebMPg-wyFg" value="CLIMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5z87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_50M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_50c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_50s7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5087uEem5ebMPg-wyFg" name="SPECIAL" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_51M7uEem5ebMPg-wyFg" value="SPECIAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_51c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_51s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_5187uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_52M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_52c7uEem5ebMPg-wyFg" name="CODSOC" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_52s7uEem5ebMPg-wyFg" value="CODSOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_53M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_53c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_53s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5387uEem5ebMPg-wyFg" name="TELVRP" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_54M7uEem5ebMPg-wyFg" value="TELVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_54c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_54s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_5487uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_55M7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_55c7uEem5ebMPg-wyFg" name="NOMVRP" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_55s7uEem5ebMPg-wyFg" value="NOMVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5587uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_56M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_56c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_56s7uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5687uEem5ebMPg-wyFg" name="LNGCLI" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_57M7uEem5ebMPg-wyFg" value="LNGCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_57c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_57s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_5787uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_58M7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_58c7uEem5ebMPg-wyFg" name="PWDMD5" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_58s7uEem5ebMPg-wyFg" value="PWDMD5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5887uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_59M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_59c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_59s7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5987uEem5ebMPg-wyFg" name="NOMCOM" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_5-M7uEem5ebMPg-wyFg" value="NOMCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5-c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_5-s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_5-87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_5_M7uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_5_c7uEem5ebMPg-wyFg" name="MELCOM" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_5_s7uEem5ebMPg-wyFg" value="MELCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_5_87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_6AM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_6Ac7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_6As7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_6A87uEem5ebMPg-wyFg" name="TOPCRE" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_6BM7uEem5ebMPg-wyFg" value="TOPCRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_6Bc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_6Bs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_6B87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_6CM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_6Cc7uEem5ebMPg-wyFg" name="TOPPROMO" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_6Cs7uEem5ebMPg-wyFg" value="TOPPROMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_6C87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_6DM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_6Dc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_6Ds7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_6D87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_6EM7uEem5ebMPg-wyFg" name="TOPMERCURIALE" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_6Ec7uEem5ebMPg-wyFg" value="TOPMERCURIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_6Es7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_6E87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_6FM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_6Fc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_6Fs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_6F87uEem5ebMPg-wyFg" name="TOPKIT" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_6GM7uEem5ebMPg-wyFg" value="TOPKIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_6Gc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_6Gs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_6G87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_6HM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_6Hc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_6Hs7uEem5ebMPg-wyFg" name="TOPPRINCIPAL" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_6H87uEem5ebMPg-wyFg" value="TOPPRINCIPAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_6IM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_6Ic7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_6Is7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_6I87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_6JM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_6Jc7uEem5ebMPg-wyFg" name="TOPSUPERVISION" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_6Js7uEem5ebMPg-wyFg" value="TOPSUPERVISION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_6J87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l7_6KM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l7_6Kc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l7_6Ks7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l7_6K87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l7_6LM7uEem5ebMPg-wyFg" name="MODRGLMT" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l7_6Lc7uEem5ebMPg-wyFg" value="MODRGLMT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l7_6Ls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8AgwM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Agwc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Agws7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Agw87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AgxM7uEem5ebMPg-wyFg" name="TOPFA" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Agxc7uEem5ebMPg-wyFg" value="TOPFA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Agxs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Agx87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AgyM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Agyc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Agys7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Agy87uEem5ebMPg-wyFg" name="TOPLE" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AgzM7uEem5ebMPg-wyFg" value="TOPLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Agzc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Agzs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Agz87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ag0M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ag0c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ag0s7uEem5ebMPg-wyFg" name="FACTURATION" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ag087uEem5ebMPg-wyFg" value="FACTURATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ag1M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ag1c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ag1s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ag187uEem5ebMPg-wyFg" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ag2M7uEem5ebMPg-wyFg" name="ADRESSE" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ag2c7uEem5ebMPg-wyFg" value="ADRESSE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ag2s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ag287uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ag3M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ag3c7uEem5ebMPg-wyFg" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ag3s7uEem5ebMPg-wyFg" name="TXREMISE" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ag387uEem5ebMPg-wyFg" value="TXREMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ag4M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ag4c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ag4s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ag487uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ag5M7uEem5ebMPg-wyFg" name="TOPCHR" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ag5c7uEem5ebMPg-wyFg" value="TOPCHR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ag5s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Ag587uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ag6M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ag6c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ag6s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ag687uEem5ebMPg-wyFg" name="MAXCDE" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ag7M7uEem5ebMPg-wyFg" value="MAXCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ag7c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Ag7s7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ag787uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ag8M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ag8c7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ag8s7uEem5ebMPg-wyFg" name="MERCUGRP" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ag887uEem5ebMPg-wyFg" value="MERCUGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ag9M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ag9c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ag9s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ag987uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ag-M7uEem5ebMPg-wyFg" name="CDEMERC" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ag-c7uEem5ebMPg-wyFg" value="CDEMERC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ag-s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ag-87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ag_M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ag_c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ag_s7uEem5ebMPg-wyFg" name="TOPEFAC" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ag_87uEem5ebMPg-wyFg" value="TOPEFAC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhAM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8AhAc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhAs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhA87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhBM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AhBc7uEem5ebMPg-wyFg" name="AUTOLOGKEY" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhBs7uEem5ebMPg-wyFg" value="AUTOLOGKEY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhB87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhCM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhCc7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhCs7uEem5ebMPg-wyFg" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8AhC87uEem5ebMPg-wyFg" name="PIXEL_MAGENTO_USERS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8AhDM7uEem5ebMPg-wyFg" value="PIXEL_MAGENTO_USERS"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8AhDc7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8AhDs7uEem5ebMPg-wyFg" name="PMU_CLIENT_CODE" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhD87uEem5ebMPg-wyFg" value="PMU_CLIENT_CODE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhEM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8AhEc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhEs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhE87uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhFM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AhFc7uEem5ebMPg-wyFg" name="PMU_SOCIETE_CODE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhFs7uEem5ebMPg-wyFg" value="PMU_SOCIETE_CODE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhF87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhGM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhGc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhGs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AhG87uEem5ebMPg-wyFg" name="PMU_ID_SOCIETE_MAGENTO" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhHM7uEem5ebMPg-wyFg" value="PMU_ID_SOCIETE_MAGENTO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhHc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8AhHs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhH87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhIM7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhIc7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AhIs7uEem5ebMPg-wyFg" name="PMU_USER_ID_MAGENTO" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhI87uEem5ebMPg-wyFg" value="PMU_USER_ID_MAGENTO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhJM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8AhJc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhJs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhJ87uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhKM7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AhKc7uEem5ebMPg-wyFg" name="PMU_DATE_CREATION" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhKs7uEem5ebMPg-wyFg" value="PMU_DATE_CREATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhK87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8AhLM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhLc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhLs7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhL87uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AhMM7uEem5ebMPg-wyFg" name="PMU_CIVILITE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhMc7uEem5ebMPg-wyFg" value="PMU_CIVILITE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhMs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhM87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhNM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhNc7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AhNs7uEem5ebMPg-wyFg" name="PMU_PRENOM_USER" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhN87uEem5ebMPg-wyFg" value="PMU_PRENOM_USER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhOM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhOc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhOs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhO87uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AhPM7uEem5ebMPg-wyFg" name="PMU_NOM_USER" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhPc7uEem5ebMPg-wyFg" value="PMU_NOM_USER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhPs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhP87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8AhQM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8AhQc7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8AhQs7uEem5ebMPg-wyFg" name="PMU_INTITULE_POSTE" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8AhQ87uEem5ebMPg-wyFg" value="PMU_INTITULE_POSTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8AhRM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8AhRc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BH0M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BH0c7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BH0s7uEem5ebMPg-wyFg" name="PMU_ADRESSE_EMAIL" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BH087uEem5ebMPg-wyFg" value="PMU_ADRESSE_EMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BH1M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BH1c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BH1s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BH187uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BH2M7uEem5ebMPg-wyFg" name="PMU_NIVEAU_USER" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BH2c7uEem5ebMPg-wyFg" value="PMU_NIVEAU_USER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BH2s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BH287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BH3M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BH3c7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BH3s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BH387uEem5ebMPg-wyFg" name="PMU_DATE_SUPPRESSION" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BH4M7uEem5ebMPg-wyFg" value="PMU_DATE_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BH4c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BH4s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BH487uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BH5M7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BH5c7uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BH5s7uEem5ebMPg-wyFg" name="PMU_SUPPRESSION_LOGIQUE" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BH587uEem5ebMPg-wyFg" value="PMU_SUPPRESSION_LOGIQUE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BH6M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BH6c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BH6s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BH687uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BH7M7uEem5ebMPg-wyFg" name="PMU_DATE_MODIF" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BH7c7uEem5ebMPg-wyFg" value="PMU_DATE_MODIF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BH7s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BH787uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BH8M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BH8c7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BH8s7uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BH887uEem5ebMPg-wyFg" name="PMU_ABONNEMENT_NEWSLETTER" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BH9M7uEem5ebMPg-wyFg" value="PMU_ABONNEMENT_NEWSLETTER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BH9c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BH9s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BH987uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BH-M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BH-c7uEem5ebMPg-wyFg" name="PMU_STORE_ID" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BH-s7uEem5ebMPg-wyFg" value="PMU_STORE_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BH-87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BH_M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BH_c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BH_s7uEem5ebMPg-wyFg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BH_87uEem5ebMPg-wyFg" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8BIAM7uEem5ebMPg-wyFg" name="OTKHCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8BIAc7uEem5ebMPg-wyFg" value="OTKHCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8BIAs7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8BIA87uEem5ebMPg-wyFg" name="KHBPCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BIBM7uEem5ebMPg-wyFg" value="KHBPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BIBc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BIBs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BIB87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BICM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BICc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BICs7uEem5ebMPg-wyFg" name="KHEONB" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BIC87uEem5ebMPg-wyFg" value="KHEONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BIDM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BIDc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BIDs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BID87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BIEM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BIEc7uEem5ebMPg-wyFg" name="KHYBC1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BIEs7uEem5ebMPg-wyFg" value="KHYBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BIE87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BIFM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BIFc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BIFs7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BIF87uEem5ebMPg-wyFg" name="KHQ7S3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BIGM7uEem5ebMPg-wyFg" value="KHQ7S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BIGc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BIGs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BIG87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BIHM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BIHc7uEem5ebMPg-wyFg" name="KHQ8S3" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BIHs7uEem5ebMPg-wyFg" value="KHQ8S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BIH87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BIIM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BIIc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BIIs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BII87uEem5ebMPg-wyFg" name="KHQ9S3" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BIJM7uEem5ebMPg-wyFg" value="KHQ9S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BIJc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BIJs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BIJ87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BIKM7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8BIKc7uEem5ebMPg-wyFg" name="ORB6REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8BIKs7uEem5ebMPg-wyFg" value="ORB6REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8BIK87uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8BILM7uEem5ebMPg-wyFg" name="B6AMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BILc7uEem5ebMPg-wyFg" value="B6AMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BILs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BIL87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BIMM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BIMc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BIMs7uEem5ebMPg-wyFg" name="B6BTCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BIM87uEem5ebMPg-wyFg" value="B6BTCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BINM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BINc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BINs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BIN87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BIOM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BIOc7uEem5ebMPg-wyFg" name="B6AUNB" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BIOs7uEem5ebMPg-wyFg" value="B6AUNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BIO87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BIPM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BIPc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BIPs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BIP87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BIQM7uEem5ebMPg-wyFg" name="B6RUNB" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Bu4M7uEem5ebMPg-wyFg" value="B6RUNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Bu4c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Bu4s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Bu487uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Bu5M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Bu5c7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Bu5s7uEem5ebMPg-wyFg" name="B6AZCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Bu587uEem5ebMPg-wyFg" value="B6AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Bu6M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Bu6c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Bu6s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Bu687uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Bu7M7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Bu7c7uEem5ebMPg-wyFg" name="B6BPCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Bu7s7uEem5ebMPg-wyFg" value="B6BPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Bu787uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Bu8M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Bu8c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Bu8s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Bu887uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Bu9M7uEem5ebMPg-wyFg" name="B6EONB" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Bu9c7uEem5ebMPg-wyFg" value="B6EONB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Bu9s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Bu987uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Bu-M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Bu-c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Bu-s7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Bu-87uEem5ebMPg-wyFg" name="B6BQCD" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Bu_M7uEem5ebMPg-wyFg" value="B6BQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Bu_c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Bu_s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Bu_87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvAM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvAc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvAs7uEem5ebMPg-wyFg" name="B6RHNB" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvA87uEem5ebMPg-wyFg" value="B6RHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvBM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvBc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvBs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvB87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvCM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvCc7uEem5ebMPg-wyFg" name="B6RINB" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvCs7uEem5ebMPg-wyFg" value="B6RINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvC87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvDM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvDc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvDs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvD87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvEM7uEem5ebMPg-wyFg" name="B6CACD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvEc7uEem5ebMPg-wyFg" value="B6CACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvEs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvE87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvFM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvFc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvFs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvF87uEem5ebMPg-wyFg" name="B6B9CD" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvGM7uEem5ebMPg-wyFg" value="B6B9CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvGc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvGs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvG87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvHM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvHc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvHs7uEem5ebMPg-wyFg" name="B6RFNB" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvH87uEem5ebMPg-wyFg" value="B6RFNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvIM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvIc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvIs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvI87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvJM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvJc7uEem5ebMPg-wyFg" name="B6RGNB" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvJs7uEem5ebMPg-wyFg" value="B6RGNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvJ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvKM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvKc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvKs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvK87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvLM7uEem5ebMPg-wyFg" name="B6S2CD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvLc7uEem5ebMPg-wyFg" value="B6S2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvLs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvL87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvMM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvMc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvMs7uEem5ebMPg-wyFg" name="B6DKN1" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvM87uEem5ebMPg-wyFg" value="B6DKN1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvNM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvNc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvNs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvN87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvOM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvOc7uEem5ebMPg-wyFg" name="B6LKNB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvOs7uEem5ebMPg-wyFg" value="B6LKNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvO87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvPM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvPc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvPs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvP87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvQM7uEem5ebMPg-wyFg" name="B6RWNB" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvQc7uEem5ebMPg-wyFg" value="B6RWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvQs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvQ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvRM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvRc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvRs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvR87uEem5ebMPg-wyFg" name="B6BITX" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvSM7uEem5ebMPg-wyFg" value="B6BITX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvSc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvSs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvS87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvTM7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvTc7uEem5ebMPg-wyFg" name="B6BFCD" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvTs7uEem5ebMPg-wyFg" value="B6BFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvT87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvUM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvUc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvUs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvU87uEem5ebMPg-wyFg" name="B6XIST" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvVM7uEem5ebMPg-wyFg" value="B6XIST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvVc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvVs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvV87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvWM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvWc7uEem5ebMPg-wyFg" name="B6R2NB" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvWs7uEem5ebMPg-wyFg" value="B6R2NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvW87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvXM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvXc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvXs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvX87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvYM7uEem5ebMPg-wyFg" name="B6HMCD" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8BvYc7uEem5ebMPg-wyFg" value="B6HMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8BvYs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8BvY87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8BvZM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8BvZc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8BvZs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8BvZ87uEem5ebMPg-wyFg" name="B6XFST" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CV8M7uEem5ebMPg-wyFg" value="B6XFST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CV8c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CV8s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CV887uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CV9M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CV9c7uEem5ebMPg-wyFg" name="B6HBCD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CV9s7uEem5ebMPg-wyFg" value="B6HBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CV987uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CV-M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CV-c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CV-s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CV-87uEem5ebMPg-wyFg" name="B6LGST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CV_M7uEem5ebMPg-wyFg" value="B6LGST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CV_c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CV_s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CV_87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWAM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWAc7uEem5ebMPg-wyFg" name="B6AKQT" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWAs7uEem5ebMPg-wyFg" value="B6AKQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWA87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWBM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWBc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWBs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWB87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWCM7uEem5ebMPg-wyFg" name="B6HCCD" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWCc7uEem5ebMPg-wyFg" value="B6HCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWCs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWC87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWDM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWDc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWDs7uEem5ebMPg-wyFg" name="B6C4PC" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWD87uEem5ebMPg-wyFg" value="B6C4PC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWEM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWEc7uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWEs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWE87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWFM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWFc7uEem5ebMPg-wyFg" name="B6E3PR" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWFs7uEem5ebMPg-wyFg" value="B6E3PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWF87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWGM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWGc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWGs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWG87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWHM7uEem5ebMPg-wyFg" name="B6E4PR" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWHc7uEem5ebMPg-wyFg" value="B6E4PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWHs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWH87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWIM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWIc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWIs7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWI87uEem5ebMPg-wyFg" name="B6OTPR" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWJM7uEem5ebMPg-wyFg" value="B6OTPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWJc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWJs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWJ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWKM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWKc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWKs7uEem5ebMPg-wyFg" name="B6AOPR" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWK87uEem5ebMPg-wyFg" value="B6AOPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWLM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWLc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWLs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWL87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWMM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWMc7uEem5ebMPg-wyFg" name="B6JNP2" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWMs7uEem5ebMPg-wyFg" value="B6JNP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWM87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWNM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWNc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWNs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWN87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWOM7uEem5ebMPg-wyFg" name="B6HBP1" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWOc7uEem5ebMPg-wyFg" value="B6HBP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWOs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWO87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWPM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWPc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWPs7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWP87uEem5ebMPg-wyFg" name="B6JOP2" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWQM7uEem5ebMPg-wyFg" value="B6JOP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWQc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWQs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWQ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWRM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWRc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWRs7uEem5ebMPg-wyFg" name="B6G9P1" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWR87uEem5ebMPg-wyFg" value="B6G9P1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWSM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWSc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWSs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWS87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWTM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWTc7uEem5ebMPg-wyFg" name="B6HAP1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWTs7uEem5ebMPg-wyFg" value="B6HAP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWT87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWUM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWUc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWUs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWU87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWVM7uEem5ebMPg-wyFg" name="B6APPR" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWVc7uEem5ebMPg-wyFg" value="B6APPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWVs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWV87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWWM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWWc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWWs7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWW87uEem5ebMPg-wyFg" name="B6LDP2" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWXM7uEem5ebMPg-wyFg" value="B6LDP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWXc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWXs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWX87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWYM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWYc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWYs7uEem5ebMPg-wyFg" name="B6CBPC" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWY87uEem5ebMPg-wyFg" value="B6CBPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWZM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWZc7uEem5ebMPg-wyFg" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWZs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWZ87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWaM7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWac7uEem5ebMPg-wyFg" name="B6HNNB" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWas7uEem5ebMPg-wyFg" value="B6HNNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWa87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWbM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWbc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWbs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8CWb87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8CWcM7uEem5ebMPg-wyFg" name="B6JFNB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8CWcc7uEem5ebMPg-wyFg" value="B6JFNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8CWcs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8CWc87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8CWdM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8CWdc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9AM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9Ac7uEem5ebMPg-wyFg" name="B6HECD" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9As7uEem5ebMPg-wyFg" value="B6HECD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9A87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9BM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9Bc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9Bs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9B87uEem5ebMPg-wyFg" name="B6HLCD" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9CM7uEem5ebMPg-wyFg" value="B6HLCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9Cc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8C9Cs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9C87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9DM7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9Dc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9Ds7uEem5ebMPg-wyFg" name="B6LIST" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9D87uEem5ebMPg-wyFg" value="B6LIST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9EM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9Ec7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9Es7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9E87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9FM7uEem5ebMPg-wyFg" name="B6BCCD" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9Fc7uEem5ebMPg-wyFg" value="B6BCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9Fs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8C9F87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9GM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9Gc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9Gs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9G87uEem5ebMPg-wyFg" name="B6DDS1" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9HM7uEem5ebMPg-wyFg" value="B6DDS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9Hc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9Hs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9H87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9IM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9Ic7uEem5ebMPg-wyFg" name="B6JGNB" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9Is7uEem5ebMPg-wyFg" value="B6JGNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9I87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8C9JM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9Jc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9Js7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9J87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9KM7uEem5ebMPg-wyFg" name="B6DES1" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9Kc7uEem5ebMPg-wyFg" value="B6DES1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9Ks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9K87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9LM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9Lc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9Ls7uEem5ebMPg-wyFg" name="B6JHNB" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9L87uEem5ebMPg-wyFg" value="B6JHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9MM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8C9Mc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9Ms7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9M87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9NM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9Nc7uEem5ebMPg-wyFg" name="B6DFS1" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9Ns7uEem5ebMPg-wyFg" value="B6DFS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9N87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9OM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9Oc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9Os7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9O87uEem5ebMPg-wyFg" name="B6DGS1" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9PM7uEem5ebMPg-wyFg" value="B6DGS1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9Pc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9Ps7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9P87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9QM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9Qc7uEem5ebMPg-wyFg" name="B6JJC1" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9Qs7uEem5ebMPg-wyFg" value="B6JJC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9Q87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9RM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9Rc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9Rs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9R87uEem5ebMPg-wyFg" name="B6QSDT" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9SM7uEem5ebMPg-wyFg" value="B6QSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9Sc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8C9Ss7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9S87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9TM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9Tc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9Ts7uEem5ebMPg-wyFg" name="B6QTDT" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9T87uEem5ebMPg-wyFg" value="B6QTDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9UM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8C9Uc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9Us7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9U87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9VM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9Vc7uEem5ebMPg-wyFg" name="B6RXP2" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9Vs7uEem5ebMPg-wyFg" value="B6RXP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9V87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8C9WM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9Wc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9Ws7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9W87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9XM7uEem5ebMPg-wyFg" name="B6RYP2" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9Xc7uEem5ebMPg-wyFg" value="B6RYP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9Xs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8C9X87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9YM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9Yc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9Ys7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9Y87uEem5ebMPg-wyFg" name="B6W6S2" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9ZM7uEem5ebMPg-wyFg" value="B6W6S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9Zc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9Zs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9Z87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9aM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9ac7uEem5ebMPg-wyFg" name="B6W7S2" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9as7uEem5ebMPg-wyFg" value="B6W7S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9a87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9bM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9bc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9bs7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8C9b87uEem5ebMPg-wyFg" name="ORB7REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8C9cM7uEem5ebMPg-wyFg" value="ORB7REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8C9cc7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8C9cs7uEem5ebMPg-wyFg" name="B7AMCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9c87uEem5ebMPg-wyFg" value="B7AMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9dM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9dc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9ds7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9d87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9eM7uEem5ebMPg-wyFg" name="B7BTCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9ec7uEem5ebMPg-wyFg" value="B7BTCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9es7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8C9e87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9fM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9fc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9fs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8C9f87uEem5ebMPg-wyFg" name="B7CQST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8C9gM7uEem5ebMPg-wyFg" value="B7CQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8C9gc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8C9gs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8C9g87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8C9hM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkEM7uEem5ebMPg-wyFg" name="B7PMST" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkEc7uEem5ebMPg-wyFg" value="B7PMST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkEs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkE87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkFM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkFc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkFs7uEem5ebMPg-wyFg" name="B7ATPR" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkF87uEem5ebMPg-wyFg" value="B7ATPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkGM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkGc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkGs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkG87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkHM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkHc7uEem5ebMPg-wyFg" name="B7JLP2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkHs7uEem5ebMPg-wyFg" value="B7JLP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkH87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkIM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkIc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkIs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkI87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkJM7uEem5ebMPg-wyFg" name="B7ASPR" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkJc7uEem5ebMPg-wyFg" value="B7ASPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkJs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkJ87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkKM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkKc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkKs7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkK87uEem5ebMPg-wyFg" name="B7JYP2" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkLM7uEem5ebMPg-wyFg" value="B7JYP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkLc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkLs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkL87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkMM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkMc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkMs7uEem5ebMPg-wyFg" name="B7K8PR" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkM87uEem5ebMPg-wyFg" value="B7K8PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkNM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkNc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkNs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkN87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkOM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkOc7uEem5ebMPg-wyFg" name="B7HCP1" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkOs7uEem5ebMPg-wyFg" value="B7HCP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkO87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkPM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkPc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkPs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkP87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkQM7uEem5ebMPg-wyFg" name="B7HDP1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkQc7uEem5ebMPg-wyFg" value="B7HDP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkQs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkQ87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkRM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkRc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkRs7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkR87uEem5ebMPg-wyFg" name="B7HEP1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkSM7uEem5ebMPg-wyFg" value="B7HEP1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkSc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkSs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkS87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkTM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkTc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkTs7uEem5ebMPg-wyFg" name="B7JMP2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkT87uEem5ebMPg-wyFg" value="B7JMP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkUM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkUc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkUs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkU87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkVM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkVc7uEem5ebMPg-wyFg" name="B7C3PC" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkVs7uEem5ebMPg-wyFg" value="B7C3PC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkV87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8DkWM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkWc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkWs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkW87uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkXM7uEem5ebMPg-wyFg" name="B7LDST" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkXc7uEem5ebMPg-wyFg" value="B7LDST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkXs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkX87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkYM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkYc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkYs7uEem5ebMPg-wyFg" name="B7K7ST" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkY87uEem5ebMPg-wyFg" value="B7K7ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkZM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkZc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkZs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkZ87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DkaM7uEem5ebMPg-wyFg" name="B7HHNB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Dkac7uEem5ebMPg-wyFg" value="B7HHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Dkas7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Dka87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkbM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Dkbc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Dkbs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Dkb87uEem5ebMPg-wyFg" name="B7CSST" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkcM7uEem5ebMPg-wyFg" value="B7CSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Dkcc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Dkcs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Dkc87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkdM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Dkdc7uEem5ebMPg-wyFg" name="B7K8ST" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Dkds7uEem5ebMPg-wyFg" value="B7K8ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Dkd87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkeM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Dkec7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Dkes7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Dke87uEem5ebMPg-wyFg" name="B7LEST" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkfM7uEem5ebMPg-wyFg" value="B7LEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Dkfc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Dkfs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Dkf87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8DkgM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Dkgc7uEem5ebMPg-wyFg" name="B7K9ST" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Dkgs7uEem5ebMPg-wyFg" value="B7K9ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Dkg87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8DkhM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Dkhc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Dkhs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Dkh87uEem5ebMPg-wyFg" name="B7E0DT" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8DkiM7uEem5ebMPg-wyFg" value="B7E0DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Dkic7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Dkis7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Dki87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8DkjM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Dkjc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Dkjs7uEem5ebMPg-wyFg" name="B7ASCD" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Dkj87uEem5ebMPg-wyFg" value="B7ASCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8DkkM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Dkkc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Dkks7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Dkk87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8DklM7uEem5ebMPg-wyFg" name="B7E1DT" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Dklc7uEem5ebMPg-wyFg" value="B7E1DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Dkls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELIM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELIc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELIs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELI87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELJM7uEem5ebMPg-wyFg" name="B7LBST" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELJc7uEem5ebMPg-wyFg" value="B7LBST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELJs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELJ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELKM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELKc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELKs7uEem5ebMPg-wyFg" name="B7LCST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELK87uEem5ebMPg-wyFg" value="B7LCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELLM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELLc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELLs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELL87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELMM7uEem5ebMPg-wyFg" name="B7FTNB" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELMc7uEem5ebMPg-wyFg" value="B7FTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELMs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELM87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELNM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELNc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELNs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELN87uEem5ebMPg-wyFg" name="B7AZCD" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELOM7uEem5ebMPg-wyFg" value="B7AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELOc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELOs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELO87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELPM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELPc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELPs7uEem5ebMPg-wyFg" name="B7JENB" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELP87uEem5ebMPg-wyFg" value="B7JENB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELQM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELQc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELQs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELQ87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELRM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELRc7uEem5ebMPg-wyFg" name="B7NPTX" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELRs7uEem5ebMPg-wyFg" value="B7NPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELR87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELSM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELSc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELSs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELS87uEem5ebMPg-wyFg" name="B7BCCD" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELTM7uEem5ebMPg-wyFg" value="B7BCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELTc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELTs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELT87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELUM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELUc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELUs7uEem5ebMPg-wyFg" name="B7GHCD" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELU87uEem5ebMPg-wyFg" value="B7GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELVM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELVc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELVs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELV87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELWM7uEem5ebMPg-wyFg" name="B7AQCD" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELWc7uEem5ebMPg-wyFg" value="B7AQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELWs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELW87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELXM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELXc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELXs7uEem5ebMPg-wyFg" name="B7BHCD" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELX87uEem5ebMPg-wyFg" value="B7BHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELYM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELYc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELYs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELY87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELZM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELZc7uEem5ebMPg-wyFg" name="B7B1CD" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELZs7uEem5ebMPg-wyFg" value="B7B1CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELZ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELaM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELac7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELas7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELa87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELbM7uEem5ebMPg-wyFg" name="B7GYCD" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELbc7uEem5ebMPg-wyFg" value="B7GYCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELbs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELb87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELcM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELcc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELcs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELc87uEem5ebMPg-wyFg" name="B7BWPC" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELdM7uEem5ebMPg-wyFg" value="B7BWPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELdc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELds7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELd87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELeM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELec7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELes7uEem5ebMPg-wyFg" name="B7BXPC" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELe87uEem5ebMPg-wyFg" value="B7BXPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELfM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELfc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELfs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELf87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELgM7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELgc7uEem5ebMPg-wyFg" name="B7ADCD" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELgs7uEem5ebMPg-wyFg" value="B7ADCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELg87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELhM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELhc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELhs7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELh87uEem5ebMPg-wyFg" name="B7J2ST" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELiM7uEem5ebMPg-wyFg" value="B7J2ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELic7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELis7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELi87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELjM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELjc7uEem5ebMPg-wyFg" name="B7D0S1" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELjs7uEem5ebMPg-wyFg" value="B7D0S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELj87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELkM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELkc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELks7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELk87uEem5ebMPg-wyFg" name="B7PNST" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELlM7uEem5ebMPg-wyFg" value="B7PNST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELlc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ELls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELl87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELmM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELmc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELms7uEem5ebMPg-wyFg" name="B7D1S1" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELm87uEem5ebMPg-wyFg" value="B7D1S1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELnM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELnc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELns7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELn87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELoM7uEem5ebMPg-wyFg" name="B7LHS2" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ELoc7uEem5ebMPg-wyFg" value="B7LHS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ELos7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ELo87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ELpM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ELpc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ELps7uEem5ebMPg-wyFg" name="B7X0CD" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EyMM7uEem5ebMPg-wyFg" value="B7X0CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyMc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EyMs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyM87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EyNM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EyNc7uEem5ebMPg-wyFg" name="B7AADT" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EyNs7uEem5ebMPg-wyFg" value="B7AADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyN87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8EyOM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EyOc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyOs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EyO87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EyPM7uEem5ebMPg-wyFg" name="B7AATX" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EyPc7uEem5ebMPg-wyFg" value="B7AATX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyPs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EyP87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyQM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EyQc7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EyQs7uEem5ebMPg-wyFg" name="B7LBTX" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EyQ87uEem5ebMPg-wyFg" value="B7LBTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyRM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EyRc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyRs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EyR87uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EySM7uEem5ebMPg-wyFg" name="B7QYDT" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EySc7uEem5ebMPg-wyFg" value="B7QYDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EySs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8EyS87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EyTM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyTc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EyTs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EyT87uEem5ebMPg-wyFg" name="B7QZDT" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EyUM7uEem5ebMPg-wyFg" value="B7QZDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyUc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8EyUs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EyU87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyVM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EyVc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EyVs7uEem5ebMPg-wyFg" name="B7R3P2" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EyV87uEem5ebMPg-wyFg" value="B7R3P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyWM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8EyWc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EyWs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyW87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EyXM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EyXc7uEem5ebMPg-wyFg" name="B7R4P2" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EyXs7uEem5ebMPg-wyFg" value="B7R4P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyX87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8EyYM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EyYc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyYs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EyY87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EyZM7uEem5ebMPg-wyFg" name="B7XCS2" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EyZc7uEem5ebMPg-wyFg" value="B7XCS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyZs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EyZ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyaM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eyac7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Eyas7uEem5ebMPg-wyFg" name="B7XDS2" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eya87uEem5ebMPg-wyFg" value="B7XDS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EybM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eybc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Eybs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eyb87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EycM7uEem5ebMPg-wyFg" name="B7WST1" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eycc7uEem5ebMPg-wyFg" value="B7WST1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Eycs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eyc87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EydM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eydc7uEem5ebMPg-wyFg" value="35"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Eyds7uEem5ebMPg-wyFg" name="OSNCREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Eyd87uEem5ebMPg-wyFg" value="OSNCREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8EyeM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Eyec7uEem5ebMPg-wyFg" name="NCAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eyes7uEem5ebMPg-wyFg" value="NCAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Eye87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8EyfM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eyfc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Eyfs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eyf87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EygM7uEem5ebMPg-wyFg" name="NCYCS2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eygc7uEem5ebMPg-wyFg" value="NCYCS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Eygs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eyg87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EyhM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eyhc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Eyhs7uEem5ebMPg-wyFg" name="NCYMS2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eyh87uEem5ebMPg-wyFg" value="NCYMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyiM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eyic7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Eyis7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eyi87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EyjM7uEem5ebMPg-wyFg" name="NCYDS2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eyjc7uEem5ebMPg-wyFg" value="NCYDS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Eyjs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eyj87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EykM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eykc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Eyks7uEem5ebMPg-wyFg" name="NCRSDT" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eyk87uEem5ebMPg-wyFg" value="NCRSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EylM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Eylc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eyls7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Eyl87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EymM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Eymc7uEem5ebMPg-wyFg" name="NCYES2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eyms7uEem5ebMPg-wyFg" value="NCYES2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Eym87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8EynM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Eync7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eyns7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Eyn87uEem5ebMPg-wyFg" name="NCRTDT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8EyoM7uEem5ebMPg-wyFg" value="NCRTDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Eyoc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Eyos7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eyo87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8EypM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eypc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Eyps7uEem5ebMPg-wyFg" name="NCZ5N5" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eyp87uEem5ebMPg-wyFg" value="NCZ5N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8EyqM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Eyqc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eyqs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Eyq87uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8EyrM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Eyrc7uEem5ebMPg-wyFg" name="NCRUDT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eyrs7uEem5ebMPg-wyFg" value="NCRUDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Eyr87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8EysM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Eysc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Eyss7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Eys87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8EytM7uEem5ebMPg-wyFg" name="NCZ6N5" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Eytc7uEem5ebMPg-wyFg" value="NCZ6N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZQM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8FZQc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZQs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZQ87uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZRM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZRc7uEem5ebMPg-wyFg" name="NCRVDT" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZRs7uEem5ebMPg-wyFg" value="NCRVDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZR87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8FZSM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZSc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZSs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZS87uEem5ebMPg-wyFg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8FZTM7uEem5ebMPg-wyFg" name="ORAKREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8FZTc7uEem5ebMPg-wyFg" value="ORAKREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8FZTs7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8FZT87uEem5ebMPg-wyFg" name="AKAKCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZUM7uEem5ebMPg-wyFg" value="AKAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZUc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZUs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZU87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZVM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZVc7uEem5ebMPg-wyFg" name="AKAGTX" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZVs7uEem5ebMPg-wyFg" value="AKAGTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZV87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZWM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZWc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZWs7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZW87uEem5ebMPg-wyFg" name="AKAGST" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZXM7uEem5ebMPg-wyFg" value="AKAGST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZXc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZXs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZX87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZYM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZYc7uEem5ebMPg-wyFg" name="AKNSST" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZYs7uEem5ebMPg-wyFg" value="AKNSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZY87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZZM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZZc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZZs7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8FZZ87uEem5ebMPg-wyFg" name="ORGQREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8FZaM7uEem5ebMPg-wyFg" value="ORGQREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8FZac7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8FZas7uEem5ebMPg-wyFg" name="GQFWNB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZa87uEem5ebMPg-wyFg" value="GQFWNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZbM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8FZbc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZbs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZb87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZcM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZcc7uEem5ebMPg-wyFg" name="GQAKCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZcs7uEem5ebMPg-wyFg" value="GQAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZc87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZdM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZdc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZds7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZd87uEem5ebMPg-wyFg" name="GQBDCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZeM7uEem5ebMPg-wyFg" value="GQBDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZec7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZes7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZe87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZfM7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZfc7uEem5ebMPg-wyFg" name="GQFOTX" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZfs7uEem5ebMPg-wyFg" value="GQFOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZf87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZgM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZgc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZgs7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZg87uEem5ebMPg-wyFg" name="GQFQTX" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZhM7uEem5ebMPg-wyFg" value="GQFQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZhc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZhs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZh87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZiM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZic7uEem5ebMPg-wyFg" name="GQFRTX" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZis7uEem5ebMPg-wyFg" value="GQFRTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZi87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZjM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZjc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZjs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZj87uEem5ebMPg-wyFg" name="GQHUNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZkM7uEem5ebMPg-wyFg" value="GQHUNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZkc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZks7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZk87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZlM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZlc7uEem5ebMPg-wyFg" name="GQOVST" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZls7uEem5ebMPg-wyFg" value="GQOVST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZl87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZmM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZmc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZms7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZm87uEem5ebMPg-wyFg" name="GQDQN3" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZnM7uEem5ebMPg-wyFg" value="GQDQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZnc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZns7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZn87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZoM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZoc7uEem5ebMPg-wyFg" name="GQDRN3" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZos7uEem5ebMPg-wyFg" value="GQDRN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZo87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZpM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZpc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZps7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZp87uEem5ebMPg-wyFg" name="GQKINA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZqM7uEem5ebMPg-wyFg" value="GQKINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZqc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZqs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZq87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZrM7uEem5ebMPg-wyFg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZrc7uEem5ebMPg-wyFg" name="GQKJNA" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZrs7uEem5ebMPg-wyFg" value="GQKJNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZr87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZsM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZsc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZss7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZs87uEem5ebMPg-wyFg" name="GQMKTX" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZtM7uEem5ebMPg-wyFg" value="GQMKTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZtc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZts7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZt87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZuM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZuc7uEem5ebMPg-wyFg" name="GQMLTX" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZus7uEem5ebMPg-wyFg" value="GQMLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZu87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZvM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZvc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZvs7uEem5ebMPg-wyFg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZv87uEem5ebMPg-wyFg" name="GQMMTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZwM7uEem5ebMPg-wyFg" value="GQMMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZwc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8FZws7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8FZw87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8FZxM7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8FZxc7uEem5ebMPg-wyFg" name="GQMNTX" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8FZxs7uEem5ebMPg-wyFg" value="GQMNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8FZx87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAUM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAUc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAUs7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAU87uEem5ebMPg-wyFg" name="GQN8CD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAVM7uEem5ebMPg-wyFg" value="GQN8CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAVc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAVs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAV87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAWM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAWc7uEem5ebMPg-wyFg" name="GQN9CD" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAWs7uEem5ebMPg-wyFg" value="GQN9CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAW87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAXM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAXc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAXs7uEem5ebMPg-wyFg" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8GAX87uEem5ebMPg-wyFg" name="OSNEREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8GAYM7uEem5ebMPg-wyFg" value="OSNEREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8GAYc7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8GAYs7uEem5ebMPg-wyFg" name="NEZ7N5" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAY87uEem5ebMPg-wyFg" value="NEZ7N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAZM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8GAZc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAZs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAZ87uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAaM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAac7uEem5ebMPg-wyFg" name="NEYFS2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAas7uEem5ebMPg-wyFg" value="NEYFS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAa87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAbM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAbc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAbs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAb87uEem5ebMPg-wyFg" name="NEYGS2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAcM7uEem5ebMPg-wyFg" value="NEYGS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAcc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAcs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAc87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAdM7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8GAdc7uEem5ebMPg-wyFg" name="OSLZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8GAds7uEem5ebMPg-wyFg" value="OSLZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8GAd87uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8GAeM7uEem5ebMPg-wyFg" name="LZJKC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAec7uEem5ebMPg-wyFg" value="LZJKC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAes7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAe87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAfM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAfc7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAfs7uEem5ebMPg-wyFg" name="LZJLC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAf87uEem5ebMPg-wyFg" value="LZJLC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAgM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAgc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAgs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAg87uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAhM7uEem5ebMPg-wyFg" name="LZXGN5" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAhc7uEem5ebMPg-wyFg" value="LZXGN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAhs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8GAh87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAiM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAic7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAis7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAi87uEem5ebMPg-wyFg" name="LZWUT1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAjM7uEem5ebMPg-wyFg" value="LZWUT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAjc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAjs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAj87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAkM7uEem5ebMPg-wyFg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAkc7uEem5ebMPg-wyFg" name="LZWVT1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAks7uEem5ebMPg-wyFg" value="LZWVT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAk87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAlM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAlc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAls7uEem5ebMPg-wyFg" value="70"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8GAl87uEem5ebMPg-wyFg" name="OSMVCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8GAmM7uEem5ebMPg-wyFg" value="OSMVCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8GAmc7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8GAms7uEem5ebMPg-wyFg" name="MVAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAm87uEem5ebMPg-wyFg" value="MVAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAnM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8GAnc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAns7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAn87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAoM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAoc7uEem5ebMPg-wyFg" name="MVGHCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAos7uEem5ebMPg-wyFg" value="MVGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAo87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GApM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GApc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAps7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAp87uEem5ebMPg-wyFg" name="MVJ6C1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAqM7uEem5ebMPg-wyFg" value="MVJ6C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAqc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAqs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAq87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GArM7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GArc7uEem5ebMPg-wyFg" name="MVLQN3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GArs7uEem5ebMPg-wyFg" value="MVLQN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAr87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAsM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAsc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAss7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAs87uEem5ebMPg-wyFg" name="MVJZC1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAtM7uEem5ebMPg-wyFg" value="MVJZC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAtc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAts7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAt87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAuM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAuc7uEem5ebMPg-wyFg" name="MVJ0C1" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAus7uEem5ebMPg-wyFg" value="MVJ0C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAu87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAvM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAvc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAvs7uEem5ebMPg-wyFg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAv87uEem5ebMPg-wyFg" name="MVYAT1" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAwM7uEem5ebMPg-wyFg" value="MVYAT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAwc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAws7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAw87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAxM7uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAxc7uEem5ebMPg-wyFg" name="MVYBT1" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAxs7uEem5ebMPg-wyFg" value="MVYBT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAx87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAyM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAyc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GAys7uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GAy87uEem5ebMPg-wyFg" name="MVYCT1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GAzM7uEem5ebMPg-wyFg" value="MVYCT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GAzc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GAzs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GAz87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GA0M7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GA0c7uEem5ebMPg-wyFg" name="MVYDT1" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GA0s7uEem5ebMPg-wyFg" value="MVYDT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GA087uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GA1M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GA1c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GnYM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GnYc7uEem5ebMPg-wyFg" name="MVYET1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GnYs7uEem5ebMPg-wyFg" value="MVYET1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GnY87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GnZM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GnZc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GnZs7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8GnZ87uEem5ebMPg-wyFg" name="MVYFT1" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GnaM7uEem5ebMPg-wyFg" value="MVYFT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnac7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gnas7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gna87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GnbM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnbc7uEem5ebMPg-wyFg" name="MVYGT1" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gnbs7uEem5ebMPg-wyFg" value="MVYGT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnb87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GncM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gncc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gncs7uEem5ebMPg-wyFg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnc87uEem5ebMPg-wyFg" name="MVZ1N5" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GndM7uEem5ebMPg-wyFg" value="MVZ1N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gndc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Gnds7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gnd87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8GneM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gnec7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnes7uEem5ebMPg-wyFg" name="MVZ2N5" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gne87uEem5ebMPg-wyFg" value="MVZ2N5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8GnfM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Gnfc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gnfs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnf87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GngM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gngc7uEem5ebMPg-wyFg" name="MVYAS2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gngs7uEem5ebMPg-wyFg" value="MVYAS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gng87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GnhM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnhc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gnhs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnh87uEem5ebMPg-wyFg" name="MVYHT1" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GniM7uEem5ebMPg-wyFg" value="MVYHT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnic7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gnis7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gni87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GnjM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnjc7uEem5ebMPg-wyFg" name="MVYIT1" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gnjs7uEem5ebMPg-wyFg" value="MVYIT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnj87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GnkM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnkc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gnks7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnk87uEem5ebMPg-wyFg" name="MVYJT1" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GnlM7uEem5ebMPg-wyFg" value="MVYJT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnlc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gnls7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnl87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GnmM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnmc7uEem5ebMPg-wyFg" name="MVYKT1" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gnms7uEem5ebMPg-wyFg" value="MVYKT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnm87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GnnM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnnc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gnns7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnn87uEem5ebMPg-wyFg" name="MVYLT1" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GnoM7uEem5ebMPg-wyFg" value="MVYLT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnoc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gnos7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gno87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GnpM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnpc7uEem5ebMPg-wyFg" name="MVYMT1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gnps7uEem5ebMPg-wyFg" value="MVYMT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnp87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GnqM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnqc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gnqs7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnq87uEem5ebMPg-wyFg" name="MVYNT1" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GnrM7uEem5ebMPg-wyFg" value="MVYNT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnrc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gnrs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnr87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GnsM7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnsc7uEem5ebMPg-wyFg" name="MVYOT1" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gnss7uEem5ebMPg-wyFg" value="MVYOT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gns87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GntM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gntc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gnts7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnt87uEem5ebMPg-wyFg" name="MVYPT1" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GnuM7uEem5ebMPg-wyFg" value="MVYPT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnuc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gnus7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnu87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GnvM7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnvc7uEem5ebMPg-wyFg" name="MVYQT1" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gnvs7uEem5ebMPg-wyFg" value="MVYQT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnv87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GnwM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnwc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gnws7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnw87uEem5ebMPg-wyFg" name="MVYRT1" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8GnxM7uEem5ebMPg-wyFg" value="MVYRT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gnxc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gnxs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnx87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8GnyM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnyc7uEem5ebMPg-wyFg" name="MVYST1" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gnys7uEem5ebMPg-wyFg" value="MVYST1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gny87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8GnzM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gnzc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gnzs7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gnz87uEem5ebMPg-wyFg" name="MVYTT1" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gn0M7uEem5ebMPg-wyFg" value="MVYTT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gn0c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gn0s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gn087uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gn1M7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gn1c7uEem5ebMPg-wyFg" name="MVYUT1" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gn1s7uEem5ebMPg-wyFg" value="MVYUT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gn187uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gn2M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gn2c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gn2s7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gn287uEem5ebMPg-wyFg" name="MVYVT1" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gn3M7uEem5ebMPg-wyFg" value="MVYVT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gn3c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gn3s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Gn387uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Gn4M7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Gn4c7uEem5ebMPg-wyFg" name="MVYWT1" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Gn4s7uEem5ebMPg-wyFg" value="MVYWT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Gn487uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Gn5M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOcM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOcc7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOcs7uEem5ebMPg-wyFg" name="MVYXT1" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOc87uEem5ebMPg-wyFg" value="MVYXT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOdM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOdc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOds7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOd87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOeM7uEem5ebMPg-wyFg" name="MVY2T1" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOec7uEem5ebMPg-wyFg" value="MVY2T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOes7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOe87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOfM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOfc7uEem5ebMPg-wyFg" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOfs7uEem5ebMPg-wyFg" name="MVY3T1" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOf87uEem5ebMPg-wyFg" value="MVY3T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOgM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOgc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOgs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOg87uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOhM7uEem5ebMPg-wyFg" name="MVY4T1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOhc7uEem5ebMPg-wyFg" value="MVY4T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOhs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOh87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOiM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOic7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOis7uEem5ebMPg-wyFg" name="MVY5T1" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOi87uEem5ebMPg-wyFg" value="MVY5T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOjM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOjc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOjs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOj87uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOkM7uEem5ebMPg-wyFg" name="MVY6T1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOkc7uEem5ebMPg-wyFg" value="MVY6T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOk87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOlM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOlc7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOls7uEem5ebMPg-wyFg" name="MVYYT1" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOl87uEem5ebMPg-wyFg" value="MVYYT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOmM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOmc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOms7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOm87uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOnM7uEem5ebMPg-wyFg" name="MVYZT1" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOnc7uEem5ebMPg-wyFg" value="MVYZT1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOns7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOn87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOoM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOoc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOos7uEem5ebMPg-wyFg" name="MVY0T1" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOo87uEem5ebMPg-wyFg" value="MVY0T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOpM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOpc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOps7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOp87uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOqM7uEem5ebMPg-wyFg" name="MVY1T1" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOqc7uEem5ebMPg-wyFg" value="MVY1T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOqs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOq87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOrM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOrc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOrs7uEem5ebMPg-wyFg" name="MVEHN6" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOr87uEem5ebMPg-wyFg" value="MVEHN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOsM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8HOsc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOss7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOs87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOtM7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOtc7uEem5ebMPg-wyFg" name="MVNPC1" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOts7uEem5ebMPg-wyFg" value="MVNPC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOt87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOuM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOuc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOus7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOu87uEem5ebMPg-wyFg" name="MVNQC1" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOvM7uEem5ebMPg-wyFg" value="MVNQC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOvc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOvs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOv87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOwM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOwc7uEem5ebMPg-wyFg" name="MVHHT2" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOws7uEem5ebMPg-wyFg" value="MVHHT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOw87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOxM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOxc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOxs7uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOx87uEem5ebMPg-wyFg" name="MVHIT2" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOyM7uEem5ebMPg-wyFg" value="MVHIT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOyc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HOys7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HOy87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HOzM7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HOzc7uEem5ebMPg-wyFg" name="MVHGT2" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HOzs7uEem5ebMPg-wyFg" value="MVHGT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HOz87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HO0M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HO0c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HO0s7uEem5ebMPg-wyFg" value="76"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8HO087uEem5ebMPg-wyFg" name="OSOZREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8HO1M7uEem5ebMPg-wyFg" value="OSOZREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8HO1c7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8HO1s7uEem5ebMPg-wyFg" name="OZDBC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HO187uEem5ebMPg-wyFg" value="OZDBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HO2M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HO2c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HO2s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HO287uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HO3M7uEem5ebMPg-wyFg" name="OZDCC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HO3c7uEem5ebMPg-wyFg" value="OZDCC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HO3s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HO387uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HO4M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HO4c7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HO4s7uEem5ebMPg-wyFg" name="OZDDC1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HO487uEem5ebMPg-wyFg" value="OZDDC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HO5M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HO5c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HO5s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HO587uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HO6M7uEem5ebMPg-wyFg" name="OZDEC1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HO6c7uEem5ebMPg-wyFg" value="OZDEC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HO6s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HO687uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HO7M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HO7c7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HO7s7uEem5ebMPg-wyFg" name="OZZKS2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8HO787uEem5ebMPg-wyFg" value="OZZKS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8HO8M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8HO8c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8HO8s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8HO887uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8HO9M7uEem5ebMPg-wyFg" name="OZQ8S2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1gM7uEem5ebMPg-wyFg" value="OZQ8S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1gc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1gs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1g87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1hM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1hc7uEem5ebMPg-wyFg" name="OZQ7S2" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1hs7uEem5ebMPg-wyFg" value="OZQ7S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1h87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1iM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1ic7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1is7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1i87uEem5ebMPg-wyFg" name="OZDZN6" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1jM7uEem5ebMPg-wyFg" value="OZDZN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1jc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H1js7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1j87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1kM7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1kc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1ks7uEem5ebMPg-wyFg" name="OZDAC1" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1k87uEem5ebMPg-wyFg" value="OZDAC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1lM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1lc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1ls7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1l87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1mM7uEem5ebMPg-wyFg" name="OZZLS2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1mc7uEem5ebMPg-wyFg" value="OZZLS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1ms7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1m87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1nM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1nc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1ns7uEem5ebMPg-wyFg" name="OZM3C1" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1n87uEem5ebMPg-wyFg" value="OZM3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1oM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1oc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1os7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1o87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1pM7uEem5ebMPg-wyFg" name="OZQ9S2" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1pc7uEem5ebMPg-wyFg" value="OZQ9S2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1ps7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1p87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1qM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1qc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1qs7uEem5ebMPg-wyFg" name="OZQ4T1" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1q87uEem5ebMPg-wyFg" value="OZQ4T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1rM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1rc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1rs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1r87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1sM7uEem5ebMPg-wyFg" name="OZQ5T1" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1sc7uEem5ebMPg-wyFg" value="OZQ5T1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1ss7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1s87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1tM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1tc7uEem5ebMPg-wyFg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1ts7uEem5ebMPg-wyFg" name="OZGJT2" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1t87uEem5ebMPg-wyFg" value="OZGJT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1uM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1uc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1us7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1u87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1vM7uEem5ebMPg-wyFg" name="OZGKT2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1vc7uEem5ebMPg-wyFg" value="OZGKT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1vs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1v87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1wM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1wc7uEem5ebMPg-wyFg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1ws7uEem5ebMPg-wyFg" name="OZD8VA" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1w87uEem5ebMPg-wyFg" value="OZD8VA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1xM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H1xc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1xs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1x87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1yM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1yc7uEem5ebMPg-wyFg" name="OZD4N6" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1ys7uEem5ebMPg-wyFg" value="OZD4N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1y87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H1zM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1zc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1zs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1z87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H10M7uEem5ebMPg-wyFg" name="OZOWPC" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H10c7uEem5ebMPg-wyFg" value="OZOWPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H10s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H1087uEem5ebMPg-wyFg" value="6"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H11M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H11c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H11s7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1187uEem5ebMPg-wyFg" name="OZBCCD" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H12M7uEem5ebMPg-wyFg" value="OZBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H12c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H12s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1287uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H13M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H13c7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H13s7uEem5ebMPg-wyFg" name="OZD9VA" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1387uEem5ebMPg-wyFg" value="OZD9VA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H14M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H14c7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H14s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1487uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H15M7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H15c7uEem5ebMPg-wyFg" name="OZEAVA" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H15s7uEem5ebMPg-wyFg" value="OZEAVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1587uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H16M7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H16c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H16s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1687uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H17M7uEem5ebMPg-wyFg" name="OZEBVA" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H17c7uEem5ebMPg-wyFg" value="OZEBVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H17s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H1787uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H18M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H18c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H18s7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1887uEem5ebMPg-wyFg" name="OZECVA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H19M7uEem5ebMPg-wyFg" value="OZECVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H19c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H19s7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1987uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1-M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H1-c7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H1-s7uEem5ebMPg-wyFg" name="OZEDVA" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H1-87uEem5ebMPg-wyFg" value="OZEDVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H1_M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H1_c7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H1_s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8H1_87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8H2AM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8H2Ac7uEem5ebMPg-wyFg" name="OZEEVA" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8H2As7uEem5ebMPg-wyFg" value="OZEEVA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8H2A87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8H2BM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8H2Bc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8IckM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ickc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Icks7uEem5ebMPg-wyFg" name="OZD2N6" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ick87uEem5ebMPg-wyFg" value="OZD2N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8IclM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Iclc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Icls7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Icl87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8IcmM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Icmc7uEem5ebMPg-wyFg" name="OZD3N6" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Icms7uEem5ebMPg-wyFg" value="OZD3N6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Icm87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8IcnM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Icnc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Icns7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Icn87uEem5ebMPg-wyFg" value="5"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8IcoM7uEem5ebMPg-wyFg" name="KITCLI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Icoc7uEem5ebMPg-wyFg" value="KITCLI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8Icos7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Ico87uEem5ebMPg-wyFg" name="KCLNOM" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8IcpM7uEem5ebMPg-wyFg" value="KCLNOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Icpc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Icps7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Icp87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8IcqM7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Icqc7uEem5ebMPg-wyFg" name="KCLCLI" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Icqs7uEem5ebMPg-wyFg" value="KCLCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Icq87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8IcrM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Icrc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Icrs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Icr87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8IcsM7uEem5ebMPg-wyFg" name="KCLPRX" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Icsc7uEem5ebMPg-wyFg" value="KCLPRX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Icss7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Ics87uEem5ebMPg-wyFg" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8IctM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ictc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Icts7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ict87uEem5ebMPg-wyFg" name="KCLMOD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8IcuM7uEem5ebMPg-wyFg" value="KCLMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Icuc7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Icus7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Icu87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8IcvM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Icvc7uEem5ebMPg-wyFg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Icvs7uEem5ebMPg-wyFg" name="KITDEF">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Icv87uEem5ebMPg-wyFg" value="KITDEF"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8IcwM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Icwc7uEem5ebMPg-wyFg" name="KITNOM" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Icws7uEem5ebMPg-wyFg" value="KITNOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Icw87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8IcxM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Icxc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Icxs7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Icx87uEem5ebMPg-wyFg" name="KITLIB" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8IcyM7uEem5ebMPg-wyFg" value="KITLIB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Icyc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Icys7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Icy87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8IczM7uEem5ebMPg-wyFg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Iczc7uEem5ebMPg-wyFg" name="KITSUP" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Iczs7uEem5ebMPg-wyFg" value="KITSUP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Icz87uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ic0M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ic0c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ic0s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ic087uEem5ebMPg-wyFg" name="KITMEL" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ic1M7uEem5ebMPg-wyFg" value="KITMEL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ic1c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ic1s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ic187uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ic2M7uEem5ebMPg-wyFg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ic2c7uEem5ebMPg-wyFg" name="KITMOD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ic2s7uEem5ebMPg-wyFg" value="KITMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ic287uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Ic3M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ic3c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ic3s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ic387uEem5ebMPg-wyFg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Ic4M7uEem5ebMPg-wyFg" name="KITART">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Ic4c7uEem5ebMPg-wyFg" value="KITART"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8Ic4s7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Ic487uEem5ebMPg-wyFg" name="KARNOM" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ic5M7uEem5ebMPg-wyFg" value="KARNOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ic5c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ic5s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ic587uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ic6M7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ic6c7uEem5ebMPg-wyFg" name="KARART" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ic6s7uEem5ebMPg-wyFg" value="KARART"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ic687uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Ic7M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ic7c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ic7s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ic787uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ic8M7uEem5ebMPg-wyFg" name="KARQTE" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ic8c7uEem5ebMPg-wyFg" value="KARQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ic8s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Ic887uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ic9M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ic9c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ic9s7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ic987uEem5ebMPg-wyFg" name="KARMOD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ic-M7uEem5ebMPg-wyFg" value="KARMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ic-c7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Ic-s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ic-87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ic_M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ic_c7uEem5ebMPg-wyFg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Ic_s7uEem5ebMPg-wyFg" name="TRIGGER_PXMAMAVP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Ic_87uEem5ebMPg-wyFg" value="TRIGGER_PXMAMAVP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8IdAM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8IdAc7uEem5ebMPg-wyFg" name="TMV_CODE_MERCURIALE_" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8IdAs7uEem5ebMPg-wyFg" value="TMV_CODE_MERCURIALE_"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8IdA87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8IdBM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8IdBc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8IdBs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8IdB87uEem5ebMPg-wyFg" name="TMV_CODE_ARTICLE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8IdCM7uEem5ebMPg-wyFg" value="TMV_CODE_ARTICLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8IdCc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8IdCs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8IdC87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8IdDM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JDoM7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JDoc7uEem5ebMPg-wyFg" name="TMV_PRIX" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JDos7uEem5ebMPg-wyFg" value="TMV_PRIX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JDo87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8JDpM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JDpc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JDps7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JDp87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JDqM7uEem5ebMPg-wyFg" name="TMV_TYPE_MODIFICATION_AMS" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JDqc7uEem5ebMPg-wyFg" value="TMV_TYPE_MODIFICATION_AMS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JDqs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JDq87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JDrM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JDrc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JDrs7uEem5ebMPg-wyFg" name="TMV_TOP_TRAITE_O_N" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JDr87uEem5ebMPg-wyFg" value="TMV_TOP_TRAITE_O_N"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JDsM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JDsc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JDss7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JDs87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JDtM7uEem5ebMPg-wyFg" name="TMV_DATE_HEURE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JDtc7uEem5ebMPg-wyFg" value="TMV_DATE_HEURE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JDts7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8JDt87uEem5ebMPg-wyFg" value="6"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JDuM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JDuc7uEem5ebMPg-wyFg" value="TIMESTAMP"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JDus7uEem5ebMPg-wyFg" value="26"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8JDu87uEem5ebMPg-wyFg" name="WDFAVCLP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8JDvM7uEem5ebMPg-wyFg" value="WDFAVCLP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8JDvc7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8JDvs7uEem5ebMPg-wyFg" name="FACREP" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JDv87uEem5ebMPg-wyFg" value="FACREP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JDwM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JDwc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JDws7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JDw87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JDxM7uEem5ebMPg-wyFg" name="FACSOC" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JDxc7uEem5ebMPg-wyFg" value="FACSOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JDxs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JDx87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JDyM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JDyc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JDys7uEem5ebMPg-wyFg" name="FACMVT" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JDy87uEem5ebMPg-wyFg" value="FACMVT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JDzM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JDzc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JDzs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JDz87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JD0M7uEem5ebMPg-wyFg" name="FACREG" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JD0c7uEem5ebMPg-wyFg" value="FACREG"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JD0s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JD087uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JD1M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JD1c7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JD1s7uEem5ebMPg-wyFg" name="FACART" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JD187uEem5ebMPg-wyFg" value="FACART"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JD2M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JD2c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JD2s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JD287uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JD3M7uEem5ebMPg-wyFg" name="FALART" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JD3c7uEem5ebMPg-wyFg" value="FALART"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JD3s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JD387uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JD4M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JD4c7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JD4s7uEem5ebMPg-wyFg" name="FAQTEE" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JD487uEem5ebMPg-wyFg" value="FAQTEE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JD5M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JD5c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JD5s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JD587uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JD6M7uEem5ebMPg-wyFg" name="FAPUHT" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JD6c7uEem5ebMPg-wyFg" value="FAPUHT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JD6s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JD687uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JD7M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JD7c7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JD7s7uEem5ebMPg-wyFg" name="FATREM" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JD787uEem5ebMPg-wyFg" value="FATREM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JD8M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JD8c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JD8s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JD887uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JD9M7uEem5ebMPg-wyFg" name="FAREMI" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JD9c7uEem5ebMPg-wyFg" value="FAREMI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JD9s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JD987uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JD-M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JD-c7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JD-s7uEem5ebMPg-wyFg" name="FATPXN" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JD-87uEem5ebMPg-wyFg" value="FATPXN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JD_M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JD_c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JD_s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JD_87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JEAM7uEem5ebMPg-wyFg" name="FAPOSI" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JEAc7uEem5ebMPg-wyFg" value="FAPOSI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JEAs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JEA87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JEBM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JEBc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JEBs7uEem5ebMPg-wyFg" name="FATGRA" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JEB87uEem5ebMPg-wyFg" value="FATGRA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JECM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JECc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JECs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JEC87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JEDM7uEem5ebMPg-wyFg" name="FADERC" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JEDc7uEem5ebMPg-wyFg" value="FADERC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JEDs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JED87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JEEM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JEEc7uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JEEs7uEem5ebMPg-wyFg" name="FAFREQ" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JEE87uEem5ebMPg-wyFg" value="FAFREQ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JEFM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JEFc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JEFs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JEF87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JEGM7uEem5ebMPg-wyFg" name="FAQTEC" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JEGc7uEem5ebMPg-wyFg" value="FAQTEC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JEGs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JEG87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JEHM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JEHc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JEHs7uEem5ebMPg-wyFg" name="FALIGP" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JEH87uEem5ebMPg-wyFg" value="FALIGP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JEIM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8JqsM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jqsc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jqss7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jqs87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JqtM7uEem5ebMPg-wyFg" name="FACOPE" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jqtc7uEem5ebMPg-wyFg" value="FACOPE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jqts7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jqt87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JquM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jquc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Jqus7uEem5ebMPg-wyFg" name="FADATE" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jqu87uEem5ebMPg-wyFg" value="FADATE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JqvM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jqvc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jqvs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jqv87uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JqwM7uEem5ebMPg-wyFg" name="FAHEUR" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jqwc7uEem5ebMPg-wyFg" value="FAHEUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jqws7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jqw87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JqxM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jqxc7uEem5ebMPg-wyFg" value="6"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Jqxs7uEem5ebMPg-wyFg" name="ORBQREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Jqx87uEem5ebMPg-wyFg" value="ORBQREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8JqyM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Jqyc7uEem5ebMPg-wyFg" name="BQBOCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jqys7uEem5ebMPg-wyFg" value="BQBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jqy87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JqzM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jqzc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jqzs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Jqz87uEem5ebMPg-wyFg" name="BQHRST" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jq0M7uEem5ebMPg-wyFg" value="BQHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jq0c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jq0s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jq087uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jq1M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Jq1c7uEem5ebMPg-wyFg" name="BQAFCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jq1s7uEem5ebMPg-wyFg" value="BQAFCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jq187uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jq2M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jq2c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jq2s7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Jq287uEem5ebMPg-wyFg" name="BQAMCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jq3M7uEem5ebMPg-wyFg" value="BQAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jq3c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jq3s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jq387uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jq4M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Jq4c7uEem5ebMPg-wyFg" name="BQGHCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jq4s7uEem5ebMPg-wyFg" value="BQGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jq487uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jq5M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jq5c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jq5s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Jq587uEem5ebMPg-wyFg" name="BQJ4CD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jq6M7uEem5ebMPg-wyFg" value="BQJ4CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jq6c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jq6s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jq687uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jq7M7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Jq7c7uEem5ebMPg-wyFg" name="BQBYNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jq7s7uEem5ebMPg-wyFg" value="BQBYNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jq787uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jq8M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jq8c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jq8s7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Jq887uEem5ebMPg-wyFg" name="BQAZDT" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jq9M7uEem5ebMPg-wyFg" value="BQAZDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jq9c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Jq9s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jq987uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jq-M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Jq-c7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Jq-s7uEem5ebMPg-wyFg" name="BQA0DT" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Jq-87uEem5ebMPg-wyFg" value="BQA0DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Jq_M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Jq_c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Jq_s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Jq_87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JrAM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JrAc7uEem5ebMPg-wyFg" name="BQBQST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JrAs7uEem5ebMPg-wyFg" value="BQBQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JrA87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JrBM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JrBc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JrBs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JrB87uEem5ebMPg-wyFg" name="BQBRST" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8JrCM7uEem5ebMPg-wyFg" value="BQBRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8JrCc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8JrCs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8JrC87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8JrDM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8JrDc7uEem5ebMPg-wyFg" name="BQAEQT" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KRwM7uEem5ebMPg-wyFg" value="BQAEQT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KRwc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8KRws7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KRw87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KRxM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KRxc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KRxs7uEem5ebMPg-wyFg" name="BQDQDT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KRx87uEem5ebMPg-wyFg" value="BQDQDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KRyM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8KRyc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KRys7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KRy87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KRzM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KRzc7uEem5ebMPg-wyFg" name="BQDRDT" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KRzs7uEem5ebMPg-wyFg" value="BQDRDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KRz87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8KR0M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KR0c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KR0s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KR087uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KR1M7uEem5ebMPg-wyFg" name="BQO1ST" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KR1c7uEem5ebMPg-wyFg" value="BQO1ST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KR1s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KR187uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KR2M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KR2c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KR2s7uEem5ebMPg-wyFg" name="BQD3C1" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KR287uEem5ebMPg-wyFg" value="BQD3C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KR3M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KR3c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KR3s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KR387uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KR4M7uEem5ebMPg-wyFg" name="BQBCCD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KR4c7uEem5ebMPg-wyFg" value="BQBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KR4s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8KR487uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KR5M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KR5c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KR5s7uEem5ebMPg-wyFg" value="3"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8KR587uEem5ebMPg-wyFg" name="CLIENTCD">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8KR6M7uEem5ebMPg-wyFg" value="CLIENTCD"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8KR6c7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8KR6s7uEem5ebMPg-wyFg" name="IDCWEB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KR687uEem5ebMPg-wyFg" value="IDCWEB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KR7M7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8KR7c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KR7s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KR787uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KR8M7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KR8c7uEem5ebMPg-wyFg" name="MFERME" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KR8s7uEem5ebMPg-wyFg" value="MFERME"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KR887uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KR9M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KR9c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KR9s7uEem5ebMPg-wyFg" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KR987uEem5ebMPg-wyFg" name="PRENOM" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KR-M7uEem5ebMPg-wyFg" value="PRENOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KR-c7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KR-s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KR-87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KR_M7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KR_c7uEem5ebMPg-wyFg" name="ANNIV" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KR_s7uEem5ebMPg-wyFg" value="ANNIV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KR_87uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8KSAM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KSAc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KSAs7uEem5ebMPg-wyFg" value="DATE"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KSA87uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KSBM7uEem5ebMPg-wyFg" name="NEWSCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KSBc7uEem5ebMPg-wyFg" value="NEWSCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KSBs7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8KSB87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KSCM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KSCc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KSCs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KSC87uEem5ebMPg-wyFg" name="NEWSPA" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KSDM7uEem5ebMPg-wyFg" value="NEWSPA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KSDc7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8KSDs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8KSD87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8KSEM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8KSEc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8KSEs7uEem5ebMPg-wyFg" name="NSIRET" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8KSE87uEem5ebMPg-wyFg" value="NSIRET"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8KSFM7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K40M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K40c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K40s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K4087uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K41M7uEem5ebMPg-wyFg" name="BGROUP" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K41c7uEem5ebMPg-wyFg" value="BGROUP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K41s7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K4187uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K42M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K42c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K42s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K4287uEem5ebMPg-wyFg" name="TYPGRP" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K43M7uEem5ebMPg-wyFg" value="TYPGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K43c7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K43s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K4387uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K44M7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K44c7uEem5ebMPg-wyFg" name="IDMARC" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K44s7uEem5ebMPg-wyFg" value="IDMARC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K4487uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K45M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K45c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K45s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K4587uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K46M7uEem5ebMPg-wyFg" name="IDACTI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K46c7uEem5ebMPg-wyFg" value="IDACTI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K46s7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K4687uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K47M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K47c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K47s7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K4787uEem5ebMPg-wyFg" name="IDDETA" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K48M7uEem5ebMPg-wyFg" value="IDDETA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K48c7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K48s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K4887uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K49M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K49c7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K49s7uEem5ebMPg-wyFg" name="TOPMOD" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K4987uEem5ebMPg-wyFg" value="TOPMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K4-M7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K4-c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K4-s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K4-87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K4_M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K4_c7uEem5ebMPg-wyFg" name="NUMCLI" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K4_s7uEem5ebMPg-wyFg" value="NUMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K4_87uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K5AM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5Ac7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5As7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5A87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K5BM7uEem5ebMPg-wyFg" name="TOPCWB" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5Bc7uEem5ebMPg-wyFg" value="TOPCWB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5Bs7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K5B87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5CM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5Cc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5Cs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K5C87uEem5ebMPg-wyFg" name="TOPMWB" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5DM7uEem5ebMPg-wyFg" value="TOPMWB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5Dc7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K5Ds7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5D87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5EM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5Ec7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K5Es7uEem5ebMPg-wyFg" name="TOPVWB" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5E87uEem5ebMPg-wyFg" value="TOPVWB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5FM7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5Fc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5Fs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5F87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K5GM7uEem5ebMPg-wyFg" name="TOPCDI" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5Gc7uEem5ebMPg-wyFg" value="TOPCDI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5Gs7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5G87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5HM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5Hc7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8K5Hs7uEem5ebMPg-wyFg" name="WEBCTRL">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8K5H87uEem5ebMPg-wyFg" value="WEBCTRL"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8K5IM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8K5Ic7uEem5ebMPg-wyFg" name="CLIPAY" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5Is7uEem5ebMPg-wyFg" value="CLIPAY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5I87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K5JM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5Jc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5Js7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5J87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K5KM7uEem5ebMPg-wyFg" name="CLICEN" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5Kc7uEem5ebMPg-wyFg" value="CLICEN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5Ks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K5K87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5LM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5Lc7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5Ls7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K5L87uEem5ebMPg-wyFg" name="CDEMAX" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5MM7uEem5ebMPg-wyFg" value="CDEMAX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5Mc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K5Ms7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5M87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5NM7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5Nc7uEem5ebMPg-wyFg" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8K5Ns7uEem5ebMPg-wyFg" name="ELICLIP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8K5N87uEem5ebMPg-wyFg" value="ELICLIP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8K5OM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8K5Oc7uEem5ebMPg-wyFg" name="IDWEBC" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5Os7uEem5ebMPg-wyFg" value="IDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5O87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5PM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5Pc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5Ps7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K5P87uEem5ebMPg-wyFg" name="CPTCLI" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5QM7uEem5ebMPg-wyFg" value="CPTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5Qc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8K5Qs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5Q87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5RM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5Rc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K5Rs7uEem5ebMPg-wyFg" name="NOMCLI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5R87uEem5ebMPg-wyFg" value="NOMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8K5SM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8K5Sc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8K5Ss7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8K5S87uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8K5TM7uEem5ebMPg-wyFg" name="PRECLI" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8K5Tc7uEem5ebMPg-wyFg" value="PRECLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Lf4M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Lf4c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Lf4s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Lf487uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Lf5M7uEem5ebMPg-wyFg" name="PWDCLI" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Lf5c7uEem5ebMPg-wyFg" value="PWDCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Lf5s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Lf587uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Lf6M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Lf6c7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Lf6s7uEem5ebMPg-wyFg" name="MELADR" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Lf687uEem5ebMPg-wyFg" value="MELADR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Lf7M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Lf7c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Lf7s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Lf787uEem5ebMPg-wyFg" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Lf8M7uEem5ebMPg-wyFg" name="MELSIT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Lf8c7uEem5ebMPg-wyFg" value="MELSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Lf8s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Lf887uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Lf9M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Lf9c7uEem5ebMPg-wyFg" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Lf9s7uEem5ebMPg-wyFg" name="NIVCLI" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Lf987uEem5ebMPg-wyFg" value="NIVCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Lf-M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Lf-c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Lf-s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Lf-87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Lf_M7uEem5ebMPg-wyFg" name="ACTCLI" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Lf_c7uEem5ebMPg-wyFg" value="ACTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Lf_s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Lf_87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgAM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgAc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgAs7uEem5ebMPg-wyFg" name="TYPTAR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgA87uEem5ebMPg-wyFg" value="TYPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgBM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgBc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgBs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgB87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgCM7uEem5ebMPg-wyFg" name="TXREMI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgCc7uEem5ebMPg-wyFg" value="TXREMI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgCs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8LgC87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgDM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgDc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgDs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgD87uEem5ebMPg-wyFg" name="FRANCO" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgEM7uEem5ebMPg-wyFg" value="FRANCO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgEc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8LgEs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgE87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgFM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgFc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgFs7uEem5ebMPg-wyFg" name="FRPORT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgF87uEem5ebMPg-wyFg" value="FRPORT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgGM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8LgGc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgGs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgG87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgHM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgHc7uEem5ebMPg-wyFg" name="MINCDE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgHs7uEem5ebMPg-wyFg" value="MINCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgH87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8LgIM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgIc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgIs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgI87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgJM7uEem5ebMPg-wyFg" name="MERCUR" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgJc7uEem5ebMPg-wyFg" value="MERCUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgJs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgJ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgKM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgKc7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgKs7uEem5ebMPg-wyFg" name="CDECAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgK87uEem5ebMPg-wyFg" value="CDECAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgLM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgLc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgLs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgL87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgMM7uEem5ebMPg-wyFg" name="CDELST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgMc7uEem5ebMPg-wyFg" value="CDELST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgMs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgM87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgNM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgNc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgNs7uEem5ebMPg-wyFg" name="CDEMER" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgN87uEem5ebMPg-wyFg" value="CDEMER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgOM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgOc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgOs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgO87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgPM7uEem5ebMPg-wyFg" name="CDEFAV" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgPc7uEem5ebMPg-wyFg" value="CDEFAV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgPs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgP87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgQM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgQc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgQs7uEem5ebMPg-wyFg" name="TARCLI" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgQ87uEem5ebMPg-wyFg" value="TARCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgRM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgRc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgRs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgR87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgSM7uEem5ebMPg-wyFg" name="UNIVCL" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgSc7uEem5ebMPg-wyFg" value="UNIVCL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgSs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgS87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgTM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgTc7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgTs7uEem5ebMPg-wyFg" name="REMISE" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgT87uEem5ebMPg-wyFg" value="REMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgUM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8LgUc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgUs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgU87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgVM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgVc7uEem5ebMPg-wyFg" name="IMPTAR" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgVs7uEem5ebMPg-wyFg" value="IMPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgV87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgWM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgWc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgWs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgW87uEem5ebMPg-wyFg" name="CENTRA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgXM7uEem5ebMPg-wyFg" value="CENTRA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8LgXc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8LgXs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8LgX87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8LgYM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8LgYc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8LgYs7uEem5ebMPg-wyFg" name="MAILVRP" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8LgY87uEem5ebMPg-wyFg" value="MAILVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MG8M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MG8c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MG8s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MG887uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MG9M7uEem5ebMPg-wyFg" name="CLIMAIL" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MG9c7uEem5ebMPg-wyFg" value="CLIMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MG9s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MG987uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MG-M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MG-c7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MG-s7uEem5ebMPg-wyFg" name="SPECIAL" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MG-87uEem5ebMPg-wyFg" value="SPECIAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MG_M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MG_c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MG_s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MG_87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHAM7uEem5ebMPg-wyFg" name="CODSOC" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHAc7uEem5ebMPg-wyFg" value="CODSOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHAs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHA87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHBM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHBc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHBs7uEem5ebMPg-wyFg" name="TELVRP" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHB87uEem5ebMPg-wyFg" value="TELVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHCM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHCc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHCs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHC87uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHDM7uEem5ebMPg-wyFg" name="NOMVRP" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHDc7uEem5ebMPg-wyFg" value="NOMVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHDs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHD87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHEM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHEc7uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHEs7uEem5ebMPg-wyFg" name="LNGCLI" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHE87uEem5ebMPg-wyFg" value="LNGCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHFM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHFc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHFs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHF87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHGM7uEem5ebMPg-wyFg" name="PWDMD5" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHGc7uEem5ebMPg-wyFg" value="PWDMD5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHGs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHG87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHHM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHHc7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHHs7uEem5ebMPg-wyFg" name="NOMCOM" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHH87uEem5ebMPg-wyFg" value="NOMCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHIM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHIc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHIs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHI87uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHJM7uEem5ebMPg-wyFg" name="MELCOM" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHJc7uEem5ebMPg-wyFg" value="MELCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHJs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHJ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHKM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHKc7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHKs7uEem5ebMPg-wyFg" name="TOPCRE" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHK87uEem5ebMPg-wyFg" value="TOPCRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHLM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHLc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHLs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHL87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHMM7uEem5ebMPg-wyFg" name="TOPPROMO" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHMc7uEem5ebMPg-wyFg" value="TOPPROMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHMs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MHM87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHNM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHNc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHNs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHN87uEem5ebMPg-wyFg" name="TOPMERCURIALE" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHOM7uEem5ebMPg-wyFg" value="TOPMERCURIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHOc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MHOs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHO87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHPM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHPc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHPs7uEem5ebMPg-wyFg" name="TOPKIT" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHP87uEem5ebMPg-wyFg" value="TOPKIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHQM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MHQc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHQs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHQ87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHRM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHRc7uEem5ebMPg-wyFg" name="TOPPRINCIPAL" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHRs7uEem5ebMPg-wyFg" value="TOPPRINCIPAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHR87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MHSM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHSc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHSs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHS87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHTM7uEem5ebMPg-wyFg" name="TOPSUPERVISION" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHTc7uEem5ebMPg-wyFg" value="TOPSUPERVISION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHTs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MHT87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHUM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHUc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHUs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHU87uEem5ebMPg-wyFg" name="MODRGLMT" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHVM7uEem5ebMPg-wyFg" value="MODRGLMT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHVc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MHVs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHV87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHWM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHWc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHWs7uEem5ebMPg-wyFg" name="TOPFA" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHW87uEem5ebMPg-wyFg" value="TOPFA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHXM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MHXc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHXs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHX87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHYM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHYc7uEem5ebMPg-wyFg" name="TOPLE" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHYs7uEem5ebMPg-wyFg" value="TOPLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHY87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MHZM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHZc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHZs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHZ87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHaM7uEem5ebMPg-wyFg" name="FACTURATION" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHac7uEem5ebMPg-wyFg" value="FACTURATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHas7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHa87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHbM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MHbc7uEem5ebMPg-wyFg" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MHbs7uEem5ebMPg-wyFg" name="ADRESSE" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MHb87uEem5ebMPg-wyFg" value="ADRESSE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MHcM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MHcc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MHcs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuAM7uEem5ebMPg-wyFg" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuAc7uEem5ebMPg-wyFg" name="TXREMISE" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuAs7uEem5ebMPg-wyFg" value="TXREMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuA87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuBM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuBc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuBs7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuB87uEem5ebMPg-wyFg" name="TOPCHR" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuCM7uEem5ebMPg-wyFg" value="TOPCHR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuCc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MuCs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuC87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuDM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuDc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuDs7uEem5ebMPg-wyFg" name="MAXCDE" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuD87uEem5ebMPg-wyFg" value="MAXCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuEM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MuEc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuEs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuE87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuFM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuFc7uEem5ebMPg-wyFg" name="MERCUGRP" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuFs7uEem5ebMPg-wyFg" value="MERCUGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuF87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuGM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuGc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuGs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuG87uEem5ebMPg-wyFg" name="CDEMERC" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuHM7uEem5ebMPg-wyFg" value="CDEMERC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuHc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuHs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuH87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuIM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuIc7uEem5ebMPg-wyFg" name="TOPEFAC" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuIs7uEem5ebMPg-wyFg" value="TOPEFAC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuI87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MuJM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuJc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuJs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuJ87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuKM7uEem5ebMPg-wyFg" name="AUTOLOGKEY" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuKc7uEem5ebMPg-wyFg" value="AUTOLOGKEY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuKs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuK87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuLM7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuLc7uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuLs7uEem5ebMPg-wyFg" name="USRCDF" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuL87uEem5ebMPg-wyFg" value="USRCDF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuMM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuMc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuMs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuM87uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuNM7uEem5ebMPg-wyFg" name="USRPRF" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuNc7uEem5ebMPg-wyFg" value="USRPRF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuNs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MuN87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuOM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuOc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuOs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuO87uEem5ebMPg-wyFg" name="USRSIT" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuPM7uEem5ebMPg-wyFg" value="USRSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuPc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuPs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuP87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuQM7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuQc7uEem5ebMPg-wyFg" name="USRDIV" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuQs7uEem5ebMPg-wyFg" value="USRDIV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuQ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MuRM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuRc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuRs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuR87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuSM7uEem5ebMPg-wyFg" name="USRHIER" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuSc7uEem5ebMPg-wyFg" value="USRHIER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuSs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuS87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuTM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuTc7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuTs7uEem5ebMPg-wyFg" name="TOPCST" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuT87uEem5ebMPg-wyFg" value="TOPCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuUM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuUc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuUs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuU87uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8MuVM7uEem5ebMPg-wyFg" name="WSOCLIP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8MuVc7uEem5ebMPg-wyFg" value="WSOCLIP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8MuVs7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8MuV87uEem5ebMPg-wyFg" name="IDWEBC" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuWM7uEem5ebMPg-wyFg" value="IDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuWc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuWs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuW87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuXM7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuXc7uEem5ebMPg-wyFg" name="CPTCLI" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuXs7uEem5ebMPg-wyFg" value="CPTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuX87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8MuYM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuYc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuYs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8MuY87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MuZM7uEem5ebMPg-wyFg" name="NOMCLI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8MuZc7uEem5ebMPg-wyFg" value="NOMCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MuZs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8MuZ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8MuaM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Muac7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Muas7uEem5ebMPg-wyFg" name="PRECLI" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Mua87uEem5ebMPg-wyFg" value="PRECLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8MubM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Mubc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Mubs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Mub87uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8MucM7uEem5ebMPg-wyFg" name="PWDCLI" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Mucc7uEem5ebMPg-wyFg" value="PWDCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVEM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVEc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVEs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVE87uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVFM7uEem5ebMPg-wyFg" name="MELADR" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVFc7uEem5ebMPg-wyFg" value="MELADR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVFs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVF87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVGM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVGc7uEem5ebMPg-wyFg" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVGs7uEem5ebMPg-wyFg" name="MELSIT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVG87uEem5ebMPg-wyFg" value="MELSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVHM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVHc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVHs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVH87uEem5ebMPg-wyFg" value="200"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVIM7uEem5ebMPg-wyFg" name="NIVCLI" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVIc7uEem5ebMPg-wyFg" value="NIVCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVIs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVI87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVJM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVJc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVJs7uEem5ebMPg-wyFg" name="ACTCLI" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVJ87uEem5ebMPg-wyFg" value="ACTCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVKM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVKc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVKs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVK87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVLM7uEem5ebMPg-wyFg" name="TYPTAR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVLc7uEem5ebMPg-wyFg" value="TYPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVLs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVL87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVMM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVMc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVMs7uEem5ebMPg-wyFg" name="TXREMI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVM87uEem5ebMPg-wyFg" value="TXREMI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVNM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8NVNc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVNs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVN87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVOM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVOc7uEem5ebMPg-wyFg" name="FRANCO" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVOs7uEem5ebMPg-wyFg" value="FRANCO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVO87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8NVPM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVPc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVPs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVP87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVQM7uEem5ebMPg-wyFg" name="FRPORT" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVQc7uEem5ebMPg-wyFg" value="FRPORT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVQs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8NVQ87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVRM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVRc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVRs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVR87uEem5ebMPg-wyFg" name="MINCDE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVSM7uEem5ebMPg-wyFg" value="MINCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVSc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8NVSs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVS87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVTM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVTc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVTs7uEem5ebMPg-wyFg" name="MERCUR" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVT87uEem5ebMPg-wyFg" value="MERCUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVUM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVUc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVUs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVU87uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVVM7uEem5ebMPg-wyFg" name="CDECAT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVVc7uEem5ebMPg-wyFg" value="CDECAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVVs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVV87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVWM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVWc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVWs7uEem5ebMPg-wyFg" name="CDELST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVW87uEem5ebMPg-wyFg" value="CDELST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVXM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVXc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVXs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVX87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVYM7uEem5ebMPg-wyFg" name="CDEMER" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVYc7uEem5ebMPg-wyFg" value="CDEMER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVYs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVY87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVZM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVZc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVZs7uEem5ebMPg-wyFg" name="CDEFAV" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVZ87uEem5ebMPg-wyFg" value="CDEFAV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVaM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVac7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVas7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVa87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVbM7uEem5ebMPg-wyFg" name="TARCLI" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVbc7uEem5ebMPg-wyFg" value="TARCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVbs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVb87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVcM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVcc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVcs7uEem5ebMPg-wyFg" name="UNIVCL" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVc87uEem5ebMPg-wyFg" value="UNIVCL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVdM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVdc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVds7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVd87uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVeM7uEem5ebMPg-wyFg" name="REMISE" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVec7uEem5ebMPg-wyFg" value="REMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVes7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8NVe87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVfM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVfc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVfs7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVf87uEem5ebMPg-wyFg" name="IMPTAR" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVgM7uEem5ebMPg-wyFg" value="IMPTAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVgc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVgs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVg87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVhM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVhc7uEem5ebMPg-wyFg" name="CENTRA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVhs7uEem5ebMPg-wyFg" value="CENTRA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVh87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8NViM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVic7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVis7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVi87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVjM7uEem5ebMPg-wyFg" name="MAILVRP" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVjc7uEem5ebMPg-wyFg" value="MAILVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8NVjs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8NVj87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8NVkM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8NVkc7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8NVks7uEem5ebMPg-wyFg" name="CLIMAIL" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8NVk87uEem5ebMPg-wyFg" value="CLIMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8IM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8Ic7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8Is7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8I87uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8JM7uEem5ebMPg-wyFg" name="SPECIAL" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8Jc7uEem5ebMPg-wyFg" value="SPECIAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8Js7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8J87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8KM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8Kc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8Ks7uEem5ebMPg-wyFg" name="CODSOC" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8K87uEem5ebMPg-wyFg" value="CODSOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8LM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8Lc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8Ls7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8L87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8MM7uEem5ebMPg-wyFg" name="TELVRP" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8Mc7uEem5ebMPg-wyFg" value="TELVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8Ms7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8M87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8NM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8Nc7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8Ns7uEem5ebMPg-wyFg" name="NOMVRP" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8N87uEem5ebMPg-wyFg" value="NOMVRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8OM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8Oc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8Os7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8O87uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8PM7uEem5ebMPg-wyFg" name="LNGCLI" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8Pc7uEem5ebMPg-wyFg" value="LNGCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8Ps7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8P87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8QM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8Qc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8Qs7uEem5ebMPg-wyFg" name="PWDMD5" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8Q87uEem5ebMPg-wyFg" value="PWDMD5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8RM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8Rc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8Rs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8R87uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8SM7uEem5ebMPg-wyFg" name="NOMCOM" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8Sc7uEem5ebMPg-wyFg" value="NOMCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8Ss7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8S87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8TM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8Tc7uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8Ts7uEem5ebMPg-wyFg" name="MELCOM" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8T87uEem5ebMPg-wyFg" value="MELCOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8UM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8Uc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8Us7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8U87uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8VM7uEem5ebMPg-wyFg" name="TOPCRE" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8Vc7uEem5ebMPg-wyFg" value="TOPCRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8Vs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8V87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8WM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8Wc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8Ws7uEem5ebMPg-wyFg" name="TOPPROMO" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8W87uEem5ebMPg-wyFg" value="TOPPROMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8XM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8N8Xc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8Xs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8X87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8YM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8Yc7uEem5ebMPg-wyFg" name="TOPMERCURIALE" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8Ys7uEem5ebMPg-wyFg" value="TOPMERCURIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8Y87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8N8ZM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8Zc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8Zs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8Z87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8aM7uEem5ebMPg-wyFg" name="TOPKIT" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8ac7uEem5ebMPg-wyFg" value="TOPKIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8as7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8N8a87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8bM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8bc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8bs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8b87uEem5ebMPg-wyFg" name="TOPPRINCIPAL" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8cM7uEem5ebMPg-wyFg" value="TOPPRINCIPAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8cc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8N8cs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8c87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8dM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8dc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8ds7uEem5ebMPg-wyFg" name="TOPSUPERVISION" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8d87uEem5ebMPg-wyFg" value="TOPSUPERVISION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8eM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8N8ec7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8es7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8e87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8fM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8fc7uEem5ebMPg-wyFg" name="MODRGLMT" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8fs7uEem5ebMPg-wyFg" value="MODRGLMT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8f87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8N8gM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8gc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8gs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8g87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8hM7uEem5ebMPg-wyFg" name="TOPFA" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8hc7uEem5ebMPg-wyFg" value="TOPFA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8hs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8N8h87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8iM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8ic7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8is7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8i87uEem5ebMPg-wyFg" name="TOPLE" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8jM7uEem5ebMPg-wyFg" value="TOPLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8jc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8N8js7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8j87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8kM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8kc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8ks7uEem5ebMPg-wyFg" name="FACTURATION" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8k87uEem5ebMPg-wyFg" value="FACTURATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8lM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8lc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8ls7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8l87uEem5ebMPg-wyFg" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8mM7uEem5ebMPg-wyFg" name="ADRESSE" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8mc7uEem5ebMPg-wyFg" value="ADRESSE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8ms7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8m87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8nM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8nc7uEem5ebMPg-wyFg" value="300"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8ns7uEem5ebMPg-wyFg" name="TXREMISE" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8n87uEem5ebMPg-wyFg" value="TXREMISE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8oM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8N8oc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8N8os7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8N8o87uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8N8pM7uEem5ebMPg-wyFg" name="TOPCHR" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8N8pc7uEem5ebMPg-wyFg" value="TOPCHR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8N8ps7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8OjMM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8OjMc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjMs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8OjM87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjNM7uEem5ebMPg-wyFg" name="MAXCDE" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8OjNc7uEem5ebMPg-wyFg" value="MAXCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjNs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8OjN87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8OjOM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjOc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8OjOs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjO87uEem5ebMPg-wyFg" name="MERCUGRP" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8OjPM7uEem5ebMPg-wyFg" value="MERCUGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjPc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8OjPs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjP87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8OjQM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjQc7uEem5ebMPg-wyFg" name="CDEMERC" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8OjQs7uEem5ebMPg-wyFg" value="CDEMERC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjQ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8OjRM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjRc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8OjRs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjR87uEem5ebMPg-wyFg" name="TOPEFAC" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8OjSM7uEem5ebMPg-wyFg" value="TOPEFAC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjSc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8OjSs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8OjS87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjTM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8OjTc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjTs7uEem5ebMPg-wyFg" name="AUTOLOGKEY" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8OjT87uEem5ebMPg-wyFg" value="AUTOLOGKEY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjUM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8OjUc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjUs7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8OjU87uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjVM7uEem5ebMPg-wyFg" name="USRCDF" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8OjVc7uEem5ebMPg-wyFg" value="USRCDF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjVs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8OjV87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjWM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8OjWc7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjWs7uEem5ebMPg-wyFg" name="USRPRF" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8OjW87uEem5ebMPg-wyFg" value="USRPRF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjXM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8OjXc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8OjXs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjX87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8OjYM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjYc7uEem5ebMPg-wyFg" name="USRSIT" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8OjYs7uEem5ebMPg-wyFg" value="USRSIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjY87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8OjZM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjZc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8OjZs7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjZ87uEem5ebMPg-wyFg" name="USRDIV" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8OjaM7uEem5ebMPg-wyFg" value="USRDIV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ojac7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Ojas7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Oja87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjbM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojbc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ojbs7uEem5ebMPg-wyFg" name="USRHIER" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojb87uEem5ebMPg-wyFg" value="USRHIER"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjcM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojcc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ojcs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojc87uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjdM7uEem5ebMPg-wyFg" name="TOPCST" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojdc7uEem5ebMPg-wyFg" value="TOPCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ojds7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojd87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjeM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojec7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Ojes7uEem5ebMPg-wyFg" name="OTEFCPP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Oje87uEem5ebMPg-wyFg" value="OTEFCPP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8OjfM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Ojfc7uEem5ebMPg-wyFg" name="EFAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojfs7uEem5ebMPg-wyFg" value="EFAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ojf87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8OjgM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojgc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ojgs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojg87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjhM7uEem5ebMPg-wyFg" name="EFTAC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojhc7uEem5ebMPg-wyFg" value="EFTAC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ojhs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojh87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjiM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojic7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ojis7uEem5ebMPg-wyFg" name="EFNBN3" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Oji87uEem5ebMPg-wyFg" value="EFNBN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjjM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojjc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ojjs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojj87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjkM7uEem5ebMPg-wyFg" name="EFPMT2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojkc7uEem5ebMPg-wyFg" value="EFPMT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ojks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojk87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjlM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojlc7uEem5ebMPg-wyFg" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ojls7uEem5ebMPg-wyFg" name="EFAWNA" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojl87uEem5ebMPg-wyFg" value="EFAWNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjmM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojmc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ojms7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojm87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjnM7uEem5ebMPg-wyFg" name="EFNDCD" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojnc7uEem5ebMPg-wyFg" value="EFNDCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ojns7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojn87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjoM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojoc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ojos7uEem5ebMPg-wyFg" name="EFHQTX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojo87uEem5ebMPg-wyFg" value="EFHQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjpM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojpc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ojps7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojp87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjqM7uEem5ebMPg-wyFg" name="EFHPTX" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojqc7uEem5ebMPg-wyFg" value="EFHPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Ojqs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojq87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8OjrM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojrc7uEem5ebMPg-wyFg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ojrs7uEem5ebMPg-wyFg" name="EFAKCD" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojr87uEem5ebMPg-wyFg" value="EFAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8OjsM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ojsc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Ojss7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Ojs87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8OjtM7uEem5ebMPg-wyFg" name="EFAGTX" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Ojtc7uEem5ebMPg-wyFg" value="EFAGTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKQM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKQc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKQs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKQ87uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKRM7uEem5ebMPg-wyFg" name="EFAUTX" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKRc7uEem5ebMPg-wyFg" value="EFAUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKRs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKR87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKSM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKSc7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKSs7uEem5ebMPg-wyFg" name="EFHOTX" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKS87uEem5ebMPg-wyFg" value="EFHOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKTM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKTc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKTs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKT87uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKUM7uEem5ebMPg-wyFg" name="EFU9NA" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKUc7uEem5ebMPg-wyFg" value="EFU9NA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKUs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKU87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKVM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKVc7uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKVs7uEem5ebMPg-wyFg" name="EFZLTX" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKV87uEem5ebMPg-wyFg" value="EFZLTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKWM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8PKWc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKWs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKW87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKXM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKXc7uEem5ebMPg-wyFg" name="EFFMTX" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKXs7uEem5ebMPg-wyFg" value="EFFMTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKX87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8PKYM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKYc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKYs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKY87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKZM7uEem5ebMPg-wyFg" name="EFIZST" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKZc7uEem5ebMPg-wyFg" value="EFIZST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKZs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKZ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKaM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKac7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKas7uEem5ebMPg-wyFg" name="EFABCD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKa87uEem5ebMPg-wyFg" value="EFABCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKbM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKbc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKbs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKb87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKcM7uEem5ebMPg-wyFg" name="EFABNA" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKcc7uEem5ebMPg-wyFg" value="EFABNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKcs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKc87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKdM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKdc7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKds7uEem5ebMPg-wyFg" name="EFBCCD" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKd87uEem5ebMPg-wyFg" value="EFBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKeM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8PKec7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKes7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKe87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKfM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKfc7uEem5ebMPg-wyFg" name="EFBSNA" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKfs7uEem5ebMPg-wyFg" value="EFBSNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKf87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKgM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKgc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKgs7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKg87uEem5ebMPg-wyFg" name="EFBTNA" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKhM7uEem5ebMPg-wyFg" value="EFBTNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKhc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKhs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKh87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKiM7uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKic7uEem5ebMPg-wyFg" name="EFDNTX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKis7uEem5ebMPg-wyFg" value="EFDNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKi87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8PKjM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKjc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKjs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKj87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKkM7uEem5ebMPg-wyFg" name="EFBHCD" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKkc7uEem5ebMPg-wyFg" value="EFBHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8PKk87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKlM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKlc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKls7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKl87uEem5ebMPg-wyFg" name="EFBBTX" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKmM7uEem5ebMPg-wyFg" value="EFBBTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKmc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKms7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKm87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKnM7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKnc7uEem5ebMPg-wyFg" name="EFAQCD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKns7uEem5ebMPg-wyFg" value="EFAQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKn87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKoM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKoc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKos7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKo87uEem5ebMPg-wyFg" name="EFAOTX" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKpM7uEem5ebMPg-wyFg" value="EFAOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKpc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKps7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKp87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKqM7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKqc7uEem5ebMPg-wyFg" name="EFGHCD" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKqs7uEem5ebMPg-wyFg" value="EFGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKq87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKrM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKrc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKrs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKr87uEem5ebMPg-wyFg" name="EFHBNA" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKsM7uEem5ebMPg-wyFg" value="EFHBNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKsc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKss7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKs87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKtM7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKtc7uEem5ebMPg-wyFg" name="EFAMCD" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKts7uEem5ebMPg-wyFg" value="EFAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKt87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKuM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKuc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKus7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKu87uEem5ebMPg-wyFg" name="EFSINA" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKvM7uEem5ebMPg-wyFg" value="EFSINA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKvc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKvs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKv87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKwM7uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PKwc7uEem5ebMPg-wyFg" name="EFBOCD" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PKws7uEem5ebMPg-wyFg" value="EFBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PKw87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PKxM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PKxc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PKxs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PxUM7uEem5ebMPg-wyFg" name="EFHRST" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PxUc7uEem5ebMPg-wyFg" value="EFHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PxUs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PxU87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PxVM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PxVc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PxVs7uEem5ebMPg-wyFg" name="EFBYNA" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PxV87uEem5ebMPg-wyFg" value="EFBYNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PxWM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PxWc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PxWs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PxW87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PxXM7uEem5ebMPg-wyFg" name="EFPFN6" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PxXc7uEem5ebMPg-wyFg" value="EFPFN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PxXs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8PxX87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PxYM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PxYc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PxYs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PxY87uEem5ebMPg-wyFg" name="EFVANA" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PxZM7uEem5ebMPg-wyFg" value="EFVANA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PxZc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PxZs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PxZ87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PxaM7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Pxac7uEem5ebMPg-wyFg" name="EFTFC1" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Pxas7uEem5ebMPg-wyFg" value="EFTFC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Pxa87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PxbM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Pxbc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Pxbs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Pxb87uEem5ebMPg-wyFg" name="EFFTNB" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PxcM7uEem5ebMPg-wyFg" value="EFFTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Pxcc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Pxcs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Pxc87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PxdM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Pxdc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Pxds7uEem5ebMPg-wyFg" name="EFFJTX" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Pxd87uEem5ebMPg-wyFg" value="EFFJTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PxeM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Pxec7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Pxes7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Pxe87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PxfM7uEem5ebMPg-wyFg" name="EFPGN6" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Pxfc7uEem5ebMPg-wyFg" value="EFPGN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Pxfs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Pxf87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8PxgM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Pxgc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Pxgs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Pxg87uEem5ebMPg-wyFg" name="EFVBNA" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8PxhM7uEem5ebMPg-wyFg" value="EFVBNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Pxhc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Pxhs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Pxh87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PxiM7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Pxic7uEem5ebMPg-wyFg" name="EFAGCD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Pxis7uEem5ebMPg-wyFg" value="EFAGCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Pxi87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8PxjM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Pxjc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Pxjs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Pxj87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8PxkM7uEem5ebMPg-wyFg" name="EFACTX" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Pxkc7uEem5ebMPg-wyFg" value="EFACTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Pxks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Pxk87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8PxlM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Pxlc7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Pxls7uEem5ebMPg-wyFg" name="EFF2CD" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Pxl87uEem5ebMPg-wyFg" value="EFF2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8PxmM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Pxmc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Pxms7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Pxm87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8PxnM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Pxnc7uEem5ebMPg-wyFg" name="EFHNNA" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Pxns7uEem5ebMPg-wyFg" value="EFHNNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Pxn87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYYM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYYc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYYs7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYY87uEem5ebMPg-wyFg" name="EFKVS3" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYZM7uEem5ebMPg-wyFg" value="EFKVS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYZc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYZs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYZ87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYaM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYac7uEem5ebMPg-wyFg" name="EFTWP3" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYas7uEem5ebMPg-wyFg" value="EFTWP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYa87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYbM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYbc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYbs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYb87uEem5ebMPg-wyFg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYcM7uEem5ebMPg-wyFg" name="EFTXP3" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYcc7uEem5ebMPg-wyFg" value="EFTXP3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYcs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYc87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYdM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYdc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYds7uEem5ebMPg-wyFg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYd87uEem5ebMPg-wyFg" name="EFVUN2" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYeM7uEem5ebMPg-wyFg" value="EFVUN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYec7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYes7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYe87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYfM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYfc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYfs7uEem5ebMPg-wyFg" name="EFV1N2" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYf87uEem5ebMPg-wyFg" value="EFV1N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYgM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYgc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYgs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYg87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYhM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYhc7uEem5ebMPg-wyFg" name="EFV2N2" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYhs7uEem5ebMPg-wyFg" value="EFV2N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYh87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYiM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYic7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYis7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYi87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYjM7uEem5ebMPg-wyFg" name="EFV3N2" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYjc7uEem5ebMPg-wyFg" value="EFV3N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYjs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYj87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYkM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYkc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYks7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYk87uEem5ebMPg-wyFg" name="EFV4N2" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYlM7uEem5ebMPg-wyFg" value="EFV4N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYlc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYls7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYl87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYmM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYmc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYms7uEem5ebMPg-wyFg" name="EFBACD" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYm87uEem5ebMPg-wyFg" value="EFBACD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYnM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYnc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYns7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYn87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYoM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYoc7uEem5ebMPg-wyFg" name="EFVCNA" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYos7uEem5ebMPg-wyFg" value="EFVCNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYo87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYpM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYpc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYps7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYp87uEem5ebMPg-wyFg" name="EFPHN6" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYqM7uEem5ebMPg-wyFg" value="EFPHN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYqc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYqs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYq87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYrM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYrc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYrs7uEem5ebMPg-wyFg" name="EFBBCD" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYr87uEem5ebMPg-wyFg" value="EFBBCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYsM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYsc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYss7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYs87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYtM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYtc7uEem5ebMPg-wyFg" name="EFVDNA" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYts7uEem5ebMPg-wyFg" value="EFVDNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYt87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYuM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYuc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYus7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYu87uEem5ebMPg-wyFg" name="EFPIN6" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYvM7uEem5ebMPg-wyFg" value="EFPIN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYvc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QYvs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYv87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYwM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYwc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYws7uEem5ebMPg-wyFg" name="EFBNNA" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYw87uEem5ebMPg-wyFg" value="EFBNNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYxM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYxc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYxs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYx87uEem5ebMPg-wyFg" value="30"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYyM7uEem5ebMPg-wyFg" name="EFPNT2" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYyc7uEem5ebMPg-wyFg" value="EFPNT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QYys7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QYy87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QYzM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QYzc7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QYzs7uEem5ebMPg-wyFg" name="EFVENA" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QYz87uEem5ebMPg-wyFg" value="EFVENA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QY0M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QY0c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QY0s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QY087uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QY1M7uEem5ebMPg-wyFg" name="EFQON6" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QY1c7uEem5ebMPg-wyFg" value="EFQON6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QY1s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QY187uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QY2M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QY2c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QY2s7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QY287uEem5ebMPg-wyFg" name="EFQPN6" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QY3M7uEem5ebMPg-wyFg" value="EFQPN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QY3c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8QY3s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QY387uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QY4M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QY4c7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8QY4s7uEem5ebMPg-wyFg" name="EFK8S3" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8QY487uEem5ebMPg-wyFg" value="EFK8S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8QY5M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8QY5c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8QY5s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8QY587uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_cM7uEem5ebMPg-wyFg" name="EFK9S3" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_cc7uEem5ebMPg-wyFg" value="EFK9S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_cs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_c87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_dM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_dc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_ds7uEem5ebMPg-wyFg" name="EFLBS3" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_d87uEem5ebMPg-wyFg" value="EFLBS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_eM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_ec7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_es7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_e87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_fM7uEem5ebMPg-wyFg" name="EFLAS3" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_fc7uEem5ebMPg-wyFg" value="EFLAS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_fs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_f87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_gM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_gc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_gs7uEem5ebMPg-wyFg" name="EFOMS3" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_g87uEem5ebMPg-wyFg" value="EFOMS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_hM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_hc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_hs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_h87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_iM7uEem5ebMPg-wyFg" name="EFLCS3" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_ic7uEem5ebMPg-wyFg" value="EFLCS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_is7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_i87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_jM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_jc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_js7uEem5ebMPg-wyFg" name="EFLDS3" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_j87uEem5ebMPg-wyFg" value="EFLDS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_kM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_kc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_ks7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_k87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_lM7uEem5ebMPg-wyFg" name="EFLES3" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_lc7uEem5ebMPg-wyFg" value="EFLES3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_ls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_l87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_mM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_mc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_ms7uEem5ebMPg-wyFg" name="EFLFS3" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_m87uEem5ebMPg-wyFg" value="EFLFS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_nM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_nc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_ns7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_n87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_oM7uEem5ebMPg-wyFg" name="EFLGS3" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_oc7uEem5ebMPg-wyFg" value="EFLGS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_os7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_o87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_pM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_pc7uEem5ebMPg-wyFg" value="1"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Q_ps7uEem5ebMPg-wyFg" name="ORBUREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Q_p87uEem5ebMPg-wyFg" value="ORBUREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8Q_qM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Q_qc7uEem5ebMPg-wyFg" name="BUBOCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_qs7uEem5ebMPg-wyFg" value="BUBOCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_q87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_rM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_rc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_rs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_r87uEem5ebMPg-wyFg" name="BUHRST" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_sM7uEem5ebMPg-wyFg" value="BUHRST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_sc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_ss7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_s87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_tM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_tc7uEem5ebMPg-wyFg" name="BUFHNB" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_ts7uEem5ebMPg-wyFg" value="BUFHNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_t87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Q_uM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_uc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_us7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_u87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_vM7uEem5ebMPg-wyFg" name="BUC7PR" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_vc7uEem5ebMPg-wyFg" value="BUC7PR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_vs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Q_v87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_wM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_wc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_ws7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_w87uEem5ebMPg-wyFg" name="BUHGNA" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_xM7uEem5ebMPg-wyFg" value="BUHGNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_xc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_xs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_x87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_yM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_yc7uEem5ebMPg-wyFg" name="BUFINB" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_ys7uEem5ebMPg-wyFg" value="BUFINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_y87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Q_zM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_zc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_zs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_z87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_0M7uEem5ebMPg-wyFg" name="BUN1NB" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_0c7uEem5ebMPg-wyFg" value="BUN1NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_0s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Q_087uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_1M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_1c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_1s7uEem5ebMPg-wyFg" value="6"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Q_187uEem5ebMPg-wyFg" name="MYCARTP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Q_2M7uEem5ebMPg-wyFg" value="MYCARTP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8Q_2c7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Q_2s7uEem5ebMPg-wyFg" name="IDPREF" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_287uEem5ebMPg-wyFg" value="IDPREF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_3M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Q_3c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_3s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_387uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_4M7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_4c7uEem5ebMPg-wyFg" name="ARTCOD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_4s7uEem5ebMPg-wyFg" value="ARTCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_487uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Q_5M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_5c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_5s7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_587uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_6M7uEem5ebMPg-wyFg" name="TOPTRT" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_6c7uEem5ebMPg-wyFg" value="TOPTRT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_6s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Q_687uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_7M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_7c7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_7s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_787uEem5ebMPg-wyFg" name="I1K0NB" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_8M7uEem5ebMPg-wyFg" value="I1K0NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Q_8c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Q_8s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Q_887uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Q_9M7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Q_9c7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Q_9s7uEem5ebMPg-wyFg" name="I1FPS2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Q_987uEem5ebMPg-wyFg" value="I1FPS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8RmgM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rmgc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmgs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rmg87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8RmhM7uEem5ebMPg-wyFg" name="I1CNN3" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rmhc7uEem5ebMPg-wyFg" value="I1CNN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmhs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rmh87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8RmiM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rmic7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmis7uEem5ebMPg-wyFg" name="I1JWCD" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rmi87uEem5ebMPg-wyFg" value="I1JWCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8RmjM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rmjc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmjs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rmj87uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8RmkM7uEem5ebMPg-wyFg" name="DTEMAJ" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rmkc7uEem5ebMPg-wyFg" value="DTEMAJ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Rmk87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8RmlM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmlc7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rmls7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rml87uEem5ebMPg-wyFg" name="I1QQST" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8RmmM7uEem5ebMPg-wyFg" value="I1QQST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmmc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rmms7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmm87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8RmnM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmnc7uEem5ebMPg-wyFg" name="I1QPST" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rmns7uEem5ebMPg-wyFg" value="I1QPST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmn87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8RmoM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmoc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rmos7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmo87uEem5ebMPg-wyFg" name="I1CPCD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8RmpM7uEem5ebMPg-wyFg" value="I1CPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmpc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rmps7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmp87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8RmqM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmqc7uEem5ebMPg-wyFg" name="I1ISTX" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rmqs7uEem5ebMPg-wyFg" value="I1ISTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmq87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8RmrM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmrc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rmrs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmr87uEem5ebMPg-wyFg" name="B8IOS3" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8RmsM7uEem5ebMPg-wyFg" value="B8IOS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmsc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rmss7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rms87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8RmtM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmtc7uEem5ebMPg-wyFg" name="B8I7S3" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rmts7uEem5ebMPg-wyFg" value="B8I7S3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmt87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8RmuM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmuc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rmus7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmu87uEem5ebMPg-wyFg" name="B8IMS3" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8RmvM7uEem5ebMPg-wyFg" value="B8IMS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmvc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rmvs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmv87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8RmwM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmwc7uEem5ebMPg-wyFg" name="A00F20" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rmws7uEem5ebMPg-wyFg" value="A00F20"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmw87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8RmxM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmxc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rmxs7uEem5ebMPg-wyFg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmx87uEem5ebMPg-wyFg" name="A00F90" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8RmyM7uEem5ebMPg-wyFg" value="A00F90"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmyc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rmys7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rmy87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8RmzM7uEem5ebMPg-wyFg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rmzc7uEem5ebMPg-wyFg" name="A00F92" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rmzs7uEem5ebMPg-wyFg" value="A00F92"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rmz87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rm0M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rm0c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rm0s7uEem5ebMPg-wyFg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rm087uEem5ebMPg-wyFg" name="A00F30" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rm1M7uEem5ebMPg-wyFg" value="A00F30"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rm1c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rm1s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rm187uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rm2M7uEem5ebMPg-wyFg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rm2c7uEem5ebMPg-wyFg" name="A00F91" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rm2s7uEem5ebMPg-wyFg" value="A00F91"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rm287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rm3M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rm3c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rm3s7uEem5ebMPg-wyFg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rm387uEem5ebMPg-wyFg" name="A00F93" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rm4M7uEem5ebMPg-wyFg" value="A00F93"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rm4c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rm4s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rm487uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rm5M7uEem5ebMPg-wyFg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rm5c7uEem5ebMPg-wyFg" name="A00E25" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rm5s7uEem5ebMPg-wyFg" value="A00E25"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rm587uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Rm6M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rm6c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rm6s7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rm687uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rm7M7uEem5ebMPg-wyFg" name="A00F40" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rm7c7uEem5ebMPg-wyFg" value="A00F40"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rm7s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rm787uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rm8M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rm8c7uEem5ebMPg-wyFg" value="250"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rm8s7uEem5ebMPg-wyFg" name="MQXYNA" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rm887uEem5ebMPg-wyFg" value="MQXYNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rm9M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rm9c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rm9s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rm987uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rm-M7uEem5ebMPg-wyFg" name="ITMU" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Rm-c7uEem5ebMPg-wyFg" value="ITMU"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Rm-s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Rm-87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Rm_M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Rm_c7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Rm_s7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Rm_87uEem5ebMPg-wyFg" name="ITMR" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8RnAM7uEem5ebMPg-wyFg" value="ITMR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8RnAc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8RnAs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNkM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SNkc7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SNks7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SNk87uEem5ebMPg-wyFg" name="ITMF" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SNlM7uEem5ebMPg-wyFg" value="ITMF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SNlc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SNls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNl87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SNmM7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SNmc7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SNms7uEem5ebMPg-wyFg" name="ITMDNG" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SNm87uEem5ebMPg-wyFg" value="ITMDNG"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SNnM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SNnc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNns7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SNn87uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SNoM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SNoc7uEem5ebMPg-wyFg" name="X00001" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SNos7uEem5ebMPg-wyFg" value="X00001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SNo87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SNpM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNpc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SNps7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SNp87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SNqM7uEem5ebMPg-wyFg" name="X00002" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SNqc7uEem5ebMPg-wyFg" value="X00002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SNqs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SNq87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNrM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SNrc7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SNrs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SNr87uEem5ebMPg-wyFg" name="X00003" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SNsM7uEem5ebMPg-wyFg" value="X00003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SNsc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SNss7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNs87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SNtM7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SNtc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SNts7uEem5ebMPg-wyFg" name="X00004" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SNt87uEem5ebMPg-wyFg" value="X00004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SNuM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SNuc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNus7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SNu87uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SNvM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SNvc7uEem5ebMPg-wyFg" name="X00005" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SNvs7uEem5ebMPg-wyFg" value="X00005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SNv87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SNwM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNwc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SNws7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SNw87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SNxM7uEem5ebMPg-wyFg" name="X00006" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SNxc7uEem5ebMPg-wyFg" value="X00006"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SNxs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SNx87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNyM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SNyc7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SNys7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SNy87uEem5ebMPg-wyFg" name="X00007" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SNzM7uEem5ebMPg-wyFg" value="X00007"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SNzc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SNzs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SNz87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SN0M7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SN0c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SN0s7uEem5ebMPg-wyFg" name="X00008" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SN087uEem5ebMPg-wyFg" value="X00008"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SN1M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SN1c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SN1s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SN187uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SN2M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SN2c7uEem5ebMPg-wyFg" name="X00009" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SN2s7uEem5ebMPg-wyFg" value="X00009"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SN287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SN3M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SN3c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SN3s7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SN387uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SN4M7uEem5ebMPg-wyFg" name="X00010" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SN4c7uEem5ebMPg-wyFg" value="X00010"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SN4s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SN487uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SN5M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SN5c7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SN5s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SN587uEem5ebMPg-wyFg" name="X00011" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SN6M7uEem5ebMPg-wyFg" value="X00011"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SN6c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SN6s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SN687uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SN7M7uEem5ebMPg-wyFg" value="NUMERIC"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SN7c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SN7s7uEem5ebMPg-wyFg" name="I1MINB" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SN787uEem5ebMPg-wyFg" value="I1MINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SN8M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SN8c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SN8s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SN887uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SN9M7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SN9c7uEem5ebMPg-wyFg" name="I1MJNB" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SN9s7uEem5ebMPg-wyFg" value="I1MJNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SN987uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SN-M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SN-c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SN-s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SN-87uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SN_M7uEem5ebMPg-wyFg" name="I1MKNB" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SN_c7uEem5ebMPg-wyFg" value="I1MKNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SN_s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SN_87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SOAM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SOAc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SOAs7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SOA87uEem5ebMPg-wyFg" name="I1MLNB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SOBM7uEem5ebMPg-wyFg" value="I1MLNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SOBc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SOBs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SOB87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SOCM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SOCc7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SOCs7uEem5ebMPg-wyFg" name="I1MMNB" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SOC87uEem5ebMPg-wyFg" value="I1MMNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SODM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8SODc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SODs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SOD87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SOEM7uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SOEc7uEem5ebMPg-wyFg" name="CDECOT" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8SOEs7uEem5ebMPg-wyFg" value="CDECOT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8SOE87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8SOFM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8SOFc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8SOFs7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8SOF87uEem5ebMPg-wyFg" name="CDCORP" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0oM7uEem5ebMPg-wyFg" value="CDCORP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0oc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S0os7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0o87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S0pM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0pc7uEem5ebMPg-wyFg" name="TITRE_AKENEO" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0ps7uEem5ebMPg-wyFg" value="TITRE_AKENEO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0p87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S0qM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0qc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S0qs7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0q87uEem5ebMPg-wyFg" name="DESCRIPTIF_AKENEO" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0rM7uEem5ebMPg-wyFg" value="DESCRIPTIF_AKENEO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0rc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S0rs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0r87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S0sM7uEem5ebMPg-wyFg" value="2048"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0sc7uEem5ebMPg-wyFg" name="CARACTERISTIQUE_AKENEO" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0ss7uEem5ebMPg-wyFg" value="CARACTERISTIQUE_AKENEO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0s87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S0tM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0tc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S0ts7uEem5ebMPg-wyFg" value="2048"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0t87uEem5ebMPg-wyFg" name="CARACTERISTIQUE_NORME" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0uM7uEem5ebMPg-wyFg" value="CARACTERISTIQUE_NORME"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0uc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S0us7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0u87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S0vM7uEem5ebMPg-wyFg" value="2048"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8S0vc7uEem5ebMPg-wyFg" name="ORBDREP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8S0vs7uEem5ebMPg-wyFg" value="ORBDREP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8S0v87uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8S0wM7uEem5ebMPg-wyFg" name="BDBCCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0wc7uEem5ebMPg-wyFg" value="BDBCCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0ws7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8S0w87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S0xM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0xc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S0xs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0x87uEem5ebMPg-wyFg" name="BDSTST" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0yM7uEem5ebMPg-wyFg" value="BDSTST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0yc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S0ys7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0y87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S0zM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0zc7uEem5ebMPg-wyFg" name="BDJXNA" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0zs7uEem5ebMPg-wyFg" value="BDJXNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0z87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S00M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S00c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S00s7uEem5ebMPg-wyFg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0087uEem5ebMPg-wyFg" name="BDNPCD" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S01M7uEem5ebMPg-wyFg" value="BDNPCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S01c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S01s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0187uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S02M7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S02c7uEem5ebMPg-wyFg" name="BDNQCD" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S02s7uEem5ebMPg-wyFg" value="BDNQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S03M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S03c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S03s7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0387uEem5ebMPg-wyFg" name="BDBSNA" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S04M7uEem5ebMPg-wyFg" value="BDBSNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S04c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S04s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0487uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S05M7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S05c7uEem5ebMPg-wyFg" name="BDBTNA" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S05s7uEem5ebMPg-wyFg" value="BDBTNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0587uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S06M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S06c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S06s7uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0687uEem5ebMPg-wyFg" name="BDA9TX" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S07M7uEem5ebMPg-wyFg" value="BDA9TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S07c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S07s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0787uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S08M7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S08c7uEem5ebMPg-wyFg" name="BDFZTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S08s7uEem5ebMPg-wyFg" value="BDFZTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0887uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S09M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S09c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S09s7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0987uEem5ebMPg-wyFg" name="BDL2CD" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0-M7uEem5ebMPg-wyFg" value="BDL2CD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0-c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S0-s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S0-87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S0_M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S0_c7uEem5ebMPg-wyFg" name="BDBHCD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S0_s7uEem5ebMPg-wyFg" value="BDBHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S0_87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8S1AM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S1Ac7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S1As7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S1A87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S1BM7uEem5ebMPg-wyFg" name="BDAMCD" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S1Bc7uEem5ebMPg-wyFg" value="BDAMCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S1Bs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S1B87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S1CM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S1Cc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S1Cs7uEem5ebMPg-wyFg" name="BDGHCD" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S1C87uEem5ebMPg-wyFg" value="BDGHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S1DM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S1Dc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S1Ds7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S1D87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S1EM7uEem5ebMPg-wyFg" name="BDAQCD" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S1Ec7uEem5ebMPg-wyFg" value="BDAQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S1Es7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S1E87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S1FM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S1Fc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S1Fs7uEem5ebMPg-wyFg" name="BDAZCD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S1F87uEem5ebMPg-wyFg" value="BDAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S1GM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8S1Gc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S1Gs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8S1G87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8S1HM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8S1Hc7uEem5ebMPg-wyFg" name="BDDNTX" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8S1Hs7uEem5ebMPg-wyFg" value="BDDNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8S1H87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8S1IM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8S1Ic7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TbsM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tbsc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tbss7uEem5ebMPg-wyFg" name="BDBJST" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tbs87uEem5ebMPg-wyFg" value="BDBJST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TbtM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tbtc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tbts7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tbt87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8TbuM7uEem5ebMPg-wyFg" name="BDARDT" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tbuc7uEem5ebMPg-wyFg" value="BDARDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tbus7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Tbu87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8TbvM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tbvc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tbvs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tbv87uEem5ebMPg-wyFg" name="BDASDT" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8TbwM7uEem5ebMPg-wyFg" value="BDASDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tbwc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Tbws7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tbw87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TbxM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tbxc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tbxs7uEem5ebMPg-wyFg" name="BDXCST" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tbx87uEem5ebMPg-wyFg" value="BDXCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TbyM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tbyc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tbys7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tby87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8TbzM7uEem5ebMPg-wyFg" name="BDJBST" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tbzc7uEem5ebMPg-wyFg" value="BDJBST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tbzs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tbz87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tb0M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tb0c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tb0s7uEem5ebMPg-wyFg" name="BDF0TX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tb087uEem5ebMPg-wyFg" value="BDF0TX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tb1M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tb1c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tb1s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tb187uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tb2M7uEem5ebMPg-wyFg" name="BDHTNB" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tb2c7uEem5ebMPg-wyFg" value="BDHTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tb2s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Tb287uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tb3M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tb3c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tb3s7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tb387uEem5ebMPg-wyFg" name="BDAVDT" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tb4M7uEem5ebMPg-wyFg" value="BDAVDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tb4c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Tb4s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tb487uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tb5M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tb5c7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tb5s7uEem5ebMPg-wyFg" name="BDAWDT" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tb587uEem5ebMPg-wyFg" value="BDAWDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tb6M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Tb6c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tb6s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tb687uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tb7M7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tb7c7uEem5ebMPg-wyFg" name="BDJCST" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tb7s7uEem5ebMPg-wyFg" value="BDJCST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tb787uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tb8M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tb8c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tb8s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tb887uEem5ebMPg-wyFg" name="BDJDST" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tb9M7uEem5ebMPg-wyFg" value="BDJDST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tb9c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tb9s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tb987uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tb-M7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tb-c7uEem5ebMPg-wyFg" name="BDJEST" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Tb-s7uEem5ebMPg-wyFg" value="BDJEST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Tb-87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Tb_M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Tb_c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Tb_s7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Tb_87uEem5ebMPg-wyFg" name="BDDOPR" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8TcAM7uEem5ebMPg-wyFg" value="BDDOPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TcAc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8TcAs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8TcA87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TcBM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8TcBc7uEem5ebMPg-wyFg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8TcBs7uEem5ebMPg-wyFg" name="BDDPPR" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8TcB87uEem5ebMPg-wyFg" value="BDDPPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TcCM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8TcCc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8TcCs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TcC87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8TcDM7uEem5ebMPg-wyFg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8TcDc7uEem5ebMPg-wyFg" name="BDDQPR" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8TcDs7uEem5ebMPg-wyFg" value="BDDQPR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TcD87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8TcEM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8TcEc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TcEs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8TcE87uEem5ebMPg-wyFg" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8TcFM7uEem5ebMPg-wyFg" name="BDF2NB" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8TcFc7uEem5ebMPg-wyFg" value="BDF2NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TcFs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8TcF87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8TcGM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TcGc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8TcGs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8TcG87uEem5ebMPg-wyFg" name="BDF3NB" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8TcHM7uEem5ebMPg-wyFg" value="BDF3NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TcHc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8TcHs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8TcH87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TcIM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8TcIc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8TcIs7uEem5ebMPg-wyFg" name="BDF4NB" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8TcI87uEem5ebMPg-wyFg" value="BDF4NB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TcJM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8TcJc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8TcJs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TcJ87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8TcKM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8TcKc7uEem5ebMPg-wyFg" name="BDTSST" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8TcKs7uEem5ebMPg-wyFg" value="BDTSST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TcK87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8TcLM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TcLc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8TcLs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8TcL87uEem5ebMPg-wyFg" name="BDC6N3" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8TcMM7uEem5ebMPg-wyFg" value="BDC6N3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8TcMc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8TcMs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8TcM87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8TcNM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UCwM7uEem5ebMPg-wyFg" name="BDC7N3" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UCwc7uEem5ebMPg-wyFg" value="BDC7N3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UCws7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UCw87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UCxM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UCxc7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UCxs7uEem5ebMPg-wyFg" name="BDKSTX" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UCx87uEem5ebMPg-wyFg" value="BDKSTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UCyM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UCyc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UCys7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UCy87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UCzM7uEem5ebMPg-wyFg" name="BDKTTX" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UCzc7uEem5ebMPg-wyFg" value="BDKTTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UCzs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UCz87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UC0M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UC0c7uEem5ebMPg-wyFg" value="26"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UC0s7uEem5ebMPg-wyFg" name="BDAKCD" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UC087uEem5ebMPg-wyFg" value="BDAKCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UC1M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UC1c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UC1s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UC187uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UC2M7uEem5ebMPg-wyFg" name="BDKVTX" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UC2c7uEem5ebMPg-wyFg" value="BDKVTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UC2s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UC287uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UC3M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UC3c7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UC3s7uEem5ebMPg-wyFg" name="BDKWTX" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UC387uEem5ebMPg-wyFg" value="BDKWTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UC4M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UC4c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UC4s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UC487uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UC5M7uEem5ebMPg-wyFg" name="BDZUST" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UC5c7uEem5ebMPg-wyFg" value="BDZUST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UC5s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UC587uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UC6M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UC6c7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UC6s7uEem5ebMPg-wyFg" name="BDWNTX" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UC687uEem5ebMPg-wyFg" value="BDWNTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UC7M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UC7c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UC7s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UC787uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UC8M7uEem5ebMPg-wyFg" name="BDWOTX" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UC8c7uEem5ebMPg-wyFg" value="BDWOTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UC8s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UC887uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UC9M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UC9c7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UC9s7uEem5ebMPg-wyFg" name="BDWPTX" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UC987uEem5ebMPg-wyFg" value="BDWPTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UC-M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UC-c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UC-s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UC-87uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UC_M7uEem5ebMPg-wyFg" name="BDWQTX" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UC_c7uEem5ebMPg-wyFg" value="BDWQTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UC_s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UC_87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDAM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDAc7uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UDAs7uEem5ebMPg-wyFg" name="BDWRTX" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDA87uEem5ebMPg-wyFg" value="BDWRTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDBM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UDBc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDBs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDB87uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UDCM7uEem5ebMPg-wyFg" name="BDWSTX" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDCc7uEem5ebMPg-wyFg" value="BDWSTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDCs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UDC87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDDM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDDc7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UDDs7uEem5ebMPg-wyFg" name="BDWTTX" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDD87uEem5ebMPg-wyFg" value="BDWTTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDEM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UDEc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDEs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDE87uEem5ebMPg-wyFg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UDFM7uEem5ebMPg-wyFg" name="BDWUTX" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDFc7uEem5ebMPg-wyFg" value="BDWUTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDFs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UDF87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDGM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDGc7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UDGs7uEem5ebMPg-wyFg" name="BDWVTX" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDG87uEem5ebMPg-wyFg" value="BDWVTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDHM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UDHc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDHs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDH87uEem5ebMPg-wyFg" value="25"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8UDIM7uEem5ebMPg-wyFg" name="WEBSUIVI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8UDIc7uEem5ebMPg-wyFg" value="WEBSUIVI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8UDIs7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8UDI87uEem5ebMPg-wyFg" name="WSAZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDJM7uEem5ebMPg-wyFg" value="WSAZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDJc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UDJs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UDJ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDKM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDKc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UDKs7uEem5ebMPg-wyFg" name="WSDATE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDK87uEem5ebMPg-wyFg" value="WSDATE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDLM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UDLc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UDLs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDL87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDMM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UDMc7uEem5ebMPg-wyFg" name="WSHEURE" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDMs7uEem5ebMPg-wyFg" value="WSHEURE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDM87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UDNM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UDNc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDNs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDN87uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UDOM7uEem5ebMPg-wyFg" name="WSTYPE" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDOc7uEem5ebMPg-wyFg" value="WSTYPE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDOs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UDO87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UDPM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UDPc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UDPs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UDP87uEem5ebMPg-wyFg" name="WSIDWEBC" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UDQM7uEem5ebMPg-wyFg" value="WSIDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UDQc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Up0M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Up0c7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Up0s7uEem5ebMPg-wyFg" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Up087uEem5ebMPg-wyFg" name="OSI6REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Up1M7uEem5ebMPg-wyFg" value="OSI6REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8Up1c7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Up1s7uEem5ebMPg-wyFg" name="I6GHCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Up187uEem5ebMPg-wyFg" value="I6GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Up2M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Up2c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Up2s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Up287uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Up3M7uEem5ebMPg-wyFg" name="I6AQCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Up3c7uEem5ebMPg-wyFg" value="I6AQCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Up3s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Up387uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Up4M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Up4c7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Up4s7uEem5ebMPg-wyFg" name="I6PFP2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Up487uEem5ebMPg-wyFg" value="I6PFP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Up5M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Up5c7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Up5s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Up587uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Up6M7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Up6c7uEem5ebMPg-wyFg" name="I6PGP2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Up6s7uEem5ebMPg-wyFg" value="I6PGP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Up687uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Up7M7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Up7c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Up7s7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Up787uEem5ebMPg-wyFg" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8Up8M7uEem5ebMPg-wyFg" name="OSI4REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8Up8c7uEem5ebMPg-wyFg" value="OSI4REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8Up8s7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8Up887uEem5ebMPg-wyFg" name="I4FTNB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Up9M7uEem5ebMPg-wyFg" value="I4FTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Up9c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Up9s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Up987uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Up-M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Up-c7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Up-s7uEem5ebMPg-wyFg" name="I4O8P2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Up-87uEem5ebMPg-wyFg" value="I4O8P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Up_M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Up_c7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Up_s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Up_87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqAM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqAc7uEem5ebMPg-wyFg" name="I4O9P2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqAs7uEem5ebMPg-wyFg" value="I4O9P2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqA87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UqBM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqBc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqBs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqB87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqCM7uEem5ebMPg-wyFg" name="I4PAP2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqCc7uEem5ebMPg-wyFg" value="I4PAP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqCs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UqC87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqDM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqDc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqDs7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqD87uEem5ebMPg-wyFg" name="I4SUN5" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqEM7uEem5ebMPg-wyFg" value="I4SUN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqEc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UqEs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqE87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqFM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqFc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqFs7uEem5ebMPg-wyFg" name="I4PBP2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqF87uEem5ebMPg-wyFg" value="I4PBP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqGM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UqGc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqGs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqG87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqHM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqHc7uEem5ebMPg-wyFg" name="I4PCP2" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqHs7uEem5ebMPg-wyFg" value="I4PCP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqH87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UqIM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqIc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqIs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqI87uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqJM7uEem5ebMPg-wyFg" name="I4SVN5" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqJc7uEem5ebMPg-wyFg" value="I4SVN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqJs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UqJ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqKM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqKc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqKs7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqK87uEem5ebMPg-wyFg" name="I4SWN5" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqLM7uEem5ebMPg-wyFg" value="I4SWN5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqLc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UqLs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqL87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqMM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqMc7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqMs7uEem5ebMPg-wyFg" name="I4UNS2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqM87uEem5ebMPg-wyFg" value="I4UNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqNM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqNc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqNs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqN87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqOM7uEem5ebMPg-wyFg" name="I4UOS2" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqOc7uEem5ebMPg-wyFg" value="I4UOS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqOs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqO87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqPM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqPc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqPs7uEem5ebMPg-wyFg" name="I4GST2" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqP87uEem5ebMPg-wyFg" value="I4GST2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqQM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqQc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqQs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqQ87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqRM7uEem5ebMPg-wyFg" name="I4OXPC" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqRc7uEem5ebMPg-wyFg" value="I4OXPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqRs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UqR87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8UqSM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8UqSc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8UqSs7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8UqS87uEem5ebMPg-wyFg" name="I4OYPC" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8UqTM7uEem5ebMPg-wyFg" value="I4OYPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8UqTc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8UqTs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VQ4M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VQ4c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VQ4s7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VQ487uEem5ebMPg-wyFg" name="I4OZPC" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VQ5M7uEem5ebMPg-wyFg" value="I4OZPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VQ5c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8VQ5s7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VQ587uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VQ6M7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VQ6c7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VQ6s7uEem5ebMPg-wyFg" name="I4GTT2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VQ687uEem5ebMPg-wyFg" value="I4GTT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VQ7M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VQ7c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VQ7s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VQ787uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VQ8M7uEem5ebMPg-wyFg" name="I4GUT2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VQ8c7uEem5ebMPg-wyFg" value="I4GUT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VQ8s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VQ887uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VQ9M7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VQ9c7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VQ9s7uEem5ebMPg-wyFg" name="I4GVT2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VQ987uEem5ebMPg-wyFg" value="I4GVT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VQ-M7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VQ-c7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VQ-s7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VQ-87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VQ_M7uEem5ebMPg-wyFg" name="I4EFN6" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VQ_c7uEem5ebMPg-wyFg" value="I4EFN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VQ_s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8VQ_87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRAM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRAc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRAs7uEem5ebMPg-wyFg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRA87uEem5ebMPg-wyFg" name="I4ABCD" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRBM7uEem5ebMPg-wyFg" value="I4ABCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRBc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRBs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRB87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRCM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRCc7uEem5ebMPg-wyFg" name="I4ACCD" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRCs7uEem5ebMPg-wyFg" value="I4ACCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRC87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRDM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRDc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRDs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRD87uEem5ebMPg-wyFg" name="I4NBC1" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VREM7uEem5ebMPg-wyFg" value="I4NBC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VREc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8VREs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRE87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRFM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRFc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRFs7uEem5ebMPg-wyFg" name="I4ZQS2" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRF87uEem5ebMPg-wyFg" value="I4ZQS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRGM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRGc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRGs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRG87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRHM7uEem5ebMPg-wyFg" name="I4AZCD" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRHc7uEem5ebMPg-wyFg" value="I4AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRHs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8VRH87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRIM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRIc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRIs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRI87uEem5ebMPg-wyFg" name="I4EGN6" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRJM7uEem5ebMPg-wyFg" value="I4EGN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRJc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8VRJs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRJ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRKM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRKc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRKs7uEem5ebMPg-wyFg" name="I4ZRS2" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRK87uEem5ebMPg-wyFg" value="I4ZRS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRLM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRLc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRLs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRL87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRMM7uEem5ebMPg-wyFg" name="I4NCC1" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRMc7uEem5ebMPg-wyFg" value="I4NCC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRMs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRM87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRNM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRNc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRNs7uEem5ebMPg-wyFg" name="I4NDC1" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRN87uEem5ebMPg-wyFg" value="I4NDC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VROM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VROc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VROs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRO87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRPM7uEem5ebMPg-wyFg" name="I4ZSS2" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRPc7uEem5ebMPg-wyFg" value="I4ZSS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRPs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRP87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRQM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRQc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRQs7uEem5ebMPg-wyFg" name="I4IGS3" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRQ87uEem5ebMPg-wyFg" value="I4IGS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRRM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRRc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRRs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRR87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRSM7uEem5ebMPg-wyFg" name="I4IIS3" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRSc7uEem5ebMPg-wyFg" value="I4IIS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRSs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRS87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRTM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRTc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRTs7uEem5ebMPg-wyFg" name="I4IJS3" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRT87uEem5ebMPg-wyFg" value="I4IJS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRUM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRUc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRUs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRU87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRVM7uEem5ebMPg-wyFg" name="I4IKS3" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRVc7uEem5ebMPg-wyFg" value="I4IKS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRVs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRV87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRWM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRWc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8VRWs7uEem5ebMPg-wyFg" name="I4RYC1" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8VRW87uEem5ebMPg-wyFg" value="I4RYC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8VRXM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8VRXc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8VRXs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8VRX87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V38M7uEem5ebMPg-wyFg" name="I4KSN6" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V38c7uEem5ebMPg-wyFg" value="I4KSN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V38s7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8V3887uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V39M7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V39c7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V39s7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V3987uEem5ebMPg-wyFg" name="I4NKT2" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V3-M7uEem5ebMPg-wyFg" value="I4NKT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V3-c7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V3-s7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V3-87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V3_M7uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V3_c7uEem5ebMPg-wyFg" name="I4ILS3" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V3_s7uEem5ebMPg-wyFg" value="I4ILS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V3_87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4AM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4Ac7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4As7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4A87uEem5ebMPg-wyFg" name="I4RZC1" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4BM7uEem5ebMPg-wyFg" value="I4RZC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4Bc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4Bs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4B87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4CM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4Cc7uEem5ebMPg-wyFg" name="I4KTN6" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4Cs7uEem5ebMPg-wyFg" value="I4KTN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4C87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8V4DM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4Dc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4Ds7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4D87uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4EM7uEem5ebMPg-wyFg" name="I4KUN6" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4Ec7uEem5ebMPg-wyFg" value="I4KUN6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4Es7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8V4E87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4FM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4Fc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4Fs7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4F87uEem5ebMPg-wyFg" name="I4NLT2" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4GM7uEem5ebMPg-wyFg" value="I4NLT2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4Gc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4Gs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4G87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4HM7uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4Hc7uEem5ebMPg-wyFg" name="I4IHS3" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4Hs7uEem5ebMPg-wyFg" value="I4IHS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4H87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4IM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4Ic7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4Is7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4I87uEem5ebMPg-wyFg" name="I4R0C1" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4JM7uEem5ebMPg-wyFg" value="I4R0C1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4Jc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4Js7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4J87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4KM7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4Kc7uEem5ebMPg-wyFg" name="I4LNS3" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4Ks7uEem5ebMPg-wyFg" value="I4LNS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4K87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4LM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4Lc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4Ls7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4L87uEem5ebMPg-wyFg" name="I4UWC1" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4MM7uEem5ebMPg-wyFg" value="I4UWC1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4Mc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4Ms7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4M87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4NM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4Nc7uEem5ebMPg-wyFg" name="I4LOS3" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4Ns7uEem5ebMPg-wyFg" value="I4LOS3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4N87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4OM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4Oc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4Os7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4O87uEem5ebMPg-wyFg" name="I4Q5T2" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4PM7uEem5ebMPg-wyFg" value="I4Q5T2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4Pc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4Ps7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4P87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4QM7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4Qc7uEem5ebMPg-wyFg" name="I4W4DT" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4Qs7uEem5ebMPg-wyFg" value="I4W4DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4Q87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8V4RM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4Rc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4Rs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4R87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4SM7uEem5ebMPg-wyFg" name="I4W5DT" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4Sc7uEem5ebMPg-wyFg" value="I4W5DT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4Ss7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8V4S87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4TM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4Tc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4Ts7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4T87uEem5ebMPg-wyFg" name="I4NLN3" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4UM7uEem5ebMPg-wyFg" value="I4NLN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4Uc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4Us7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4U87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4VM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8V4Vc7uEem5ebMPg-wyFg" name="I4NMN3" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8V4Vs7uEem5ebMPg-wyFg" value="I4NMN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8V4V87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8V4WM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8V4Wc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8V4Ws7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfAM7uEem5ebMPg-wyFg" name="I4NNN3" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfAc7uEem5ebMPg-wyFg" value="I4NNN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfAs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfA87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfBM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfBc7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfBs7uEem5ebMPg-wyFg" name="I4NON3" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfB87uEem5ebMPg-wyFg" value="I4NON3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfCM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfCc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfCs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfC87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfDM7uEem5ebMPg-wyFg" name="I4NPN3" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfDc7uEem5ebMPg-wyFg" value="I4NPN3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfDs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfD87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfEM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfEc7uEem5ebMPg-wyFg" value="35"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8WfEs7uEem5ebMPg-wyFg" name="OSI5REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8WfE87uEem5ebMPg-wyFg" value="OSI5REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8WfFM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8WfFc7uEem5ebMPg-wyFg" name="I5GHCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfFs7uEem5ebMPg-wyFg" value="I5GHCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfF87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfGM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfGc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfGs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfG87uEem5ebMPg-wyFg" name="I5PDP2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfHM7uEem5ebMPg-wyFg" value="I5PDP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfHc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WfHs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfH87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfIM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfIc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfIs7uEem5ebMPg-wyFg" name="I5PEP2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfI87uEem5ebMPg-wyFg" value="I5PEP2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfJM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WfJc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfJs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfJ87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfKM7uEem5ebMPg-wyFg" value="9"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8WfKc7uEem5ebMPg-wyFg" name="PIXEL_MAGENTO_CLIENTS_MAILS">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8WfKs7uEem5ebMPg-wyFg" value="PIXEL_MAGENTO_CLIENTS_MAILS"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8WfK87uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8WfLM7uEem5ebMPg-wyFg" name="CODE_CLIENT" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfLc7uEem5ebMPg-wyFg" value="CODE_CLIENT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfLs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WfL87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfMM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfMc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfMs7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfM87uEem5ebMPg-wyFg" name="STATUT" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfNM7uEem5ebMPg-wyFg" value="STATUT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfNc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WfNs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfN87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfOM7uEem5ebMPg-wyFg" value="SMALLINT"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfOc7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfOs7uEem5ebMPg-wyFg" name="MAIL_MAGENTO" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfO87uEem5ebMPg-wyFg" value="MAIL_MAGENTO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfPM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfPc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfPs7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfP87uEem5ebMPg-wyFg" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfQM7uEem5ebMPg-wyFg" name="MAIL_MYCHOMETTE" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfQc7uEem5ebMPg-wyFg" value="MAIL_MYCHOMETTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfQs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfQ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfRM7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfRc7uEem5ebMPg-wyFg" value="100"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8WfRs7uEem5ebMPg-wyFg" name="ORQ3REP">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8WfR87uEem5ebMPg-wyFg" value="ORQ3REP"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8WfSM7uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8WfSc7uEem5ebMPg-wyFg" name="Q3AZCD" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfSs7uEem5ebMPg-wyFg" value="Q3AZCD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfS87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WfTM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfTc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfTs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfT87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfUM7uEem5ebMPg-wyFg" name="Q3VTN2" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfUc7uEem5ebMPg-wyFg" value="Q3VTN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfUs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WfU87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfVM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfVc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfVs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfV87uEem5ebMPg-wyFg" name="Q3VUN2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfWM7uEem5ebMPg-wyFg" value="Q3VUN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfWc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WfWs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfW87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfXM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfXc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfXs7uEem5ebMPg-wyFg" name="Q3VVN2" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfX87uEem5ebMPg-wyFg" value="Q3VVN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfYM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WfYc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8WfYs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfY87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfZM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfZc7uEem5ebMPg-wyFg" name="Q3VWN2" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8WfZs7uEem5ebMPg-wyFg" value="Q3VWN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfZ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WfaM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Wfac7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Wfas7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Wfa87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfbM7uEem5ebMPg-wyFg" name="Q3LMS2" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Wfbc7uEem5ebMPg-wyFg" value="Q3LMS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Wfbs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Wfb87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8WfcM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Wfcc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Wfcs7uEem5ebMPg-wyFg" name="Q3KODT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Wfc87uEem5ebMPg-wyFg" value="Q3KODT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8WfdM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Wfdc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Wfds7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Wfd87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8WfeM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Wfec7uEem5ebMPg-wyFg" name="Q3VXN2" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Wfes7uEem5ebMPg-wyFg" value="Q3VXN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Wfe87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8WffM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Wffc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Wffs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Wff87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8WfgM7uEem5ebMPg-wyFg" name="Q3VYN2" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Wfgc7uEem5ebMPg-wyFg" value="Q3VYN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Wfgs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Wfg87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGEM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGEc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGEs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGE87uEem5ebMPg-wyFg" name="Q3VZN2" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGFM7uEem5ebMPg-wyFg" value="Q3VZN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGFc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGFs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGF87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGGM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGGc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGGs7uEem5ebMPg-wyFg" name="Q3V0N2" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGG87uEem5ebMPg-wyFg" value="Q3V0N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGHM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGHc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGHs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGH87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGIM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGIc7uEem5ebMPg-wyFg" name="Q3V1N2" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGIs7uEem5ebMPg-wyFg" value="Q3V1N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGI87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGJM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGJc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGJs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGJ87uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGKM7uEem5ebMPg-wyFg" name="Q3V2N2" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGKc7uEem5ebMPg-wyFg" value="Q3V2N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGKs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGK87uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGLM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGLc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGLs7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGL87uEem5ebMPg-wyFg" name="Q3V3N2" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGMM7uEem5ebMPg-wyFg" value="Q3V3N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGMc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGMs7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGM87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGNM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGNc7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGNs7uEem5ebMPg-wyFg" name="Q3V4N2" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGN87uEem5ebMPg-wyFg" value="Q3V4N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGOM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGOc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGOs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGO87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGPM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGPc7uEem5ebMPg-wyFg" name="Q3V5N2" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGPs7uEem5ebMPg-wyFg" value="Q3V5N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGP87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGQM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGQc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGQs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGQ87uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGRM7uEem5ebMPg-wyFg" name="Q3V6N2" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGRc7uEem5ebMPg-wyFg" value="Q3V6N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGRs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGR87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGSM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGSc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGSs7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGS87uEem5ebMPg-wyFg" name="Q3V7N2" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGTM7uEem5ebMPg-wyFg" value="Q3V7N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGTc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGTs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGT87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGUM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGUc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGUs7uEem5ebMPg-wyFg" name="Q3V8N2" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGU87uEem5ebMPg-wyFg" value="Q3V8N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGVM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGVc7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGVs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGV87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGWM7uEem5ebMPg-wyFg" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGWc7uEem5ebMPg-wyFg" name="Q3KPDT" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGWs7uEem5ebMPg-wyFg" value="Q3KPDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGW87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGXM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGXc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGXs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGX87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGYM7uEem5ebMPg-wyFg" name="Q3LNS2" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGYc7uEem5ebMPg-wyFg" value="Q3LNS2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGYs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGY87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGZM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGZc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGZs7uEem5ebMPg-wyFg" name="Q3V9N2" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGZ87uEem5ebMPg-wyFg" value="Q3V9N2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGaM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGac7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGas7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGa87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGbM7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGbc7uEem5ebMPg-wyFg" name="Q3WAN2" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGbs7uEem5ebMPg-wyFg" value="Q3WAN2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGb87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGcM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGcc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGcs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGc87uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGdM7uEem5ebMPg-wyFg" name="Q3KQDT" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGdc7uEem5ebMPg-wyFg" value="Q3KQDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGds7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGd87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGeM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGec7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGes7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGe87uEem5ebMPg-wyFg" name="Q3QHNA" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGfM7uEem5ebMPg-wyFg" value="Q3QHNA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGfc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGfs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGf87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGgM7uEem5ebMPg-wyFg" value="60"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_l8XGgc7uEem5ebMPg-wyFg" name="PIX_CLIENT_1ERE_COMMANDE">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_l8XGgs7uEem5ebMPg-wyFg" value="PIX_CLIENT_1ERE_COMMANDE"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_l8XGg87uEem5ebMPg-wyFg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_l8XGhM7uEem5ebMPg-wyFg" name="P1CCLT" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGhc7uEem5ebMPg-wyFg" value="P1CCLT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGhs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_l8XGh87uEem5ebMPg-wyFg" value="Code client"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGiM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGic7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGis7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGi87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGjM7uEem5ebMPg-wyFg" name="P1CDTE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGjc7uEem5ebMPg-wyFg" value="P1CDTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGjs7uEem5ebMPg-wyFg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_l8XGj87uEem5ebMPg-wyFg" value="Date 1ere commande"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XGkM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XGkc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XGks7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XGk87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XGlM7uEem5ebMPg-wyFg" name="P1CCANAL" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XGlc7uEem5ebMPg-wyFg" value="P1CCANAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XGls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_l8XtIM7uEem5ebMPg-wyFg" value="Mode de Passation 1ere Commande"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtIc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtIs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtI87uEem5ebMPg-wyFg" value="20"/>
      </node>
    </node>
  </node>
  <node defType="com.stambia.rdbms.queryFolder" id="_l8XtJM7uEem5ebMPg-wyFg" name="Requetes utiles">
    <node defType="com.stambia.rdbms.query" id="_l8XtJc7uEem5ebMPg-wyFg" name="Sysdate_formatdujour">
      <attribute defType="com.stambia.rdbms.query.expression" id="_l8XtJs7uEem5ebMPg-wyFg" value="select * &#xD;&#xA;from (&#xD;&#xA;&#x9;SELECT concat(concat(year(CURRENT DATE)-1900,right(concat('0',month(CURRENT DATE)),2)),right(concat('0',day(CURRENT DATE)),2)) AS DateDuJourSynon &#xD;&#xA;&#x9;FROM sysibm.sysdummy1&#xD;&#xA;&#x9;)  T"/>
      <node defType="com.stambia.rdbms.column" id="_l8XtJ87uEem5ebMPg-wyFg" name="DATEDUJOURSYNON" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtKM7uEem5ebMPg-wyFg" value="DATEDUJOURSYNON"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtKc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtKs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtK87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtLM7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtLc7uEem5ebMPg-wyFg" value="33"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_l8XtLs7uEem5ebMPg-wyFg" name="SysDateN-1">
      <attribute defType="com.stambia.rdbms.query.expression" id="_l8XtL87uEem5ebMPg-wyFg" value="select                                                       &#xD;&#xA;VARCHAR_FORMAT(current date - 1 month  -  day(current date) day  + 1 day, 'YYYYMMDD') - 19000000 AS DteDeb,&#xD;&#xA;VARCHAR_FORMAT(current date -  day(current date) day, 'YYYYMMDD') - 19000000   AS DteFin,&#xD;&#xA;trim(VARCHAR_FORMAT((current date - 1 year) - 1 month  -  day(current date) day  + 1 day, 'YYYYMMDD') )AS DteDebN_1&#xD;&#xA;from sysibm.sysdummy1        "/>
      <node defType="com.stambia.rdbms.column" id="_l8XtMM7uEem5ebMPg-wyFg" name="DTEDEB" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtMc7uEem5ebMPg-wyFg" value="DTEDEB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtMs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtM87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtNM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtNc7uEem5ebMPg-wyFg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtNs7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XtN87uEem5ebMPg-wyFg" name="DTEFIN" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtOM7uEem5ebMPg-wyFg" value="DTEFIN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtOc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtOs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtO87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtPM7uEem5ebMPg-wyFg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtPc7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XtPs7uEem5ebMPg-wyFg" name="DTEDEBN_1" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtP87uEem5ebMPg-wyFg" value="DTEDEBN_1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtQM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtQc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtQs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtQ87uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtRM7uEem5ebMPg-wyFg" value="255"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_l8XtRc7uEem5ebMPg-wyFg" name="Mercur_tous_sites_unique">
      <attribute defType="com.stambia.rdbms.query.expression" id="_l8XtRs7uEem5ebMPg-wyFg" value="SELECT DISTINCT  MERCUR FROM orionbd.Eliclip  where MERCUR &lt;> '' &#xD;&#xA;union&#xD;&#xA;SELECT DISTINCT  MERCUR  FROM orionbd.Webcli  where MERCUR &lt;> '' &#xD;&#xA;union&#xD;&#xA;SELECT DISTINCT  MERCUR  FROM orionbd.Wsoclip where MERCUR &lt;> ''&#xD;&#xA;"/>
      <node defType="com.stambia.rdbms.column" id="_l8XtR87uEem5ebMPg-wyFg" name="MERCUR" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtSM7uEem5ebMPg-wyFg" value="MERCUR"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtSc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtSs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtS87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtTM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtTc7uEem5ebMPg-wyFg" value="20"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_l8XtTs7uEem5ebMPg-wyFg" name="DateSynon">
      <attribute defType="com.stambia.rdbms.query.expression" id="_l8XtT87uEem5ebMPg-wyFg" value="select                                                             &#xD;&#xA; VARCHAR_FORMAT(current date - 01 month  -  day(current date) day  +&#xD;&#xA;1 day, 'YYYYMMDD') - 19000000 AS DteDebM1,                         &#xD;&#xA; VARCHAR_FORMAT(current date   -  day(current date) day             &#xD;&#xA;      , 'YYYYMMDD') - 19000000 AS DteFinM1,                         &#xD;&#xA; VARCHAR_FORMAT(current date - 01 day, 'YYYYMMDD') - 19000000 AS    &#xD;&#xA; Hier  ,                                                            &#xD;&#xA; VARCHAR_FORMAT(current date , 'YYYYMMDD') - 19000000 AS Today      &#xD;&#xA; ,VARCHAR_FORMAT(current date + 01 day, 'YYYYMMDD') - 19000000 AS   &#xD;&#xA; Demain                                                             &#xD;&#xA; from sysibm.sysdummy1                                              &#xD;&#xA;"/>
      <node defType="com.stambia.rdbms.column" id="_l8XtUM7uEem5ebMPg-wyFg" name="DTEDEBM1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtUc7uEem5ebMPg-wyFg" value="DTEDEBM1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtUs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtU87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtVM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtVc7uEem5ebMPg-wyFg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtVs7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XtV87uEem5ebMPg-wyFg" name="DTEFINM1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtWM7uEem5ebMPg-wyFg" value="DTEFINM1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtWc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtWs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtW87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtXM7uEem5ebMPg-wyFg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtXc7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XtXs7uEem5ebMPg-wyFg" name="HIER" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtX87uEem5ebMPg-wyFg" value="HIER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtYM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtYc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtYs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtY87uEem5ebMPg-wyFg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtZM7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XtZc7uEem5ebMPg-wyFg" name="TODAY" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtZs7uEem5ebMPg-wyFg" value="TODAY"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtZ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtaM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Xtac7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Xtas7uEem5ebMPg-wyFg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Xta87uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XtbM7uEem5ebMPg-wyFg" name="DEMAIN" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Xtbc7uEem5ebMPg-wyFg" value="DEMAIN"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Xtbs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Xtb87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtcM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Xtcc7uEem5ebMPg-wyFg" value="INTEGER"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Xtcs7uEem5ebMPg-wyFg" value="10"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_l8Xtc87uEem5ebMPg-wyFg" name="Oslzrep_with_rownumber">
      <attribute defType="com.stambia.rdbms.query.expression" id="_l8XtdM7uEem5ebMPg-wyFg" value="select &#x9;LZJKC1, &#xD;&#xA;&#x9;LZJLC1,&#xD;&#xA;&#x9;LZXGN5,&#x9;&#xD;&#xA;&#x9;trim(LZWUT1) as LZWUT1,&#xD;&#xA;&#x9;trim(LZWVT1) as LZWVT1,&#xD;&#xA;&#x9; concat('username',ROW_NUMBER() OVER () ) as username, &#xD;&#xA;&#x9;ROW_NUMBER() OVER () rownumber,&#xD;&#xA;&#x9;case&#xD;&#xA;&#x9;when ( ltrim(rtrim(LZWUT1)) = '' or ltrim(rtrim(LZWVT1)) = '' ) then concat(concat('ecftest',ROW_NUMBER() OVER ()),'@ecf.fr')&#xD;&#xA;&#x9;else concat(concat(ltrim(rtrim(LZWUT1)),'@') , ltrim(rtrim(LZWVT1)))&#xD;&#xA;&#x9;end as mail&#xD;&#xA;from orionbd.oslzrep&#xD;&#xA;order by LZJKC1"/>
      <node defType="com.stambia.rdbms.column" id="_l8Xtdc7uEem5ebMPg-wyFg" name="LZJKC1" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Xtds7uEem5ebMPg-wyFg" value="LZJKC1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Xtd87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XteM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Xtec7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Xtes7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Xte87uEem5ebMPg-wyFg" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XtfM7uEem5ebMPg-wyFg" name="LZJLC1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Xtfc7uEem5ebMPg-wyFg" value="LZJLC1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Xtfs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Xtf87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtgM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Xtgc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Xtgs7uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Xtg87uEem5ebMPg-wyFg" name="LZXGN5" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XthM7uEem5ebMPg-wyFg" value="LZXGN5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Xthc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Xths7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Xth87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtiM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Xtic7uEem5ebMPg-wyFg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Xtis7uEem5ebMPg-wyFg" name="USERNAME" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Xti87uEem5ebMPg-wyFg" value="USERNAME"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8XtjM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Xtjc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Xtjs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Xtj87uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8XtkM7uEem5ebMPg-wyFg" value="28"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Xtkc7uEem5ebMPg-wyFg" name="ROWNUMBER" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Xtks7uEem5ebMPg-wyFg" value="ROWNUMBER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Xtk87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8XtlM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Xtlc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Xtls7uEem5ebMPg-wyFg" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Xtl87uEem5ebMPg-wyFg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8XtmM7uEem5ebMPg-wyFg" name="MAIL" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Xtmc7uEem5ebMPg-wyFg" value="MAIL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Xtms7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Xtm87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8XtnM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Xtnc7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Xtns7uEem5ebMPg-wyFg" value="141"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Xtn87uEem5ebMPg-wyFg" name="LZWUT1" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8XtoM7uEem5ebMPg-wyFg" value="LZWUT1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Xtoc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Xtos7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Xto87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8XtpM7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUMM7uEem5ebMPg-wyFg" value="70"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUMc7uEem5ebMPg-wyFg" name="LZWVT1" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUMs7uEem5ebMPg-wyFg" value="LZWVT1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUM87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUNM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUNc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUNs7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUN87uEem5ebMPg-wyFg" value="70"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_l8YUOM7uEem5ebMPg-wyFg" name="Orazrep_with_rownum">
      <attribute defType="com.stambia.rdbms.query.expression" id="_l8YUOc7uEem5ebMPg-wyFg" value="select ROW_NUMBER() OVER () rownumber,azazcd,azamcd,aztkst,azasst,azawna,azautx,azzltx,azfmtx,azcgcd,azj4cd,azbccd,azbocd,azfwnb,azbacd,azajcd,azcqtx,azi6st,azbon3,azbpn3,azhotx,azhqtx,azbqn3,azakcd,azgidt,aza6na,aza2na,aza3na,aza4st,azftnb, rtrim(azajnb) as azajnb from orionbd.orazrep"/>
      <node defType="com.stambia.rdbms.column" id="_l8YUOs7uEem5ebMPg-wyFg" name="ROWNUMBER" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUO87uEem5ebMPg-wyFg" value="ROWNUMBER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUPM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUPc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUPs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUP87uEem5ebMPg-wyFg" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUQM7uEem5ebMPg-wyFg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUQc7uEem5ebMPg-wyFg" name="AZAZCD" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUQs7uEem5ebMPg-wyFg" value="AZAZCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUQ87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YURM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YURc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YURs7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUR87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUSM7uEem5ebMPg-wyFg" name="AZAMCD" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUSc7uEem5ebMPg-wyFg" value="AZAMCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUSs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUS87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUTM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUTc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUTs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUT87uEem5ebMPg-wyFg" name="AZTKST" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUUM7uEem5ebMPg-wyFg" value="AZTKST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUUc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUUs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUU87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUVM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUVc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUVs7uEem5ebMPg-wyFg" name="AZASST" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUV87uEem5ebMPg-wyFg" value="AZASST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUWM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUWc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUWs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUW87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUXM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUXc7uEem5ebMPg-wyFg" name="AZAWNA" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUXs7uEem5ebMPg-wyFg" value="AZAWNA"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUX87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUYM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUYc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUYs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUY87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUZM7uEem5ebMPg-wyFg" name="AZAUTX" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUZc7uEem5ebMPg-wyFg" value="AZAUTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUZs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUZ87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUaM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUac7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUas7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUa87uEem5ebMPg-wyFg" name="AZZLTX" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUbM7uEem5ebMPg-wyFg" value="AZZLTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUbc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUbs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUb87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUcM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUcc7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUcs7uEem5ebMPg-wyFg" name="AZFMTX" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUc87uEem5ebMPg-wyFg" value="AZFMTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUdM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUdc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUds7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUd87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUeM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUec7uEem5ebMPg-wyFg" name="AZCGCD" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUes7uEem5ebMPg-wyFg" value="AZCGCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUe87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUfM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUfc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUfs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUf87uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUgM7uEem5ebMPg-wyFg" name="AZJ4CD" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUgc7uEem5ebMPg-wyFg" value="AZJ4CD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUgs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUg87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUhM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUhc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUhs7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUh87uEem5ebMPg-wyFg" name="AZBCCD" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUiM7uEem5ebMPg-wyFg" value="AZBCCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUic7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUis7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUi87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUjM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUjc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUjs7uEem5ebMPg-wyFg" name="AZBOCD" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUj87uEem5ebMPg-wyFg" value="AZBOCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUkM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUkc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUks7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUk87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUlM7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUlc7uEem5ebMPg-wyFg" name="AZFWNB" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUls7uEem5ebMPg-wyFg" value="AZFWNB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUl87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUmM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUmc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUms7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUm87uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUnM7uEem5ebMPg-wyFg" name="AZBACD" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUnc7uEem5ebMPg-wyFg" value="AZBACD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUns7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUn87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUoM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUoc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUos7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUo87uEem5ebMPg-wyFg" name="AZAJCD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUpM7uEem5ebMPg-wyFg" value="AZAJCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUpc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUps7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUp87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUqM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUqc7uEem5ebMPg-wyFg" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUqs7uEem5ebMPg-wyFg" name="AZCQTX" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUq87uEem5ebMPg-wyFg" value="AZCQTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8YUrM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8YUrc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8YUrs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8YUr87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8YUsM7uEem5ebMPg-wyFg" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8YUsc7uEem5ebMPg-wyFg" name="AZI6ST" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8YUss7uEem5ebMPg-wyFg" value="AZI6ST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7QM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7Qc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7Qs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7Q87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7RM7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7Rc7uEem5ebMPg-wyFg" name="AZBON3" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7Rs7uEem5ebMPg-wyFg" value="AZBON3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7R87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7SM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7Sc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7Ss7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7S87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7TM7uEem5ebMPg-wyFg" name="AZBPN3" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7Tc7uEem5ebMPg-wyFg" value="AZBPN3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7Ts7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7T87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7UM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7Uc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7Us7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7U87uEem5ebMPg-wyFg" name="AZHOTX" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7VM7uEem5ebMPg-wyFg" value="AZHOTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7Vc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7Vs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7V87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7WM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7Wc7uEem5ebMPg-wyFg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7Ws7uEem5ebMPg-wyFg" name="AZHQTX" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7W87uEem5ebMPg-wyFg" value="AZHQTX"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7XM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7Xc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7Xs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7X87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7YM7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7Yc7uEem5ebMPg-wyFg" name="AZBQN3" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7Ys7uEem5ebMPg-wyFg" value="AZBQN3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7Y87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7ZM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7Zc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7Zs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7Z87uEem5ebMPg-wyFg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7aM7uEem5ebMPg-wyFg" name="AZAKCD" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7ac7uEem5ebMPg-wyFg" value="AZAKCD"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7as7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7a87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7bM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7bc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7bs7uEem5ebMPg-wyFg" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7b87uEem5ebMPg-wyFg" name="AZGIDT" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7cM7uEem5ebMPg-wyFg" value="AZGIDT"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7cc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7cs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7c87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7dM7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7dc7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7ds7uEem5ebMPg-wyFg" name="AZA6NA" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7d87uEem5ebMPg-wyFg" value="AZA6NA"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7eM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7ec7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7es7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7e87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7fM7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7fc7uEem5ebMPg-wyFg" name="AZA2NA" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7fs7uEem5ebMPg-wyFg" value="AZA2NA"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7f87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7gM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7gc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7gs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7g87uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7hM7uEem5ebMPg-wyFg" name="AZA3NA" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7hc7uEem5ebMPg-wyFg" value="AZA3NA"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7hs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7h87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7iM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7ic7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7is7uEem5ebMPg-wyFg" value="35"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7i87uEem5ebMPg-wyFg" name="AZA4ST" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7jM7uEem5ebMPg-wyFg" value="AZA4ST"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7jc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7js7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7j87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7kM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7kc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7ks7uEem5ebMPg-wyFg" name="AZFTNB" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7k87uEem5ebMPg-wyFg" value="AZFTNB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7lM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7lc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7ls7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7l87uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7mM7uEem5ebMPg-wyFg" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7mc7uEem5ebMPg-wyFg" name="AZAJNB" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7ms7uEem5ebMPg-wyFg" value="AZAJNB"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7m87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7nM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7nc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7ns7uEem5ebMPg-wyFg" value="VARCHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7n87uEem5ebMPg-wyFg" value="7"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.query" id="_l8Y7oM7uEem5ebMPg-wyFg" name="WEBCLI_with_rownum">
      <attribute defType="com.stambia.rdbms.query.expression" id="_l8Y7oc7uEem5ebMPg-wyFg" value="select ROW_NUMBER() OVER () rownumber,idwebc,cptcli,nomcli,precli,nivcli,actcli, climail,pwdmd5,maxcde,lngcli from orionbd.webcli"/>
      <node defType="com.stambia.rdbms.column" id="_l8Y7os7uEem5ebMPg-wyFg" name="ROWNUMBER" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7o87uEem5ebMPg-wyFg" value="ROWNUMBER"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7pM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7pc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7ps7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7p87uEem5ebMPg-wyFg" value="BIGINT"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7qM7uEem5ebMPg-wyFg" value="19"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7qc7uEem5ebMPg-wyFg" name="IDWEBC" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7qs7uEem5ebMPg-wyFg" value="IDWEBC"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7q87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7rM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7rc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7rs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7r87uEem5ebMPg-wyFg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7sM7uEem5ebMPg-wyFg" name="CPTCLI" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7sc7uEem5ebMPg-wyFg" value="CPTCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7ss7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7s87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7tM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7tc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7ts7uEem5ebMPg-wyFg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7t87uEem5ebMPg-wyFg" name="NOMCLI" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7uM7uEem5ebMPg-wyFg" value="NOMCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7uc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Y7us7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Y7u87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Y7vM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Y7vc7uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Y7vs7uEem5ebMPg-wyFg" name="PRECLI" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Y7v87uEem5ebMPg-wyFg" value="PRECLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Y7wM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ZiUM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ZiUc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ZiUs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ZiU87uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ZiVM7uEem5ebMPg-wyFg" name="NIVCLI" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ZiVc7uEem5ebMPg-wyFg" value="NIVCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ZiVs7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ZiV87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ZiWM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ZiWc7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ZiWs7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ZiW87uEem5ebMPg-wyFg" name="ACTCLI" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ZiXM7uEem5ebMPg-wyFg" value="ACTCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ZiXc7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ZiXs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ZiX87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ZiYM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ZiYc7uEem5ebMPg-wyFg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ZiYs7uEem5ebMPg-wyFg" name="CLIMAIL" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ZiY87uEem5ebMPg-wyFg" value="CLIMAIL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8ZiZM7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ZiZc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ZiZs7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ZiZ87uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8ZiaM7uEem5ebMPg-wyFg" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Ziac7uEem5ebMPg-wyFg" name="PWDMD5" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Zias7uEem5ebMPg-wyFg" value="PWDMD5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Zia87uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8ZibM7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Zibc7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Zibs7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Zib87uEem5ebMPg-wyFg" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8ZicM7uEem5ebMPg-wyFg" name="MAXCDE" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8Zicc7uEem5ebMPg-wyFg" value="MAXCDE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Zics7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Zic87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8ZidM7uEem5ebMPg-wyFg" value="2"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8Zidc7uEem5ebMPg-wyFg" value="DECIMAL"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Zids7uEem5ebMPg-wyFg" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_l8Zid87uEem5ebMPg-wyFg" name="LNGCLI" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_l8ZieM7uEem5ebMPg-wyFg" value="LNGCLI"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_l8Ziec7uEem5ebMPg-wyFg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_l8Zies7uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_l8Zie87uEem5ebMPg-wyFg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_l8ZifM7uEem5ebMPg-wyFg" value="CHAR"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_l8Zifc7uEem5ebMPg-wyFg" value="2"/>
      </node>
    </node>
  </node>
</md:node>