<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.json.schema" id="_gF6fwB1oEemosZyXDkDTkg" md:ref="platform:/plugin/com.indy.environment/.tech/file/json.tech#UUID_TECH_JSON1?fileId=UUID_TECH_JSON1$type=tech$name=json?">
  <node defType="com.stambia.json.rootObject" id="_gMnksB1oEemosZyXDkDTkg" name="chdinfos">
    <attribute defType="com.stambia.json.rootObject.reverseURLPath" id="_gNRsAB1oEemosZyXDkDTkg"/>
    <attribute defType="com.stambia.json.rootObject.reverseFilePath" id="_gNRsAR1oEemosZyXDkDTkg" value="C:\app_dev\Used_Files\In_Files\Json\chdinfos.json"/>
    <node defType="com.stambia.json.value" id="_qBMnsR1pEemosZyXDkDTkg" name="retour" position="1">
      <attribute defType="com.stambia.json.value.type" id="_qBMnsh1pEemosZyXDkDTkg" value="string"/>
    </node>
    <node defType="com.stambia.json.object" id="_qBMnsx1pEemosZyXDkDTkg" name="data" position="2">
      <node defType="com.stambia.json.value" id="_qBMntB1pEemosZyXDkDTkg" name="idclientchomette" position="1">
        <attribute defType="com.stambia.json.value.type" id="_qBMntR1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnth1pEemosZyXDkDTkg" name="comptewebexiste " position="2">
        <attribute defType="com.stambia.json.value.type" id="_qBMntx1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnuB1pEemosZyXDkDTkg" name="Raison_sociale" position="3">
        <attribute defType="com.stambia.json.value.type" id="_qBMnuR1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnuh1pEemosZyXDkDTkg" name="rue1" position="4">
        <attribute defType="com.stambia.json.value.type" id="_qBMnux1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnvB1pEemosZyXDkDTkg" name="rue2" position="5">
        <attribute defType="com.stambia.json.value.type" id="_qBMnvR1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnvh1pEemosZyXDkDTkg" name="code_postal" position="6">
        <attribute defType="com.stambia.json.value.type" id="_qBMnvx1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnwB1pEemosZyXDkDTkg" name="ville" position="7">
        <attribute defType="com.stambia.json.value.type" id="_qBMnwR1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnwh1pEemosZyXDkDTkg" name="Pays" position="8">
        <attribute defType="com.stambia.json.value.type" id="_qBMnwx1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnxB1pEemosZyXDkDTkg" name="Telephone" position="9">
        <attribute defType="com.stambia.json.value.type" id="_qBMnxR1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnxh1pEemosZyXDkDTkg" name="email" position="10">
        <attribute defType="com.stambia.json.value.type" id="_qBMnxx1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnyB1pEemosZyXDkDTkg" name="tva" position="11">
        <attribute defType="com.stambia.json.value.type" id="_qBMnyR1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnyh1pEemosZyXDkDTkg" name="siret" position="12">
        <attribute defType="com.stambia.json.value.type" id="_qBMnyx1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnzB1pEemosZyXDkDTkg" name="culinary_specialties" position="13">
        <attribute defType="com.stambia.json.value.type" id="_qBMnzR1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMnzh1pEemosZyXDkDTkg" name="tableware_count" position="14">
        <attribute defType="com.stambia.json.value.type" id="_qBMnzx1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMn0B1pEemosZyXDkDTkg" name="meals_served_count" position="15">
        <attribute defType="com.stambia.json.value.type" id="_qBMn0R1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMn0h1pEemosZyXDkDTkg" name="rooms_count" position="16">
        <attribute defType="com.stambia.json.value.type" id="_qBMn0x1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMn1B1pEemosZyXDkDTkg" name="beds_count" position="17">
        <attribute defType="com.stambia.json.value.type" id="_qBMn1R1pEemosZyXDkDTkg" value="string"/>
      </node>
      <node defType="com.stambia.json.value" id="_qBMn1h1pEemosZyXDkDTkg" name="students_count" position="18">
        <attribute defType="com.stambia.json.value.type" id="_qBMn1x1pEemosZyXDkDTkg" value="string"/>
      </node>
    </node>
  </node>
</md:node>