<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_r4-YcBT0EemsgZOghPO5Pg" name="RapportActiviteSodexo" md:ref="platform:/plugin/com.indy.environment/.tech/rdbms/excel/excel.rdbms.md#UUID_MD_RDBMS_MICROSOFT_EXCEL?fileId=UUID_MD_RDBMS_MICROSOFT_EXCEL$type=md$name=Microsoft%20Excel?">
  <attribute defType="com.stambia.rdbms.server.url" id="_2Yvi8BT0EemsgZOghPO5Pg" value="jdbc:stambia:excel://C:\app_dev\Used_Files\In_Files\Excel_Files\WEBSODEXO.xlsx?forceDatatypeAsString=true&amp;columnNameStyle=PRESERVE"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_2Yvi8RT0EemsgZOghPO5Pg" value="com.stambia.jdbc.driver.excel.XLSXDriver"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_2Yvi8hT0EemsgZOghPO5Pg" value="3951C0D79B227B95C1DC348DD0BCE8F1"/>
  <node defType="com.stambia.rdbms.schema" id="_sB4c0BT0EemsgZOghPO5Pg" name="WEBSODEXO">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_sCLXwBT0EemsgZOghPO5Pg" value="WEBSODEXO"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_sCLXwRT0EemsgZOghPO5Pg" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_sCLXwhT0EemsgZOghPO5Pg" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_sCLXwxT0EemsgZOghPO5Pg" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_2YS3ART0EemsgZOghPO5Pg" name="WEBSODEXO1$">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_2YS3AhT0EemsgZOghPO5Pg" value="WEBSODEXO1$"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_2YS3AxT0EemsgZOghPO5Pg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_2YcoABT0EemsgZOghPO5Pg" name="Compte client" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_2YcoART0EemsgZOghPO5Pg" value="Compte client"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_2YcoAhT0EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_2YcoAxT0EemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_2YcoBBT0EemsgZOghPO5Pg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_2YcoBRT0EemsgZOghPO5Pg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_2YcoBhT0EemsgZOghPO5Pg" name="Raison Sociale" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_2YcoBxT0EemsgZOghPO5Pg" value="Raison Sociale"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_2YcoCBT0EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_2YcoCRT0EemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_2YcoChT0EemsgZOghPO5Pg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_2YcoCxT0EemsgZOghPO5Pg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_2YcoDBT0EemsgZOghPO5Pg" name="B0BTPR TOTAL" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_2YcoDRT0EemsgZOghPO5Pg" value="B0BTPR TOTAL"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_2YcoDhT0EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_2YcoDxT0EemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_2YcoEBT0EemsgZOghPO5Pg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_2YcoERT0EemsgZOghPO5Pg" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_2YcoEhT0EemsgZOghPO5Pg" name="B0BTPR COMPTAGE" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_2YcoExT0EemsgZOghPO5Pg" value="B0BTPR COMPTAGE"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_2YcoFBT0EemsgZOghPO5Pg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_2YcoFRT0EemsgZOghPO5Pg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_2YcoFhT0EemsgZOghPO5Pg" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_2YcoFxT0EemsgZOghPO5Pg" value="VARCHAR"/>
      </node>
    </node>
  </node>
</md:node>