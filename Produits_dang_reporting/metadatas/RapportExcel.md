<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_0BS7IGqwEeumDM2wiIScNA" name="RapportExcel" md:ref="platform:/plugin/com.indy.environment/technology/rdbms/excel/excel.rdbms.md#UUID_MD_RDBMS_MICROSOFT_EXCEL?fileId=UUID_MD_RDBMS_MICROSOFT_EXCEL$type=md$name=Microsoft%20Excel?">
  <attribute defType="com.stambia.rdbms.server.url" id="_D7K94GqxEeumDM2wiIScNA" value="jdbc:stambia:excel://C:\app_dev\Used_Files\In_Files\Excel_Files\Prdt_dangereux.xlsx?forceDatatypeAsString=true&amp;columnNameStyle=PRESERVE"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_D7VV8GqxEeumDM2wiIScNA" value="com.stambia.jdbc.driver.excel.XLSXDriver"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_D7YZQGqxEeumDM2wiIScNA" value="3951C0D79B227B95C1DC348DD0BCE8F1"/>
  <node defType="com.stambia.rdbms.schema" id="_mwbzMGyLEeuSfeATVu2URQ" name="Prdt_dangereux">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_mw67YGyLEeuSfeATVu2URQ" value="Prdt_dangereux"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_mw7icGyLEeuSfeATVu2URQ" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_mw7icWyLEeuSfeATVu2URQ" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_mw7icmyLEeuSfeATVu2URQ" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_od-6IWyLEeuSfeATVu2URQ" name="Prd_dgr$">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_od_hMGyLEeuSfeATVu2URQ" value="Prd_dgr$"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_od_hMWyLEeuSfeATVu2URQ" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_oeGO4GyLEeuSfeATVu2URQ" name="Article" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_oeGO4WyLEeuSfeATVu2URQ" value="Article"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_oeGO4myLEeuSfeATVu2URQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_oeG18GyLEeuSfeATVu2URQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_oeG18WyLEeuSfeATVu2URQ" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_oeG18myLEeuSfeATVu2URQ" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_oeG182yLEeuSfeATVu2URQ" name="Designation" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_oeG19GyLEeuSfeATVu2URQ" value="Designation"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_oeG19WyLEeuSfeATVu2URQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_oeHdAGyLEeuSfeATVu2URQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_oeHdAWyLEeuSfeATVu2URQ" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_oeHdAmyLEeuSfeATVu2URQ" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_oeHdA2yLEeuSfeATVu2URQ" name="Qte_expediees" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_oeHdBGyLEeuSfeATVu2URQ" value="Qte_expediees"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_oeHdBWyLEeuSfeATVu2URQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_oeHdBmyLEeuSfeATVu2URQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_oeHdB2yLEeuSfeATVu2URQ" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_oeHdCGyLEeuSfeATVu2URQ" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_oeHdCWyLEeuSfeATVu2URQ" name="Qte_receptionnees" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_oeHdCmyLEeuSfeATVu2URQ" value="Qte_receptionnees"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_oeHdC2yLEeuSfeATVu2URQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_oeHdDGyLEeuSfeATVu2URQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_oeHdDWyLEeuSfeATVu2URQ" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_oeHdDmyLEeuSfeATVu2URQ" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_oeHdD2yLEeuSfeATVu2URQ" name="Fournisseur" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_oeHdEGyLEeuSfeATVu2URQ" value="Fournisseur"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_oeHdEWyLEeuSfeATVu2URQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_oeHdEmyLEeuSfeATVu2URQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_oeHdE2yLEeuSfeATVu2URQ" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_oeHdFGyLEeuSfeATVu2URQ" value="VARCHAR"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_oeHdFWyLEeuSfeATVu2URQ" name="Transporteur" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_oeHdFmyLEeuSfeATVu2URQ" value="Transporteur"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_oeHdF2yLEeuSfeATVu2URQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_oeHdGGyLEeuSfeATVu2URQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.remarks" id="_oeHdGWyLEeuSfeATVu2URQ" value=""/>
        <attribute defType="com.stambia.rdbms.column.type" id="_oeHdGmyLEeuSfeATVu2URQ" value="VARCHAR"/>
      </node>
    </node>
  </node>
</md:node>